<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user		= JFactory::getUser();
$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
?>
<form action="<?php echo JRoute::_('index.php?option=com_heartdart&view=messages'); ?>" method="post" id="adminForm" name="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif; ?>

<?php
	// Search tools bar
	echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
?>
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th width="2%"><?php echo JText::_('COM_HEARTDART_NUM'); ?></th>
				<th width="2%">
					<?php echo JHtml::_('grid.checkall'); ?>
				</th>
				<th width="2%" style="min-width:55px" class="nowrap center">
					<?php echo JHtml::_('grid.sort', 'JSTATUS', 'a.published', $listDirn, $listOrder); ?>
				</th>
				<th width="20%">
					<?php echo JHtml::_('grid.sort', 'COM_HEARTDART_MESSAGES_HEADING_MESSAGE_CAPTION', 'a.message', $listDirn, $listOrder);?>
				</th>
				<th width="10%"><?php echo JHtml::_('grid.sort', 'COM_HEARTDART_MESSAGES_HEADING_CREATOR', 'u.name', $listDirn, $listOrder);?></th>

				<?php /*<th width="1%"><?php echo JHtml::_('grid.sort', 'COM_HEARTDART_MESSAGES_HEADING_YES_VOTE', 'pi.yes_vote', $listDirn, $listOrder);?></th>
				<th width="1%"><?php echo JHtml::_('grid.sort', 'COM_HEARTDART_MESSAGES_HEADING_NO_VOTE', 'pi.no_vote', $listDirn, $listOrder);?></th>*/?>

				<th width="2%"><?php echo JText::_('COM_HEARTDART_MESSAGES_HEADING_CREATED'); ?></th>
				<th width="2%"><?php echo JHtml::_('grid.sort', 'COM_HEARTDART_MESSAGES_HEADING_MSGID', 'a.msg_id', $listDirn, $listOrder);?></th>
			</tr>
		</thead>

		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) :
					$link = JRoute::_('index.php?option=com_heartdart&view=message&id=' . $row->msg_id); ?>
					<tr>
						<td><?php echo $this->pagination->getRowOffset($i); ?></td>
						<td><?php echo JHtml::_('grid.id', $i, $row->msg_id); ?></td>
						<td class="center">
							<div class="btn-group">
								<?php echo JHtml::_('jgrid.published', $row->published, $i, 'message.', true, 'cb'); ?>
							</div>
						</td>
						<td><a href="<?php echo $link ?>"><?php echo $row->message; ?></a></td>
						<td><?php echo $row->author; ?></td>
						<?php /*<td><?php echo $row->yes_vote; ?></td>
						<td><?php echo $row->no_vote; ?></td> */ ?>
						<td><?php echo $row->created; ?></td>
						<td><?php echo $row->msg_id; ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="9"><?php echo $this->pagination->getListFooter(); ?></td>
			</tr>
		</tfoot>
	</table>
	<?php //echo $this->pagination->getListFooter(); ?>
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHtml::_('form.token'); ?>
	</div>
</form>