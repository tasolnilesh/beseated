<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Categorory View
 *
 * @since  0.0.1
 */
class HeartdartViewMessage extends JViewLegacy
{
	/**
	 * View Category
	 *
	 * @var   form
	 */
	protected $form = null;

	/**
	 * Display the Category view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	public function display($tpl = null)
	{
		// Get the Data
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new RuntimeException(implode('<br />', $errors), 500);

			return false;
		}
		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	protected function addToolBar()
	{
		$input = JFactory::getApplication()->input;

		// Hide Joomla Administrator Main menu
		$input->set('hidemainmenu', true);

		$isNew = ($this->item->msg_id == 0);

		if ($isNew)
		{
			$title = JText::_('COM_HEARTDART_MESSAGE_LIBRARY_NEW');
		}
		else
		{
			$title = JText::_('COM_HEARTDART_MESSAGE_LIBRARY_EDIT');
		}

		JToolBarHelper::title($title, 'Message');
		JToolBarHelper::save('message.save');
		JToolBarHelper::cancel(
			'catlibraryegory.cancel',
			$isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE'
		);
	}
}
