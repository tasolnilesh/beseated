<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Categories View
 *
 * @since  0.0.1
 */
class HeartdartViewLibraries extends JViewLegacy
{
	/**
	 * Display the Categories view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since  0.0.1
	 */
	function display($tpl = null)
	{
		// Get data from the model
		$this->items         = $this->get('Items');
		$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');
		//$this->filterForm    = $this->get('FilterForm');
		//$this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode('<br />', $errors), 500);

			return false;
		}

		// Set the tool-bar and number of found items
		$this->addToolBar();
		$this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	protected function addToolBar()
	{
		$title = JText::_('COM_HEARTDART_MANAGER_LIBRARIES');

		if ($this->pagination->total)
		{
			$title .= "<span style='font-size: 0.5em; vertical-align: middle;'>(" . $this->pagination->total . ")</span>";
		}

		JToolBarHelper::title($title, 'Libraries');
		JToolBarHelper::addNew('library.add');
		JToolBarHelper::editList('library.edit');
		JToolBarHelper::deleteList('', 'libraries.delete');
		JToolbarHelper::preferences('com_heartdart');

		$vName = "libraries";

		JHtmlSidebar::addEntry(JText::_('Categories'), 'index.php?option=com_heartdart&view=categories', $vName == 'categories');
		JHtmlSidebar::addEntry(JText::_('Libraries'), 'index.php?option=com_heartdart&view=libraries', $vName == 'libraries');
		JHtmlSidebar::addEntry(JText::_('Messages'), 'index.php?option=com_heartdart&view=messages', $vName == 'messages');
		JHtmlSidebar::addEntry(JText::_('Reports'), 'index.php?option=com_heartdart&view=reports', $vName == 'reports');

		JHtmlSidebar::setAction('index.php?option=com_heartdart&view=libraries');
	}
}
