<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

JHtml::_('formbehavior.chosen', 'select');
$listOrder     = $this->escape($this->state->get('list.ordering'));
$listDirn      = $this->escape($this->state->get('list.direction'));
?>
<form action="index.php?option=com_heartdart&view=categories" method="post" id="adminForm" name="adminForm">

<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
<?php
	// Search tools bar
	//echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
?>
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="2%"><?php echo JText::_('COM_HEARTDART_LIBRARIES_NUM'); ?></th>
			<th width="4%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
			<th width="2%" style="min-width:55px" class="nowrap center">
				<?php echo JHtml::_('grid.sort', 'COM_HEARTDART_LIBRARIES_PUBLISHED', 'a.published', $listDirn, $listOrder); ?>
			</th>
			<th width="25%">
				<?php echo JHtml::_('grid.sort', 'COM_HEARTDART_LIBRARIES_NAME', 'a.lib_name', $listDirn, $listOrder);?>
			</th>
			<?php /*<th width="50%">
				<?php echo JHtml::_('grid.sort', 'COM_HEARTDART_LIBRARIES_DESCRIPTION', 'a.description 	', $listDirn, $listOrder); ?>
			</th> */ ?>

			<th width="20%">
				<?php echo JText::_('COM_HEARTDART_LIBRARIES_ADD_MEDIA_TO_LIBRATRY'); ?>
			</th>
			<th width="20%">
				<?php echo JText::_('COM_HEARTDART_LIBRARIES_ADD_MESSAGES_TO_LIBRATRY'); ?>
			</th>
			<th width="10%">
				<?php echo JText::_('COM_HEARTDART_LIBRARIES_LIBRATRY_SCORE'); ?>
			</th>
			<th width="10%">
				<?php echo JHtml::_('grid.sort', 'COM_HEARTDART_LIBRARIES_LIBRARY_ID', 'a.library_id', $listDirn, $listOrder); ?>
			</th>
		</tr>
		</thead>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) :
					$link = JRoute::_('index.php?option=com_heartdart&view=librarymedias&library_id=' . $row->library_id);
				?>
					<tr>
						<td><?php echo $this->pagination->getRowOffset($i); ?></td>
						<td>
							<?php echo JHtml::_('grid.id', $i, $row->library_id); ?>
						</td>
						<td width="2%" style="min-width:55px" class="nowrap center">
							<?php echo JHtml::_('jgrid.published', $row->published, $i, 'libraries.', true, 'cb'); ?>
						</td>
						<td>
							<a href="<?php echo $link ?>">
								<?php echo $row->lib_name; ?>
							</a>
						</td>

						<td>
							<?php /*<a href="index.php?option=com_heartdart&view=librarymedia&library_id=<?php echo $row->library_id; ?>">Add media to library</a> */ ?>
							<a class="btn-add-media" href="index.php?option=com_heartdart&view=librarymedia&library_id=<?php echo $row->library_id; ?>&tmpl=component" role="button" data-target="#myModal" data-toggle="modal">Add media to library</a>
						</td>
						<td>
							<a href="index.php?option=com_heartdart&view=message&msg_id=0&library_id=<?php echo $row->library_id; ?>">Add Messages</a>
						</td>
						<td class="nowrap center">
							<?php echo $row->score; ?>
						</td>
						<td class="nowrap center">
							<?php echo $row->library_id; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	<?php echo $this->pagination->getListFooter(); ?>
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
	<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<?php /*<a class="btn-add-media" href="index.php?option=com_heartdart&view=librarymedia&library_id=1&tmpl=component" role="button" data-target="#myModal" data-toggle="modal">Add Media</a> */?>

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add Library Media</h3>
	</div>
	<div class="modal-body">

	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary">Save changes</button>
	</div>
</div>
