-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 12, 2014 at 04:26 PM
-- Server version: 5.5.35
-- PHP Version: 5.3.10-1ubuntu3.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bcted`
--

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_venue`
--

CREATE TABLE IF NOT EXISTS `#__bted_venue` (
  `venue_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `venue_name` varchar(255) NOT NULL,
  `venue_address` varchar(255) NOT NULL,
  `venue_about` text NOT NULL,
  `venue_amenities` text NOT NULL,
  `venue_signs` varchar(255) NOT NULL,
  `venue_timings` varchar(255) NOT NULL,
  `venue_image` varchar(255) NOT NULL,
  `venue_active` int(11) NOT NULL,
  `venue_created` varchar(255) NOT NULL,
  `venue_modified` varchar(255) NOT NULL,
  `time_stamp` bigint(20) NOT NULL,
  PRIMARY KEY (`venue_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted Venues'
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_user_profile`
--

CREATE TABLE IF NOT EXISTS `#__bted_user_profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `heartdart_status` varchar(255) NOT NULL,
  `total_msg_received` int(11) NOT NULL,
  `total_msg_send` int(11) NOT NULL,
  `total_friends` int(11) NOT NULL,
  `mood_color` varchar(255) NOT NULL,
  `phoneno` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`profile_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted users Profile'
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_venue_table_type`
--

CREATE TABLE IF NOT EXISTS `#__bted_venue_table_type` (
  `venue_table_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_table_type_name` varchar(255) NOT NULL,
  `venue_table_type_active` int(11) NOT NULL,
  `venue_table_type_created` varchar(255) NOT NULL,
  `venue_table_type_modified` varchar(255) NOT NULL,
  `time_stamp` bigint(20) NOT NULL,

  PRIMARY KEY (`venue_table_type_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted Venues Table Type'
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_venue_table`
--

CREATE TABLE IF NOT EXISTS `#__bted_venue_table` (
  `venue_table_id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` varchar(255) NOT NULL,
  `venue_table_type_id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `venue_table_name` varchar(255) NOT NULL,
  `venue_table_price` varchar(255) NOT NULL,
  `venue_table_description` varchar(255) NOT NULL,
  `venue_table_active` varchar(255) NOT NULL,
  `venue_table_created` varchar(255) NOT NULL,
  `venue_table_modified` varchar(255) NOT NULL,
  `time_stamp` bigint(20) NOT NULL,

  PRIMARY KEY (`venue_table_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted Venues Tables'
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_venue_booking`
--

CREATE TABLE IF NOT EXISTS `#__bted_venue_booking` (
  `venue_booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `venue_table_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `venue_booking_table_privacy` int(11) NOT NULL,
  `venue_booking_datetime` varchar(255) NOT NULL,
  `venue_booking_number_of_guest` int(11) NOT NULL,
  `venue_booking_additional_info` varchar(255) NOT NULL,
  `venue_booking_created` varchar(255) NOT NULL,
  `time_stamp` bigint(20) NOT NULL,

  PRIMARY KEY (`venue_booking_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted Venues Tables Booking'
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_company`
--

CREATE TABLE IF NOT EXISTS `#__bted_company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `company_active` int(2) NOT NULL,
  `company_created` varchar(255) NOT NULL,
  `company_modified` varchar(255) NOT NULL,
  `time_stamp` bigint(20) NOT NULL,

  PRIMARY KEY (`company_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted Company'
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_company_services`
--

CREATE TABLE IF NOT EXISTS `#__bted_company_services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_image` varchar(255) NOT NULL,
  `service_description` varchar(255) NOT NULL,
  `service_active` int(2) NOT NULL,
  `service_created` varchar(255) NOT NULL,
  `service_modified` varchar(255) NOT NULL,
  `time_stamp` bigint(20) NOT NULL,

  PRIMARY KEY (`service_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted Company Services'
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_service_booking`
--

CREATE TABLE IF NOT EXISTS `#__bted_service_booking` (
  `service_booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_booking_datetime` varchar(255) NOT NULL,
  `service_location` varchar(255) NOT NULL,
  `service_booking_additional_info` varchar(255) NOT NULL,
  `service_booking_created` varchar(255) NOT NULL,
  `service_booking_is_cancelled` int(2) NOT NULL,
  `time_stamp` bigint(20) NOT NULL,

  PRIMARY KEY (`service_booking_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted Company Services Booking'
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_package`
--

CREATE TABLE IF NOT EXISTS `#__bted_package` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `package_image` varchar(255) NOT NULL,
  `package_details` varchar(255) NOT NULL,
  `package_price` int(8,2) NOT NULL,
  `package_active` int(2) NOT NULL,
  `package_created` varchar(255) NOT NULL,
  `package_modified` varchar(255) NOT NULL,
  `time_stamp` bigint(20) NOT NULL,

  PRIMARY KEY (`package_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted Packages'
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_package_purchased`
--

CREATE TABLE IF NOT EXISTS `#__bted_package_purchased` (
  `package_purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `package_datetime` varchar(255) NOT NULL,
  `package_number_of_guest` int(11) NOT NULL,
  `package_location` varchar(255) NOT NULL,
  `package_additional_info` varchar(255) NOT NULL,
  `package_purchased_datetime` varchar(255) NOT NULL,
  `time_stamp` bigint(20) NOT NULL,

  PRIMARY KEY (`package_purchase_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted Package Purchased'
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_ratings`
--

CREATE TABLE IF NOT EXISTS `#__bted_ratings` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating_type` varchar(255) NOT NULL,
  `rated_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `rating_comment` varchar(255) NOT NULL,
  `rating_datetime` varchar(255) NOT NULL,
  `time_stamp` bigint(20) NOT NULL,

  PRIMARY KEY (`rating_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted Ratings'
AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__bted_favourites`
--

CREATE TABLE IF NOT EXISTS `#__bted_favourites` (
  `favourite_id` int(11) NOT NULL AUTO_INCREMENT,
  `favourite_type` varchar(255) NOT NULL,
  `favourited_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `favourite_datetime` varchar(255) NOT NULL,
  `time_stamp` bigint(20) NOT NULL,

  PRIMARY KEY (`favourite_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COMMENT='Store BC Ted Favourites'
AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
