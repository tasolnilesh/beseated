DROP TABLE IF EXISTS `#__bted_venue`;

DROP TABLE IF EXISTS `#__bted_user_profile`;

DROP TABLE IF EXISTS `#__bted_venue_table_type`;

DROP TABLE IF EXISTS `#__bted_venue_table`;

DROP TABLE IF EXISTS `#__bted_venue_booking`;

DROP TABLE IF EXISTS `#__bted_company`;

DROP TABLE IF EXISTS `#__bted_company_services`;

DROP TABLE IF EXISTS `#__bted_service_booking`;

DROP TABLE IF EXISTS `#__bted_packages`;

DROP TABLE IF EXISTS `#__bted_package_purchased`;

DROP TABLE IF EXISTS `#__bted_ratings`;

DROP TABLE IF EXISTS `#__bted_favourites`;
