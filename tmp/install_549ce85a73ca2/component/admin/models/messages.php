<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class HeartdartModelMessages extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'msg_id','a.msg_id',
				'message','a.message',
				'published','a.published'
			);
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Filter: like / search
		$search = $this->getState('filter.search');

		// Create the base select statement.
		$query->select('a.*')
			->from($db->quoteName('#__heartdart_message','a'));


		if (!empty($search))
		{
			$like = $db->quote('%' . $search . '%');
			//$query->where('a.caption LIKE ' . $like);
		}

		// Filter by published state
		$published = $this->getState('filter.published');
		//$fullordering = $this->getState('list.fullordering');
		//echo "<br>fullordering : " . $fullordering;
		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.published IN (0, 1))');
		}

		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering', 'a.msg_id');
		$orderDirn 	= $this->state->get('list.direction', 'asc');

		$query->group('a.msg_id');

		//echo $orderCol . " || " . $orderDirn;
		$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));

		//$query->order($db->quoteName('a.post_id') . ' DESC');

		//echo $query->dump();

		return $query;
	}

	/*
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   12.2
	 */
	protected function populateState($ordering = 'id', $direction = 'ASC')
	{
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.' . $layout;
		}

		$value = $app->input->get('limit', $app->getCfg('list_limit', 0), 'uint');
		$this->setState('list.limit', 10);

		$value = $app->input->get('limitstart', 0, 'uint');
		$this->setState('list.start', $value);

		$searchdate = $this->getUserStateFromRequest($this->context . '.filter.selecteddate', 'filter_selecteddate');
		$this->setState('filter.selecteddate', $searchdate);

		$orderCol = $app->input->get('filter_order', 'a.msg_id');

		if (!in_array($orderCol, $this->filter_fields))
		{
			$orderCol = 'a.msg_id';
		}

		$this->setState('list.ordering', $orderCol);

		$listOrder = $app->input->get('filter_order_Dir', 'ASC');

		$this->setState('layout', $app->input->getString('layout'));

		// List state information.
		//parent:opulateState();

	}
	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	/*public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}*/
}
