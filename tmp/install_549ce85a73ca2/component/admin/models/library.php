<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Category Model
 *
 * @since  0.0.1
 */
class HeartdartModelLibrary extends JModelAdmin
{
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   1.6
	 */
	public function getTable($type = 'Library', $prefix = 'HeartdartTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   0.0.1
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_heartdart.library',
			'library',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   0.0.1
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_heartdart.edit.library.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		if(!$data->library_id)
		{
			$data->score = "10.00";
		}
		return $data;
	}

	/**
	 * Delete Selected Category
	 *
	 * @param   array  $catIDs  Array of category ids to delete
	 *
	 * @return  boolean
	 */
	/*public function deleteCategory($catIDs)
	{
		$ids = implode(',', $catIDs);
		$currentTime = time();

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base update statement.
		$query->update($db->quoteName('#__heartdart_category'))
			->set($db->quoteName('is_deleted') . ' = ' . $db->quote(1))
			->set($db->quoteName('time_stamp') . ' = ' . $db->quote($currentTime))
			->where($db->quoteName('id') . ' IN (' . $db->quote($ids) . ')');

		// Set the query and execute the update.
		$db->setQuery($query);

		try
		{
			$db->execute();

			return true;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
		return false;
	}*/
}
