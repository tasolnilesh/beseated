<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Controller
 *
 * @since  0.0.1
 */
class HeartdartControllerMessages extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'Message', $prefix = 'HeartdartModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function setPublished()
	{
		$input  = JFactory::getApplication()->input;
		$cid    = $input->get('cid', 0, 'int');
		$itemid = $input->get('Itemid', 0, 'int');

		if($cid)
		{
			$model = $this->getModel();
			$result = $model->setPublished($cid);

			//JFactory::getApplication()->enqueueMessage('Default media set successfully');

		}

		$this->setRedirect('index.php?option=com_heartdart&view=messages&Itemid='.$itemid);


	}

	public function setUnpublished()
	{
		$input  = JFactory::getApplication()->input;
		$cid    = $input->get('cid', 0, 'int');
		$itemid = $input->get('Itemid', 0, 'int');

		if($cid)
		{
			$model = $this->getModel();
			$result = $model->setUnpublished($cid);

			//JFactory::getApplication()->enqueueMessage('Default media set successfully');

		}

		$this->setRedirect('index.php?option=com_heartdart&view=messages&Itemid='.$itemid);
	}
}
