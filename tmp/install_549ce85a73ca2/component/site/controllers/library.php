<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Category Controller
 *
 * @since  0.0.1
 */
class HeartdartControllerLibrary extends JControllerForm
{
	public function save()
	{
		$model = $this->getModel();

		$input = JFactory::getApplication()->input;

		$Itemid=$input->get('Itemid',0,'int');

		$data = $input->get('jform',array(),'array');

		//$result = array_merge($data, $_FILES['jform']['name']);
		$lib_name = $data['lib_name'];

		if(!empty($lib_name))
		{
			$model->addLibrary($data);
		}
			$this->setRedirect('index.php?option=com_heartdart&view=libraries&Itemid='.$Itemid);
	}
}
