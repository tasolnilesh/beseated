<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Category Controller
 *
 * @since  0.0.1
 */
class HeartdartControllerMessage extends JControllerForm
{

	public function save()
	{
		$model = $this->getModel();

		$input  = JFactory::getApplication()->input;
		$lib_id = $input->get('library_id',0,'int');
		$Itemid = $input->get('Itemid',0,'int');
		$data   = $input->get('jform',array(),'array');

		//$result = array_merge($data, $_FILES['jform']['name']);

		$msg = $data['message'];

		if(!empty($lib_id) && !empty($msg))
		{
			$model->addMessage($data);
		}
			$this->setRedirect('index.php?option=com_heartdart&view=librarymessages&library_id='.$lib_id.'&Itemid='.$Itemid);
	}
}
