<?php
/**
 * @package     Pass.Administrator
 * @subpackage  com_pass
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
?>
<form action="<?php echo JRoute::_('index.php?option=com_heartdart&view=library&task=library.save'); ?>"
    method="post" name="adminForm" id="adminForm">
    <div class="form-horizontal">
        <fieldset>
            <div class="span12">
                <legend><?php echo JText::_('COM_HEARTDART_LIBRARY_DETAIL'); ?></legend>
                <div class="control-group">
                    <div class="controls"><?php echo $this->form->getInput('library_id'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('score'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('lib_name'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('lib_name'); ?></div>
                </div>
                 <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('description'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('description'); ?></div>
                </div>
            </div>
            <?php echo JHtml::_('form.token'); ?>
        </fieldset>
    </div>
   <!-- <input type="hidden" name="task" value="" />
    <input type="hidden" name="user_id" value="<?php // echo $this->user->id; ?>" />-->
    <div class="row" align="center">
            <input type="submit" value="Save" name="Save" />
            <input type="button" value="Cancel" name="Cancel" />
    </div>
</form>
