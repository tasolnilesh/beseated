<?php
/**
 * @package     Pass.Administrator
 * @subpackage  com_pass
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
$input = JFactory::getApplication()->input;
$lib_id=$input->get('library_id',0,'int');
?>

<script type="text/javascript">

	jQuery(document).ready(function() {
	jQuery("#image").hide();
	jQuery("#video").hide();
	jQuery("#audio").hide();
	jQuery("#youtube").hide();
	jQuery("#jform_upload").change(function(event) {
		var selectedValue = jQuery("#jform_upload").val();

		if (selectedValue == "none") {
			jQuery("#image").hide();
			jQuery("#video").hide();
			jQuery("#audio").hide();
			jQuery("#youtube").hide();
		};

		if (selectedValue == "image") {
			jQuery("#image").show();
			jQuery("#video").hide();
			jQuery("#audio").hide();
			jQuery("#youtube").hide();
		};

		if (selectedValue == "video") {
			jQuery("#image").hide();
			jQuery("#video").show();
			jQuery("#audio").hide();
			jQuery("#youtube").hide();
		};

		if (selectedValue == "audio") {
			jQuery("#image").show();
			jQuery("#video").hide();
			jQuery("#audio").show();
			jQuery("#youtube").hide();
		};

		if (selectedValue == "youtube") {
			jQuery("#image").hide();
			jQuery("#video").hide();
			jQuery("#audio").hide();
			jQuery("#youtube").show();
		};
	});
});

</script>

<form action="<?php echo JRoute::_('index.php?option=com_heartdart&view=message&task=message.save&library_id='.$lib_id); ?>"
    method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="form-horizontal" >
        <fieldset>
            <div class="span12">
                <legend><?php echo JText::_('COM_HEARTDART_MESSAGE_DETAIL'); ?></legend>
                <div class="control-group">
                    <div class="controls"><?php echo $this->form->getInput('msg_id'); ?></div>
                    <!--<div class="controls"><?php // echo $this->form->getInput('library_id'); ?></div>-->
                </div>

                 <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('author'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('author'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('message'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('message'); ?></div>
                </div>
                <div id="image" class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('image'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('image'); ?></div>
                </div>
                 <div id="video" class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('video'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('video'); ?></div>
                </div>
                 <div id="audio" class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('audio'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('audio'); ?></div>
                </div>
                <div id="youtube" class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('youtube'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('youtube'); ?></div>
                </div>
 				<div id="upload" class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('upload'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('upload'); ?></div>
                </div>
                <!--<div class="control-group">
                    <div class="control-label"><?php// echo $this->form->getLabel('library_id'); ?></div>
                    <div class="controls"><?php //echo $this->form->getInput('library_id'); ?></div>
                </div>-->
            </div>
            <?php echo JHtml::_('form.token'); ?>
        </fieldset>

        <div class="row" align="center">
            <input type="submit" value="Save" name="Save" />
            <input type="reset" value="Cancel" name="Cancel" />
        </div>
    </div>
</form>
