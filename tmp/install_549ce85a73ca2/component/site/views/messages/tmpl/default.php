<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

?>

<ul class="thumbnails">
<h1>Messages</h1>

<?php foreach ($this->items as $key => $row) : ?>

	<?php $filename = 'images/heartdart/messages/'.$row->media; ?>
	<?php $link = JRoute::_('index.php?option=com_heartdart&view=viewmessage&media_id='.$row->media_id.'&library_id=' . $row->library_id); ?>

	<?php if($key%3==0): ?>
	<li class="span4 margin-box">
	<?php else: ?>
	<li class="span4 no-margin">
	<?php endif; ?>
	<div class="thumbnail">


	<?php if(!empty($row->media) && file_exists($filename) && $row->media_type == 'image'): ?>
			<a href="<?php echo $link; ?>"><img alt="300x200" data-src="holder.js/300x200" style="width: 300px; height: 190px;" src="<?php echo JUri::base().'images/heartdart/messages/'.$row->media; ?>"></a>
	<?php elseif(!empty($row->media) && file_exists($filename) && $row->media_type == 'audio' && !empty($row->image)): ?>
			<a href="<?php echo $link; ?>"><img alt="300x200" data-src="holder.js/300x200" style="width: 300px; height: 190px;" src="<?php echo JUri::base().'images/heartdart/messages/'.$row->image; ?>"></a>
	<?php elseif(!empty($row->media) && file_exists($filename) && $row->media_type == 'video'): ?>
			<a href="<?php echo $link; ?>"><img alt="300x200" data-src="holder.js/300x200" style="width: 300px; height: 190px;" src="<?php echo JUri::base().'images/heartdart/messages/images/images.jpeg'; ?>"></a>
	<?php elseif(!empty($row->media) && $row->media_type == 'youtube'): ?>
			<a href="<?php echo $link; ?>"><img alt="300x200" data-src="holder.js/300x200" style="width: 300px; height: 190px;" src="<?php echo JUri::base().'images/heartdart/messages/images/images.jpeg'; ?>"></a>
	<?php else: ?>
			<a href="<?php echo $link; ?>"><img alt="300x200" data-src="holder.js/300x200" style="width: 300px; height: 190px;" src="<?php echo JUri::base().'components/com_heartdart/assets/images/message.jpg'; ?>"></a>
	<?php endif; ?>

		<div class="caption">
			<h3><?php echo substr($row->author, 0, 25); if(strlen($row->author)>25){ echo "...."; } ?></h3>
			<?php $link = JRoute::_('index.php?option=com_heartdart&view=librarymessages&library_id=' . $row->library_id);?>
			<? echo "<b>Library</b>: <a href=$link>$row->lib_name</a>";?>
			<p>
				<?php echo substr($row->message,0, 50); ?>
			</p>

			<?php if($this->isRoot): ?>
				<?php if($row->published): ?>
					<p class="msg-action">
						<a href="index.php?option=com_heartdart&amp;view=messages&amp;task=messages.setUnpublished&amp;cid=<?php echo $row->msg_id; ?>&amp;Itemid=<?php echo $Itemid; ?>" class="btn btn-primary">Unpublished</a>
					</p>
				<?php else: ?>
					<p class="msg-action">
						<a href="index.php?option=com_heartdart&amp;view=messages&amp;task=messages.setPublished&amp;cid=<?php echo $row->msg_id; ?>&amp;Itemid=<?php echo $Itemid; ?>" class="btn btn-primary">Published</a>
					</p>
				<?php endif; ?>
			<?php endif; ?>

		</div>
	</div>
	</li>
<?php endforeach; // End of main foreach loop ?>
</ul>

<?php echo $this->pagination->getListFooter(); ?>
