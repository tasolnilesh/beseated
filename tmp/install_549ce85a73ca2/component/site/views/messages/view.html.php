<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages View
 *
 * @since  0.0.1
 */
class HeartdartViewMessages extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;
	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		// Get data from the model
		$this->items         = $this->get('Items');
		$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');

		//$this->filterForm    = $this->get('FilterForm');
		//$this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Set the tool-bar and number of found items
		//$this->addToolBar();
		//$this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	protected function addToolBar()
	{
		$title = JText::_('COM_HEARTDART_MANAGER_MESSAGES');

		if ($this->pagination->total)
		{
			$title .= "<span style='font-size: 0.5em; vertical-align: middle;'>(" . $this->pagination->total . ")</span>";
		}

		/*JToolBarHelper::title($title, 'Events');
		JToolBarHelper::addNew('post.add');
		JToolBarHelper::editList('post.edit');
		JToolBarHelper::deleteList('', 'posts.delete');*/

		JToolbarHelper::preferences('com_heartdart');

		$vName = "messages";

		JHtmlSidebar::addEntry(JText::_('Categories'), 'index.php?option=com_heartdart&view=categories', $vName == 'categories');
		JHtmlSidebar::addEntry(JText::_('Libraries'), 'index.php?option=com_heartdart&view=libraries', $vName == 'libraries');
		JHtmlSidebar::addEntry(JText::_('Messages'), 'index.php?option=com_heartdart&view=messages', $vName == 'messages');
		JHtmlSidebar::addEntry(JText::_('Reports'), 'index.php?option=com_heartdart&view=reports', $vName == 'reports');

		JHtmlSidebar::setAction('index.php?option=com_heartdart&view=messages');
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'a.published' => JText::_('JSTATUS'),
			'a.message'     => JText::_('JGLOBAL_TITLE'),
			'a.msg_id'        => JText::_('JGRID_HEADING_ID')
		);
	}
}
