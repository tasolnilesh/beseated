<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

?>

<ul class="thumbnails">
<h1>Libraries</h1>

<?php if($this->isRoot): ?>
		<p class="msg-action" align="right">
			<a href="index.php?option=com_heartdart&view=library&Itemid=<?php echo $Itemid; ?>" class="btn btn-primary">Add Library</a>
		</p>
<?php endif; ?>

<?php foreach ($this->items as $key => $row) : ?>
	<?php $link = JRoute::_('index.php?option=com_heartdart&view=librarymessages&library_id=' . $row->library_id);?>
	<?php $link1 = JRoute::_('index.php?option=com_heartdart&view=librarymedias&library_id=' . $row->library_id); ?>

	<?php if($key%3==0): ?>
	<li class="span4 margin-box">
	<?php else: ?>
	<li class="span4 no-margin">
	<?php endif; ?>
	<div class="thumbnail">
		<?php if(!empty($row->path)): ?>
			<img alt="300x200" data-src="holder.js/300x200" style="width: 300px; height: 180px;" src="<?php echo JUri::base().$row->path; ?>">
		<?php else: ?>
			<img alt="300x200" data-src="holder.js/300x200" style="width: 300px; height: 180px;" src="<?php echo JUri::base().'components/com_heartdart/assets/images/message.jpg'; ?>">
		<?php endif; ?>
		<div class="caption">
			<h3> <a href="<?php echo $link; ?>"><?php echo substr($row->lib_name, 0, 25); if(strlen($row->lib_name)>25){ echo "...."; } ?></a></h3>
			<a href="<?php echo $link1.'&task=showmedia'; ?>">Show Media</a>
			<p>
				<?php echo "Total Message available : ".$row->msg_count; ?>
			</p>

			<?php if($this->isRoot): ?>
				<p class="msg-action">
					<a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
				</p>
			<?php endif; ?>

		</div>
	</div>
	</li>
<?php endforeach; // End of main foreach loop ?>
</ul>

<?php echo $this->pagination->getListFooter(); ?>

<?php /*
<form action="index.php?option=com_heartdart&view=categories" method="post" id="adminForm" name="adminForm">

<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="2%"><?php echo JText::_('COM_HEARTDART_LIBRARIES_NUM'); ?></th>
			<th width="4%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
			<th width="2%" style="min-width:55px" class="nowrap center">
				<?php echo JHtml::_('grid.sort', 'COM_HEARTDART_LIBRARIES_PUBLISHED', 'a.published', $listDirn, $listOrder); ?>
			</th>
			<th width="25%">
				<?php echo JHtml::_('grid.sort', 'COM_HEARTDART_LIBRARIES_NAME', 'a.lib_name', $listDirn, $listOrder);?>
			</th>

			<th width="20%">
				<?php echo JText::_('COM_HEARTDART_LIBRARIES_ADD_MEDIA_TO_LIBRATRY'); ?>
			</th>
			<th width="20%">
				<?php echo JText::_('COM_HEARTDART_LIBRARIES_ADD_MESSAGES_TO_LIBRATRY'); ?>
			</th>
			<th width="10%">
				<?php echo JText::_('COM_HEARTDART_LIBRARIES_LIBRATRY_SCORE'); ?>
			</th>
			<th width="10%">
				<?php echo JHtml::_('grid.sort', 'COM_HEARTDART_LIBRARIES_LIBRARY_ID', 'a.library_id', $listDirn, $listOrder); ?>
			</th>
		</tr>
		</thead>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) :
					$link = JRoute::_('index.php?option=com_heartdart&view=librarymedias&library_id=' . $row->library_id);
				?>
					<tr>
						<td><?php echo $this->pagination->getRowOffset($i); ?></td>
						<td>
							<?php echo JHtml::_('grid.id', $i, $row->library_id); ?>
						</td>
						<td width="2%" style="min-width:55px" class="nowrap center">
							<?php echo JHtml::_('jgrid.published', $row->published, $i, 'libraries.', true, 'cb'); ?>
						</td>
						<td>
							<a href="<?php echo $link ?>">
								<?php echo $row->lib_name; ?>
							</a>
						</td>

						<td>
							<a class="btn-add-media" href="index.php?option=com_heartdart&view=librarymedia&library_id=<?php echo $row->library_id; ?>&tmpl=component" role="button" data-target="#myModal" data-toggle="modal">Add media to library</a>
						</td>
						<td>
							<a href="index.php?option=com_heartdart&view=message&msg_id=0&library_id=<?php echo $row->library_id; ?>">Add Messages</a>
						</td>
						<td class="nowrap center">
							<?php echo $row->score; ?>
						</td>
						<td class="nowrap center">
							<?php echo $row->library_id; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	<?php echo $this->pagination->getListFooter(); ?>
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
	<?php echo JHtml::_('form.token'); ?>
	</div>
</form> */ ?>


<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add Library Media</h3>
	</div>
	<div class="modal-body">

	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary">Save changes</button>
	</div>
</div>
