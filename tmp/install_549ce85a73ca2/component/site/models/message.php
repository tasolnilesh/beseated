<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
jimport( 'joomla.application.component.helper' );
jimport('joomla.filesystem.folder');
/**
 * Heartdart Message Model
 *
 * @since  0.0.1
 */
class HeartdartModelMessage extends JModelList
{
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'Message', $prefix = 'HeartdartTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getItem()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__heartdart_message'));

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   0.0.1
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_heartdart.message',
			'message',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   0.0.1
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_heartdart.edit.Message.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}


	public function setPublished($msgID)
	{
		$tblMessage = JTable::getInstance('Message', 'HeartdartTable', array());

		$tblMessage->load($msgID);

		if(!$tblMessage->msg_id)
		{
			return false;
		}

		$tblMessage->published = 1;

		$tblMessage->store();

	}

	public function setUnpublished($msgID)
	{

		$tblMessage = JTable::getInstance('Message', 'HeartdartTable', array());

		$tblMessage->load($msgID);

		if(!$tblMessage->msg_id)
		{
			return false;
		}

		$tblMessage->published = 0;

		$tblMessage->store();
	}

	function addMessage($data)
	{

		$uploadLimit	= 8;
		$uploadLimit	= ( $uploadLimit * 1024 * 1024 );

		$imgtype    = $_FILES['jform']['type']['image'];
		$audiotype  = $_FILES['jform']['type']['audio'];
		$videotype  = $_FILES['jform']['type']['video'];

		jimport('joomla.filesystem.file');
		jimport('joomla.utilities.utility');

		$input = JFactory::getApplication()->input;
		$lib_id=$input->get('library_id',0,'int');

		$loginUser = JFactory::getUser();

		if(!empty($imgtype) && is_array($_FILES))
		{
			$imageVal = "";

			$imgsize    = $_FILES['jform']['size']['image'];
			$imgtmpname = $_FILES['jform']['tmp_name']['image'];
			$imgname    = $_FILES['jform']['name']['image'];

			if (isset($imgsize) && $imgsize > 0)
			{
				if (filesize($imgtmpname) > $uploadLimit && $uploadLimit != 0)
				{
					return false;
				}

				$imageMaxWidth	= 160;
				$filename = JApplication::getHash($imgtmpname . time());
				$hashFileName	= JString::substr($filename , 0 , 24 );

				if (!JFolder::exists(JPATH_SITE . DS . "images" . DS . "heartdart". DS))
				{
					JFolder::create(JPATH_SITE . DS . "images" . DS . "heartdart". DS);
				}

				if (!JFolder::exists(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS))
				{
					JFolder::create(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS);
				}

				if (!JFolder::exists(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS . "images" . DS))
				{
					JFolder::create(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS . "images" . DS);
				}

				$info['extension'] = pathinfo($imgname,PATHINFO_EXTENSION);
				$info['extension'] = '.'.$info['extension'];
				$storage = JPATH_SITE . "/images/heartdart/messages/images";
				$storageImage	= $storage . '/' . $hashFileName .  $info['extension'];
				$imageVal	= 'images/' . $hashFileName . $info['extension'];

				if (!JFile::upload($imgtmpname, $storageImage))
				{
					return false;
				}

				$mediaVal = $imageVal;
				$mediaType = "image";
				$mediaImage = $imageVal;
			}
		}
		else
		{
			$imageVal = '';
		}

		$videoVal = "";
		if(!empty($videotype))
		{
			$videosize    = $_FILES['jform']['size']['video'];
			$videotmpname = $_FILES['jform']['tmp_name']['video'];
			$videoname    = $_FILES['jform']['name']['video'];

			$randomname = 'ijoomeradv_'.substr(md5(microtime()),rand(0,26),5);
			$filename 	= JFile::makeSafe($videoname);
			$fileext	= strtolower(JFile::getExt($filename));
			$src		= $videotmpname;
			$filetype	= $videotype;

			if (!JFolder::exists(JPATH_SITE . DS . "images" . DS . "heartdart". DS))
			{
				JFolder::create(JPATH_SITE . DS . "images" . DS . "heartdart". DS);
			}

			if (!JFolder::exists(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS))
			{
				JFolder::create(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS);
			}

			if (!JFolder::exists(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS . "videos" . DS))
			{
				JFolder::create(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS . "videos" . DS);
			}

			$destpath	= JPATH_SITE . "/images/heartdart/messages/videos/" . $randomname.'.'.$fileext;
			$destConvertPath	= JPATH_SITE . "/images/heartdart/messages/videos";
			$videoVal= 'videos/'.$randomname.'.'.$fileext;

			if (strtolower($fileext) == '3gp' or strtolower($fileext) == 'mov' or strtolower($fileext) == 'mp4')
			{
				if(!JFile::upload($src, $destpath))
				{
					return false;
				}

			}
			else
			{
				return false;
			}

			$convertedFileName = '';
			if(strtolower($fileext) != 'mp4')
			{
				$convertedFileName = $this->helper->convertVideo($destpath, $destConvertPath,'400x300',false);
			}

			if(!empty($convertedFileName))
			{
				$mediaVal = 'videos/'.$convertedFileName;

				$videoVal = $mediaVal;
			}
			else
			{
				$mediaVal = $videoVal;
			}

			$mediaType = "video";
		}
		else
		{
			$videoVal = '';
		}


		$audioVal = "";
		if(!empty($audiotype) && is_array($_FILES))
		{
			$audiosize    = $_FILES['jform']['size']['audio'];
			$audiotmpname = $_FILES['jform']['tmp_name']['audio'];
			$audioname    = $_FILES['jform']['name']['audio'];

			if (isset($audiosize) && $audiosize > 0)
			{
				if (filesize($audiotmpname) > $uploadLimit && $uploadLimit != 0)
				{
					return false;
				}

				$imageMaxWidth	= 160;
				$filename = JApplication::getHash( $audiotmpname . time() );
				$hashFileName	= JString::substr( $filename , 0 , 24 );

				if (!JFolder::exists(JPATH_SITE . DS . "images" . DS . "heartdart". DS))
				{
					JFolder::create(JPATH_SITE . DS . "images" . DS . "heartdart". DS);
				}

				if (!JFolder::exists(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS))
				{
					JFolder::create(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS);
				}

				if (!JFolder::exists(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS . "audio" . DS))
				{
					JFolder::create(JPATH_SITE . DS . "images" . DS . "heartdart". DS . "messages" . DS . "audio" . DS);
				}

				$info['extension'] = pathinfo($audioname,PATHINFO_EXTENSION);
				$info['extension'] = '.'.$info['extension'];
				$storage = JPATH_SITE . "/images/heartdart/messages/audio";
				$storageImage	= $storage . DS . $hashFileName .  $info['extension'];
				$destConvertPath = $storage . DS . 'ijadv_'.$hashFileName.'.mp3';
				$audioVal	= 'audio/' . $hashFileName . $info['extension'];

				if (!JFile::upload($audiotmpname, $storageImage))
				{
					return false;
				}

				//$command = $this->helper->ffmpegPath().' -i '.$storageImage.' -acodec mp3 '.$destConvertPath.' | '.$this->helper->ffmpegPath().' -i '.$storageImage.' -sameq '.$destConvertPath;
				//$command = 'ffmpeg -i '.$storageImage.' -acodec mp3 '.$destConvertPath.' | ffmpeg -i '.$storageImage.' -sameq '.$destConvertPath;
				//$command = $this->helper->ffmpegPath().' -i '.$storageImage.' -acodec mp3 '.$storageImage.'|'.$this->helper->ffmpegPath().' -i '.$storageImage.' -sameq '.$storageImage;

				//$data = $this->helper->_runCommand($command);

				if(JFile::exists($destConvertPath))
				{
					//JFile::delete($storageImage);
					$audioVal	= 'audio/' . 'ijadv_'.$hashFileName.'.mp3';
				}


				$mediaVal = $audioVal;
				$mediaType = "audio";
			}
		}
		else
		{
			$audioVal = '';
		}

		if(!empty($data['youtube']))
		{
			$path=$data['youtube'];
			$path1=str_replace("watch?",'',$path);
			$newpath=str_replace("=",'/',$path1);

			$mediaVal = $newpath;
			$mediaType = "youtube";
			//$youtubeUrl = $mediaVal;
		}

		$tblMessage = JTable::getInstance('Message', 'HeartdartTable', array());
		$postData = array();

		if(!empty($data['author']))
		{
			$postData['author'] = $data['author'];
		}
		else
		{
			$postData['author']     = $loginUser->name;
		}

		$postData['message']    = $data['message'];
		$postData['msg_type']   = 'msg';
		$postData['url']        = '';
		$postData['image']      = $imageVal;
		$postData['audio']      = $audioVal;
		$postData['video']      = $videoVal;
		$postData['youtube']    = $mediaVal;
		$postData['library_id'] = $lib_id;
		$postData['to_users']   = '';
		$postData['media_id']   = '';
		$postData['userid']     = $loginUser->id;
		$postData['created']	= date("Y-m-d h:i:s");
		$postData['time_stamp'] = time();
		$postData['published']  = 1;
		$postData['is_deleted'] = 0;

		$tblMessage->load(0);

		$tblMessage->bind($postData);
		$tblMessage->store();

		$tblMessageMedia = JTable::getInstance('MessageMedia', 'HeartdartTable', array());
		$data = array();

		$data['msg_id']     = $tblMessage->msg_id;
		$data['media']      = $mediaVal;
		$data['media_type'] = $mediaType;
		$data['image']      = ($mediaImage == $mediaVal)?'':$mediaImage;
		$data['time_stamp'] = time();
		$data['created']    = date("Y-m-d h:i:s");

		$tblMessageMedia->bind($data);
		$tblMessageMedia->store();

		$postData['media_id'] = $tblMessageMedia->media_id;
		$tblMessage->bind($postData);
		$tblMessage->store();
	}

}

