<?php
/**
* @version		$Id: mod_appzoom.php 10000 2014-01-22 03:35:53Z schro $
* @package		Joomla 3.2.x
* @copyright	Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* */
// no direct access
defined('_JEXEC') or die('Restricted access');

abstract class modBctedTrendingVenues
{
	public static function getVenueByRating()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue'))
			->where($db->quoteName('venue_active') . ' = ' . $db->quote('1'))
			->order($db->quoteName('venue_rating') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query,0,3);

		try
		{
			$result = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		return $result;
	}
}
