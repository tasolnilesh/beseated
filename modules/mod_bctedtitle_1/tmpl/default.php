<?php
/**
	 * @package   AppImage Slider
	 * @version   1.0
	 * @author    Erwin Schro (http://www.joomla-labs.com)
	 * @author	  Based on BxSlider jQuery plugin script
	 * @copyright Copyright (C) 2013 J!Labs. All rights reserved.
	 * @license   GNU/GPL http://www.gnu.org/copyleft/gpl.html
	 *
	 * @copyright Joomla is Copyright (C) 2005-2013 Open Source Matters. All rights reserved.
	 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
	 */


defined('_JEXEC') or die('Restricted access');

$doc 	= JFactory::getDocument();


$modbase 	= JURI::base(true) .'/modules/mod_bctedtitle'; /* juri::base(true) will not added full path and slash at the path end */
// add style
//$doc->addStyleSheet($modbase . '/assets/css/style.css');
// add javascript
/*$doc->addScript($modbase . '/assets/js/libs/prototype.js');
$doc->addScript($modbase . '/assets/js/libs/scriptaculous.js');
$doc->addScript($modbase . '/assets/js/libs/sizzle.js');
$doc->addScript($modbase . '/assets/js/loupe.js');*/

?>
<div class="user-info-container">
	<h2><?php echo $firstLine; ?></h2>
	<div class="last-visited-date"><?php echo $secondLine; ?></div>
	<div class="last-visited-date"><?php echo $thirdLine; ?></div>
</div>
<?php if($userType == 'Registered' || $userType == 'Guest'): ?>
<div class="moduletable user-dispay-menu">
	<ul class="nav menu">
		<?php if($view == 'clubinformation'): ?>
			<li class="item-128 current active">
		<?php else: ?>
			<li class="item-128">
		<?php endif; ?>
			<i class="info-icn"></i>
            <!-- <a href="/MobileProject/bc-tedlive/index.php?option=com_bcted&amp;view=clubinformation&amp;club_id=<?php //echo $clubID; ?>&amp;Itemid=128">Information</a> -->
            <a href="<?php echo JRoute::_('index.php?option=com_bcted&view=clubinformation&club_id=' . $clubID . '&Itemid=128'); ?>">Information</a>
		</li>

		<?php if($view == 'clubguestlist'): ?>
			<li class="item-129 current active">
		<?php else: ?>
			<li class="item-129">
		<?php endif; ?>
		   	<i class="guest-icn"></i>
			<!-- <a href="/MobileProject/bc-tedlive/index.php?option=com_bcted&amp;view=clubguestlist&amp;club_id=<?php //echo $clubID; ?>&amp;Itemid=129">Guestlist</a> -->
			<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=clubguestlist&club_id=' . $clubID . '&Itemid=129'); ?>">Guestlist</a>
		</li>

		<?php if($view == 'clubtables'): ?>
			<li class="item-130 current active">
		<?php else: ?>
			<li class="item-130">
		<?php endif; ?>
       		<i class="tabl-icn"></i>
			<!-- <a href="/MobileProject/bc-tedlive/index.php?option=com_bcted&amp;view=clubtables&amp;club_id=<?php //echo $clubID; ?>&amp;Itemid=130">Tables</a> -->
			<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=clubtables&club_id=' . $clubID . '&Itemid=130'); ?>">Tables</a>
		</li>

		<?php if($view == 'clubfriendsattending'): ?>
			<li class="item-131 current active">
		<?php else: ?>
			<li class="item-131 ">
		<?php endif; ?>
        	<i class="frnd-icn"></i>
			<!-- <a href="/MobileProject/bc-tedlive/index.php?option=com_bcted&amp;view=clubfriendsattending&amp;club_id=<?php //echo $clubID; ?>&amp;Itemid=131">Friends Attending</a> -->
			<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=clubfriendsattending&club_id=' . $clubID . '&Itemid=131'); ?>">Friends Attending</a>
		</li>

		<?php if($view == 'clubratings'): ?>
			<li class="item-132 current active">
		<?php else: ?>
			<li class="item-132 ">
		<?php endif; ?>
        	<i class="rate-icn"></i>
			<!-- <a href="/MobileProject/bc-tedlive/index.php?option=com_bcted&amp;view=clubratings&amp;club_id=<?php //echo $clubID; ?>&amp;Itemid=132">User Ratings</a> -->
			<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=clubratings&club_id=' . $clubID . '&Itemid=132'); ?>">User Ratings</a>
		</li>
		<li class="item-132 book-now-btn">
			<a href="#">Book Now</a>
		</li>
	</ul>
</div>
<?php endif; ?>



