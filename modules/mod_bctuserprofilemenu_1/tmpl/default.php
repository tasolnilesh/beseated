<?php
/**
	 * @package   AppImage Slider
	 * @version   1.0
	 * @author    Erwin Schro (http://www.joomla-labs.com)
	 * @author	  Based on BxSlider jQuery plugin script
	 * @copyright Copyright (C) 2013 J!Labs. All rights reserved.
	 * @license   GNU/GPL http://www.gnu.org/copyleft/gpl.html
	 *
	 * @copyright Joomla is Copyright (C) 2005-2013 Open Source Matters. All rights reserved.
	 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
	 */


defined('_JEXEC') or die('Restricted access');

$doc 	= JFactory::getDocument();


$modbase 	= JURI::base(true) .'/modules/mod_bctedtitle'; /* juri::base(true) will not added full path and slash at the path end */
// add style
//$doc->addStyleSheet($modbase . '/assets/css/style.css');
// add javascript
/*$doc->addScript($modbase . '/assets/js/libs/prototype.js');
$doc->addScript($modbase . '/assets/js/libs/scriptaculous.js');
$doc->addScript($modbase . '/assets/js/libs/sizzle.js');
$doc->addScript($modbase . '/assets/js/loupe.js');*/

$Itemid = 147;

$app = JFactory::getApplication();
$view = $app->input->get('view','','string');

?>
<?php if($userType == 'Registered'): ?>
<div class="moduletable user-dispay-menu">
	<ul class="nav menu remove-menu-space">
		<?php if($view == 'userbookings'): ?>
			<li class="current active">
		<?php else: ?>
			<li class="">
		<?php endif; ?>
            <a href="<?php echo JRoute::_('index.php?option=com_bcted&view=userbookings&Itemid='.$Itemid); ?>">Bookings</a>
		</li>

		<?php if($view == 'userpackageinvitations'): ?>
			<li class="current active">
		<?php else: ?>
			<li class="">
		<?php endif; ?>
            <a href="<?php echo JRoute::_('index.php?option=com_bcted&view=userpackageinvitations&Itemid='.$Itemid); ?>">Package Invitations</a>
		</li>

		<?php if($view == 'favourites'): ?>
			<li class="current active">
		<?php else: ?>
			<li class="">
		<?php endif; ?>
			<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=favourites&Itemid='.$Itemid); ?>">Favorites</a>
		</li>

		<?php if($view == 'messages'): ?>
			<li class="current active">
		<?php else: ?>
			<li class="">
		<?php endif; ?>
			<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=messages&Itemid='.$Itemid); ?>">Messages</a>
		</li>

		<?php if($view == 'loyalty'): ?>
			<li class="current active">
		<?php else: ?>
			<li class="">
		<?php endif; ?>
			<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=loyalty&Itemid='.$Itemid); ?>">Loyalty</a>
		</li>

		<?php if($view == 'userprofile'): ?>
			<li class="current active">
		<?php else: ?>
			<li class="">
		<?php endif; ?>
			<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=userprofile&Itemid='.$Itemid); ?>">Profile</a>
		</li>



	</ul>
</div>
<?php endif; ?>



