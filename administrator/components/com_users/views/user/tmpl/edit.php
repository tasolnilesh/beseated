<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

// Get the form fieldsets.
$fieldsets = $this->form->getFieldsets();
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'user.cancel' || document.formvalidator.isValid(document.id('user-form')))
		{
			Joomla.submitform(task, document.getElementById('user-form'));
		}
	}

	Joomla.twoFactorMethodChange = function(e)
	{
		var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

		jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function(i, el) {
			if (el.id != selectedPane)
			{
				jQuery('#' + el.id).hide(0);
			}
			else
			{
				jQuery('#' + el.id).show(0);
			}
		});
	}


</script>

<script type="text/javascript">
jQuery(function(){
    jQuery("#groups input:radio").click(function(){
       	var selected = jQuery('input[id^=1group_]:checked').val();

		if(selected == 11) // Club
		{
			jQuery('#jform_name-lbl').html(' Club Name<span class="star"> *</span>');

			jQuery('#group_last_name').hide();
			jQuery('#jform_last_name').val();

			jQuery('#group_phoneno').hide();
			jQuery('#jform_phoneno').val();

			jQuery('#jform_last_name').attr("aria-invalid","false");
			jQuery('#jform_last_name').attr("aria-required","false");
			jQuery('#jform_last_name').removeAttr('required');
			jQuery('#jform_last_name').removeAttr('class');
			//jQuery('jform_last_name').addClass(' required invalid');

			jQuery('#jform_phoneno').attr("aria-invalid","false");
			jQuery('#jform_phoneno').attr("aria-required","false");
			jQuery('#jform_phoneno').removeAttr('required');
			jQuery('#jform_phoneno').removeAttr('class');
			//jQuery('jform_phoneno').addClass(' required invalid');
		}

		if(selected == 10) // Company
		{
			jQuery('#jform_name-lbl').html(' Company Name<span class="star"> *</span>');
			jQuery('#group_last_name').hide();
			jQuery('#jform_last_name').val();

			jQuery('#group_phoneno').hide();
			jQuery('#jform_phoneno').val();

			jQuery('#jform_last_name').attr("aria-invalid","false");
			jQuery('#jform_last_name').attr("aria-required","false");
			jQuery('#jform_last_name').removeAttr('required');
			jQuery('#jform_last_name').removeAttr('class');
			//jQuery('jform_last_name').addClass(' required invalid');

			jQuery('#jform_phoneno').attr("aria-invalid","false");
			jQuery('#jform_phoneno').attr("aria-required","false");
			jQuery('#jform_phoneno').removeAttr('required');
			jQuery('#jform_phoneno').removeAttr('class');
			//jQuery('jform_phoneno').addClass(' required invalid');
		}

		if(selected == 14) // Beseated Guest
		{
			jQuery('#jform_name-lbl').html(' First Name<span class="star"> *</span>');
			jQuery('#group_last_name').show();
			jQuery('#group_phoneno').show();

			jQuery('#jform_last_name').attr("aria-invalid","true");
			jQuery('#jform_last_name').attr("aria-required","true");
			jQuery('#jform_last_name').attr("required","true");
			jQuery('#jform_last_name').removeAttr('class');
			jQuery('jform_last_name').addClass(' required invalid');

			jQuery('#jform_phoneno').attr("aria-invalid","true");
			jQuery('#jform_phoneno').attr("aria-required","true");
			jQuery('#jform_phoneno').attr("required","true");
			jQuery('#jform_phoneno').removeAttr('class');
			jQuery('jform_phoneno').addClass(' required invalid');
		}
    });
});
</script>

<script type="text/javascript">
jQuery( document ).ready(function() {
    jQuery('#jform_last_name').attr("aria-invalid","true");
	jQuery('#jform_last_name').attr("aria-required","true");
	jQuery('#jform_last_name').attr("required","true");
	jQuery('#jform_last_name').removeAttr('class');
	jQuery('jform_last_name').addClass(' required invalid');

	jQuery('#jform_phoneno').attr("aria-invalid","true");
	jQuery('#jform_phoneno').attr("aria-required","true");
	jQuery('#jform_phoneno').attr("required","true");
	jQuery('#jform_phoneno').removeAttr('class');
	jQuery('jform_phoneno').addClass(' required invalid');

	var selected = jQuery('input[id^=1group_]:checked').val();


	if(selected == 11) // Club
	{
		jQuery('#jform_name-lbl').html(' Club Name<span class="star"> *</span>');

		jQuery('#group_last_name').hide();
		jQuery('#jform_last_name').val();

		jQuery('#group_phoneno').hide();
		jQuery('#jform_phoneno').val();

		jQuery('#jform_last_name').attr("aria-invalid","false");
		jQuery('#jform_last_name').attr("aria-required","false");
		jQuery('#jform_last_name').removeAttr('required');
		jQuery('#jform_last_name').removeAttr('class');
		//jQuery('jform_last_name').addClass(' required invalid');

		jQuery('#jform_phoneno').attr("aria-invalid","false");
		jQuery('#jform_phoneno').attr("aria-required","false");
		jQuery('#jform_phoneno').removeAttr('required');
		jQuery('#jform_phoneno').removeAttr('class');
		//jQuery('jform_phoneno').addClass(' required invalid');
	}

	if(selected == 10) // Company
	{
		jQuery('#jform_name-lbl').html(' Company Name<span class="star"> *</span>');
		jQuery('#group_last_name').hide();
		jQuery('#jform_last_name').val();

		jQuery('#group_phoneno').hide();
		jQuery('#jform_phoneno').val();

		jQuery('#jform_last_name').attr("aria-invalid","false");
		jQuery('#jform_last_name').attr("aria-required","false");
		jQuery('#jform_last_name').removeAttr('required');
		jQuery('#jform_last_name').removeAttr('class');
		//jQuery('jform_last_name').addClass(' required invalid');

		jQuery('#jform_phoneno').attr("aria-invalid","false");
		jQuery('#jform_phoneno').attr("aria-required","false");
		jQuery('#jform_phoneno').removeAttr('required');
		jQuery('#jform_phoneno').removeAttr('class');
		//jQuery('jform_phoneno').addClass(' required invalid');
	}

	if(selected == 14) // Beseated Guest
	{
		jQuery('#jform_name-lbl').html(' First Name<span class="star"> *</span>');
		jQuery('#group_last_name').show();
		jQuery('#group_phoneno').show();

		jQuery('#jform_last_name').attr("aria-invalid","true");
		jQuery('#jform_last_name').attr("aria-required","true");
		jQuery('#jform_last_name').attr("required","true");
		jQuery('#jform_last_name').removeAttr('class');
		jQuery('jform_last_name').addClass(' required invalid');

		jQuery('#jform_phoneno').attr("aria-invalid","true");
		jQuery('#jform_phoneno').attr("aria-required","true");
		jQuery('#jform_phoneno').attr("required","true");
		jQuery('#jform_phoneno').removeAttr('class');
		jQuery('jform_phoneno').addClass(' required invalid');
	}
});
</script>

<form action="<?php echo JRoute::_('index.php?option=com_users&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="user-form" class="form-validate form-horizontal" enctype="multipart/form-data">

	<?php echo JLayoutHelper::render('joomla.edit.item_title', $this); ?>

	<fieldset>
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'groups')); ?>
			<?php if ($this->grouplist) : ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'groups', JText::_('COM_USERS_ASSIGNED_GROUPS', true)); ?>
					<?php echo $this->loadTemplate('groups'); ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php endif; ?>
			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_USERS_USER_ACCOUNT_DETAILS', true)); ?>
				<?php foreach ($this->form->getFieldset('user_details') as $field) : ?>
					<?php
					//echo $field->name;

					//exit;
					?>
					<?php if($field->name == 'jform[last_name]'): ?>
						<div class="control-group" id="group_last_name">
					<?php elseif($field->name == 'jform[phoneno]'): ?>
						<div class="control-group" id="group_phoneno">
					<?php else: ?>
						<div class="control-group">
					<?php endif; ?>

						<div class="control-label">
							<?php echo $field->label; ?>
						</div>
						<div class="controls">
							<?php echo $field->input; ?>
						</div>
					</div>
				<?php endforeach; ?>
			<?php echo JHtml::_('bootstrap.endTab'); ?>



			<?php
			foreach ($fieldsets as $fieldset) :
				if ($fieldset->name == 'user_details') :
					continue;
				endif;
			?>
			<?php echo JHtml::_('bootstrap.addTab', 'myTab', $fieldset->name, JText::_($fieldset->label, true)); ?>
				<?php foreach ($this->form->getFieldset($fieldset->name) as $field) : ?>
					<?php if ($field->hidden) : ?>
						<div class="control-group">
							<div class="controls">
								<?php echo $field->input; ?>
							</div>
						</div>
					<?php else: ?>
						<div class="control-group">
							<div class="control-label">
								<?php echo $field->label; ?>
							</div>
							<div class="controls">
								<?php echo $field->input; ?>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php endforeach; ?>

		<?php if (!empty($this->tfaform) && $this->item->id): ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'twofactorauth', JText::_('COM_USERS_USER_TWO_FACTOR_AUTH', true)); ?>
		<div class="control-group">
			<div class="control-label">
				<label id="jform_twofactor_method-lbl" for="jform_twofactor_method" class="hasTooltip"
					   title="<strong><?php echo JText::_('COM_USERS_USER_FIELD_TWOFACTOR_LABEL') ?></strong><br/><?php echo JText::_('COM_USERS_USER_FIELD_TWOFACTOR_DESC') ?>">
					<?php echo JText::_('COM_USERS_USER_FIELD_TWOFACTOR_LABEL'); ?>
				</label>
			</div>
			<div class="controls">
				<?php echo JHtml::_('select.genericlist', Usershelper::getTwoFactorMethods(), 'jform[twofactor][method]', array('onchange' => 'Joomla.twoFactorMethodChange()'), 'value', 'text', $this->otpConfig->method, 'jform_twofactor_method', false) ?>
			</div>
		</div>
		<div id="com_users_twofactor_forms_container">
			<?php foreach($this->tfaform as $form): ?>
			<?php $style = $form['method'] == $this->otpConfig->method ? 'display: block' : 'display: none'; ?>
			<div id="com_users_twofactor_<?php echo $form['method'] ?>" style="<?php echo $style; ?>">
				<?php echo $form['form'] ?>
			</div>
			<?php endforeach; ?>
		</div>

		<fieldset>
			<legend>
				<?php echo JText::_('COM_USERS_USER_OTEPS') ?>
			</legend>
			<div class="alert alert-info">
				<?php echo JText::_('COM_USERS_USER_OTEPS_DESC') ?>
			</div>
			<?php if (empty($this->otpConfig->otep)): ?>
			<div class="alert alert-warning">
				<?php echo JText::_('COM_USERS_USER_OTEPS_WAIT_DESC') ?>
			</div>
			<?php else: ?>
			<?php foreach ($this->otpConfig->otep as $otep): ?>
			<span class="span3">
				<?php echo substr($otep, 0, 4) ?>-<?php echo substr($otep, 4, 4) ?>-<?php echo substr($otep, 8, 4) ?>-<?php echo substr($otep, 12, 4) ?>
			</span>
			<?php endforeach; ?>
			<div class="clearfix"></div>
			<?php endif; ?>
		</fieldset>

		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
	</fieldset>

	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>

