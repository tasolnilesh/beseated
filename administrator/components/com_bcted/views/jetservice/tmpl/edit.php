<?php
/**
 * @package     Pass.Administrator
 * @subpackage  com_pass
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
?>
<form action="<?php echo JRoute::_('index.php?option=com_bcted&view=premiumtable&layout=edit&company_id=' . (int) $this->item->company_id); ?>"
    method="post" name="adminForm" id="adminForm">
    <div class="form-horizontal">
        <fieldset>
            <div class="span12">
                <legend><?php echo JText::_('COM_BCTED_JET_SERVICE_DETAIL'); ?></legend>
                <div class="control-group">
                    <div class="controls"><?php echo $this->form->getInput('company_id'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('company_type'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('company_created'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('userid'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('company_modified'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('company_active'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('time_stamp'); ?></div>

                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('company_name'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('company_name'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('email_address'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('email_address'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('company_about'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('company_about'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('company_image'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('company_image'); ?></div>
                </div>

            </div>
            <?php echo JHtml::_('form.token'); ?>
        </fieldset>
    </div>
    <input type="hidden" name="task" value="" />

</form>
