<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Companies View
 *
 * @since  0.0.1
 */
class BctedViewClubbookings extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;
	/**
	 * Display the Companies view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		// Get data from the model
		$this->items         = $this->get('Items');
		$this->pagination    = $this->get('Pagination');

		/*echo "<pre>";
		print_r($this->pagination);
		echo "</pre>";*/

		$this->state         = $this->get('State');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Set the tool-bar and number of found items
		$this->addToolBar();
		$this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	protected function addToolBar()
	{
		$title = JText::_('COM_BCTED_MANAGE_VENUE_BOOKINGS');

		if ($this->pagination->total)
		{
			$title .= "";
		}

		JToolBarHelper::title($title, 'Venue Bookings');

		//JToolBarHelper::editList('company.edit');
		//JToolbarHelper::trash('companies.trash');
		//JToolBarHelper::deleteList('', 'Backevents.delete');
		//JToolbarHelper::custom('companies.featured', 'featured.png', 'featured_f2.png', 'JFEATURED', true);

		JToolbarHelper::preferences('com_bcted');

		$vName = "clubbookings";
		JHtmlSidebar::addEntry(JText::_('Beseated Guests'), 'index.php?option=com_bcted&view=guests', $vName == 'guests');
		JHtmlSidebar::addEntry(JText::_('Companies'), 'index.php?option=com_bcted&view=companies', $vName == 'companies');
		JHtmlSidebar::addEntry(JText::_('Service Bookings'), 'index.php?option=com_bcted&view=bookings', $vName == 'bookings');
		JHtmlSidebar::addEntry(JText::_('Messages'), 'index.php?option=com_bcted&view=messages', $vName == 'messages');
		JHtmlSidebar::addEntry(JText::_('Packages'), 'index.php?option=com_bcted&view=packages', $vName == 'packages');
		JHtmlSidebar::addEntry(JText::_('Package Bookings'), 'index.php?option=com_bcted&view=packagebookings', $vName == 'packagebookings');
		JHtmlSidebar::addEntry(JText::_('Premium Tables'), 'index.php?option=com_bcted&view=premiumtables', $vName == 'premiumtables');
		JHtmlSidebar::addEntry(JText::_('Private Jet Service'), 'index.php?option=com_bcted&view=jetservices', $vName == 'jetservices');
		JHtmlSidebar::addEntry(JText::_('Venues'), 'index.php?option=com_bcted&view=venues', $vName == 'venues');
		JHtmlSidebar::addEntry(JText::_('Venue Bookings'), 'index.php?option=com_bcted&view=clubbookings', $vName == 'clubbookings');


		JHtmlSidebar::setAction('index.php?option=com_bcted&view=bookings');
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'a.published' => JText::_('JSTATUS'),
			'a.message'     => JText::_('JGLOBAL_TITLE'),
			'a.company_id'        => JText::_('JGRID_HEADING_ID')
		);
	}
}
