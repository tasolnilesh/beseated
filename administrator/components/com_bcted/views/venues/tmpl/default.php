<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('bootstrap.framework');
JHtml::_('formbehavior.chosen', 'select');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

$user		= JFactory::getUser();
$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
?>
<form action="<?php echo JRoute::_('index.php?option=com_bcted&view=venues'); ?>" method="post" id="adminForm" name="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif; ?>

<?php
	// Search tools bar
	echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
?>

<script type="text/javascript">

	function setValForModel(elementID)
	{
		jQuery('#elementID').val(elementID);
	}

	function setCommission()
	{
		var elementID = jQuery('#elementID').val();
		var newRate = jQuery('#commission_rate').val();

		//alert(elementID + " || " + newRate);

		// Ajax call to send message....
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=venue.chnageCommissionRate',
			type: 'GET',
			data: 'element_id='+elementID+'&new_rate='+newRate,

			success: function(response){

				if(response == "200")
				{
					//jQuery('#booking_'+bookingID).remove();
					 location.reload();
				}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});
	}

	function changeLicenceType(element,venueID)
	{
		//alert(element.value + venueID);

		// Ajax call to send message....
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=venue.changeLicenceType',
			type: 'GET',
			data: 'licence_type='+element.value+'&venue_id='+venueID,

			success: function(response){

				if(response == "200")
				{
					//jQuery('#booking_'+bookingID).remove();
				}

				//jQuery('#myModal').hide();
				jQuery('#myModal').modal('hide');
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}

	jQuery(function () {
	    jQuery("#saveButton").on('click', function() {
	        jQuery('#myModal').modal('hide');
	    });
	});
</script>
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th width="2%"><?php echo JText::_('COM_BCTED_NUM'); ?></th>
				<th width="2%">
					<?php echo JHtml::_('grid.checkall'); ?>
				</th>
				<th width="2%" style="min-width:55px" class="nowrap center">
					<?php echo JHtml::_('grid.sort', 'JSTATUS', 'a.published', $listDirn, $listOrder); ?>
				</th>

				<th width="15%">
					<?php echo JHtml::_('grid.sort', 'COM_BCTED_VENUE_HEADING_VENUE_NAME', 'a.venue_name', $listDirn, $listOrder);?>
				</th>
				<th width="8%">
					<?php echo JHtml::_('grid.sort', 'COM_BCTED_VENUE_HEADING_VENUE_STARS', 'a.venue_signs', $listDirn, $listOrder);?>
				</th>
				<th width="5%">
					<?php echo JText::_('COM_BCTED_VENUE_HEADING_VENUE_LICENCE_TYPE');?>
				</th>
				<th width="10%"><?php echo JHtml::_('grid.sort', 'COM_BCTED_VENUE_HEADING_VENUE_ADDRESS', 'a.venue_address', $listDirn, $listOrder);?></th>

				<?php /*<th width="1%"><?php echo JHtml::_('grid.sort', 'COM_HEARTDART_MESSAGES_HEADING_YES_VOTE', 'pi.yes_vote', $listDirn, $listOrder);?></th>
				<th width="1%"><?php echo JHtml::_('grid.sort', 'COM_HEARTDART_MESSAGES_HEADING_NO_VOTE', 'pi.no_vote', $listDirn, $listOrder);?></th>*/?>

				<!--<th width="2%"><?php //echo JText::_('COM_BCTED_VENUE_HEADING_VENUE_CREATED'); ?></th>-->
				<th width="2%"><?php echo JHtml::_('grid.sort', 'COM_BCTED_VENUE_HEADING_COMMISSION_RATE', 'a.commission_rate', $listDirn, $listOrder); ?></th>
				<th width="2%"><?php echo JHtml::_('grid.sort', 'COM_BCTED_VENUE_HEADING_VENUE_ID', 'a.venue_id', $listDirn, $listOrder);?></th>
			</tr>
		</thead>

		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) :
					$link = JRoute::_('index.php?option=com_bcted&view=venue&id=' . $row->venue_id); ?>
					<tr>
						<td><?php echo $this->pagination->getRowOffset($i); ?></td>
						<td><?php echo JHtml::_('grid.id', $i, $row->venue_id); ?></td>
						<td class="center">
							<div class="btn-group">
								<?php echo JHtml::_('jgrid.published', $row->venue_active, $i, 'venue.', true, 'cb'); ?>
								<?php echo JHtml::_('main.featured', $row->r_bcted, $i, 1); ?>
							</div>
						</td>

						<td> <a><?php echo $row->venue_name; ?></a></td>
						<td>
							<?php $lastIndex = 0; ?>
							<?php for($i=1;$i<=$row->venue_signs;$i++): ?>
								<?php $lastIndex = $i; ?>
								<a href="index.php?option=com_bcted&task=venues.setsings&type=0&venue_id=<?php echo $row->venue_id; ?>">
									<img src="../images/bcted/default/dollar_on.png">
								</a>
							<?php endfor; ?>

							<?php for($i=$lastIndex+1;$i<=3;$i++): ?>
								<a href="index.php?option=com_bcted&task=venues.setsings&type=1&venue_id=<?php echo $row->venue_id; ?>">
									<img src="../images/bcted/default/dollar_off.png">
								</a>
							<?php endfor; ?>
							<?php //echo $row->venue_signs; ?>

						</td>
						<td>
							<?php //echo $row->licence_type; ?>
							<select name="licence_type" onchange="changeLicenceType(this,'<?php echo $row->venue_id; ?>');">
								<?php if($row->licence_type == 'basic') : ?>
									<option value="basic" selected="selected">Basic</option>
								<?php else: ?>
									<option value="basic" >Basic</option>
								<?php endif; ?>

								<?php if ($row->licence_type == 'plus'): ?>
									<option value="plus" selected="selected">Plus</option>
								<?php else: ?>
									<option value="plus">Plus</option>
								<?php endif; ?>

								<?php if ($row->licence_type == 'premium'): ?>
									<option value="premium" selected="selected">Premium</option>
								<?php else: ?>
									<option value="premium">Premium</option>
								<?php endif; ?>
							</select>
						</td>
						<td><?php echo $row->venue_address; ?></td>
						<?php /*<td><?php echo $row->yes_vote; ?></td>
						<td><?php echo $row->no_vote; ?></td> */ ?>
						<td><a href="#myModal" role="button" class="btn" data-toggle="modal" onclick="setValForModel('<?php echo $row->venue_id; ?>')"><?php echo $row->commission_rate; ?></a></td>
						<td><?php echo $row->venue_id; ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="9"><?php echo $this->pagination->getListFooter(); ?></td>
			</tr>
		</tfoot>
	</table>
	<?php //echo $this->pagination->getListFooter(); ?>
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery("#commission_rate").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });
</script>
<!-- Button to trigger modal -->
<!--<a href="#myModal" role="button" class="btn" data-toggle="modal">Launch demo modal</a>-->

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Set Commission Rate</h3>
	</div>
	<div class="modal-body">
		<!-- <p>Set commission Rate </p>-->
		<input type="text" name="commission_rate_val" id="commission_rate">
		<input type="hidden" name="elementID" id="elementID" value="0">

	</div>
	<div class="modal-footer">
		<button class="btn btn-primary" id="saveButton" onclick="setCommission()">Save changes</button>
	</div>
</div>