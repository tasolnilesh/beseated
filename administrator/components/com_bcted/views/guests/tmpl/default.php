<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('bootstrap.framework');
JHtml::_('formbehavior.chosen', 'select');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

$user		= JFactory::getUser();
$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
?>
<script type="text/javascript">
	function setValForModel(userName,userID)
	{
		//jQuery('#user_id').val(userID);
		jQuery('#myModalLabel').html('Loyalty Points Of '+userName);
		jQuery('#loyalty_list').html('Some text and markup');
		jQuery('#user_id').val(userID);

		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=guests.getUserLoyaltyList',
			type: 'GET',
			data: 'user_id='+userID,

			success: function(response){

				jQuery('#loyalty_list').html(response);
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});
	}

	function changePoint(type)
	{
		var user_id = jQuery('#user_id').val();
		var admin_point = jQuery('#admin_point').val();
		var point_type = type;

		// Ajax call to send message....
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=guests.changePoint',
			type: 'GET',
			data: 'user_id='+user_id+'&admin_point='+admin_point+'&point_type='+point_type,

			success: function(response){

				if(response == "200")
				{
					//jQuery('#booking_'+bookingID).remove();
					 location.reload();
				}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});
	}

</script>
<form action="<?php echo JRoute::_('index.php?option=com_bcted&view=guests'); ?>" method="post" id="adminForm" name="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif; ?>

<?php
	// Search tools bar
	echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
?>
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th width="2%"><?php echo JText::_('COM_BCTED_NUM'); ?></th>
				<th width="2%"><?php echo JText::_('COM_BCTED_COMPANY_HEADING_BCTEDGUEST_FIRST_NAME'); ?></th>
				<th width="2%"><?php echo JText::_('COM_BCTED_COMPANY_HEADING_BCTEDGUEST_LAST_NAME'); ?></th>
				<th width="2%"><?php echo JText::_('COM_BCTED_COMPANY_HEADING_BCTEDGUEST_EMAIL'); ?></th>
				<th width="2%"><?php echo JText::_('COM_BCTED_COMPANY_HEADING_BCTEDGUEST_CITY'); ?></th>
				<th width="2%"><?php echo JText::_('COM_BCTED_COMPANY_HEADING_BCTEDGUEST_PHONE_NUMBER'); ?></th>
				<th width="2%"><?php echo JText::_('COM_BCTED_COMPANY_HEADING_BCTEDGUEST_LAST_VISITED'); ?></th>
				<th width="2%"><?php echo JText::_('COM_BCTED_COMPANY_HEADING_BCTEDGUEST_CREATED'); ?></th>
				<th width="2%"><?php echo JText::_('COM_BCTED_COMPANY_HEADING_LOYALTY_POINTS'); ?></th>

			</tr>
		</thead>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) :
					$link = JRoute::_('index.php?option=com_users&task=user.edit&id=' . $row->userid); ?>
					<tr>
						<td><?php echo $this->pagination->getRowOffset($i); ?></td>
						<td><?php echo $row->name; ?></td>
						<td><?php echo $row->last_name; ?></td>
						<td><a href="<?php echo $link; ?>"><?php echo $row->email; ?></a></td>
						<td><?php echo $row->city; ?></td>
						<td><?php echo $row->phoneno; ?></td>
						<td><?php echo date('d-m-Y H:i',strtotime($row->lastvisitDate)); ?></td>
						<td><?php echo date('d-m-Y H:i',strtotime($row->registerDate)); ?></td>
						<td>
							<a href="#myModal" onclick="setValForModel('<?php echo $row->name; ?>','<?php echo $row->userid; ?>');" role="button" class="btn" data-toggle="modal">
								<?php echo $this->model->get_user_sum_of_loyalty_point($row->userid); ?>
							</a>

						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="9"><?php echo $this->pagination->getListFooter(); ?></td>
			</tr>
		</tfoot>
	</table>
	<?php //echo $this->pagination->getListFooter(); ?>
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery("#admin_point").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });
</script>

<style type="text/css">
.acnt-histry-tbl table thead {
	padding-bottom: 30px;
	border-bottom: 2px solid #000;
	height: 50px;
	vertical-align: top;
}
.acnt-histry-tbl table th {
	text-align: left;
	font-size: 18px;
	vertical-align: top;
	padding: 5px 20px;
}
.acnt-histry-tbl table td {
	text-align: left;
	font-size: 18px;
	padding: 20px;
	width: 35%;
}
</style>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"></h3>
	</div>
	<div class="modal-body">
		<!-- <p>Set commission Rate </p>-->
		<div class="span12 acnt-histry-tbl loyalty-tbl" id="loyalty_list">
		</div>
		<input type="hidden" name="user_id" id="user_id">


	</div>
	<div class="modal-footer">
		<span>Points : </span><input type="text" value="" name="admin_point" id="admin_point">

		<button class="btn btn-primary" id="saveButton" onclick="changePoint('add');">Add Point</button>
		<button class="btn btn-primary" id="saveButton" onclick="changePoint('sub');">Substract Point</button>
	</div>
</div>