<?php
/**
 * @package     Pass.Administrator
 * @subpackage  com_pass
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
JHTML::_('behavior.formvalidation');
?>

<script type="text/javascript">
    Joomla.submitbutton = function(task)
    {
        if (task == 'package.cancel' || document.formvalidator.isValid(document.id('adminForm')))
        {
            Joomla.submitform(task, document.getElementById('adminForm'));
        }
    }
</script>
<script type="text/javascript">
jQuery(document).ready( function() {
    jQuery('#jform_currency_code').on('change', function() {
        var currency = this.value;
        if(currency == 'EUR')
        {
            jQuery('#jform_currency_sign').val('€');
        }
        else if(currency == 'GBP')
        {
            jQuery('#jform_currency_sign').val('£');
        }
        else if(currency == 'AED')
        {
            jQuery('#jform_currency_sign').val('AED');
        }
        else if(currency == 'USD')
        {
            jQuery('#jform_currency_sign').val('$');
        }
        else if(currency == 'CAD')
        {
            jQuery('#jform_currency_sign').val('$');
        }
        else if(currency == 'AUD')
        {
            jQuery('#jform_currency_sign').val('$');
        }
    });
});
</script>
<form action="<?php echo JRoute::_('index.php?option=com_bcted&view=package&package_id=' . (int) $this->item->package_id); ?>"
    method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal" enctype="multipart/form-data">
    <div class="form-horizontal">
        <fieldset>
            <div class="span12">
                <legend><?php echo JText::_('COM_BCTED_PACKAGE_DETAIL'); ?></legend>
                <div class="control-group">
                    <div class="controls"><?php echo $this->form->getInput('package_id'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('user_id'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('package_modified'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('package_created'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('package_active'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('time_stamp'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('currency_sign'); ?></div>

                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('package_name'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('package_name'); ?></div>
                </div>
                 <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('package_details'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('package_details'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('package_image'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('package_image'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('package_price'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('package_price'); ?></div>
                </div>
                 <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('currency_code'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('currency_code'); ?></div>
                </div>
                 <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('package_date'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('package_date'); ?></div>
                </div>
            </div>
            <?php echo JHtml::_('form.token'); ?>
        </fieldset>
    </div>
    <input type="hidden" name="task" value="" />

</form>
