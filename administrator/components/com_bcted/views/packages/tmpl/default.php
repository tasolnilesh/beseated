<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

$user		= JFactory::getUser();
$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));

?>
<?php $oldService = ''; ?>

<script type="text/javascript">
	function assignPackageToVenue(element,packageID)
	{
		var selectValue = element.value;
		var arr = selectValue.split('|');
		var elementID = arr[0];
		var elementType = arr[1];

		// Ajax call to send message....
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=packages.assignPackage',
			type: 'GET',
			data: 'element_id='+elementID+'&package_id='+packageID+'&element_type='+elementType,

			success: function(response){

				if(response == "200")
				{
					//jQuery('#booking_'+bookingID).remove();
				}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});


	}

	function setValueInModel(packageID,servicesIDs)
	{
		jQuery('#package_to_assign').val(packageID);

		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=packages.getAlreadySelecteServices',
			type: 'GET',
			data: 'services_ids='+servicesIDs,

			success: function(response){

				if(response.length != 0)
				{
					//jQuery('#booking_'+bookingID).remove();
					jQuery("#model-body").html(response);
				}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}

	function assing_package_to_services()
	{
		var selectedServices = jQuery('#selectedServices').val();
		var packageID = jQuery('#package_to_assign').val();

		// Ajax call to send message....
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=packages.assignPackageToServices',
			type: 'GET',
			data: 'package_id='+packageID+'&service_ids='+selectedServices,

			success: function(response){

				if(response == "200")
				{
					//jQuery('#booking_'+bookingID).remove();
					jQuery("#selectedServices option:selected").removeAttr("selected");
					location.reload();
				}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_bcted&view=packages'); ?>" method="post" id="adminForm" name="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif; ?>

<?php
	// Search tools bar
	echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
?>
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th width="2%"><?php echo JText::_('COM_BCTED_NUM'); ?></th>
				<th width="2%">
					<?php echo JHtml::_('grid.checkall'); ?>
				</th>
				<th width="2%" style="min-width:55px" class="nowrap center">
					<?php echo JHtml::_('grid.sort', 'JSTATUS', 'a.package_active', $listDirn, $listOrder); ?>
				</th>
				<th width="15%">
					<?php echo JHtml::_('grid.sort', 'COM_BCTED_PACKAGES_PACKAGE_HEADING_CAPTION', 'a.package_name', $listDirn, $listOrder);?>
				</th>

				<th width="8%">
					<?php echo JText::_('COM_BCTED_PACKAGES_PACKAGE_HEADING_ASSIGNED_VENUE');?>
				</th>
				<th width="8%">
					<?php echo JText::_('COM_BCTED_PACKAGES_PACKAGE_HEADING_ASSIGNED_SERVICE');?>
				</th>

				<th width="10%">
					<?php echo JText::_('COM_BCTED_PACKAGES_PACKAGE_HEADING_PRICE');?>
				</th>

				<th width="5%">
					<?php echo JText::_('COM_BCTED_PACKAGES_PACKAGE_HEADING_DATE');?>
				</th>

				<th width="20%"><?php echo JHtml::_('grid.sort', 'COM_BCTED_PACKAGES_PACKAGE_HEADING_DESCRIPTION', 'a.package_details', $listDirn, $listOrder);?></th>

				<?php /*<th width="1%"><?php echo JHtml::_('grid.sort', 'COM_HEARTDART_MESSAGES_HEADING_YES_VOTE', 'pi.yes_vote', $listDirn, $listOrder);?></th>
				<th width="1%"><?php echo JHtml::_('grid.sort', 'COM_HEARTDART_MESSAGES_HEADING_NO_VOTE', 'pi.no_vote', $listDirn, $listOrder);?></th>*/?>

				<!--<th width="2%"><?php //echo JText::_('COM_BCTED_PACKAGES_PACKAGE_HEADING_CREATED_'); ?></th>-->
				<th width="2%"><?php echo JHtml::_('grid.sort', 'COM_BCTED_PACKAGES_PACKAGE_HEADING_ID', 'a.package_id', $listDirn, $listOrder);?></th>
			</tr>
		</thead>

		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) :
					$link = JRoute::_('index.php?option=com_bcted&view=package&layout=edit&package_id=' . $row->package_id); ?>
					<tr>
						<td><?php echo $this->pagination->getRowOffset($i); ?></td>
						<td><?php echo JHtml::_('grid.id', $i, $row->package_id); ?></td>
						<td class="center">
							<div class="btn-group">
								<?php echo JHtml::_('jgrid.published', $row->package_active, $i, 'package.', true, 'cb'); ?>
								<?php echo JHtml::_('main.packageFeatured', $row->r_bcted, $i, 1); ?>
							</div>
						</td>
						<td><a href="<?php echo $link ?>"><?php echo $row->package_name; ?></a></td>
						<td>
							<select id="clubList" name="clubList" onchange="assignPackageToVenue(this,'<?php echo $row->package_id; ?>');">
								<option value="">Assign club</option>
								<?php foreach ($this->clubs as $key => $club): ?>
									<?php if($row->venue_id == $club->venue_id) : ?>
										<option selected="selected" value="<?php echo $club->venue_id.'|venue'; ?>"> <?php echo $club->venue_name; ?></option>
									<?php else: ?>
										<option value="<?php echo $club->venue_id.'|venue'; ?>"> <?php echo $club->venue_name; ?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>
						</td>
						<td>
							<?php /*<select multiple="multiple">
								<?php foreach ($this->companies as $key => $company): ?>
									<?php if($row->company_id == $company->company_id) : ?>
										<option selected="selected" value="<?php echo $company->company_id.'|company'; ?>"> <?php echo $company->company_name; ?></option>
									<?php else: ?>
										<option value="<?php echo $company->company_id.'|company'; ?>"> <?php echo $company->company_name; ?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>*/ ?>
							<a href="#myModal" onclick="setValueInModel(<?php echo $row->package_id; ?>,'<?php echo $row->company_ids; ?>');" role="button" class="btn" data-toggle="modal">Assign Services</a>
						</td>
						<td><?php echo $row->package_price . ' ' .$row->currency_code .'('. $row->currency_sign .')'; ?></td>
						<td><?php echo date('d-m-Y',strtotime($row->package_date)); ?></td>
						<td>
							<?php $descLength = strlen($row->package_details); ?>
							<?php if($descLength >160): ?>
								<?php echo substr($row->package_details, 0, 160).'....'; ?>
							<?php else: ?>
								<?php echo $row->package_details; ?>
							<?php endif; ?>
							<?php //echo $row->package_details; ?>
						</td>
						<?php /*<td><?php echo $row->yes_vote; ?></td>
						<td><?php echo $row->no_vote; ?></td> */ ?>
						<!-- <td><?php //echo $row->package_created; ?></td> -->
						<td><?php echo $row->package_id; ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="9"><?php echo $this->pagination->getListFooter(); ?></td>
			</tr>
		</tfoot>
	</table>
	<?php //echo $this->pagination->getListFooter(); ?>
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<style type="text/css">
.modal-body.assign-company {
    height: 300px;
}

#model-body .multi-select-service {
    height: 100%;
    width: 100%;
}
</style>



<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Select Services<?php $oldService; ?></h3>
	</div>
	<div id="model-body" class="modal-body assign-company">
		<select class="multi-select-service" multiple="multiple" id="selectedServices" >
			<?php foreach ($this->services as $key => $services): ?>
				<option value="<?php echo $services->service_id; ?>"> <?php echo $services->service_name; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div class="modal-footer">
		<input type="hidden" name="package_to_assign" id="package_to_assign" value="0">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary" onclick="assing_package_to_services()">Save changes</button>
	</div>
</div>

