<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('bootstrap.framework');
JHtml::_('formbehavior.chosen', 'select');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

$user		= JFactory::getUser();
$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
?>

<form action="<?php echo JRoute::_('index.php?option=com_bcted&view=bookings'); ?>" method="post" id="adminForm" name="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif; ?>

<?php
	// Search tools bar
	//echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
?>
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th width="2%"><?php echo JText::_('COM_BCTED_NUM'); ?></th>
				<th width="10%"><?php echo JText::_('COM_BCTED_SERVICE_BOOKING_USER_NAME'); ?></th>
				<th width="15%"><?php echo JText::_('COM_BCTED_SERVICE_BOOKING_DATE_TIME');?></th>
				<th width="15%"><?php echo JText::_('COM_BCTED_SERVICE_BOOKING_NUMBER_OF_GUEST');?></th>
				<th width="10%"><?php echo JText::_('COM_BCTED_SERVICE_BOOKING_SERVICE_NAME');?></th>
				<th width="10%"><?php echo JText::_('COM_BCTED_SERVICE_BOOKING_VENUE_NAME');?></th>
			</tr>
		</thead>

		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) :
					/*echo "<pre>";
					print_r($row);
					echo "</pre>";
					exit;*/
					//$link = JRoute::_('index.php?option=com_bcted&view=company&id=' . $row->company_id); ?>
					<tr>
						<td><?php echo $this->pagination->getRowOffset($i); ?></td>
						<td><?php echo $row->name . ' ' . $row->last_name; ?></td>
						<td>
							<?php $fromTime = explode(':',$row->package_time); ?>
							<?php //$toTime = explode(':',$row->booking_to_time); ?>

							<?php echo date('d-m-Y',strtotime($row->package_datetime)) . ' ' .$fromTime[0].':'.$fromTime[1]; ?>
						</td>
						<td>
							<?php echo $row->package_number_of_guest; ?>
						</td>
						<td><?php echo $row->package_name; ?></td>
						<td><?php echo $row->venue_name; ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="9"><?php echo $this->pagination->getListFooter(); ?></td>
			</tr>
		</tfoot>
	</table>
	<?php //echo $this->pagination->getListFooter(); ?>
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
