DROP TABLE IF EXISTS `#__bcted_venue`;

DROP TABLE IF EXISTS `#__bcted_user_profile`;

DROP TABLE IF EXISTS `#__bcted_venue_table_type`;

DROP TABLE IF EXISTS `#__bcted_venue_table`;

DROP TABLE IF EXISTS `#__bcted_venue_booking`;

DROP TABLE IF EXISTS `#__bcted_company`;

DROP TABLE IF EXISTS `#__bcted_company_services`;

DROP TABLE IF EXISTS `#__bcted_service_booking`;

DROP TABLE IF EXISTS `#__bcted_packages`;

DROP TABLE IF EXISTS `#__bcted_package_purchased`;

DROP TABLE IF EXISTS `#__bcted_ratings`;

DROP TABLE IF EXISTS `#__bcted_favourites`;
