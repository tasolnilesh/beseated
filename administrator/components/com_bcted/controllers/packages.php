<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Packages Controller
 *
 * @since  0.0.1
 */
class BctedControllerPackages extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'Package', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	/**
	 * Method to toggle the featured setting of a list of articles.
	 *
	 * @return  void
	 * @since   1.6
	 */
	public function unfeatured()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$user   = JFactory::getUser();
		$ids    = $this->input->get('cid', array(), 'array');

		/*echo "<pre>";
		print_r($ids);
		echo "</pre>";
		exit;*/

		// Access checks.
		foreach ($ids as $i => $id)
		{
			if (!$user->authorise('core.edit.state', 'com_bcted.venue.'.(int) $id))
			{
				// Prune items that you can't change.
				unset($ids[$i]);
				JError::raiseNotice(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
			}
		}

		/*echo "<pre>";
		print_r($ids);
		echo "</pre>";
		exit;*/

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('JERROR_NO_ITEMS_SELECTED'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Publish the items.
			if (!$model->unfeatured($ids))
			{
				JError::raiseWarning(500, $model->getError());
			}

			//echo "after model";
		}

		$this->setRedirect('index.php?option=com_bcted&view=packages');
	}

	/**
	 * Method to toggle the featured setting of a list of articles.
	 *
	 * @return  void
	 * @since   1.6
	 */
	public function featured()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$user   = JFactory::getUser();
		$ids    = $this->input->get('cid', array(), 'array');



		// Access checks.
		foreach ($ids as $i => $id)
		{
			if (!$user->authorise('core.edit.state', 'com_bcted.venue.'.(int) $id))
			{
				// Prune items that you can't change.
				unset($ids[$i]);
				JError::raiseNotice(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
			}
		}

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('JERROR_NO_ITEMS_SELECTED'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Publish the items.
			if (!$model->featured($ids))
			{
				JError::raiseWarning(500, $model->getError());
			}
		}

		$this->setRedirect('index.php?option=com_bcted&view=packages');
	}

	public function getAlreadySelecteServices()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$ids = $input->get('services_ids','','string');
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('company_id')
			->from($db->quoteName('#__bcted_company'))
			->where($db->quoteName('company_active') . ' = ' . $db->quote('1'));
		$db->setQuery($query);
		$activeCompanies = $db->loadColumn();

		$query1 = $db->getQuery(true);
		$query1->select('service_id,service_name')
			->from($db->quoteName('#__bcted_company_services'))
			->where($db->quoteName('service_active') . ' = ' . $db->quote(1))
			->where($db->quoteName('company_id') . ' IN (' . implode(',', $activeCompanies) .')')
			->order($db->quoteName('service_name') . ' ASC');

		$db->setQuery($query1);
		$services = $db->loadObjectList();

		$selectedServices = explode(",", $ids);

		/*echo "<pre>";
		print_r($selectedServices);
		echo "</pre>";
		exit;*/
		$selectBox='';
		$selectBox.='<select class="multi-select-service" multiple="multiple" id="selectedServices" >';
			foreach ($services as $key => $service)
			{
				if(in_array($service->service_id, $selectedServices))
				{
					$selectBox.='<option value="'.$service->service_id.'" selected="selected"> '.$service->service_name.'</option>';
				}
				else
				{
					$selectBox.='<option value="'.$service->service_id.'"> '.$service->service_name.'</option>';
				}

			}
			$selectBox.='endforeach';
		$selectBox.='</select>';

		echo  $selectBox;
		exit;
	}

	public function assignPackageToVenue()
	{


		$user   = JFactory::getUser();
		$club_id    = $this->input->get('club_id', 0, 'int');
		$package_id = $this->input->get('package_id', 0, 'int');

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base update statement.
		$query->update($db->quoteName('#__bcted_package'))
			->set($db->quoteName('venue_id') . ' = ' . $db->quote($club_id))
			->where($db->quoteName('package_id') . ' = ' . $db->quote($package_id));

		// Set the query and execute the update.
		$db->setQuery($query);

		try
		{
			$db->execute();
			echo "200";
			exit;
		}
		catch (RuntimeException $e)
		{
			//throw new RuntimeException($e->getMessage(), $e->getCode());
			echo "500";
			exit;
		}

		echo "500";
		exit;

	}

	public function assignPackage()
	{
		$user   = JFactory::getUser();
		$element_id   = $this->input->get('element_id', 0, 'int');
		$element_type = $this->input->get('element_type', '', 'string');
		$package_id   = $this->input->get('package_id', 0, 'int');

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		if(empty($element_type))
		{
			echo "400";
			exit;
		}

		if($element_type == 'venue')
		{
			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_package'))
				->set($db->quoteName('venue_id') . ' = ' . $db->quote($element_id))
				->set($db->quoteName('company_id') . ' = ' . $db->quote(0))
				->where($db->quoteName('package_id') . ' = ' . $db->quote($package_id));
		}
		else if($element_type == 'company')
		{
			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_package'))
				->set($db->quoteName('venue_id') . ' = ' . $db->quote(0))
				->set($db->quoteName('company_id') . ' = ' . $db->quote($element_id))
				->where($db->quoteName('package_id') . ' = ' . $db->quote($package_id));
		}
		else
		{
			echo "400";
			exit;
		}

		// Set the query and execute the update.
		$db->setQuery($query);

		try
		{
			$db->execute();
			echo "200";
			exit;
		}
		catch (RuntimeException $e)
		{
			//throw new RuntimeException($e->getMessage(), $e->getCode());
			echo "500";
			exit;
		}

		echo "500";
		exit;
	}

	public function assignPackageToServices()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		$packageID = $input->get('package_id',0,'int');
		$service_ids = $input->get('service_ids','','string');

		$model = $this->getModel();
		$tblPackage = $model->getTable();

		//$tblPackage = $this->getTable();
		$tblPackage->load($packageID);

		if(!$tblPackage->package_id)
		{
			echo "500";
			exit;
		}

		if(empty($service_ids))
		{
			echo "500";
			exit;
		}

		$tblPackage->company_id = 0;
		$tblPackage->company_ids = $service_ids;

		if($tblPackage->store())
		{
			echo "200";
			exit;
		}

		echo "500";
		exit;
	}

}
