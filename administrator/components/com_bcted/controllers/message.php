<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Category Controller
 *
 * @since  0.0.1
 */
class BctedControllerMessage extends JControllerForm
{
	/**
	 * Class constructor.
	 *
	 * @param   array  $config  A named array of configuration variables.
	 *
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	public function save()
	{

		$app = JFactory::getApplication();
		$input = $app->input;

		$formData = $input->get('jform',array(),'array');

		$city = $formData['city'];
		$message = $formData['message'];


		//$message=$input->get('message_text','','string');

		$model = $this->getModel();

		$model->sendPushnotification($city,$message);

		$this->setRedirect('index.php?option=com_bcted&view=messages');


	}
}
