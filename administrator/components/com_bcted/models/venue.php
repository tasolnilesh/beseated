<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Venue Model
 *
 * @since  0.0.1
 */
class BctedModelVenue extends JModelAdmin
{
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'Venue', $prefix = 'BctedTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   0.0.1
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_bcted.venue',
			'venue',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   0.0.1
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_bcted.edit.Venue.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   0.0.1
	 */
	public function unpublished($ids)
	{
		$idsStr = implode(",", $ids);

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$queryVenue = $db->getQuery(true);

		// Create the base update statement.
		$queryVenue->update($db->quoteName('#__bcted_venue'))
			->set($db->quoteName('venue_active') . ' = ' . $db->quote(0))
			->where($db->quoteName('venue_id') . ' IN (' . $idsStr.')');

		// Set the query and execute the update.
		$db->setQuery($queryVenue);

		$db->execute();

		$queryTable = $db->getQuery(true);

		// Create the base update statement.
		$queryTable->update($db->quoteName('#__bcted_venue_table'))
			->set($db->quoteName('venue_table_active') . ' = ' . $db->quote(0))
			->where($db->quoteName('venue_id') . ' IN (' . $idsStr.')');

		// Set the query and execute the update.
		$db->setQuery($queryTable);

		$db->execute();

		return true;
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   0.0.1
	 */
	public function published($ids)
	{
		$idsStr = implode(",", $ids);

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$queryVenue = $db->getQuery(true);

		// Create the base update statement.
		$queryVenue->update($db->quoteName('#__bcted_venue'))
			->set($db->quoteName('venue_active') . ' = ' . $db->quote(1))
			->where($db->quoteName('venue_id') . ' IN (' . $idsStr.')');

		// Set the query and execute the update.
		$db->setQuery($queryVenue);

		$db->execute();

		/*$queryTable = $db->getQuery(true);

		// Create the base update statement.
		$queryTable->update($db->quoteName('#__bcted_venue_table'))
			->set($db->quoteName('venue_table_active') . ' = ' . $db->quote())
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($idsStr));

		// Set the query and execute the update.
		$db->setQuery($queryTable);

		$db->execute();*/

		return true;
	}

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function unfeatured($ids)
	{
		/*echo "in model<pre>";
		print_r($ids);
		echo "</pre>";
		exit;*/
		$idsStr = implode(",", $ids);

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$queryVenue = $db->getQuery(true);

		// Create the base update statement.
		$queryVenue->update($db->quoteName('#__bcted_venue'))
			->set($db->quoteName('r_bcted') . ' = ' . $db->quote(0))
			->where($db->quoteName('venue_id') . ' IN (' . $idsStr.')');

		// Set the query and execute the update.
		$db->setQuery($queryVenue);

		/*echo $queryVenue->dump();
		exit;*/

		$db->execute();

		/*$queryTable = $db->getQuery(true);

		// Create the base update statement.
		$queryTable->update($db->quoteName('#__bcted_venue_table'))
			->set($db->quoteName('venue_table_active') . ' = ' . $db->quote())
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($idsStr));

		// Set the query and execute the update.
		$db->setQuery($queryTable);

		$db->execute();*/

		return true;
	}

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function featured($ids)
	{
		$idsStr = implode(",", $ids);

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$queryVenue = $db->getQuery(true);

		// Create the base update statement.
		$queryVenue->update($db->quoteName('#__bcted_venue'))
			->set($db->quoteName('r_bcted') . ' = ' . $db->quote(1))
			->where($db->quoteName('venue_id') . ' IN (' . $idsStr.')');

		// Set the query and execute the update.
		$db->setQuery($queryVenue);

		$db->execute();

		/*$queryTable = $db->getQuery(true);

		// Create the base update statement.
		$queryTable->update($db->quoteName('#__bcted_venue_table'))
			->set($db->quoteName('venue_table_active') . ' = ' . $db->quote())
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($idsStr));

		// Set the query and execute the update.
		$db->setQuery($queryTable);

		$db->execute();*/

		return true;
	}

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function setsings($venueID,$type = 0)
	{

		$tblVenue = $this->getTable();

		$tblVenue->load($venueID);

		if(!$tblVenue->venue_id)
		{
			return true;
		}

		if($type == 0 && $tblVenue->venue_signs == 0)
		{

			return true;
		}

		if($type == 1 && $tblVenue->venue_signs == 3)
		{
			$tblVenue->venue_signs = $tblVenue->venue_signs -1;
			return true;
		}

		if($type == 0)
		{
			$tblVenue->venue_signs = $tblVenue->venue_signs - 1;
		}
		else if($type == 1)
		{
			$tblVenue->venue_signs = $tblVenue->venue_signs + 1;
		}

		$tblVenue->store();

		return true;
	}

	public function change_licence_type($licence,$venue_id)
	{
		$tblVenue = $this->getTable();

		$tblVenue->load($venue_id);

		if(!$tblVenue->venue_id)
		{
			return false;
		}

		$tblVenue->licence_type = $licence;

		if($tblVenue->store())
		{
			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_venue_table'))
				->set($db->quoteName('premium_table_id') . ' = ' . $db->quote(0))
				->where($db->quoteName('venue_id') . ' = ' . $db->quote($tblVenue->venue_id));

			// Set the query and execute the update.
			$db->setQuery($query);
			$db->execute();

			$query = $db->getQuery(true);

			// Create the base update statement.
			$query2->update($db->quoteName('#__bcted_premium_table'))
				->set($db->quoteName('venue_id') . ' = ' . $db->quote(0))
				->set($db->quoteName('venue_table_id') . ' = ' . $db->quote(0))
				->where($db->quoteName('venue_id') . ' = ' . $db->quote($tblVenue->venue_id));

			// Set the query and execute the update.
			$db->setQuery($query2);
			$db->execute();

			return true;
		}

		return false;
	}

	public function change_commission_rate($new_rate,$venue_id)
	{
		$tblVenue = $this->getTable();

		$tblVenue->load($venue_id);

		if(!$tblVenue->venue_id)
		{
			return false;
		}

		$tblVenue->commission_rate = $new_rate;

		if($tblVenue->store())
		{
			return true;
		}

		return false;
	}
}


