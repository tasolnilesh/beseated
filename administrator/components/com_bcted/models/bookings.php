<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Bookings Model
 *
 * @since  0.0.1
 */
class BctedModelBookings extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			/*$config['filter_fields'] = array(
				'company_id','a.company_id',
				'company_name','a.company_name',
				'company_active','a.company_active'
			);*/
		}

		parent::__construct($config);
	}

	public function getLiveUserID()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('id')
			->from($db->quoteName('#__users'))
			->where($db->quoteName('block') . ' = ' . $db->quote('0'));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadColumn();

		return $result;
	}

	protected function getListQuery()
	{
		/*echo "<pre>";
		print_r($this->state);
		echo "</pre>";*/
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Filter: like / search
		$search = $this->getState('filter.search');

		$liveUSers = $this->getLiveUserID();
		$liveUSersStr = implode(",", $liveUSers);

		// Create the base select statement.
		$query->select('payment.payment_id,payment.booked_element_id,payment.booked_element_type,payment.currency_code,payment.currency_sign,payment.element_price')
			->from($db->quoteName('#__bcted_payment_status','payment'))
			->where($db->quoteName('payment.paid_status') . ' = ' . $db->quote(1))
			->where($db->quoteName('payment.booked_element_type') . ' = ' . $db->quote('service'))
			->order($db->quoteName('payment.time_stamp') . ' DESC');

		$query->select('bookservice.service_id,bookservice.company_id,bookservice.user_id,bookservice.service_booking_datetime,bookservice.booking_from_time,bookservice.booking_to_time,bookservice.service_location,bookservice.service_booking_additional_info,bookservice.service_booking_number_of_guest,bookservice.male_count,bookservice.female_count')
			->join('LEFT','#__bcted_service_booking AS bookservice ON bookservice.service_booking_id=payment.booked_element_id');

		$query->select('service.service_name')
			->join('LEFT','#__bcted_company_services AS service ON service.service_id=bookservice.service_id');

		$query->select('company.company_name')
			->join('LEFT','#__bcted_company AS company ON company.company_id=bookservice.company_id');

		$query->select('user.name')
			->join('LEFT','#__users AS user ON user.id=bookservice.user_id');

		$query->select('profile.last_name')
			->join('LEFT','#__bcted_user_profile AS profile ON profile.userid=bookservice.user_id');


		/*echo $query->dump();
		exit;

		if (!empty($search))
		{
			$like = $db->quote('%' . $search . '%');
			$query->where('a.company_name LIKE ' . $like);
		}

		// Filter by published state
		$published = $this->getState('filter.published');

		if (is_numeric($published))
		{
			$query->where('a.company_active = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.company_active IN (0, 1))');
		}

		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering', 'a.company_id');
		$orderDirn 	= $this->state->get('list.direction', 'ASC');

		$fullordering = $this->state->get('list.fullordering', '');



		if(!empty($fullordering))
		{
			$query->order($db->escape($fullordering));
		}
		else
		{
			$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));
		}*/


		//$query->order($db->quoteName('a.post_id') . ' DESC');

		//echo $query->dump();

		return $query;
	}

	/*
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   12.2
	 */
	protected function populateState($ordering = 'id', $direction = 'ASC')
	{
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.' . $layout;
		}

		$value = $app->input->get('limit', $app->getCfg('list_limit', 0), 'uint');
		$this->setState('list.limit', 10);

		$value = $app->input->get('limitstart', 0, 'uint');
		$this->setState('list.start', $value);

		$value = $app->input->get('listdirection', 'ASC', 'string');
		$this->setState('list.direction', $value);

		/*$searchdate = $this->getUserStateFromRequest($this->context . '.filter.selecteddate', 'filter_selecteddate');
		$this->setState('filter.selecteddate', $searchdate);*/

		$orderCol = $app->input->get('filter_order', 'a.company_id');

		if (!in_array($orderCol, $this->filter_fields))
		{
			$orderCol = 'a.company_id';
		}

		$this->setState('list.ordering', $orderCol);

		/*$listOrder = $app->input->get('filter_order_Dir', 'ASC');

		$this->setState('layout', $app->input->getString('layout'));*/

		// List state information.
		parent::populateState();

	}

}
