<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Packages Messages Model
 *
 * @since  0.0.1
 */
class BctedModelPackages extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'package_id','a.package_id',
				'package_name','a.package_name',
				'package_active','a.package_active'
			);
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		/*echo "<pre>";
		print_r($this->state);
		echo "</pre>";*/
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Filter: like / search
		$search = $this->getState('filter.search');

		// Create the base select statement.
		$query->select('a.*')
			->from($db->quoteName('#__bcted_package','a'));


		if (!empty($search))
		{
			$like = $db->quote('%' . $search . '%');
			//$query->where('a.caption LIKE ' . $like);
		}

		// Filter by published state
		$published = $this->getState('filter.published');
		//$fullordering = $this->getState('list.fullordering');
		//echo "<br>fullordering : " . $fullordering;
		if (is_numeric($published))
		{
			$query->where('a.package_active = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.package_active IN (0, 1))');
		}

		// Add the list ordering clause.
		/*$orderCol	= $this->state->get('list.ordering', 'a.package_id');
		$orderDirn 	= $this->state->get('list.direction', 'asc');*/

		$orderCol	= $this->state->get('list.ordering', 'a.package_id');
		$orderDirn 	= $this->state->get('list.direction', 'ASC');

		$fullordering = $this->state->get('list.fullordering', '');
		if(!empty($fullordering))
		{
			$query->order($db->escape($fullordering));
		}
		else
		{
			$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));
		}



		return $query;
	}

	/*
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   12.2
	 */
	protected function populateState($ordering = 'id', $direction = 'ASC')
	{
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.' . $layout;
		}

		$value = $app->input->get('limit', $app->getCfg('list_limit', 0), 'uint');
		$this->setState('list.limit', 10);

		$value = $app->input->get('limitstart', 0, 'uint');
		$this->setState('list.start', $value);

		$value = $app->input->get('listdirection', 'ASC', 'string');
		$this->setState('list.direction', $value);

		/*$searchdate = $this->getUserStateFromRequest($this->context . '.filter.selecteddate', 'filter_selecteddate');
		$this->setState('filter.selecteddate', $searchdate);*/

		$orderCol = $app->input->get('filter_order', 'a.package_id');

		if (!in_array($orderCol, $this->filter_fields))
		{
			$orderCol = 'a.package_id';
		}

		$this->setState('list.ordering', $orderCol);

		/*$listOrder = $app->input->get('filter_order_Dir', 'ASC');

		$this->setState('layout', $app->input->getString('layout'));*/

		// List state information.
		parent::populateState();

	}

	public function getClubList()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue'))
			->where($db->quoteName('venue_active') . ' = ' . $db->quote('1'))
			->order($db->quoteName('venue_name') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadObjectList();

			return $result;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		return array();
	}

	public function getCompanyList()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_company'))
			->where($db->quoteName('company_active') . ' = ' . $db->quote('1'))
			->order($db->quoteName('company_name') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadObjectList();

			return $result;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		return array();
	}

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function getServiceList()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('company_id')
			->from($db->quoteName('#__bcted_company'))
			->where($db->quoteName('company_active') . ' = ' . $db->quote('1'));
		$db->setQuery($query);
		$activeCompanies = $db->loadColumn();

		$query1 = $db->getQuery(true);
		$query1->select('service_id,service_name')
			->from($db->quoteName('#__bcted_company_services'))
			->where($db->quoteName('service_active') . ' = ' . $db->quote(1))
			->where($db->quoteName('company_id') . ' IN (' . implode(',', $activeCompanies) .')');

		$db->setQuery($query1);
		$services = $db->loadObjectList();

		return $services;

	}
}
