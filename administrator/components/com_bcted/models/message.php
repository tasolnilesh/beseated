<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Message Model
 *
 * @since  0.0.1
 */
class BctedModelMessage extends JModelAdmin
{
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'Message', $prefix = 'BctedTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	public function sendPushnotification($city,$message)
	{
		/*echo $city . " || " . $message;
		exit;*/

		if(empty($city) || empty($message))
		{
			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$queryIJUser = $db->getQuery(true);

		// Create the base select statement.
		/*$queryIJUser->select('userid')
			->from($db->quoteName('#__ijoomeradv_users'))
			->where($db->quoteName('device_token') . ' != ' . $db->quote(''));

		// Set the query and load the result.
		$db->setQuery($queryIJUser);

		$ijUsers = $db->loadColumn();*/

		/*echo "<pre>";
		print_r($ijUsers);
		echo "</pre>";
		exit;*/

		/*if(count($ijUsers) == 0)
		{
			return false;
		}*/

		$queryCityUser = $db->getQuery(true);

		// Create the base select statement.
		$queryCityUser->select('userid')
			->from($db->quoteName('#__bcted_user_profile'))
			->where($db->quoteName('user_type_for_push') . ' = ' . $db->quote('guest'))
			->where($db->quoteName('city') . ' LIKE  ' . $db->quote('%'.$city.'%'));
			//->where($db->quoteName('userid') . ' IN (' . implode(",", $ijUsers) . ')');

		// Set the query and load the result.
		$db->setQuery($queryCityUser);

		$cityUser = $db->loadColumn();

		/*echo "<pre>";
		print_r($cityUser);
		echo "</pre>";
		exit;*/

		$sendPushToUserArray = array();

		$user = JFactory::getUser();
		$sentDeviceToken = array();

		foreach ($cityUser as $key => $userID)
		{
			$tblMessage = JTable::getInstance('Message', 'BctedTable',array());
			$tblMessage->load(0);

			$connectionID = $this->getMessageConnection($userID,$user->id);

			$data = array();

			$data['venue_id']   = 0;
			$data['company_id'] = 0;
			$data['table_id']   = 0;
			$data['service_id'] = 0;
			$data['userid']     = $userID;
			$data['to_userid']  = $userID;
			$data['message']    = $message;
			$data['message_type'] = 'system';
			$data['from_userid'] = $user->id;
			$data['created']    = date('Y-m-d H:i:s');
			$data['time_stamp'] = time();

			$data['connection_id'] = $connectionID;
			$tblMessage->bind($data);

			if($tblMessage->store())
			{
				//$sendPushToUserArray[] = $userID;

				$jsonarray=array();

				$messageText                                               = 'Message From Administrator';
				$messageText = JText::_('PUSHNOTIFICATION_TYPE_CITYBASEPUSHNOTIFICATION_MESSAGE');
				$foundUser = JFactory::getUser($userID);
				$jsonarray['pushNotificationData']['message']    = $messageText . ' || ' .$foundUser->name;
				$jsonarray['pushNotificationData']['id']         = "administrator;$connectionID;admin"; //$connectionID;
				$jsonarray['pushNotificationData']['to']         = $userID;
				$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_CITYBASEPUSHNOTIFICATION'); //'CityBasePushNotification';
				$jsonarray['pushNotificationData']['configtype'] = '';

				/*echo $key."<pre>";
				print_r($jsonarray);
				echo "</pre>";*/



				$lastSendToken = $this->goForPushNotification($jsonarray,$sentDeviceToken);

				if(!empty($lastSendToken))
				{
					$sentDeviceToken[] = $lastSendToken;
				}

				/*echo "<pre>";
				print_r($sentDeviceToken);
				echo "</pre>";*/
				//exit;

			}
		}

		$tblSystemMessage = JTable::getInstance('SystemMessage', 'BctedTable',array());
		$tblSystemMessage->load(0);
		$tblSystemMessage->message = $message;
		$tblSystemMessage->city = $city;
		$tblSystemMessage->published = 1;
		$tblSystemMessage->users = implode(",", $cityUser);
		$tblSystemMessage->created = date('Y-m-d H:i:s');
		$tblSystemMessage->time_stamp = time();

		$tblSystemMessage->store();

		/*echo "<pre>";
		print_r($sentDeviceToken);
		echo "</pre>";

		exit;*/

		//411,409,403,429

		/*if(count($sendPushToUserArray)!=0)
		{
			$messageText                                               = 'Message From Administrator';
			$jsonarray['pushNotificationData']['message']    = $messageText;
			$jsonarray['pushNotificationData']['id']         = "administrator;$connectionID;admin"; //$connectionID;
			$jsonarray['pushNotificationData']['to']         = implode(",", $sendPushToUserArray);
			$jsonarray['pushNotificationData']['type']       = 'CityBasePushNotification';
			$jsonarray['pushNotificationData']['configtype'] = '';


			$this->goForPushNotification($jsonarray);
		}*/
	}

	public function goForPushNotification($jsonarray,$sentDeviceToken)
	{
		$deviceToken = "";
		if (!empty($jsonarray['pushNotificationData']))
		{
			require_once JPATH_SITE.'/components/com_ijoomeradv/models/ijoomeradv.php';
			require_once JPATH_SITE.'/components/com_ijoomeradv/helpers/helper.php';

			$ijoomeradvModel = new IjoomeradvModelijoomeradv();
			$result = $ijoomeradvModel->getApplicationConfig();

			foreach ($result as $value)
			{
				defined($value->name) or define($value->name, $value->value);
			}


			$db = JFactory::getDbo();

			$memberlist = $jsonarray['pushNotificationData']['to'];

			if ($memberlist)
			{
				$query = $db->getQuery(true);

				// Create the base select statement.
				$query->select('a.userid, a.jomsocial_params, a.device_token, a.device_type')
					->from($db->qn('#__ijoomeradv_users','a'))
					->where($db->qn('a.userid') . ' = ' . $db->q($memberlist));
				$query->select('b.user_type_for_push AS user_type')
					->join('LEFT','#__bcted_user_profile AS b ON b.userid=a.userid');

				/*echo "<pre>";
				print_r($query->dump());
				echo "</pre>";*/

				// Set the query and load the result.
				$db->setQuery($query);

				$puserlist = $db->loadObjectList();

				/*echo "<pre>";
				print_r($puserlist);
				echo "</pre>";*/


				//$trackID= array(411,409,403,429);
				$alredySentToken = array();

				foreach ($puserlist as $puser)
				{
					// Check config allow for jomsocial


					if (!empty($jsonarray['pushNotificationData']['configtype']) and $jsonarray['pushNotificationData']['configtype'] != '')
					{
						$ijparams = json_decode($puser->jomsocial_params);
						$configallow = $jsonarray['pushNotificationData']['configtype'];
					}
					else
					{
						$configallow = 1;
					}

					if ($configallow && !empty($puser))
					{
						/*$params = json_decode($puser->params);

						if(!$params->pushNotification->updateMyBookingStatus)
						{
							continue;
						}*/

						if(in_array($puser->device_token, $sentDeviceToken))
						{
							continue;
						}

						// $alredySentToken[] = $puser->device_token;
						$deviceToken = $puser->device_token;

						/*echo "<pre>";
						print_r($alredySentToken);
						echo "</pre>";*/




						if (IJOOMER_PUSH_ENABLE_IPHONE == 1 && $puser->device_type == 'iphone')
						{
							$options = array();
							$options['device_token'] = $puser->device_token;
							$options['live'] = intval(IJOOMER_PUSH_DEPLOYMENT_IPHONE);
							$options['aps']['alert'] = strip_tags($jsonarray['pushNotificationData']['message']);
							$options['aps']['type'] = $jsonarray['pushNotificationData']['type'];
							$options['aps']['id'] = ($jsonarray['pushNotificationData']['id'] != 0) ? $jsonarray['pushNotificationData']['id'] : $jsonarray['pushNotificationData']['id'];
							IJPushNotif::sendIphonePushNotification($options,$puser->user_type);
						}

						if (IJOOMER_PUSH_ENABLE_ANDROID == 1 && $puser->device_type == 'android')
						{
							$options = array();
							$options['registration_ids'] = array($puser->device_token);
							$options['data']['message'] = strip_tags($jsonarray['pushNotificationData']['message']);
							$options['data']['type'] = $jsonarray['pushNotificationData']['type'];
							$options['data']['id'] = ($jsonarray['pushNotificationData']['id'] != 0) ? $jsonarray['pushNotificationData']['id'] : $jsonarray['pushNotificationData']['id'];

							IJPushNotif::sendAndroidPushNotification($options);
						}


					}
				}
				/*echo "End of loop";
				exit;*/
			}

			unset($jsonarray['pushNotificationData']);
		}

		return $deviceToken;
	} // End of goForPushNotification

	public function getMessageConnection($fromUser,$toUser)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_message_connection'))
			->where($db->quoteName('from_userid') . ' IN (' . $fromUser.','.$toUser .')')
			->where($db->quoteName('to_userid') . ' IN (' . $fromUser.','.$toUser .')');

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObject();

		if($result)
		{
			return $result->connection_id;
		}
		else
		{
			//JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
			$tblMessageconnection = JTable::getInstance('Messageconnection', 'BctedTable',array());

			/*echo "<pre>";
			print_r($tblMessageconnection);
			echo "</pre>";*/
			$data = array();
			$data['from_userid'] = $fromUser;
			$data['to_userid']   = $toUser;

			$tblMessageconnection->load(0);
			$tblMessageconnection->bind($data);
			if($tblMessageconnection->store())
			{
				return $tblMessageconnection->connection_id;
			}
			else
			{
				return 0;
			}
		}
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   0.0.1
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_bcted.message',
			'message',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   0.0.1
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_bcted.edit.Message.data',
			array()
		);

		$user = JFactory::getUser();

		if (empty($data))
		{
			$data = $this->getItem();


			/*$data->package_modified=date('Y-m-d H:i:s');

			if(empty($data->package_created))
			{
				$data->package_modified=date('Y-m-d H:i:s');
				$data->package_created=date('Y-m-d H:i:s');
			}

			if(!$data->package_id)
			{
				$data->package_active=1;
				$data->user_id = $user->id;
				$data->time_stamp = time();
			}*/
		}

		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;*/

		return $data;
	}
}
