<?php

/**
 * @package     Pass.Plugin
 * @subpackage  com_pass
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Pass PaymentsPaypal plugin
 *
 * @since  0.0.1
 */
class plgBctPaypal extends JPlugin
{
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  0.0.1
	 */
	protected $autoloadLanguage = true;

	function onPackageInvitePaymentPaypal()
	{
		$jinput  = JFactory::getApplication()->input;
		$inviteID = $jinput->get('package_invite_id', 0, 'int');
		$bookingType = $jinput->get('booking_type', '', 'string');
		$bcDollars = $jinput->get('bc_dollars', 0, 'int');
		$goForPaypal = 1;
		$itemName = "";
		$itemNumber = 0;
		$elemntAmount = 0.00;
		$elemntCurrencySign = '';
		$elemntCurrencyCode = '';

		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_bcted/tables');

		$user    = JFactory::getUser();
		/*echo "<pre>";
		print_r($inviteID);
		echo "</pre>";
		exit;*/

		if ($user->id == 0)
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?option=com_users&view=login');
			$app->close();
		}

		if(!$inviteID)
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?option=com_users&view=login');
			$app->close();
		}

		$bctParams = $this->getExtensionParam();
		$paypalEmail = $bctParams->paypalId;

		if(empty($paypalEmail))
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?success=false3');
			$app->close();
		}

		$tblPackageInvite = JTable::getInstance('PackageInvite', 'BctedTable');
		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackage = JTable::getInstance('Package', 'BctedTable');

		$tblPackageInvite->load($inviteID);
		$tblPackagePurchased->load($tblPackageInvite->package_purchase_id);
		$tblPackage->load($tblPackageInvite->package_id);

		$itemNumber   = $tblPackage->package_id;
		$itemName     = $tblPackage->package_name;
		$elemntAmount = $tblPackageInvite->amount_payable;

		/*echo $elemntAmount;
		exit;*/
		$elemntCurrencySign = $tblPackage->currency_sign;
		$elemntCurrencyCode = $tblPackage->currency_code;

		$PaymentStatus = JTable::getInstance('PaymentStatus', 'BctedTable');
		$PaymentStatus->load(0);

		$convertExp = "";

		if($elemntCurrencyCode!="USD")
		{
			$convertExp = $elemntCurrencyCode."_USD";
		}

		$currencyRateUSD = 0;

		if(!empty($convertExp))
		{
			$url = "http://www.freecurrencyconverterapi.com/api/v3/convert?q=".$convertExp."&compact=y";
			$ch = curl_init();
			$timeout = 0;
			curl_setopt ($ch, CURLOPT_URL, $url);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$rawdata = curl_exec($ch);
			curl_close($ch);
			if(empty($rawdata))
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php?success=false12');
				$app->close();
			}
			$object = json_decode($rawdata);
			$currencyRateUSD = $object->$convertExp->val;
		}

		//$depositPer = ($bctParams->deposit_per)?$bctParams->deposit_per:20;
		$loyaltyPer = ($bctParams->loyalty_per)?$bctParams->loyalty_per:1;

		$depositAmount = $elemntAmount;


		/*echo $depositAmount . " || " . $bookingType . " || " .$convertExp;
		exit;*/

		if($currencyRateUSD)
		{
			$convertedElementAmount = $elemntAmount * $currencyRateUSD;
		}
		else
		{
			$convertedElementAmount = $elemntAmount;
		}



		$loyaltyPoints = ($convertedElementAmount * $loyaltyPer) / 100;
		$loyaltyPoints = number_format($loyaltyPoints,2);

		/*echo "Currency RAte : ".$currencyRateUSD."<br />";
		echo "BC deposit Amount : " . $convertedElementAmount . "<br />";
		echo "BC Dollars : " . $loyaltyPoints . "<br />";
		exit;*/

		if($bcDollars)
		{
			$bcdToUsd = $bcDollars / 10;
			$bcdToUsd = number_format($bcdToUsd);

			//echo "BC Dollars : " . $bcdToUsd . "<br />";

			$payAfterBCD = $depositAmount - $bcdToUsd;

			$minusBCD = $bcDollars * 2;

			$minusBCD = $bcDollars - $minusBCD;

			if($payAfterBCD > 0)
			{
				$depositAmountAfertBCD = $payAfterBCD;
				$goForPaypal = 1;
			}
			else
			{
				$goForPaypal = 0;
			}
		}

		/*echo $bcDollars;
		exit;*/

		$paymentData['booked_element_id']   = $inviteID;
		$paymentData['booked_element_type'] = $bookingType;
		$paymentData['currency_code']       = $elemntCurrencyCode;

		$paymentData['currency_sign']       = $elemntCurrencySign;
		$paymentData['customer_paid']       = $elemntAmount;
		$paymentData['element_price']       = $elemntAmount;
		$paymentData['platform_income']     = ($bcDollars)?$depositAmountAfertBCD:$depositAmount;
		$paymentData['element_income']      = $elemntAmount;
		$paymentData['bcd_used']            = $bcDollars;
		$paymentData['paid_status']         = 0;
		$paymentData['time_stamp']          = time();
		$paymentData['created']             = date('Y-m-d H:i:s');

		/*echo "<pre>";
		print_r($paymentData);
		echo "</pre>";
		exit;*/

		//$orderPostData['offer_id'] = $offerID;
		$PaymentStatus->bind($paymentData);



		if(!$PaymentStatus->store())
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?success=false13');
			$app->close();

			return false;
		}

		$paymentEntryID = $PaymentStatus->payment_id;

		if($bcDollars)
		{
			if($goForPaypal == 1)
			{
				$this->addLoyaltyPoints($user->id,'Payout',$minusBCD,$paymentEntryID);
			}
			else
			{
				$this->addLoyaltyPoints($user->id,'Payout',$minusBCD,$paymentEntryID,1);
			}

		}

		$this->addLoyaltyPoints($user->id,'purchase.'.$bookingType,$loyaltyPoints,$paymentEntryID);




		if($bcDollars)
		{
			$amountToPaypal = $depositAmountAfertBCD;
		}
		else
		{
			$amountToPaypal = $depositAmount;
		}

		if($elemntCurrencyCode=="AED" && $currencyRateUSD)
		{
			$amountToPaypal = $amountToPaypal * $currencyRateUSD;
			$amountToPaypal = number_format($amountToPaypal,2);
			$elemntCurrencyCode = "USD";

			/*echo $amountToPaypal."<br />";
			echo $elemntCurrencyCode."<br />";
			exit;*/
		}

		$post_variables = Array(
			"cmd"            => "_xclick",
			"upload"         => "2",
			"business"       => $paypalEmail,
			"receiver_email" => $paypalEmail,
			"quantity"       => 1,
			"item_name"		 => $itemName,
			"item_number"    => $itemNumber,
			"no_shipping"	 => 0,
			"amount"         => $amountToPaypal,
			"return"         => JURI::base() . "index.php?success=true&payment_entry_id={$paymentEntryID}",
			"notify_url"     => JURI::base() . 'index.php?option=com_bcted&payment_entry_id='.$paymentEntryID.'&task=packageorder.ipninvite',
			"cancel_return"  => JURI::base() . "index.php?success=false",
			"currency_code"  => $elemntCurrencyCode
		);

		if($goForPaypal)
		{
			echo "<form action='https://www.sandbox.paypal.com/cgi-bin/webscr' method='post' name='paypalfrm' id='paypalfrm'>";
			echo "<h3>" . JText::_('You will be redirected to paypal.') . "</h3>";

			foreach ($post_variables as $name => $value)
			{
				echo "<input type='hidden' name='$name' value='$value' />";
			}

			echo '<INPUT TYPE="hidden" name="charset" value="utf-8">';
			echo "</form>";
			?>
			<script type='text/javascript'>document.paypalfrm.submit();</script>
			<?php
		}
		else
		{
			$tblPaymentStatus = JTable::getInstance('PaymentStatus', 'BctedTable',array());
			$tblPaymentStatus->load($paymentEntryID);

			if(!$tblPaymentStatus->payment_id)
			{
				$app = JFactory::getApplication();
				$app->redirect("index.php?success=false14");
				$app->close();
			}

			$elementID = $tblPaymentStatus->booked_element_id;
			$elementType = $tblPaymentStatus->booked_element_type;

			$paymentGross = $depositAmount; //$input->get('payment_gross',0.00);
			$paymentFee   = 0.00; //$input->get('payment_fee',0.00);
			$txnID        = 'paybybcd';//$input->get('txn_id','','string');
			$paymentStatusText  = $bookingType;

			$tblPaymentStatus->payment_fee = $paymentFee;
			$tblPaymentStatus->txn_id = $txnID;
			$tblPaymentStatus->payment_status = $paymentStatusText;

			if($paymentStatusText == 'Completed')
			{
				$tblPaymentStatus->paid_status = 1;

				if($elementType == 'service')
				{
					$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');
					$tblServiceBooking->load($elementID);
					$tblServiceBooking->status = 5;
					$tblServiceBooking->user_status = 5;

					$tblServiceBooking->total_price = $tblPaymentStatus->element_price;
					$tblServiceBooking->deposit_amount = $tblPaymentStatus->platform_income;
					$tblServiceBooking->amount_payable = $tblPaymentStatus->element_price - $tblPaymentStatus->platform_income;

					$tblServiceBooking->store();
				}
				else if($elementType == 'venue')
				{
					$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');
					$tblVenuebooking->load($elementID);
					$tblVenuebooking->status = 5;
					$tblVenuebooking->user_status = 5;

					$tblVenuebooking->total_price = $tblPaymentStatus->element_price;
					$tblVenuebooking->deposit_amount = $tblPaymentStatus->platform_income;
					$tblVenuebooking->amount_payable = $tblPaymentStatus->element_price - $tblPaymentStatus->platform_income;

					$tblVenuebooking->store();
				}
				else if($elementType == 'package')
				{
					$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
					$tblPackagePurchased->load($elementID);
					$tblPackagePurchased->status = 5;
					$tblPackagePurchased->user_status = 5;
					$tblPackagePurchased->store();
				}

				// Initialiase variables.
				$db    = JFactory::getDbo();
				$query = $db->getQuery(true);

				// Create the base update statement.
				$query->update($db->quoteName('#__bcted_loyalty_point'))
					->set($db->quoteName('is_valid') . ' = ' . $db->quote(1))
					->where($db->quoteName('cid') . ' = ' . $db->quote($paymentEntryID));

				// Set the query and execute the update.
				$db->setQuery($query);
				$db->execute();
			}

			$tblPaymentStatus->store();

			$app = JFactory::getApplication();
			$app->redirect("index.php?success=true&payment_entry_id={$paymentEntryID}");
			$app->close();
		}
		return true;

	}



	/**
	 * Plugin method with the same name as the event will be called automatically.
	 *
	 * @return boolean
	 */
	function onPreparePaymentPaypal()
	{

		require_once ("paypalplatform.php");

		$jinput  = JFactory::getApplication()->input;

		$bookingID = $jinput->get('booking_id', 0, 'int');
		$bookingType = $jinput->get('booking_type', '', 'string');
		$bcDollars = $jinput->get('bc_dollars', 0, 'int');
		$goForPaypal = 1;

		$itemName = "";
		$itemNumber = 0;

		$elemntAmount = 0.00;
		$elemntCurrencySign = '';
		$elemntCurrencyCode = '';

		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_bcted/tables');

		$user    = JFactory::getUser();

		if ($user->id == 0)
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?option=com_users&view=login');
			$app->close();
		}

		if ($bookingID == 0)
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?success=false1');
			$app->close();
		}

		if (empty($bookingType))
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?success=false2');
			$app->close();
		}

		$bctParams = $this->getExtensionParam();
		$paypalEmail = $bctParams->paypalId;

		if(empty($paypalEmail))
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?success=false3');
			$app->close();
		}

		if($bookingType == 'package' || $bookingType == 'Package')
		{
			$bookingType = "package";
			$tblBooking = JTable::getInstance('PackagePurchased', 'BctedTable');
			$tblPackage = JTable::getInstance('Package', 'BctedTable');

			$tblBooking->load($bookingID);

			if(!$tblBooking->package_purchase_id)
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php?success=false4');
				$app->close();
			}

			$tblPackage->load($tblBooking->package_id);

			if(!$tblPackage->package_id || $tblPackage->package_price == 0)
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php?success=false5');
				$app->close();
			}

			$itemNumber   = $tblPackage->package_id;
			$itemName     = $tblPackage->package_name;
			$elemntAmount = $tblBooking->total_price;

			/*echo $elemntAmount;
			exit;*/
			$elemntCurrencySign = $tblPackage->currency_sign;
			$elemntCurrencyCode = $tblPackage->currency_code;
		}
		else if($bookingType == 'venue' || $bookingType == 'Venue')
		{
			$bookingType = "venue";
			$tblBooking = JTable::getInstance('Venuebooking', 'BctedTable');
			$tblVenue = JTable::getInstance('Venue', 'BctedTable');
			$tblTable = JTable::getInstance('Table', 'BctedTable');

			$tblBooking->load($bookingID);

			if(!$tblBooking->venue_booking_id)
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php?success=false6');
				$app->close();
			}

			$tblVenue->load($tblBooking->venue_id);

			if(!$tblVenue->venue_id || $tblVenue->venue_active == 0)
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php?success=false7');
				$app->close();
			}

			$elemntCurrencySign=$tblVenue->currency_sign;
			$elemntCurrencyCode=$tblVenue->currency_code;

			$tblTable->load($tblBooking->venue_table_id);

			if(!$tblTable->venue_table_id || $tblTable->venue_table_price == 0 || !$tblTable->venue_table_active || $tblTable->venue_id != $tblVenue->venue_id )
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php?success=false8');
				$app->close();
			}

			$itemNumber   = $tblTable->venue_table_id;
			$itemName     = ($tblTable->premium_table_id)?$tblTable->venue_table_name:$tblTable->custom_table_name;
			$elemntAmount = $tblTable->venue_table_price;
		}
		else if($bookingType == 'service' || $bookingType == 'Service')
		{
			$bookingType = "service";
			$tblCompany        = JTable::getInstance('Company', 'BctedTable');
			$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');
			$tblService        = JTable::getInstance('Service', 'BctedTable');

			$tblServiceBooking->load($bookingID);

			if(!$tblServiceBooking->service_booking_id)
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php?success=false9');
				$app->close();
			}

			$tblCompany->load($tblServiceBooking->company_id);

			if(!$tblCompany->company_id || $tblCompany->company_active==0)
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php?success=false10');
				$app->close();
			}

			$elemntCurrencySign=$tblCompany->currency_sign;
			$elemntCurrencyCode=$tblCompany->currency_code;

			$tblService->load($tblServiceBooking->service_id);

			if(!$tblService->service_id || $tblService->service_price <= 0 || !$tblService->service_active || $tblService->company_id != $tblCompany->company_id )
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php?success=false11');
				$app->close();
			}

			$itemNumber   = $tblService->service_id;
			$itemName     = $tblService->service_name;
			$elemntAmount = $tblService->service_price;
		}

		/*if($siteTip!=0)
		{
			if($siteTip<=100)
			{
				$tip = $siteTip * $tblOffer->price/100;
			}
		}*/

		/*$tblRest = JTable::getInstance('Restaurant', 'DiffTable');
		$tblRest->load($tblOffer->rest_id);*/

		/*if(empty($tblRest->paypal_id))
		{*/
			/*$app = JFactory::getApplication();
			$app->redirect('index.php', true);
			$app->close();*/

			/*$error = JText::_('Restaurant has no paypal configuration');
            JFactory::getApplication()->redirect(JURI::base(), $error, 'error' );
            return false;
		}*/

		//$tblRest->paypal_id = "pratik.dasa1@tasolglobal.com";

		/*$tblDish = JTable::getInstance('Dish', 'DiffTable');
		$tblDish->load($tblOffer->dish_id);

		$price      = $tblOffer->price + $tip;*/

		$PaymentStatus = JTable::getInstance('PaymentStatus', 'BctedTable');
		$PaymentStatus->load(0);

		/*$referenceID = $tblRest->reference_id + 1;
		$refString  = substr($tblRest->name, 0, 3);
		$threeDigit = str_pad( $referenceID, 3, "0", STR_PAD_LEFT );
		$reference = strtolower($refString).$threeDigit;
		$orderPostData['reference'] = $reference;
		$restPost['reference_id'] = $referenceID;
		$tblRest->bind($restPost);
		$tblRest->store();*/

		$convertExp = "";

		if($elemntCurrencyCode!="USD")
		{
			$convertExp = $elemntCurrencyCode."_USD";
		}

		$currencyRateUSD = 0;

		if(!empty($convertExp))
		{
			$url = "http://www.freecurrencyconverterapi.com/api/v3/convert?q=".$convertExp."&compact=y";
			$ch = curl_init();
			$timeout = 0;
			curl_setopt ($ch, CURLOPT_URL, $url);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$rawdata = curl_exec($ch);
			curl_close($ch);
			if(empty($rawdata))
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php?success=false12');
				$app->close();
			}
			$object = json_decode($rawdata);
			$currencyRateUSD = $object->$convertExp->val;
		}

		$depositPer = ($bctParams->deposit_per)?$bctParams->deposit_per:20;
		$loyaltyPer = ($bctParams->loyalty_per)?$bctParams->loyalty_per:1;

		if($bookingType == 'package')
		{
			$depositAmount = $elemntAmount;
		}
		else
		{
			$depositAmount = ($elemntAmount * $depositPer) / 100;
		}

		/*echo $depositAmount . " || " . $bookingType . " || " .$convertExp;
		exit;*/

		if($currencyRateUSD)
		{
			$convertedElementAmount = $elemntAmount * $currencyRateUSD;
		}
		else
		{
			$convertedElementAmount = $elemntAmount;
		}



		$loyaltyPoints = ($convertedElementAmount * $loyaltyPer) / 100;
		$loyaltyPoints = number_format($loyaltyPoints,2);

		/*echo "Currency RAte : ".$currencyRateUSD."<br />";
		echo "BC deposit Amount : " . $convertedElementAmount . "<br />";
		echo "BC Dollars : " . $loyaltyPoints . "<br />";
		exit;*/

		if($bcDollars)
		{
			$bcdToUsd = $bcDollars / 10;
			$bcdToUsd = number_format($bcdToUsd);

			$currencyRate = $this->convertCurrency('USD',$elemntCurrencyCode);
			$bcdToUsd = $bcdToUsd * $currencyRate;

			//echo "BC Dollars : " . $bcdToUsd . "<br />";exit;

			$payAfterBCD = $depositAmount - $bcdToUsd;

			$minusBCD = $bcDollars * 2;
			$minusBCD = $bcDollars - $minusBCD;

			if($payAfterBCD > 0)
			{
				$depositAmountAfertBCD = $payAfterBCD;
				$depositAmountAfertBCD = number_format($depositAmountAfertBCD,2);
				$goForPaypal = 1;
			}
			else
			{
				$goForPaypal = 0;
			}
		}

		/*echo $bcDollars . ' || ' . $depositAmountAfertBCD;
		exit;*/

		$paymentData['booked_element_id']   = $bookingID;
		$paymentData['booked_element_type'] = $bookingType;
		$paymentData['currency_code']       = $elemntCurrencyCode;
		$paymentData['currency_sign']       = $elemntCurrencySign;
		$paymentData['customer_paid']       = $elemntAmount;
		$paymentData['element_price']       = $elemntAmount;
		$paymentData['platform_income']     = ($bcDollars)?$depositAmountAfertBCD:$depositAmount;
		$paymentData['element_income']      = $elemntAmount;
		$paymentData['bcd_used']            = $bcDollars;
		$paymentData['paid_status']         = 0;
		$paymentData['time_stamp']          = time();
		$paymentData['created']             = date('Y-m-d H:i:s');

		/*echo "<pre>";
		print_r($paymentData);
		echo "</pre>";
		exit;*/

		//$orderPostData['offer_id'] = $offerID;
		$PaymentStatus->bind($paymentData);



		if(!$PaymentStatus->store())
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?success=false13');
			$app->close();

			return false;
		}



		$paymentEntryID = $PaymentStatus->payment_id;

		if($bcDollars)
		{
			if($goForPaypal == 1)
			{
				$this->addLoyaltyPoints($user->id,'Payout',$minusBCD,$paymentEntryID);
			}
			else
			{
				$this->addLoyaltyPoints($user->id,'Payout',$minusBCD,$paymentEntryID,1);
			}

		}

		$this->addLoyaltyPoints($user->id,'purchase.'.$bookingType,$loyaltyPoints,$paymentEntryID);


		$actionType         = "PAY";
		$cancelUrl          = JURI::base() . "index.php?success=false";
		$returnUrl          = JURI::base() . "index.php?success=true&payment_entry_id={$paymentEntryID}";
		//$ipnNotificationUrl = JURI::base() . 'nofity.php';
		$ipnNotificationUrl = JURI::base() . "index.php?option=com_bcted&payment_entry_id={$paymentEntryID}&task=packageorder.ipn";

		//$currencyCode       = "USD";
		//$currencyCode       = "GBP";

		if($bcDollars)
		{
			$amountToPaypal = $depositAmountAfertBCD;
		}
		else
		{
			$amountToPaypal = $depositAmount;
		}

		if($elemntCurrencyCode=="AED" && $currencyRateUSD)
		{
			$amountToPaypal = $amountToPaypal * $currencyRateUSD;
			$amountToPaypal = number_format($amountToPaypal,2);
			$elemntCurrencyCode = "USD";

			/*echo $amountToPaypal."<br />";
			echo $elemntCurrencyCode."<br />";
			exit;*/
		}

		$post_variables = Array(
			"cmd"            => "_xclick",
			"upload"         => "2",
			"business"       => $paypalEmail,
			"receiver_email" => $paypalEmail,
			"quantity"       => 1,
			"item_name"		 => $itemName,
			"item_number"    => $itemNumber,
			"no_shipping"	 => 0,
			"amount"         => $amountToPaypal,
			"return"         => JURI::base() . "index.php?success=true&payment_entry_id={$paymentEntryID}",
			"notify_url"     => JURI::base() . 'index.php?option=com_bcted&payment_entry_id='.$paymentEntryID.'&task=packageorder.ipn',
			"cancel_return"  => JURI::base() . "index.php?success=false",
			"currency_code"  => $elemntCurrencyCode
		);

		/*echo "<pre>";
		print_r($post_variables);
		echo "</pre>";
		exit;*/

		if($goForPaypal)
		{
			echo "<form action='https://www.sandbox.paypal.com/cgi-bin/webscr' method='post' name='paypalfrm' id='paypalfrm'>";
			echo "<h3>" . JText::_('You will be redirected to paypal.') . "</h3>";

			foreach ($post_variables as $name => $value)
			{
				echo "<input type='hidden' name='$name' value='$value' />";
			}

			echo '<INPUT TYPE="hidden" name="charset" value="utf-8">';
			echo "</form>";
			?>
			<script type='text/javascript'>document.paypalfrm.submit();</script>
			<?php
		}
		else
		{
			$tblPaymentStatus = JTable::getInstance('PaymentStatus', 'BctedTable',array());
			$tblPaymentStatus->load($paymentEntryID);

			if(!$tblPaymentStatus->payment_id)
			{
				$app = JFactory::getApplication();
				$app->redirect("index.php?success=false14");
				$app->close();
			}

			$elementID = $tblPaymentStatus->booked_element_id;
			$elementType = $tblPaymentStatus->booked_element_type;

			$paymentGross = $depositAmount; //$input->get('payment_gross',0.00);
			$paymentFee   = 0.00; //$input->get('payment_fee',0.00);
			$txnID        = 'paybybcd';//$input->get('txn_id','','string');
			$paymentStatusText  = $bookingType;

			$tblPaymentStatus->payment_fee = $paymentFee;
			$tblPaymentStatus->txn_id = $txnID;
			$tblPaymentStatus->payment_status = $paymentStatusText;

			if($paymentStatusText == 'Completed')
			{
				$tblPaymentStatus->paid_status = 1;

				if($elementType == 'service')
				{
					$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');
					$tblServiceBooking->load($elementID);
					$tblServiceBooking->status = 5;
					$tblServiceBooking->user_status = 5;

					$tblServiceBooking->total_price = $tblPaymentStatus->element_price;
					$tblServiceBooking->deposit_amount = $tblPaymentStatus->platform_income;
					$tblServiceBooking->amount_payable = $tblPaymentStatus->element_price - $tblPaymentStatus->platform_income;

					$tblServiceBooking->store();
				}
				else if($elementType == 'venue')
				{
					$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');
					$tblVenuebooking->load($elementID);
					$tblVenuebooking->status = 5;
					$tblVenuebooking->user_status = 5;

					$tblVenuebooking->total_price = $tblPaymentStatus->element_price;
					$tblVenuebooking->deposit_amount = $tblPaymentStatus->platform_income;
					$tblVenuebooking->amount_payable = $tblPaymentStatus->element_price - $tblPaymentStatus->platform_income;

					$tblVenuebooking->store();
				}
				else if($elementType == 'package')
				{
					$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
					$tblPackagePurchased->load($elementID);
					$tblPackagePurchased->status = 5;
					$tblPackagePurchased->user_status = 5;
					$tblPackagePurchased->store();
				}

				// Initialiase variables.
				$db    = JFactory::getDbo();
				$query = $db->getQuery(true);

				// Create the base update statement.
				$query->update($db->quoteName('#__bcted_loyalty_point'))
					->set($db->quoteName('is_valid') . ' = ' . $db->quote(1))
					->where($db->quoteName('cid') . ' = ' . $db->quote($paymentEntryID));

				// Set the query and execute the update.
				$db->setQuery($query);
				$db->execute();
			}

			$tblPaymentStatus->store();

			$app = JFactory::getApplication();
			$app->redirect("index.php?success=true&payment_entry_id={$paymentEntryID}");
			$app->close();




		}
		return true;
	}

	private function convertCurrency($from,$to)
	{
		$convertExp = $from."_".$to;

		$currencyRate = 0;

		if(!empty($convertExp))
		{
			$url = "http://www.freecurrencyconverterapi.com/api/v3/convert?q=".$convertExp."&compact=y";
			$ch = curl_init();
			$timeout = 0;
			curl_setopt ($ch, CURLOPT_URL, $url);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$rawdata = curl_exec($ch);
			curl_close($ch);
			if(empty($rawdata))
			{
			    return $currencyRate;
			}

			$object = json_decode($rawdata);
			$currencyRate = $object->$convertExp->val;
        }

        return $currencyRate;
	}

	private function addLoyaltyPoints($userID,$activity,$totalPrice,$paymentEntryID,$isValid = 0)
	{
		$tblLoyaltyPoint = JTable::getInstance('LoyaltyPoint', 'BctedTable');
		$tblLoyaltyPoint->load(0);

		$lpPost['user_id']    = $userID;
		$lpPost['earn_point'] = $totalPrice;
		$lpPost['point_app']  = $activity;
		$lpPost['cid']        = $paymentEntryID;
		$lpPost['is_valid']   = $isValid;
		$lpPost['created']    = date('Y-m-d H:i:s');
		$lpPost['time_stamp'] = time();


		$tblLoyaltyPoint->bind($lpPost);
		$tblLoyaltyPoint->store();
	}

	private function getExtensionParam()
	{
		$app    = JFactory::getApplication();

		$option = "com_bcted";
		$db     = JFactory::getDbo();

		$option = '%' . $db->escape($option, true) . '%';

		// Initialiase variables.
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->qn('#__extensions'))
			->where($db->qn('name') . ' LIKE ' . $db->q($option))
			->where($db->qn('type') . ' = ' . $db->q('component'))
			->order($db->qn('ordering') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadObject();

			$params = json_decode($result->params);
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), 500);
		}

		return $params;
	}

	/**
	 * Plugin method with the same name as the event will be called automatically.
	 *
	 * @return boolean
	 */
	function onPreparePaymentPaypalForRequest()
	{
		$jinput  = JFactory::getApplication()->input;
		$voiceID = $jinput->get('offer_id', 0);
		$tip     = $jinput->get('tip', 0);
		$restID  = $jinput->get('rest_id', 0);
		$user    = JFactory::getUser();

		if ($user->id == 0)
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?option=com_users&view=login');
			$app->close();
		}

		if ($voiceID == 0)
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php');
			$app->close();
		}

		if($restID==0)
		{
			$error = JText::_('Invalid Restaurant');
            JFactory::getApplication()->redirect(JURI::base(), $error, 'error' );
            return false;
		}

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('*')
			->from($db->quoteName('#__dfflow_voice_tmp'))
			->where($db->quoteName('voice_id') . ' = ' . $db->quote($voiceID))
			->where($db->quoteName('rest_id') . ' = ' . $db->quote($restID));

		$db->setQuery($query);

		$tmpVoice = $db->loadObject();


		require_once ("paypalplatform.php");

		$jinput  = JFactory::getApplication()->input;

		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_diff/tables');
		$tblVoice = JTable::getInstance('Voice', 'DiffTable');
		$tblVoice->load($voiceID);

		$voicePost['price']    = $tmpVoice->price;
		$voicePost['rest_ids'] = $tmpVoice->rest_id;
		$voicePost['message']  = $tmpVoice->msg;

		$tblVoice->bind($voicePost);
		$tblVoice->store();

		$tblVoice = JTable::getInstance('Voice', 'DiffTable');
		$tblVoice->load($voiceID);

		$tblRest = JTable::getInstance('Restaurant', 'DiffTable');

		$tblRest->load($restID);
		//$tblRest->load($tblVoice->rest_ids);

		if(empty($tblRest->paypal_id))
		{
			$error = JText::_('Restaurant has no paypal configuration');
            JFactory::getApplication()->redirect(JURI::base(), $error, 'error' );
            return false;
		}

		// $tblRest->paypal_id = "pratik.dasa1@tasolglobal.com";

		$price      = $tblVoice->price + $tip;

		$tblOrders = JTable::getInstance('Orders', 'DiffTable');
		$tblOrders->load(0);

		$referenceID = $tblRest->reference_id + 1;
		$refString  = substr($tblRest->name, 0, 3);
		$threeDigit = str_pad( $referenceID, 3, "0", STR_PAD_LEFT );
		$reference = strtolower($refString).$threeDigit;
		$orderPostData['reference'] = $reference;
		$restPost['reference_id'] = $referenceID;
		$tblRest->bind($restPost);
		$tblRest->store();

		$orderPostData['voice_id'] = $voiceID;
		$orderPostData['is_voice'] = 1;
		//$orderPostData['rest_id'] = $tblVoice->rest_ids;
		$orderPostData['rest_id'] = $restID;
		$orderPostData['price'] = $tblVoice->price;
		$orderPostData['income_rest'] = $tblVoice->price;
		$orderPostData['income_site'] = $tip;
		$orderPostData['payment_status'] = 'np';
		$orderPostData['time_stamp'] = time();
		//$orderPostData['offer_id'] = $offerID;
		$tblOrders->bind($orderPostData);

		if(!$tblOrders->store())
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php');
			$app->close();

			return false;
		}

		$orderID = $tblOrders->id;

		$actionType         = "PAY";
		$cancelUrl          = JURI::base() . "index.php?success=false";
		$returnUrl          = JURI::base() . "index.php?success=true&reference={$reference}";
		//$ipnNotificationUrl = JURI::base() . 'nofity.php';
		$ipnNotificationUrl = JURI::base() . 'index.php?option=com_diff&order_id='.$orderID.'&task=order.notifyoffer';

		$currencyCode       = "GBP";

		if($tip >0 )
		{
			$receiverEmailArray	= array(
				$tblRest->paypal_id,
				'Jack.hill.cc@gmail.com'
			);

			$receiverAmountArray = array(
				$price,
				$tip
			);

			$receiverPrimaryArray = array(
				true,
				false
			);

			$receiverInvoiceIdArray = array(
				'',
				'',
				'',
				'',
				'',
				''
			);

			$senderEmail					= "";
			$feesPayer						= "PRIMARYRECEIVER";
			$memo							= "";
			$pin							= "";
			$preapprovalKey = "";
			$reverseAllParallelPaymentsOnError	= "";
			$trackingId						= generateTrackingID();


			/*echo "$actionType, $cancelUrl, $returnUrl, $currencyCode, $receiverEmailArray,
							$receiverAmountArray, $receiverPrimaryArray, $receiverInvoiceIdArray,
							$feesPayer, $ipnNotificationUrl, $memo, $pin, $preapprovalKey,
							$reverseAllParallelPaymentsOnError, $senderEmail, $trackingId";
			*/

			$resArray = CallPay ($actionType, $cancelUrl, $returnUrl, $currencyCode, $receiverEmailArray,
							$receiverAmountArray, $receiverPrimaryArray, $receiverInvoiceIdArray,
							$feesPayer, $ipnNotificationUrl, $memo, $pin, $preapprovalKey,
							$reverseAllParallelPaymentsOnError, $senderEmail, $trackingId
			);
			mail("nilesh@tasolglobal.com", "Paypal Call Pay Response ".$user->username, json_encode($resArray));
			$ack = strtoupper($resArray["responseEnvelope.ack"]);
			if($ack=="SUCCESS")
			{
				if ("" == $preapprovalKey)
				{
					// redirect for web approval flow
					$cmd = "cmd=_ap-payment&paykey=" . urldecode($resArray["payKey"]);
					RedirectToPayPal ( $cmd );
				}
				else
				{
					// the Pay API call was made for an existing preapproval agreement so no approval flow follows
					// payKey is the key that you can use to identify the result from this Pay call
					$payKey = urldecode($resArray["payKey"]);
					// paymentExecStatus is the status of the payment
					$paymentExecStatus = urldecode($resArray["paymentExecStatus"]);
					// note that in order to get the exact status of the transactions resulting from
					// a Pay API call you should make the PaymentDetails API call for the payKey
				}
			}
			else
			{
				//Display a user friendly Error on the page using any of the following error information returned by PayPal
				//TODO - There can be more than 1 error, so check for "error(1).errorId", then "error(2).errorId", and so on until you find no more errors.
				$ErrorCode = urldecode($resArray["error(0).errorId"]);
				$ErrorMsg = urldecode($resArray["error(0).message"]);
				$ErrorDomain = urldecode($resArray["error(0).domain"]);
				$ErrorSeverity = urldecode($resArray["error(0).severity"]);
				$ErrorCategory = urldecode($resArray["error(0).category"]);

				echo "Pay API call failed. ";
				echo "Detailed Error Message: " . $ErrorMsg;
				echo "Error Code: " . $ErrorCode;
				echo "Error Severity: " . $ErrorSeverity;
				echo "Error Domain: " . $ErrorDomain;
				echo "Error Category: " . $ErrorCategory;
			}
		}
		else
		{
			$post_variables = Array(
				"cmd"            => "_xclick",
				"upload"         => "2",
				"business"       => $tblRest->paypal_id,
				"receiver_email" => $tblRest->paypal_id,
				"quantity"       => 1,
				"item_name"		 => $orderID,
				"item_number"    => "Voice Offer",
				"no_shipping"	 => 0,
				"amount"         => $price,
				"return"         => JURI::base() . "index.php?success=true&reference={$reference}",
				"notify_url"     => JURI::base() . 'index.php?option=com_diff&order_id='.$orderID.'&task=order.notifyofferNotTip',
				"cancel_return"  => JURI::base() . "index.php?success=false",
				"currency_code"  => "GBP"
			);

			/*$cancelUrl          = JURI::base() . "index.php?success=false";
			$returnUrl          = JURI::base() . "index.php?success=true&reference={$reference}";*/

			echo "<form action='https://www.paypal.com/cgi-bin/webscr' method='post' name='paypalfrm' id='paypalfrm'>";
			echo "<h3>" . JText::_('You will be redirected to paypal.') . "</h3>";

			foreach ($post_variables as $name => $value)
			{
				echo "<input type='hidden' name='$name' value='$value' />";
			}

			echo '<INPUT TYPE="hidden" name="charset" value="utf-8">';
			echo "</form>";
			?>
			<script type='text/javascript'>document.paypalfrm.submit();</script>
			<?php
		}


		return true;
	}
}
