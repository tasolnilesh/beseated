<?php

/**
 * @package     Pass.Plugin
 * @subpackage  com_pass
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Pass PaymentsPaypal plugin
 *
 * @since  0.0.1
 */
class plgBctPaypal extends JPlugin
{
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  0.0.1
	 */
	protected $autoloadLanguage = true;

	/**
	 * Plugin method with the same name as the event will be called automatically.
	 *
	 * @return boolean
	 */
	function onPreparePaymentPaypal()
	{
		require_once ("paypalplatform.php");

		$jinput  = JFactory::getApplication()->input;

		$bookingID = $jinput->get('booking_id', 0, 'int');
		$bookingType = $jinput->get('booking_type', 'package', 'string');

		$elemntAmount = 0.00;

		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_diff/tables');

		$user    = JFactory::getUser();

		if ($user->id == 0)
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?option=com_users&view=login');
			$app->close();
		}

		if ($bookingID == 0)
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php');
			$app->close();
		}

		if($bookingType == 'package' || $bookingType == 'Package')
		{
			$bookingType = "package";
			$tblBooking = JTable::getInstance('PackagePurchased', 'BctedTable');
			$tblPackage = JTable::getInstance('Package', 'BctedTable');

			$tblBooking->load($bookingID);

			if(!$tblBooking->package_purchase_id)
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php');
				$app->close();
			}

			$tblPackage->load($tblBooking->package_id);

			if(!$tblPackage->package_id || $tblPackage->package_price == 0)
			{
				$app = JFactory::getApplication();
				$app->redirect('index.php');
				$app->close();
			}

			$elemntAmount = $tblPackage->package_price;
		}

		/*if($siteTip!=0)
		{
			if($siteTip<=100)
			{
				$tip = $siteTip * $tblOffer->price/100;
			}
		}*/

		/*$tblRest = JTable::getInstance('Restaurant', 'DiffTable');
		$tblRest->load($tblOffer->rest_id);*/

		/*if(empty($tblRest->paypal_id))
		{*/
			/*$app = JFactory::getApplication();
			$app->redirect('index.php', true);
			$app->close();*/

			/*$error = JText::_('Restaurant has no paypal configuration');
            JFactory::getApplication()->redirect(JURI::base(), $error, 'error' );
            return false;
		}*/

		//$tblRest->paypal_id = "pratik.dasa1@tasolglobal.com";

		/*$tblDish = JTable::getInstance('Dish', 'DiffTable');
		$tblDish->load($tblOffer->dish_id);

		$price      = $tblOffer->price + $tip;*/

		$PaymentStatus = JTable::getInstance('PaymentStatus', 'BctedTable');
		$PaymentStatus->load(0);

		/*$referenceID = $tblRest->reference_id + 1;
		$refString  = substr($tblRest->name, 0, 3);
		$threeDigit = str_pad( $referenceID, 3, "0", STR_PAD_LEFT );
		$reference = strtolower($refString).$threeDigit;
		$orderPostData['reference'] = $reference;
		$restPost['reference_id'] = $referenceID;
		$tblRest->bind($restPost);
		$tblRest->store();*/



		$paymentData['booked_element_id'] = $bookingID;
		$paymentData['bookingType']       = $bookingType;
		$paymentData['customer_paid']     = $elemntAmount;
		$paymentData['element_price']     = $elemntAmount;
		$paymentData['platform_income']   = 0.00;
		$paymentData['element_income']    = $elemntAmount;
		$paymentData['paid_status']       = 0;
		$paymentData['time_stamp']        = time();
		$paymentData['created']           = date('Y-m-d H:i:s');

		//$orderPostData['offer_id'] = $offerID;
		$PaymentStatus->bind($paymentData);

		if(!$PaymentStatus->store())
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php');
			$app->close();

			return false;
		}

		$paymentEntryID = $PaymentStatus->payment_id;

		$actionType         = "PAY";
		$cancelUrl          = JURI::base() . "index.php?success=false";
		$returnUrl          = JURI::base() . "index.php?success=true&reference={$reference}";
		//$ipnNotificationUrl = JURI::base() . 'nofity.php';
		$ipnNotificationUrl = JURI::base() . 'index.php?option=com_diff&order_id='.$orderID.'&task=order.notifyoffer';

		//$currencyCode       = "USD";
		$currencyCode       = "GBP";

		if($tip > 0)
		{
			$receiverEmailArray	= array(
				$tblRest->paypal_id,
				'Jack.hill.cc@gmail.com'
			);

			$receiverAmountArray = array(
				$price,
				$tip
			);

			$receiverPrimaryArray = array(
				true,
				false
			);

			$receiverInvoiceIdArray = array(
				'',
				'',
				'',
				'',
				'',
				''
			);

			$senderEmail					= "";
			$feesPayer						= "PRIMARYRECEIVER";
			$memo							= "";
			$pin							= "";
			$preapprovalKey = "";
			$reverseAllParallelPaymentsOnError	= "";
			$trackingId						= generateTrackingID();


			/*echo "$actionType, $cancelUrl, $returnUrl, $currencyCode, $receiverEmailArray,
							$receiverAmountArray, $receiverPrimaryArray, $receiverInvoiceIdArray,
							$feesPayer, $ipnNotificationUrl, $memo, $pin, $preapprovalKey,
							$reverseAllParallelPaymentsOnError, $senderEmail, $trackingId";
			*/

			$resArray = CallPay ($actionType, $cancelUrl, $returnUrl, $currencyCode, $receiverEmailArray,
							$receiverAmountArray, $receiverPrimaryArray, $receiverInvoiceIdArray,
							$feesPayer, $ipnNotificationUrl, $memo, $pin, $preapprovalKey,
							$reverseAllParallelPaymentsOnError, $senderEmail, $trackingId
			);

			$ack = strtoupper($resArray["responseEnvelope.ack"]);
			if($ack=="SUCCESS")
			{
				if ("" == $preapprovalKey)
				{
					// redirect for web approval flow
					$cmd = "cmd=_ap-payment&paykey=" . urldecode($resArray["payKey"]);
					RedirectToPayPal ( $cmd );
				}
				else
				{
					// the Pay API call was made for an existing preapproval agreement so no approval flow follows
					// payKey is the key that you can use to identify the result from this Pay call
					$payKey = urldecode($resArray["payKey"]);
					// paymentExecStatus is the status of the payment
					$paymentExecStatus = urldecode($resArray["paymentExecStatus"]);
					// note that in order to get the exact status of the transactions resulting from
					// a Pay API call you should make the PaymentDetails API call for the payKey
				}
			}
			else
			{
				//Display a user friendly Error on the page using any of the following error information returned by PayPal
				//TODO - There can be more than 1 error, so check for "error(1).errorId", then "error(2).errorId", and so on until you find no more errors.
				$ErrorCode = urldecode($resArray["error(0).errorId"]);
				$ErrorMsg = urldecode($resArray["error(0).message"]);
				$ErrorDomain = urldecode($resArray["error(0).domain"]);
				$ErrorSeverity = urldecode($resArray["error(0).severity"]);
				$ErrorCategory = urldecode($resArray["error(0).category"]);

				echo "Pay API call failed. ";
				echo "Detailed Error Message: " . $ErrorMsg;
				echo "Error Code: " . $ErrorCode;
				echo "Error Severity: " . $ErrorSeverity;
				echo "Error Domain: " . $ErrorDomain;
				echo "Error Category: " . $ErrorCategory;
			}
		}
		else
		{
			$post_variables = Array(
				"cmd"            => "_xclick",
				"upload"         => "2",
				"business"       => $tblRest->paypal_id,
				"receiver_email" => $tblRest->paypal_id,
				"quantity"       => 1,
				"item_name"		 => $orderID,
				"item_number"    => $tblDish->name,
				"no_shipping"	 => 0,
				"amount"         => $price,
				"return"         => JURI::base() . "index.php?success=true&reference={$reference}",
				"notify_url"     => JURI::base() . 'index.php?option=com_diff&order_id='.$orderID.'&task=order.notifyofferNotTip',
				"cancel_return"  => JURI::base() . "index.php?success=false",
				"currency_code"  => "GBP"
			);

			echo "<form action='https://www.paypal.com/cgi-bin/webscr' method='post' name='paypalfrm' id='paypalfrm'>";
			echo "<h3>" . JText::_('You will be redirected to paypal.') . "</h3>";

			foreach ($post_variables as $name => $value)
			{
				echo "<input type='hidden' name='$name' value='$value' />";
			}

			echo '<INPUT TYPE="hidden" name="charset" value="utf-8">';
			echo "</form>";
			?>
			<script type='text/javascript'>document.paypalfrm.submit();</script>
			<?php
		}



		return true;
	}

	/**
	 * Plugin method with the same name as the event will be called automatically.
	 *
	 * @return boolean
	 */
	function onPreparePaymentPaypalForRequest()
	{
		$jinput  = JFactory::getApplication()->input;
		$voiceID = $jinput->get('offer_id', 0);
		$tip     = $jinput->get('tip', 0);
		$restID  = $jinput->get('rest_id', 0);
		$user    = JFactory::getUser();

		if ($user->id == 0)
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?option=com_users&view=login');
			$app->close();
		}

		if ($voiceID == 0)
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php');
			$app->close();
		}

		if($restID==0)
		{
			$error = JText::_('Invalid Restaurant');
            JFactory::getApplication()->redirect(JURI::base(), $error, 'error' );
            return false;
		}

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('*')
			->from($db->quoteName('#__dfflow_voice_tmp'))
			->where($db->quoteName('voice_id') . ' = ' . $db->quote($voiceID))
			->where($db->quoteName('rest_id') . ' = ' . $db->quote($restID));

		$db->setQuery($query);

		$tmpVoice = $db->loadObject();


		require_once ("paypalplatform.php");

		$jinput  = JFactory::getApplication()->input;

		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_diff/tables');
		$tblVoice = JTable::getInstance('Voice', 'DiffTable');
		$tblVoice->load($voiceID);

		$voicePost['price']    = $tmpVoice->price;
		$voicePost['rest_ids'] = $tmpVoice->rest_id;
		$voicePost['message']  = $tmpVoice->msg;

		$tblVoice->bind($voicePost);
		$tblVoice->store();

		$tblVoice = JTable::getInstance('Voice', 'DiffTable');
		$tblVoice->load($voiceID);

		$tblRest = JTable::getInstance('Restaurant', 'DiffTable');

		$tblRest->load($restID);
		//$tblRest->load($tblVoice->rest_ids);

		if(empty($tblRest->paypal_id))
		{
			$error = JText::_('Restaurant has no paypal configuration');
            JFactory::getApplication()->redirect(JURI::base(), $error, 'error' );
            return false;
		}

		// $tblRest->paypal_id = "pratik.dasa1@tasolglobal.com";

		$price      = $tblVoice->price + $tip;

		$tblOrders = JTable::getInstance('Orders', 'DiffTable');
		$tblOrders->load(0);

		$referenceID = $tblRest->reference_id + 1;
		$refString  = substr($tblRest->name, 0, 3);
		$threeDigit = str_pad( $referenceID, 3, "0", STR_PAD_LEFT );
		$reference = strtolower($refString).$threeDigit;
		$orderPostData['reference'] = $reference;
		$restPost['reference_id'] = $referenceID;
		$tblRest->bind($restPost);
		$tblRest->store();

		$orderPostData['voice_id'] = $voiceID;
		$orderPostData['is_voice'] = 1;
		//$orderPostData['rest_id'] = $tblVoice->rest_ids;
		$orderPostData['rest_id'] = $restID;
		$orderPostData['price'] = $tblVoice->price;
		$orderPostData['income_rest'] = $tblVoice->price;
		$orderPostData['income_site'] = $tip;
		$orderPostData['payment_status'] = 'np';
		$orderPostData['time_stamp'] = time();
		//$orderPostData['offer_id'] = $offerID;
		$tblOrders->bind($orderPostData);

		if(!$tblOrders->store())
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php');
			$app->close();

			return false;
		}

		$orderID = $tblOrders->id;

		$actionType         = "PAY";
		$cancelUrl          = JURI::base() . "index.php?success=false";
		$returnUrl          = JURI::base() . "index.php?success=true&reference={$reference}";
		//$ipnNotificationUrl = JURI::base() . 'nofity.php';
		$ipnNotificationUrl = JURI::base() . 'index.php?option=com_diff&order_id='.$orderID.'&task=order.notifyoffer';

		$currencyCode       = "GBP";

		if($tip >0 )
		{
			$receiverEmailArray	= array(
				$tblRest->paypal_id,
				'Jack.hill.cc@gmail.com'
			);

			$receiverAmountArray = array(
				$price,
				$tip
			);

			$receiverPrimaryArray = array(
				true,
				false
			);

			$receiverInvoiceIdArray = array(
				'',
				'',
				'',
				'',
				'',
				''
			);

			$senderEmail					= "";
			$feesPayer						= "PRIMARYRECEIVER";
			$memo							= "";
			$pin							= "";
			$preapprovalKey = "";
			$reverseAllParallelPaymentsOnError	= "";
			$trackingId						= generateTrackingID();


			/*echo "$actionType, $cancelUrl, $returnUrl, $currencyCode, $receiverEmailArray,
							$receiverAmountArray, $receiverPrimaryArray, $receiverInvoiceIdArray,
							$feesPayer, $ipnNotificationUrl, $memo, $pin, $preapprovalKey,
							$reverseAllParallelPaymentsOnError, $senderEmail, $trackingId";
			*/

			$resArray = CallPay ($actionType, $cancelUrl, $returnUrl, $currencyCode, $receiverEmailArray,
							$receiverAmountArray, $receiverPrimaryArray, $receiverInvoiceIdArray,
							$feesPayer, $ipnNotificationUrl, $memo, $pin, $preapprovalKey,
							$reverseAllParallelPaymentsOnError, $senderEmail, $trackingId
			);
			mail("nilesh@tasolglobal.com", "Paypal Call Pay Response ".$user->username, json_encode($resArray));
			$ack = strtoupper($resArray["responseEnvelope.ack"]);
			if($ack=="SUCCESS")
			{
				if ("" == $preapprovalKey)
				{
					// redirect for web approval flow
					$cmd = "cmd=_ap-payment&paykey=" . urldecode($resArray["payKey"]);
					RedirectToPayPal ( $cmd );
				}
				else
				{
					// the Pay API call was made for an existing preapproval agreement so no approval flow follows
					// payKey is the key that you can use to identify the result from this Pay call
					$payKey = urldecode($resArray["payKey"]);
					// paymentExecStatus is the status of the payment
					$paymentExecStatus = urldecode($resArray["paymentExecStatus"]);
					// note that in order to get the exact status of the transactions resulting from
					// a Pay API call you should make the PaymentDetails API call for the payKey
				}
			}
			else
			{
				//Display a user friendly Error on the page using any of the following error information returned by PayPal
				//TODO - There can be more than 1 error, so check for "error(1).errorId", then "error(2).errorId", and so on until you find no more errors.
				$ErrorCode = urldecode($resArray["error(0).errorId"]);
				$ErrorMsg = urldecode($resArray["error(0).message"]);
				$ErrorDomain = urldecode($resArray["error(0).domain"]);
				$ErrorSeverity = urldecode($resArray["error(0).severity"]);
				$ErrorCategory = urldecode($resArray["error(0).category"]);

				echo "Pay API call failed. ";
				echo "Detailed Error Message: " . $ErrorMsg;
				echo "Error Code: " . $ErrorCode;
				echo "Error Severity: " . $ErrorSeverity;
				echo "Error Domain: " . $ErrorDomain;
				echo "Error Category: " . $ErrorCategory;
			}
		}
		else
		{
			$post_variables = Array(
				"cmd"            => "_xclick",
				"upload"         => "2",
				"business"       => $tblRest->paypal_id,
				"receiver_email" => $tblRest->paypal_id,
				"quantity"       => 1,
				"item_name"		 => $orderID,
				"item_number"    => "Voice Offer",
				"no_shipping"	 => 0,
				"amount"         => $price,
				"return"         => JURI::base() . "index.php?success=true&reference={$reference}",
				"notify_url"     => JURI::base() . 'index.php?option=com_diff&order_id='.$orderID.'&task=order.notifyofferNotTip',
				"cancel_return"  => JURI::base() . "index.php?success=false",
				"currency_code"  => "GBP"
			);

			/*$cancelUrl          = JURI::base() . "index.php?success=false";
			$returnUrl          = JURI::base() . "index.php?success=true&reference={$reference}";*/

			echo "<form action='https://www.paypal.com/cgi-bin/webscr' method='post' name='paypalfrm' id='paypalfrm'>";
			echo "<h3>" . JText::_('You will be redirected to paypal.') . "</h3>";

			foreach ($post_variables as $name => $value)
			{
				echo "<input type='hidden' name='$name' value='$value' />";
			}

			echo '<INPUT TYPE="hidden" name="charset" value="utf-8">';
			echo "</form>";
			?>
			<script type='text/javascript'>document.paypalfrm.submit();</script>
			<?php
		}


		return true;
	}
}
