<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');
$loginUser = JFactory::getUser();
?>
<style type="text/css">
.msg-action {
	float: left;
	width: 100%;
	margin-left: 17px;
	margin-bottom: 10px;
}
</style>
<script type="text/javascript">
	function acceptAddMeRequest(inviteID,actionBy,actionType)
	{
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=message.action_addme_at_venue_table',
			type: 'GET',
			data: 'invite_id='+inviteID+'&action_by='+actionBy+'&action_type='+actionType,

			success: function(response){

				if(response == "200")
				{
					//jQuery('#booking_'+bookingID).remove();
					location.reload();
				}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});
	}
</script>
<div class="table-wrp">
	<div class="req-detailwrp msg-detailwrp">
		<h2>Messages to <?php echo $this->otherUser->name; ?></h2>
		<div class="message-list">
			<ul>
				<?php foreach ($this->messages as $key => $message): //echo "<pre>";	print_r($message); echo "</pre>"; ?>
				<li>
					<?php if($message->to_userid == $loginUser->id && $message->message_type == "tableaddme"): ?>
						<?php $params = json_decode($message->extra_params); ?>
						<?php $invitationDetail = BctedHelper::getVenueBookingInvite($params->inviteID); ?>
						<?php /*echo "<pre>";
						print_r($invitationDetail);
						echo "</pre>";
						exit;*/ ?>
						<?php $bookingDetail = BctedHelper::getVenueBookingDetail($invitationDetail->booking_id); ?>
						<?php $bookingDateTime = $bookingDetail->venue_booking_datetime.' '.$bookingDetail->booking_from_time; ?>
						<?php $bookingTimeStamp = strtotime($bookingDateTime); ?>
						<?php $currentTimeStamp = time(); ?>
						<?php if($currentTimeStamp < $bookingTimeStamp && empty($invitationDetail->user_action)): ?>
							<div class="msg-dscr"><a href="#"><?php echo $message->message;?></a></div>
							<div class="msg-action">
								<div class="span4"><button class="cancel-btn" type="button"></button> </div>
								<div class="span4"><button onclick="acceptAddMeRequest('<?php echo $params->inviteID; ?>','user','accept')" class="ok-btn pull-right" type="button"></button> </div>
							</div>
						<?php elseif($currentTimeStamp < $bookingTimeStamp && empty($invitationDetail->venue_action)): ?>
							<div class="msg-dscr"><a href="#"><?php echo $message->message;?></a></div>
							<div class="msg-action">
								<div class="span4"><button class="cancel-btn" type="button"></button> </div>
								<div class="span4"><button onclick="acceptAddMeRequest('<?php echo $params->inviteID; ?>','venue','accept')" class="ok-btn pull-right" type="button"></button> </div>
							</div>
						<?php else: ?>
							<div class="msg-dscr"><?php echo $message->message;?></div>
						<?php endif; ?>
					<?php else: ?>
						<div class="msg-dscr"><?php echo $message->message;?></div>
					<?php endif; ?>
					<?php $date = date('d-m-Y H:i',($message->time_stamp + $this->offset)); ?>
					<div class="msg-datetime"><?php echo $date;?></div>
				</li>
				<?php endforeach; ?>

			</ul>
		</div>
		<?php if($this->userType != 'Registered'): ?>
			<?php if($this->userProfile->licence_type != "basic"): ?>
				<div class="row-fluid msg-post-wrp">
					<div class="span6">
						<input type="text" id="message" name="message" class="span" placeholder="Enter message here">
						<input type="hidden" name="other_user_id" id="other_user_id" value="<?php echo $this->otherUser->id; ?>">
						<input type="hidden" name="connectionID" id="connectionID" value="<?php echo $this->connectionID; ?>">
					</div>
					<div class="span6">
						<button id="send_mail" class="send-btn" ></button>
					</div>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>
<?php echo $this->pagination->getListFooter(); ?>
<script type="text/javascript">
	jQuery("#send_mail").click(function() {
		var msg = jQuery('#message').val();
		msg = jQuery.trim(msg);

		if(msg.length == 0)
		{
			return 0;
		}

		var otherUserID = jQuery('#other_user_id').val();
		var connectionID = jQuery('#connectionID').val();

		// Ajax call to send message....
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=message.send_message',
			type: 'GET',
			data: 'connection_id='+connectionID+'&other_user_id='+otherUserID+'&message='+msg,

			success: function(response){

				if(response == "200")
				{
					//jQuery('#booking_'+bookingID).remove();
					location.reload();
				}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});


	});
</script>


