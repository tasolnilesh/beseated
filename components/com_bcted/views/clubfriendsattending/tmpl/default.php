<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

/*

venue_booking_id
name
premium_table_id
venue_name
 */
?>
<script type="text/javascript">
	function setValueInModel(bookingID,userName,tableName,venueName)
	{
		jQuery('#booking_id').val(bookingID);
		jQuery('#model-body-text').html('Send a request to '+ userName+'to add you to their '+tableName+' table at '+venueName+' venue.');
	}

	function sendAddMeRequest()
	{
		var booking_id = jQuery('#booking_id').val();
		var msg_for_addme = jQuery('#msg_for_addme').val();

		//alert(elementID + " || " + newRate);

		// Ajax call to send message....
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=clubfriendsattending.addMeForVenueTable',
			type: 'GET',
			data: 'booking_id='+booking_id+'&message='+msg_for_addme,

			success: function(response){

				if(response == "200")
				{
					//jQuery('#booking_'+bookingID).remove();
					//location.reload();
				}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});
	}
</script>
<div class="table-wrp">
	<h2>Friends Attending</h2>
	<div class="frnd-wrp row-fluid">
		<?php foreach ($this->items as $key => $item):  //echo "<pre>"; print_r($item); echo "</pre>"; ?>
			<?php $fbImage = "http://graph.facebook.com/".$item->fbid."/picture" ;?>
			<div class="span6">
				<div class="media">
					<a class="pull-left" href="#">
						<img class="media-object" data-src="<?php echo $fbImage; ?>" src="<?php echo $fbImage; ?>">
					</a>
					<div class="media-body">
						<h4 class="media-heading"><?php echo $item->name; ?></h4>
						<h5><?php echo ($item->venue_table_name)?$item->venue_table_name:$item->custom_table_name; ?></h5>
						<h6><?php echo date('d-m-Y',strtotime($item->venue_booking_datetime)); ?></h6>
					</div>

					<a href="#myModal" role="button" onclick="setValueInModel('<?php echo $item->venue_booking_id; ?>','<?php echo $item->name; ?>','<?php echo ($item->premium_table_id)?$item->venue_table_name:$item->custom_table_name; ?>','<?php echo $item->venue_name; ?>');" class="frnd-actn-btn pull-right" data-toggle="modal"></a>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Send Add me Request</h3>
	</div>
	<div class="modal-body">
		<p id="model-body-text">Send a request to name to add you to their table booking at venue.</p>
		<input type="hidden" name="booking_id" id="booking_id" value="0">
		<textarea class="add-me-venue-table" id="msg_for_addme"></textarea>
	</div>
	<div class="modal-footer">
		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
		<button class="btn btn-primary" onclick="sendAddMeRequest()">Send Request</button>
	</div>
</div>
