<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Friends Attending View
 *
 * @since  0.0.1
 */
class BctedViewClubFriendsAttending extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $user;

	protected $clubID;
	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		$this->user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$this->clubID = $input->get('club_id',0,'int');

		if(!$this->user->id)
        {
        	$app = JFactory::getApplication();
			$menu = $app->getMenu();
			$menuItem = $menu->getItems( 'link', 'index.php?option=com_users&view=login', true );

			$bctParams = BctedHelper::getExtensionParam_2();
			$accessLevel = BctedHelper::getGroupAccessLevel($bctParams->guest);

			$access = array('access','link');
			$property = array($accessLevel,'index.php?option=com_bcted&view=clubfriendsattending');
			$menuItem2 = $menu->getItems( $access, $property, true );


			$link2 = 'index.php?option=com_bcted&view=clubfriendsattending&club_id='.$this->clubID.'&Itemid='.$menuItem2->id;


            JFactory::getApplication()->redirect(JRoute::_(JURI::root().'index.php?option=com_users&view=login&Itemid='.$menuItem->id.'&return='.base64_encode($link2)), 'Please login first!');
        }

		// Get data from the model
		$this->items         = $this->get('MyFbFriendID');

		/*echo "<pre>";
		print_r($this->items);
		echo "</pre>";
		exit;*/
		/*$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');*/

		//$this->filterForm    = $this->get('FilterForm');
		//$this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Set the tool-bar and number of found items
		//$this->addToolBar();
		//$this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}

}
