<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$config = BctedHelper::getExtensionParam_2();
/*echo "<pre>";
print_r($config);
echo "</pre>";
exit;
stdClass Object
(
    [merchantId] => 43269
    [encryptionKey] => 23D39F9BCB8D07538E81F375479AAB37
    [accessCode] => AVCD02CE26AF53DCFA
    [guest] => 14
    [club] => 11
    [service_provider] => 10
    [deposit_per] => 20
    [loyalty_per] => 1
    [friends_referral] => 50
    [loyalty_level_lowest_min] => 1
    [loyalty_level_lowest_max] => 100000
    [loyalty_level_lowest_color] => #000000
    [loyalty_level_middle_min] => 100001
    [loyalty_level_middle_max] => 1000000
    [loyalty_level_middle_color] => #fbb829
    [loyalty_level_highest] => 1000000
    [loyalty_level_highest_color] => #ffffff
)*/
$myTotalLoyalty = $this->loyaltyPoints;

$colorCode = "";

//$myTotalLoyalty = 1000001;

if($myTotalLoyalty > $config->loyalty_level_highest)
{
    $colorCode = $config->loyalty_level_highest_color;
}
else if($myTotalLoyalty >= $config->loyalty_level_middle_min && $myTotalLoyalty <= $config->loyalty_level_middle_max)
{
    $colorCode = $config->loyalty_level_middle_color;
}
else if($myTotalLoyalty <= $config->loyalty_level_lowest_max)
{
    $colorCode = $config->loyalty_level_lowest_color;
}
?>
<div class="table-wrp">
    <h1>My Loyalty Points : <span style="color:<?php echo $colorCode; ?>"><?php echo number_format($this->loyaltyPoints,2); ?></span></h1>
    <div class="acnt-histry-tbl loyalty-tbl">
        <table class="activity" id="accordion">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Reference</th>
                    <th>Points</th>
                    <th>App</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($this->loyaltyHistory as $key => $loyalty): ?>
                    <tr>
                        <td><?php echo date('d-m-Y',strtotime($loyalty->created)); ?></td>
                        <td><?php echo $loyalty->lp_id; ?></td>
                        <td><?php echo number_format($loyalty->earn_point,2); ?></td>
                        <td>
                            <?php if($loyalty->point_app == 'purchase.package')
                                echo "Purchase";
                            else if($loyalty->point_app == 'purchase.venue')
                                echo "Purchase";
                            else if($loyalty->point_app == 'purchase.service')
                                echo "Purchase";
                            else if($loyalty->point_app == 'Payout')
                                echo "Payout";
                            else if($loyalty->point_app == 'Referral')
                                echo "Referral";
                            else if($loyalty->point_app == 'purchase.packageinvitation')
                               echo "Purchase";
                            else if($loyalty->point_app == 'admin.added')
                                echo "Admin";
                            else if($loyalty->point_app == 'admin.removed')
                                echo "Admin";
                            else if($loyalty->point_app == 'venue.cancelbooking')
                                echo "Cancelled";
                            else if($loyalty->point_app == 'service.cancelbooking')
                                echo "Cancelled";
                            else if($loyalty->point_app == 'venue.noshow')
                                echo "Deduction";
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="4"><?php echo $this->pagination->getListFooter(); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>


