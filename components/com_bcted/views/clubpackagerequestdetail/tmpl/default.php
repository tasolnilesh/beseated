<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

/*echo "<pre>";
print_r($this->booking);
echo "</pre>";*/

// /http://192.168.5.31/development/thebeseated/index.php?option=com_bcted&view=clubpackagerequestdetail&request_id=39&Itemid=127

?>
<script type="text/javascript">
function changeRequestStatus(status)
{
	var owner_message = jQuery('#owner_message').val();
	jQuery.ajax({
		type: "GET",
		url: "index.php?option=com_bcted&task=clubpackagerequestdetail.changeRequstStatus",
		data: "request_id=<?php echo $this->booking->package_purchase_id; ?>&status="+status+"&owner_message="+owner_message,
		success: function(response){
			if(response == "1")
				{
					//jQuery('#booking_'+bookingID).remove();
					location.reload();
				}
			}
    });
}
</script>

<div class="table-wrp">
<h1>Request Detail</h1>
    <div class="req-detailwrp">
    <?php //echo "<pre>";print_r($this->booking); echo "</pre>"; //exit; ?>
    	<h2> <?php echo ucfirst($this->booking->name); ?>  has requested the following booking:</h2>
        <h4>
            <?php echo "<b>Package Name : </b>".ucfirst($this->booking->package_name); ?>
        </h4>
        <h4><?php echo "<b>Date : </b>" . date('d-m-Y',strtotime($this->booking->package_datetime)); ?></h4>
        <?php
            $packageTime = explode(":", $this->booking->package_time);
        ?>
        <h4><?php echo "<b>Time : </b>" . $packageTime[0].":".$packageTime[1]; ?></h4>
        <h4><b>Number of Guests : </b><?php echo $this->booking->package_number_of_guest .'('.$this->booking->male_count .'M/'.$this->booking->female_count.'F)'; ?></h4>
        <h4><?php echo ucfirst($this->booking->package_additional_info); ?></h4>
        <textarea id="owner_message" rows="7" placeholder="Enter message here"></textarea>
        <div class="row-fluid req-btnwrp">
        	<div class="span4"><button onclick="changeRequestStatus('cancel')" class="cancel-btn" type="button"></button> </div>
            <!-- <div class="span4"><button onclick="changeRequestStatus('waiting')" class="wait-btn" type="button"></button> </div> -->
            <div class="span4"><button onclick="changeRequestStatus('ok')" class="ok-btn pull-right" type="button"></button> </div>

        </div>
    </div>
</div>
<?php //echo $this->pagination->getListFooter(); ?>
<!--

Requst Detail

stdClass Object
(
    [venue_booking_id] => 8
    [venue_id] => 4
    [venue_table_id] => 8
    [user_id] => 142
    [venue_booking_table_privacy] => 0
    [venue_booking_datetime] => 24-03-2015
    [booking_from_time] => 14:30:00
    [booking_to_time] => 14:30:00
    [venue_booking_number_of_guest] => 4
    [male_count] => 2
    [female_count] => 2
    [venue_booking_additional_info] => this is booking for 24 March 2015.
    [total_price] => 0.00
    [deposit_amount] => 0.00
    [amount_payable] => 0.00
    [owner_message] =>
    [status] => 1
    [user_status] => 2
    [is_rated] => 0
    [is_deleted] => 0
    [venue_booking_created] => 2015-03-18 17:28:10
    [time_stamp] => 1426679890
    [venue_table_name] => Gold Table
    [custom_table_name] => aaa
    [venue_table_image] => images/bcted/table/147/5a3798d32a5d078bba89b1d9.jpg
    [venue_table_price] => 1000.00
    [venue_table_capacity] => 8
    [venue_table_description] => This gold table is perfect for an awesome night out
    [status_text] => Request
    [user_status_text] => Pending
    [venue_name] => Crystal
    [venue_address] => United Arab Emirates
    [venue_about] => This all about crystal club added in by Roger
    [venue_amenities] =>
    [venue_signs] => 1
    [venue_rating] => 3.00
    [venue_timings] => 05:35:00
    [venue_image] => images/bcted/venue/147/c334b4ee3d9f98b671649bf0.jpg
    [is_smart] => 1
    [is_casual] => 0
    [is_food] => 0
    [is_drink] => 1
    [working_days] => 1,6,7
    [is_smoking] => 1
    [name] => guest1 Firstname
    [last_name] => guest1
    [phoneno] => +919033000000
)
-->