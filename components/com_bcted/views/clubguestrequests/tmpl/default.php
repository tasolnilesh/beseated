<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

/*echo "<pre>";
print_r($this->bookings);
echo "</pre>";*/

?>
<script type="text/javascript">
function deleteRequest(requestID)
{

	jQuery.ajax({
		type: "GET",
		url: "index.php?option=com_bcted&task=ajax.calculateExp",
		data: "&discount="+disocunt+"&auction_length="+auctionLength+"&batch_id="+batchID,
		success: function(data){

			jQuery('#min_accepted_bid').html(data);
			jQuery('#minimum_accepted_bid').val(data);
			/*if (!html.trim()) {
				jQuery('#upload_next').hide();
			}
			else
			{
				jQuery('#upload_next').show();
			}
			jQuery("#invoice_container").html(html);*/

			//console.log(data);
		}
    });

}
</script>
<div class="bct-summary-container">
	<div class="summary-list guest-list">

		<table class="table">
			<thead>
                <tr>
                    <th>Date</th>
                    <th>Guestlist Count</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            	<?php $hasRequest = 0; ?>
            	<?php if($this->bookings['requestToday']): ?>
            		<?php $hasRequest = 1; ?>
					<?php foreach ($this->bookings['requestToday'] as $key => $booking): ?>
						<tr>
							<td><?php echo "Today"; ?></td>
							<td><?php echo $booking['listCount']." Guests"; ?></td>
							<td><a href="index.php?option=com_bcted&view=ClubGuestRequestsByDate&guestlist_date=<?php echo $booking['date']; ?>&Itemid=<?php echo $Itemid; ?>"><button class="btn btn-primary ">Detail</button></a></td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>

				<?php if($this->bookings['requestFuture']): ?>
					<?php $hasRequest = 1; ?>
					<?php foreach ($this->bookings['requestFuture'] as $key => $booking): ?>
						<tr>
							<td><?php echo date('d-m-Y',strtotime($booking['date'])); ?></td>
							<td><?php echo $booking['listCount']." Guests"; ?></td>
							<td><a href="index.php?option=com_bcted&view=ClubGuestRequestsByDate&guestlist_date=<?php echo $booking['date']; ?>&Itemid=<?php echo $Itemid; ?>"><button class="btn btn-primary">Detail</button></a></td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>

				<?php if($this->bookings['requestPast']): ?>
					<?php $hasRequest = 1; ?>
					<?php foreach ($this->bookings['requestPast'] as $key => $booking): ?>
						<tr>
							<td><?php echo date('d-m-Y',strtotime($booking['date'])); ?></td>
							<td><?php echo $booking['listCount']." Guests"; ?></td>
							<td><a href="index.php?option=com_bcted&view=ClubGuestRequestsByDate&guestlist_date=<?php echo $booking['date']; ?>&Itemid=<?php echo $Itemid; ?>"><button class="btn btn-primary">Detail</button></a></td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>

            </tbody>
		</table>

		<?php if($hasRequest == 0): ?>
			<div id="system-message">
				<div class="alert alert-block">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4><?php echo JText::_('COM_BCTED_CLUBREQUESTS_NO_REQUEST_FOUND_TITLE'); ?></h4>
					<div><p> <?php echo JText::_('COM_BCTED_CLUBREQUESTS_NO_REQUEST_FOUND_DESC'); ?></p></div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>

<?php //echo $this->pagination->getListFooter(); ?>
