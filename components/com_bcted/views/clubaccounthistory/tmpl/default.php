<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$messageMenu = BctedHelper::getBctedMenuItem('club-messages');

$messageLink = $messageMenu->link.'&Itemid='.$messageMenu->id;


/*echo "<pre>";
print_r($this->elementDetail);
echo "</pre>";*/

?>

<script type="text/javascript">
function deleteRequest(requestID)
{
	jQuery.ajax({
		type: "GET",
		url: "index.php?option=com_bcted&task=ajax.calculateExp",
		data: "&discount="+disocunt+"&auction_length="+auctionLength+"&batch_id="+batchID,
		success: function(data){

			jQuery('#min_accepted_bid').html(data);
			jQuery('#minimum_accepted_bid').val(data);
			/*if (!html.trim()) {
				jQuery('#upload_next').hide();
			}
			else
			{
				jQuery('#upload_next').show();
			}
			jQuery("#invoice_container").html(html);*/

			//console.log(data);
		}
	});
}
</script>

</script>
<div class="table-wrp">

	<div class="acnt-histry-tbl">
		<table class="activity" id="accordion">
			<thead>
				<tr>
					<th><?php echo JText::_('COM_BCTED_DATE'); ?></th>
					<th><?php echo JText::_('COM_BCTED_REFERENCE'); ?></th>
					<th><?php echo JText::_('COM_BCTED_AMOUNT'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($this->history as $key => $value): //echo "<pre>";print_r($value); echo "</pre>"; ?>
					<tr id="row<?php echo $value->venue_booking_id; ?>" class="accordion" data-toggle="collapse" data-target="#demo<?php echo $value->venue_booking_id; ?>">
						<td><?php echo date('d-m-Y',strtotime($value->venue_booking_datetime)); ?></td>
						<td><?php echo str_pad($value->venue_booking_id, 5, '0', STR_PAD_LEFT); ?></td>
						<td><?php echo $this->elementDetail->currency_sign.' '.number_format($value->amount_payable,0); ?></td>
					</tr>
					<tr class="expand-data">
						<td class="hiddenRow" colspan="3">
							<div class="collapse" id="demo<?php echo $value->venue_booking_id; ?>">
								<table class="tbl-hdndata">
									<tbody>
										<tr>
											<td><?php echo JText::_('COM_BCTED_NAME') ?>:</td>
											<td><?php echo JText::_('COM_BCTED_NO_OF_PEOPLE') ?>:</td>
											<td rowspan="2" valign="bottom">
												<?php if($this->elementDetail->licence_type != 'basic'): ?>
													<?php $app = JFactory::getApplication(); ?>
													<?php $menu = $app->getMenu(); ?>
													<?php $menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=messages', true ); ?>
													<?php $Itemid = $menuItem->id; ?>
													<?php $connectionID = BctedHelper::getMessageConnection($this->elementDetail->userid,$value->user_id); ?>
													<?php $link = JRoute::_("index.php?option=com_bcted&view=messagedetail&user_id=".$value->user_id."&connection_id=".$connectionID."&Itemid=".$Itemid); ?>
													<a href="<?php echo $link; ?>"><button class="msg-btn"><i class="msg-icon"></i></button></a>
												<?php else: ?>
													&nbsp;
												<?php endif; ?>
											</td>
										</tr>
										<tr>
											<?php $bookingUSer = JFactory::getUser($value->user_id); ?>
											<td><?php echo ucfirst($bookingUSer->name) . ' ' . ucfirst($value->last_name); ?></td>

											<td><?php echo $value->venue_booking_number_of_guest.'('.$value->male_count.'M/'.$value->female_count.'F)'; ?></td>
										</tr>
									</tbody>

								</table>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
				<!--<tr id="row1" class="accordion" data-toggle="collapse" data-target="#demo1">
					<td>12/12/2004</td>
					<td>00001</td>
					<td>$1000</td>
				</tr>
				<tr class="expand-data">
					<td class="hiddenRow" colspan="3">
						<div class="collapse" id="demo1">
							<table class="tbl-hdndata">
								<tbody>
									<tr>
										<td>Name:</td>
										<td>Num Ppl:</td>
										<td rowspan="2" valign="bottom"><button class="msg-btn"><i class="msg-icon"></i></button></td>
									</tr>
									<tr>
										<td>First Last</td>
										<td>8(6m/2f)</td>
									</tr>
								</tbody>

							</table>
						</div>
					</td>
				</tr>

				<tr id="row2" class="accordion" data-toggle="collapse" data-target="#demo2">
					<td>12/12/2004</td>
					<td>00001</td>
					<td>$1000</td>
				</tr>
				<tr class="expand-data">
					<td class="hiddenRow" colspan="3">
						<div class="collapse" id="demo2">
							<table class="tbl-hdndata">
								<tbody>
									<tr>
										<td>Name:</td>
										<td>Num Ppl:</td>
										<td rowspan="2" valign="bottom"><button class="msg-btn"><i class="msg-icon"></i></button></td>
									</tr>
									<tr>
										<td>First Last</td>
										<td>8(6m/2f)</td>
									</tr>
								</tbody>

							</table>
						</div>
					</td>
				</tr>

				<tr id="row3" class="accordion" data-toggle="collapse" data-target="#demo3">
					<td>12/12/2004</td>
					<td>00001</td>
					<td>$1000</td>
				</tr>
				<tr class="expand-data">
					<td class="hiddenRow" colspan="3">
						<div class="collapse" id="demo3">
							<table class="tbl-hdndata">
								<tbody>
									<tr>
										<td>Name:</td>
										<td>Num Ppl:</td>
										<td rowspan="2" valign="bottom"><button class="msg-btn"><i class="msg-icon"></i></button></td>
									</tr>
									<tr>
										<td>First Last</td>
										<td>8(6m/2f)</td>
									</tr>
								</tbody>

							</table>
						</div>
					</td>
				</tr>

				<tr id="row4" class="accordion" data-toggle="collapse" data-target="#demo4">
					<td>12/12/2004</td>
					<td>00001</td>
					<td>$1000</td>
				</tr>
				<tr class="expand-data">
					<td class="hiddenRow" colspan="3">
						<div class="collapse" id="demo4">
							<table class="tbl-hdndata">
								<tbody>
									<tr>
										<td>Name:</td>
										<td>Num Ppl:</td>
										<td rowspan="2" valign="bottom"><button class="msg-btn"><i class="msg-icon"></i></button></td>
									</tr>
									<tr>
										<td>First Last</td>
										<td>8(6m/2f)</td>
									</tr>
								</tbody>

							</table>
						</div>
					</td>
				</tr>

				<tr id="row5" class="accordion" data-toggle="collapse" data-target="#demo5">
					<td>12/12/2004</td>
					<td>00001</td>
					<td>$1000</td>
				</tr>
				<tr class="expand-data">
					<td class="hiddenRow" colspan="3">
						<div class="collapse" id="demo5">
							<table class="tbl-hdndata">
								<tbody>
									<tr>
										<td>Name:</td>
										<td>Num Ppl:</td>
										<td rowspan="2" valign="bottom"><button class="msg-btn"><i class="msg-icon"></i></button></td>
									</tr>
									<tr>
										<td>First Last</td>
										<td>8(6m/2f)</td>
									</tr>
								</tbody>

							</table>
						</div>
					</td>
				</tr>-->
			</tbody>
		</table>

	</div>
</div>

<script type="text/javascript">
	jQuery('.collapse').on('show.bs.collapse', function () {
		jQuery('.in.collapse').collapse('hide');
	});

	jQuery(document).ready(function() {
		jQuery('#accordion tr').click(function() {
			//var href = jQuery(this).find("a").attr("href");

			//alert(this.hasClass('active_row'));
			if(jQuery(this).hasClass('active_row'))
			{
				jQuery('#accordion tr').removeClass("active_row");
				jQuery(this).removeClass('active_row');
			}
			else
			{
				jQuery('#accordion tr').removeClass("active_row");
				jQuery(this).addClass(' active_row');
			}

		});

	});
</script>

<?php echo $this->pagination->getListFooter(); ?>
