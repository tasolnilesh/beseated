<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Bookings View
 *
 * @since  0.0.1
 */
class BctedViewClubBookings extends JViewLegacy
{
	protected $items;

	protected $user;

	protected $pagination;

	protected $state;
	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		// Get data from the model
		//$this->items         = $this->get('Items');
		//$this->pagination    = $this->get('Pagination');
		//$this->state         = $this->get('State');

		//$this->filterForm    = $this->get('FilterForm');
		//$this->activeFilters = $this->get('ActiveFilters');

            $this->user = JFactory::getUser();

            if(!$this->user->id)
            {
                  JFactory::getApplication()->redirect(JRoute::_(JURI::root().'index.php?option=com_users&view=login'), 'Please login first!');

                  //&return='.base64_encode(JURI::getInstance())
                  /*$app = JFactory::getApplication();
                  $message = "You must be logged in to view this content";
                  $url = JRoute::_('index.php?option=com_users&view=login&return=' . base64_encode(JUri::base()));
                  $app->redirect($url, $message);*/
            }

		$this->bookings         = $this->get('ClubBookings');

		/*echo "<pre>";
		print_r($this->bookings);
		echo "</pre>";
		exit;

            [venue_booking_id] => 5
            [venue_id] => 2
            [venue_table_id] => 5
            [user_id] => 118
            [venue_booking_table_privacy] => 0
            [venue_booking_datetime] => 2015-05-11 00:56:31
            [venue_booking_number_of_guest] => 5
            [male_count] => 0
            [female_count] => 0
            [venue_booking_additional_info] => handheld
            [total_price] => 0.00
            [deposit_amount] => 0.00
            [amount_payable] => 0.00
            [owner_message] => welcome N enjoy

            [status] => 5
            [user_status] => 5
            [is_rated] => 0
            [is_deleted] => 0
            [venue_booking_created] => 2015-02-11 03:12:37
            [time_stamp] => 1423645957
            [venue_table_name] => table for 3
            [custom_table_name] => couple table
            [venue_table_image] => images/bcted/table/114/b83375d7d0f8c52467121d8c.jpg
            [venue_table_price] => 2000.00
            [venue_table_capacity] => 2
            [venue_table_description] =>  description
            [status_text] => Booked
            [user_status_text] => Booked
            [venue_name] => Dasa Club
            [venue_address] =>
            [venue_about] => Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent posuere maximus nisl a rhoncus. Duis mauris ante, ornare ac lacinia fringilla, posuere et odio. Quisque et nibh sit amet ex mattis feugiat at quis dui. Duis iaculis massa a tempor placerat. Sed sed sapien nibh. Morbi venenatis urna sit amet enim porta molestie. Aenean at convallis nisl, quis fermentum leo. Aenean non risus non neque vestibulum efficitur at sed velit. Nunc at ligula in dolor blandit sagittis
            [venue_amenities] =>
            [venue_signs] => 2
            [venue_rating] => 0.00
            [venue_timings] =>
            [venue_image] =>
            [is_smart] => 0
            [is_casual] => 0
            [is_food] => 0
            [is_drink] => 0
            [working_days] =>
            [is_smoking] => 0
            [name] => testing
            [last_name] => user
            [phoneno] => 9879871828
        )*/
		$model = $this->getModel();

		$user = JFactory::getUser();

		//$elementDetail = BctedHelper::getUserElementID($user->id);

		//$summaries = $model->summaryForVenue($elementDetail->venue_id);

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Set the tool-bar and number of found items
		//$this->addToolBar();
		//$this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}

}
