<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');
$currentItemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$defaultPath =  JUri::base(); //http://192.168.5.31/development/thebeseated/
/*exit;*/

$document = JFactory::getDocument();

/*$document->addStyleSheet($defaultPath.'components/com_bcted/assets/tag-it/css/jquery.tagit.css');
$document->addStyleSheet($defaultPath.'components/com_bcted/assets/tag-it/css/tagit.ui-zendesk.css');
$document->addScript($defaultPath.'components/com_bcted/assets/tag-it/js/tag-it.js');*/

$document->addStyleSheet($defaultPath.'components/com_bcted/assets/tag-it//bootstrap/bootstrap-tagsinput.css');
$document->addScript($defaultPath.'components/com_bcted/assets/tag-it/bootstrap/bootstrap-tagsinput.js');



//$document->addScript('http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
//$document->addScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js');

?>

<?php //echo $this->bookingType."<pre>"; print_r($this->booking); echo "</pre>"; ?>
<div class="table-wrp">
    <div class="row-fluid book-dtlwrp">
    	<div class="span6">
	    	<?php if($this->booking->user_status == 2 || $this->booking->user_status == 5  || $this->booking->user_status == 4 || $this->booking->user_status == 10): ?>
	    		<?php if($this->booking->venue_id): ?>
					<h4><?php echo "<b>Club Name : </b>".$this->booking->venue_name; ?></h4>
				<?php elseif ($this->booking->company_id) :?>
					<h4><?php echo "<b>Company Name : </b>".$this->booking->company_name; ?></h4>
				<?php endif; ?>
				<h4><?php echo "<b>Package Name : </b>".$this->booking->package_name; ?></h4>
				<h4><?php echo "<b>Price : </b>".$this->booking->package_currency_sign.' '.$this->booking->package_price; ?></h4>
				<h4><?php echo "<b>Date : </b>".date('d-m-y',strtotime($this->booking->package_datetime)); ?></h4>
				<h4><?php echo "<b>Time : </b>".$this->booking->package_time; ?></h4>
				<h4><?php echo "<b>Status : </b>".$this->booking->user_status_text; ?></h4>

			<?php elseif($this->booking->user_status == 3): ?>
				<h4><?php echo ucfirst($this->booking->venue_name); ?> has confirmed your booking for <?php echo ucfirst($this->booking->package_name); ?> </h4>
	            <h4>The total price for this is <?php echo $this->booking->package_currency_sign.' '.$this->booking->package_price; ?></h4>
	            <!--<h4>To secure your booking, you will need to pay a deposit.</h4>
	            <h4>Remaining amount to be paid at table.</h4>-->

	            <?php $diposit = $this->booking->total_price; ?>
	            <?php if($this->booking->status==2): ?>
            	<a href="index.php?option=com_bcted&task=packageorder.packageInvitePayment&package_invite_id=<?php echo $this->booking->package_invite_id; ?>&booking_type=packageinvitation&auto_approve=1">
                	<button type="button" class="btn btn-primary">Pay <?php echo $this->booking->package_currency_sign.' '.$diposit; ?></button>
                </a>
            	<?php endif; ?>
			<?php endif; ?>

			<?php if($this->booking->status == 5): ?>
				<a href="index.php?option=com_bcted&task=userbookings.send_invited_request_refund&package_invite_id=<?php echo $this->booking->package_invite_id; ?>&Itemid=<?php echo $currentItemid; ?>">
                	<button type="button" class="btn btn-primary">Request Refund</button>
                </a>
			<?php endif; ?>
        </div>
        <div class="span6">
        	<!-- <button type="button" class="msg-btn pull-right"></button> -->
        	<img src="<?php echo ($this->booking->package_image)?JUri::base().$this->booking->package_image:'images/bcted/default/banner.png'; ?>">
        </div>
    </div>
    <div class="booklst-tbl">
         <div class="span12 pck-bking">
         	<?php /*if($this->booking->user_status == 3): ?>
				<h4>Or, Invite your friends to split amount</h4>
				<form method="post" action="index.php?option=com_bcted&task=userbookings.inviteuserinpackage">
					<div class="span12 package-invite">
						<input type="text" name="invite_user" id="invite_user" value="" data-role="tagsinput" />
					</div>
					<input type="hidden" name="package_purchase_id" id="package_purchase_id" value="<?php echo $this->booking->package_purchase_id; ?>">
					<button type="submit" class="btn btn-primary">Invite User</button>
				</form>
        	<?php elseif($this->booking->user_status == 2 || $this->booking->user_status == 5  || $this->booking->user_status == 4): ?>
	        	<a href="index.php?option=com_bcted&view=clubinformation&club_id=<?php echo $this->booking->venue_id; ?>&Itemid=128">
	               <button type="button" class="btn btn-primary">View Profile</button>
	           	</a>
        	<?php endif;*/ ?>

        </div>
    </div>
</div>



<!--
<VENUE/SERVICE> has confirmed your booking for <product>

The total price for this is <amount>

To secure your booking, you will need to pay a deposit.

Remaining amount to be paid at table.

Pay $10,000

-->