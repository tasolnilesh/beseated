<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Bookings View
 *
 * @since  0.0.1
 */
class BctedViewUserPackageInviteDetail extends JViewLegacy
{
	protected $booking;

	protected $user;

	protected $pagination;

	protected $state;

	protected $bookingType;

	protected $bctConfig;

	protected $loyaltyPoints;

	protected $currencyRateUSD = 0;
	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		$this->user = JFactory::getUser();

		if(!$this->user->id)
		{
			  JFactory::getApplication()->redirect(JRoute::_(JURI::root().'index.php?option=com_users&view=login'), 'Please login first!');
		}

		$app = JFactory::getApplication();
		$input = $app->input;


		$this->booking = $this->get('InvitationDetail');


		$this->bctConfig = BctedHelper::getExtensionParam_2();

		$this->loyaltyPoints = BctedHelper::getLoyaltyPointsOfUser($this->user->id);

		$convertExp = "";
		$elemntCurrencyCode =  $this->booking->package_currency_code;

		if($elemntCurrencyCode!="USD")
		{
			  $convertExp = $elemntCurrencyCode."_USD";
		}

		$this->currencyRateUSD = 0;

		if(!empty($convertExp))
		{
			  $url = "http://www.freecurrencyconverterapi.com/api/v3/convert?q=".$convertExp."&compact=y";
			  $ch = curl_init();
			  $timeout = 0;
			  curl_setopt ($ch, CURLOPT_URL, $url);
			  curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			  curl_setopt($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			  curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			  $rawdata = curl_exec($ch);
			  curl_close($ch);
			  if(empty($rawdata))
			  {
					$app = JFactory::getApplication();
					$app->redirect('index.php?success=false12');
					$app->close();
			  }
			  $object = json_decode($rawdata);
			  $this->currencyRateUSD = $object->$convertExp->val;
		}

		$model = $this->getModel();

		$user = JFactory::getUser();

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Display the template
		parent::display($tpl);
	}

}
