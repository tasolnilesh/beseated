<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

/*echo "<pre>";
print_r($this->bookings);
echo "</pre>";*/

?>
<script type="text/javascript">
function deleteRequest(requestID)
{

	jQuery.ajax({
		type: "GET",
		url: "index.php?option=com_bcted&task=ajax.calculateExp",
		data: "&discount="+disocunt+"&auction_length="+auctionLength+"&batch_id="+batchID,
		success: function(data){

			jQuery('#min_accepted_bid').html(data);
			jQuery('#minimum_accepted_bid').val(data);
			/*if (!html.trim()) {
				jQuery('#upload_next').hide();
			}
			else
			{
				jQuery('#upload_next').show();
			}
			jQuery("#invoice_container").html(html);*/

			//console.log(data);
		}
    });

}
</script>
<div class="bct-summary-container">
	<!--<div class="summary-statustrics">
		<div class="cls-bookings">123</div>
		<div class="cls-request">123</div>
		<div class="cls-revenu">123</div>
	</div>-->

	<ul class="nav nav-tabs book-tab">
		<li class="active" id="table1"><a href="#table" data-toggle="tab">Table</a></li>
		<li id="packages1"><a href="#packages" data-toggle="tab">Packages</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="table">
			<div class="summary-list">
				<ul>
					<?php $hasRequest = 0; ?>
					<?php if($this->bookings): ?>
						<?php foreach ($this->bookings as $key => $booking): //echo "<pre>"; print_r($booking); echo "</pre>"; ?>

							<?php $bookingTimeStamp = strtotime($booking->venue_booking_datetime.' '.$booking->booking_to_time); ?>
							<?php $currentTimeStamp = time(); ?>
							<?php if($currentTimeStamp>$bookingTimeStamp): ?>
								<?php continue; ?>
							<?php endif; ?>
							<?php $hasRequest = 1; ?>

							<li id="request_booking_<?php echo $booking->venue_booking_id; ?>">
								<?php $link = JRoute::_("index.php?option=com_bcted&view=clubrequestdetail&request_id=".$booking->venue_booking_id."&Itemid=".$Itemid); ?>
								<?php if($booking->status == 11): ?>
									<a href="#">
								<?php else: ?>
									<a href="<?php echo $link; ?>">
								<?php endif; ?>


								<div class="prd-single-left span8">
									<div class="prd-person-nm">
										<?php echo ucfirst($booking->name); ?>
									</div>
									<div class="prd-title">
										<?php
											if($booking->premium_table_id)
												echo $booking->venue_table_name;
											else
												echo $booking->custom_table_name;
										?>
									</div>
									<div class="prd-date-time"><?php echo date('d-m-Y',strtotime($booking->venue_booking_datetime)); ?></div>
									<?php
								        $fromTime = explode(":", $booking->booking_from_time);
								        $toTime = explode(":", $booking->booking_to_time);
								    ?>
									<div class="prd-date-time"><?php echo "From: " . $fromTime[0].':'.$fromTime[1]. ' To: ' . $toTime[0].':'.$toTime[1]; ?></div>
								</div>
								<div class="bk-single-right span4">

										<button class="reqlnk-btn"  type="button"><?php echo $booking->status_text; ?></button>

								</div>
								</a>
							</li>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php if(!$hasRequest): ?>
						<div id="system-message">
							<div class="alert alert-block">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<h4><?php echo JText::_('COM_BCTED_CLUBREQUESTS_NO_REQUEST_FOUND_TITLE'); ?></h4>
								<div><p> <?php echo JText::_('COM_BCTED_CLUBREQUESTS_NO_REQUEST_FOUND_DESC'); ?></p></div>
							</div>
						</div>
					<?php endif; ?>
				</ul>
			</div>
		</div>

		<div class="tab-pane" id="packages">
			<div class="summary-list">
				<ul>
					<?php if($this->packageRequests): ?>
						<?php foreach ($this->packageRequests as $key => $packageRequest): //echo "<pre>"; print_r($packageRequest); echo "</pre>"; exit;?>
							<?php $hasRequest = 1; ?>
							<?php $link = JRoute::_("index.php?option=com_bcted&view=clubpackagerequestdetail&request_id=".$packageRequest->package_purchase_id."&Itemid=".$Itemid); ?>
							<li id="request_booking_<?php echo $packageRequest->package_purchase_id; ?>">
								<a href="<?php echo $link; ?>">
								<div class="prd-single-left span8">
									<div class="prd-person-nm">
										<?php echo ucfirst($packageRequest->name); ?>
									</div>
									<div class="prd-title">
										<?php echo $packageRequest->package_name; ?>
									</div>
									<div class="prd-date-time"><?php echo date('d-m-Y',strtotime($packageRequest->package_datetime)); ?></div>
									<?php
								        $packageTime = explode(":", $packageRequest->package_time);

								    ?>
									<div class="prd-date-time"><?php echo $packageTime[0].':'.$packageTime[1]; ?></div>
								</div>
								<div class="bk-single-right span4">
									<button class="reqlnk-btn"  type="button"><?php echo $packageRequest->status_text; ?></button>
								</div>
								</a>
							</li>
						<?php endforeach; ?>
					<?php else: ?>
						<div id="system-message">
							<div class="alert alert-block">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<h4><?php echo JText::_('COM_BCTED_CLUBREQUESTS_NO_REQUEST_FOUND_TITLE'); ?></h4>
								<div><p> <?php echo JText::_('COM_BCTED_CLUBREQUESTS_NO_REQUEST_FOUND_DESC'); ?></p></div>
							</div>
						</div>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>

<?php //echo $this->pagination->getListFooter(); ?>
