<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');


$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');
?>
<script type="text/javascript">

	/*function addCompanyToFavourite(companyID,userID)
	{

		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=companies.addtofavourite',
			type: 'GET',
			data: '&company_id='+companyID+'&user_id='+userID,

			success: function(response){

	        	if(response == "1" || response == "2")
	        	{
	        		//jQuery('#favourite-btn').hide();
	        		jQuery('#favourite-add').hide();
	        		jQuery('#favourite-remove').show();
	        	}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}

	function removeCompanyFromFavourite(companyID,userID)
	{

		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=companies.removefromfavourite',
			type: 'GET',
			data: '&company_id='+companyID+'&user_id='+userID,

			success: function(response){

	        	if(response == "1" || response == "2")
	        	{
	        		//jQuery('#favourite-btn').hide();
	        		jQuery('#favourite-add').show();
	        		jQuery('#favourite-remove').hide();
	        	}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});
	}
	*/
</script>

<?php /*echo "<pre>";
print_r($this->package);
echo "</pre>";*/ ?>

<div class="venuedetail">
	<div class="venue-img">
		<div class="info-image-only">
			<?php if(file_exists($this->package->package_image)): ?>
				<img src="<?php echo $this->package->package_image; ?>" alt="" />
			<?php else: ?>
				<img src="images/bcted/default/banner.png" alt="" />
			<?php endif; ?>
		</div>
	</div>
	<div class="venue-text">
		<?php echo $this->package->package_details; ?>
	</div>

	<div>
		<h4>Price : <?php echo $this->package->currency_sign . " " .$this->package->package_price; ?></h4>
		<h4>Package Date : <?php echo date('d-m-Y',strtotime($this->package->package_date)); ?></h4>
		<h4>Venue : <?php echo $this->package->venue_name; ?></h4>
	</div>

	<div>
		<br />
	</div>
	<div class="row-fluid">
	<div class="pull-left table-img span6">
	<a href="index.php?option=com_bcted&view=packagepurchase&package_id=<?php echo $this->package->package_id; ?>" >
		Purchase Package
	</a>
	</div>
	</div>
</div>




