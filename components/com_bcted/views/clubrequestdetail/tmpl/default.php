<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

/*echo "<pre>";
print_r($this->booking);
echo "</pre>";*/

?>
<script type="text/javascript">
function changeRequestStatus(status)
{
	var owner_message = jQuery('#owner_message').val();
	jQuery.ajax({
		type: "GET",
		url: "index.php?option=com_bcted&task=clubrequestdetail.changeRequstStatus",
		data: "&request_id=<?php echo $this->booking->venue_booking_id; ?>&status="+status+"&owner_message="+owner_message,
		success: function(response){
            jQuery('#system-message').show();
			if(response == "1")
			{
				//jQuery('#booking_'+bookingID).remove();
                jQuery('#system-message').show();
                jQuery('#already-system-message').hide();

				location.reload('index.php?option=com_bcted&view=clubrequests&Itemid=<?php echo $Itemid; ?>');
			}

            if(response == "3")
            {
                jQuery('#already-system-message').show();
                jQuery('#system-message').hide();
            }
		}
    });

}

function removeRequestBooking(bookingID,userID)
{

    jQuery.ajax({
        url: 'index.php?option=com_bcted&task=clubbookings.deletePastBooking',
        type: 'GET',
        data: '&user_type=venue&booking_id='+bookingID+'&user_id='+userID,

        success: function(response){

            if(response == "200")
            {
                jQuery('#booking_'+bookingID).remove();
                window.location.href='index.php?option=com_bcted&view=clubrequests&Itemid=<?php echo $Itemid; ?>';
            }
        }
    })
    .done(function() {
        //console.log("success");
    })
    .fail(function() {
        //console.log("error");
    })
    .always(function() {
        //console.log("complete");
    });

}
</script>

<script type="text/javascript">
    jQuery(function(){
        jQuery('#system-message').hide();
        jQuery('#already-system-message').hide();
    });
</script>

<div class="table-wrp">
<div id="system-message">
    <div class="alert alert-block">
        <!--<button type="button" class="close" data-dismiss="alert">&times;</button>-->
        <h4><?php echo JText::_('COM_BCTED_REQUEST_STATUS_CHANGED_TITLE'); ?></h4>
        <div><p><?php echo JText::_('COM_BCTED_REQUEST_STATUS_CHANGED_DESC'); ?></p></div>
    </div>
</div>
<div id="already-system-message">
    <div class="alert alert-block">
        <!--<button type="button" class="close" data-dismiss="alert">&times;</button>-->
        <h4><?php echo JText::_('COM_BCTED_TABLE_TIME_SLOT_ALREADY_BOOKED_TITLE'); ?></h4>
        <div><p><?php echo JText::_('COM_BCTED_TABLE_TIME_SLOT_ALREADY_BOOKED_DESC'); ?></p></div>
    </div>
</div>

<h1>Request Detail</h1>
    <div class="req-detailwrp" id="booking_<?php echo $this->booking->venue_booking_id; ?>">
    <?php //echo "<pre>";print_r($this->booking); echo "</pre>"; //exit; ?>
    <?php
        $fromTime = explode(":", $this->booking->booking_from_time);
        $toTime = explode(":", $this->booking->booking_to_time);
    ?>
    	<h2> <?php echo ucfirst($this->booking->name); ?>  has requested the following booking:</h2>
        <h4>
            <?php
                if($this->booking->premium_table_id)
                    echo "<b>Table Name : </b>".ucfirst($this->booking->venue_table_name);
                else
                    echo "<b>Table Name : </b>".ucfirst($this->booking->custom_table_name);

            ?>
        </h4>
        <h4><?php echo "<b>Date : </b>" . date('d-m-Y',strtotime($this->booking->venue_booking_datetime)); ?></h4>
        <h4><?php echo "<b>Time : From </b>" . $fromTime[0].':'.$fromTime[1] ."<b> To </b>" . $toTime[0].':'.$toTime[1]; ?></h4>
        <h4><b>Number of Guests : </b><?php echo $this->booking->venue_booking_number_of_guest .'('.$this->booking->male_count .'M/'.$this->booking->female_count.'F)'; ?></h4>
        <h4><b>Status : </b><?php echo ucfirst($this->booking->status_text); ?></h4>
        <h4><?php echo ucfirst($this->booking->venue_booking_additional_info); ?></h4>

        <?php if($this->booking->status != '9' && $this->booking->status != '11'){ ?>

        <textarea id="owner_message" rows="7" placeholder="Enter message here"></textarea>

        <?php } ?>

        <div class="row-fluid req-btnwrp">

        	<?php
            if($this->booking->status == '9' || $this->booking->status == '11' || $this->booking->status == '7')
            {?>
        		<div class="span4"><button class="btn btn-primary"  onclick="removeRequestBooking('<?php echo $this->booking->venue_booking_id; ?>','<?php echo $this->booking->user_id; ?>')" type="button" >Delete</button> </div>


        	<?php }
            else
            { ?>
        		<div class="span4"><button onclick="changeRequestStatus('cancel')" class="cancel-btn" type="button"></button> </div>

        	<?php }
            ?>

            <?php
            if($this->booking->status != '9' && $this->booking->status != '11')
            {
                if($this->booking->status == 7 )
                {?>
    				<div class="span4"><button class="wait-btn" type="button" style=" border: 1px solid red;
        cursor: unset;"></button> </div>
                <?php }
                else
                {?>
                	<div class="span4"><button onclick="changeRequestStatus('waiting')" class="wait-btn" type="button"></button> </div>
                <?php }
                ?>
    			<?php
                if($this->booking->status == 6 )
                {?>
                	<div class="span4"><button class="ok-btn pull-right" type="button" style=" border: 1px solid red;
        cursor: unset;"></button> </div>
                <?php }
                else
                {?>
    					<div class="span4"><button onclick="changeRequestStatus('ok')" class="ok-btn pull-right" type="button"></button> </div>
    			<?php }
            }
            ?>
        </div>
    </div>
</div>
<?php //echo $this->pagination->getListFooter(); ?>
<!--

Requst Detail

stdClass Object
(
    [venue_booking_id] => 8
    [venue_id] => 4
    [venue_table_id] => 8
    [user_id] => 142
    [venue_booking_table_privacy] => 0
    [venue_booking_datetime] => 24-03-2015
    [booking_from_time] => 14:30:00
    [booking_to_time] => 14:30:00
    [venue_booking_number_of_guest] => 4
    [male_count] => 2
    [female_count] => 2
    [venue_booking_additional_info] => this is booking for 24 March 2015.
    [total_price] => 0.00
    [deposit_amount] => 0.00
    [amount_payable] => 0.00
    [owner_message] =>
    [status] => 1
    [user_status] => 2
    [is_rated] => 0
    [is_deleted] => 0
    [venue_booking_created] => 2015-03-18 17:28:10
    [time_stamp] => 1426679890
    [venue_table_name] => Gold Table
    [custom_table_name] => aaa
    [venue_table_image] => images/bcted/table/147/5a3798d32a5d078bba89b1d9.jpg
    [venue_table_price] => 1000.00
    [venue_table_capacity] => 8
    [venue_table_description] => This gold table is perfect for an awesome night out
    [status_text] => Request
    [user_status_text] => Pending
    [venue_name] => Crystal
    [venue_address] => United Arab Emirates
    [venue_about] => This all about crystal club added in by Roger
    [venue_amenities] =>
    [venue_signs] => 1
    [venue_rating] => 3.00
    [venue_timings] => 05:35:00
    [venue_image] => images/bcted/venue/147/c334b4ee3d9f98b671649bf0.jpg
    [is_smart] => 1
    [is_casual] => 0
    [is_food] => 0
    [is_drink] => 1
    [working_days] => 1,6,7
    [is_smoking] => 1
    [name] => guest1 Firstname
    [last_name] => guest1
    [phoneno] => +919033000000
)
-->