<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

?>
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
var myCenter=new google.maps.LatLng(<?php echo $this->profile->latitude; ?>,<?php echo $this->profile->longitude; ?>);
var marker;

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:5,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  animation:google.maps.Animation.BOUNCE
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);

function codeAddress() {
    var address = document.getElementById('company_address').value;
    geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status)
    {
    	if (status == google.maps.GeocoderStatus.OK)
    	{
    		//console.log(results[0].geometry.location.lat);
    		latitude = results[0].geometry.location.lat();
            longitude = results[0].geometry.location.lng();

            //alert(latitude + " " + longitude);
            jQuery('#latitude').val(latitude);
            jQuery('#longitude').val(longitude);

    		var mapProp = {
				center:myCenter,
				zoom:5,
				mapTypeId:google.maps.MapTypeId.ROADMAP
			};

			var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
	        	map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
				map: map,
				position: results[0].geometry.location
	        });
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }
</script>
<script type="text/javascript">
	jQuery('#file_upload_error_msg').hide();
	jQuery('#currency_change_error').hide();

</script>

<div class="table-wrp profile-wrp">
	<div>
		<div id="file_upload_error_msg" class="alert alert-error">image dimenssions must be greater than 500px width and 350px height</div>
		<div id="currency_change_error" class="alert"></div>
	</div>
	<form class="form-horizontal prf-form" enctype="multipart/form-data" method="post" accept="<?php echo JRoute::_('index.php?option=com_bcted&view=companyprofile'); ?>">
		<div class="row-fluid prof-locatnwrp">
			<div class="span6 prof-img">
				<?php if(file_exists($this->profile->company_image)): ?>
					<img id="display_company_image" src="<?php echo $this->profile->company_image; ?>" alt="" />
				<?php else: ?>
					<img id="display_company_image" src="templates/bcted/images/16e919d7e46be3edcc1727f5.jpg" alt="">
				<?php endif; ?>
				<input type="file" id="company_image" name="company_image" style="display: none;" />
			</div>
			<div class="span6">
				<div id="googleMap" style="height:250px;"></div>
			</div>
		</div>

		<div class="control-group">
			<div class="controls span12">
				<input type="text" name="company_name" id="company_name" value="<?php echo $this->profile->company_name; ?>" placeholder="Company Name">
			</div>
		</div>
		<div class="control-group">
			<?php /*<div class="controls span6">
				<input type="text" name="city" id="city" value="<?php echo $this->profile->city; ?>" placeholder="City">
			</div> */ ?>
			<div class="controls span6">
				<input type="text" name="company_address" id="company_address" onblur="codeAddress()" value="<?php echo $this->profile->company_address; ?>" placeholder="Location">
			</div>
		</div>
		<div class="control-group">

			<div class="controls span6">
				<textarea rows="8" name="company_about" id="company_about"><?php echo $this->profile->company_about; ?></textarea>
			</div>

			<!--<div class="controls span6">
				<?php //if(!empty($this->profile->from_time) && !empty($this->profile->to_time)): ?>
					<input type="text" name="timing" id="timing" value="<?php echo $this->profile->from_time .' - ' . $this->profile->to_time; ?>" placeholder="Opening hour(from - to)">
				<?php //else: ?>
					<input type="text" name="timing" id="timing" placeholder="Opening hour(from - to)">
				<?php //endif; ?>
			</div>-->
		</div>
		<!--<div class="control-group">
		</div>-->
		<div class="control-group">
			<div class="controls span12">
				<select class="currency_select_box" name="currency_code" id="currency_code">
					<option value="">Select Currency</option>
					<?php if($this->profile->currency_code == "EUR"): ?>
						<option selected="selected" value="EUR">Euro(€)</option>
					<?php else: ?>
						<option value="EUR">Euro(€)</option>
					<?php endif; ?>

					<?php if($this->profile->currency_code == "GBP"): ?>
						<option selected="selected" value="GBP">British pound(£)</option>
					<?php else: ?>
						<option value="GBP">British pound(£)</option>
					<?php endif; ?>

					<?php if($this->profile->currency_code == "AED"): ?>
						<option selected="selected" value="AED">UAE dirham(AED)</option>
					<?php else: ?>
						<option value="AED">UAE dirham(AED)</option>
					<?php endif; ?>

					<?php if($this->profile->currency_code == "USD"): ?>
						<option selected="selected" value="USD">United States dollar($)</option>
					<?php else: ?>
						<option value="USD">United States dollar($)</option>
					<?php endif; ?>

					<?php if($this->profile->currency_code == "CAD"): ?>
						<option selected="selected" value="CAD">Canadian dollar($)</option>
					<?php else: ?>
						<option value="CAD">Canadian dollar($)</option>
					<?php endif; ?>

					<?php if($this->profile->currency_code == "AUD"): ?>
						<option selected="selected" value="AUD">Australian dollar($)</option>
					<?php else: ?>
						<option value="AUD">Australian dollar($)</option>
					<?php endif; ?>
				</select>
			</div>
		</div>



		<?php
/*
.profile-wrp .prf-form .smart-attire-btn{background:url(../images/attire-icn.png) no-repeat; width:104px; height:103px; border:0 none;}
.profile-wrp .prf-form .smart-category-btn{background:url(../images/category-icn.png) no-repeat; width:103px; height:103px; border:0 none;}
.profile-wrp .prf-form .smoking-btn{background: url(../images/smoking-icn.png) no-repeat; width:103px; height:103px; border:0 none;}

.profile-wrp .prf-form .casual-attire-btn{background:url(../images/attire-icn.png) -106px 0 no-repeat; width:103px; height:103px; border:0 none;}
.profile-wrp .prf-form .casual-category-btn{background:url(../images/category-icn.png) no-repeat; width:103px; height:103px; border:0 none;}
.profile-wrp .prf-form .no-smoking-btn{background: url(../images/smoking-icn.png) no-repeat; width:103px; height:103px; border:0 none;}
*/
		?>




		<div class="control-group">
			<div class="controls span12">

				<input type="hidden" id="task" name="task" value="CompanyProfile.save">
				<input type="hidden" id="view" name="view" value="companyprofile">

				<?php //echo "<pre>"; print_r($this->profile); echo "</pre>"; ?>

				<input type="hidden" id="latitude" name="latitude" value="<?php echo $this->profile->latitude; ?>">
				<input type="hidden" id="longitude" name="longitude" value="<?php echo $this->profile->longitude; ?>">

				<input type="hidden" id="company_id" name="company_id" value="<?php echo $this->profile->company_id; ?>">
				<input type="hidden" id="Itemid" name="Itemid" value="<?php echo $this->Itemid; ?>">
				<button type="submit" class="btn btn-large btn-block">Save Profile</button>

			</div>
		</div>

	</form>
</div>
<script type="text/javascript">
	jQuery('#file_upload_error_msg').hide();
	jQuery('#currency_change_error').hide();
	jQuery("#display_company_image").click(function() {
		jQuery("input[id='company_image']").click();
	});

	var _URL = window.URL || window.webkitURL;

	jQuery(document).ready(function(){
	    function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                jQuery('#display_company_image').attr('src', e.target.result);
	            }

	            reader.readAsDataURL(input.files[0]);
	        }
	    }

	    jQuery("#company_image").change(function(){

	    	var image, file;

			if ((file = this.files[0])) {
				image = new Image();
				image.onload = function() {
					if(this.width <= 500 || this.height<=350)
					{
						document.getElementById("company_image").value = "";
						jQuery('#file_upload_error_msg').show();
						//alert("The image width is " +this.width + " and image height is " + this.height);
						//image dimenssions must be greater than 500px width and 350px height

						return false;
					}
					else
					{
						jQuery('#file_upload_error_msg').hide();
					}
				};
			  image.src = _URL.createObjectURL(file);
			}

	        readURL(this);
	    });

	    jQuery('#currency_code').on('change', function() {
			//alert( this.value ); // or $(this).val()
			var venue_id = jQuery('#company_id').val();
			jQuery.ajax({
				type: "GET",
				url: "index.php?option=com_bcted&task=companyprofile.change_currency",
				data: "&company_id="+venue_id+"&currency_code="+this.value,
				success: function(response){

					if(response == "200")
					{
						jQuery('#currency_change_error').html('Currency changed successfully');
						jQuery('#currency_change_error').removeClass('alert-error');
						jQuery('#currency_change_error').addClass(' alert-success');
					}

					if(response == "400")
					{
						jQuery('#currency_change_error').html('Invalid currency selected');
						jQuery('#currency_change_error').removeClass('alert-success');
						jQuery('#currency_change_error').addClass(' alert-error');
					}

					if(response == "500")
					{
						jQuery('#currency_change_error').html('Error while changing currency');
						jQuery('#currency_change_error').removeClass('alert-success');
						jQuery('#currency_change_error').addClass(' alert-error');
					}

					if(response == "707")
					{
						jQuery('#currency_change_error').html('Can not changed currency. bookings are available');
						jQuery('#currency_change_error').removeClass('alert-success');
						jQuery('#currency_change_error').addClass(' alert-error');
					}

					jQuery('#currency_change_error').show();
				}
			});

		});

	});

	/*jQuery("#company_image").change(function(e) {

		var image, file;

		if ((file = this.files[0])) {

			image = new Image();

			image.onload = function() {
				if(this.width <= 500 || this.height<=350)
				{
					document.getElementById("company_image").value = "";
					jQuery('#file_upload_error_msg').show();
					//alert("The image width is " +this.width + " and image height is " + this.height);
				}
				else
				{
					jQuery('#file_upload_error_msg').hide();
				}
			};
		  image.src = _URL.createObjectURL(file);
		}

	});*/
</script>

<?php
	/*
	$("#hinizio").change(function () {
		var re = /([01]\d|2[0-3]):([0-5]\d)/;
		var hinizio = $("#hinizio");
		if (hinizio.val().match(re)) {
			alert('MATCH');
		} else {
			alert('NOT MATCH');
		}
	});
	 */
?>
