<?php
/**
 * @package     Pass.Administrator
 * @subpackage  com_pass
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
$input = JFactory::getApplication()->input;
$lib_id=$input->get('library_id',0,'int');
?>
<script type="text/javascript">
function numbersonly(events){
    var unicodes=events.charCode? events.charCode :events.keyCode;
    if (unicodes!=8)
    {
        if( ((unicodes>47 && unicodes<58) || unicodes == 43 || unicodes == 46 || unicodes == 9 )){
            return true;
        }else{
            return false;
        }
    }
}

jQuery(document).ready(function()
{
    jQuery("#jform_phoneno").focusout(function() {
        var mobno = document.getElementById('jform_phoneno');
        if (mobno.value.length <10)
        {
            jQuery('#mobilenoid').css('display', 'block');
            jQuery('#mobilenoid').addClass(' invalid');

            return true;
        }
        else
        {
            jQuery('#mobilenoid').css('display', 'none');
            return false;
        }
    });
});
</script>
<!--<form action="<?php //echo JRoute::_('index.php?option=com_bcted&view=registration&task=registration.save'); ?>"
    method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="form-horizontal" >
        <fieldset>
            <div class="span12">
                <input type="text" name="username">
            </div>
            <?php //echo JHtml::_('form.token'); ?>0
        </fieldset>

        <div class="row" align="center">
            <input type="submit" value="Save" name="Save" />
            <input type="reset" value="Cancel" name="Cancel" />
        </div>
    </div>
</form> -->

<div class="guest-club-wrp register-wrp">
    <div class="inner-guest-wrp">
        <form class="form-horizontal" method="post" action="<?php echo JRoute::_('index.php?option=com_bcted');?>">
            <!--<div class="control-group">
                <label class="control-label req-title span6" >Request Guestlist</label>
                <div class="controls span6">
                    <div class="g-reqtime">Time left for today’s Guestlist: <span id="future_date"></span></div>
                </div>
            </div> -->
            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('first_name'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('first_name'); ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('last_name'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('last_name'); ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('city'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('city'); ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('email'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('email'); ?>
                </div>
            </div>

            <div class="control-group">
                <span id="mobilenoid" style="display:none ;margin-top: 10px;color: red;">
                    Invalid Phone number.
                </span>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('phoneno'); ?></label>

                <div class="controls span7" onkeypress="return numbersonly(event);">
                    <?php echo $this->form->getInput('phoneno'); ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('password'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('password'); ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('password2'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('password2'); ?>
                </div>
            </div>

            <div class="control-group">
                <div class="control-label span5"></div>
                <div class="controls span7">
                <?php echo JHtml::_('form.token'); ?>
                    <button type="submit" class="btn btn-block btn-primary">Registration</button>
                    <input type="hidden" id="task" name="task" value="registration.save">
                </div>
            </div>
        </form>
    </div>
</div>
