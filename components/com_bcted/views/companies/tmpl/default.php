<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');


$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');
/*echo "<pre>";
print_r($Itemid);
echo "</pre>";
exit;*/

$app = JFactory::getApplication();
$menu = $app->getMenu();

//exit;
$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=companyinformation', true );
/*echo "<pre>";
print_r($menuItem);
echo "</pre>";
exit;*/
$Itemid = $menuItem->id;

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');
$hasDisplay = 0;
?>
<div class="wrapper">
	<?php
		$app = JFactory::getApplication();
		$prvCity = $app->input->cookie->get('search_in_city', '');
		$currentItemdid = $app->input->get('Itemid',0,'int');
	?>
	<a href="index.php"></a>
	<button class="filter-btn" type='button'  id='fltr-icn' value='hide/show'></button>

		<form action="<?php echo JRoute::_('index.php?option=com_bcted&view=companies&Itemid='.$currentItemdid);?>" method="post" class="form-inline" id="search_form">
			<div class="filter-bxwrp">
				<h3>Filter</h3>
				<div class="form-grp">
					<input type="text" name="caption">
				</div>
 				<div class="control-group">
					<input type="hidden" name="<?php echo $this->serachType; ?>" value="<?php echo $this->serachType; ?>">
					<input type="hidden" name="inner_search" value="1">
					<button type="submit" name="<?php echo $this->serachType; ?>" value="<?php echo $this->serachType; ?>" class="ok-btn small btn btn-block"></button>
				 </div>
			</div>
		</form>
   	<div class="span12 filter-result">
		<?php if($this->serachType == 'club'): ?>
			<?php foreach ($this->items as $key => $result): ?>
				<?php $hasDisplay = 1; ?>
				<div class="span4 venue_blck ">
					<div class="venue-img">
						<?php if(file_exists($result->venue_image)): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=clubinformation&club_id='.$result->venue_id.'&Itemid='.$Itemid) ?>"><img src="<?php echo $result->venue_image; ?>" alt="" /></a>
						<?php else: ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=clubinformation&club_id='.$result->venue_id.'&Itemid='.$Itemid) ?>"><img src="images/bcted/default/banner.png" alt="" /></a>
						<?php endif; ?>
						<div class="rating-title">
							<div class="venue-title span6">
								<h4><?php echo $result->venue_name; ?></h4>
								<div class="venue-location"><?php echo $result->venue_address; ?></div>
							</div>
                            <div class="rating-wrp span6">
                            	<?php $overallRating = $result->venue_rating; ?>
                            	<?php $starValue = floor($result->venue_rating); ?>
                            	<?php $maxRating = 5; ?>
                            	<?php $printedStart = 0 ;?>
								<?php for($i = 1; $i <= $starValue;$i++): ?>
									<i class="full"> </i>
									<?php $printedStart = $printedStart + 1; ?>
								<?php endfor; ?>
								<?php if($starValue<$overallRating): ?>
									<i class="half"> </i>
									<?php $printedStart = $printedStart + 1; ?>
								<?php endif; ?>
								<?php if($printedStart < $maxRating): ?>
									<?php for($i = $maxRating-$printedStart; $i > 0;$i--): ?>
										<i class="empty"> </i>
									<?php endfor; ?>
								<?php endif; ?>
								<?php /*for($i = 1; $i <= floor($result->venue_rating);$i++): ?>
									<i class="full"> </i>
								<?php endfor; ?>
								<?php for($i = floor($result->venue_rating)+1; $i <= 5; $i++): ?>
									<i class="empty"> </i>
								<?php endfor;*/ ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		<?php elseif($this->serachType == "service"): ?>
			<?php //$Itemid = 137; ?>
			<?php foreach ($this->items as $key => $result): ?>
				<?php $hasDisplay = 1; ?>
				<div class="span4 venue_blck ">
					<div class="venue-img">
						<?php if($result->company_type=='jet'): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=jetserviceinformation&company_id='.$result->company_id.'&Itemid='.$Itemid) ?>">
						<?php else: ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=companyinformation&company_id='.$result->company_id.'&Itemid='.$Itemid) ?>">
						<?php endif; ?>
						<?php if(file_exists($result->company_image)): ?>
							<img src="<?php echo $result->company_image; ?>" alt="" />
						<?php else: ?>
							<img src="images/bcted/default/banner.png" alt="" />
						<?php endif; ?>
						</a>

						<div class="rating-title">
							<div class="venue-title span6">
								<h4><?php echo $result->company_name; ?></h4>
								<div class="venue-location"><?php echo $result->company_address; ?></div>
							</div>
                            <div class="rating-wrp span6">
                            	<?php if($result->company_type=='jet'): ?>
                            	<?php else: ?>
                            		<?php $overallRating = $result->company_rating; ?>
	                            	<?php $starValue = floor($result->company_rating); ?>
	                            	<?php $maxRating = 5; ?>
	                            	<?php $printedStart = 0 ;?>
									<?php for($i = 1; $i <= $starValue;$i++): ?>
										<i class="full"> </i>
										<?php $printedStart = $printedStart + 1; ?>
									<?php endfor; ?>
									<?php if($starValue<$overallRating): ?>
										<i class="half"> </i>
										<?php $printedStart = $printedStart + 1; ?>
									<?php endif; ?>
									<?php if($printedStart < $maxRating): ?>
										<?php for($i = $maxRating-$printedStart; $i > 0;$i--): ?>
											<i class="empty"> </i>
										<?php endfor; ?>
									<?php endif; ?>
                            		<?php /*for($i = 1; $i <= floor($result->company_rating);$i++): ?>
										<i class="full"> </i>
									<?php endfor; ?>
									<?php for($i = floor($result->company_rating)+1; $i <= 5; $i++): ?>
										<i class="empty"> </i>
									<?php endfor;*/ ?>
                            	<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
		<?php if($hasDisplay == 0): ?>

		<div id="system-message">
			<div class="alert alert-block">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<h4><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_RESULT_FOUND_TITLE'); ?></h4>
				<!--<div><p><?php //echo JText::sprintf('COM_BCTED_USERBOOKINGS_NO_RESULT_FOUND_DESC',$this->serachType); ?></p></div>-->
			</div>
		</div>
		<?php endif; ?>
		<!--<div class="span4 venue_blck ">
			<div class="venue-img"><img src="images/venue-1.jpg" alt="" />
			<div class="rating-wrp"><i> </i><i> </i><i> </i><i> </i><i> </i></div>
			</div>
			<div class="venue-title">
			<h4>Bar blue</h4>
			<div class="venue-location">Dubai</div>
			</div>

		</div>
		<div class="span4 venue_blck">
			<div class="venue-img"><img src="images/venue-1.jpg" alt="" />
			<div class="rating-wrp"><i> </i><i> </i><i> </i><i> </i><i> </i></div>
			</div>
			<div class="venue-title">
			<h4>Bar blue</h4>
			<div class="venue-location">Dubai</div>
			</div>

		</div> -->
	</div>
</div>
<?php echo $this->pagination->getListFooter(); ?>
