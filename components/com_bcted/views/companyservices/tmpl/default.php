<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

?>
<div class="table-wrp">
<H2>Services</H2>
<?php foreach ($this->items as $key => $item): //echo "<pre>"; print_r($item); echo "</pre>";exit; ?>
	<div class="media">
		<div class="pull-left table-img span6">
			<?php if(file_exists($item->service_image)): ?>
				<img src="<?php echo $item->service_image; ?>">
			<?php else: ?>
				<img src="images/tabl-img.jpg">
			<?php endif; ?>
			<?php $link = JRoute::_('index.php?option=com_bcted&view=companyservicebooking&company_id='.$item->company_id.'&service_id='.$item->service_id.'&Itemid='.$Itemid); ?>
        	<a class="book-tbl" href="<?php echo $link; ?>">Book Service</a>
        </div>
		<div class="media-body span6">
			<div class="control-group">
				<h4 class="media-heading"><?php echo $item->service_name; ?></h4>

			</div>
			<div class="control-group">
				<h4 class="media-heading">
					<?php echo BctedHelper::currencyFormat($item->currency_code,$item->currency_sign,$item->service_price).'/hr'; ?>
				</h4>
			</div>

			<?php echo $item->service_description; ?>

		</div>
	</div>
<?php endforeach; ?>


<!--
	<div class="media">
	  <a class="pull-left" href="#">
		<img src="images/tabl-img.jpg">
	  </a>
	  <div class="media-body">
		<div class="control-group">
			<h4 class="media-heading">Table name</h4>
			<h4 class="media-heading">Table Type</h4>
		</div>
		<div class="control-group">
			<h4 class="media-heading">8 People</h4>
			<h4 class="media-heading">$10,000 min</h4>
		</div>

	Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae elementum augue, quis faucibus nisl. Vivamus accumsan lorem ligula, vitae porta purus vestibulum sit amet. Sed ut efficitur massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	</div>
	</div>
	<div class="media">
	  <a class="pull-left" href="#">
		<img src="images/tabl-img.jpg">
	  </a>
	  <div class="media-body">
		<div class="control-group">
			<h4 class="media-heading">Table name</h4>
			<h4 class="media-heading">Table Type</h4>
		</div>
		<div class="control-group">
			<h4 class="media-heading">8 People</h4>
			<h4 class="media-heading">$10,000 min</h4>
		</div>

	Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae elementum augue, quis faucibus nisl. Vivamus accumsan lorem ligula, vitae porta purus vestibulum sit amet. Sed ut efficitur massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	</div>
	</div>
	<div class="media">
  <a class="pull-left" href="#">
	<img src="images/tabl-img.jpg">
  </a>
  <div class="media-body">
	<div class="control-group">
		<h4 class="media-heading">Table name</h4>
		<h4 class="media-heading">Table Type</h4>
	</div>
	<div class="control-group">
		<h4 class="media-heading">8 People</h4>
		<h4 class="media-heading">$10,000 min</h4>
	</div>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae elementum augue, quis faucibus nisl. Vivamus accumsan lorem ligula, vitae porta purus vestibulum sit amet. Sed ut efficitur massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
</div>
</div>
-->
</div>
