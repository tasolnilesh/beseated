<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

?>

<script type="text/javascript">
	function removePastBooking(bookingID,userID)
	{

		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=companybookings.deletePastBooking',
			type: 'GET',
			data: '&booking_id='+bookingID+'&user_type=company',

			success: function(response){

	        	if(response == "200")
	        	{
	        		jQuery('#booking_'+bookingID).remove();
	        	}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}
</script>


<div class="bct-summary-container">
	<!--<div class="summary-statustrics">
		<div class="cls-bookings">123</div>
		<div class="cls-request">123</div>
		<div class="cls-revenu">123</div>
	</div>-->

	<div class="summary-list">
		<ul>
		<?php /*for($i = 0; $i<10;$i++): ?>
			<li>
			<div class="prd-single-left span8">
				<div class="prd-person-nm">persons name</div>
                <div class="prd-title">table name</div>
				<div class="prd-date-time">Date & Time</div>
			</div>
			<div class="bk-single-right span4">
				<button class="del-btn" type="button">-</button>
			</div>
			</li>
		<?php endfor;*/ ?>
		<?php if($this->bookings): ?>
			<?php foreach ($this->bookings as $key => $booking): ?> <?php //print_r($booking);exit; ?>
				<li id="booking_<?php echo $booking->service_booking_id; ?>">
					<a href="index.php?option=com_bcted&view=companybookingdetail&booking_id=<?php echo $booking->service_booking_id; ?>&Itemid=<?php echo $Itemid; ?>">
						<div class="prd-single-left span8">
							<div class="prd-person-nm">
								<?php echo $booking->name; ?>
							</div>
							<div class="prd-title"><?php echo $booking->service_name; ?></div>
							<div class="prd-date-time"><?php echo date('d-m-Y',strtotime($booking->service_booking_datetime)); ?></div>
							<div class="prd-date-time"><?php echo BctedHelper::convertToHM($booking->booking_from_time) . ' - ' . BctedHelper::convertToHM($booking->booking_to_time); ?></div>
						</div>
					</a>
					<div class="bk-single-right span4">
						<?php $bookingDate = strtotime($booking->service_booking_datetime); ?>
						<?php $currentTime = strtotime(date('Y-m-d')); ?>
						<?php if($booking->status_text == 'Booked' && $currentTime>$bookingDate): ?>
							<button class="del-btn" onclick="removePastBooking('<?php echo $booking->service_booking_id; ?>','<?php echo $this->user->id; ?>')" type="button">-</button>
						<?php elseif($booking->status_text == 'Cancelled'): ?>
							<button class="del-btn" onclick="removePastBooking('<?php echo $booking->service_booking_id; ?>','<?php echo $this->user->id; ?>')" type="button">-</button>
						<?php endif; ?>
					</div>
				</li>
			<?php endforeach; ?>
		<?php else: ?>
			<div id="system-message">
                <div class="alert alert-block">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_GENERAL_TITLE'); ?></h4>
                    <div><p><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_FOR_GENERAL_DESC'); ?></p></div>
                </div>
            </div>
		<?php endif; ?>
		</ul>
	</div>
</div>

<?php //echo $this->pagination->getListFooter(); ?>
