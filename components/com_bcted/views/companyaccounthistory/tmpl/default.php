<?php
/**
 * @package     Beseated.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$messageMenu = BctedHelper::getBctedMenuItem('club-messages');

$messageLink = $messageMenu->link.'&Itemid='.$messageMenu->id;

?>

<script type="text/javascript">
function deleteRequest(requestID)
{
	jQuery.ajax({
		type: "GET",
		url: "index.php?option=com_bcted&task=ajax.calculateExp",
		data: "&discount="+disocunt+"&auction_length="+auctionLength+"&batch_id="+batchID,
		success: function(data){
			jQuery('#min_accepted_bid').html(data);
			jQuery('#minimum_accepted_bid').val(data);
		}
	});
}
</script>

<div class="table-wrp">

	<div class="acnt-histry-tbl">
		<table class="activity" id="accordion">
			<thead>
				<tr>
					<th><?php echo JText::_('COM_BCTED_DATE'); ?></th>
					<th><?php echo JText::_('COM_BCTED_REFERENCE'); ?></th>
					<th><?php echo JText::_('COM_BCTED_AMOUNT'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($this->history as $key => $value): //echo "<pre>";print_r($value); echo "</pre>"; ?>
					<?php $bookingUSer = JFactory::getUser($value->user_id); ?>
					<?php //if($) ?>
					<tr id="row<?php echo $value->service_booking_id; ?>" class="accordion" data-toggle="collapse" data-target="#demo<?php echo $value->service_booking_id; ?>">
						<td><?php echo date('d-m-Y',strtotime($value->service_booking_datetime)); ?></td>
						<td><?php echo str_pad($value->service_booking_id, 5, '0', STR_PAD_LEFT); ?></td>
						<td>$<?php echo number_format($value->amount_payable); ?></td>
					</tr>
					<tr class="expand-data">
						<td class="hiddenRow" colspan="3">
							<div class="collapse" id="demo<?php echo $value->service_booking_id; ?>">
								<table class="tbl-hdndata">
									<tbody>
										<tr>
											<td><?php echo JText::_('COM_BCTED_NAME') ?>:</td>
											<td><?php echo JText::_('COM_BCTED_NO_OF_PEOPLE') ?>:</td>
											<td rowspan="2" valign="bottom">
												<?php if($this->elementDetail->licence_type != 'basic'): ?>
													<a href="<?php echo $messageLink; ?>"><button class="msg-btn"><i class="msg-icon"></i></button></a>
												<?php else: ?>
													&nbsp;
												<?php endif; ?>
											</td>
										</tr>
										<tr>
											<?php $bookingUSer = JFactory::getUser($value->user_id); ?>
											<td><?php echo $bookingUSer->name . ' ' . $value->last_name; ?></td>

											<td><?php echo $value->service_booking_number_of_guest.'('.$value->male_count.'M/'.$value->female_count.'F)'; ?></td>
										</tr>
									</tbody>

								</table>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

<script type="text/javascript">
	jQuery('.collapse').on('show.bs.collapse', function () {
		jQuery('.in.collapse').collapse('hide');
	});

	jQuery(document).ready(function() {
		jQuery('#accordion tr').click(function() {
			if(jQuery(this).hasClass('active_row'))
			{
				jQuery('#accordion tr').removeClass("active_row");
				jQuery(this).removeClass('active_row');
			}
			else
			{
				jQuery('#accordion tr').removeClass("active_row");
				jQuery(this).addClass(' active_row');
			}

		});

	});
</script>
<?php echo $this->pagination->getListFooter(); ?>
