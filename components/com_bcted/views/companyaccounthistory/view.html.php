<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Account History View
 *
 * @since  0.0.1
 */
class BctedViewCompanyAccountHistory extends JViewLegacy
{
	protected $user;

	protected $history;

	protected $state;

	protected $elementDetail;
	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
            $this->user = JFactory::getUser();

            if(!$this->user->id)
            {
                  JFactory::getApplication()->redirect(JRoute::_(JURI::root().'index.php?option=com_users&view=login'), 'Please login first!');

                  /*$app = JFactory::getApplication();
                  $message = "You must be logged in to view this content";
                  $url = JRoute::_('index.php?option=com_users&view=login&return=' . base64_encode(JUri::base()));
                  $app->redirect($url, $message);*/
            }

            $this->elementDetail = BctedHelper::getUserElementID($this->user->id);

		// Get data from the model
		$this->history         = $this->get('Items');
		$this->pagination    = $this->get('Pagination');
		//$this->state         = $this->get('State');

		//$this->filterForm    = $this->get('FilterForm');
		//$this->activeFilters = $this->get('ActiveFilters');

		//$this->history         = $this->get('ClubAccountHistory');

		$model = $this->getModel();

		//$elementDetail = BctedHelper::getUserElementID($user->id);

		//$summaries = $model->summaryForVenue($elementDetail->venue_id);

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Set the tool-bar and number of found items
		//$this->addToolBar();
		//$this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}

}
