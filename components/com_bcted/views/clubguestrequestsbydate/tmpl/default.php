<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

/*echo "<pre>";
print_r($this->bookings);
echo "</pre>";*/

?>
<script type="text/javascript">
function updateRemainingGuest()
{

	var guestlistID   = jQuery('#guestlistID').val();
    var updatedGuest = jQuery('#updatedGuest').val();

    jQuery.ajax({
	    type: "GET",
	    url: "index.php?option=com_bcted&task=clubguestlist.updateremainingguest",
	    data: "guestlist_request_id="+guestlistID+"&updated_guest="+updatedGuest,
	    success: function(data){
	        //console.log(data);
	        location.reload();
	    }
    });
}

function setModelValue(guestlistID,totalRmaining)
{
	 jQuery('#guestlistID').val(guestlistID);

	 options = "<select name='updatedGuest' id='updatedGuest'>";

	 for(i=1;i<=totalRmaining;i++)
	 {
	 	options = options + "<option value='"+i+"'>"+i+"</option>";
	 }

	 options = options + "</select>";

	 jQuery('#guestlistDropdown').html(options);
}
</script>

<div class="bct-summary-container">
	<div class="summary-list guest-list">
		<ul>
		<?php if($this->bookings): ?>
			<?php /*echo "<pre>";
			print_r($this->bookings);
			echo "</pre>"; exit;*/ ?>
			<?php foreach ($this->bookings as $key => $booking): ?> <?php //print_r($booking);exit; ?>
				<li id="guestlist_<?php echo $booking['requestID']; ?>">
					<div class="guestlist span8">
						<div class="guestlist-header">
							<h1><?php echo ucfirst($booking['username']); ?></h1>

							<span class="guestlist-mf">
								<?php echo "(".$booking['maleCount']."M/".$booking['femaleCount']."F)"; ?>
							</span>
						</div>
						<div class="guestlist-description">
							<?php echo $booking['additionalInfo']; ?>
						</div>

						<div class="guestlist-remaining" id="remainingGuest">
							<?php echo "Remaining Guest: ".$booking['remainingGuest']; ?>
						</div>

					</div>
					<div class="bk-single-right span4">
						<?php if($booking['remainingGuest']!=0): ?>
							<a href="#myModal" role="button" data-toggle="modal" onclick="setModelValue('<?php echo $booking['requestID']; ?>','<?php echo $booking['remainingGuest']; ?>')"><button class="del-btn"  type="button">-</button></a>
						<?php endif; ?>
					</div>

				</li>
			<?php endforeach; ?>
		<?php else: ?>
			<div id="system-message">
                <div class="alert alert-block">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_GENERAL_TITLE'); ?></h4>
                    <div><p><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_FOR_GENERAL_DESC'); ?></p></div>
                </div>
            </div>
		<?php endif; ?>
		</ul>
	</div>
</div>

<?php //echo $this->pagination->getListFooter(); ?>

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Remaining Guest</h3>
    </div>
    <div class="modal-body">
        <div class="large-rating-wrp">

        </div>
        <p id="guestlistDropdown">
        </p>

        <!-- <input type="text" name="updatedGuest" id="updatedGuest" placeholder="Enter number of guest">-->

    </div>
    <div class="modal-footer">
        <!-- <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> -->
        <input type="hidden" name="guestlistID" id="guestlistID" value="1">

        <button class="btn btn-primary" onclick="updateRemainingGuest()">Update Guest</button>
    </div>
</div>
