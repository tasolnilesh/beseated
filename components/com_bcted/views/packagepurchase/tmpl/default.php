<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$document = JFactory::getDocument();
$document->addScript(Juri::root(true) . '/components/com_bcted/assets/rangeslider/js/rangeslider.js');
$document->addStyleSheet(Juri::root(true) . '/components/com_bcted/assets/rangeslider/css/rangeslider.css');

$document->addScript(Juri::root(true) . '/components/com_bcted/assets/datepicker/jquery.timepicker.js');
$document->addStyleSheet(Juri::root(true) . '/components/com_bcted/assets/datepicker/jquery.timepicker.css');

$document->addScript(Juri::root(true) . '/components/com_bcted/assets/timer/jquery.countdownTimer.js');
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>

<?php $currentTime = date('H'); ?>

<script type="text/javascript">
    jQuery(function() {
    	var clientTime = new Date();
    	var clientHours = clientTime.getHours();
        jQuery('#timepicker_from').timepicker({
        	timeFormat: 'HH:mm',
        	interval: 15,
        	defaultTime: "'"+ (clientHours + 1 )+"'",
            scrollbar: true,
            change: function(time) {
				checkForDate();
			}
        });

        jQuery('#timepicker_to').timepicker({
        	timeFormat: 'HH:mm',
        	interval: 15,
        	hour: 3,
            scrollbar: true
        });
    });
</script>

<script type="text/javascript">
    jQuery(function() {
        jQuery('#timepicker').timepicker({
        	timeFormat: 'HH:mm',
        	interval: 15,
            scrollbar: true
        });
    });
</script>

<script>
jQuery(function() {
	jQuery( "#datepicker" ).datepicker({ minDate: 0, maxDate: "+1M +10D",dateFormat:"dd-mm-yy" });
	jQuery('#invalid-date-time').hide();
});
</script>

<script type="text/javascript">
	function changedSelectedTable(selectedTable)
	{
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=clubtablebooking.getTableDetail',
			type: 'GET',
			data: 'table_id='+selectedTable.value,

			success: function(response){

	        	if(response.length!=0)
	        	{

	        		var responseArray = response.split("|");
	        		var tblName = "Book " + responseArray[0] + " Table";
	        		jQuery('#tableName').html(tblName);
	        	}
			}
		})
		.done(function() {

		})
		.fail(function() {

		})
		.always(function() {

		});

	}
</script>

<script type="text/javascript">
	function checkForDate()
	{
		//alert('ca;;');
		var requested_date = jQuery('#requested_date').val();
		var timepicker_from = jQuery('#timepicker_from').val();

		var currentTime = new Date();

		//var checkindatestr = requested_date;
		var dateParts = requested_date.split("-");
		var timeParts = timepicker_from.split(":");

		var selectedDate = new Date(dateParts[2],(dateParts[1] - 1), dateParts[0],timeParts[0],timeParts[1],00);
		//alert(currentTime.getTime() + " || " + selectedDate.getTime());


		if(currentTime.getTime() >= selectedDate.getTime())
		{
			//alert('past');
			jQuery('#invalid-date-time').show();

			return false;
		}
		else
		{
			jQuery('#invalid-date-time').hide();

			return true;
		}


	}
</script>

<script type="text/javascript">
	var d = new Date();
	var day = d.getDate();
	var month = d.getMonth()+1;
	var year = d.getFullYear();
	var dateOnly =  year + '/' + month + '/' + day;

	jQuery(function(){
		jQuery('#future_date').countdowntimer({
			dateAndTime : dateOnly + ' 20:00:00',
			size : "lg",
			regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
			regexpReplaceWith: "$2h:$3m"
		});
	});

</script>
<?php /*echo "<pre>";
print_r($this->packageDetail);
echo "</pre>";*/ ?>
<div class="guest-club-wrp">
	<div class="inner-guest-wrp">
		<h4>
			<?php echo "Book " . $this->packageDetail->package_name . ' package'; ?>
		</h4>
		<form class="form-horizontal" method="post" action="">

			<div class="control-group">
				<label class="control-label span6">Date:</label>
				<div class="controls span6">
					<input type="text" name="requested_date" readonly="true" class="span6" value="<?php echo date('d-m-Y',strtotime($this->packageDetail->package_date));?>" required="required" id="requested_date"/>
				</div>

			</div>


			<div class="control-group">
				<label class="control-label span6">Time:</label>
				<div class="controls span6">
					<input type="text" onchange="checkForDate();" name="requested_from_time" readonly="true" class="span6" required="required" id="timepicker_from"/>
					<!-- <input type="text" name="requested_to_time" readonly="true" class="span6" required="required" id="timepicker_to"> -->
					<div id="invalid-date-time" class="invalid-date-time">Selected date and time is not valid</div>
				</div>
			</div>



			<div class="control-group guest-range">
				<label class="control-label span6">Number of guests:</label>
				<div class="controls span6" >
					<div class="guest-range-inner">
						<input id="range" type="range" value="0" min="1" max="10" data-rangeslider>
						<output id="slider-value-disp"></output>
						<input type="hidden" id="guest_count" name="guest_count" value="1">
						<input type="hidden" id="male_count" name="male_count" value="1">
						<input type="hidden" id="female_count" name="female_count" value="0">
					</div>
					<div id="guest-icon" class="gust-icn">
						<a class="gust-icn-male" > </a>
					</div>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label span6">Your Location:</label>
				<div class="controls span6">
					<input type="text" class="span12" name="requested_location" class="span6" required="required" id="requested_location"/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label span6">Additional Information:</label>
				<div class="controls span6">
					<textarea class="span12" name="additional_information" ></textarea>
				</div>
			</div>

			<div class="control-group">
				<div class="controls span6"></div>
				<div class="controls span6">
					<button onclick="return checkForDate();" type="submit" class="btn btn-large span"><span id="package_book_now_caption">Book Now(<?php echo $this->packageDetail->currency_sign.' '.round($this->packageDetail->package_price); ?>)</span></button>
					<input type="hidden" id="task" name="task" value="packagepurchase.purchasePackage">
					<input type="hidden" id="Itemid" name="Itemid" value="<?php echo $Itemid; ?>">
					<input type="hidden" id="package_id" name="package_id" value="<?php echo $this->packageDetail->package_id;?>">
					<input type="hidden" id="venue_id" name="venue_id" value="<?php echo $this->packageDetail->venue_id;?>">
					<input type="hidden" id="company_id" name="company_id" value="<?php echo $this->packageDetail->company_id;?>">
				</div>
			</div>
		</form>
	</div>
</div>

<?php //echo $this->pagination->getListFooter(); ?>

<script>

function format1(n) {
    return n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

var $ = jQuery.noConflict();
$('#slider-value-disp').hide();
$(function() {
var $document = $(document),
selector = '[data-rangeslider]',
$element = $(selector);
// Example functionality to demonstrate a value feedback
function valueOutput(element) {
var value = element.value,
output = element.parentNode.getElementsByTagName('output')[0];
output.innerHTML = value;

}
for (var i = $element.length - 1; i >= 0; i--) {
valueOutput($element[i]);
};
$document.on('change', 'input[type="range"]', function(e) {
valueOutput(e.target);
});
// Example functionality to demonstrate disabled functionality
$document .on('click', '#js-example-disabled button[data-behaviour="toggle"]', function(e) {
var $inputRange = $('input[type="range"]', e.target.parentNode);
if ($inputRange[0].disabled) {
$inputRange.prop("disabled", false);
}
else {
$inputRange.prop("disabled", true);
}
$inputRange.rangeslider('update');
});
// Example functionality to demonstrate programmatic value changes
$document.on('click', '#js-example-change-value button', function(e) {
var $inputRange = $('input[type="range"]', e.target.parentNode),
value = $('input[type="number"]', e.target.parentNode)[0].value;
$inputRange.val(value).change();
});
// Example functionality to demonstrate destroy functionality
$document
.on('click', '#js-example-destroy button[data-behaviour="destroy"]', function(e) {
$('input[type="range"]', e.target.parentNode).rangeslider('destroy');
})
.on('click', '#js-example-destroy button[data-behaviour="initialize"]', function(e) {
$('input[type="range"]', e.target.parentNode).rangeslider({ polyfill: false });
});
// Example functionality to test initialisation on hidden elements
$document
.on('click', '#js-example-hidden button[data-behaviour="toggle"]', function(e) {
var $container = $(e.target.previousElementSibling);
$container.toggle();
});
// Basic rangeslider initialization
$element.rangeslider({
// Deactivate the feature detection
polyfill: false,
// Callback function
onInit: function() {},
// Callback function
onSlide: function(position, value) {
console.log('onSlide');
console.log('position: ' + position, 'value: ' + value);
},
// Callback function
onSlideEnd: function(position, value) {
console.log('onSlideEnd');
console.log('position: ' + position, 'value: ' + value);

	newHtml = '';
	oldValue = $('#guest_count').val();
	oldMale = $('#male_count').val();


	/*for(i = 0 ; i < value; i++)
	{
		newHtml = newHtml + '<a class="gust-icn-male" href="#"> </a>';
	}*/
	var packagePrice = "<?php echo $this->packageDetail->package_price; ?>";

	var formatPrice = format1(packagePrice * value);

	$('#package_book_now_caption').html("Book Now ( <?php echo $this->packageDetail->currency_sign; ?> "+formatPrice+")");

	if(oldValue == value)
	{

	}
	else if(oldValue < value)
	{
		//$("#guest-icon a").length


		appendGuest = value - oldValue;
		//alert("Old : " + oldValue + " current : " + value + " Callback : " + appendGuest);
		newHtml ='';
		for(i = 0; i < appendGuest; i++)
		{
			newHtml = newHtml + '<a class="gust-icn-male" > </a>';

		}
		$('#guest-icon').append(newHtml);
		$('#guest_count').val(value);

		/*newMale = oldMale + appendGuest;
		alert(newMale);*/

		newMale = parseInt(oldMale) + parseInt(appendGuest);
		//alert(newMale);


		$('#male_count').val(newMale);

	}
	else if(oldValue > value)
	{
		retmoveGuest = value - 1;
		$("#guest-icon > a:gt("+retmoveGuest+")").remove();

		$('.gust-icn-male').length;
		$('.gust-icn-female').length;

		$('#guest_count').val(value);
		$('#male_count').val($('.gust-icn-male').length);
		$('#female_count').val($('.gust-icn-female').length);
	}

}
});
});


$(document).on('click', "a.gust-icn-male", function() {
    /*var liId = $(this);
    alert(liId);*/
    if(this.hasClass('gust-icn-male'))
    {
    	this.removeClass('gust-icn-male');
    	this.addClass(' gust-icn-female');

    	var male_count   = $('#male_count').val();
		var female_count = $('#female_count').val();

		$('#male_count').val(parseInt(male_count) - 1);
		$('#female_count').val(parseInt(female_count) + 1);
    }
});

$(document).on('click', "a.gust-icn-female", function() {
    /*var liId = $(this);
    alert(liId);*/
    if(this.hasClass('gust-icn-female'))
    {
    	/*this.removeClass('gust-icn-female');
    	this.addClass(' gust-icn-male');

		var male_count   = $('#male_count').val();
		var female_count = $('#female_count').val();

		$('#male_count').val(parseInt(male_count) + 1);
		$('#female_count').val(parseInt(female_count) - 1);*/
    }
});


</script>
