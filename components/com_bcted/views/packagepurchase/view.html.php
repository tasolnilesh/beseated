<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Guest List View
 *
 * @since  0.0.1
 */
class BctedViewPackagePurchase extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $clubID;
	protected $user;
	protected $Itemid;
	protected $packageDetail;

	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		$input  = JFactory::getApplication()->input;
		$this->packageId  = $input->get('package_id', 0, 'int');
		$this->Itemid  = $input->get('Itemid', 0, 'int');

		$this->user = JFactory::getUser();
		$this->packageDetail = BctedHelper::getpackageDetail($this->packageId);

		/*echo "<pre>";
		print_r($this->packageDetail);
		echo "</pre>";*/


		/*if(!$this->user->id)
        {
              JFactory::getApplication()->redirect(JRoute::_(JURI::root().'index.php?option=com_users&view=login&Itemid='.$this->Itemid), 'Please login first!');
        }*/

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Set the tool-bar and number of found items
		//$this->addToolBar();
		//$this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}

}
