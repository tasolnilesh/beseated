<?php
/**
 * @package     Pass.Administrator
 * @subpackage  com_pass
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
$input = JFactory::getApplication()->input;

$ratingData = BctedHelper::getUserLastBookingDetail($this->user->id);


?>

<!--<form action="<?php //echo JRoute::_('index.php?option=com_bcted&view=registration&task=registration.save'); ?>"
    method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
    <div class="form-horizontal" >
        <fieldset>
            <div class="span12">
                <input type="text" name="username">
            </div>
            <?php //echo JHtml::_('form.token'); ?>0
        </fieldset>

        <div class="row" align="center">
            <input type="submit" value="Save" name="Save" />
            <input type="reset" value="Cancel" name="Cancel" />
        </div>
    </div>
</form> -->
<script type="text/javascript">
function numbersonly(events){
    var unicodes=events.charCode? events.charCode :events.keyCode;
    if (unicodes!=8)
    {
        if( ((unicodes>47 && unicodes<58) || unicodes == 43 || unicodes == 46 || unicodes == 9 )){
            return true;
        }else{
            return false;
        }
    }
}

jQuery(document).ready(function()
{
    jQuery("#jform_phoneno").focusout(function() {
        var mobno = document.getElementById('jform_phoneno');
        if (mobno.value.length <10)
        {
            jQuery('#mobilenoid').css('display', 'block');
            jQuery('#mobilenoid').addClass(' invalid');

            return true;
        }
        else
        {
            jQuery('#mobilenoid').css('display', 'none');
            return false;
        }
    });
});
</script>
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript">
    function codeAddress()
    {
        //google.maps.event.addDomListener(window, 'load', initialize);
        var address = document.getElementById('jform_city').value;
        geocoder = new google.maps.Geocoder();
        geocoder.geocode( { 'address': address}, function(results, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                //console.log(results[0].geometry.location.lat);
                latitude = results[0].geometry.location.lat();
                longitude = results[0].geometry.location.lng();

                //alert(latitude + " " + longitude);
                jQuery('#jform_latitude').val(latitude);
                jQuery('#jform_longitude').val(longitude);
            } else {
                // alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    jQuery(document).ready(function(){
        jQuery("#jform_city").blur(function(){
            codeAddress();
        });
    });
</script>

<div class="guest-club-wrp register-wrp userprofile">

    <div class="inner-guest-wrp">
        <form class="form-horizontal" method="post" action="<?php echo JRoute::_('index.php?option=com_bcted');?>">
            <!--<div class="control-group">
                <label class="control-label req-title span6" >Request Guestlist</label>
                <div class="controls span6">
                    <div class="g-reqtime">Time left for today’s Guestlist: <span id="future_date"></span></div>
                </div>
            </div> -->
            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('first_name'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('first_name'); ?>
                    <?php echo $this->form->getInput('userid'); ?>
                    <?php echo $this->form->getInput('latitude'); ?>
                    <?php echo $this->form->getInput('longitude'); ?>

                </div>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('last_name'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('last_name'); ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('city'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('city'); ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('email'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('email'); ?>
                </div>
            </div>

            <div class="control-group">
                <span id="mobilenoid" style="display:none ;margin-top: 10px;color: red;">
                    Invalid Phone number.
                </span>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('phoneno'); ?></label>
                <div class="controls span7" onkeypress="return numbersonly(event);">
                    <?php echo $this->form->getInput('phoneno'); ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('password'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('password'); ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label span5"><?php echo $this->form->getLabel('password2'); ?></label>
                <div class="controls span7">
                    <?php echo $this->form->getInput('password2'); ?>
                </div>
            </div>

            <div class="control-group">
                <div class="control-label span5"></div>
                <div class="controls span7">
                <?php echo JHtml::_('form.token'); ?>
                    <button type="submit" class="btn btn-block btn-primary">Update Profile</button>
                    <input type="hidden" id="task" name="task" value="userprofile.save">
                </div>
            </div>
        </form>
    </div>
</div>

<?php
$venueRating = count($ratingData['venues']);
$companyRating = count($ratingData['service']);
/*echo "<pre>";
print_r($ratingData);
echo "</pre>";*/

if($venueRating)
{
    ?>
    <script type="text/javascript">
        //alert("<?php echo $ratingData['venues'][0]['tableName']; ?>");
        jQuery(window).load(function(){
            jQuery('#myModal').modal('show');
        });

        jQuery(document).ready(function() {
            jQuery('#myModal').modal({
                    keyboard: false,
                    backdrop:true,
                    backdrop: 'static'
                });
            jQuery('#myModalLabel').text("Rate for <?php echo $ratingData['venues'][0]['tableName']; ?>");
            jQuery('#elementType').val('venue');
            jQuery('#bookingID').val("<?php echo $ratingData['venues'][0]['bookingID']; ?>");
            jQuery('#elementID').val("<?php echo $ratingData['venues'][0]['venueID']; ?>");
        });
    </script>
    <?php
}
else if($companyRating)
{
   ?>
    <script type="text/javascript">
        //alert("<?php echo $ratingData['service'][0]['serviceName']; ?>");
        jQuery(window).load(function(){
            jQuery('#myModal').modal('show');
        });

        jQuery(document).ready(function() {
            jQuery('#myModal').modal({
                    keyboard: false,
                    backdrop:true,
                    backdrop: 'static'
                });
            jQuery('#myModalLabel').text("Rate for <?php echo $ratingData['service'][0]['serviceName']; ?>");
            jQuery('#elementType').val('service');
            jQuery('#bookingID').val("<?php echo $ratingData['service'][0]['bookingID']; ?>");
            jQuery('#elementID').val("<?php echo $ratingData['service'][0]['companyID']; ?>");
        });
    </script>
    <?php
}
?>

<script type="text/javascript">
    function giveRate(rateValue)
    {   //alert(rateValue);
        if(rateValue == 5)
        {
            jQuery('#star1,#star2,#star3,#star4,#star5').removeClass('empty full');
            jQuery('#star1,#star2,#star3,#star4,#star5').addClass('full');
        }
        else if(rateValue == 4)
        {
            jQuery('#star1,#star2,#star3,#star4,#star5').removeClass('empty full');
            jQuery('#star1,#star2,#star3,#star4').addClass('full');
            jQuery('#star5').addClass('empty');
        }
        else if(rateValue == 3)
        {
            jQuery('#star1,#star2,#star3,#star4,#star5').removeClass('empty full');
            jQuery('#star1,#star2,#star3').addClass('full');
            jQuery('#star5,#star4').addClass('empty');
        }
        else if(rateValue == 2)
        {
            jQuery('#star1,#star2,#star3,#star4,#star5').removeClass('empty full');
            jQuery('#star1,#star2').addClass('full');
            jQuery('#star5,#star4,#star3').addClass('empty');
        }
        else if(rateValue == 1)
        {
            jQuery('#star1,#star2,#star3,#star4,#star5').removeClass('empty full');
            jQuery('#star1').addClass('full');
            jQuery('#star5,#star4,#star3,#star2').addClass('empty');
        }

        jQuery('#rateValue').val(rateValue);

        /*else if(rateValue == 4)
        {
            jQuery('#star1,#star2,#star3,#star4,#star5').removeClass('empty full');
            jQuery('#star1,#star2,#star3,#star4').addClass('full');
            jQuery('#star5').addClass('empty');
        }*/
    } // End of give Rate

    function giveRatingToElement()
    {
        var rateValue   = jQuery('#rateValue').val();
        var elementType = jQuery('#elementType').val();
        var ratedUserID = jQuery('#ratedUserID').val();
        var bookingID   = jQuery('#bookingID').val();
        var userMessage   = jQuery('#userMessage').val();
        var elementID = jQuery('#elementID').val();

        userMessage = userMessage.trim();
        if(userMessage.length == 0)
        {
            jQuery( "#userMessage" ).focus();
            return false;
        }
        else
        {
            jQuery.ajax({
                type: "GET",
                url: "index.php?option=com_bcted&task=userprofile.giverating",
                data: "rate_value="+rateValue+"&element_type="+elementType+"&rated_user_id="+ratedUserID+"&booking_id="+bookingID+"&user_message="+userMessage+"&element_id="+elementID,
                success: function(data){
                    //console.log(data);
                    location.reload();
                }
            });
        }
    }
</script>

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Rate for </h3>
    </div>
    <div class="modal-body">
        <div class="large-rating-wrp">
            <i id="star1" onClick="giveRate(1)" class="full"> </i>
            <i id="star2" onClick="giveRate(2)" class="empty"> </i>
            <i id="star3" onClick="giveRate(3)" class="empty"> </i>
            <i id="star4" onClick="giveRate(4)" class="empty"> </i>
            <i id="star5" onClick="giveRate(5)" class="empty"> </i>
        </div>
        <p><textarea id="userMessage" style="width=100%" cols="200" placeholder="enter your feedback" required="true"></textarea></p>
    </div>
    <div class="modal-footer">
        <!-- <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> -->
        <input type="hidden" name="rateValue" id="rateValue" value="1">
        <input type="hidden" name="elementType" id="elementType" value="">
        <input type="hidden" name="ratedUserID" id="ratedUserID" value="<?php echo $this->user->id; ?>">
        <input type="hidden" name="bookingID" id="bookingID" value="0">
        <input type="hidden" name="elementID" id="elementID" value="0">

        <button class="btn btn-primary" onclick="giveRatingToElement()">Rate It</button>
    </div>
</div>
