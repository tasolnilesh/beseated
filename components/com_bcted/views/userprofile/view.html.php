<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Categorory View
 *
 * @since  0.0.1
 */
class BctedViewUserProfile extends JViewLegacy
{
	protected $user;
	/**
	 * Display the Category view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	public function display($tpl = null)
	{
		$app = JFactory::getApplication();
		$this->user = JFactory::getUser();
		$userType = BctedHelper::getUserGroupType($this->user->id);
		if($userType == "Club")
		{
			$profileMenu = BctedHelper::getBctedMenuItem('club-profile');
			$link = $profileMenu->link."&Itemid=".$profileMenu->id;
			$app->redirect($link);
		}
		elseif($userType == "ServiceProvider")
		{
			$profileMenu = BctedHelper::getBctedMenuItem('company-profile');

			$link = $profileMenu->link."&Itemid=".$profileMenu->id;
			$app->redirect($link);
		}
		elseif($userType == "Registered")
		{

		}

		// Get the Data
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');

		/*echo "<pre>";
		print_r($this->form);
		echo "</pre>";

		echo "<pre>";
		print_r($this->item);
		echo "</pre>";
		exit;*/

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new RuntimeException(implode('<br />', $errors), 500);

			return false;
		}
		// Display the template
		parent::display($tpl);
	}
}
