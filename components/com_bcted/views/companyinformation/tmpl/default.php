<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');


$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

/*BctedHelper::isFavouriteVenue();
echo "end";
exit;*/

?>
<script type="text/javascript">
	function addCompanyToFavourite(companyID,userID)
	{

		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=companies.addtofavourite',
			type: 'GET',
			data: '&company_id='+companyID+'&user_id='+userID,

			success: function(response){

	        	if(response == "1" || response == "2")
	        	{
	        		//jQuery('#favourite-btn').hide();
	        		jQuery('#favourite-add').hide();
	        		jQuery('#favourite-remove').show();
	        	}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}

	function removeCompanyFromFavourite(companyID,userID)
	{

		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=companies.removefromfavourite',
			type: 'GET',
			data: '&company_id='+companyID+'&user_id='+userID,

			success: function(response){

	        	if(response == "1" || response == "2")
	        	{
	        		//jQuery('#favourite-btn').hide();
	        		jQuery('#favourite-add').show();
	        		jQuery('#favourite-remove').hide();
	        	}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}
</script>
<div class="venuedetail">
	<div class="venue-img">
		<div class="info-image-only">
			<?php if(file_exists($this->company->company_image)): ?>
				<img src="<?php echo $this->company->company_image; ?>" alt="" />
			<?php else: ?>
				<img src="images/bcted/default/banner.png" alt="" />
			<?php endif; ?>
		</div>
		<div class="rating-title">

			<div class="favourite-wrp span5">

					<button id="favourite-add" onclick="addCompanyToFavourite('<?php echo $this->company->company_id; ?>','<?php echo $this->user->id; ?>')" type="button" class="fav-btn"></button>

					<button id="favourite-remove" onclick="removeVenueFromFavourite('<?php echo $this->company->company_id; ?>','<?php echo $this->user->id; ?>')" type="button" class="fav-btn active"></button>
				<?php if(!$this->isFavourite):?>
					<script type="text/javascript">
						jQuery('#favourite-add').show();
		        		jQuery('#favourite-remove').hide();
					</script>
				<?php else: ?>
					<script type="text/javascript">
						jQuery('#favourite-add').hide();
	        			jQuery('#favourite-remove').show();
	        		</script>
				<?php endif; ?>
			</div>
			<div class="rating-wrp span5">
				<?php $overallRating = $this->company->company_rating; ?>
            	<?php $starValue = floor($this->company->company_rating); ?>
            	<?php $maxRating = 5; ?>
            	<?php $printedStart = 0 ;?>
				<?php for($i = 1; $i <= $starValue;$i++): ?>
					<i class="full"> </i>
					<?php $printedStart = $printedStart + 1; ?>
				<?php endfor; ?>
				<?php if($starValue<$overallRating): ?>
					<i class="half"> </i>
					<?php $printedStart = $printedStart + 1; ?>
				<?php endif; ?>
				<?php if($printedStart < $maxRating): ?>
					<?php for($i = $maxRating-$printedStart; $i > 0;$i--): ?>
						<i class="empty"> </i>
					<?php endfor; ?>
				<?php endif; ?>
				<?php /*for($i = 1; $i <= floor($this->company->company_rating);$i++): ?>
					<i class="full"> </i>
				<?php endfor; ?>
				<?php for($i = floor($this->company->company_rating)+1; $i <= 5; $i++): ?>
					<i class="empty"> </i>
				<?php endfor;*/ ?>
			</div>
			<!-- <div class="category-wrp span5">
				<a href="#"></a>
				<a href="#"></a>
				<a href="#"></a>
				<a href="#"></a>
			</div>-->
			<!--<div class="sign-wrp span5">
				<?php //if($this->company->company_signs == 1): ?>
					<?php //echo "$"; ?>
				<?php //elseif($this->company->venue_signs == 2): ?>
					<?php //echo "$$"; ?>
				<?php //elseif($this->company->venue_signs == 3): ?>
					<?php //echo "$$$"; ?>
				<?php //endif; ?>
			</div> -->
		</div>
	</div>
	<div class="venue-text">
		<?php echo $this->company->company_about; ?>
	</div>
</div>




