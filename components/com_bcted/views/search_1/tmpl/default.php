<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
?>
<?php

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$app = JFactory::getApplication();
$menu = $app->getMenu();
$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=clubinformation', true );



$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');
?>



<div class="wrapper">
	<h2 class="result-title"><a href="index.php"></a>Search Results  <button class="filter-btn" type='button'  id='fltr-icn' value='hide/show'></button>
    <div class="filter-bxwrp">
    	<h3>Filter</h3>
        <div class="form-grp">
        	<input type="text">
        </div>
         <div class="city-dd">
         <select id="provincesList1" name="provincesOption1">
            <option selected="selected">Select your City</option>
            <option>Ahmedabad</option>
            <option>Surat</option>
         </select>
        </div>
         <table>
        	<tr>
            	<td>
            		<input id="smart_casual" class="search-smart-attire-btn" type="button">
					<input id="is_smart" type="hidden" value="1" name="is_smart">
					<input id="is_casual" type="hidden" value="0" name="is_casual">
            	</td>
                <td rowspan="2" align="center" valign="middle" class="centr-img">
                	<input id="food_drink" class="search-smart-category-btn" type="button">
					<input id="is_food" type="hidden" value="1" name="is_food">
					<input id="is_drink" type="hidden" value="0" name="is_drink">
                </td>
                <td align="right">
                	<input id="smoking_nosmoking" class="search-smoking-btn" type="button">
					<input id="is_smoking" type="hidden" value="1" name="is_smoking">
                </td>
            </tr>
            <tr>
            	<td>
            		<input id="ratting_yesno" class="search-ratting-btn" type="button">
					<input id="is_ratting" type="hidden" value="1" name="is_ratting">
            	</td>
                <td align="right">
                	<input id="costly_yesno" class="search-costly-btn" type="button">
					<input id="is_costly" type="hidden" value="1" name="is_costly">
                </td>
            </tr>
        </table>
         <div class="control-group flter-option">
            <div class="controls">
              <div class="control-group">
              <input type="checkbox" value="1" id="c1" name="c1" /><label for="c1"> My Friends are attending    											 	          </label>
              </div>
              <div class="control-group">
                  <input type="checkbox" value="2" checked="checked" id="c2" name="c2" />
                  <label for="c2">Venues near me</label>
              </div>
            </div>
	      </div>
         <div class="control-group">
         	<button class="ok-btn small btn btn-block"></button>
         </div>
    </h2>

   	<div class="span12 filter-result">
		<?php if($this->serachType == 'club'): ?>
			<?php foreach ($this->items as $key => $result): ?>
				<div class="span4 venue_blck ">
					<div class="venue-img">
						<?php if(file_exists($result->venue_image)): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=clubinformation&club_id='.$result->venue_id.'&Itemid='.$menuItem->id) ?>"><img src="<?php echo $result->venue_image; ?>" alt="" /></a>
						<?php else: ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=clubinformation&club_id='.$result->venue_id.'&Itemid='.$menuItem->id) ?>"><img src="images/bcted/default/banner.png" alt="" /></a>
						<?php endif; ?>
						<div class="rating-title">
							<div class="venue-title span6">
								<h4><?php echo $result->venue_name; ?></h4>
								<div class="venue-location"><?php echo $result->country; ?></div>
							</div>
                            <div class="rating-wrp span6">
								<?php for($i = 1; $i <= $result->venue_rating;$i++): ?>
									<i class="full"> </i>
								<?php endfor; ?>
								<?php for($i = $result->venue_rating+1; $i <= 5; $i++): ?>
									<i class="empty"> </i>
								<?php endfor; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		<?php elseif($this->serachType == "service"): ?>
			<?php foreach ($this->items as $key => $result): ?>
				<div class="span4 venue_blck ">
					<div class="venue-img">
						<?php if(file_exists($result->company_image)): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=companyinformation&company_id='.$result->company_id.'&Itemid='.$menuItem->id) ?>">
							<img src="<?php echo $result->company_image; ?>" alt="" />
							</a>
						<?php else: ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=companyinformation&company_id='.$result->company_id.'&Itemid='.$menuItem->id) ?>">
							<img src="images/bcted/default/banner.png" alt="" />
							</a>
						<?php endif; ?>

						<div class="rating-title">
							<div class="venue-title span6">
								<h4><?php echo $result->company_name; ?></h4>
								<div class="venue-location"><?php echo $result->country; ?></div>
							</div>
                            <div class="rating-wrp span6">
								<?php for($i = 1; $i <= $result->company_rating;$i++): ?>
									<i class="full"> </i>
								<?php endfor; ?>
								<?php for($i = $result->company_rating+1; $i <= 5; $i++): ?>
									<i class="empty"> </i>
								<?php endfor; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
		<!--<div class="span4 venue_blck ">
			<div class="venue-img"><img src="images/venue-1.jpg" alt="" />
			<div class="rating-wrp"><i> </i><i> </i><i> </i><i> </i><i> </i></div>
			</div>
			<div class="venue-title">
			<h4>Bar blue</h4>
			<div class="venue-location">Dubai</div>
			</div>

		</div>
		<div class="span4 venue_blck">
			<div class="venue-img"><img src="images/venue-1.jpg" alt="" />
			<div class="rating-wrp"><i> </i><i> </i><i> </i><i> </i><i> </i></div>
			</div>
			<div class="venue-title">
			<h4>Bar blue</h4>
			<div class="venue-location">Dubai</div>
			</div>

		</div> -->
	</div>
</div>

<script type="text/javascript">
	jQuery('#smart_casual').on('click',function(){
		//alert('hello');
		if (jQuery(this).hasClass('search-smart-attire-btn'))
		{
			jQuery('#is_smart').val('0');
			jQuery('#is_casual').val('1');
			jQuery(this).removeClass('search-smart-attire-btn');
			jQuery(this).addClass(' search-casual-attire-btn');
		}
		else
		{
			jQuery('#is_smart').val('1');
			jQuery('#is_casual').val('0');
			jQuery(this).removeClass('search-casual-attire-btn');
			jQuery(this).addClass(' search-smart-attire-btn');
		}
	});

	jQuery('#food_drink').on('click',function(){
		if (jQuery(this).hasClass('search-smart-category-btn'))
		{
			jQuery('#is_food').val('0');
			jQuery('#is_drink').val('1');
			jQuery(this).removeClass('search-smart-category-btn');
			jQuery(this).addClass(' search-casual-category-btn');
		}
		else
		{
			jQuery('#is_food').val('1');
			jQuery('#is_drink').val('0');
			jQuery(this).removeClass('search-casual-category-btn');
			jQuery(this).addClass(' search-smart-category-btn');
		}
	});

	jQuery('#smoking_nosmoking').on('click',function(){
		if (jQuery(this).hasClass('search-smoking-btn'))
		{
			jQuery('#is_smoking').val('0');
			jQuery(this).removeClass('search-smoking-btn');
			jQuery(this).addClass(' search-no-smoking-btn');
		}
		else
		{
			jQuery('#is_smoking').val('1');
			jQuery(this).removeClass('search-no-smoking-btn');
			jQuery(this).addClass(' search-smoking-btn');
		}
	});

	jQuery('#ratting_yesno').on('click',function(){
		if (jQuery(this).hasClass('search-ratting-btn'))
		{
			jQuery('#is_ratting').val('0');
			jQuery(this).removeClass('search-ratting-btn');
			jQuery(this).addClass(' search-no-ratting-btn');
		}
		else
		{
			jQuery('#is_ratting').val('1');
			jQuery(this).removeClass('search-no-ratting-btn');
			jQuery(this).addClass(' search-ratting-btn');
		}
	});

	jQuery('#costly_yesno').on('click',function(){
		if (jQuery(this).hasClass('search-costly-btn'))
		{
			jQuery('#is_costly').val('0');
			jQuery(this).removeClass('search-costly-btn');
			jQuery(this).addClass(' search-no-costly-btn');
		}
		else
		{
			jQuery('#is_costly').val('1');
			jQuery(this).removeClass('search-no-costly-btn');
			jQuery(this).addClass(' search-costly-btn');
		}
	});
</script>
<?php echo $this->pagination->getListFooter(); ?>

