<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

?>
<div class="table-wrp">
	<div class="media row-fluid">

	<!-- index.php?option=com_bcted&view=companyownerserviceedit&service_id=24&Itemid=145 -->
		<div class="pull-left table-img span6">
		<?php $link = JRoute::_("index.php?option=com_bcted&view=companyownerserviceedit&service_id=0&=&Itemid=".$Itemid); ?>
			<a class="club-add-table" href="<?php echo $link; ?>"><?php echo JText::_('COM_BCTED_COMPANYOWNERSERVICE_ADD_NEW_SERVICE'); ?></a>
		</div>
	</div>
<?php foreach ($this->items as $key => $item): //echo "<pre>"; print_r($item); echo "</pre>"; exit; ?>
	<div class="media row-fluid">
		<div class="pull-left table-img span6">
			<?php if(file_exists($item->service_image)): ?>
				<img src="<?php echo $item->service_image; ?>">
			<?php else: ?>
				<img src="images/tabl-img.jpg">
			<?php endif; ?>
        </div>
		<div class="media-body span6">
			<div class="control-group">
				<h4 class="media-heading"><?php echo $item->service_name; ?></h4>
				<!-- <h4 class="media-heading">Table Type</h4> -->
			</div>
			<div class="control-group">
				<h4 class="media-heading"><?php echo "Price : ".$item->currency_sign." ".number_format($item->service_price,2); ?></h4>
			</div>

			<?php //echo $item->venue_table_description; ?>
            <div class="tbl-actn-btn">
                <button><?php echo JText::_('COM_BCTED_DELETE');?></button>
                <?php $link = JRoute::_("index.php?option=com_bcted&view=companyownerserviceedit&service_id=".$item->service_id."&Itemid=".$Itemid); ?>
                <a href="<?php echo $link; ?>"><button><?php echo JText::_('COM_BCTED_EDIT');?></button></a>
            </div>

		</div>
	</div>
<?php endforeach; ?>
</div>
