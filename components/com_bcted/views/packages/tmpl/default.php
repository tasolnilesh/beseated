<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');


$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');
//$Itemid = 128;
$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$app = JFactory::getApplication();
$menu = $app->getMenu();
$bctParams = BctedHelper::getExtensionParam_2();

$accessLevel = BctedHelper::getGroupAccessLevel($bctParams->guest);

$access    = array('access','link');

$property  = array($accessLevel,'index.php?option=com_bcted&view=packagepurchase');
$menuItem2 = $menu->getItems( $access, $property, true );

//echo "<pre/>";print_r($menuItem2);exit;

$Itemid_new    = $menuItem2->id;

//echo "<pre/>";print_r($menuItem2);exit;

?>
<?php $flg = 0; ?>
<div class="wrapper">

   	<div class="span12 filter-result pkg-wrp">

			<?php foreach ($this->items as $key => $result): ?>
				<?php $flg = 1; ?>
				<div class="span4 venue_blck ">
					<div class="venue-img">
					<?php
					$redirectUrl_view    = 'index.php?option=com_bcted&view=packagepurchase&package_id='.$result->package_id.'&Itemid='.$Itemid_new;
					$redirectUrl_view    = urlencode(base64_encode($redirectUrl_view));
					$redirectUrl_view    = '&return='.$redirectUrl_view;
					$joomlaLoginUrl_view = 'index.php?option=com_users&view=login&Itemid='.$Itemid;
					$finalUrl_view       = $joomlaLoginUrl_view . $redirectUrl_view;

					if($this->user->id > 0)
					{
						$finalUrl_view = 'index.php?option=com_bcted&view=packagepurchase&package_id='.$result->package_id.'&Itemid='.$Itemid;
					}

					$user = JFactory::getUser();
					$userType = BctedHelper::getUserGroupType($user->id);
					if($userType == 'Club' || $userType == 'ServiceProvider')
					{
						$finalUrl_view = "";
					}

					?>
						<?php if(file_exists($result->package_image)): ?>
							<?php //echo JRoute::_('index.php?option=com_bcted&view=packageinformation&package_id='.$result->package_id); ?>
							<a class="white-color" href="<?php echo $finalUrl_view; ?>" >
								<img src="<?php echo $result->package_image; ?>" alt="" />
							</a>
						<?php else: ?>
							<?php //echo JRoute::_('index.php?option=com_bcted&view=packageinformation&package_id='.$result->package_id); ?>
							<a class="white-color" href="<?php echo $finalUrl_view; ?>" >
								<img src="images/bcted/default/banner.png" alt="" />
							</a>
						<?php endif; ?>
						<div class="rating-title">
							<div class="pkg-descr">
								<h4><?php echo $result->package_name; ?></h4>
								<?php if($result->venue_id): ?>
									<h5><?php echo $result->venue_name; ?></h5>
								<?php endif; ?>
								<?php // $services = BctedHelper::getServiceNameForPackage($result->company_ids); ?>
								<?php /*foreach ($services as $key => $service): ?>
									<h5><?php echo $service; ?></h5>
								<?php endforeach; */ ?>

								<h6><?php echo date('d-m-Y',strtotime($result->package_date)); ?></h6>
								<?php if(strlen($result->package_details)>230): ?>
									<p><?php echo substr($result->package_details, 0,230).'...'; ?></p>
								<?php else: ?>
									<p><?php echo substr($result->package_details, 0,230); ?></p>
								<?php endif; ?>
							</div>
							<?php $user = JFactory::getUser(); ?>
							<?php $userType = BctedHelper::getUserGroupType($user->id); ?>
							<?php if($userType == 'Club' || $userType == 'ServiceProvider'): ?>
							<?php else: ?>
								<div class="venue-title pkg-price">
									<a class="white-color" href="<?php echo $finalUrl_view; ?>" >
										<?php echo BctedHelper::currencyFormat($result->currency_code,$result->currency_sign,$result->package_price); ?>
									</a>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>

			<?php if($flg == 0): ?>
			<?php $user = JFactory::getUser(); ?>
			<?php $userType = BctedHelper::getUserGroupType($user->id); ?>
				<div id="system-message">
	                <div class="alert alert-block">
	                    <button type="button" class="close" data-dismiss="alert">&times;</button>
	                    <?php if($userType == 'Club'): ?>
	                    	<h4><?php echo JText::_('COM_BCTED_PACKAGE_NOT_AVAILABLE_FOR_YOUR_CLUB_TITLE'); ?></h4>
	                    	<div><p><?php echo JText::_('COM_BCTED_PACKAGE_NOT_AVAILABLE_FOR_YOUR_CLUB_DESC'); ?></p></div>
	                    <?php elseif($userType == 'ServiceProvider'): ?>
	                    	<h4><?php echo JText::_('COM_BCTED_PACKAGE_NOT_AVAILABLE_FOR_YOUR_COMPANY_TITLE'); ?></h4>
	                    	<div><p><?php echo JText::_('COM_BCTED_PACKAGE_NOT_AVAILABLE_FOR_YOUR_COMPANY_DESC'); ?></p></div>
	                    <?php else: ?>
	                    	<h4><?php echo JText::_('COM_BCTED_PACKAGE_NOT_AVAILABLE_FOR_BESEATED_GUEST_TITLE'); ?></h4>
	                    	<div><p><?php echo JText::_('COM_BCTED_PACKAGE_NOT_AVAILABLE_FOR_BESEATED_GUEST_DESC'); ?></p></div>
	                	<?php endif; ?>
	                </div>
	            </div>
			<?php endif; ?>
		</div>
</div>

<?php echo $this->pagination->getListFooter(); ?>

<style type="text/css">
a.white-color {
	color: white;
	text-decoration: blink;
}
</style>
