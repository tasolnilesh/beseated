<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Tables View
 *
 * @since  0.0.1
 */
class BctedViewClubOwnerTables extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $elementDetail;

	protected $user;
	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		// Get data from the model
		$this->items         = $this->get('VenueTables');
		//$this->pagination    = $this->get('Pagination');
		/*$this->state         = $this->get('State');*/

		/*echo "<pre>";
		print_r($this->items);
		echo "</pre>before exit";
		exit;*/

		$this->user = JFactory::getUser();


		//$this->filterForm    = $this->get('FilterForm');
		//$this->activeFilters = $this->get('ActiveFilters');

		 if(!$this->user->id)
        {
          JFactory::getApplication()->redirect(JRoute::_(JURI::root().'index.php?option=com_users&view=login'), 'Please login first!');

          /*$app = JFactory::getApplication();
          $message = "You must be logged in to view this content";
          $url = JRoute::_('index.php?option=com_users&view=login&return=' . base64_encode(JUri::base()));
              $app->redirect($url, $message);*/
        }

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		$this->elementDetail = BctedHelper::getUserElementID($this->user->id);

		// Set the tool-bar and number of found items
		//$this->addToolBar();
		//$this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}

}
