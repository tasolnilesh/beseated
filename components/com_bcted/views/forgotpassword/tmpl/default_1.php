<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');


$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

?>
<div class="login">

		<form class="form-horizontal" method="post" action="<?php echo JRoute::_('index.php?option=com_bcted&Itemid='.$Itemid);?>">
			<fieldset>
				<p><?php echo JText::_('COM_BCTED_FORGOT_PASSWORD_PRE_TEXT'); ?></p>
				<div class="control-group">
					<label class="control-label span6">Email ID:</label>
					<div class="controls span6">
						<input type="text" name="email" id="email" value="">
					</div>
				</div>
			</fieldset>

			<div class="control-group">
				<label class="control-label span6"></label>
				<div class="controls span6">
					<button type="submit" class="btn btn-primary validate">Submit</button>
					<input type="hidden" name="task" value="forgotpassword.setpassword">
					<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>">
					<input type="hidden" name="option" value="com_bcted">
				</div>
			</div>
		</form>

</div>

