<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');
$type = $input->get('type', '', 'string');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

?>
<script type="text/javascript">

jQuery(document).ready(function()
{
	var tabid = window.location.hash;

	var type = "<?php echo $type; ?>";

	if(type.length > 0 && type == 'packages')
	{
		jQuery('#table1').removeClass('active');
		jQuery('#service1').removeClass('active');
		jQuery('#packages1').addClass('active');

		jQuery('#table').removeClass('active');
		jQuery('#service').removeClass('active');
		jQuery('#packages').addClass('active');
	}
	else if(type.length > 0 && type == 'companyservices')
	{
		jQuery('#table1').removeClass('active');
		jQuery('#packages1').removeClass('active');
		jQuery('#service1').addClass('active');

		jQuery('#table').removeClass('active');
		jQuery('#packages').removeClass('active');
		jQuery('#service').addClass('active');
	}
	else
	{
		if(tabid == '#packages')
		{
			jQuery('#table1').removeClass('active');
			jQuery('#service1').removeClass('active');
			jQuery('#packages1').addClass('active');
		}else if(tabid == '#service')
		{
			jQuery('#table1').removeClass('active');
			jQuery('#packages1').removeClass('active');
			jQuery('#service1').addClass('active');
		}
		else
		{
			jQuery('#service1').removeClass('active');
			jQuery('#packages1').removeClass('active');
			jQuery('#table1').addClass('active');
		}
	}

});
</script>
<div class="bct-summary-container">
	<ul class="nav nav-tabs book-tab">
		<li class="active" id="table1"><a href="#table" data-toggle="tab">Table</a></li>
		<li id="service1"><a href="#service" data-toggle="tab">Services</a></li>
		<li id="packages1"><a href="#packages" data-toggle="tab">Packages</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="table">
			<div class="summary-list">
				<ul>
				<?php $hasTableBooking = 0; ?>
				<?php if($this->bookings): ?>
					<?php foreach ($this->bookings as $key => $booking): ?> <?php //echo "<pre>"; print_r($booking); echo "</pre>"; exit; ?>
						<?php $currentTimeStamp = time(); ?>
						<?php $bookingTimeStamp = strtotime($booking->venue_booking_datetime.' '.$booking->booking_to_time); ?>
						<?php if($currentTimeStamp>$bookingTimeStamp): ?>
							<?php continue; ?>
						<?php endif; ?>
						<?php $hasTableBooking = 1; ?>
						<li id="booking_<?php echo $booking->venue_booking_id; ?>">
						<a href="index.php?option=com_bcted&view=userbookingdetail&booking_type=table&booking_id=<?php echo $booking->venue_booking_id; ?>&Itemid=<?php echo $Itemid; ?>">
							<div class="prd-single-left span8">
								<div class="prd-person-nm">
									<!--<a href="index.php?option=com_bcted&view=userbookingdetail&booking_id=<?php echo $booking->venue_booking_id; ?>&Itemid=<?php echo $Itemid; ?>">
										<?php //echo $booking->name; ?>
									</a>-->
								</div>
								<div class="prd-title">
									<?php
										if($booking->premium_table_id)
										{
											echo $booking->venue_table_name;
										}
										else
										{
											echo $booking->custom_table_name;
										}

									?>
								</div>
								<div class="prd-title">

									<?php echo BctedHelper::currencyFormat($booking->currency_code,$booking->currency_sign,$booking->venue_table_price); ?>
								</div>

								<?php
									$fromTime = explode(":", $booking->booking_from_time);
									$toTime = explode(":", $booking->booking_to_time);
								?>
								<div class="prd-date-time"><?php echo date('d-m-Y',strtotime($booking->venue_booking_datetime)); ?></div>
								<div class="prd-date-time"> <?php echo $fromTime[0].':'.$fromTime[1] . ' - ' . $toTime[0].':'.$toTime[1]; ?></div>
							</div>
							<div class="bk-single-right span4">
								<?php /* if($booking->status_text == 'Booked'): ?>
									<button class="del-btn" onclick="removePastBooking('<?php echo $booking->venue_booking_id; ?>','<?php echo $this->user->id; ?>')" type="button">-</button>
								<?php endif; */ ?>
								<!-- <a href="#"><?php //echo $booking->user_status_text; ?></a>-->


									<button class="reqlnk-btn" type="button"><?php echo $booking->user_status_text; ?></button>

							</div>
							</a>
						</li>
					<?php endforeach; ?>
				<?php endif; ?>
				<?php if(!$hasTableBooking): ?>
					<div id="system-message">
						<div class="alert alert-block">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<h4><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_FOR_CLUB_TITLE'); ?></h4>
							<div><p><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_FOR_CLUB_DESC'); ?></p></div>
						</div>
					</div>
				<?php endif; ?>
				</ul>
			</div>
		</div>
		<div class="tab-pane" id="service">
			<div class="summary-list">
				<ul>
				<?php $hasServiceBooking = 0; ?>
				<?php if($this->serviceBookings): ?>
					<?php foreach ($this->serviceBookings as $key => $booking): ?> <?php //echo "<pre>"; print_r($booking); echo "</pre>"; exit; ?>
						<?php $currentTimeStamp = time(); ?>
						<?php $bookingTimeStamp = strtotime($booking->service_booking_datetime.' '.$booking->booking_to_time); ?>
						<?php if($currentTimeStamp>$bookingTimeStamp): ?>
							<?php continue; ?>
						<?php endif; ?>
						<?php $hasServiceBooking = 1; ?>
						<li id="booking_<?php echo $booking->service_booking_id; ?>">
						<a href="index.php?option=com_bcted&view=userbookingdetail&booking_type=service&booking_id=<?php echo $booking->service_booking_id; ?>&Itemid=<?php echo $Itemid; ?>">
							<div class="prd-single-left span8">
								<!--<div class="prd-person-nm">
									<a href="index.php?option=com_bcted&view=userbookingdetail&booking_id=<?php echo $booking->venue_booking_id; ?>&Itemid=<?php echo $Itemid; ?>">
										<?php //echo $booking->name; ?>
									</a>
								</div>-->
								<div class="prd-title"><?php echo $booking->service_name; ?></div>
								<div class="prd-title">
									<?php echo BctedHelper::currencyFormat($booking->currency_code,$booking->currency_sign,$booking->total_price); ?>
								</div>
								<?php
									$fromTime = explode(":", $booking->booking_from_time);
									$toTime = explode(":", $booking->booking_to_time);
								?>
								<div class="prd-date-time"><?php echo date('d-m-Y',strtotime($booking->service_booking_datetime)); ?></div>
								<div class="prd-date-time"> <?php echo $fromTime[0].':'.$fromTime[1] . ' - ' . $toTime[0].':'.$toTime[1]; ?></div>
							</div>
							<div class="bk-single-right span4">
								<?php /* if($booking->status_text == 'Booked'): ?>
									<button class="del-btn" onclick="removePastBooking('<?php echo $booking->venue_booking_id; ?>','<?php echo $this->user->id; ?>')" type="button">-</button>
								<?php endif; */ ?>
								<!-- <a href="#"><?php //echo $booking->user_status_text; ?></a>-->
								<button class="reqlnk-btn" type="button"><?php echo $booking->user_status_text; ?></button>

							</div>
							</a>
						</li>
					<?php endforeach; ?>
				<?php endif; ?>
				<?php if(!$hasServiceBooking): ?>
					<div id="system-message">
					<div class="alert alert-block">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<h4><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_FOR_COMPANY_TITLE'); ?></h4>
						<div><p><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_FOR_COMPANY_DESC'); ?></p></div>
					</div>
					</div>
				<?php endif; ?>
				</ul>
			</div>
		</div>
		<div class="tab-pane" id="packages">
			<div class="summary-list">
				<ul>
				<?php $hasPackageBooking = 0; ?>
				<?php if($this->packagesPurchase): ?>
					<?php foreach ($this->packagesPurchase    as $key => $booking): ?> <?php //echo "<pre>"; print_r($booking); echo "</pre>"; exit; ?>
						<?php $currentTimeStamp = time(); ?>
						<?php $bookingTimeStamp = strtotime($booking->package_datetime.' '.$booking->package_time); ?>
						<?php if($currentTimeStamp>$bookingTimeStamp): ?>
							<?php continue; ?>
						<?php endif; ?>
						<?php $hasPackageBooking = 1; ?>
						<li id="booking_<?php echo $booking->package_purchase_id; ?>">
						<a href="index.php?option=com_bcted&view=userbookingdetail&booking_type=package&purchase_id=<?php echo $booking->package_purchase_id; ?>&Itemid=<?php echo $Itemid; ?>">
							<div class="prd-single-left span8">
								<!--<div class="prd-person-nm">
									<a href="index.php?option=com_bcted&view=userbookingdetail&booking_id=<?php //echo $booking->venue_booking_id; ?>&Itemid=<?php echo $Itemid; ?>">
										<?php //echo $booking->name; ?>
									</a>
								</div>-->
								<div class="prd-title"><?php echo $booking->package_name; ?></div>
								<div class="prd-title">
									<?php //echo $booking->package_currency_sign .' '.$booking->package_price; ?>
									<?php if($booking->package_currency_code): ?>
										<?php echo BctedHelper::currencyFormat($booking->package_currency_code,$booking->package_currency_sign,$booking->package_price * $booking->package_number_of_guest); ?>
									<?php else:?>
										<?php echo BctedHelper::currencyFormat($booking->pakcge_current_currency_code,$booking->pakcge_current_currency_sign,$booking->package_price * $booking->package_number_of_guest); ?>
									<?php endif; ?>

								</div>
								<div class="prd-date-time"><?php echo date('d-m-Y',strtotime($booking->package_datetime)); ?></div>
							</div>
							<div class="bk-single-right span4">

								<!--<a href="#">-->
									<button class="reqlnk-btn" type="button"><?php echo $booking->user_status_text; ?></button>

							</div>
							</a>
						</li>
					<?php endforeach; ?>
				<?php endif; ?>
				<?php if(!$hasPackageBooking): ?>
					<div id="system-message">
					<div class="alert alert-block">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<h4><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_FOR_PACKAGE_TITLE'); ?></h4>
						<div><p><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_FOR_PACKAGE_DESC'); ?></p></div>
					</div>
					</div>
				<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php //echo $this->pagination->getListFooter(); ?>
