<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$document = JFactory::getDocument();
$document->addScript(Juri::root(true) . '/components/com_bcted/assets/rangeslider/js/rangeslider.js');
$document->addStyleSheet(Juri::root(true) . '/components/com_bcted/assets/rangeslider/css/rangeslider.css');

$document->addScript(Juri::root(true) . '/components/com_bcted/assets/datepicker/jquery.timepicker.js');
$document->addStyleSheet(Juri::root(true) . '/components/com_bcted/assets/datepicker/jquery.timepicker.css');

$document->addScript(Juri::root(true) . '/components/com_bcted/assets/timer/jquery.countdownTimer.js');

//$document->addScript(Juri::root(true) . '/components/com_bcted/assets/datepicker/BeatPicker.min.js');
//$document->addStyleSheet(Juri::root(true) . '/components/com_bcted/assets/datepicker/BeatPicker.min.css');


/*
<link href='<?php echo $this->baseurl; ?>/templates/bcted/css/BeatPicker.min.css' rel='stylesheet' type='text/css' />

<script src="<?php echo $this->baseurl; ?>/templates/bcted/js/BeatPicker.min.js"></script>
*/
?>
<!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>

<?php //echo "<pre>"; print_r($this->clubDetail); echo "</pre>"; exit; ?>

<?php

	$fromTime = $this->clubDetail->from_time;
	$arrayFromTime = explode(":", $fromTime);
 	$toTime = $this->clubDetail->to_time;
 	$arrayToTime = explode(":", $toTime);

 	/*echo "<pre>";
 	print_r($arrayFromTime);
 	echo "</pre>";

 	echo "<pre>";
 	print_r($arrayToTime);
 	echo "</pre>";*/

 	//$arrayFromTime[0] = 0;
 	//$arrayToTime[0] = 5;
 ?>

<?php $currentTime = date('H'); ?>

<script type="text/javascript">
	//var currentdate = new Date();
	var isDefault = 1;
	var clientTime = new Date();
	var clientHours = clientTime.getHours();
    jQuery(function() {
        jQuery('#timepicker_from').timepicker({
        	timeFormat: 'HH:mm',
        	interval: 15,
        	defaultTime: "'"+ (clientHours + 1 )+"'",
        	scrollbar: true,
			change: function(time) {
				//alert(isDefault);
				if(isDefault == 0)
				{
					var element = jQuery(this), text;
					//alert(time);
					var timepicker = element.timepicker();
					var selectedTime = timepicker.format(time);
					jQuery("#timepicker_to").timepicker('setTime', selectedTime, null);
				}

				isDefault = 0;
				checkForDate();

			}
        });

        jQuery('#timepicker_to').timepicker({
        	timeFormat: 'HH:mm',
        	defaultTime: "'"+ (clientHours + 1 )+"'",
        	interval: 15,
        	scrollbar: true
        });
    });
</script>

<script type="text/javascript">
    jQuery(function() {
        jQuery('#timepicker').timepicker({
        	timeFormat: 'HH:mm',
        	interval: 15,
            minTime: '2p',
            maxTime: '8p',
            scrollbar: true,
        });
    });
</script>
<script type="text/javascript">
/*jQuery(document).ready({
jQuery('#timepicker_from').timepicker({
    change: function(time) {
        // the input field
        //var element = jQuery(this), text;
        // get access to this TimePicker instance
        //var timepicker = element.timepicker();
        //text = 'Selected time is: ' + timepicker.format(time);
        //element.siblings('span.help-line').text(text);
        alert(timepicker.format(time));
    }
});
});*/

</script>

 <script>
jQuery(function() {
	jQuery( "#datepicker" ).datepicker({ minDate: 0, maxDate: "+1M +10D",dateFormat:"dd-mm-yy" });
	var currentDate = new Date();
	jQuery("#datepicker").datepicker("setDate", currentDate);
	jQuery('#invalid-date-time').hide();
});
</script>

<script type="text/javascript">
	function checkForVenueTableAvaibility()
	{
		//alert('checking');
		var table_id = jQuery('#table_id').val();
		var requested_date = jQuery('#datepicker').val();
		var timepicker_from = jQuery('#timepicker_from').val();
		var timepicker_to = jQuery('#timepicker_to').val();

		/*alert(requested_date);
		return false;*/

		//jQuery('tableName')

		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=clubtablebooking.checkForTableAvaibility',
			type: 'GET',
			data: 'table_id='+table_id+'&requested_date='+requested_date+'&from_time='+timepicker_from+'&to_time='+timepicker_to,

			success: function(response){

	        	if(response != "200")
	        	{
	        		jQuery('#alert-error').show();
	        		jQuery('#alert-error').html('<h4>Table not available!</h4><br />Please change your time for this table');
	        		return false;
	        	}
	        	else
	        	{

	        		jQuery('#alert-error').hide();
	        		jQuery('#form_venuetablebooking').submit();
	        	}

	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

		return false;

	}
</script>

<script type="text/javascript">
	function changedSelectedTable(selectedTable)
	{
		//alert(selectedTable.value);
		//jQuery('tableName')

		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=clubtablebooking.getTableDetail',
			type: 'GET',
			data: 'table_id='+selectedTable.value,

			success: function(response){

	        	if(response.length!=0)
	        	{

	        		var responseArray = response.split("|");
	        		//alert(responseArray[0]);
	        		var tblName = "Book " + responseArray[0] + " Table";
	        		//jQuery('#favourite-btn').hide();
	        		jQuery('#tableName').html(tblName);
	        		//jQuery('#table_id').val(selectedTable.value);
	        		//jQuery('#range').show();
	        	}
	        	else
	        	{
	        		var tblName = "Please Select Table";
	        		jQuery('#tableName').html(tblName);
	        	}

	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}
</script>

<script type="text/javascript">
	function checkForDate()
	{
		var requested_date = jQuery('#datepicker').val();
		var timepicker_from = jQuery('#timepicker_from').val();
		var timepicker_to = jQuery('#timepicker_to').val();
		var currentTime = new Date();

		//var checkindatestr = requested_date;
		var dateParts = requested_date.split("-");
		var timeParts = timepicker_from.split(":");

		var selectedDate = new Date(dateParts[2],dateParts[1] - 1, dateParts[0],timeParts[0],timeParts[1],00);



		if(currentTime.getTime() >= selectedDate.getTime())
		{
			//alert('past');
			jQuery('#invalid-date-time').show();

			return false;
		}
		else
		{
			jQuery('#invalid-date-time').hide();

			return true;

		}


	}
</script>

<?php
/*$start_date = new DateTime();
$end_date = new DateTime(date('Y-m-d'.' 20:00:00'));
echo "<pre>";
print_r($end_date);
echo "</pre>";
exit;
$since_start = $start_date->diff($end_date);
echo $since_start->days.' days total<br>';
echo $since_start->y.' years<br>';
echo $since_start->m.' months<br>';
echo $since_start->d.' days<br>';
echo $since_start->h.' hours<br>';
echo $since_start->i.' minutes<br>';
echo $since_start->s.' seconds<br>';
echo "call";
exit;*/
?>

<script type="text/javascript">
	var d = new Date();
	var day = d.getDate();
	var month = d.getMonth()+1;
	var year = d.getFullYear();
	var dateOnly =  year + '/' + month + '/' + day;

	jQuery(function(){
		jQuery('#future_date').countdowntimer({
			dateAndTime : dateOnly + ' 20:00:00',
			size : "lg",
			regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
			regexpReplaceWith: "$2h:$3m"
		});

		jQuery('#alert-error').hide();
	});
</script>
<?php
$tblName = "Please Select Table";
if($this->tableDetail)
{
	if($this->tableDetail->premium_table_id)
	{
		$tblName = "Book ". ucfirst($this->tableDetail->venue_table_name) . " Table";
	}
	else
	{
		$tblName = "Book ". ucfirst($this->tableDetail->custom_table_name) . " Table";
	}

	$maxVal = $this->tableDetail->venue_table_capacity;
}
else
{
	$maxVal = 10;
}
?>

    <div id="alert-error" class="alert alert-error">
    </div>

<div class="guest-club-wrp">
	<div class="inner-guest-wrp">
		<form id="form_venuetablebooking" class="form-horizontal" method="post" action="<?php echo JRoute::_('index.php?option=com_bcted&view=userbookings&club_id='.$this->clubID.'&Itemid='.$Itemid);?>">
			<div class="control-group">
				<label id="tableName" class="control-label req-title span12"><?php echo trim($tblName); ?></label>
				<!--<div class="controls span6">
					<div style="display:none" class="g-reqtime">Time left for today’s Guestlist: <span id="future_date"></span></div>
				</div> -->
			</div>
			<?php if($this->showtableList): ?>
				<div class="control-group">
					<label class="control-label span6">Select Table:</label>
					<div class="controls club-add-table-premium span6">
						<select class="currency_select_box" name="table_id" id="table_id" onchange="changedSelectedTable(this)">
							<option value="">Select Table</option>
							<<?php foreach ($this->allTables as $key => $table): ?>
								<?php if ($table->premium_table_id): ?>
									<option value="<?php echo $table->venue_table_id; ?>"><?php echo $table->venue_table_name; ?></option>
								<?php else: ?>
									<option value="<?php echo $table->venue_table_id; ?>"><?php echo $table->custom_table_name; ?></option>
								<?php endif; ?>
							<?php endforeach ?>
						</select>
					</div>
				</div>

			<?php else: ?>
				<input type="hidden" id="table_id" name="table_id" value="<?php echo $this->tableID; ?>" >
			<?php endif; ?>
			<div class="control-group">
				<label class="control-label span6">Date:</label>
				<div class="controls span6">
					<input type="text" onchange="checkForDate()" name="requested_date" readonly="true" class="span12" required="required" id="datepicker"/>

				</div>
			</div>

			<div class="control-group">
				<label class="control-label span6">From & To:</label>
				<div class="controls span6">
					<input type="text" onchange="checkForDate()" name="requested_from_time" readonly="true" class="span6" required="required" id="timepicker_from"/>
					<input type="text" onchange="checkForDate()" name="requested_to_time" readonly="true" class="span6" required="required" id="timepicker_to">
					<div id="invalid-date-time" class="invalid-date-time">Selected date and time is not valid</div>
				</div>
			</div>

			<div class="control-group guest-range">
				<label class="control-label span6">Number of guests:</label>
				<div class="controls span6" >
					<div class="guest-range-inner">
						<input id="range" type="range" value="0" min="1" max="<?php echo $maxVal; ?>" data-rangeslider>
						<output id="slider-value-disp"></output>
						<input type="hidden" id="guest_count" name="guest_count" value="1">
						<input type="hidden" id="male_count" name="male_count" value="1">
						<input type="hidden" id="female_count" name="female_count" value="0">
					</div>
					<div id="guest-icon" class="gust-icn">
						<a class="gust-icn-male"> </a>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label span6">Additional Information:</label>
				<div class="controls span6">
					<textarea class="span12" name="additional_information" ></textarea>
				</div>
			</div>

			<div class="control-group">
				<div class="controls span6"></div>
				<div class="controls span6">
					<button onclick="return checkForVenueTableAvaibility();" type="button" class="btn btn-large span">Book Now</button>
					<input type="hidden" id="task" name="task" value="clubtablebooking.bookVenueTable">
					<input type="hidden" id="view" name="view" value="clubguestlist">
					<input type="hidden" id="Itemid" name="Itemid" value="<?php echo $Itemid; ?>">

				</div>
			</div>
		</form>
	</div>
</div>

<?php //echo $this->pagination->getListFooter(); ?>

<script>

var $ = jQuery.noConflict();
$('#slider-value-disp').hide();
$(function() {
var $document = $(document),
selector = '[data-rangeslider]',
$element = $(selector);
// Example functionality to demonstrate a value feedback
function valueOutput(element) {
var value = element.value,
output = element.parentNode.getElementsByTagName('output')[0];
output.innerHTML = value;

}
for (var i = $element.length - 1; i >= 0; i--) {
valueOutput($element[i]);
};
$document.on('change', 'input[type="range"]', function(e) {
valueOutput(e.target);
});
// Example functionality to demonstrate disabled functionality
$document .on('click', '#js-example-disabled button[data-behaviour="toggle"]', function(e) {
var $inputRange = $('input[type="range"]', e.target.parentNode);
if ($inputRange[0].disabled) {
$inputRange.prop("disabled", false);
}
else {
$inputRange.prop("disabled", true);
}
$inputRange.rangeslider('update');
});
// Example functionality to demonstrate programmatic value changes
$document.on('click', '#js-example-change-value button', function(e) {
var $inputRange = $('input[type="range"]', e.target.parentNode),
value = $('input[type="number"]', e.target.parentNode)[0].value;
$inputRange.val(value).change();
});
// Example functionality to demonstrate destroy functionality
$document
.on('click', '#js-example-destroy button[data-behaviour="destroy"]', function(e) {
$('input[type="range"]', e.target.parentNode).rangeslider('destroy');
})
.on('click', '#js-example-destroy button[data-behaviour="initialize"]', function(e) {
$('input[type="range"]', e.target.parentNode).rangeslider({ polyfill: false });
});
// Example functionality to test initialisation on hidden elements
$document
.on('click', '#js-example-hidden button[data-behaviour="toggle"]', function(e) {
var $container = $(e.target.previousElementSibling);
$container.toggle();
});
// Basic rangeslider initialization
$element.rangeslider({
// Deactivate the feature detection
polyfill: false,
// Callback function
onInit: function() {},
// Callback function
onSlide: function(position, value) {
console.log('onSlide');
console.log('position: ' + position, 'value: ' + value);
},
// Callback function
onSlideEnd: function(position, value) {
console.log('onSlideEnd');
console.log('position: ' + position, 'value: ' + value);

	newHtml = '';
	oldValue = $('#guest_count').val();
	oldMale = $('#male_count').val();


	/*for(i = 0 ; i < value; i++)
	{
		newHtml = newHtml + '<a class="gust-icn-male" href="#"> </a>';
	}*/

	if(oldValue == value)
	{

	}
	else if(oldValue < value)
	{
		//$("#guest-icon a").length


		appendGuest = value - oldValue;
		//alert("Old : " + oldValue + " current : " + value + " Callback : " + appendGuest);
		newHtml ='';
		for(i = 0; i < appendGuest; i++)
		{
			newHtml = newHtml + '<a class="gust-icn-male" > </a>';

		}
		$('#guest-icon').append(newHtml);
		$('#guest_count').val(value);

		/*newMale = oldMale + appendGuest;
		alert(newMale);*/

		newMale = parseInt(oldMale) + parseInt(appendGuest);
		//alert(newMale);


		$('#male_count').val(newMale);

	}
	else if(oldValue > value)
	{
		retmoveGuest = value - 1;
		$("#guest-icon > a:gt("+retmoveGuest+")").remove();

		/*$('.gust-icn-male').length;
		$('.gust-icn-female').length;*/

		$('#guest_count').val(value);
		$('#male_count').val($('.gust-icn-male').length);
		$('#female_count').val($('.gust-icn-female').length);
	}

}
});
});


$(document).on('click', "a.gust-icn-male", function() {
    /*var liId = $(this);
    alert(liId);*/
    if(this.hasClass('gust-icn-male'))
    {
    	this.removeClass('gust-icn-male');
    	this.addClass(' gust-icn-female');

    	var male_count   = $('#male_count').val();
		var female_count = $('#female_count').val();

		$('#male_count').val(parseInt(male_count) - 1);
		$('#female_count').val(parseInt(female_count) + 1);
    }
});

$(document).on('click', "a.gust-icn-female", function() {
    /*var liId = $(this);
    alert(liId);*/
    if(this.hasClass('gust-icn-female'))
    {
    	this.removeClass('gust-icn-female');
    	this.addClass(' gust-icn-male');

		var male_count   = $('#male_count').val();
		var female_count = $('#female_count').val();

		$('#male_count').val(parseInt(male_count) + 1);
		$('#female_count').val(parseInt(female_count) - 1);
    }
});


</script>
