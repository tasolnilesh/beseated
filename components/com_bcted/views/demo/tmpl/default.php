<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;

?>
<script type="text/javascript">
    jQuery(function() {
    	jQuery("#display_venue_image").click(function(){
			jQuery("#upl").click();
		});

		// Add events
		jQuery('input[type=file]').on('change', fileUpload);

		function fileUpload(event){
			jQuery("#uploading_msg").html("<p>"+event.target.value+" uploading...</p>");
			jQuery('#spinner').fadeIn('fast');
			files = event.target.files;
			var data = new FormData();
			var error = 0;
			for (var i = 0; i < files.length; i++) {
	  			var file = files[i];
	  			console.log(file.size);
	  			console.log("File Pritned fewllow");
	  			console.log(file.type);
				if(!file.type.match('image.*') && !file.type.match('video.*')) {
			   		jQuery("#drop-box").html("<p> Images only. Select another file</p>");
			   		error = 1;
			  	}/*else if(file.size > 1048576){
			  		jQuery("#drop-box").html("<p> Too large Payload. Select another file</p>");
			   		error = 1;
			  	}*/else{
			  		data.append('image', file, file.name);
			  	}
		 	}
		 	if(!error){
			 	var xhr = new XMLHttpRequest();
			 	xhr.open('POST', 'index.php?option=com_bcted&task=profile.uploadImage', true);
			 	xhr.send(data);
			 	xhr.onload = function () {
					if (xhr.status === 200) {
						jQuery("#uploading_msg").html("<p> File Uploaded. Select more files</p>");
					} else {
						jQuery("#uploading_msg").html("<p> Error in upload, try again.</p>");
					}
					jQuery('#spinner').stop().fadeOut('fast');
				};
			}
		}
    });
</script>
<style>
div#spinner
{
    display: none;
    width:100px;
    height: 100px;
    position: fixed;
    top: 50%;
    left: 50%;
    text-align:center;
    margin-left: -50px;
    margin-top: -100px;
    z-index:2;
    overflow: auto;
}
</style>
<div id="spinner">
    <img src="<?php echo JUri::base(); ?>images/bcted/default/ajax-loader.gif" alt="Loading..."/>
</div>
<script type="text/javascript">
    jQuery('#spinner').ajaxStart(function () {
        jQuery(this).fadeIn('fast');
    }).ajaxStop(function () {
        jQuery(this).stop().fadeOut('fast');
    });

</script>

<form class="form-horizontal prf-form" enctype="multipart/form-data" method="post" accept="<?php echo JRoute::_('index.php?option=com_bcted&view=profile'); ?>">
	<div class="row-fluid prof-locatnwrp">
		 <div class="span6 prof-img" style="float: left;" id="drop-box">
		 	<img id="display_venue_image" src="images/bcted/default/banner.png" alt="" />
		 	<div class="column-small-12 padd0">
				<input type="file" name="upl" id="upl" />
				<input type="hidden" name="club_id" id="club_id" value="101">
			</div>
		 </div>
		 <div id="uploading_msg"></div>
	</div>
</form>
