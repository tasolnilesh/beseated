<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');


$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');
$clubID = $input->get('club_id', 0, 'int');

/*BctedHelper::isFavouriteVenue();
echo "end";
exit;*/
$app = JFactory::getApplication();
$menu = $app->getMenu();
$menuItem = $menu->getItems( 'link', 'index.php?option=com_users&view=login', true );
$loginItemid = $menuItem->id;

$bctParams = BctedHelper::getExtensionParam_2();


$accessLevel = BctedHelper::getGroupAccessLevel($bctParams->guest);

$access = array('access','link');
$property = array($accessLevel,'index.php?option=com_bcted&view=clubinformation');
$menuItem2 = $menu->getItems( $access, $property, true );

/*echo "<pre>";
print_r($menuItem2);
echo "</pre>";
exit;*/

//$Itemid2 = $menuItem2->id;
$link2 = 'index.php?option=com_bcted&view=clubinformation&club_id='.$clubID.'&Itemid='.$menuItem2->id;

$loginLink = 'index.php?option=com_users&view=login&Itemid='.$loginItemid.'&return='.base64_encode($link2);

?>


<script type="text/javascript">
	function addVenueToFavourite(venueID,userID)
	{

		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=venues.addtofavourite',
			type: 'GET',
			data: '&venue_id='+venueID+'&user_id='+userID,

			success: function(response){

	        	if(response == "1" || response == "2")
	        	{
	        		//jQuery('#favourite-btn').hide();
	        		jQuery('#favourite-add').hide();
	        		jQuery('#favourite-remove').show();
	        	}
	        	else if(response == "3")
	        	{
	        		window.location.href="<?php echo $loginLink; ?>";
	        	}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}

	function removeVenueFromFavourite(venueID,userID)
	{

		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=venues.removefromfavourite',
			type: 'GET',
			data: '&venue_id='+venueID+'&user_id='+userID,

			success: function(response){

	        	if(response == "1" || response == "2")
	        	{
	        		//jQuery('#favourite-btn').hide();
	        		jQuery('#favourite-add').show();
	        		jQuery('#favourite-remove').hide();
	        	}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}
</script>
<div class="venuedetail clb-infowrp">
	<div class="venue-img">
		<div class="info-image-only">

			<?php if(file_exists($this->club->venue_video)): ?>
				<!--<video width="400" controls>
					<source src="<?php //echo JUri::base().$this->club->venue_video; ?>" type="video/mp4">
					Your browser does not support HTML5 video.
				</video>-->
				<?php /*<link href="<?php echo JUri::base().'components/com_bcted/assets/video-js/video-js.css'; ?>" rel="stylesheet" type="text/css">
				<script src="<?php echo JUri::base().'components/com_bcted/assets/video-js/video.js'; ?>"></script>
				<script>
					videojs.options.flash.swf = "<?php echo JUri::base().'components/com_bcted/assets/video-js/video-js1.swf'; ?>";
				</script>

				<video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="640" height="264"
				poster="<?php echo JUri::base().$this->club->venue_image; ?>"
				data-setup="{}">
					<source src="<?php echo JUri::base().$this->club->venue_video; ?>" type='video/mp4' />
					<source src="<?php echo JUri::base().$this->club->venue_video; ?>" type='video/webm' />
					<source src="<?php echo JUri::base().$this->club->venue_video; ?>" type='video/ogg' />


				</video> */ ?>

				<video width="700" height="460" controls>
					<source src="<?php echo JUri::base().$this->club->venue_video_webm; ?>" type='video/webm;codecs="vp8, vorbis"'/>
					<source src="<?php echo JUri::base().$this->club->venue_video; ?>" type="video/mp4">
					Your browser does not support HTML5 video.
				</video>


			<?php elseif(file_exists($this->club->venue_image)): ?>
				<img src="<?php echo $this->club->venue_image; ?>" alt="" />
			<?php else: ?>
				<img src="images/bcted/default/banner.png" alt="" />
			<?php endif; ?>
		</div>
		<div class="rating-title">

			<div class="favourite-wrp span6">

					<button id="favourite-add" onclick="addVenueToFavourite('<?php echo $this->club->venue_id; ?>','<?php echo $this->user->id; ?>')" type="button" class="fav-btn"></button>

					<button id="favourite-remove" onclick="removeVenueFromFavourite('<?php echo $this->club->venue_id; ?>','<?php echo $this->user->id; ?>')" type="button" class="fav-btn active"></button>
				<?php if(!$this->isFavourite):?>
					<script type="text/javascript">
						jQuery('#favourite-add').show();
		        		jQuery('#favourite-remove').hide();
					</script>
				<?php else: ?>
					<script type="text/javascript">
						jQuery('#favourite-add').hide();
	        			jQuery('#favourite-remove').show();
	        		</script>
				<?php endif; ?>
			</div>
			<div class="rating-wrp span6">
				<?php //echo floor($this->club->venue_rating); ?>
				<?php $overallRating = $this->club->venue_rating; ?>
            	<?php $starValue = floor($this->club->venue_rating); ?>
            	<?php $maxRating = 5; ?>
            	<?php $printedStart = 0 ;?>
				<?php for($i = 1; $i <= $starValue;$i++): ?>
					<i class="full"> </i>
					<?php $printedStart = $printedStart + 1; ?>
				<?php endfor; ?>
				<?php if($starValue<$overallRating): ?>
					<i class="half"> </i>
					<?php $printedStart = $printedStart + 1; ?>
				<?php endif; ?>
				<?php if($printedStart < $maxRating): ?>
					<?php for($i = $maxRating-$printedStart; $i > 0;$i--): ?>
						<i class="empty"> </i>
					<?php endfor; ?>
				<?php endif; ?>
				<?php /*for($i = 1; $i <= floor($this->club->venue_rating);$i++): ?>
					<i class="full"> </i>
				<?php endfor; ?>
				<?php for($i = floor($this->club->venue_rating)+1; $i <= 5; $i++): ?>
					<i class="empty"> </i>
				<?php endfor;*/ ?>
			</div>
			<div class="category-wrp span6">
				<?php if($this->club->is_smart): ?>
					<a><img src="images/bcted/default/amenity_smart.png"></a>
				<?php endif; ?>
				<?php if($this->club->is_casual): ?>
					<a><img src="images/bcted/default/amenity_casual.png"></a>
				<?php endif; ?>

				<?php if($this->club->is_food): ?>
					<a><img src="images/bcted/default/amenity_food.png"></a>
				<?php endif; ?>
				<?php if($this->club->is_drink): ?>
					<a><img src="images/bcted/default/amenity_drink.png"></a>
				<?php endif; ?>

				<?php if($this->club->is_smoking): ?>
					<a><img src="images/bcted/default/amenity_smoking.png"></a>
				<?php else: ?>
					<a><img src="images/bcted/default/amenity_nosmoking.png"></a>
				<?php endif; ?>


			</div>
			<div class="sign-wrp span6">
				<?php if($this->club->venue_signs == 1): ?>
					<?php echo "$"; ?>
				<?php elseif($this->club->venue_signs == 2): ?>
					<?php echo "$$"; ?>
				<?php elseif($this->club->venue_signs == 3): ?>
					<?php echo "$$$"; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>


	<div class="venue-text">
		<?php echo $this->club->venue_about; ?>
	</div>

	<div class="venue-text">
		<?php echo JText::_('COM_BCTED_FROM_TIME') . ' ' . $this->club->from_time;?>
		<?php echo JText::_('COM_BCTED_TO_TIME') .  ' ' . $this->club->to_time;?>
	</div>
	<div class="venue-text">
		<?php echo JText::_('COM_BCTED_CLUB_INFORMATION_WORKING_DAYS');?>
		<?php
			if(!empty($this->club->working_days))
			{
				$days = explode(",", $this->club->working_days);
				$workingDay = array();
				if(in_array(7, $days))
				{
					$workingDay[] = JText::_('COM_BCTED_SUNDAY');
				}
				if(in_array(1, $days))
				{
					$workingDay[] = JText::_('COM_BCTED_MONDAY');
				}
				if(in_array(2, $days))
				{
					$workingDay[] = JText::_('COM_BCTED_TUESDAY');
				}
				if(in_array(3, $days))
				{
					$workingDay[] = JText::_('COM_BCTED_WEDNESDAY');
				}
				if(in_array(4, $days))
				{
					$workingDay[] = JText::_('COM_BCTED_THURSDAY');
				}
				if(in_array(5, $days))
				{
					$workingDay[] = JText::_('COM_BCTED_FRIDAY');
				}
				if(in_array(6, $days))
				{
					$workingDay[] = JText::_('COM_BCTED_SATURDAY');
				}

				echo ' : '. implode(",", $workingDay);

			}
		?>
	</div>
</div>




