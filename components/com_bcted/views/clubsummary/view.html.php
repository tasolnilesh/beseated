<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Summary View
 *
 * @since  0.0.1
 */
class BctedViewClubSummary extends JViewLegacy
{
	protected $bookings;

	protected $pagination;

	protected $state;

	protected $user;

	protected $totalRequests;

    protected $totalBooking;

    protected $totalRevenue;

    protected $elementDetail;


	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		$this->bookings         = $this->get('Items');
		// Get data from the model
		$this->pagination    = $this->get('Pagination');

		/*$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');*/
		$this->user = JFactory::getUser();

		if(!$this->user->id)
		{
			JFactory::getApplication()->redirect(JRoute::_(JURI::root().'index.php?option=com_users&view=login'), 'Please login first!');
		}

		//$this->clubDetail =

		$model = $this->getModel();
		$user = JFactory::getUser();
		$this->elementDetail = BctedHelper::getUserElementID($user->id);
		$summaries = $model->summaryForVenue($this->elementDetail->venue_id);
		$this->totalRequests = 0;
		$this->totalBooking = 0;

		foreach ($summaries as $key => $summary)
		{
			$statusText = BctedHelper::getStatusNameFromStatusID($summary->status);

			if($statusText == 'Request')
			{
				$this->totalRequests = $summary->total_count;
			}
			else if($statusText == 'Booked')
			{
				$this->totalBooking = $summary->total_count;
			}

			$statusText = "";
		}

		$this->totalRevenue = $model->getRevenueForVenue($this->elementDetail->venue_id);

		// Display the template
		parent::display($tpl);
	}



}
