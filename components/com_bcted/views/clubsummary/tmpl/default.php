<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$ItemidRequest = BctedHelper::getBctedMenuItem('requests');
$ItemidBooked = BctedHelper::getBctedMenuItem('club-bookings');

?>

<?php $app = JFactory::getApplication(); ?>
<?php $menu = $app->getMenu(); ?>


<div class="bct-summary-container">
    <div class="row">
        <div class="summary-statustrics span12">
            <div class="cls-bookings span4">
                <?php $menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=clubbookings', true ); ?>
                <a href="index.php?option=com_bcted&view=clubbookings&Itemid=<?php echo $menuItem->id; ?>">
                <div class="cir-wrp">
                    <div class="smry-counter"><?php echo $this->totalBooking; ?></div>
                </div>
                <div class="cir-bx-title">
                    <?php echo JText::_('COM_BCTED_VIEW_CLUBSUMMARY_BOOKINGS'); ?>
                </div>
                </a>
            </div>
            <div class="cls-request span4">

                <?php $menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=clubrequests', true ); ?>
                <a href="index.php?option=com_bcted&view=clubrequests&Itemid=<?php echo $menuItem->id; ?>">
                <div class="cir-wrp">
                    <div class="smry-counter"><?php echo $this->totalRequests; ?></div>
                </div>
                <div class="cir-bx-title">
                    <?php echo JText::_('COM_BCTED_VIEW_CLUBSUMMARY_REQUESTS'); ?>
                </div>
                </a>
            </div>
            <div class="cls-revenu span4">
                <?php $menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=clubaccounthistory', true ); ?>
                <a href="index.php?option=com_bcted&view=clubaccounthistory&Itemid=<?php echo $menuItem->id; ?>">
                <div class="cir-wrp">
                    <div class="smry-counter" style="font-size:22px;"><?php echo ($this->totalRevenue)?$this->elementDetail->currency_sign.' '.number_format($this->totalRevenue,0):'0'; ?></div>
                </div>
                <div class="cir-bx-title">
                    <?php echo JText::_('COM_BCTED_VIEW_CLUBSUMMARY_REVENUE'); ?>
                </div>
                </a>
            </div>
        </div>
    </div>
    <div class="summary-list">
        <ul>
        <?php /*for($i = 0; $i<10;$i++): ?>
            <li>
            <div class="prd-single-left span8">
                <div class="prd-title">product</div>
                <div class="prd-person-nm">persons name</div>
                <div class="prd-date-time">Date & Time</div>
            </div>
            <div class="prd-single-right span4">
                <button class="reqlnk-btn" type="button">request</button>
            </div>
            </li>
        <?php endfor;*/ ?>
        <?php if($this->bookings): ?>
            <?php foreach ($this->bookings as $key => $booking): //echo "<pre>"; print_r($booking); echo "</pre>";exit; ?>
                <?php $statusText=trim($booking->status_text); ?>
                <?php if($statusText=='Booked'): ?>
                    <li class="green-booking">
                <?php else: ?>
                    <li>
                <?php endif; ?>

                    <?php $statusText=trim($booking->status_text); ?>
                    <?php
                        $fromTime = explode(":", $booking->booking_from_time);
                        $toTime = explode(":", $booking->booking_to_time);
                    ?>
                    <?php if($statusText == 'Request'): ?>
                        <?php $link = JRoute::_('index.php?option=com_bcted&view=clubrequestdetail&request_id='.$booking->venue_booking_id.'&Itemid='.$ItemidRequest->id); ?>
                    <?php elseif($statusText == 'Awaiting Payment'): ?>
                        <?php $link = JRoute::_('index.php?option=com_bcted&view=clubrequestdetail&request_id='.$booking->venue_booking_id.'&Itemid='.$ItemidRequest->id); ?>
                    <?php elseif($statusText == 'Booked'): ?>
                        <?php $link = JRoute::_('index.php?option=com_bcted&view=clubbookingdetail&booking_id='.$booking->venue_booking_id.'&Itemid='.$ItemidBooked->id); ?>
                    <?php elseif($statusText == 'Waiting List'): ?>
                        <?php $link = JRoute::_('index.php?option=com_bcted&view=clubrequestdetail&request_id='.$booking->venue_booking_id.'&Itemid='.$ItemidRequest->id); ?>
                    <?php else: ?>
                        <?php $link = '#'; ?>
                    <?php endif; ?>
                    <a href="<?php echo $link; ?>">
                    <div class="prd-single-left span8">
                        <div class="prd-title"><?php echo $booking->venue_table_name; ?></div>
                        <div class="prd-person-nm"><?php echo $booking->name; ?></div>
                        <div class="prd-date-time"><b>Date : </b><?php echo date('d-m-Y',strtotime($booking->venue_booking_datetime)); ?></div>
                        <div class="prd-date-time"><b>Timing : </b><?php echo $fromTime[0].':'.$fromTime[1] . ' - ' . $toTime[0].':'.$toTime[1]; ?></div>
                    </div>
                    <div class="prd-single-right span4">

                        <?php //echo trim($booking->status_text);  index.php?option=com_bcted&view=clubrequestdetail&request_id=15&Itemid=127 ?>

                        <?php if($statusText == 'Request'): ?>
                            <?php $link = JRoute::_('index.php?option=com_bcted&view=clubrequestdetail&request_id='.$booking->venue_booking_id.'&Itemid='.$ItemidRequest->id); ?>
                            <a href="<?php echo $link; ?>"><button class="reqlnk-btn" type="button"><?php echo trim($booking->status_text); ?></button></a>
                        <?php elseif($statusText == 'Awaiting Payment'): ?>
                            <?php $link = JRoute::_('index.php?option=com_bcted&view=clubrequestdetail&request_id='.$booking->venue_booking_id.'&Itemid='.$ItemidRequest->id); ?>
                            <a href="<?php echo $link; ?>"><button class="reqlnk-btn" type="button"><?php echo trim($booking->status_text); ?></button></a>
                        <?php elseif($statusText == 'Booked'): ?>
                            <?php $link = JRoute::_('index.php?option=com_bcted&view=clubbookingdetail&booking_id='.$booking->venue_booking_id.'&Itemid='.$ItemidBooked->id); ?>
                            <a href="<?php echo $link; ?>"><button class="reqlnk-btn" type="button"><?php echo trim($booking->status_text); ?></button></a>
                        <?php elseif($statusText == 'Waiting List'): ?>
                            <?php $link = JRoute::_('index.php?option=com_bcted&view=clubrequestdetail&request_id='.$booking->venue_booking_id.'&Itemid='.$ItemidRequest->id); ?>
                            <a href="<?php echo $link; ?>"><button class="reqlnk-btn" type="button"><?php echo trim($booking->status_text); ?></button></a>
                        <?php else: ?>
                            <button class="reqlnk-btn" type="button"><?php echo trim($booking->status_text); ?></button>
                        <?php endif; ?>

                    </div>
                    </a>
                </li>
            <?php endforeach; ?>
        <?php else: ?>
            <div id="system-message">
                <div class="alert alert-block">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_GENERAL_TITLE'); ?></h4>
                    <div><p><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_FOR_GENERAL_DESC'); ?></p></div>
                </div>
            </div>
        <?php endif; ?>
        </ul>
    </div>
</div>

<?php echo $this->pagination->getListFooter(); ?>
