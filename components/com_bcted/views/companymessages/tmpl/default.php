<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$user = JFactory::getUser();

$userType = BctedHelper::getUserGroupType($user->id);
$passUserType = '';
if($userType == 'Club')
    $passUserType = 'venue';
elseif($userType == 'ServiceProvider')
    $passUserType = 'company';
elseif($userType == 'Registered')
    $passUserType = 'user';
?>
<script type="text/javascript">

    function deleteMessage(userType, venueID, companyID, userID,connectionID)
    {
        //alert(bookingID);
        jQuery.ajax({
            url: 'index.php?option=com_bcted&task=message.delete_message',
            type: 'GET',
            data: '&delete_by='+userType+'&venue_id='+venueID+'&company_id='+companyID+'&userid='+userID,

            success: function(response){

                if(response == "200")
                {
                    jQuery('#msg_'+connectionID).remove();
                    jQuery('#system-message').show();
                }
            }
        })
        .done(function() {
            //console.log("success");
        })
        .fail(function() {
            //console.log("error");
        })
        .always(function() {
            //console.log("complete");
        });

    }
</script>
<div class="table-wrp message-wrp">
    <div class="message-list">
		<ul>
            <?php foreach ($this->messages as $key => $message): //echo "<pre>"; print_r($message); echo "</pre>"; ?>
                <li id="msg_<?php echo $message->connection_id; ?>">

                 <?php if($this->user->id == $message->from_userid): ?>
                            <?php $otherUser = JFactory::getUser($message->to_userid); ?>
                        <?php else: ?>
                            <?php $otherUser = JFactory::getUser($message->from_userid); ?>

                        <?php endif; ?>
                        <?php $link = JRoute::_("index.php?option=com_bcted&view=messagedetail&user_id=".$otherUser->id."&connection_id=".$message->connection_id."&Itemid=".$Itemid); ?>

                <a href="<?php echo $link; ?>" >
                    <div class="prd-single-left span8" style="color:white;">
                        <div class="msg-title" >

                            <?php /*if($message->message_type == "system"): ?>
                                <?php $link="#"; ?>
                            <?php endif;*/ ?>
                           <!--  <a href=" <?php //echo $link; ?>"> -->
                                <?php echo $otherUser->name; ?>
                            <!-- </a> -->
                        </div>
                        <div class="msg-dscr" ><?php echo $message->message; ?></div>
                        <div class="msg-datetime" ><?php echo date('d-m-Y H:i',strtotime($message->created)); ?></div>
                    </div>
                </a>
                    <div class="bk-single-right span4">
                    <?php if($userType == 'Club'): ?>
                        <?php

                            //$userType  = $passUserType;
                            $venueID   = 0;
                            $companyID = 0;
                            $userID    = $otherUser->id;
                        ?>
                        <button class="del-btn" type="button" onclick="deleteMessage('<?php echo $passUserType; ?>','<?php echo $venueID; ?>','<?php echo $companyID; ?>','<?php echo $userID; ?>','<?php echo $message->connection_id; ?>')">-</button>
                    <?php elseif($userType == 'ServiceProvider'): ?>
                        <?php

                            //$userType  = $passUserType;
                            $venueID   = 0;
                            $companyID = 0;
                            $userID    = $otherUser->id;
                        ?>
                        <button class="del-btn" type="button" onclick="deleteMessage('<?php echo $passUserType; ?>','<?php echo $venueID; ?>','<?php echo $companyID; ?>','<?php echo $userID; ?>','<?php echo $message->connection_id; ?>')">-</button>
                    <?php elseif($userType == 'Registered'): ?>
                        <?php

                            //$userType  = $passUserType;
                            $venueID   = $message->venue_id;
                            $companyID = $message->company_id;
                            $userID    = $otherUser->id;
                        ?>
                        <button class="del-btn" type="button" onclick="deleteMessage('<?php echo $passUserType; ?>','<?php echo $venueID; ?>','<?php echo $companyID; ?>','<?php echo $userID; ?>','<?php echo $message->connection_id; ?>')">-</button>
                    <?php endif; ?>
                    </div>
                </li>

            <?php endforeach; ?>
        </ul>
    </div>
</div>

