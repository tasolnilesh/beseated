<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Messages View
 *
 * @since  0.0.1
 */
class BctedViewCompanyMessages extends JViewLegacy
{
	protected $messages;

	protected $pagination;

	protected $state;

	protected $user;

	protected $userType;
	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		// Get data from the model
		$this->messages = $this->get('Items');
		$this->user     = JFactory::getUser();

		/*echo "<pre>";
		print_r($this->items);
		echo "</pre>";*/
		/*$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');*/

		//$this->filterForm    = $this->get('FilterForm');
		//$this->activeFilters = $this->get('ActiveFilters');

		if(!$this->user->id)
		{
			$app = JFactory::getApplication();
			$app->redirect('index.php?option=com_users&view=login');
		}

		$this->userType = BctedHelper::getUserGroupType($this->user->id);

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Set the tool-bar and number of found items
		//$this->addToolBar();
		//$this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}


}
