<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

?>

<div class="bct-summary-container">
	<div class="tab-pane" id="packagesinvited">
			<div class="summary-list">
				<ul>
				<?php $hasInvitation = 0; ?>
				<?php if($this->packagesInvitation): ?>
					<?php foreach ($this->packagesInvitation    as $key => $booking): ?> <?php //echo "<pre>"; print_r($booking); echo "</pre>"; exit; ?>
						<?php $packageTimeStamp = strtotime($booking->package_datetime.' '.$package_time); ?>
						<?php $currentTimeStamp = time(); ?>
						<?php if($currentTimeStamp>$packageTimeStamp): ?>
							<?php continue; ?>
						<?php endif; ?>

						<?php $hasInvitation = 1; ?>
						<li id="booking_<?php echo $booking->package_purchase_id; ?>">
							<a href="index.php?option=com_bcted&view=userpackageinvitedetail&package_invite_id=<?php echo $booking->package_invite_id; ?>&Itemid=<?php echo $Itemid; ?>">
								<div class="prd-single-left span8">
									<div class="prd-title"><?php echo $booking->package_name; ?></div>
									<div class="prd-title">
									<?php
										//echo "<pre/>";print_r($booking);exit;
										echo BctedHelper::currencyFormat($booking->currency_code,$booking->currency_sign,$booking->total_price);
									?></div>
									<div class="prd-date-time"><?php echo date('d-m-Y',strtotime($booking->package_datetime)); ?></div>
								</div>
								<div class="bk-single-right span4">
									<button class="reqlnk-btn" type="button"><?php echo $booking->status_text; ?></button>
								</div>
							</a>
						</li>
					<?php endforeach; ?>
				<?php endif; ?>
				<?php if(!$hasInvitation): ?>
					<div id="system-message">
						<div class="alert alert-block">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<h4><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_PACKAGE_INVITATION_FOUND_TITLE'); ?></h4>
							<div><p><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_PACKAGE_INVITATION_FOUND_DESC'); ?></p></div>
						</div>
					</div>
				<?php endif; ?>
				</ul>
			</div>
	</div>
</div>

<?php //echo $this->pagination->getListFooter(); ?>
