<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$document = JFactory::getDocument();
$document->addScript(Juri::root(true) . '/components/com_bcted/assets/rangeslider/js/rangeslider.js');
$document->addStyleSheet(Juri::root(true) . '/components/com_bcted/assets/rangeslider/css/rangeslider.css');

$document->addScript(Juri::root(true) . '/components/com_bcted/assets/datepicker/jquery.timepicker.js');
$document->addStyleSheet(Juri::root(true) . '/components/com_bcted/assets/datepicker/jquery.timepicker.css');

$document->addScript(Juri::root(true) . '/components/com_bcted/assets/timer/jquery.countdownTimer.js');

//$document->addScript(Juri::root(true) . '/components/com_bcted/assets/datepicker/BeatPicker.min.js');
//$document->addStyleSheet(Juri::root(true) . '/components/com_bcted/assets/datepicker/BeatPicker.min.css');


/*
<link href='<?php echo $this->baseurl; ?>/templates/bcted/css/BeatPicker.min.css' rel='stylesheet' type='text/css' />

<script src="<?php echo $this->baseurl; ?>/templates/bcted/js/BeatPicker.min.js"></script>
*/
?>
<!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
<?php $currentTime = date('H'); ?>
<script type="text/javascript">
	var isDefault = 1;
    jQuery(function() {
        jQuery('#timepicker_from').timepicker({
        	timeFormat: 'HH:mm',
        	interval: 15,
        	defaultTime: '<?php echo $currentTime; ?>',
        	scrollbar: true,
        	change: function(time) {
        		if(isDefault == 0)
				{
					var element = jQuery(this), text;
					var timepicker = element.timepicker();
					var selectedTime = timepicker.format(time);
					jQuery("#timepicker_to").timepicker('setTime', selectedTime, null);
				}

				isDefault = 0;
			}
        });

        jQuery('#timepicker_to').timepicker({
        	timeFormat: 'HH:mm',
        	interval: 15,
        	scrollbar: true,
        	defaultTime: '<?php echo $currentTime; ?>'
        });
    });
</script>

 <script>
jQuery(function() {
	var currentDate = new Date();
	jQuery( "#datepicker" ).datepicker({minDate: 0, maxDate: "+1M +10D",dateFormat:"dd-mm-yy" });
	jQuery("#datepicker").datepicker("setDate", currentDate);
});
</script>



<?php
/*$start_date = new DateTime();
$end_date = new DateTime(date('Y-m-d'.' 20:00:00'));
echo "<pre>";
print_r($end_date);
echo "</pre>";
exit;
$since_start = $start_date->diff($end_date);
echo $since_start->days.' days total<br>';
echo $since_start->y.' years<br>';
echo $since_start->m.' months<br>';
echo $since_start->d.' days<br>';
echo $since_start->h.' hours<br>';
echo $since_start->i.' minutes<br>';
echo $since_start->s.' seconds<br>';
echo "call";
exit;*/
?>

<script type="text/javascript">
	var d = new Date();
	var day = d.getDate();
	var month = d.getMonth()+1;
	var year = d.getFullYear();
	var dateOnly =  year + '/' + month + '/' + day;

	jQuery(function(){
		jQuery('#future_date').countdowntimer({
			dateAndTime : dateOnly + ' 20:00:00',
			size : "lg",
			regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
			regexpReplaceWith: "$2h:$3m"
		});
	});

</script>
<?php


$serviceName = "Book ".$this->companyDetail->company_name." Service";
$maxVal = 10;
?>
<div class="guest-club-wrp">
	<div class="inner-guest-wrp">
		<form class="form-horizontal" method="post" action="<?php echo JRoute::_('index.php?option=com_bcted&service_id='.$this->serviceID.'&company_id='.$this->companyID.'&Itemid='.$this->Itemid);?>">
			<div class="control-group">
				<label id="serviceName" class="control-label req-title span12" ><?php echo $serviceName; ?></label>
			</div>
			<input type="hidden" name="company_id" value="<?php echo $this->companyDetail->company_id; ?>" >
			<div class="control-group">
				<label class="control-label span6">Date	of flight : </label>
				<div class="controls span6">
					<input type="text" name="date_of_flight" readonly="true" class="span12" required="required" id="datepicker"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label span6">Flying From : </label>
				<div class="controls span6">
					<input type="text" name="flying_from"  class="span12" required="required" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label span6">Flying To : </label>
				<div class="controls span6">
					<input type="text" name="flying_to"  class="span12" required="required" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label span6">Additional Stops : </label>
				<div class="controls span6">
					<input type="text" name="additional_stops"  class="span12" />
				</div>
			</div>
			<div class="control-group guest-range">
				<label class="control-label span6">Number of guests:</label>
				<div class="controls span6" >
					<div class="guest-range-inner">
						<input type="range" value="0" min="1" max="10" data-rangeslider>
						<output id="slider-value-disp"></output>
						<input type="hidden" id="guest_count" name="guest_count" value="1">
						<input type="hidden" id="male_count" name="male_count" value="1">
						<input type="hidden" id="female_count" name="female_count" value="0">
					</div>
					<div id="guest-icon" class="gust-icn">
						<a class="gust-icn-male" > </a>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label span6">Additional Information : </label>
				<div class="controls span6">
					<input type="text" name="additional_info"  class="span12" />
				</div>
			</div>



			<div class="control-group">
				<div class="controls span6"></div>
				<div class="controls span6">
					<button type="submit" class="btn btn-large span">Book Now</button>
					<input type="hidden" id="task" name="task" value="jetservicebooking.bookJetService">
					<input type="hidden" id="view" name="view" value="">
					<input type="hidden" id="Itemid" name="Itemid" value="<?php echo $Itemid; ?>">
				</div>
			</div>
		</form>
	</div>
</div>

<?php //echo $this->pagination->getListFooter(); ?>

<script>

var $ = jQuery.noConflict();
$('#slider-value-disp').hide();
$(function() {
var $document = $(document),
selector = '[data-rangeslider]',
$element = $(selector);
// Example functionality to demonstrate a value feedback
function valueOutput(element) {
var value = element.value,
output = element.parentNode.getElementsByTagName('output')[0];
output.innerHTML = value;

}
for (var i = $element.length - 1; i >= 0; i--) {
valueOutput($element[i]);
};
$document.on('change', 'input[type="range"]', function(e) {
valueOutput(e.target);
});
// Example functionality to demonstrate disabled functionality
$document .on('click', '#js-example-disabled button[data-behaviour="toggle"]', function(e) {
var $inputRange = $('input[type="range"]', e.target.parentNode);
if ($inputRange[0].disabled) {
$inputRange.prop("disabled", false);
}
else {
$inputRange.prop("disabled", true);
}
$inputRange.rangeslider('update');
});
// Example functionality to demonstrate programmatic value changes
$document.on('click', '#js-example-change-value button', function(e) {
var $inputRange = $('input[type="range"]', e.target.parentNode),
value = $('input[type="number"]', e.target.parentNode)[0].value;
$inputRange.val(value).change();
});
// Example functionality to demonstrate destroy functionality
$document
.on('click', '#js-example-destroy button[data-behaviour="destroy"]', function(e) {
$('input[type="range"]', e.target.parentNode).rangeslider('destroy');
})
.on('click', '#js-example-destroy button[data-behaviour="initialize"]', function(e) {
$('input[type="range"]', e.target.parentNode).rangeslider({ polyfill: false });
});
// Example functionality to test initialisation on hidden elements
$document
.on('click', '#js-example-hidden button[data-behaviour="toggle"]', function(e) {
var $container = $(e.target.previousElementSibling);
$container.toggle();
});
// Basic rangeslider initialization
$element.rangeslider({
// Deactivate the feature detection
polyfill: false,
// Callback function
onInit: function() {},
// Callback function
onSlide: function(position, value) {
console.log('onSlide');
console.log('position: ' + position, 'value: ' + value);
},
// Callback function
onSlideEnd: function(position, value) {
console.log('onSlideEnd');
console.log('position: ' + position, 'value: ' + value);

	newHtml = '';
	oldValue = $('#guest_count').val();
	oldMale = $('#male_count').val();


	/*for(i = 0 ; i < value; i++)
	{
		newHtml = newHtml + '<a class="gust-icn-male" href="#"> </a>';
	}*/

	if(oldValue == value)
	{

	}
	else if(oldValue < value)
	{
		//$("#guest-icon a").length


		appendGuest = value - oldValue;
		//alert("Old : " + oldValue + " current : " + value + " Callback : " + appendGuest);
		newHtml ='';
		for(i = 0; i < appendGuest; i++)
		{
			newHtml = newHtml + '<a class="gust-icn-male" > </a>';

		}
		$('#guest-icon').append(newHtml);
		$('#guest_count').val(value);

		/*newMale = oldMale + appendGuest;
		alert(newMale);*/

		newMale = parseInt(oldMale) + parseInt(appendGuest);
		//alert(newMale);


		$('#male_count').val(newMale);

	}
	else if(oldValue > value)
	{
		retmoveGuest = value - 1;
		$("#guest-icon > a:gt("+retmoveGuest+")").remove();

		/*$('.gust-icn-male').length;
		$('.gust-icn-female').length;*/

		$('#guest_count').val(value);
		$('#male_count').val($('.gust-icn-male').length);
		$('#female_count').val($('.gust-icn-female').length);
	}

}
});
});


$(document).on('click', "a.gust-icn-male", function() {
    /*var liId = $(this);
    alert(liId);*/
    if(this.hasClass('gust-icn-male'))
    {
    	this.removeClass('gust-icn-male');
    	this.addClass(' gust-icn-female');

    	var male_count   = $('#male_count').val();
		var female_count = $('#female_count').val();

		$('#male_count').val(parseInt(male_count) - 1);
		$('#female_count').val(parseInt(female_count) + 1);
    }
});

$(document).on('click', "a.gust-icn-female", function() {
    /*var liId = $(this);
    alert(liId);*/
    if(this.hasClass('gust-icn-female'))
    {
    	this.removeClass('gust-icn-female');
    	this.addClass(' gust-icn-male');

		var male_count   = $('#male_count').val();
		var female_count = $('#female_count').val();

		$('#male_count').val(parseInt(male_count) + 1);
		$('#female_count').val(parseInt(female_count) - 1);
    }
});


</script>
