<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Guest List View
 *
 * @since  0.0.1
 */
class BctedViewJetServiceBooking extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	protected $companyID;
	protected $serviceID;
	protected $user;
	protected $Itemid;
	protected $companyDetail;
	protected $serviceDetail;
	protected $showServiceList;
	protected $allServices;

	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		$input  = JFactory::getApplication()->input;
		$this->companyID  = $input->get('company_id', 0, 'int');
		$this->Itemid  = $input->get('Itemid', 0, 'int');
		$this->user = JFactory::getUser();

		if(!$this->user->id)
        {
            $app = JFactory::getApplication();
			$menu = $app->getMenu();
			$menuItem = $menu->getItems( 'link', 'index.php?option=com_users&view=login', true );

            JFactory::getApplication()->redirect(JRoute::_(JURI::root().'index.php?option=com_users&view=login&Itemid='.$menuItem->id.'&return='.base64_encode(JURI::getInstance())), 'Please login first!');
        }

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		$this->companyDetail = BctedHelper::getCompanyDetail($this->companyID);

		// Display the template
		parent::display($tpl);
	}

}
