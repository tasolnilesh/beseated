<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

/*echo "<pre>";
print_r($this->booking);
echo "</pre>";*/

$companyDetail = BctedHelper::getUserElementID($this->user->id);

/*echo "<pre>";
print_r($companyDetail);
echo "</pre>";*/

?>

<script type="text/javascript">
	function removePastBooking(bookingID,userID)
	{
		alert(bookingID);
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=companybookings.deletePastBooking',
			type: 'GET',
			data: '&booking_id='+bookingID+'&user_id='+userID,

			success: function(response){

	        	if(response == "200")
	        	{
	        		jQuery('#booking_'+bookingID).remove();
	        	}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}
</script>
<div class="table-wrp">
    <div class="row-fluid book-dtlwrp">
    	<div class="span10">

            <?php
                $fromTime = explode(":", $this->booking->booking_from_time);
                $toTime = explode(":", $this->booking->booking_to_time);
            ?>
        	<h4><?php echo "<b>".ucfirst($this->booking->name)."</b>"; ?></h4>
            <h4><?php echo "<b>Service Name : </b>" . $this->booking->service_name; ?></h4>
            <h4><?php echo "<b>Date : </b>" . date('d-m-Y',strtotime($this->booking->service_booking_datetime)); ?></h4>
            <h4><?php echo "<b>Time From </b>" . $fromTime[0].':'.$fromTime[1] ."<b> To </b>" . $toTime[0].':'.$toTime[1]; ?></h4>
            <h4><b>Number of Guests: </b><?php echo $this->booking->service_booking_number_of_guest .'('.$this->booking->male_count .'M/'.$this->booking->female_count.'F)'; ?></h4>
            <h4><b>Service Location : </b><?php echo $this->booking->service_location; ?></h4>
            <h4><b>Description : </b><?php echo $this->booking->service_booking_additional_info; ?></h4>
            <h4><b>Deposit Paid on:</b> <?php echo date('d-m-Y',strtotime($this->booking->deposit_paid_date)); ?></h4>
            <h4><b>Status : </b><?php echo $this->booking->status_text; ?></h4>
        </div>
        <?php if($companyDetail->licence_type!='basic'): ?>
            <?php $app = JFactory::getApplication(); ?>
            <?php $menu = $app->getMenu(); ?>
            <?php $menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=companymessages', true ); ?>
            <?php $Itemid = $menuItem->id; ?>
            <?php $connectionID = BctedHelper::getMessageConnection($this->user->id ,$this->booking->user_id); ?>
            <?php $link = JRoute::_("index.php?option=com_bcted&view=messagedetail&user_id=".$this->booking->user_id."&connection_id=".$connectionID."&Itemid=".$Itemid); ?>
            <div class="span2">
                <a href="<?php echo $link; ?>"><button type="button" class="msg-btn pull-right"></button></a>
            </div>
        <?php endif; ?>
    </div>
    <div class="booklst-tbl">
    	<table>
        	<tbody>
            	<tr>
                	<td width="80%">Total Price Paid</td>
                    <td><?php echo BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$this->booking->total_price); ?></td>
                </tr>
                <tr>
                    <td width="80%">BCT Commission</td>
                    <td><?php echo BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$this->booking->deposit_amount); ?></td>
                </tr>
                <tr>
                	<td width="80%">‘Total to receive</td>
                    <td><?php echo BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$this->booking->amount_payable); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
