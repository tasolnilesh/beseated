<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');


$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');
//$Itemid = 128;
$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$app = JFactory::getApplication();

$menu = $app->getMenu();


?>
<div class="wrapper">

   	<div class="span12 filter-result favourite-list">

			<?php foreach ($this->items as $key => $result): ?>
			<?php if($result->favourite_type == 'Venue' && $result->venue_id): ?>
				<div class="span6 venue_blck ">
					<div class="venue-img">
						<?php $menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=clubinformation', true ); ?>
						<?php $Itemid = $menuItem->id; ?>
						<?php if(file_exists($result->venue_image)): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=clubinformation&club_id='.$result->venue_id.'&Itemid='.$Itemid) ?>"><img src="<?php echo $result->venue_image; ?>" alt="" /></a>
						<?php else: ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=clubinformation&club_id='.$result->venue_id.'&Itemid='.$Itemid) ?>"><img src="images/bcted/default/banner.png" alt="" /></a>
						<?php endif; ?>
						<div class="rating-title">
							<div class="venue-title span7">
								<h4><?php echo $result->venue_name; ?></h4>
								<?php //if(!empty($result->city)): ?>
								<div class="venue-location"><?php echo $result->veneu_city; ?></div>
								<?php //endif; ?>
							</div>
                            <div class="rating-wrp span5">
                            	<?php $result->venue_rating = ceil($result->venue_rating); ?>
								<?php for($i = 1; $i <= $result->venue_rating;$i++): ?>
									<i class="full"> </i>
								<?php endfor; ?>
								<?php for($i = $result->venue_rating+1; $i <= 5; $i++): ?>
									<i class="empty"> </i>
								<?php endfor; ?>
							</div>
						</div>
					</div>
				</div>

			<?php elseif($result->favourite_type == 'Service' && $result->company_id): ?>
				<div class="span6 venue_blck ">
					<div class="venue-img">
						<?php $menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=clubinformation', true ); ?>
						<?php $Itemid = $menuItem->id; ?>
						<?php if(file_exists($result->company_image)): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=companyinformation&company_id='.$result->company_id.'&Itemid='.$Itemid) ?>"><img src="<?php echo $result->company_image; ?>" alt="" /></a>
						<?php else: ?>
							<a href="<?php echo JRoute::_('index.php?option=com_bcted&view=companyinformation&company_id='.$result->company_id.'&Itemid='.$Itemid) ?>"><img src="images/bcted/default/banner.png" alt="" /></a>
						<?php endif; ?>

						<div class="rating-title">
							<div class="venue-title span7">
								<h4><?php echo $result->company_name; ?></h4>
								<div class="venue-location"><?php echo $result->company_city; ?></div>
							</div>
							<div class="rating-wrp span5">
								<?php $result->company_rating = ceil($result->company_rating); ?>
								<?php for($i = 1; $i <= $result->company_rating;$i++): ?>
									<i class="full"> </i>
								<?php endfor; ?>
								<?php for($i = $result->company_rating+1; $i <= 5; $i++): ?>
									<i class="empty"> </i>
								<?php endfor; ?>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
			<?php endforeach; ?>

		<!--<div class="span4 venue_blck ">
			<div class="venue-img"><img src="images/venue-1.jpg" alt="" />
			<div class="rating-wrp"><i> </i><i> </i><i> </i><i> </i><i> </i></div>
			</div>
			<div class="venue-title">
			<h4>Bar blue</h4>
			<div class="venue-location">Dubai</div>
			</div>

		</div>
		<div class="span4 venue_blck">
			<div class="venue-img"><img src="images/venue-1.jpg" alt="" />
			<div class="rating-wrp"><i> </i><i> </i><i> </i><i> </i><i> </i></div>
			</div>
			<div class="venue-title">
			<h4>Bar blue</h4>
			<div class="venue-location">Dubai</div>
			</div>

		</div> -->
	</div>
</div>
<?php //echo $this->pagination->getListFooter(); ?>
