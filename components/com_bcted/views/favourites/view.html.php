<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Favourites View
 *
 * @since  0.0.1
 */
class BctedViewFavourites extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $user;
	/**
	 * Display the Favourites view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		// Get data from the model
		$this->items         = $this->get('Items');



		$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');

		$this->user = JFactory::getUser();

		if(!$this->user->id)
        {
	        	/*echo JUri::getInstance();
	        	exit;*/
              JFactory::getApplication()->redirect(JRoute::_(JURI::root().'index.php?option=com_users&view=login&return='.base64_encode(JURI::getInstance())), 'Please login first!');
        }

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Display the template
		parent::display($tpl);
	}


}
