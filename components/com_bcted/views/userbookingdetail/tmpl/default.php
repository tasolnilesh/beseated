<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$currentItemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$defaultPath =  JUri::base(); //http://192.168.5.31/development/thebeseated/
/*exit;*/

$document = JFactory::getDocument();

/*$document->addStyleSheet($defaultPath.'components/com_bcted/assets/tag-it/css/jquery.tagit.css');
$document->addStyleSheet($defaultPath.'components/com_bcted/assets/tag-it/css/tagit.ui-zendesk.css');
$document->addScript($defaultPath.'components/com_bcted/assets/tag-it/js/tag-it.js');*/

$document->addStyleSheet($defaultPath.'components/com_bcted/assets/tag-it//bootstrap/bootstrap-tagsinput.css');
$document->addScript($defaultPath.'components/com_bcted/assets/tag-it/bootstrap/bootstrap-tagsinput.js');



//$document->addScript('http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
//$document->addScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js');



?>
<script type="text/javascript">
	function removeVenuePastBooking(bookingID)
	{
		//alert(bookingID);
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=clubbookings.deletePastBooking',
			type: 'GET',
			data: '&user_type=user&booking_id='+bookingID,

			success: function(response){

	        	if(response == "200")
	        	{
	        		window.location = "index.php?option=com_bcted&view=userbookings&Itemid=<?php echo $currentItemid; ?>";
	        	}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}

	function removeCompanyPastBooking(bookingID)
	{
		//alert(bookingID);
		jQuery.ajax({
			url: 'index.php?option=com_bcted&task=companybookings.deletePastBooking',
			type: 'GET',
			data: '&user_type=user&booking_id='+bookingID,

			success: function(response){

	        	if(response == "200")
	        	{
	        		window.location = "index.php?option=com_bcted&view=userbookings&Itemid=<?php echo $currentItemid; ?>";
	        	}
	        }
		})
		.done(function() {
			//console.log("success");
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			//console.log("complete");
		});

	}
</script>
<?php //echo "<pre>"; print_r($this->booking); echo "</pre>"; ?>
<?php
	$app = JFactory::getApplication();
	$menu = $app->getMenu();
?>
<?php if($this->bookingType == 'service'): ?>
<div class="table-wrp">
    <div class="row-fluid book-dtlwrp">
    	<div class="span6">

		<?php
			$fromTime = explode(":", $this->booking->booking_from_time);
			$toTime = explode(":", $this->booking->booking_to_time);
		?>

			<?php if($this->booking->user_status == 2 || $this->booking->user_status == 5 || $this->booking->user_status == 4 || $this->booking->user_status == 7 || $this->booking->user_status == 10 || $this->booking->user_status == 11): ?>
				<h4><?php echo "<b>Company Name : </b>".$this->booking->company_name; ?></h4>
				<h4><?php echo "<b>Service Name : </b>".$this->booking->service_name; ?></h4>
				<?php /*<h4><?php echo "<b>Price : </b>". $this->booking->currency_sign.' '.$this->booking->service_price; ?></h4> */ ?>
				<h4><?php echo "<b>Price : </b>". BctedHelper::currencyFormat($this->booking->booked_currency_code,$this->booking->booked_currency_sign,$this->booking->total_price); ?></h4>
				<h4><?php echo "<b>Date : </b>".date('d-m-Y',strtotime($this->booking->service_booking_datetime)); ?></h4>
				<h4><?php echo "<b>From : </b>".$fromTime[0].':'.$fromTime[1]; ?></h4>
				<h4><?php echo "<b>To : </b>".$toTime[0].':'.$toTime[1]; ?></h4>
				<h4><?php echo "<b>Status : </b>".$this->booking->user_status_text; ?></h4>
			<?php elseif($this->booking->user_status == 3): ?>
				<h4><?php echo $this->booking->company_name; ?> has confirmed your booking for <?php echo $this->booking->service_name; ?> </h4>
	            <h4>The total price for this is <?php echo BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$this->booking->total_price); ?></h4>
	            <h4>To secure your booking, you will need to pay a deposit.</h4>
	            <h4>Remaining amount to be paid later.</h4>
			<?php endif; ?>
        </div>
       <div class="span6">
        	<!-- <button type="button" class="msg-btn pull-right"></button> -->
        	<img src="<?php echo ($this->booking->service_image)?JUri::base().$this->booking->service_image:'images/tabl-img.jpg'; ?>">
        </div>
    </div>
    <div class="booklst-tbl">
         <div class="span12">
         	<?php if($this->booking->user_status == 3 || $this->booking->user_status == 12): ?>
	            <form method="post" accept="index.php?option=com_bcted&view=userbookings">
	            	<?php /*$depositePer = ($this->booking->commission_rate)?$this->booking->commission_rate:$this->bctConfig->deposit_per;*/ ?>
	            	<?php /*$diposit = $this->booking->deposit_amount;*/ ?>

	            	<a href="index.php?option=com_bcted&view=dummypayment&task=packageorder.packagePurchased&booking_id=<?php echo $this->booking->service_booking_id; ?>&booking_type=service&auto_approve=1">
	                	<button type="button" class="btn btn-primary">Pay <?php echo BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$this->booking->deposit_amount); ?></button>
	                </a>

	                <?php /*$usdRate = BctedHelper::convertCurrency($this->booking->currency_code,'USD');*/ ?>
	                <?php $usdRate = BctedHelper::convertCurrencyGoogle(1,$this->booking->currency_code,'USD'); ?>
	                <?php /*echo "USD Rate : ".$usdRate; exit;*/ ?>
	                <?php /*$currencyRate = BctedHelper::convertCurrency('USD',$this->booking->currency_code);*/ ?>
	                <?php $currencyRate = BctedHelper::convertCurrencyGoogle(1,'USD',$this->booking->currency_code); ?>
	                <?php //echo "USD Rate : ".$usdRate; exit; ?>
	                <?php if($this->loyaltyPoints['totalPoint']>=10): ?>
		                <?php $totalPoint = floor($this->loyaltyPoints['totalPoint']); ?>
		                <?php $roundedPoint = ($totalPoint-($totalPoint%10)); ?>
		                <?php $bcDollers = $roundedPoint/10; ?>
		                <?php $bcDollersInCurrencyCode = $bcDollers * $currencyRate; ?>
		                <?php $amoutWithPoint = ($this->booking->deposit_amount - $bcDollersInCurrencyCode); ?>

		                <?php /*echo "<br />Total Point :".$totalPoint; ?>
		                <?php echo "<br />Rounded Point :".$roundedPoint; ?>
		                <?php echo "<br />Amoount After Round Point :".$amoutWithPoint;*/ ?>

		               		<h4>Or, save money and use your points</h4>


						<a href="index.php?option=com_bcted&view=dummypayment&task=packageorder.packagePurchased&bc_dollars=<?php echo $roundedPoint; ?>&booking_id=<?php echo $this->booking->service_booking_id; ?>&booking_type=service&auto_approve=1">
							<button type="button" class="btn btn-primary">Pay <?php echo BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$amoutWithPoint) . ' & ' . $roundedPoint.' Points'; ?></button>
						</a>

	            	<?php endif; ?>


	            </form>
	        <?php elseif($this->booking->user_status == 2 || $this->booking->user_status == 5 || $this->booking->user_status == 4  || $this->booking->user_status == 7 || $this->booking->user_status == 10 || $this->booking->user_status == 11): ?>
	        	<?php
	        		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=companyinformation', true );
					$Itemid = $menuItem->id;
				?>
	        	<a href="index.php?option=com_bcted&view=companyinformation&company_id=<?php echo $this->booking->company_id; ?>&Itemid=<?php echo $Itemid; ?>">
	               <button type="button" class="btn btn-primary"><?php echo JText::_('COM_BCTED_VIEW_PROFILE_BUTTON_TEXT'); ?></button>
	           	</a>

	           	<?php if($this->booking->user_status == 11): ?>
		               <button type="button" onclick="removeCompanyPastBooking(<?php echo $this->booking->service_booking_id; ?>)" class="btn btn-primary"><?php echo JText::_('COM_BCTED_VIEW_DELETE_BUTTON_TEXT'); ?></button>
	           	<?php endif; ?>

	           	<?php $timestampOfBooking = strtotime($this->booking->service_booking_datetime.' '.$this->booking->booking_from_time); ?>
	           	<?php $currentTimeStamp = time(); ?>


	           	<?php if($this->booking->user_status == 5 && $currentTimeStamp>$timestampOfBooking): ?>
		               <button type="button" onclick="removeCompanyPastBooking(<?php echo $this->booking->service_booking_id; ?>)" class="btn btn-primary"><?php echo JText::_('COM_BCTED_VIEW_DELETE_BUTTON_TEXT'); ?></button>
	           	<?php endif; ?>



	           	<?php


	           	$bookingTS = $this->booking->service_booking_datetime.' '.$this->booking->booking_from_time;
	           //	$currntTS = time();

	           	$before24 = strtotime("-24 hours", strtotime($bookingTS));
				$currentTime = time();
				if($currentTime<=$before24 && $this->booking->status == 5)
				{
					?>
				<a href="index.php?option=com_bcted&task=userbookings.cancelBooking&bookingType=service&booking_id=<?php echo $this->booking->service_booking_id; ?>&Itemid=<?php echo $currentItemid; ?>&auto_approve=1">
	               <button type="button" class="btn btn-primary"><?php echo JText::_('COM_BCTED_CANCEL_BUTTON_TEXT'); ?></button>
	           	</a>
					<?php
				} ?>


        	<?php endif; ?>
        </div>
    </div>
</div>
<?php elseif($this->bookingType == 'table'): ?>
<?php //echo $this->bookingType."<pre>"; print_r($this->booking); echo "</pre>"; ?>
<?php //echo "<pre>"; print_r($this->bctConfig); echo "</pre>"; exit; ?>

	<?php
		$fromTime = explode(":", $this->booking->booking_from_time);
		$toTime = explode(":", $this->booking->booking_to_time);
	?>
<div class="table-wrp">
    <div class="row-fluid book-dtlwrp">
    	<div class="span6">

	    	<?php if($this->booking->user_status == 2 || $this->booking->user_status == 5  || $this->booking->user_status == 4  || $this->booking->user_status == 7  || $this->booking->user_status == 10 || $this->booking->user_status == 11): ?>
				<h4><?php echo "<b>Club Name : </b>".$this->booking->venue_name; ?></h4>
				<h4><b>Table Name : </b><?php echo ($this->booking->premium_table_id)?$this->booking->venue_table_name:$this->booking->custom_table_name; ?></h4>
				<h4><?php echo "<b>Price : </b>".BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$this->booking->venue_table_price); ?></h4>
				<h4><?php echo "<b>Date : </b>".date('d-m-Y',strtotime($this->booking->venue_booking_datetime)); ?></h4>
				<h4><?php echo "<b>From : </b>".$fromTime[0].':'.$fromTime[1]; ?></h4>
				<h4><?php echo "<b>To : </b>".$toTime[0].':'.$toTime[1]; ?></h4>
				<h4><?php echo "<b>Status : </b>".$this->booking->user_status_text; ?></h4>

			<?php elseif($this->booking->user_status == 3): ?>
				<h4><?php echo ucfirst($this->booking->venue_name); ?> has confirmed your booking for <?php echo ($this->booking->premium_table_id)?ucfirst($this->booking->venue_table_name):ucfirst($this->booking->custom_table_name); ?> </h4>
	            <h4>The total price for this is <?php echo BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$this->booking->venue_table_price); ?></h4>
	            <h4>To secure your booking, you will need to pay a deposit.</h4>
	            <h4>Remaining amount to be paid at table.</h4>
			<?php endif; ?>
        </div>
        <div class="bk-dtl-list">
            <div class="span6 bk-status">
                Status: <?php echo $this->booking->user_status_text; ?>
            </div>
            <div class="span6 bk-dtl-img">
                <!-- <button type="button" class="msg-btn pull-right"></button> -->
                <img src="<?php echo ($this->booking->venue_table_image)?JUri::base().$this->booking->venue_table_image:'images/tabl-img.jpg'; ?>">
            </div>
        </div>
    </div>
    <div class="booklst-tbl">
         <div class="span12">
         	<?php if($this->booking->user_status == 3): ?>
            <form method="post" accept="index.php?option=com_bcted&view=userbookings">
           		<?php $depositePer = ($this->booking->commission_rate)?$this->booking->commission_rate:$this->bctConfig->deposit_per; ?>
            	<?php $diposit = (($this->booking->venue_table_price * $depositePer)/100); ?>
            	<?php
		        		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&task=packageorder.packagePurchased', true );
						$Itemid = $menuItem->id;
					?>

				<?php if($this->bctConfig->auto_approve ==1): ?>
					<a href="index.php?option=com_bcted&view=dummypayment&task=packageorder.packagePurchased&booking_id=<?php echo $this->booking->venue_booking_id; ?>&booking_type=venue&auto_approve=1">
	                	<button type="button" class="btn btn-primary">Pay <?php echo BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$diposit); ?></button>
	                </a>
				<?php else: ?>
					<a href="index.php?option=com_bcted&view=dummypayment&task=packageorder.packagePurchased&booking_id=<?php echo $this->booking->venue_booking_id; ?>&booking_type=venue">
                	<button type="button" class="btn btn-primary">Pay <?php echo BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$diposit); ?></button>
                </a>
				<?php endif; ?>

                <?php //$usdRate = BctedHelper::convertCurrency($this->booking->currency_code,'USD'); ?>
                <?php //$currencyRate = BctedHelper::convertCurrency('USD',$this->booking->currency_code); ?>
                <?php $usdRate = BctedHelper::convertCurrencyGoogle(1,$this->booking->currency_code,'USD'); ?>
                <?php $currencyRate = BctedHelper::convertCurrencyGoogle(1,'USD',$this->booking->currency_code); ?>
                <?php if($this->loyaltyPoints['totalPoint']>=10): ?>
	                <?php $totalPoint = floor($this->loyaltyPoints['totalPoint']); ?>
	                <?php $roundedPoint = ($totalPoint-($totalPoint%10)); ?>
	                <?php $bcDollers = $roundedPoint/10; ?>
	                <?php $bcDollersInCurrencyCode = $bcDollers * $currencyRate; ?>
	                <?php

	                if($bcDollersInCurrencyCode > $diposit)
	                {
	                	$bcdUsed = $diposit * 10;
	                	$amoutWithPoint = 0;
	                }
	                else
	                {
	                	$bcdUsed = $roundedPoint;
	                	$amoutWithPoint = ($diposit - $bcDollersInCurrencyCode);
	                }

	                	/*echo "Point in Dollars :". $bcDollersInCurrencyCode;
	                	exit;*/

	                ?>


	                <?php /*echo "<br />Total Point :".$totalPoint; ?>
	                <?php echo "<br />Rounded Point :".$roundedPoint; ?>
	                <?php echo "<br />Amoount After Round Point :".$amoutWithPoint; exit;*/ ?>


	               		<h4>Or, save money and user your points</h4>


	               	<?php
		        		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&task=packageorder.packagePurchased', true );
						$Itemid = $menuItem->id;


					?>

					<?php if($this->bctConfig->auto_approve ==1): ?>
						<a href="index.php?option=com_bcted&view=dummypayment&task=packageorder.packagePurchased&bc_dollars=<?php echo $bcdUsed; ?>&booking_id=<?php echo $this->booking->venue_booking_id; ?>&booking_type=venue&auto_approve=1">
		                	<button type="button" class="btn btn-primary">Pay <?php echo BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$amoutWithPoint) . ' & ' . $bcdUsed.' Points'; ?></button>
		                </a>
					<?php else: ?>
						<a href="index.php?option=com_bcted&view=dummypayment&task=packageorder.packagePurchased&bc_dollars=<?php echo $bcdUsed; ?>&booking_id=<?php echo $this->booking->venue_booking_id; ?>&booking_type=venue">
		                	<button type="button" class="btn btn-primary">Pay <?php echo BctedHelper::currencyFormat($this->booking->currency_code,$this->booking->currency_sign,$amoutWithPoint) . ' & ' . $bcdUsed.' Points'; ?></button>
		                </a>
					<?php endif;

						if($this->booking->user_status == 3)
						{
						?>
							<a href="index.php?option=com_bcted&task=userbookings.cancelBooking&bookingType=venue&booking_id=<?php echo $this->booking->venue_booking_id; ?>&Itemid=<?php echo $currentItemid; ?>">
				               <button type="button" class="btn btn-primary"><?php echo JText::_('COM_BCTED_CANCEL_BUTTON_TEXT'); ?></button>
				           	</a>

					<?php
				        }
            	    endif;
            	        ?>

            </form>
        	<?php elseif($this->booking->user_status == 2 || $this->booking->user_status == 5  || $this->booking->user_status == 4  || $this->booking->user_status == 7  || $this->booking->user_status == 10 || $this->booking->user_status == 11): ?>
				<?php
	        		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=clubinformation', true );
					$Itemid = $menuItem->id;

				?>
				<a href="index.php?option=com_bcted&view=clubinformation&club_id=<?php echo $this->booking->venue_id; ?>&Itemid=<?php echo $Itemid; ?>">
	               <button type="button" class="btn btn-primary"><?php echo JText::_('COM_BCTED_VIEW_PROFILE_BUTTON_TEXT'); ?></button>
	           	</a>

	           <?php if($this->booking->user_status == 11): ?>
		               <button type="button" onclick="removeVenuePastBooking(<?php echo $this->booking->venue_booking_id; ?>)" class="btn btn-primary"><?php echo JText::_('COM_BCTED_VIEW_DELETE_BUTTON_TEXT'); ?></button>
	           	<?php endif; ?>

	           	<?php


	           	$bookingTS = $this->booking->venue_booking_datetime.' '.$this->booking->booking_from_time;
	           //	$currntTS = time();

	           	$before24 = strtotime("-24 hours", strtotime($bookingTS));
				$currentTime = time();
				if($currentTime<=$before24 && $this->booking->status == 5)
				{
					?>
				<a href="index.php?option=com_bcted&task=userbookings.cancelBooking&bookingType=venue&booking_id=<?php echo $this->booking->venue_booking_id; ?>&Itemid=<?php echo $currentItemid; ?>">
	               <button type="button" class="btn btn-primary"><?php echo JText::_('COM_BCTED_CANCEL_BUTTON_TEXT'); ?></button>
	           	</a>
					<?php
				}


	           	?>


        	<?php endif; ?>

        </div>
    </div>
</div>
<?php elseif($this->bookingType == 'package'): ?>
<?php //echo $this->bookingType."<pre>"; print_r($this->booking); echo "</pre>"; ?>

<?php $packageTime = explode(":", $this->booking->package_time); ?>
<div class="table-wrp">
    <div class="row-fluid book-dtlwrp">
    	<div class="span6">
	    	<?php if($this->booking->user_status == 2 || $this->booking->user_status == 11 || $this->booking->user_status == 5  || $this->booking->user_status == 4 || $this->booking->user_status == 6 || $this->booking->user_status == 10): ?>
	    		<?php if($this->booking->venue_id): ?>
	    			<h4><?php echo "<b>Club Name : </b>".$this->booking->venue_name; ?></h4>
	    		<?php elseif($this->booking->company_id): ?>
	    			<h4><?php echo "<b>Company Name : </b>".$this->booking->company_name; ?></h4>
	    		<?php endif; ?>

				<h4><?php echo "<b>Package Name : </b>".$this->booking->package_name; ?></h4>
				<?php if($this->booking->package_currency_code): ?>
					<h4><?php echo "<b>Price : </b>". BctedHelper::currencyFormat($this->booking->package_currency_code,$this->booking->package_currency_sign,$this->booking->package_price * $this->booking->package_number_of_guest); ?>(PP)</h4>
				<?php else:?>
					<h4><?php echo "<b>Price : </b>". BctedHelper::currencyFormat($booking->pakcge_current_currency_code,$this->booking->pakcge_current_currency_sign,$this->booking->package_price * $this->booking->package_number_of_guest); ?>(PP)</h4>
				<?php endif; ?>

				<h4><?php echo "<b>No. Of Guests : </b> " . $this->booking->package_number_of_guest; ?>
				<h4><?php echo "<b>Date : </b>".date('d-m-Y',strtotime($this->booking->package_datetime)); ?></h4>
				<h4><?php echo "<b>Time : </b>".$packageTime[0].':'.$packageTime[1]; ?></h4>
				<h4><?php echo "<b>Status : </b>".$this->booking->user_status_text; ?></h4>

			<?php elseif($this->booking->user_status == 3): ?>
				<?php if($this->booking->venue_id): ?>
					<h4><?php echo ucfirst($this->booking->venue_name); ?> has confirmed your booking for <?php echo ucfirst($this->booking->package_name); ?> </h4>
				<?php elseif($this->booking->company_id): ?>
					<h4><?php echo ucfirst($this->booking->company_name); ?> has confirmed your booking for <?php echo ucfirst($this->booking->package_name); ?> </h4>
				<?php endif; ?>

				<?php /* if($this->booking->pakcge_current_currency_sign): ?>
				<?php else: ?>
				<?php endif; */ ?>


	            <h4>The total price for this is <?php echo BctedHelper::currencyFormat($this->booking->pakcge_current_currency_code,$this->booking->pakcge_current_currency_sign,($this->booking->package_price * $this->booking->package_number_of_guest)); ?></h4>
	            <!--<h4>To secure your booking, you will need to pay a deposit.</h4>
	            <h4>Remaining amount to be paid at table.</h4>-->

	            <?php $diposit = $this->booking->total_price; ?>
            	<a href="index.php?option=com_bcted&view=dummypayment&task=packageorder.packagePurchased&booking_id=<?php echo $this->booking->package_purchase_id; ?>&booking_type=package&auto_approve=1">
                	<button type="button" class="btn btn-primary">Pay <?php echo BctedHelper::currencyFormat($this->booking->pakcge_current_currency_code,$this->booking->pakcge_current_currency_sign,$diposit); ?></button>
                </a>
			<?php endif; ?>
        </div>
        <div class="span6">
        	<!-- <button type="button" class="msg-btn pull-right"></button> -->
        	<img src="<?php echo ($this->booking->package_image)?JUri::base().$this->booking->package_image:'images/bcted/default/banner.png'; ?>">
        </div>
    </div>
    <div class="booklst-tbl">
         <div class="span12 pck-bking">
         	<?php if($this->booking->user_status == 3): ?>
				<h4>Or, Invite your friends to split amount</h4>
				<form method="post" action="index.php?option=com_bcted&task=userbookings.inviteuserinpackage">
					<div class="span12 package-invite">
						<input type="text" name="invite_user" placeholder="Enter emails" id="invite_user" value="" data-role="tagsinput" />
					</div>
					<input type="hidden" name="package_purchase_id" id="package_purchase_id" value="<?php echo $this->booking->package_purchase_id; ?>">
					<input type="hidden" name="Itemid" value="<?php echo $currentItemid; ?>" id="Itemid">
					<button type="submit" class="btn btn-primary"><?php echo JText::_('COM_BCTED_INVITE_USER'); ?></button>
				</form>

				<?php $invitationsSent = BctedHelper::getPackageInvitedUserDetail($this->booking->package_purchase_id); ?>
        		<?php //echo "<pre>"; print_r($invitationsSent); echo "</pre>"; exit; ?>
        		<?php if($invitationsSent): ?>
					<table class="table table-striped">
						<tr>
							<th>Email</th>
							<th>Status</th>
						</tr>
						<?php foreach ($invitationsSent as $key => $invitedDetail) : ?>
							<tr>
								<td><?php echo $invitedDetail->invited_email; ?></td>
								<td>
									<?php if($invitedDetail->status == 2): ?>
										<?php echo "Not Paid"; ?>
									<?php elseif($invitedDetail->status == 5): ?>
										<?php echo "Paid"; ?>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</table>
					<?php if($this->booking->user_status == 3): ?>
						<a href="index.php?option=com_bcted&view=dummypayment&task=packageorder.packagePurchased&booking_id=<?php echo $this->booking->package_purchase_id; ?>&booking_type=package&full_payment=1&auto_approve=1">
							<button type="button" class="btn btn-primary">Pay Full Amount</button>
						</a>
					<?php endif; ?>
        		<?php endif; ?>

        	<?php elseif($this->booking->user_status == 2 || $this->booking->user_status == 5  || $this->booking->user_status == 4 || $this->booking->user_status == 6): ?>

        		<?php $invitationsSent = BctedHelper::getPackageInvitedUserDetail($this->booking->package_purchase_id); ?>
        		<?php //echo "<pre>"; print_r($invitationsSent); echo "</pre>"; exit; ?>
        		<?php if($invitationsSent): ?>
					<table class="table table-striped">
						<tr>
							<th>Email</th>
							<th>Status</th>
						</tr>
						<?php foreach ($invitationsSent as $key => $invitedDetail) : ?>
							<tr>
								<td><?php echo $invitedDetail->invited_email; ?></td>
								<td>
									<?php if($invitedDetail->status == 2): ?>
										<?php echo "Not Paid"; ?>
									<?php elseif($invitedDetail->status == 5): ?>
										<?php echo "Paid"; ?>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</table>
        		<?php endif; ?>
				<div>
					<?php if($this->booking->user_status == 6): ?>
					<a href="index.php?option=com_bcted&view=dummypayment&task=packageorder.packagePurchased&booking_id=<?php echo $this->booking->package_purchase_id; ?>&booking_type=package&full_payment=1">
						<button type="button" class="btn btn-primary">Pay Full Amount</button>
					</a>
					<?php endif; ?>


					<?php if($this->booking->user_status == 6 || $this->booking->user_status == 5): ?>
						<?php $bookingDT = $this->booking->package_datetime . ' ' . $this->booking->package_time; ?>
						<?php $bookingTS = strtotime($bookingDT); ?>
						<?php $before24 = strtotime("-24 hours", strtotime($bookingDT)); ?>
						<?php $currentTime = time(); ?>
						<?php if($currentTime<=$before24): ?>
							<!-- <a href="index.php?option=com_bcted&task=userbookings.send_request_refund&booking_id=<?php //echo $this->booking->package_purchase_id; ?>&Itemid=<?php //echo $currentItemid; ?>">
								<button type="button" class="btn btn-primary">Request Refund</button>
							</a> -->
							<a href="index.php?option=com_bcted&task=userbookings.send_request_refund&booking_id=<?php echo $this->booking->package_purchase_id; ?>&Itemid=<?php echo $currentItemid; ?>">
								<button type="button" class="btn btn-primary">Request Refund</button>
							</a>
						<?php endif; ?>
					<?php endif; ?>

					<?php
		        		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=clubinformation', true );
						$Itemid = $menuItem->id;
					?>
		        	<a href="index.php?option=com_bcted&view=clubinformation&club_id=<?php echo $this->booking->venue_id; ?>&Itemid=<?php echo $Itemid; ?>">
		               <button type="button" class="btn btn-primary"><?php echo JText::_('COM_BCTED_VIEW_PROFILE_BUTTON_TEXT'); ?></button>
		           	</a>
		           	<?php if($this->booking->user_status == 2): ?>
			           	<?php
			        		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=userbookingdetail', true );
							$Itemid = $menuItem->id;
						?>
		           		<a href="index.php?option=com_bcted&task=userbookings.cancelPacakgeBooking&package_booking_id=<?php echo $this->booking->package_purchase_id; ?>&Itemid=<?php echo $currentItemid; ?>">
			               <button type="button" class="btn btn-primary"><?php echo JText::_('COM_BCTED_CANCEL_BUTTON_TEXT'); ?></button>
			           	</a>
		           	<?php endif; ?>
				</div>


        	<?php endif; ?>

        </div>
    </div>
</div>
<?php endif; ?>

<script type="text/javascript">
	jQuery('#invite_user').on('beforeItemAdd', function(event) {
	    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
	    if(pattern.test(event.item))
	    {
	    }
	    else
	    {
	    	event.cancel=true;
	    }
	});
</script>

<!--
<VENUE/SERVICE> has confirmed your booking for <product>

The total price for this is <amount>

To secure your booking, you will need to pay a deposit.

Remaining amount to be paid at table.

Pay $10,000

-->