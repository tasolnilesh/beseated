<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

$ItemidRequest = BctedHelper::getBctedMenuItem('company-request');
$ItemidBookings = BctedHelper::getBctedMenuItem('company-bookings');
//$requestLink = $Itemid_request->link.'&Itemid='.$Itemid_request->id;


?>

<?php $app = JFactory::getApplication(); ?>
<?php $menu = $app->getMenu(); ?>

<div class="bct-summary-container">
	<div class="row">
        <div class="summary-statustrics span12">
            <div class="cls-bookings span4">
                <?php $menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=companybookings', true ); ?>
                <a href="index.php?option=com_bcted&view=companybookings&Itemid=<?php echo $menuItem->id; ?>">
                <div class="cir-wrp">
                    <div class="smry-counter"><?php echo $this->totalBooking; ?></div>
                </div>
                <div class="cir-bx-title">
                    <?php echo JText::_('COM_BCTED_VIEW_COMPANY_SUMMARY_BOOKING_CIRCLE_LABLE'); ?>
                </div>
                </a>
            </div>
            <div class="cls-request span4">
                <?php $menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=companyrequests', true ); ?>
                <a href="index.php?option=com_bcted&view=companyrequests&Itemid=<?php echo $menuItem->id; ?>">
                <div class="cir-wrp">
                    <div class="smry-counter"><?php echo $this->totalRequests; ?></div>
                </div>
                <div class="cir-bx-title">
                    <?php echo JText::_('COM_BCTED_VIEW_COMPANY_SUMMARY_REQUESTS_CIRCLE_LABLE'); ?>
                </div>
                </a>
            </div>
            <div class="cls-revenu span4">
                <?php $menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=companyaccounthistory', true ); ?>
                <a href="index.php?option=com_bcted&view=companyaccounthistory&Itemid=<?php echo $menuItem->id; ?>">
                <div class="cir-wrp">
                    <div class="smry-counter" style="font-size:23px;"><?php echo ($this->totalRevenue)?$this->elementDetail->currency_sign.' '.number_format($this->totalRevenue,0):'0'; ?></div>
                </div>
                <div class="cir-bx-title">
                    <?php echo JText::_('COM_BCTED_VIEW_COMPANY_SUMMARY_REVENUE_CIRCLE_LABLE'); ?>
                </div>
                </a>
            </div>
        </div>
    </div>
	<div class="summary-list">
		<ul>
		<?php if($this->bookings): ?>
            <?php foreach ($this->bookings as $key => $booking): ?>

                <?php
                    $fromTime = explode(":", $booking->booking_from_time);
                    $toTime = explode(":", $booking->booking_to_time);
                ?>

                <?php if($booking->status == 5): ?>
                    <li class="green-booking">
                <?php else: ?>
                    <li>
                <?php endif; ?>
                    <?php $statusText=trim($booking->status_text); ?>
                    <?php if($statusText == 'Request'): ?>
                        <?php $link = JRoute::_('index.php?option=com_bcted&view=companyrequestdetail&request_id='.$booking->service_booking_id.'&Itemid='.$ItemidRequest->id); ?>
                    <?php elseif($statusText == 'Awaiting Payment'): ?>
                        <?php $link = JRoute::_('index.php?option=com_bcted&view=companyrequestdetail&request_id='.$booking->service_booking_id.'&Itemid='.$ItemidRequest->id); ?>
                    <?php elseif($statusText == 'Waiting List'): ?>
                        <?php $link = JRoute::_('index.php?option=com_bcted&view=companyrequestdetail&request_id='.$booking->service_booking_id.'&Itemid='.$ItemidRequest->id); ?>
                     <?php elseif($statusText == 'Booked'): ?>
                        <?php $link = JRoute::_('index.php?option=com_bcted&view=companybookingdetail&booking_id='.$booking->service_booking_id.'&Itemid='.$ItemidBookings->id); ?>
                    <?php else: ?>
                         <?php $link = "#"; ?>
                    <?php endif; ?>

                    <a href="<?php echo $link; ?>">

                    <div class="prd-single-left span8">
                        <div class="prd-title"><?php echo $booking->service_name; ?></div>
                        <div class="prd-person-nm"><?php echo $booking->name; ?></div>
                        <div class="prd-date-time"><b><?php echo JText::_('COM_BCTED_DATE'); ?> : </b><?php echo date('d-m-Y',strtotime($booking->service_booking_datetime)); ?></div>
                        <div class="prd-date-time"><b><?php echo JText::_('COM_BCTED_FROM_TIME'); ?> : </b><?php echo $fromTime[0].':'.$fromTime[1] .' '. JText::_('COM_BCTED_TO_TIME') . $toTime[0].':'.$toTime[1];; ?></div>
                    </div>
                    <div class="prd-single-right span4">


                        <?php if($statusText == 'Request'): ?>
                            <button class="reqlnk-btn" type="button"><?php echo trim($booking->status_text); ?></button>
                        <?php elseif($statusText == 'Awaiting Payment'): ?>
                            <button class="reqlnk-btn" type="button"><?php echo trim($booking->status_text); ?></button>
                        <?php elseif($statusText == 'Waiting List'): ?>
                            <button class="reqlnk-btn" type="button"><?php echo trim($booking->status_text); ?></button>
                         <?php elseif($statusText == 'Booked'): ?>
                            <button class="reqlnk-btn" type="button"><?php echo trim($booking->status_text); ?></button>
                        <?php else: ?>
                            <button class="reqlnk-btn" type="button"><?php echo trim($booking->status_text); ?></button>
                        <?php endif; ?>
                    </div>
                    </a>
                </li>
            <?php endforeach; ?>
        <?php else: ?>
                <div id="system-message">
                    <div class="alert alert-block">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <h4><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_GENERAL_TITLE'); ?></h4>
                        <div><p><?php echo JText::_('COM_BCTED_USERBOOKINGS_NO_BOOKING_FOUND_FOR_GENERAL_DESC'); ?></p></div>
                    </div>
                </div>
        <?php endif; ?>
		</ul>
	</div>
</div>

<?php echo $this->pagination->getListFooter(); ?>
