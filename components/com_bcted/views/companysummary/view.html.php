<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Company Summary View
 *
 * @since  0.0.1
 */
class BctedViewCompanySummary extends JViewLegacy
{
	protected $bookings;

	protected $pagination;

	protected $state;

	protected $user;

	protected $totalRequests;

    protected $totalBooking;

    protected $totalRevenue;

    protected $elementDetail;
	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{
		$this->bookings         = $this->get('Items');

		$this->pagination    = $this->get('Pagination');

		//$this->pagination    = $this->get('Pagination');
		//$this->state         = $this->get('State');

		$this->user = JFactory::getUser();

		if(!$this->user->id)
		{
			JFactory::getApplication()->redirect(JRoute::_(JURI::root().'index.php?option=com_users&view=login'), 'Please login first!');
		}

		$model = $this->getModel();
		$user = JFactory::getUser();
		$this->elementDetail = BctedHelper::getUserElementID($user->id);
		$summaries = $model->summaryForCompany($this->elementDetail->company_id);
		$this->totalRequests = 0;
		$this->totalBooking = 0;

		foreach ($summaries as $key => $summary)
		{
			$statusText = BctedHelper::getStatusNameFromStatusID($summary->status);

			if($statusText == 'Request')
			{
				$this->totalRequests = $summary->total_count;
			}
			else if($statusText == 'Booked')
			{
				$this->totalBooking = $summary->total_count;
			}

			$statusText = "";
		}

		$this->totalRevenue = $model->getRevenueForCompany($this->elementDetail->company_id);

		// Display the template
		parent::display($tpl);
	}



}
