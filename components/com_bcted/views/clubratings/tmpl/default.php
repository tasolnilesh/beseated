<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

?>
<div class="guest-club-wrp">
	<H1>Venue Ratings</H1>
	<div class="review-mainwrp">
		<!-- <div class="large-rating-wrp">

		988646604488021,10202193627002000,1410596819255480,354834858043053,654174034688785,1626418810912840,1112610372089200,1627156410837720,10153173400029700,967077526644929,354834858043053,10153173400029700,1627156410837720,967077526644929,354834858043053,1410596819255480,654174034688785,967077526644929,10152541803552600,10152541803552600,
			<i class="full"> </i>
			<i class="full"> </i>
			<i class="full"> </i>
			<i class="empty"> </i>
			<i class="empty"> </i>
		</div> -->
		<div class="large-rating-wrp">
			<?php /*for($i = 1; $i <= floor($this->club->venue_rating);$i++): ?>
				<i class="full"> </i>
			<?php endfor; ?>
			<?php for($i = floor($this->club->venue_rating)+1; $i <= 5; $i++): ?>
				<i class="empty"> </i>
			<?php endfor;*/ ?>
			<?php $overallRating = $this->club->venue_rating; ?>
        	<?php $starValue = floor($this->club->venue_rating); ?>
        	<?php $maxRating = 5; ?>
        	<?php $printedStart = 0 ;?>
			<?php for($i = 1; $i <= $starValue;$i++): ?>
				<i class="full"> </i>
				<?php $printedStart = $printedStart + 1; ?>
			<?php endfor; ?>
			<?php if($starValue<$overallRating): ?>
				<i class="half"> </i>
				<?php $printedStart = $printedStart + 1; ?>
			<?php endif; ?>
			<?php if($printedStart < $maxRating): ?>
				<?php for($i = $maxRating-$printedStart; $i > 0;$i--): ?>
					<i class="empty"> </i>
				<?php endfor; ?>
			<?php endif; ?>
		</div>
		<?php foreach ($this->items as $key => $item): ?>
			<div class="media">
				<div class="black-rating-wrp">
					<?php for($i = 1; $i <= $item->rating;$i++): ?>
						<i class="full"> </i>
					<?php endfor; ?>
					<?php for($i = $item->rating+1; $i <= 5; $i++): ?>
						<i class="empty"> </i>
					<?php endfor; ?>
				</div>
				<div class="media-body">
					<?php echo trim($item->rating_comment); ?>
				</div>
				<div class="rw-user-info"><span><?php echo $item->name; ?></span> /<span> <?php echo date('d-m-Y H:i:m',strtotime($item->rating_datetime)); ?></span></div>
			</div>
		<?php endforeach; ?>
		<!--<div class="media">
			<div class="black-rating-wrp">
				<i class="full"> </i>
				<i class="full"> </i>
				<i class="full"> </i>
				<i class="empty"> </i>
				<i class="empty"> </i>
			</div>
			<div class="media-body">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vel odio finibus sem malesuada ornare at at massa. Aenean feugiat nec lorem ut fermentum.
			</div>
			<div class="rw-user-info"><span>Username</span> /<span> Date</span></div>
		</div>-->
		<!-- <div class="media">
			<div class="black-rating-wrp">
				<i class="full"> </i>
				<i class="full"> </i>
				<i class="full"> </i>
				<i class="empty"> </i>
				<i class="empty"> </i>
			</div>
			<div class="media-body">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vel odio finibus sem malesuada ornare at at massa. Aenean feugiat nec lorem ut fermentum.
			</div>
			<div class="rw-user-info"><span>Username</span> /<span> Date</span></div>
		</div>
		<div class="media">
			<div class="black-rating-wrp">
				<i class="full"> </i>
				<i class="full"> </i>
				<i class="full"> </i>
				<i class="empty"> </i>
				<i class="empty"> </i>
			</div>
			<div class="media-body">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vel odio finibus sem malesuada ornare at at massa. Aenean feugiat nec lorem ut fermentum.
			</div>
			<div class="rw-user-info"><span>Username</span> /<span> Date</span></div>
		</div>
		<div class="media">
			<div class="black-rating-wrp">
				<i class="full"> </i>
				<i class="full"> </i>
				<i class="full"> </i>
				<i class="empty"> </i>
				<i class="empty"> </i>
			</div>
			<div class="media-body">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vel odio finibus sem malesuada ornare at at massa. Aenean feugiat nec lorem ut fermentum.
			</div>
			<div class="rw-user-info"><span>Username</span> /<span> Date</span></div>
		</div>-->
	</div>

</div>
<?php //echo $this->pagination->getListFooter(); ?>
