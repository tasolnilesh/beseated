<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

?>
<script type="text/javascript">
function deleteRequest(requestID)
{

	jQuery.ajax({
		type: "GET",
		url: "index.php?option=com_bcted&task=ajax.calculateExp",
		data: "&discount="+disocunt+"&auction_length="+auctionLength+"&batch_id="+batchID,
		success: function(data){

			jQuery('#min_accepted_bid').html(data);
			jQuery('#minimum_accepted_bid').val(data);
			/*if (!html.trim()) {
				jQuery('#upload_next').hide();
			}
			else
			{
				jQuery('#upload_next').show();
			}
			jQuery("#invoice_container").html(html);*/

			//console.log(data);
		}
    });

}
</script>
<div class="bct-summary-container">
	<div class="summary-list">
		<ul>
		<?php $hasRequest = 0; ?>
		<?php if($this->bookings): ?>
			<?php foreach ($this->bookings as $key => $booking): //echo "<pre>";	print_r($booking); echo "</pre>"; exit; ?>
				<?php
                    $fromTime = explode(":", $booking->booking_from_time);
                    $toTime = explode(":", $booking->booking_to_time);
                ?>

                <?php $bookingTimeStamp = strtotime($booking->service_booking_datetime.' '.$booking->booking_to_time); ?>
				<?php $currentTimeStamp = time(); ?>
				<?php if($currentTimeStamp>$bookingTimeStamp): ?>
					<?php continue; ?>
				<?php endif; ?>
				<?php $hasRequest = 1; ?>
				<li id="request_booking_<?php echo $booking->service_booking_id; ?>">
				<?php $link = JRoute::_("index.php?option=com_bcted&view=companyrequestdetail&request_id=".$booking->service_booking_id."&Itemid=".$Itemid); ?>
				<!-- <?php //if($booking->status == 11): ?>
					<a href="#">
				<?php //else: ?> -->
					<a href="<?php echo $link; ?>">
				<?php// endif; ?>

					<div class="prd-single-left span8">
						<div class="prd-person-nm">
							<?php echo $booking->name; ?>

						</div>
						<div class="prd-title"><?php echo $booking->service_name; ?></div>
						<div class="prd-date-time"><?php echo date('d-m-Y',strtotime($booking->service_booking_datetime)); ?></div>
						<div class="prd-date-time"><?php echo $fromTime[0].':'.$fromTime[1] . ' - ' . $toTime[0].':'.$toTime[1]; ?></div>
					</div>
					<div class="bk-single-right span4">

						<button class="reqlnk-btn"  type="button"><?php echo $booking->status_text; ?></button>

					</div>
				</a>
				</li>
			<?php endforeach; ?>
		<?php endif; ?>
		<?php if(!$hasRequest): ?>
			<div id="system-message">
                <div class="alert alert-block">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4><?php echo JText::_('COM_BCTED_COMPANYREQUESTS_NO_REQUEST_FOUND_TITLE'); ?></h4>
                    <div><p><?php echo JText::_('COM_BCTED_COMPANYREQUESTS_NO_REQUEST_FOUND_DESC'); ?></p></div>
                </div>
            </div>
		<?php endif; ?>
		</ul>
	</div>
</div>

<?php //echo $this->pagination->getListFooter(); ?>
