<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

?>
<div class="table-wrp">
<H2>Tables</H2>
<?php foreach ($this->items as $key => $item): //echo "<pre>"; print_r($item); echo "</pre>";exit; ?>
	<div class="media">
		<div class="pull-left table-img span6">
			<?php if(file_exists($item->venue_table_image)): ?>
				<img src="<?php echo $item->venue_table_image; ?>">
			<?php else: ?>
				<img src="images/tabl-img.jpg">
			<?php endif; ?>
			<?php $link = JRoute::_('index.php?option=com_bcted&view=clubtablebooking&club_id='.$item->venue_id.'&venue_id='.$item->venue_id.'&table_id='.$item->venue_table_id.'&Itemid='.$Itemid); ?>
        	<a class="book-tbl" href="<?php echo $link; ?>">Book Table</a>
        </div>
		<div class="media-body span6">
			<div class="control-group">
				<h4 class="media-heading">
					<?php
						if($item->premium_table_id)
						{
							echo ucfirst($item->venue_table_name);
						}
						else
						{
							echo ucfirst($item->custom_table_name);
						}
					?>
					<?php //echo $item->venue_table_name; ?>
				</h4>
				<h4 class="media-heading"></h4>
			</div>
			<div class="control-group">
				<h4 class="media-heading"><?php echo "Capacity : ".$item->venue_table_capacity; ?></h4>
				<h4 class="media-heading"><?php echo "Price : ". BctedHelper::currencyFormat($item->currency_code,$item->currency_sign,$item->venue_table_price); ?></h4>
			</div>

			<?php echo $item->venue_table_description; ?>

		</div>
	</div>
<?php endforeach; ?>


<!--
	<div class="media">
	  <a class="pull-left" href="#">
		<img src="images/tabl-img.jpg">
	  </a>
	  <div class="media-body">
		<div class="control-group">
			<h4 class="media-heading">Table name</h4>
			<h4 class="media-heading">Table Type</h4>
		</div>
		<div class="control-group">
			<h4 class="media-heading">8 People</h4>
			<h4 class="media-heading">$10,000 min</h4>
		</div>

	Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae elementum augue, quis faucibus nisl. Vivamus accumsan lorem ligula, vitae porta purus vestibulum sit amet. Sed ut efficitur massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	</div>
	</div>
	<div class="media">
	  <a class="pull-left" href="#">
		<img src="images/tabl-img.jpg">
	  </a>
	  <div class="media-body">
		<div class="control-group">
			<h4 class="media-heading">Table name</h4>
			<h4 class="media-heading">Table Type</h4>
		</div>
		<div class="control-group">
			<h4 class="media-heading">8 People</h4>
			<h4 class="media-heading">$10,000 min</h4>
		</div>

	Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae elementum augue, quis faucibus nisl. Vivamus accumsan lorem ligula, vitae porta purus vestibulum sit amet. Sed ut efficitur massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	</div>
	</div>
	<div class="media">
  <a class="pull-left" href="#">
	<img src="images/tabl-img.jpg">
  </a>
  <div class="media-body">
	<div class="control-group">
		<h4 class="media-heading">Table name</h4>
		<h4 class="media-heading">Table Type</h4>
	</div>
	<div class="control-group">
		<h4 class="media-heading">8 People</h4>
		<h4 class="media-heading">$10,000 min</h4>
	</div>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae elementum augue, quis faucibus nisl. Vivamus accumsan lorem ligula, vitae porta purus vestibulum sit amet. Sed ut efficitur massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
</div>
</div>
-->
</div>
