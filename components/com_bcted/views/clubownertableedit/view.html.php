<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Categorory View
 *
 * @since  0.0.1
 */
class BctedViewClubOwnerTableEdit extends JViewLegacy
{
	protected $form;

	protected $item;

	protected $companyProfile;

	protected $premiumTable;

	/**
	 * Display the Category view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	public function display($tpl = null)
	{
		// Get the Data
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');

		$user = JFactory::getUser();

		$this->companyProfile = BctedHelper::getUserElementID($user->id);

		/*echo "<pre>";
		print_r($this->companyProfile);
		echo "</pre>";
		exit;*/

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new RuntimeException(implode('<br />', $errors), 500);

			return false;
		}

		$this->premiumTable = $this->getPremiumTable();
		// Display the template
		parent::display($tpl);
	}

	public function getPremiumTable()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_premium_table'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote('0'))
			->where($db->quoteName('venue_table_id') . ' = ' . $db->quote('0'));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;

	}
}
