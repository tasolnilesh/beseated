<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Information View
 *
 * @since  0.0.1
 */
class BctedViewJetServiceInformation extends JViewLegacy
{
	protected $company;
	protected $isFavourite;
	protected $user;

	/**
	 * Display the Lgom view
	 *
	 * @param   string  $tpl  The name of the template file to parse;
	 * automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	function display($tpl = null)
	{


		// Get data from the model
		$this->company = $this->get('JetServiceDetail');
		$this->user = JFactory::getUser();

		if($this->user->id)
		{
			$this->isFavourite = BctedHelper::isFavouriteCompany($this->company->company_id,$this->user->id);
		}
		else
		{
			$this->isFavourite = 0;
		}

		/*echo "<pre>";
		print_r($this->club);
		echo "</pre>";
		exit;*/

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Display the template
		parent::display($tpl);
	}


}
