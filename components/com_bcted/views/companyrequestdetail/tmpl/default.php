<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$input = JFactory::getApplication()->input;
$Itemid = $input->get('Itemid', 0, 'int');
$show_sys_msg = $input->get('show_sys_msg', 0, 'int');

//echo $show_sys_msg;

$this->user = JFactory::getUser();
$this->isRoot = $this->user->authorise('core.admin');

?>
<script type="text/javascript">
function changeRequestStatus(status)
{
	var owner_message = jQuery('#owner_message').val();
	jQuery.ajax({
		type: "GET",
		url: "index.php?option=com_bcted&task=companyrequestdetail.changeRequstStatus",
		data: "request_id=<?php echo $this->booking->service_booking_id; ?>&status="+status+"&owner_message="+owner_message,
		success: function(response){
			if(response == "1")
				{
					//jQuery('#booking_'+bookingID).remove();
                    jQuery('#system-message').show();
                    //alert('index.php?option=com_bcted&view=companyrequests&show_sys_msg=1&Itemid=<?php echo $Itemid; ?>');
					//location.reload('index.php?option=com_bcted&view=companyrequests&show_sys_msg=1&Itemid=<?php echo $Itemid; ?>');
                    location.href = 'index.php?option=com_bcted&view=companyrequestdetail&request_id=<?php echo $this->booking->service_booking_id; ?>&Itemid=<?php echo $Itemid; ?>';
				}
			}
    });

}

function removeRequestBooking(bookingID,userID)
{

    jQuery.ajax({
        url: 'index.php?option=com_bcted&task=companyrequestdetail.deletePastBooking',
        type: 'GET',
        data: '&user_type=service&booking_id='+bookingID+'&user_id='+userID,

        success: function(response){

            if(response == "200")
            {
                jQuery('#booking_'+bookingID).remove();
                window.location.href='index.php?option=com_bcted&view=companyrequests&Itemid=<?php echo $Itemid; ?>';
            }
        }
    })
    .done(function() {
        //console.log("success");
    })
    .fail(function() {
        //console.log("error");
    })
    .always(function() {
        //console.log("complete");
    });

}
</script>

<?php if($show_sys_msg == 0): ?>
<script type="text/javascript">
    jQuery(function(){
        jQuery('#system-message').hide();
    });
</script>

<?php endif; ?>

<div class="table-wrp">

<div id="system-message">
    <div class="alert alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4><?php echo JText::_('COM_BCTED_REQUEST_STATUS_CHANGED_TITLE'); ?></h4>
        <div><p><?php echo JText::_('COM_BCTED_REQUEST_STATUS_CHANGED_DESC'); ?></p></div>
    </div>
</div>
<h1>Requst Detail</h1>

    <div class="req-detailwrp"  id="booking_<?php echo $this->booking->service_booking_id; ?>">
    <?php //echo "<pre>";print_r($this->booking); echo "</pre>"; exit; ?>
    <?php
        $fromTime = explode(":", $this->booking->booking_from_time);
        $toTime = explode(":", $this->booking->booking_to_time);
    ?>
    	<h2> <?php echo $this->booking->name; ?>  has requested the following booking:</h2>
        <h4><?php echo "<b>Service Name : </b>".$this->booking->service_name; ?></h4>
        <h4><?php echo "<b>Date : </b>".date('d-m-Y',strtotime($this->booking->service_booking_datetime)); ?></h4>
        <h4><?php echo "<b>Time From : </b>".$fromTime[0].':'.$fromTime[1] . ' <b>To</b> ' . $toTime[0].':'.$toTime[1]; ?></h4>
        <h4><b>Number of Guests : </b><?php echo $this->booking->service_booking_number_of_guest .'('.$this->booking->male_count .'M/'.$this->booking->female_count.'F)'; ?></h4>
        <h4><?php echo $this->booking->service_booking_additional_info; ?></h4>

       <?php if($this->booking->status != '9' && $this->booking->status != '11'){ ?>

        <textarea id="owner_message" rows="7" placeholder="Enter message here"></textarea>

        <?php } ?>

        <div class="row-fluid req-btnwrp">

        <?php
           if($this->booking->status == '9' || $this->booking->status == '11')
            { ?>
                    <div class="span4"><button class="btn btn-primary"  onclick="removeRequestBooking('<?php echo $this->booking->service_booking_id; ?>','<?php echo $this->booking->user_id; ?>')" type="button" >Delete</button> </div>


      <?php }
            elseif($this->booking->status == '7')
            { ?>
                    <div class="span4"><button class="btn btn-primary"  onclick="removeRequestBooking('<?php echo $this->booking->service_booking_id; ?>','<?php echo $this->booking->user_id; ?>')" type="button" >Delete</button> </div>
                    <!-- <div class="span4"><button onclick="changeRequestStatus('waiting')" class="wait-btn" type="button"></button> </div> -->
                    <div class="span4"><button onclick="changeRequestStatus('ok')" class="ok-btn pull-right" type="button"></button> </div>
            <?php
            }
            else
            { ?>

                	<div class="span4"><button onclick="changeRequestStatus('cancel')" class="cancel-btn" type="button"></button> </div>
                    <!-- <div class="span4"><button onclick="changeRequestStatus('waiting')" class="wait-btn" type="button"></button> </div>-->
                    <div class="span4"><button onclick="changeRequestStatus('ok')" class="ok-btn pull-right" type="button"></button> </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<?php //echo $this->pagination->getListFooter(); ?>
