<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
jimport( 'joomla.application.component.helper' );
jimport('joomla.filesystem.folder');
/**
 * Heartdart Message Model
 *
 * @since  0.0.1
 */
class BctedModelClubOwnerTableEdit extends JModelAdmin
{
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'Table', $prefix = 'BctedTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}



	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   0.0.1
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_bcted.table',
			'table',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   0.0.1
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_bcted.edit.Table.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}

	public function save($data,$tableID)
	{
		$tblTable = $this->getTable();
		$tblTable->load($tableID);

		$tblTable->bind($data);
		if($tblTable->store())
		{
			if(!empty($tblTable->venue_table_name))
			{
				$premiumID = $this->checkForPremiumTable($tblTable->venue_table_name,$tblTable->venue_id,$tblTable->venue_table_id);

				$tableIDNew = $tblTable->venue_table_id;
				$tblTable2 = $this->getTable();
				$tblTable2->load($tableIDNew);

				$tblTable2->premium_table_id = $premiumID;
				$tblTable2->store();
			}

			return 1;
		}

		return 0;
	}

	public function checkForPremiumTable($tableName,$venueID,$tableID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_premium_table'))
			->where($db->quoteName('p_table_name') . ' = ' . $db->quote($tableName));

		// Set the query and load the result.
		$db->setQuery($query);

		$premiumTable = $db->loadObject();

		if($premiumTable)
		{
			$queryUPDT = $db->getQuery(true);

			// Create the base update statement.
			$queryUPDT->update($db->quoteName('#__bcted_premium_table'))
				->set($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
				->set($db->quoteName('venue_table_id') . ' = ' . $db->quote($tableID))
				->where($db->quoteName('premium_id') . ' = ' . $db->quote($premiumTable->premium_id));

			// Set the query and execute the update.
			$db->setQuery($queryUPDT);
			$db->execute();

			return $premiumTable->premium_id;
		}

		return 0;

	}

	public function deleteTable($tableID,$premiumID)
	{
		$tblTable = $this->getTable();
		$tblTable->load($tableID);
		$tblTable->venue_table_active = 0;

		if($tblTable->premium_table_id)
		{
			$tblTable->custom_table_name = $tblTable->venue_table_name;
			$tblTable->venue_table_name = "";

			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			$query->update($db->quoteName('#__bcted_premium_table'))
				->set($db->quoteName('venue_id') . ' = ' . $db->quote(0))
				->set($db->quoteName('venue_table_id') . ' = ' . $db->quote(0))
				->where($db->quoteName('premium_id') . ' = ' . $db->quote($tblTable->premium_table_id));

			$db->setQuery($query);
			$db->execute();
		}

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->update($db->quoteName('#__bcted_venue_booking'))
			->set($db->quoteName('is_deleted') . ' = ' . $db->quote(1))
			->where($db->quoteName('venue_table_id') . ' = ' . $db->quote($tblTable->venue_table_id));

		$db->setQuery($query);
		$db->execute();

		if($tblTable->store())
		{
			return "200";
		}

		return "500";
	}
}

