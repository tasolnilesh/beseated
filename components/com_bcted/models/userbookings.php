<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelUserBookings extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$user = JFactory::getUser();

		$query = $db->getQuery(true);

		$query->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'))
			->where($db->quoteName('vb.venue_booking_datetime') . ' >= ' . $db->quote(date('Y-m-d')))
			->where($db->quoteName('vb.deleted_by_user') . ' = ' . $db->quote(0))
			->where($db->quoteName('vb.user_id') . ' = ' . $db->quote($user->id));

		// Create the base select statement.

		$query->select('vt.premium_table_id,vt.venue_table_name,vt.custom_table_name,vt.venue_table_image,vt.venue_table_price,vt.venue_table_capacity,venue_table_description')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=vb.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=vb.user_status');

		$query->select('v.venue_name,v.venue_address,v.venue_about,v.currency_sign,v.currency_code,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=vb.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=vb.user_id');

		$query->order($db->quoteName('vb.venue_booking_datetime') . ' DESC');

		return $query;
	}

	public function getBookedServices()
	{
		$db    = JFactory::getDbo();
		$user = JFactory::getUser();

		$query = $db->getQuery(true);
		$query->select('sb.*')
				->from($db->quoteName('#__bcted_service_booking','sb'))
				->where($db->quoteName('sb.user_id') . ' = ' . $db->quote($user->id))
				->where($db->quoteName('sb.is_deleted') . ' = ' . $db->quote('0'))
				->where($db->quoteName('sb.deleted_by_user') . ' = ' . $db->quote('0'));
				//->where($db->quoteName('sb.service_booking_datetime') . ' >= ' . $db->quote(date('Y-m-d')))
				//->order($db->quoteName('sb.service_booking_datetime') . ' DESC');

		$query->select('cs.service_name,cs.service_image,cs.service_description,cs.service_price')
			->join('LEFT','#__bcted_company_services AS cs ON cs.service_id=sb.service_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=sb.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=sb.user_status');

		$query->select('c.company_name,c.company_image,c.company_address,c.city,c.country,c.latitude,c.longitude,c.currency_code,c.currency_sign,c.company_about,c.company_signs,c.attire,c.restaurant,c.smoking,c.rating,c.drink')
			->join('LEFT','#__bcted_company AS c ON c.company_id=sb.company_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=sb.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=sb.user_id');

		//$query->order($db->quoteName('sb.service_booking_datetime') . ' DESC');
		$query->order($db->quoteName('sb.service_booking_datetime') . ' DESC');

		//echo $query->dump();

		// Set the query and load the result.
		$db->setQuery($query);

		//$db->setQuery($queryC);

		$myBookingServiceIDs = $db->loadObjectList();

		return $myBookingServiceIDs;
	}

	public function getPackagesPurchase()
	{
		$db    = JFactory::getDbo();
		$user = JFactory::getUser();

		$query = $db->getQuery(true);
		$query->select('pp.*')
			->from($db->quoteName('#__bcted_package_purchased','pp'))
			->where($db->quoteName('pp.package_datetime') . ' >= ' . $db->quote(date('Y-m-d')))
			->where($db->quoteName('pp.user_id') . ' = ' . $db->quote($user->id));

		$query->select('pckg.package_name,pckg.package_image,pckg.package_details,pckg.package_price,pckg.currency_sign AS pakcge_current_currency_sign,pckg.currency_code AS pakcge_current_currency_code,pckg.package_date')
			->join('LEFT','#__bcted_package AS pckg ON pckg.package_id=pp.package_id');

		$query->select('ps.currency_code AS package_currency_code,ps.currency_sign AS package_currency_sign')
			->join('LEFT','#__bcted_payment_status AS ps ON ps.booked_element_id=pp.package_purchase_id AND ps.booked_element_type="package"');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=pp.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=pp.user_status');

		$query->select('v.userid AS venue_owner,v.venue_name,v.city,v.country,v.latitude,v.longitude,v.currency_code,v.currency_sign,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=pp.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=pp.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=pp.user_id');

		$query->order($db->quoteName('pp.time_stamp') . ' DESC');

		/*echo $query->dump();
		exit;*/

		$db->setQuery($query);
		$packages = $db->loadObjectList();


		return $packages;
	}

	public function getPackageInvitations()
	{
		$db    = JFactory::getDbo();
		$user = JFactory::getUser();
		$query = $db->getQuery(true);

		$query->select('pi.*')
			->from($db->quoteName('#__bcted_package_invite','pi'))
			->where($db->quoteName('pi.invited_user_id') . ' = ' . $db->quote($user->id));

		$query->select('pp.user_id AS pakcage_purchased_user_id,pp.venue_id,pp.package_datetime,pp.package_time,pp.package_number_of_guest,pp.male_count,pp.female_count,pp.package_location,pp.package_additional_info,pp.total_price,pp.deposit_amount,pp.amount_payable as pp_amount_payable')
			->join('LEFT','#__bcted_package_purchased AS pp ON pp.package_purchase_id=pi.package_purchase_id');

		$query->select('pckg.package_name,pckg.package_image,pckg.package_details,pckg.package_price,pckg.currency_code AS package_currency_code,pckg.currency_sign AS package_currency_sign,pckg.package_date')
			->join('LEFT','#__bcted_package AS pckg ON pckg.package_id=pi.package_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=pi.status');

		/*$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=pp.user_status');*/

		$query->select('v.userid AS venue_owner,v.venue_name,v.city,v.country,v.latitude,v.longitude,v.currency_code,v.currency_sign,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=pp.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=pi.invited_user_id');

		$query->select('u2.name AS pakcage_purchased_user_name')
			->join('LEFT','#__users AS u2 ON u2.id=pp.user_id');

		$query->select('bu.last_name AS pakcage_purchased_user_last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=pp.user_id');

		$query->order($db->quoteName('pi.created') . ' DESC');

		/*echo $query->dump();
		exit;*/

		$db->setQuery($query);
		$packages = $db->loadObjectList();



		return $packages;
	}

	public function invite_user_in_package1($packagePurchaseID,$usersEmail)
	{
		//$emailArray = explode(",", $usersEmail);
		$emails = $usersEmail;
		/*echo "<pre>";
		print_r($emailArray);
		echo "</pre>";
		exit;*/

		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable', array());
		$tblPackagePurchased->load($packagePurchaseID);
		$tblPackage = JTable::getInstance('Package', 'BctedTable', array());
		$tblVenue = JTable::getInstance('Venue', 'BctedTable', array());
		$tblPackage->load($tblPackagePurchased->package_id);

		$packagePrice = $tblPackage->package_price;
		$tblVenue->load($tblPackage->venue_id);

		$alreadyInvitationSend = $this->getAlreadyInvitedUsers($tblPackagePurchased->package_purchase_id);

		/*echo "<pre>";
		print_r($alreadyInvitationSend);
		echo "</pre>";
		exit;*/
		$alreadyInvitedCount = count($alreadyInvitationSend);
		$invitedPersonCount = 1;

		$newBookingID = $tblPackagePurchased->package_purchase_id;

		if(!empty($emails) && $alreadyInvitedCount!=0)
		{
			$emailArray = explode(",", $emails);
			$newEmail =array();
			foreach ($emailArray as $key => $sEmail)
			{
				//if(!in_array($sEmail, $alreadyInvitationSend))
				//{
					$newEmail[] = $sEmail;
				//}
			}

			if(count($newEmail) == 0)
			{
				$newBookingID = $tblPackagePurchased->package_purchase_id;
				$this->jsonarray['code'] = 200;
				$this->jsonarray['bookingID'] = $newBookingID;

				return $this->jsonarray;
			}

			$emails = implode(",", $newEmail);
		}


		if(!empty($emails))
		{
			$emailArray = explode(",", $emails);
			$invitedPersonCount += count($emailArray);
		}

		$invitedPersonCount = $invitedPersonCount + $alreadyInvitedCount;

		$singleUserPay = $packagePrice/$invitedPersonCount;
		$singleUserPay = number_format($singleUserPay,2);

		$tblPackagePurchased->total_price = $singleUserPay;
		$tblPackagePurchased->invited_email_count = $invitedPersonCount;

		if(!$tblPackagePurchased->store())
		{
			return false;
		}

		$userEmail = array();
		$pushToUser = array();

		if(!empty($emails))
		{
			$emailArray = explode(",", $emails);
			//$emailArray = $emails;
			foreach ($emailArray as $key => $singleEmail)
			{
				// Initialiase variables.
				$db    = JFactory::getDbo();
				$query = $db->getQuery(true);

				// Create the base select statement.
				$query->select('*')
					->from($db->quoteName('#__users'))
					->where($db->quoteName('email') . ' = ' . $db->quote($singleEmail))
					->where($db->quoteName('block') . ' = ' . $db->quote(0));
				$db->setQuery($query);

				$emailUser = $db->loadObject();
				$invitedData = array();

				$tblPackageInvite = JTable::getInstance('PackageInvite', 'BctedTable',array());
				$tblPackageInvite->load(0);

				if($emailUser)
				{
					$invitedData['invited_email'] = $singleEmail;
					$invitedData['invited_user_id'] = $emailUser->id;
					$invitedData['package_purchase_id'] = $tblPackagePurchased->package_purchase_id;
					$invitedData['package_id'] = $tblPackagePurchased->package_id;
					$invitedData['status'] = 2;
					$invitedData['amount_payable'] = $singleUserPay;
					$invitedData['created'] = date('Y-m-d H:i:s');

					$pushToUser[] = $emailUser->id;
				}
				else
				{
					$invitedData['invited_email'] = $singleEmail;
					$invitedData['invited_user_id'] = 0;
					$invitedData['package_purchase_id'] = $tblPackagePurchased->package_purchase_id;
					$invitedData['package_id'] = $tblPackagePurchased->package_id;
					$invitedData['status'] = 2;
					$invitedData['amount_payable'] = $singleUserPay;
					$invitedData['created'] = date('Y-m-d H:i:s');
				}

				$tblPackageInvite->bind($invitedData);

				if($tblPackageInvite->store())
				{
					$this->sendPackagePurchageEmail2($singleEmail, JUri::base(),$tblPackage->package_name,$tblVenue->venue_name, $tblPackagePurchased->package_datetime,$tblPackagePurchased->package_time);
				}


			}

			if(count($pushToUser))
			{
				$link = JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $newBookingID . '&booking_type=package';

				if(count($pushToUser)!=0)
				{
					$loginUser = JFactory::getUser();

					$htmlLink = '';

					//$inviteID =  getIviteDetailForPackage($ids,$userType);

					$message = JText::sprintf('COM_IJOOMERADV_BCTED_PACKAGE_PURCHASE_PAYMENT_MAIL_BODY', $loginUser->name,$tblPackage->package_name,$htmlLink);

					$jsonarray['pushNotificationData']['id']         = $newBookingID;
					$jsonarray['pushNotificationData']['to']         = implode(",", $pushToUser);
					$jsonarray['pushNotificationData']['message']    = $message.';'.$link;
					$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEPURCHASEDGUESTINVITE'); //'PackagePurchasedGuestInvite';
					$jsonarray['pushNotificationData']['configtype'] = '';

					BctedHelper::sendPushNotification($jsonarray);
				}
			}
		}

		return true;
	}

	public function invite_user_in_package($packagePurchaseID,$usersEmail)
	{
		$emails = $usersEmail;

		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable', array());
		$tblPackagePurchased->load($packagePurchaseID);

		$tblPackage = JTable::getInstance('Package', 'BctedTable', array());
		$tblPackage->load($tblPackagePurchased->package_id);

		$tblVenue = JTable::getInstance('Venue', 'BctedTable', array());
		$tblVenue->load($tblPackage->venue_id);

		$packagePrice = $tblPackage->package_price;
		$invitedPersonCount = 1;

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('invited_email')
			->from($db->quoteName('#__bcted_package_invite'))
			->where($db->quoteName('package_purchase_id') . ' = ' . $db->quote($tblPackagePurchased->package_purchase_id));

		// Set the query and load the result.
		$db->setQuery($query);

		$alreadyInvitationSend = $db->loadColumn();
		$alreadyInvitedCount = count($alreadyInvitationSend);

		if($alreadyInvitedCount >= $tblPackagePurchased->package_number_of_guest)
		{
			/*$this->jsonarray['code'] = 500;
			$this->jsonarray['message'] = JText::_('COM_IJOOMERADV_BCTED_GUEST_LIST_LIMIT_OVER');
			return $this->jsonarray;*/

			return "101||".$tblPackagePurchased->package_number_of_guest;
		}

		if(!empty($emails) && $alreadyInvitedCount!=0)
		{
			$emailArray = explode(",", $emails);

			if(($alreadyInvitedCount+count($emailArray)) >= $tblPackagePurchased->package_number_of_guest)
			{
				/*$this->jsonarray['code'] = 500;
				$this->jsonarray['message'] = JText::_('COM_IJOOMERADV_BCTED_GUEST_LIST_LIMIT_OVER');
				return $this->jsonarray;*/

				return "101||".$tblPackagePurchased->package_number_of_guest;
			}

			$newEmail =array();
			foreach ($emailArray as $key => $sEmail)
			{
				if(!in_array($sEmail, $alreadyInvitationSend))
				{
					$newEmail[] = $sEmail;
				}
			}

			if(count($newEmail) == 0)
			{
				$newBookingID = $tblPackagePurchased->package_purchase_id;
				/*$this->jsonarray['code'] = 200;
				$this->jsonarray['bookingID'] = $newBookingID;

				return $this->jsonarray;*/

				return 200;
			}

			$emails = implode(",", $newEmail);
		}

		$emailArray = explode(",", $emails);
		if( count($emailArray) >= $tblPackagePurchased->package_number_of_guest)
		{
			/*$this->jsonarray['code'] = 500;
			$this->jsonarray['message'] = JText::_('COM_IJOOMERADV_BCTED_GUEST_LIST_LIMIT_OVER');
			return $this->jsonarray;*/

			return "101||".$tblPackagePurchased->package_number_of_guest;
		}

		if(!empty($emails))
		{
			$emailArray = explode(",", $emails);
			$invitedPersonCount += count($emailArray);
		}

		$invitedPersonCount = $invitedPersonCount + $alreadyInvitedCount;

		$singleUserPay = (($packagePrice * $tblPackagePurchased->package_number_of_guest)/$invitedPersonCount);

		$tblPackagePurchased->total_price = $singleUserPay;
		$tblPackagePurchased->invited_email_count = $invitedPersonCount;

		if(!$tblPackagePurchased->store())
		{
			/*IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE2'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;*/

			return 500;
		}

		$newBookingID = $tblPackagePurchased->package_purchase_id;

		$userEmail = array();
		$pushToUser = array();

		if(!empty($emails))
		{
			$emailArray = explode(",", $emails);
			//$emailArray = $emails;
			foreach ($emailArray as $key => $singleEmail)
			{
				// Initialiase variables.
				$db    = JFactory::getDbo();
				$query = $db->getQuery(true);

				// Create the base select statement.
				$query->select('*')
					->from($db->quoteName('#__users'))
					->where($db->quoteName('email') . ' = ' . $db->quote($singleEmail))
					->where($db->quoteName('block') . ' = ' . $db->quote(0));
				$db->setQuery($query);

				$emailUser = $db->loadObject();
				$invitedData = array();

				$tblPackageInvite = JTable::getInstance('PackageInvite', 'BctedTable');
				$tblPackageInvite->load(0);

				if($emailUser)
				{
					$invitedData['invited_email'] = $singleEmail;
					$invitedData['invited_user_id'] = $emailUser->id;
					$invitedData['package_purchase_id'] = $newBookingID;
					$invitedData['package_id'] = $tblPackagePurchased->package_id;
					$invitedData['status'] = 2;
					$invitedData['amount_payable'] = $singleUserPay; // $this->helper->currencyFormat('',$singleUserPay);
					$invitedData['created'] = date('Y-m-d H:i:s');

					$pushToUser[] = $emailUser->id;
				}
				else
				{
					$invitedData['invited_email'] = $singleEmail;
					$invitedData['invited_user_id'] = 0;
					$invitedData['package_purchase_id'] = $newBookingID;
					$invitedData['package_id'] = $tblPackagePurchased->package_id;
					$invitedData['status'] = 2;
					$invitedData['amount_payable'] = $singleUserPay; // $this->helper->currencyFormat('',$singleUserPay);
					$invitedData['created'] = date('Y-m-d H:i:s');
				}

				$tblPackageInvite->bind($invitedData);

				if($tblPackageInvite->store())
				{
					if($tblPackage->venue_id)
					{
						$this->sendPackagePurchageEmail2($singleEmail, JUri::base(),$tblPackage->package_name,$tblVenue->venue_name, date('d-m-Y',strtotime($tblPackagePurchased->package_datetime)),$tblPackagePurchased->package_time);
					}
					else if ($tblPackage->company_id)
					{
						$this->sendPackagePurchageEmail2($singleEmail, JUri::base(),$tblPackage->package_name,$tblCompany->company_name, date('d-m-Y',strtotime($tblPackagePurchased->package_datetime)),$tblPackagePurchased->package_time);
					}

				}

				//echo $singleEmail;

			}
		}

		//exit;
		$link = JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $newBookingID . '&booking_type=package';

		if(count($pushToUser)!=0)
		{
			$loginUser = JFactory::getUser();

			$htmlLink = '';

			//$inviteID =  getIviteDetailForPackage($ids,$userType);

			$message = JText::sprintf('COM_IJOOMERADV_BCTED_PACKAGE_PURCHASE_PAYMENT_MAIL_BODY', $loginUser->name,$tblPackage->package_name,$htmlLink);

			$jsonarray['pushNotificationData']['id']         = $newBookingID;
			$jsonarray['pushNotificationData']['to']         = implode(",", $pushToUser);
			$jsonarray['pushNotificationData']['message']    = $message.';'.$link;
			$jsonarray['pushNotificationData']['type']       = 'PackagePurchasedGuestInvite';
			$jsonarray['pushNotificationData']['configtype'] = '';

			BctedHelper::sendPushNotification($jsonarray);
		}

		return 200;

	}

	function sendPackagePurchageEmail2($emailArray,$link,$packageName,$venueName, $packageDate,$packageTime)
	{
		$packageTime = explode(":", $packageTime);
		$packageTime = $packageTime[0].':'.$packageTime[1];

		// Initialise variables.
		$app     = JFactory::getApplication();
		$config  = JFactory::getConfig();

		$site    = $config->get('sitename');
		$from    = $config->get('mailfrom');
		$sender  = $config->get('fromname');
		$email   = $emailArray; //implode(",", $emailArray);//$app->input->get('mailto');

		// echo "call<pre>";
		// print_r($email);
		// echo "</pre>";
		// exit;

		//Confirm your payment for Package PACKAGENAME

		$subject = JText::sprintf('COM_BCTED_PACKAGE_PURCHASE_PAYMENT_INVITE_MAIL_SUBJECT', $packageName);//$app->input->get('subject');

		$loginUser = JFactory::getUser();

		//$htmlLink = '<a href="'.$link.'">Click Here</a>';
		$htmlLink = '<a href="'.JUri::base().'">Website</a>';
		$imgPath = JUri::base().'images/footer-logo.png';
		$imageLink = '<img src="'.$imgPath.'"/>';

		// Build the message to send.
		$body     = JText::sprintf('COM_BCTED_PACKAGE_PURCHASE_PAYMENT_INVITE_MAIL_BODY',$loginUser->name,$venueName,$packageDate,$packageTime,$htmlLink,$imageLink);

		/*echo $body;
		exit;*/
		//$body    = sprintf($msg, $site, $sender, $from);

		// Clean the email data.
		$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);

		//$email = array("aljo1597@gmail.com","maan1539@gmail.com");
		//$email = explode($ema, string)

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body,true);

		// Check for an error.
		if ($return !== true)
		{
			return new JException(JText::_('COM__SEND_MAIL_FAILED'), 500);
		}
	}

	public function getAlreadyInvitedUsers($purchase_id)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('invited_email')
			->from($db->quoteName('#__bcted_package_invite'))
			->where($db->quoteName('package_purchase_id') . ' = ' . $db->quote($purchase_id));

		// Set the query and load the result.
		$db->setQuery($query);

		$alreadyInvitationSend = $db->loadColumn();

		return $alreadyInvitationSend;
	}
}
