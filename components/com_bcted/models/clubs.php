<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Clubs Model
 *
 * @since  0.0.1
 */
class BctedModelClubs extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		$app = JFactory::getApplication();
		$latitude = $app->input->cookie->get('latitude', '0.00');
		$longitude = $app->input->cookie->get('longitude', '0.00');
		$input = $app->input;

		$start = $app->input->get('limitstart',0);


		/*echo "<pre>";
		print_r($input);
		echo "</pre>";*/
		/*
		[option] => com_bcted
		[view] => clubs
		[Itemid] => 116
		[caption] =>
		[country] =>
		[is_smart] => 1
		[is_casual] => 0
		[is_food] => 1
		[is_drink] => 0
		[is_smoking] => 1
		[is_ratting] => 1
		[is_costly] => 1
		[near_me] => 2
		[club] => club
		[inner_search] => 1
		[view] => search
		[country] => Australia
		[club] => club
		exit;*/

		$serachType = '';
		$clubSearch = $input->get('club','club','string');
		$serviceSearch = $input->get('service','','string');
		$countrySearch = $input->get('country','','string');
		$inner_search = $input->get('inner_search', 0, 'int');
		$myfriends_attending =$input->get('myfriends_attending',0,'int');

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);




		if($clubSearch == 'club')
		{

			$caption    = $input->get('caption','','string');
			$is_smart   = $input->get('is_smart', 0, 'int');
			$is_casual  = $input->get('is_casual', 0, 'int');
			$is_food    = $input->get('is_food', 0, 'int');
			$is_drink   = $input->get('is_drink', 0, 'int');
			$is_smoking = $input->get('is_smoking', 0, 'int');
			$near_me    = $input->get('near_me', 0, 'int');
			$is_ratting = $input->get('is_ratting', 0, 'int');
			$is_costly  = $input->get('is_costly', 0, 'int');


			$limitstart  = $input->get('limitstart', 0, 'int');

			if(!isset($_GET['limitstart']))
			{
				$app->input->cookie->set('inner_search', $inner_search);
				$app->input->cookie->set('myfriends_attending', $myfriends_attending);
				$app->input->cookie->set('caption', $caption);
				$app->input->cookie->set('is_smart', $is_smart);
				$app->input->cookie->set('is_casual', $is_casual);
				$app->input->cookie->set('is_food', $is_food);
				$app->input->cookie->set('is_drink', $is_drink);
				$app->input->cookie->set('is_smoking', $is_smoking);
				$app->input->cookie->set('near_me', $near_me);
				$app->input->cookie->set('is_ratting', $is_ratting);
				$app->input->cookie->set('is_costly', $is_costly);
			}
			else
			{
				$inner_search        = $app->input->cookie->get('inner_search', '0');
				$myfriends_attending = $app->input->cookie->get('myfriends_attending', '0');
				$caption             = $app->input->cookie->get('caption', '');
				$is_smart            = $app->input->cookie->get('is_smart', '0');
				$is_casual           = $app->input->cookie->get('is_casual', '0');
				$is_food             = $app->input->cookie->get('is_food', '0');
				$is_drink            = $app->input->cookie->get('is_drink', '0');
				$is_smoking          = $app->input->cookie->get('is_smoking', '0');
				$near_me             = $app->input->cookie->get('near_me', '0');
				$is_ratting          = $app->input->cookie->get('is_ratting', '0');
				$is_costly           = $app->input->cookie->get('is_costly', '0');
			}





			$queryVT = $db->getQuery(true);
			$queryVT->select('venue_id')
				->from($db->quoteName('#__bcted_venue_table'))
				->where($db->quoteName('venue_table_active') . ' = ' . $db->quote('1'));

			$db->setQuery($queryVT);
			$venuesHasTable = $db->loadColumn();

			$venuesHasTable = array_unique($venuesHasTable);

			$query->select("a.*,(((acos(sin((".$latitude."*pi()/180)) *
            sin((a.latitude*pi()/180))+cos((".$latitude."*pi()/180)) *
            cos((a.latitude*pi()/180)) * cos(((".$longitude."- a.longitude)*
            pi()/180))))*180/pi())*60*1.1515
        ) as distance ")
				->from($db->quoteName('#__bcted_venue','a'))
				->where($db->quoteName('a.venue_active') . ' =  ' .  $db->quote(1))
				->where($db->quoteName('a.venue_id') . ' IN (' . implode(",", $venuesHasTable) .')');
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'))
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'));

			$oldCity = $app->input->cookie->get('search_in_city', '');

			if(!empty($countrySearch))
			{
				$app->input->cookie->set('search_in_city', $countrySearch);
				$search_in_city = $app->input->cookie->get('search_in_city', '');
			}
			else if (empty($countrySearch) && !empty($oldCity))
			{
				$search_in_city = $app->input->cookie->get('search_in_city', '');
			}

			if(!empty($search_in_city))
			{
				$query->where($db->quoteName('a.city') . ' LIKE  ' .  $db->quote('%'.$search_in_city.'%'));
			}

			if($inner_search ==1)
			{
				if(!empty($caption))
				{
					$query->where($db->quoteName('a.venue_name') . ' LIKE  ' .  $db->quote('%'.$caption.'%'));
				}

				if($is_drink)
				{
					$query->where($db->quoteName('a.is_drink') . ' = ' . $db->quote(1));
				}

				if($is_food)
				{
					$query->where($db->quoteName('a.is_food') . ' = ' . $db->quote(1));
				}

				if($is_smart)
				{
					$query->where($db->quoteName('a.is_smart') . ' = ' . $db->quote(1));
				}
				if($is_casual)
				{
					$query->where($db->quoteName('a.is_casual') . ' = ' . $db->quote(1));
				}

				if($is_smoking == 1)
				{
					$query->where($db->quoteName('a.is_smoking') . ' = ' . $db->quote($is_smoking));
				}
				else if($is_smoking == 2)
				{
					$query->where($db->quoteName('a.is_smoking') . ' = ' . $db->quote(0));
				}

				if($is_ratting)
				{
					$query->where($db->quoteName('a.venue_rating') . ' >= ' . $db->quote($is_ratting));
				}

				if($is_costly)
				{
					$query->where($db->quoteName('a.venue_signs') . ' >= ' . $db->quote($is_costly));
				}

				if($myfriends_attending)
				{
					$bookings = $this->getFriendsAttending();
					/*echo "Call";
					echo "<pre>";
					print_r($bookings);
					echo "</pre>";
					exit;*/
					if(count($bookings)!=0)
					{
						$query->where($db->quoteName('a.venue_id') . ' IN (' . implode(",", $bookings) . ')');
					}
					else
					{
						$query = $db->getQuery(true);
						$query->select('a.*')
							->from($db->quoteName('#__bcted_venue','a'))
							->where($db->quoteName('a.venue_id') . ' =  ' .  $db->quote(0));

						return $query;
					}
				}

				if($near_me)
				{
					$query = $query . " HAVING distance <= 20 ORDER BY distance ASC";
					//$query->order($db->quoteName('distance') . ' ASC');
				}


			}

			if(!$near_me)
			{
				$query->order($db->quoteName('a.venue_name') . ' ASC');
			}

			/*$query = $query . " HAVING distance <= 2000";
			echo $query;
			exit;*/
		}
		else
		{
			$query->select('a.*')
				->from($db->quoteName('#__bcted_venue','a'))
				->where($db->quoteName('a.venue_active') . ' =  ' .  $db->quote(1));

				$query->order($db->quoteName('a.venue_name') . ' ASC');
		}

		/*if(!empty($countrySearch))
		{*/


		//}



		$this->setState('list.limit', 21);
		$this->setState('list.start', $start);

		//echo "<pre>".$query->dump()."</pre>";
		/*exit;*/

		return $query;
	}

	public function getFriendsAttending()
	{
		$user = JFactory::getUser();
		if(!$user->id)
		{
			return array();
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('facebook_friends')
			->from($db->quoteName('#__facebook_joomla_connect'))
			->where($db->quoteName('joomla_userid') . ' = ' . $db->quote($user->id));

		// Set the query and load the result.
		$db->setQuery($query);

		$fbFriends = $db->loadResult();

		/*echo "<pre>";
		print_r($query);
		echo "</pre>";
		exit;*/

		if(empty($fbFriends))
		{
			return array();
		}



		$query1 = $db->getQuery(true);

		// Create the base select statement.
		$query1->select('userid')
			->from($db->quoteName('#__bcted_user_profile'))
			->where($db->quoteName('fbid') . ' IN (' . $fbFriends . ')');

		/*echo "<pre>";
		print_r($query1->dump());
		echo "</pre>";
		exit;*/

		// Set the query and load the result.
		$db->setQuery($query1);

		$foundArray = $db->loadColumn();

		$foundArray = implode(",", $foundArray);

		$app = JFactory::getApplication();
		$input = $app->input;
		/*$venueID = $input->get('club_id', 0, 'int');

		if(!$venueID)
		{
			return array();
		}*/

		// Initialiase variables.
		$query3 = $db->getQuery(true);

		// Create the base select statement.
		$query3->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'))
			->where($db->quoteName('vb.status') . ' = ' . $db->quote('5'))
			->where($db->quoteName('vb.user_status') . ' = ' . $db->quote('5'))
			->where($db->quoteName('vb.is_deleted') . ' = ' . $db->quote('0'))
			->where($db->quoteName('vb.venue_booking_datetime') . ' > ' . $db->quote(date('Y-m-d H:i')))
			->where($db->quoteName('vb.user_id') . ' IN (' . $foundArray . ')')
			->order($db->quoteName('vb.venue_booking_datetime') . ' ASC');

		$query3->select('vt.premium_table_id,vt.venue_table_name,vt.custom_table_name')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		$query3->select('v.venue_name')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');

		$query3->select('u.name,u.username')
			->join('LEFT','#__users AS u ON u.id=vb.user_id');

		$query3->select('bu.fbid')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=vb.user_id');



		// Set the query and load the result.
		$db->setQuery($query3);

		$bookings = $db->loadObjectList();

		$venueIDs = array();
		foreach ($bookings as $key => $booking)
		{
			$venueIDs[] = $booking->venue_id;
		}

		$venueIDs = array_unique($venueIDs);

		/*echo "call2<pre>";
		print_r($bookings);
		echo "</pre>";
		exit;*/

		return $venueIDs;
	}
}
