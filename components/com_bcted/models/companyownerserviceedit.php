<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
jimport( 'joomla.application.component.helper' );
jimport('joomla.filesystem.folder');
/**
 * Heartdart Message Model
 *
 * @since  0.0.1
 */
class BctedModelCompanyOwnerServiceEdit extends JModelAdmin
{
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'Service', $prefix = 'BctedTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}



	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   0.0.1
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_bcted.service',
			'service',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   0.0.1
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_bcted.edit.Service.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();

			if(!$data->company_id)
			{
				$user = JFactory::getUser();
				$companyDetail =  BctedHelper::getUserElementID($user->id);
				$data->company_id = $companyDetail->company_id;
			}
		}

		return $data;
	}

	public function save($data,$serviceID)
	{
		$tblService = $this->getTable();
		$tblService->load($serviceID);

		if(isset($data['service_image']) && !empty($data['service_image']))
		{
			if(!empty($tblService->service_image))
			{
				if(file_exists(JPATH_SITE.'/'.$tblService->service_image))
				{
					unlink(JPATH_SITE.'/'.$tblService->service_image);
				}
			}
		}

		$tblService->bind($data);
		if($tblService->store())
		{
			return 1;
		}

		return 0;
	}
}

