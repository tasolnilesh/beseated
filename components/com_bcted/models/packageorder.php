<?php
/**
 * @package     Bcted.Site
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Venue Order Model
 *
 * @since  0.0.1
 */
class BctedModelPackageorder extends JModelForm
{
	/**
	 * @var array
	 */
	protected $_item = null;

	protected $app;

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return JTable A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'PackagePurchased', $prefix = 'BctedTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * @since   0.0.1
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('site');

		// Load state from the request.
		$pk = $app->input->getInt('dish_id');
		$this->setState('dish.id', $pk);
	}

	/**
	 * Get Form
	 *
	 * @param   array    $data      values array
	 * @param   boolean  $loadData  values array
	 *
	 * @return string Fetched String from Table for relevant Id
	 *
	 * @since    0.0.1
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_diff.order',
			'order',
			array('control' => 'jform',
				'load_data' => $loadData));

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Load Form Data
	 *
	 * @return array array of values to be loaded into form fields
	 *
	 * @since   0.0.1
	 */
	protected function loadFormData()
	{
		$user = JFactory::getUser();

		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_diff.order.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Gets a dish
	 *
	 * @param integer $pk  Id for the dish
	 *
	 * @return mixed Object or null
	 */
	public function getItem($pk = null)
	{
		$pk = (!empty($pk)) ? $pk : (int) $this->getState('dish.id');

		if ($this->_item === null)
		{
			$this->_item = array();
		}

		if (!isset($this->_item[$pk]))
		{
			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('d.*')
				->from($db->qn('#__dfflow_dish' , 'd'))
				->where($db->qn('d.dish_id') . ' = ' . $pk);

			$query->select('r.name as rest_name')
				->join('LEFT', '#__dfflow_restaurants AS r ON r.rest_id = d.rest_id');

			// Set the query and load the result.
			$db->setQuery($query);

			try
			{
				$result = $db->loadObject();
			}
			catch (RuntimeException $e)
			{
				throw new RuntimeException($e->getMessage(), 500);
			}
		}

		return $result;
	}

	/**
	 * GetMyTickets get logged in user's purchased tickets.
	 *
	 * @return  object array  purchased tickets array.
	 */
	//public function sendPushNotification($pushToUser, $orderID, $isVoice)
	public function sendPushNotification($pushToUser, $orderID, $isVoice,$message,$pushCode)
	{
		$msh = "user : ".$pushToUser . " | Order ID : ". $orderID . " is voice : ". $isVoice . " | msg " .$message . " | push Code :". $pushCode;
		//mail("tasol.tester@gmail.com", "Main function ", $msh);
		$db = JFactory::getDbo();



		$query="SELECT `name`, `value`
				FROM `#__ijoomeradv_config`
				WHERE `name` in ('IJOOMER_PUSH_ENABLE_IPHONE','IJOOMER_PUSH_DEPLOYMENT_IPHONE','IJOOMER_PUSH_ENABLE_SOUND_IPHONE','IJOOMER_PUSH_ENABLE_ANDROID','IJOOMER_PUSH_API_KEY_ANDROID')";
		$db->setQuery($query);
		$configvalue = $db->loadAssocList('name');

		//$message = "your ticket soldout";

		$query="SELECT `device_token`,`device_type`
			FROM #__ijoomeradv_users
			WHERE `userid` = '{$pushToUser}'";

		$db->setQuery($query);
		$IJuserss = $db->loadObjectList();



		foreach ($IJuserss as $key => $IJusers)
		{
			if(empty($IJusers->device_token))
			{
				continue;
			}

			//$dtoken=array();
			if($IJusers && $IJusers->device_type=='iphone' &&
					array_key_exists('IJOOMER_PUSH_ENABLE_IPHONE', $configvalue) &&
					$configvalue['IJOOMER_PUSH_ENABLE_IPHONE']['value'] == 1)
			{

				$deviceTokens = explode(",", $IJusers->device_token);
				//$options['device_token']=$IJusers->device_token;

				for ($i = 0; $i < count($deviceTokens); $i++)
				{
					if(empty($deviceTokens[$i]))
					{
						continue;
					}

					$options=array();
					$options['device_token']=$deviceTokens[$i];
					$options['live']=intval($configvalue['IJOOMER_PUSH_DEPLOYMENT_IPHONE']['value']);
					$options['aps']['alert']=$message;
					$options['aps']['type']=$pushCode;
					$options['aps']['id']=$orderID;
					$options['aps']['sound'] = "business.wav";

					/*echo "<pre>";
					print_r($options);
					echo "</pre>";*/

					$this->sendIphonePushNotification($options);
				}

				/*$options['device_token']	= $puser->device_token;
				$options['live']			= intval(IJOOMER_PUSH_DEPLOYMENT_IPHONE);
				$options['aps']['alert']	= strip_tags($jsonarray['pushNotificationData']['message']);
				$options['aps']['type']		= $jsonarray['pushNotificationData']['type'];
				$options['aps']['id']		= ($jsonarray['pushNotificationData']['id']!=0)?$jsonarray['pushNotificationData']['id']:$jsonarray['pushNotificationData']['multiid'][$puser->userid];*/
			}
			elseif($IJusers && $IJusers->device_type=='android' &&
				array_key_exists('IJOOMER_PUSH_ENABLE_ANDROID', $configvalue) &&
				$configvalue['IJOOMER_PUSH_ENABLE_ANDROID']['value'] == 1)
			{
				//$dtoken[]=$IJusers->device_token;

				$dtoken = explode(",", $IJusers->device_token);

				for ($i = 0; $i < count($dtoken); $i++)
				{

					if(empty($dtoken[$i]))
					{
						continue;
					}
					$options=array();

					$options['registration_ids']=array($dtoken[$i]);
					//$options['registration_ids']=$dtoken[$i];
					$options['api_key']=$configvalue['IJOOMER_PUSH_API_KEY_ANDROID']['value'];
					$options['data']['message']=$message;
					$options['data']['type']=$pushCode;
					$options['data']['id'] = $orderID;

					$this->sendAndroidPushNotification($options);
				}



				/*$options['registration_ids']	= array($puser->device_token);
				$options['data']['message']		= strip_tags($jsonarray['pushNotificationData']['message']);
				$options['data']['type']		= $jsonarray['pushNotificationData']['type'];
				$options['data']['id']			= ($jsonarray['pushNotificationData']['id']!=0)?$jsonarray['pushNotificationData']['id']:$jsonarray['pushNotificationData']['multiid'][$puser->userid];*/
			}
		}
	}

	// iphone push notification
	function sendIphonePushNotification($options)
	{
		$server=($options['live']) ? 'ssl://gateway.push.apple.com:2195' : 'ssl://gateway.sandbox.push.apple.com:2195';
		$keyCertFilePath = JPATH_SITE.'/components/com_ijoomeradv/certificates/certificates.pem';
		// Construct the notification payload

		$body = array();
		$body['aps']= $options['aps'];
		$body['aps']['badge']=(isset($options['aps']['badge']) && !empty($options['aps']['badge'])) ? $options['aps']['badge'] : 1;
		$body['aps']['sound']=(isset($options['aps']['sound']) && !empty($options['aps']['sound'])) ? $options['aps']['sound'] : 'default';
		$payload = json_encode($body);

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', $keyCertFilePath);
		$fp = stream_socket_client($server, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $ctx);

		if (!$fp){
			//global mainframe;
			print "Failed to connect ".$error." ".$errorString;
			return;
		}

		$msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $options['device_token'])) . pack("n",strlen($payload)) . $payload;
		fwrite($fp, $msg);
		fclose($fp);
	}


	//android push notification
	function sendAndroidPushNotification($options){
		$url = 'https://android.googleapis.com/gcm/send';
		$options['data']['badge']=(isset($options['data']['badge']) && !empty($options['data']['badge'])) ? $options['data']['badge'] : 1;
		$fields['registration_ids']=$options['registration_ids'];
		$fields['data']=$options['data'];

		$headers = array(
			'Authorization: key='.$options['api_key'] ,
			'Content-Type: application/json'
        );
		// Open connection
		$ch = curl_init();
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Disabling SSL Certificate support temporarly
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		// Execute post
		$result = curl_exec($ch);

		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
		// Close connection
		curl_close($ch);
	}

	/*public function goForPushNotification($memberlist, $offerID, $isVoice)
	{
		$db = JFactory::getDbo();

		if($memberlist)
		{
			$query="SELECT *
					FROM `#__ijoomeradv_config`
					WHERE `name` IN ('IJOOMER_PUSH_API_KEY_ANDROID','IJOOMER_PUSH_ENABLE_ANDROID','IJOOMER_PUSH_ENABLE_IPHONE','IJOOMER_PUSH_DEPLOYMENT_IPHONE')";
			$db->setQuery($query);
			$ijoomerConfigs=$db->loadObjectList();

			$androidKey       = "";
			$enableAndroid    = "";
			$enableIphone     = "";
			$deploymentIphone = "";

			foreach ($ijoomerConfigs as $key => $ijoomerConfig)
			{
				if($ijoomerConfig->name=="IJOOMER_PUSH_API_KEY_ANDROID")
				{
					$androidKey = $ijoomerConfig->value;
				}
				else if($ijoomerConfig->name=="IJOOMER_PUSH_ENABLE_ANDROID")
				{
					$enableAndroid = $ijoomerConfig->value;
				}
				else if($ijoomerConfig->name=="IJOOMER_PUSH_ENABLE_IPHONE")
				{
					$enableIphone = $ijoomerConfig->value;
				}
				else if($ijoomerConfig->name=="IJOOMER_PUSH_DEPLOYMENT_IPHONE")
				{
					$deploymentIphone = $ijoomerConfig->value;
				}
			}

			if($androidKey == "" || $enableAndroid == "" || $enableIphone == "" || $deploymentIphone == "")
			{
				return false;
			}

			if($isVoice == 1)
			{
				$message = "Meal request confirmed by customer.";
				$pushCode = 106;
			}
			else
			{
				$message = "Dish offer confirmed by customer.";
				$pushCode = 105;
			}

			if (!empty($memberlist))
			{
				$jsonarray['pushNotificationData']['id']         = $offerID;
				$jsonarray['pushNotificationData']['to']         = $memberlist;
				$jsonarray['pushNotificationData']['message']    = $message;
				$jsonarray['pushNotificationData']['type']       = (string)$pushCode;
				$jsonarray['pushNotificationData']['configtype'] = '';
			}
			else
			{
				return false;
			}

			$query="SELECT userid,`jomsocial_params`,`device_token`,`device_type`
					FROM #__ijoomeradv_users
					WHERE `userid` IN ({$memberlist})";
			$db->setQuery($query);
			$puserlist=$db->loadObjectList();

			foreach ($puserlist as $puser){
				//check config allow for jomsocial
				if(!empty($jsonarray['pushNotificationData']['configtype']) and $jsonarray['pushNotificationData']['configtype']!=''){
					$ijparams = json_decode($puser->jomsocial_params);
					$configallow = $jsonarray['pushNotificationData']['configtype'];
				}else{
					$configallow = 1;
				}

				if(!empty($puser)){
					if($enableIphone==1 && $puser->device_type=='iphone'){

						$options=array();
						$options['device_token']	= $puser->device_token;
						$options['live']			= intval($deploymentIphone);
						$options['aps']['alert']	= strip_tags($jsonarray['pushNotificationData']['message']);
						$options['aps']['type']		= $jsonarray['pushNotificationData']['type'];
						$options['aps']['id']		= ($jsonarray['pushNotificationData']['id']!=0)?$jsonarray['pushNotificationData']['id']:$jsonarray['pushNotificationData']['multiid'][$puser->userid];
						$this->sendIphonePushNotification($options);
					}

					if($enableAndroid==1 && $puser->device_type=='android'){
						$options=array();
						$options['registration_ids']	= array($puser->device_token);
						$options['data']['message']		= strip_tags($jsonarray['pushNotificationData']['message']);
						$options['data']['type']		= $jsonarray['pushNotificationData']['type'];
						$options['data']['id']			= ($jsonarray['pushNotificationData']['id']!=0)?$jsonarray['pushNotificationData']['id']:$jsonarray['pushNotificationData']['multiid'][$puser->userid];
						$this->sendAndroidPushNotification($options,$androidKey);
					}
				}
			}
		}
	}

	public function sendIphonePushNotification($options)
	{
		$server = ($options['live']) ? 'ssl://gateway.push.apple.com:2195' : 'ssl://gateway.sandbox.push.apple.com:2195';
		$keyCertFilePath = JPATH_SITE.DS.'components'.DS.'com_ijoomeradv'.DS.'certificates'.DS.'certificates.pem';
		// Construct the notification payload
		$body = array();
		$body['aps']= $options['aps'];
		$body['aps']['badge']=(isset($options['aps']['badge']) && !empty($options['aps']['badge'])) ? $options['aps']['badge'] : 1;
		$body['aps']['sound']=(isset($options['aps']['sound']) && !empty($options['aps']['sound'])) ? $options['aps']['sound'] : 'default';
		$payload = json_encode($body);

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', $keyCertFilePath);
		$fp = stream_socket_client($server, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $ctx);

		if (!$fp){
			//global mainframe;
			print "Failed to connect ".$error." ".$errorString;
			return;
		}

		$msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $options['device_token'])) . pack("n",strlen($payload)) . $payload;
		fwrite($fp, $msg);
		fclose($fp);
	}


	public function sendAndroidPushNotification($options, $androidKey)
	{
		$url = 'https://android.googleapis.com/gcm/send';
		$options['data']['badge']=(isset($options['data']['badge']) && !empty($options['data']['badge'])) ? $options['data']['badge'] : 1;
		$fields['registration_ids']=$options['registration_ids'];
		$fields['data']=$options['data'];

		$headers = array(
            'Authorization: key='.$androidKey ,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === FALSE) {
        	die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
	}*/

	public function getLiveUserids()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('id')
			->from($db->quoteName('#__users'))
			->where($db->quoteName('block') . ' = ' . $db->quote('0'));

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadColumn();

			return $result;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}
}
