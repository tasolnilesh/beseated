<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted ClubRequestDetail Model
 *
 * @since  0.0.1
 */
class BctedModelCompanyRequestDetail extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$requestID = $input->get('request_id',0,'int');

		/*echo "Request ID : " . $requestID;
		exit;*/

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('sb.*')
			->from($db->quoteName('#__bcted_service_booking','sb'))
			->where($db->quoteName('sb.service_booking_id') . ' = ' . $db->quote($requestID));
		// Create the base select statement.

		$query->select('cs.service_name,cs.service_image,cs.service_description,cs.service_price')
			->join('LEFT','#__bcted_company_services AS cs ON cs.service_id=sb.service_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=sb.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=sb.user_status');

		$query->select('c.company_name,c.company_image,c.company_address,c.company_about')
			->join('LEFT','#__bcted_company AS c ON c.company_id=sb.company_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=sb.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=sb.user_id');

		$query->order($db->quoteName('sb.service_booking_datetime') . ' DESC');

		return $query;

	}

	public function getCompanyBooking()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$requestID = $input->get('request_id',0,'int');

		/*echo "Request ID : " . $requestID;
		exit;*/

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('sb.*')
			->from($db->quoteName('#__bcted_service_booking','sb'))
			->where($db->quoteName('sb.service_booking_id') . ' = ' . $db->quote($requestID));
		// Create the base select statement.

		$query->select('cs.service_name,cs.service_image,cs.service_description,cs.service_price')
			->join('LEFT','#__bcted_company_services AS cs ON cs.service_id=sb.service_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=sb.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=sb.user_status');

		$query->select('c.company_name,c.company_image,c.company_address,c.company_about')
			->join('LEFT','#__bcted_company AS c ON c.company_id=sb.company_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=sb.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=sb.user_id');

		$query->order($db->quoteName('sb.service_booking_datetime') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);

		$bookings = $db->loadObject();

		/*echo "<pre>";
		print_r($bookings);
		echo "</pre>";
		exit;*/

		if(!$bookings)
		{
			return array();
		}
		return $bookings;


	}


	public function changeRequestStatus($requestID,$status,$owner_message)
	{
		$tblServiceBooking = JTable::getInstance('Servicebooking','BctedTable',array());
		$tblCompany = JTable::getInstance('Company','BctedTable',array());
		$tblServiceBooking->load($requestID);

		$tblCompany->load($tblServiceBooking->company_id);

		/*echo "<pre>";
		print_r($tblVenueBooking);
		echo "</pre>";
		exit;*/

		if($status == 'cancel')
		{
			$tblServiceBooking->status = BctedHelper::getStatusIDFromStatusName('Decline');
			$tblServiceBooking->user_status = BctedHelper::getStatusIDFromStatusName('Unavailable');
		}
		else if ($status == 'waiting')
		{
			$tblServiceBooking->status = BctedHelper::getStatusIDFromStatusName('Waiting List');
			$tblServiceBooking->user_status = BctedHelper::getStatusIDFromStatusName('Waiting List');
		}
		else if ($status == 'ok')
		{
			$tblServiceBooking->status = BctedHelper::getStatusIDFromStatusName('Awaiting Payment');
			$tblServiceBooking->user_status = BctedHelper::getStatusIDFromStatusName('Available');
		}

		$tblServiceBooking->owner_message = $owner_message;
		$tblServiceBooking->is_new = 0;

		if(!$tblServiceBooking->store())
		{
			return 0;
		}



		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$appHelper            = new bctedAppHelper;

		$bookingData = $appHelper->getBookingDetailForServices($tblServiceBooking->service_booking_id,'User');
		/*echo "<pre>";
		print_r($bookingData);
		echo "</pre>";
		exit;*/

		/*if(!empty($tblServiceBooking->owner_message))
		{
			BctedHelper::sendMessage(0,$tblServiceBooking->company_id,$tblServiceBooking->service_id,0,$tblServiceBooking->user_id,$tblServiceBooking->owner_message,$bookingData,$messageType='servicerequestaccepted');
		}*/

		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "company";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$db = JFactory::getDbo();

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{
			$userProfile    = $appHelper->getUserProfile($tblServiceBooking->user_id);
			$updateMyBookingStatus = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$updateMyBookingStatus = $params->settings->pushNotification->updateMyBookingStatus;
			}

			if($updateMyBookingStatus)
			{
				//$message = 'Your booking Status has been changed by '.$tblCompany->company_name;
				$message = JText::sprintf('PUSHNOTIFICATION_TYPE_REQUESTSTATUSCHANGED_MESSAGE',$tblCompany->company_name);


				$jsonarray['pushNotificationData']['id']         = $obj->id; //$tblServiceBooking->service_booking_id; //$obj->id;
				$jsonarray['pushNotificationData']['to']         = $tblServiceBooking->user_id;
				$jsonarray['pushNotificationData']['message']    = $message;
				$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_REQUESTSTATUSCHANGED');
				$jsonarray['pushNotificationData']['configtype'] = '';

				BctedHelper::sendPushNotification($jsonarray);
			}
		}

		return 1;

	}

	public function deleteBooking($bookingID,$userType)
	{
		$tblServicebooking = JTable::getInstance('Servicebooking', 'BctedTable',array());
		$user = JFactory::getUser();

		$tblServicebooking->load($bookingID);

		if(!$tblServicebooking->service_booking_id)
		{
			//COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE
			return 400;
		}

		$status = array();
		$status[] = BctedHelper::getStatusIDFromStatusName('Booked');
		$status[] = BctedHelper::getStatusIDFromStatusName('Cancelled');
		$status[] = BctedHelper::getStatusIDFromStatusName('Decline');
		$status[] = BctedHelper::getStatusIDFromStatusName('Waiting List');
		$status[] = BctedHelper::getStatusIDFromStatusName('Unavailable');

		//echo "<pre/>";print_r($status);exit;

		//if($status != $tblVenuebooking->status)
		if(!in_array($tblServicebooking->status, $status))
		{
			//COM_IJOOMERADV_VENUE_TABLE_BOOKING_STATUS_NOT_BOOKING

			return 400;
		}

		$tblCompany = JTable::getInstance('Company', 'BctedTable',array());
		$tblCompany->load($tblServicebooking->service_id);

		/*if($tblVenue->userid != $user->id)
		{
			//COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED

			return 706;
		}*/

		//$tblVenuebooking->is_deleted = 1;
		if($userType == 'service')
		{
			$tblServicebooking->deleted_by_company = 1;
		}
		else
		{
			$tblServicebooking->deleted_by_user = 1;
		}

		if(BctedHelper::getStatusIDFromStatusName('Waiting List')==$tblServicebooking->status)
		{
			$tblServicebooking->deleted_by_user = 1;
			$tblServicebooking->deleted_by_company = 1;
		}


		if(!$tblServicebooking->store())
		{
			return 500;
		}

		return 200;
	}


}
