<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelClubGuestRequestsByDate extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{

	}

	/*protected function populateState($ordering = null, $direction = null)
	{
		echo "populateState : ".$ordering . " || " . $direction;
		parent::populateState($ordering, $direction);
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	/*public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function getVenueGuestListRequestByDate()
	{
		$user = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$elementType = BctedHelper::getUserGroupType($user->id);
		$elementDetail = BctedHelper::getUserElementID($user->id);

		$venueID = $elementDetail->venue_id;

		$db    = JFactory::getDbo();

		$queryLiveUsers = $db->getQuery(true);

		$queryLiveUsers->select('id')
			->from($db->quoteName('#__users'))
			->where($db->quoteName('block') . ' = ' . $db->quote('0'));

		// Set the query and load the result.
		$db->setQuery($queryLiveUsers);

		$users = $db->loadColumn();

		$query = $db->getQuery(true);
		$date = $input->get('guestlist_date',date('Y-m-d'),'string');

		$date = date('Y-m-d',strtotime($date));

		$query->select('vglr.*')
			->from($db->quoteName('#__bcted_venue_guest_list_request','vglr'));
		$query->where($db->quoteName('vglr.user_status') . ' = ' . $db->quote(5));
		$query->where($db->quoteName('vglr.status') . ' = ' . $db->quote(5));
		$query->where($db->quoteName('vglr.venue_id') . ' = ' . $db->quote($venueID));
		$query->where($db->quoteName('vglr.request_date') . ' = ' . $db->quote($date));


		//$users = $this->getLiveUsers();
		if(count($users))
		{
			$liveUserStr = implode(",", $users);
			$query->where($db->quoteName('vglr.user_id') . ' IN (' . $liveUserStr . ')');
		}

		// Create the base select statement.

		/*$query->select('vt.venue_table_name,vt.custom_table_name,vt.venue_table_image,vt.venue_table_price,vt.venue_table_capacity,venue_table_description')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');*/

		$query->select('vrs.status AS status_text')
			->join('LEFT','#__bcted_status AS vrs ON vrs.id=vglr.status');

		$query->select('vrus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS vrus ON vrus.id=vglr.user_status');

		$query->select('v.venue_name,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vglr.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=vglr.user_id');

		$query->select('vru.last_name,vru.phoneno')
			->join('LEFT','#__bcted_user_profile AS vru ON vru.userid=vglr.user_id');

		//$query->order($db->quoteName('vb.venue_booking_datetime') . ' DESC');
		/*if($filterType == 0)
		{
			$todayDate = date('Y-m-d');
			$query->where($db->quoteName('vglr.request_date') . ' = ' . $db->quote($todayDate));
		}
		else if($filterType == -1)
		{
			$todayDate = date('Y-m-d');
			$query->where($db->quoteName('vglr.request_date') . ' < ' . $db->quote($todayDate));
		}
		else if($filterType == 1)
		{
			$todayDate = date('Y-m-d');
			$query->where($db->quoteName('vglr.request_date') . ' > ' . $db->quote($todayDate));
		}*/

		$query->order($db->quoteName('vglr.time_stamp') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);


		/*echo $query->dump();
		exit;*/

		$result = $db->loadObjectList();

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/


		$resultRequests = array();

		foreach ($result as $key => $value)
		{
			$tempData = array();
			$tempData['requestID']      = $value->venue_guest_list_id;
			$tempData['requestedDate']  = date('d-m-Y',strtotime($value->request_date));
			$tempData['status']         = $value->status;
			$tempData['user_status']    = $value->user_status;

			$tempData['numberOfGuest']  = $value->number_of_guest;
			$tempData['maleCount']      = $value->male_count;
			$tempData['femaleCount']    = $value->female_count;

			$tempData['remainingGuest']  = $value->remaining_total;
			$tempData['remainingMale']   = $value->remaining_male;
			$tempData['remainingFemale'] = $value->remaining_female;

			$tempData['additionalInfo'] = $value->additional_info;

			$tempData['venueID']        = $value->venue_id;
			$tempData['venueName']      = $value->venue_name;
			$tempData['venueAddress']   = $value->venue_address;
			$tempData['venueAbout']     = $value->venue_about;
			$tempData['venueAmenities'] = $value->venue_amenities;
			$tempData['venueSigns']     = $value->venue_signs;
			$tempData['venueRating']    = $value->venue_rating;
			$tempData['venueTimings']   = $value->venue_timings;
			//$tempData['venueImage']     = ($value->venue_image)?JUri::base().$value->venue_image:'';
			$tempData['isSmart']        = $value->is_smart;
			$tempData['isCasual']       = $value->is_casual;
			$tempData['smoking']        = $value->is_smoking;
			$tempData['isFood']         = $value->is_food;
			$tempData['isDrink']        = $value->is_drink;
			$tempData['workingDays']    = $value->working_days;
			$tempData['venueImage']     = ($value->venue_image)?JUri::base().$value->venue_image:$this->defaultImage;
			$tempData['venueVideo']     = ($value->venue_video)?JUri::base().$value->venue_video:'';

			$tempData['userID']         = $value->user_id;
			$tempData['username']       = $value->name;
			$tempData['lastName']       = $value->last_name;

			$tempData['phoneno']        = $value->phoneno;

			$resultRequests[] = $tempData;
		}

		return $resultRequests;
	}


}
