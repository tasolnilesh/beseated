<?php
/**
 * @package     Bcted.Site
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Venue Order Model
 *
 * @since  0.0.1
 */
class BctModelVenueorder extends JModelForm
{
	/**
	 * @var array
	 */
	protected $_item = null;

	protected $app;

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return JTable A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'Venuebooking', $prefix = 'BctedTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * @since   0.0.1
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('site');

		// Load state from the request.
		$pk = $app->input->getInt('dish_id');
		$this->setState('dish.id', $pk);
	}

	/**
	 * Get Form
	 *
	 * @param   array    $data      values array
	 * @param   boolean  $loadData  values array
	 *
	 * @return string Fetched String from Table for relevant Id
	 *
	 * @since    0.0.1
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_diff.order',
			'order',
			array('control' => 'jform',
				'load_data' => $loadData));

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Load Form Data
	 *
	 * @return array array of values to be loaded into form fields
	 *
	 * @since   0.0.1
	 */
	protected function loadFormData()
	{
		$user = JFactory::getUser();

		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_diff.order.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Gets a dish
	 *
	 * @param integer $pk  Id for the dish
	 *
	 * @return mixed Object or null
	 */
	public function getItem($pk = null)
	{
		$pk = (!empty($pk)) ? $pk : (int) $this->getState('dish.id');

		if ($this->_item === null)
		{
			$this->_item = array();
		}

		if (!isset($this->_item[$pk]))
		{
			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('d.*')
				->from($db->qn('#__dfflow_dish' , 'd'))
				->where($db->qn('d.dish_id') . ' = ' . $pk);

			$query->select('r.name as rest_name')
				->join('LEFT', '#__dfflow_restaurants AS r ON r.rest_id = d.rest_id');

			// Set the query and load the result.
			$db->setQuery($query);

			try
			{
				$result = $db->loadObject();
			}
			catch (RuntimeException $e)
			{
				throw new RuntimeException($e->getMessage(), 500);
			}
		}

		return $result;
	}

	public function makeOrder()
	{
		$user = JFactory::getUser();

		if ($user->id != 0)
		{
			$app = JFactory::getApplication();

			$appInput = JFactory::getApplication()->input;

			$dish_id = $appInput->get('dish_id','0','int');
			$price = $appInput->get('amount','0.00');
			//$service = $appInput->get('service','0.00');

			if($price == '0.00')
			{
				return false;
			}

			$description = $appInput->get('description','','string');
			$service = $appInput->get('service','Collection','string');

			$serviceAddress = $appInput->get('service_address','','string');
			if(empty($service))
			{
				$service = "Collection";
				$serviceAddress = "";
			}

			$tblDish = JTable::getInstance('Dish', 'DiffTable',array());
			$tblDish->load($dish_id);

			$ipaddress = '';
			if (isset($_SERVER['HTTP_CLIENT_IP']))
				$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
			else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_X_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
			else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_FORWARDED'];
			else if(isset($_SERVER['REMOTE_ADDR']))
				$ipaddress = $_SERVER['REMOTE_ADDR'];
			else
        		$ipaddress = 'UNKNOWN';

        	$form['latitude']  = "";
			$form['longitude'] = "";

        	if($ipaddress != 'UNKNOWN')
        	{
        		$details_url = "http://ip-api.com/json/".$ipaddress;

				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL, $details_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$geoloc = json_decode(curl_exec($ch), true);

				if($geoloc['status'] == 'success')
				{
					$form['latitude']  = $geoloc['lat'];
					$form['longitude'] = $geoloc['lon'];
				}
			}


			$form['rest_id'] = $tblDish->rest_id;
			$form['cat_id'] = $tblDish->cat_id;
			$form['dish_id'] = $dish_id;
			$form['description'] = $description;
			$form['service'] = $service;
			$form['service_address'] = $serviceAddress;
			$form['quantity'] = '1';
			$form['is_site'] = '1';
			$form['price'] = $price;
			$form['date']= date('Y-m-d');


			//$form = $app->input->post->get('jform', array(), 'array');

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base insert statement.
			$query->insert($db->qn('#__dfflow_offer'))
				->columns(
					array(
						$db->qn('rest_id'),
						$db->qn('cat_id'),
						$db->qn('dish_id'),
						$db->qn('user_id'),
						$db->qn('quantity'),
						$db->qn('is_site'),
						$db->qn('status'),
						$db->qn('service'),
						$db->qn('service_address'),
						$db->qn('description'),
						$db->qn('price'),
						$db->qn('latitude'),
						$db->qn('longitude'),
						$db->qn('date'),
						$db->qn('time_stamp'),
						$db->qn('is_deleted'),
						)
					)
				->values(
						$db->q($form['rest_id']) . ', ' .
						$db->q($form['cat_id']) . ', ' .
						$db->q($form['dish_id']) . ', ' .
						$db->q($user->id) . ', ' .
						$db->q($form['quantity']) . ', ' .
						$db->q($form['is_site']) . ', ' .
						$db->q('Pending') . ', ' .
						$db->q($form['service']) . ', ' .
						$db->q($form['service_address']) . ', ' .
						$db->q($form['description']) . ', ' .
						$db->q($form['price']) . ', ' .
						$db->q($form['latitude']) . ', ' .
						$db->q($form['longitude']) . ', ' .
						$db->q($form['date']) . ', ' .
						$db->q(time()) . ', ' .
						$db->q('0')
					);

			// Set the query and execute the insert.
			$db->setQuery($query);

			try
			{
				$db->execute();
				$offer_id = $db->insertid();
				$sqlRestOwner = "SELECT owner_id FROM `#__dfflow_restaurants` WHERE rest_id={$form['rest_id']}";
				$db->setQuery($sqlRestOwner);
				$restOwner = $db->loadResult();

				$this->sendPushNotification($restOwner,$offer_id,0,"You have new dish Offer.",107);

				return true;
			}
			catch (RuntimeException $e)
			{
				throw new RuntimeException($e->getMessage(), 500);
			}

		}

		return false;
	}

	public function getMyordersConfirmed()
	{
		$user = JFactory::getUser()->id;

		$userDetail = JFactory::getUser();

		$isSeller = 0;
		$restID = 0;

		$db   = JFactory::getDbo();

		if(in_array('10', $userDetail->groups))
		{
			$isSeller = 1;

			$query3 = $db->getQuery(true);
			$query3->select('rest_id')
				->from($db->quoteName('#__dfflow_restaurants'))
				->where($db->quoteName('owner_id') . ' = ' . $user);

			$db->setQuery($query3);
			$restID = $db->loadResult();
		}

		$liveUsers = $this->getLiveUserids();
		$strLiveUser = implode(",", $liveUsers);

		// Initialiase variables.
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('o.*')
			->from($db->quoteName('#__dfflow_offer', 'o'));

		if($isSeller==1 && $restID != 0)
		{
			$query->where($db->quoteName('o.rest_id') . ' = ' . $restID);
			$query->where($db->quoteName('o.user_id') . ' IN (' . $strLiveUser . ')' );
		}
		else
		{
			$query->where($db->quoteName('o.user_id') . ' = ' . $user);
		}

		$query->where($db->quoteName('o.status') . '  = ' . $db->quote('Confirmed'))
			->order($db->quoteName('o.id') . ' DESC');

		$query->select('r.name as rest_name,r.address1,r.address2')
				->join('LEFT', '#__dfflow_restaurants AS r ON r.rest_id = o.rest_id');

		$query->select('usr.address1 AS cust_address1 ,usr.address2  AS cust_address2')
				->join('LEFT', '#__dfflow_users AS usr ON usr.user_id = o.user_id');

		$query->select('d.name as dish_name')
				->join('LEFT', '#__dfflow_dish AS d ON d.dish_id = o.dish_id');

		$query->select('ord.id AS order_id,ord.reference,ord.price AS order_price')
				->join('LEFT', '#__dfflow_order AS ord ON ord.offer_id = o.id');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			/*echo $query->dump();
			exit;*/
			$result = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), 500);
		}

		return $result;
	}

	public function getMyordersPendings()
	{
		$user = JFactory::getUser()->id;

		$userDetail = JFactory::getUser();

		$isSeller = 0;
		$restID = 0;

		$db   = JFactory::getDbo();

		if(in_array('10', $userDetail->groups))
		{
			$isSeller = 1;

			$query3 = $db->getQuery(true);
			$query3->select('rest_id')
				->from($db->quoteName('#__dfflow_restaurants'))
				->where($db->quoteName('owner_id') . ' = ' . $user);

			$db->setQuery($query3);
			$restID = $db->loadResult();
		}

		$liveUsers = $this->getLiveUserids();
		$strLiveUser = implode(",", $liveUsers);

		// Initialiase variables.
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('o.*')
			->from($db->quoteName('#__dfflow_offer', 'o'));

		if($isSeller==1 && $restID != 0)
		{
			$query->where($db->quoteName('o.rest_id') . ' = ' . $restID);
			$query->where($db->quoteName('o.user_id') . ' IN (' . $strLiveUser . ')' );
		}
		else
		{
			$query->where($db->quoteName('o.user_id') . ' = ' . $user);
		}

		$query->where($db->quoteName('o.status') . ' = ' . $db->quote('Accepted'))
			->order($db->quoteName('o.id') . ' DESC');

		$query->select('r.name as rest_name,r.address1,r.address2')
				->join('LEFT', '#__dfflow_restaurants AS r ON r.rest_id = o.rest_id');

		$query->select('usr.address1 AS cust_address1 ,usr.address2  AS cust_address2')
				->join('LEFT', '#__dfflow_users AS usr ON usr.user_id = o.user_id');

		$query->select('d.name as dish_name')
				->join('LEFT', '#__dfflow_dish AS d ON d.dish_id = o.dish_id');

		/*$query->select('ord.id AS order_id,ord.reference,ord.price AS order_price')
				->join('LEFT', '#__dfflow_order AS ord ON ord.offer_id = o.id')
				->where($db->quoteName('ord.payment_status') . ' <> '. $db->quote('np'));*/

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			/*echo $query->dump();
			exit;*/
			$result = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), 500);
		}

		return $result;
	}

	public function conformOrder()
	{
		$user    = JFactory::getUser();
		$jinput  = JFactory::getApplication()->input;

		$dish_id = $jinput->get('item_number', 0);
		$qty     = $jinput->get('quantity', 0);
		$offerID = $jinput->get('custom', '');

		// Initialiase variables.
		$db        = JFactory::getDbo();

		$query = "UPDATE #__dfflow_offer
				SET status = 'Confirmed'
				WHERE id = $offerID";

		// Set the query and execute the update.
		$db->setQuery($query);

		try
		{
			$db->execute();
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		$table                 = JTable::getInstance('Orders', 'DiffTable', array());
		$tblOffer              = JTable::getInstance('Offer', 'DiffTable', array());
		$tblOffer->load($offerID);
		$postOrder['offer_id'] = $offerID;
		$postOrder['rest_id']  = $tblOffer->rest_id;
		$postOrder['price']    = $jinput->get('item_number', '');
		$postOrder['created '] = date('Y-m-d h:i:s');
		$postOrder['time_stamp'] = time();
		$postOrder['price']          = $tblOffer->price;
		$table->bind($postOrder);
		$table->store();
	}

	public function notifyoffer()
	{
		$user    = JFactory::getUser();
		$jinput  = JFactory::getApplication()->input;

		$trackingID = $jinput->get('tracking_id', 0);
		$status     = $jinput->get('status', 0);
		$payKey = $jinput->get('pay_key', '');
		$orderID = $jinput->get('order_id', 0);

		if($orderID == 0)
		{
			return false;
		}

		$tblOrders = JTable::getInstance('Orders', 'DiffTable', array());

		$tblOrders->load($orderID);

		if(!$tblOrders->id)
		{
			return false;
		}

		$tblRestaurant = JTable::getInstance('Restaurant', 'DiffTable', array());
		$tblRestaurant->load($tblOrders->rest_id);
		//$referenceID = $tblRestaurant->reference_id + 1;

		//$refString  = substr($tblRestaurant->name, 0, 3);
		//$threeDigit = str_pad( $referenceID, 3, "0", STR_PAD_LEFT );
		//$orderPostData['reference'] = strtolower($refString).$threeDigit;
		//$restPost['reference_id'] = $referenceID;
		//$tblRestaurant->bind($restPost);
		//$tblRestaurant->store();

		$orderPostData['tracking_id'] = $trackingID;
		$orderPostData['payment_status'] = $status;
		$orderPostData['pay_key'] = $payKey;
		//$orderPostData['time_stamp'] = time();

		$tblOrders->bind($orderPostData);

		if($tblOrders->store())
		{
			if($tblOrders->is_voice == 0)
			{
				$tblOffer  = JTable::getInstance('Offer', 'DiffTable', array());
				$tblOffer->load($tblOrders->offer_id);
				$offerPostData['status'] = "Confirmed";
				$tblOffer->bind($offerPostData);
				$tblOffer->store();

				$tblDidffUser = JTable::getInstance('Profile', 'DiffTable');
				$tblDidffUser->load($tblOffer->user_id);
				$userPostData['reward_point'] = $tblDidffUser->reward_point + 1;
				$tblDidffUser->bind($userPostData);
				$tblDidffUser->store();
			}
			else
			{
				$tblVoice  = JTable::getInstance('Voice', 'DiffTable', array());
				$tblVoice->load($tblOrders->voice_id);
				$offerPostData['status'] = "Confirmed";
				$tblVoice->bind($offerPostData);
				$tblVoice->store();

				$tblDidffUser = JTable::getInstance('Profile', 'DiffTable');
				$tblDidffUser->load($tblVoice->user_id);
				$userPostData['reward_point'] = $tblDidffUser->reward_point + 1;
				$tblDidffUser->bind($userPostData);
				$tblDidffUser->store();
			}

			//$this->sendPushNotification($tblRestaurant->owner_id, $orderID, $tblOrders->is_voice);
			if($tblOrders->is_voice==1)
			{
				$this->sendPushNotification($tblRestaurant->owner_id, $orderID, 1,"Meal request confirmed by customer.",106);
			}
			else
			{
				$this->sendPushNotification($tblRestaurant->owner_id, $orderID, 0,"Dish offer confirmed by customer.",105);
			}

		}
	}

	public function notifyofferNotTip()
	{
		$user    = JFactory::getUser();
		$jinput  = JFactory::getApplication()->input;

		$trackingID = $jinput->get('txn_id', '');
		$status     = $jinput->get('payment_status', '');
		$payKey = $jinput->get('verify_sign', '');
		$orderID = $jinput->get('order_id', 0);

		if($orderID == 0)
		{
			return false;
		}

		$tblOrders = JTable::getInstance('Orders', 'DiffTable', array());

		$tblOrders->load($orderID);

		if(!$tblOrders->id)
		{
			return false;
		}

		$tblRestaurant = JTable::getInstance('Restaurant', 'DiffTable', array());
		$tblRestaurant->load($tblOrders->rest_id);
		$referenceID = $tblRestaurant->reference_id + 1;

		$refString  = substr($tblRestaurant->name, 0, 3);
		$threeDigit = str_pad( $referenceID, 3, "0", STR_PAD_LEFT );
		$orderPostData['reference'] = strtolower($refString).$threeDigit;
		$restPost['reference_id'] = $referenceID;
		$tblRestaurant->bind($restPost);
		$tblRestaurant->store();

		$orderPostData['tracking_id'] = $trackingID;
		$orderPostData['payment_status'] = strtoupper($status);
		$orderPostData['pay_key'] = $payKey;
		$orderPostData['time_stamp'] = time();

		$tblOrders->bind($orderPostData);

		if($tblOrders->store())
		{
			$offerID = 0;
			if($tblOrders->is_voice == 0)
			{
				$tblOffer  = JTable::getInstance('Offer', 'DiffTable', array());
				$tblOffer->load($tblOrders->offer_id);
				$offerPostData['status'] = "Confirmed";
				$tblOffer->bind($offerPostData);
				$tblOffer->store();
				$offerID = $tblOrders->offer_id;

				$tblDidffUser = JTable::getInstance('Profile', 'DiffTable');
				$tblDidffUser->load($tblOffer->user_id);
				$userPostData['reward_point'] = $tblDidffUser->reward_point + 1;
				$tblDidffUser->bind($userPostData);
				$tblDidffUser->store();
			}
			else
			{
				$tblVoice  = JTable::getInstance('Voice', 'DiffTable', array());
				$tblVoice->load($tblOrders->voice_id);
				$offerPostData['status'] = "Confirmed";
				$tblVoice->bind($offerPostData);
				$tblVoice->store();
				$offerID = $tblOrders->voice_id;

				$tblDidffUser = JTable::getInstance('Profile', 'DiffTable');
				$tblDidffUser->load($tblVoice->user_id);
				$userPostData['reward_point'] = $tblDidffUser->reward_point + 1;
				$tblDidffUser->bind($userPostData);
				$tblDidffUser->store();
			}

			$tblRestaurant = JTable::getInstance('Restaurant', 'DiffTable');
			$tblRestaurant->load($tblOrders->rest_id);

			//$this->goForPushNotification($tblRestaurant->owner_id, $tblOrders->is_voice);

			if($tblOrders->is_voice==1)
			{
				$this->sendPushNotification($tblRestaurant->owner_id, $orderID, 1,"Meal request confirmed by customer.",106);
			}
			else
			{
				$this->sendPushNotification($tblRestaurant->owner_id, $orderID, 0,"Dish offer confirmed by customer.",105);
			}
		}

		return true;
	}

	/**
	 * GetMyTickets get logged in user's purchased tickets.
	 *
	 * @return  object array  purchased tickets array.
	 */
	//public function sendPushNotification($pushToUser, $orderID, $isVoice)
	public function sendPushNotification($pushToUser, $orderID, $isVoice,$message,$pushCode)
	{
		$msh = "user : ".$pushToUser . " | Order ID : ". $orderID . " is voice : ". $isVoice . " | msg " .$message . " | push Code :". $pushCode;
		mail("tasol.tester@gmail.com", "Main function ", $msh);
		$db = JFactory::getDbo();

		/*if($isVoice == 1)
		{
			$message = "Meal request confirmed by customer.";
			$pushCode = 106;
		}
		else
		{
			$message = "Dish offer confirmed by customer.";
			$pushCode = 105;
		}*/

		$query="SELECT `name`, `value`
				FROM `#__ijoomeradv_config`
				WHERE `name` in ('IJOOMER_PUSH_ENABLE_IPHONE','IJOOMER_PUSH_DEPLOYMENT_IPHONE','IJOOMER_PUSH_ENABLE_SOUND_IPHONE','IJOOMER_PUSH_ENABLE_ANDROID','IJOOMER_PUSH_API_KEY_ANDROID')";
		$db->setQuery($query);
		$configvalue = $db->loadAssocList('name');

		//$message = "your ticket soldout";

		$query="SELECT `device_token`,`device_type`
			FROM #__ijoomeradv_users
			WHERE `userid` = '{$pushToUser}'";

		$db->setQuery($query);
		$IJuserss = $db->loadObjectList();



		foreach ($IJuserss as $key => $IJusers)
		{
			if(empty($IJusers->device_token))
			{
				continue;
			}

			//$dtoken=array();
			if($IJusers && $IJusers->device_type=='iphone' &&
					array_key_exists('IJOOMER_PUSH_ENABLE_IPHONE', $configvalue) &&
					$configvalue['IJOOMER_PUSH_ENABLE_IPHONE']['value'] == 1)
			{

				$deviceTokens = explode(",", $IJusers->device_token);
				//$options['device_token']=$IJusers->device_token;

				for ($i = 0; $i < count($deviceTokens); $i++)
				{
					if(empty($deviceTokens[$i]))
					{
						continue;
					}

					$options=array();
					$options['device_token']=$deviceTokens[$i];
					$options['live']=intval($configvalue['IJOOMER_PUSH_DEPLOYMENT_IPHONE']['value']);
					$options['aps']['alert']=$message;
					$options['aps']['type']=$pushCode;
					$options['aps']['id']=$orderID;
					$options['aps']['sound'] = "business.wav";

					/*echo "<pre>";
					print_r($options);
					echo "</pre>";*/

					$this->sendIphonePushNotification($options);
				}

				/*$options['device_token']	= $puser->device_token;
				$options['live']			= intval(IJOOMER_PUSH_DEPLOYMENT_IPHONE);
				$options['aps']['alert']	= strip_tags($jsonarray['pushNotificationData']['message']);
				$options['aps']['type']		= $jsonarray['pushNotificationData']['type'];
				$options['aps']['id']		= ($jsonarray['pushNotificationData']['id']!=0)?$jsonarray['pushNotificationData']['id']:$jsonarray['pushNotificationData']['multiid'][$puser->userid];*/
			}
			elseif($IJusers && $IJusers->device_type=='android' &&
				array_key_exists('IJOOMER_PUSH_ENABLE_ANDROID', $configvalue) &&
				$configvalue['IJOOMER_PUSH_ENABLE_ANDROID']['value'] == 1)
			{
				//$dtoken[]=$IJusers->device_token;

				$dtoken = explode(",", $IJusers->device_token);

				for ($i = 0; $i < count($dtoken); $i++)
				{

					if(empty($dtoken[$i]))
					{
						continue;
					}
					$options=array();

					$options['registration_ids']=array($dtoken[$i]);
					//$options['registration_ids']=$dtoken[$i];
					$options['api_key']=$configvalue['IJOOMER_PUSH_API_KEY_ANDROID']['value'];
					$options['data']['message']=$message;
					$options['data']['type']=$pushCode;
					$options['data']['id'] = $orderID;

					$this->sendAndroidPushNotification($options);
				}



				/*$options['registration_ids']	= array($puser->device_token);
				$options['data']['message']		= strip_tags($jsonarray['pushNotificationData']['message']);
				$options['data']['type']		= $jsonarray['pushNotificationData']['type'];
				$options['data']['id']			= ($jsonarray['pushNotificationData']['id']!=0)?$jsonarray['pushNotificationData']['id']:$jsonarray['pushNotificationData']['multiid'][$puser->userid];*/
			}
		}
	}

	// iphone push notification
	function sendIphonePushNotification($options)
	{
		$server=($options['live']) ? 'ssl://gateway.push.apple.com:2195' : 'ssl://gateway.sandbox.push.apple.com:2195';
		$keyCertFilePath = JPATH_SITE.'/components/com_ijoomeradv/certificates/certificates.pem';
		// Construct the notification payload

		$body = array();
		$body['aps']= $options['aps'];
		$body['aps']['badge']=(isset($options['aps']['badge']) && !empty($options['aps']['badge'])) ? $options['aps']['badge'] : 1;
		$body['aps']['sound']=(isset($options['aps']['sound']) && !empty($options['aps']['sound'])) ? $options['aps']['sound'] : 'default';
		$payload = json_encode($body);

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', $keyCertFilePath);
		$fp = stream_socket_client($server, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $ctx);

		if (!$fp){
			//global mainframe;
			print "Failed to connect ".$error." ".$errorString;
			return;
		}

		$msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $options['device_token'])) . pack("n",strlen($payload)) . $payload;
		fwrite($fp, $msg);
		fclose($fp);
	}


	//android push notification
	function sendAndroidPushNotification($options){
		$url = 'https://android.googleapis.com/gcm/send';
		$options['data']['badge']=(isset($options['data']['badge']) && !empty($options['data']['badge'])) ? $options['data']['badge'] : 1;
		$fields['registration_ids']=$options['registration_ids'];
		$fields['data']=$options['data'];

		$headers = array(
			'Authorization: key='.$options['api_key'] ,
			'Content-Type: application/json'
        );
		// Open connection
		$ch = curl_init();
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Disabling SSL Certificate support temporarly
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		// Execute post
		$result = curl_exec($ch);

		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
		// Close connection
		curl_close($ch);
	}

	/*public function goForPushNotification($memberlist, $offerID, $isVoice)
	{
		$db = JFactory::getDbo();

		if($memberlist)
		{
			$query="SELECT *
					FROM `#__ijoomeradv_config`
					WHERE `name` IN ('IJOOMER_PUSH_API_KEY_ANDROID','IJOOMER_PUSH_ENABLE_ANDROID','IJOOMER_PUSH_ENABLE_IPHONE','IJOOMER_PUSH_DEPLOYMENT_IPHONE')";
			$db->setQuery($query);
			$ijoomerConfigs=$db->loadObjectList();

			$androidKey       = "";
			$enableAndroid    = "";
			$enableIphone     = "";
			$deploymentIphone = "";

			foreach ($ijoomerConfigs as $key => $ijoomerConfig)
			{
				if($ijoomerConfig->name=="IJOOMER_PUSH_API_KEY_ANDROID")
				{
					$androidKey = $ijoomerConfig->value;
				}
				else if($ijoomerConfig->name=="IJOOMER_PUSH_ENABLE_ANDROID")
				{
					$enableAndroid = $ijoomerConfig->value;
				}
				else if($ijoomerConfig->name=="IJOOMER_PUSH_ENABLE_IPHONE")
				{
					$enableIphone = $ijoomerConfig->value;
				}
				else if($ijoomerConfig->name=="IJOOMER_PUSH_DEPLOYMENT_IPHONE")
				{
					$deploymentIphone = $ijoomerConfig->value;
				}
			}

			if($androidKey == "" || $enableAndroid == "" || $enableIphone == "" || $deploymentIphone == "")
			{
				return false;
			}

			if($isVoice == 1)
			{
				$message = "Meal request confirmed by customer.";
				$pushCode = 106;
			}
			else
			{
				$message = "Dish offer confirmed by customer.";
				$pushCode = 105;
			}

			if (!empty($memberlist))
			{
				$jsonarray['pushNotificationData']['id']         = $offerID;
				$jsonarray['pushNotificationData']['to']         = $memberlist;
				$jsonarray['pushNotificationData']['message']    = $message;
				$jsonarray['pushNotificationData']['type']       = (string)$pushCode;
				$jsonarray['pushNotificationData']['configtype'] = '';
			}
			else
			{
				return false;
			}

			$query="SELECT userid,`jomsocial_params`,`device_token`,`device_type`
					FROM #__ijoomeradv_users
					WHERE `userid` IN ({$memberlist})";
			$db->setQuery($query);
			$puserlist=$db->loadObjectList();

			foreach ($puserlist as $puser){
				//check config allow for jomsocial
				if(!empty($jsonarray['pushNotificationData']['configtype']) and $jsonarray['pushNotificationData']['configtype']!=''){
					$ijparams = json_decode($puser->jomsocial_params);
					$configallow = $jsonarray['pushNotificationData']['configtype'];
				}else{
					$configallow = 1;
				}

				if(!empty($puser)){
					if($enableIphone==1 && $puser->device_type=='iphone'){

						$options=array();
						$options['device_token']	= $puser->device_token;
						$options['live']			= intval($deploymentIphone);
						$options['aps']['alert']	= strip_tags($jsonarray['pushNotificationData']['message']);
						$options['aps']['type']		= $jsonarray['pushNotificationData']['type'];
						$options['aps']['id']		= ($jsonarray['pushNotificationData']['id']!=0)?$jsonarray['pushNotificationData']['id']:$jsonarray['pushNotificationData']['multiid'][$puser->userid];
						$this->sendIphonePushNotification($options);
					}

					if($enableAndroid==1 && $puser->device_type=='android'){
						$options=array();
						$options['registration_ids']	= array($puser->device_token);
						$options['data']['message']		= strip_tags($jsonarray['pushNotificationData']['message']);
						$options['data']['type']		= $jsonarray['pushNotificationData']['type'];
						$options['data']['id']			= ($jsonarray['pushNotificationData']['id']!=0)?$jsonarray['pushNotificationData']['id']:$jsonarray['pushNotificationData']['multiid'][$puser->userid];
						$this->sendAndroidPushNotification($options,$androidKey);
					}
				}
			}
		}
	}

	public function sendIphonePushNotification($options)
	{
		$server = ($options['live']) ? 'ssl://gateway.push.apple.com:2195' : 'ssl://gateway.sandbox.push.apple.com:2195';
		$keyCertFilePath = JPATH_SITE.DS.'components'.DS.'com_ijoomeradv'.DS.'certificates'.DS.'certificates.pem';
		// Construct the notification payload
		$body = array();
		$body['aps']= $options['aps'];
		$body['aps']['badge']=(isset($options['aps']['badge']) && !empty($options['aps']['badge'])) ? $options['aps']['badge'] : 1;
		$body['aps']['sound']=(isset($options['aps']['sound']) && !empty($options['aps']['sound'])) ? $options['aps']['sound'] : 'default';
		$payload = json_encode($body);

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', $keyCertFilePath);
		$fp = stream_socket_client($server, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $ctx);

		if (!$fp){
			//global mainframe;
			print "Failed to connect ".$error." ".$errorString;
			return;
		}

		$msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $options['device_token'])) . pack("n",strlen($payload)) . $payload;
		fwrite($fp, $msg);
		fclose($fp);
	}


	public function sendAndroidPushNotification($options, $androidKey)
	{
		$url = 'https://android.googleapis.com/gcm/send';
		$options['data']['badge']=(isset($options['data']['badge']) && !empty($options['data']['badge'])) ? $options['data']['badge'] : 1;
		$fields['registration_ids']=$options['registration_ids'];
		$fields['data']=$options['data'];

		$headers = array(
            'Authorization: key='.$androidKey ,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === FALSE) {
        	die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
	}*/

	public function getLiveUserids()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('id')
			->from($db->quoteName('#__users'))
			->where($db->quoteName('block') . ' = ' . $db->quote('0'));

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadColumn();

			return $result;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}
}
