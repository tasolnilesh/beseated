<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Clubs Model
 *
 * @since  0.0.1
 */
class BctedModelPackages extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('a.*')
			->from($db->quoteName('#__bcted_package','a'))
			->where($db->quoteName('a.package_date') . ' >=  ' .  $db->quote(date('Y-m-d')))
			->where($db->quoteName('a.venue_id') . ' <> ' . $db->quote(0))
			->where($db->quoteName('a.package_active') . ' =  ' .  $db->quote(1));

		$query->select('b.venue_name')
			->join('LEFT','#__bcted_venue AS b ON b.venue_id=a.venue_id');

		$user = JFactory::getUser();
		$userType = BctedHelper::getUserGroupType($user->id);

		if($userType == 'Club')
		{
			$venue = BctedHelper::getUserElementID($user->id);
			$query->where($db->quoteName('a.venue_id') . ' = ' . $db->quote($venue->venue_id));
		}
		else if($userType == 'ServiceProvider')
		{
			$company = BctedHelper::getUserElementID($user->id);
			$query1 = $db->getQuery(true);
			$query1->select('service_id')
				->from($db->quoteName('#__bcted_company_services'))
				->where($db->quoteName('company_id') . ' =  ' .  $db->quote($company->company_id));
			$db->setQuery($query1);
			$services = $db->loadColumn();

			/*echo "<pre>";
			print_r($query1->dump());
			echo "</pre>";
			exit;*/

			$queryService = array();

			if(count($services))
			{
				foreach ($services as $key => $service)
				{
					$queryService[] = "(FIND_IN_SET(".$db->quote($service).", ".$db->quoteName('a.company_ids')."))";
				}

				$orQuery = implode(" OR ", $queryService);

				$query->where($orQuery);
			}
			else
			{
				$query->where($db->quoteName('a.company_id') . ' =  ' .  $db->quote($company->company_id));
			}
		}

		/*$query->select('c.company_name')
			->join('LEFT','#__bcted_company AS c ON c.company_id=a.company_id');*/

		$query->order($db->quoteName('a.package_date') . ' ASC');

		/*echo "<pre>";
		print_r($query->dump());
		echo "</pre>";*/
		//exit;

		return $query;
	}


}
