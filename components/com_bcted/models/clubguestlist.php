<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelClubGuestList extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'Venueguestlist', $prefix = 'BctedTable', $config = array())
	{
		//$type = "Venuebooking";
		return JTable::getInstance($type, $prefix, $config);
	}

	public function sendGuestListRequest($data = array())
	{
		$tblVenueguestlist = $this->getTable();

		$tblVenueguestlist->load(0);

		$tblVenueguestlist->bind($data);

		if(!$tblVenueguestlist->store())
		{
			return 0;
		}

		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$appHelper            = new bctedAppHelper;

		$tblVenue = $this->getTable('Venue','BctedTable');
		$tblVenue->load($tblVenueguestlist->venue_id);

		$loginUser = JFactory::getUser();

		$message = $loginUser->name . ' has request for Guest list';

		$extraParam = array();
		$extraParam['guestListRequstID'] = $tblVenueguestlist->venue_guest_list_id;
		$extraParam['requestedDate'] = date('d-m-Y',strtotime($tblVenueguestlist->request_date));
		$extraParam['maleCount'] = $tblVenueguestlist->male_count;
		$extraParam['femaleCount'] = $tblVenueguestlist->female_count;
		$extraParam['additionalInfo'] = $tblVenueguestlist->additional_info;

		//$this->sendAddMeVenueTableMessage($venueID,$companyID,$serviceID,$tableID,$TouserID,$message,$extraParam = array(),$messageType='tableaddme');
		BctedHelper::sendMessage($tblVenue->venue_id,0,0,0,$tblVenue->userid,$message,$extraParam,$messageType='venueguestlistrequest');

		$pushcontentdata =array();
		$requestData     = $appHelper->getVenueGuestListRequest2($tblVenueguestlist->venue_guest_list_id);

		$pushcontentdata['data']               = $requestData;
		$pushcontentdata['elementType']        = "venue";
		$pushOptions['detail']['content_data'] = $pushcontentdata;

		$pushOptions = gzcompress(json_encode($pushOptions));

		$db = JFactory::getDbo();

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{

			$userProfile    = $appHelper->getUserProfile($tblVenue->userid);
			$receiveRequest = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveRequest = $params->settings->pushNotification->receiveRequest;
			}

			if($receiveRequest)
			{
				$message = 'You have new guest list request from ' . $loginUser->name;
				$message = JText::sprintf('PUSHNOTIFICATION_TYPE_NEWREQUESTRECEIVEDFORGUESTLIST_MESSAGE',$loginUser->name);


				$jsonarray['pushNotificationData']['id']         = $obj->id;
				$jsonarray['pushNotificationData']['to']         = $tblVenue->userid;
				$jsonarray['pushNotificationData']['message']    = $message;
				$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWREQUESTRECEIVEDFORGUESTLIST');
				$jsonarray['pushNotificationData']['configtype'] = '';

				BctedHelper::sendPushNotification($jsonarray);
			}
		}

		return 1;
	}
}
