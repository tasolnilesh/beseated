<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelClubBookingDetail extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{

	}

	/*protected function populateState($ordering = null, $direction = null)
	{
		echo "populateState : ".$ordering . " || " . $direction;
		parent::populateState($ordering, $direction);
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	/*public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function getClubBooking()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$bookingID = $input->get('booking_id',0,'int');

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'))
			->where($db->quoteName('vb.venue_booking_id') . ' = ' . $db->quote($bookingID));

		// Create the base select statement.

		$query->select('vt.premium_table_id,vt.venue_table_name,vt.custom_table_name,vt.venue_table_image,vt.venue_table_price,vt.venue_table_capacity,venue_table_description')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		$query->select('ps.created AS deposit_paid_on')
			->join('LEFT','#__bcted_payment_status AS ps ON ps.booked_element_id=vb.venue_booking_id AND ps.booked_element_type="venue" AND ps.paid_status=1');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=vb.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=vb.user_status');

		$query->select('v.venue_name,v.venue_address,v.currency_code,v.currency_sign,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=vb.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=vb.user_id');

		$query->order($db->quoteName('vb.venue_booking_datetime') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);

		$bookings = $db->loadObject();


		if(!$bookings)
		{
			return array();
		}
		return $bookings;


	}

	public function summaryForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('count(status) as total_count,status')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
			->group($db->quoteName('status'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	public function getRevenueForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('sum(amount_payable) as revenue')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadResult();

		return $result;
	}

	public function deleteBooking($bookingID)
	{
		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable',array());
		$user = JFactory::getUser();

		$tblVenuebooking->load($bookingID);

		if(!$tblVenuebooking->venue_booking_id)
		{
			//COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE
			return 400;
		}

		$status = BctedHelper::getStatusIDFromStatusName('Booked');



		if($status != $tblVenuebooking->status)
		{
			//COM_IJOOMERADV_VENUE_TABLE_BOOKING_STATUS_NOT_BOOKING

			return 400;
		}

		$tblVenue = JTable::getInstance('Venue', 'BctedTable',array());
		$tblVenue->load($tblVenuebooking->venue_id);

		if($tblVenue->userid != $user->id)
		{
			//COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED

			return 706;
		}

		$tblVenuebooking->is_deleted = 1;

		if(!$tblVenuebooking->store())
		{
			return 500;
		}

		return 200;



	}
}
