<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Ratings Model
 *
 * @since  0.0.1
 */
class BctedModelCompanyMessages extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$user = JFactory::getUser();

		// Create the base select statement.
		$query->select('MAX(id)')
			->from($db->quoteName('#__bcted_message'))
			->where( '(' .
					$db->quoteName('from_userid') . ' = ' . $db->quote($user->id) .' AND '.$db->quoteName('deleted_by_from').' = '.$db->quote(0).') OR ('.
					$db->quoteName('to_userid') . ' = ' . $db->quote($user->id) .' AND '.$db->quoteName('deleted_by_to').' = '.$db->quote(0) .')'
				)
			->group($db->quoteName('connection_id'));

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			/*echo $query->dump();
			exit;*/
			$connectionIDs = $db->loadColumn();
			/*echo "<pre>";
			print_r($connectionIDs);
			echo "</pre>";
			exit;*/

			if(count($connectionIDs) == 0)
			{
				return $query;
			}
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		$queryMsg = $db->getQuery(true);

		$queryMsg->select('msg.*')
			->from($db->quoteName('#__bcted_message','msg'))
			->where($db->quoteName('msg.id') . ' IN (' . implode(",",$connectionIDs).')');
			//->group($db->quoteName('msg.connection_id'));
			//->order($db->quoteName('msg.time_stamp') . ' DESC');

		//$queryMsg .= ' HAVING MAX(`id`) ORDER BY `msg`.`time_stamp` DESC';

		// Set the query and load the result.
		//$db->setQuery($query);

		/*try
		{
			$result = $db->loadColumn();
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}*/

		/*echo "<pre>";
		print_r($queryMsg);
		echo "</pre>";*/

		return $queryMsg;
	}

	public function getMessages()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		$serachType = '';
		$venueID = $input->get('club_id',0,'int');

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Set the query and load the result.
		$query->select('a.*')
			->from($db->quoteName('#__bcted_ratings','a'))
			->where($db->quoteName('a.rating_type') . ' =  ' .  $db->quote('venue'))
			->where($db->quoteName('a.rated_id') . ' =  ' .  $db->quote($venueID));

		$query->select('b.name,b.username')
			->join('LEFT','#__users AS b ON b.id=a.user_id');

		/*echo $query->dump();
		exit;*/

		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}


}
