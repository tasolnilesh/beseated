<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted ClubRequestDetail Model
 *
 * @since  0.0.1
 */
class BctedModelDemo extends JModelList
{
	protected $venueImages = array();

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$connectionID = $input->get('connection_id',0,'int');
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$queryMsg = $db->getQuery(true);

		$user = JFactory::getUser();

		// Create the base select statement.
		$queryMsg->select('msg.*')
			->from($db->quoteName('#__bcted_message','msg'))
			->where($db->quoteName('msg.connection_id') . ' = ' . $db->quote($connectionID))
			->where( '((' .
					$db->quoteName('from_userid') . ' = ' . $db->quote($user->id) .' AND '.$db->quoteName('deleted_by_from').' = '.$db->quote(0).') OR ('.
					$db->quoteName('to_userid') . ' = ' . $db->quote($user->id) .' AND '.$db->quoteName('deleted_by_to').' = '.$db->quote(0) .'))'
				)
			->order($db->quoteName('msg.time_stamp') . ' ASC');

		return $queryMsg;
	}

	public function deleteUnusedFiles()
	{

		$venues = $this->getAllActiveVenueImages();
		foreach ($venues as $key => $venue)
		{
			if(!empty($venue->venue_image))
			{
				$folderPath = JPATH_SITE.'/images/bcted/venue/'.$venue->userid.'/';
				$output = $this->recursiveDeleteForVenue($folderPath, $venue->venue_id);
			}
		}

		$tables = $this->getAllTables();
		$allTableImages = array();
		foreach ($tables as $key => $table)
		{
			if(!empty($table->venue_table_image))
			{
				$allTableImages[] = JPATH_SITE.'/'.$table->venue_table_image;
			}
		}

		if(count($allTableImages)!=0)
		{
			$folderPath = JPATH_SITE.'/images/bcted/table/';
			$output = $this->recursiveDeleteForTable($folderPath, $allTableImages);
		}

		$companies = $this->getAllCompanies();
		foreach ($companies as $key => $company)
		{
			if(!empty($company->company_image))
			{
				$folderPath = JPATH_SITE.'/images/bcted/company/'.$company->userid.'/';
				$output = $this->recursiveDeleteForCompany($folderPath, $company->company_id);
			}
		}

		$services = $this->getAllServices();
		$allServiceImages = array();
		foreach ($services as $key => $service)
		{
			if(!empty($service->service_image))
			{
				$allServiceImages[] = JPATH_SITE.'/'.$service->service_image;
			}

			/*if(!empty($service->service_image))
			{
				$folderPath = JPATH_SITE.'/images/bcted/service/'.$service->user_id.'/';
				$output = $this->recursiveDeleteForService($folderPath, $service->service_id);
			}*/
		}

		if(count($allServiceImages)!=0)
		{
			$folderPath = JPATH_SITE.'/images/bcted/service/';
			$output = $this->recursiveDeleteForService($folderPath, $allServiceImages);
		}
	}

	/**
	 * Delete a file or recursively delete a directory
	 *
	 * @param string $str Path to file or directory
	 */
	function recursiveDeleteForVenue($str,$venueID) {
		$venueDetail = $this->getSingleVenueDetail($venueID);
		$singleVenueImages = array();
		$folderPath = JPATH_SITE.'/images/bcted/venue/'.$venueDetail->userid.'/';
		if(!empty($venueDetail->venue_image))
		{ $singleVenueImages[] = JPATH_SITE.'/'.$venueDetail->venue_image; }

		if(!empty($venueDetail->venue_thumb_image))
		{ $singleVenueImages[] = JPATH_SITE.'/'.$venueDetail->venue_thumb_image; }

		if(!empty($venueDetail->venue_video))
		{ $singleVenueImages[] = JPATH_SITE.'/'.$venueDetail->venue_video; }

		if(!empty($venueDetail->venue_video_flv))
		{ $singleVenueImages[] = JPATH_SITE.'/'.$venueDetail->venue_video_flv; }

		if(!empty($venueDetail->venue_video_mp4))
		{ $singleVenueImages[] = JPATH_SITE.'/'.$venueDetail->venue_video_mp4; }

		if(!empty($venueDetail->venue_video_webm))
		{ $singleVenueImages[] = JPATH_SITE.'/'.$venueDetail->venue_video_webm; }

		if (is_file($str)) {

			if(in_array($str, $singleVenueImages))
			{
				return true;
			}

			return @unlink($str);
		}
		elseif (is_dir($str)) {
			$scan = glob(rtrim($str,'/').'/*');
			foreach($scan as $index=>$path) {
				$this->recursiveDeleteForVenue($path,$venueID);
			}
			//return @rmdir($str);
		}
	}

	private function getAllActiveVenueImages()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('venue_id,userid,venue_image,venue_thumb_image')
			->from($db->quoteName('#__bcted_venue'));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	private function getSingleVenueDetail($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObject();

		return $result;
	}



	/**
	 * Delete a file or recursively delete a directory
	 *
	 * @param string $str Path to file or directory
	 */
	function recursiveDeleteForCompany($str,$companyID)
	{
		$companyDetail = $this->getSingleCompanyDetail($companyID);
		$singleCompanyImages = array();
		$folderPath = JPATH_SITE.'/images/bcted/company/'.$companyDetail->userid.'/';

		if(!empty($companyDetail->company_image))
		{ $singleCompanyImages[] = JPATH_SITE.'/'.$companyDetail->company_image; }

		if (is_file($str)) {

			if(in_array($str, $singleCompanyImages))
			{
				return true;
			}

			return @unlink($str);
		}
		elseif (is_dir($str)) {
			$scan = glob(rtrim($str,'/').'/*');
			foreach($scan as $index=>$path) {
				$this->recursiveDeleteForCompany($path,$companyID);
			}
			//return @rmdir($str);
		}
	}

	private function getAllCompanies()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_company'));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	private function getSingleCompanyDetail($companyID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_company'))
			->where($db->quoteName('company_id') . ' = ' . $db->quote($companyID));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObject();

		return $result;
	}

	/**
	 * Delete a file or recursively delete a directory
	 *
	 * @param string $str Path to file or directory
	 */
	function recursiveDeleteForTable($str,$allTableImages)
	{
		/*$tableDetail = $this->getSingleTableDetail($tableID);
		$singleTableImages = array();
		$folderPath = JPATH_SITE.'/images/bcted/table/'.$tableDetail->user_id.'/';

		if(!empty($tableDetail->venue_table_image))
		{ $singleTableImages[] = JPATH_SITE.'/'.$tableDetail->venue_table_image; }*/

		if (is_file($str)) {

			if(in_array($str, $allTableImages))
			{
				return true;
			}

			return @unlink($str);
		}
		elseif (is_dir($str)) {
			$scan = glob(rtrim($str,'/').'/*');
			foreach($scan as $index=>$path) {
				$this->recursiveDeleteForTable($path,$allTableImages);
			}
			//return @rmdir($str);
		}
	}

	private function getAllTables()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue_table'));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	private function getSingleTableDetail($tableID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue_table'))
			->where($db->quoteName('venue_table_id') . ' = ' . $db->quote($tableID));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObject();

		return $result;
	}

	/**
	 * Delete a file or recursively delete a directory
	 *
	 * @param string $str Path to file or directory
	 */
	function recursiveDeleteForService($str,$allServiceImages)
	{
		/*$serviceDetail = $this->getSingleServiceDetail($serviceID);
		$singleServiceImages = array();
		$folderPath = JPATH_SITE.'/images/bcted/service/'.$serviceDetail->user_id.'/';

		if(!empty($serviceDetail->service_image))
		{ $singleServiceImages[] = JPATH_SITE.'/'.$serviceDetail->service_image; }*/

		if (is_file($str)) {

			if(in_array($str, $allServiceImages))
			{
				return true;
			}

			return @unlink($str);
		}
		elseif (is_dir($str)) {
			$scan = glob(rtrim($str,'/').'/*');


			foreach($scan as $index=>$path) {
				$this->recursiveDeleteForService($path,$allServiceImages);
			}
			//return @rmdir($str);
		}
	}

	private function getAllServices()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_company_services'));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	private function getSingleServiceDetail($serviceID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_company_services'))
			->where($db->quoteName('service_id') . ' = ' . $db->quote($serviceID));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObject();

		return $result;
	}

}
