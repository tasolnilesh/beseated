<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Ratings Model
 *
 * @since  0.0.1
 */
class BctedModelProfile extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	public function getProfileData()
	{
		$user = JFactory::getUser();
		$elementType = BctedHelper::getUserGroupType($user->id);
		$elementDetail = BctedHelper::getUserElementID($user->id);

		return $elementDetail;
	}

	public function saveVenueProfile($data,$id)
	{

		$tblVenue = JTable::getInstance('Venue','BctedTable',array());
		$tblVenue->load($id);

		/*echo "<pre>";
		print_r($tblVenue);
		echo "</pre>";
		exit;*/

		$tblVenue->bind($data);

		if($tblVenue->store())
		{
			$user              = new JUser;
			$user->load($tblVenue->userid);

			if($user->id)
			{
				$user->name = trim($tblVenue->venue_name);
				$user->save();
			}


			return true;
		}

		return false;

	}

	public function is_booking_in_venue($venue_id)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();

		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venue_id))
			->where($db->quoteName('status') . ' = ' . $db->quote(5));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		if(count($result)!=0)
		{
			return 1;
		}


		$query = $db->getQuery(true);

		$statusArray = array(1,9);


		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venue_id))
			->where($db->quoteName('status') . ' IN (' . implode(",", $statusArray) .')');

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		$pastCount = 0;

		foreach ($result as $key => $value)
		{
			$bookingTime = strtotime($value->venue_booking_datetime);
			$currentTime = strtotime(date('Y-m-d'));

			if($currentTime > $bookingTime)
			{
				$pastCount = $pastCount + 1;
			}
		}

		if(count($result) == $pastCount)
		{
			return 0;
		}

		return 1;

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/

		//return count($result);
	}


}
