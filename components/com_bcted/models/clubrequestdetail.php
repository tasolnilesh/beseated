<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted ClubRequestDetail Model
 *
 * @since  0.0.1
 */
class BctedModelClubRequestDetail extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{

	}

	public function getClubBooking()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$requestID = $input->get('request_id',0,'int');

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'))
			->where($db->quoteName('vb.venue_booking_id') . ' = ' . $db->quote($requestID));
		// Create the base select statement.

		$query->select('vt.premium_table_id,vt.venue_table_name,vt.custom_table_name,vt.venue_table_image,vt.venue_table_price,vt.venue_table_capacity,venue_table_description')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=vb.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=vb.user_status');

		$query->select('v.venue_name,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=vb.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=vb.user_id');

		$query->order($db->quoteName('vb.venue_booking_datetime') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);

		$bookings = $db->loadObject();

		/*echo "<pre>";
		print_r($bookings);
		echo "</pre>";
		exit;*/


		if(!$bookings)
		{
			return array();
		}
		return $bookings;


	}

	public function summaryForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('count(status) as total_count,status')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
			->group($db->quoteName('status'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	public function getRevenueForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('sum(amount_payable) as revenue')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadResult();

		return $result;
	}

	public function changeRequestStatus($requestID,$status,$owner_message)
	{
		$tblVenueBooking = JTable::getInstance('Venuebooking','BctedTable',array());
		$tblVenueBooking->load($requestID);




		if($status == 'cancel')
		{
			$tblVenueBooking->status = BctedHelper::getStatusIDFromStatusName('Decline');
			$tblVenueBooking->user_status = BctedHelper::getStatusIDFromStatusName('Unavailable');
		}
		else if ($status == 'waiting')
		{
			$tblVenueBooking->status = BctedHelper::getStatusIDFromStatusName('Waiting List');
			$tblVenueBooking->user_status = BctedHelper::getStatusIDFromStatusName('Waiting List');
		}
		else if ($status == 'ok')
		{
			$currentBookingDate = $tblVenueBooking->venue_booking_datetime;
			$datesArray         = array();
			$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' -1 day'));
			$datesArray[]       = $tblVenueBooking->venue_booking_datetime;
			$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' +1 day'));
			$currentBookingFrom = $tblVenueBooking->booking_from_time;
			$currentBookingTo   = $tblVenueBooking->booking_to_time;

			if (strtotime($currentBookingFrom)>strtotime($currentBookingTo))
			{
				$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
				$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo.' +1 day'));
			} else {
				$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
				$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo));
			}

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('*')
				->from($db->quoteName('#__bcted_venue_booking'))
				->where($db->quoteName('venue_id') . ' = ' . $db->quote($tblVenueBooking->venue_id))
				->where($db->quoteName('venue_table_id') . ' = ' . $db->quote($tblVenueBooking->venue_table_id))
				->where($db->quoteName('venue_booking_id') . ' <> ' . $db->quote($tblVenueBooking->venue_booking_id))
				->where($db->quoteName('status') . ' = ' . $db->quote(5))
				->where($db->quoteName('venue_booking_datetime') . ' IN (\''. implode("','", $datesArray) .'\')' );

			// Set the query and load the result.
			$db->setQuery($query);

			$slotBooked = 0;
			$bookingsOnSameDate = $db->loadObjectList();
			$timeSlots = array();

			foreach ($bookingsOnSameDate as $key => $booking)
			{
				$bookingDate = $booking->venue_booking_datetime;
				$bookingFrom = $booking->booking_from_time;
				$bookingTo = $booking->booking_to_time;

				/*echo "<br />Date Time : " . $bookingDate .' '.$bookingTo;
				echo "<br />Combine : ".date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo.' +1 days'));
				exit;


				echo "<br />Date : " . $bookingDate;
				echo "<br />From : ".strtotime($bookingDate.' '.$bookingFrom);
				echo "<br />To : ".strtotime($bookingDate.' '.$bookingTo);*/
				//exit;

				if (strtotime($bookingDate.' '.$bookingFrom)>strtotime($bookingDate.' '.$bookingTo)) {
					$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
					$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo.' +1 days'));
				} else {
					$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
					$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo));
				}

				/*echo "<br /> Booking To Last : " .$bookingTo;
				exit;*/

				$numbercurrentBookingFrom = strtotime($currentBookingFrom);
				$numbercurrentBookingTo   = strtotime($currentBookingTo);
				$numberbookingFrom        = strtotime($bookingFrom);
				$numberbookingTo          = strtotime($bookingTo);

				/*echo "currentBookingFrom:" . $currentBookingFrom . " || ".$numbercurrentBookingFrom."<br />";
				echo "currentBookingTo:" . $currentBookingTo . " || ".$numbercurrentBookingTo."<br /><br />";
				echo "bookingFrom:" . $bookingFrom . " || ".$numberbookingFrom."<br />";
				echo "bookingTo:" . $bookingTo . " || ".$numberbookingTo."<br /><br />";*/

				if(($numberbookingFrom < $numbercurrentBookingFrom) && ($numbercurrentBookingFrom < $numberbookingTo))
				{
					$slotBooked = $slotBooked + 1;
				}
				else if(($numberbookingFrom < $numbercurrentBookingTo) && ($numbercurrentBookingTo < $numberbookingTo))
				{
					$slotBooked = $slotBooked + 1;
				}
				else if(($numberbookingTo == $numbercurrentBookingTo) && ($numbercurrentBookingFrom == $numberbookingFrom))
				{
					$slotBooked = $slotBooked + 1;
				}
			}

			$tblVenueBooking->status = BctedHelper::getStatusIDFromStatusName('Awaiting Payment');
			$tblVenueBooking->user_status = BctedHelper::getStatusIDFromStatusName('Available');
		}

		$tblVenueBooking->owner_message = $owner_message;
		$tblVenueBooking->is_new = 0;

		/*echo "is Slot Booked : " . $slotBooked;
		exit;*/
		if($slotBooked)
		{
			return 3;
		}

		/*echo "end";
		exit;*/

		if(!$tblVenueBooking->store())
		{
			return 0;
		}

		$tblVenue = JTable::getInstance('Venue','BctedTable',array());
		$tblVenue->load($tblVenueBooking->venue_id);

		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$appHelper            = new bctedAppHelper;

		$bookingData = $appHelper->getBookingDetailForVenueTable($requestID,'Venue');

		/*if(!empty($tblVenueBooking->owner_message))
		{
			BctedHelper::sendMessage($tblVenueBooking->venue_id,0,0,$tblVenueBooking->venue_table_id,$tblVenueBooking->user_id,$tblVenueBooking->owner_message,$bookingData,$messageType='tablerequestaccepted');
		}*/

		$pushcontentdata=array();



		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "venue";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$db = JFactory::getDbo();

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{

			$userProfile    = $appHelper->getUserProfile($tblVenueBooking->user_id);
			$updateMyBookingStatus = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$updateMyBookingStatus = $params->settings->pushNotification->updateMyBookingStatus;
			}

			if($updateMyBookingStatus)
			{
				//$message = 'Your booking Status has been changed by '.$tblVenue->venue_name;
				$message = JText::sprintf('PUSHNOTIFICATION_TYPE_REQUESTSTATUSCHANGED_MESSAGE',$tblVenue->venue_name);

				$jsonarray['pushNotificationData']['id']         = $obj->id;
				$jsonarray['pushNotificationData']['to']         = $tblVenueBooking->user_id;
				$jsonarray['pushNotificationData']['message']    = $message;
				$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_REQUESTSTATUSCHANGED'); //'RequestStatusChanged';
				$jsonarray['pushNotificationData']['configtype'] = '';

				BctedHelper::sendPushNotification($jsonarray);
			}
		}

		return 1;

	}


}

