<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelClubFriendsAttending extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Filter: like / search
		$search = $this->getState('filter.search');

		// Create the base select statement.
		$query->select('a.*,b.media,b.media_type,c.lib_name')
			->from($db->quoteName('#__heartdart_message','a'));

		$query->join('LEFT','#__heartdart_message_media as b on a.media_id = b.media_id');
		$query->join('LEFT','#__heartdart_library as c on a.library_id = c.library_id');
		$query->order($db->quoteName('a.msg_id') . ' DESC');

		if (!empty($search))
		{
			$like = $db->quote('%' . $search . '%');
			//$query->where('a.caption LIKE ' . $like);
		}

		// Filter by published state
		$published = $this->getState('filter.published');
		//$fullordering = $this->getState('list.fullordering');
		//echo "<br>fullordering : " . $fullordering;
		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.published IN (0, 1))');
		}

		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering', 'a.msg_id');
		$orderDirn 	= $this->state->get('list.direction', 'asc');

		$query->group('a.msg_id');

		//echo $orderCol . " || " . $orderDirn;
		$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));

		//$query->order($db->quoteName('a.post_id') . ' DESC');

		//echo $query->dump();exit;

		return $query;
	}

	/*protected function populateState($ordering = null, $direction = null)
	{
		echo "populateState : ".$ordering . " || " . $direction;
		parent::populateState($ordering, $direction);
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	/*public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function getMyFbFriendID()
	{
		$user = JFactory::getUser();
		if(!$user->id)
		{
			return array();
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('facebook_friends')
			->from($db->quoteName('#__facebook_joomla_connect'))
			->where($db->quoteName('joomla_userid') . ' = ' . $db->quote($user->id));

		// Set the query and load the result.
		$db->setQuery($query);

		$fbFriends = $db->loadResult();

		/*echo "<pre>";
		print_r($query);
		echo "</pre>";
		exit;*/

		if(empty($fbFriends))
		{
			return array();
		}



		$query1 = $db->getQuery(true);

		// Create the base select statement.
		$query1->select('userid')
			->from($db->quoteName('#__bcted_user_profile'))
			->where($db->quoteName('fbid') . ' IN (' . $fbFriends . ')');

		/*echo "<pre>";
		print_r($query1->dump());
		echo "</pre>";
		exit;*/

		// Set the query and load the result.
		$db->setQuery($query1);

		$foundArray = $db->loadColumn();

		$foundArray = implode(",", $foundArray);

		$app = JFactory::getApplication();
		$input = $app->input;
		$venueID = $input->get('club_id', 0, 'int');

		if(!$venueID)
		{
			return array();
		}

		// Initialiase variables.
		$query3 = $db->getQuery(true);

		// Create the base select statement.
		$query3->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'))
			->where($db->quoteName('vb.status') . ' = ' . $db->quote('5'))
			->where($db->quoteName('vb.user_status') . ' = ' . $db->quote('5'))
			->where($db->quoteName('vb.venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('vb.is_deleted') . ' = ' . $db->quote('0'))
			->where($db->quoteName('vb.venue_booking_datetime') . ' > ' . $db->quote(date('Y-m-d H:i')))
			->where($db->quoteName('vb.user_id') . ' IN (' . $foundArray . ')')
			->order($db->quoteName('vb.venue_booking_datetime') . ' ASC');

		$query3->select('vt.premium_table_id,vt.venue_table_name,vt.custom_table_name')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		$query3->select('v.venue_name')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');

		$query3->select('u.name,u.username')
			->join('LEFT','#__users AS u ON u.id=vb.user_id');

		$query3->select('bu.fbid')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=vb.user_id');



		// Set the query and load the result.
		$db->setQuery($query3);

		$bookings = $db->loadObjectList();


		return $bookings;


	}


}
