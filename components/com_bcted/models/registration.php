<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
jimport( 'joomla.application.component.helper' );
jimport('joomla.filesystem.folder');
/**
 * Heartdart Message Model
 *
 * @since  0.0.1
 */
class BctedModelRegistration extends JModelAdmin
{
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'Profile', $prefix = 'BctedTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}



	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   0.0.1
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_bcted.registration',
			'registration',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   0.0.1
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_bcted.edit.Registration.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}

	public function save($data)
	{
		$post['username']  = $data['email'];
		$post['relname']   = $data['first_name'];
		$post['password']  = $data['password'];
		$post['password1'] = $data['password'];
		$post['password2'] = $data['password2'];
		$post['token']     = $data['password'];
		$post['phoneno']   = $data['phoneno'];
		$post['email1']    = $data['email'];
		$post['email2']    = $data['email'];
		$post['email']     = $data['email'];
		$post['last_name'] = $data['last_name'];
		$post['city']      = (isset($data['city']))?$data['city']:'';

		$db = JFactory::getDbo();

		/*$query="SELECT COUNT(phoneno)
				FROM `#__bcted_user_profile`
				WHERE phoneno='".str_replace("\n","",trim($data['phoneno']))."'";
		$db->setQuery($query);

	 	if($db->loadResult() > 0)
	 	{ // check if user already exist
			//IJReq::setResponseCode(709);
			//IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PHONE_ALREADY_EXIST'));
			return 709;
		}*/

		$query = $db->getQuery(true);
		$username = str_replace("\n", "", trim($post['username']));
		// Create the base select statement.
		$query->select('id')
			->from($db->qn('#__users'))
			->where($db->qn('username') . ' = ' . $db->q($username));
		$db->setQuery($query);
		if ( $db->loadResult() > 0)
		{
			// Check if user already exist
			//IJReq::setResponseCode(701);
			//IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_USERNAME_ALREADY_EXIST'));

			return 701;
		}

		$query = $db->getQuery(true);
		$emails = str_replace("\n", "", trim($post['email']));
		// Create the base select statement.
		$query->select('id')
			->from($db->qn('#__users'))
			->where($db->qn('email') . ' = ' . $db->q($emails));
		$db->setQuery($query);
		if ($db->loadResult() > 0)
		{
			// Check if email id already exist
			//IJReq::setResponseCode(702);
			//IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_EMAIL_ALREADY_EXIST'));

			return 702;
		}

		$params         = JComponentHelper::getParams('com_users');
		$system         = $params->get('new_usertype', 2);
		//$useractivation = $params->get('useractivation');
		//$sendpassword   = $params->get('sendpassword', 1);

		// Initialise the table with JUser.
		$user              = new JUser;
		$post['name']      = trim(str_replace("\n", "", $post['relname']));
		$post['username']  = trim(str_replace("\n", "", $post['username']));
		$post['password']  = $post['password1'] = $post['password2'] = trim(str_replace("\n", "", $post['password']));
		$post['email']     = $post['email1'] = $post['email2'] = trim(str_replace("\n", "", $post['email']));
		$post['groups'][0] = $system;
		$post['phoneno']   = trim(str_replace("\n", "", $post['phoneno']));
		$post['city']      = trim(str_replace("\n", "", $post['city']));

		$post['activation'] = "";
		$post['block'] = 0;

		$user->bind($post);
		if (!$user->save())
		{
			//IJReq::setResponseCode(500);

			return 500;
		}

		$aclval = $user->id;

		if (!$aclval)
		{
			//IJReq::setResponseCode(500);

			return 500;
		}

		return 1;
	}
}

