<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelUserPackageInviteDetail extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{

	}

	public function getInvitationDetail()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$packageInviteID = $input->get('package_invite_id',0,'int');

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('pi.*')
			->from($db->quoteName('#__bcted_package_invite','pi'))
			->where($db->quoteName('pi.package_invite_id') . ' = ' . $db->quote($packageInviteID));

		$query->select('pp.user_id AS package_purchased_by,pp.venue_id,pp.company_id,pp.package_datetime,pp.package_time,pp.package_number_of_guest,pp.male_count,pp.female_count,pp.package_location,pp.package_additional_info,pp.status AS package_booking_status,pp.user_status,pp.total_price')
			->join('LEFT','#__bcted_package_purchased AS pp ON pp.package_purchase_id=pi.package_purchase_id');

		$query->select('pckg.package_name,pckg.package_image,pckg.package_details,pckg.package_price,pckg.currency_code AS package_currency_code,pckg.currency_sign AS package_currency_sign,pckg.package_date')
			->join('LEFT','#__bcted_package AS pckg ON pckg.package_id=pi.package_id');

		$query->select('bs.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=pi.status');

		/*$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=pp.user_status');*/

		$query->select('v.userid AS venue_owner,v.venue_name,v.city,v.country,v.latitude,v.longitude,v.currency_code,v.currency_sign,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=pp.venue_id');

		$query->select('c.userid AS company_owner,c.company_name,c.city,c.country,c.latitude,c.longitude,c.currency_code,c.currency_sign,c.company_address,c.company_about,c.company_image')
			->join('LEFT','#__bcted_company AS c ON c.company_id=pp.company_id');

		$query->select('u.name,u.username')
			->join('LEFT','#__users AS u ON u.id=pp.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=pp.user_id');

		$query->order($db->quoteName('pp.time_stamp') . ' DESC');


		// Set the query and load the result.
		$db->setQuery($query);

		$bookings = $db->loadObject();

		/*echo "<pre>";
		print_r($bookings);
		echo "</pre>";*/
		/*exit;*/

		if(!$bookings)
		{
			return array();
		}
		return $bookings;
	}
}
