<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelUserPackageInvitations extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	public function getPackageInvitations()
	{
		$db    = JFactory::getDbo();
		$user = JFactory::getUser();
		$query = $db->getQuery(true);

		$query->select('pi.*')
			->from($db->quoteName('#__bcted_package_invite','pi'))
			->where($db->quoteName('pi.invited_user_id') . ' = ' . $db->quote($user->id));

		$query->select('pp.user_id AS pakcage_purchased_user_id,pp.venue_id,pp.package_datetime,pp.package_time,pp.package_number_of_guest,pp.male_count,pp.female_count,pp.package_location,pp.package_additional_info,pp.total_price,pp.deposit_amount,pp.amount_payable as pp_amount_payable')
			->join('LEFT','#__bcted_package_purchased AS pp ON pp.package_purchase_id=pi.package_purchase_id');

		$query->select('pckg.package_name,pckg.package_image,pckg.package_details,pckg.package_price,pckg.currency_code AS package_currency_code,pckg.currency_sign AS package_currency_sign,pckg.package_date')
			->join('LEFT','#__bcted_package AS pckg ON pckg.package_id=pi.package_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=pi.status');

		/*$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=pp.user_status');*/

		$query->select('v.userid AS venue_owner,v.venue_name,v.city,v.country,v.latitude,v.longitude,v.currency_code,v.currency_sign,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=pp.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=pi.invited_user_id');

		$query->select('u2.name AS pakcage_purchased_user_name')
			->join('LEFT','#__users AS u2 ON u2.id=pp.user_id');

		$query->select('bu.last_name AS pakcage_purchased_user_last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=pp.user_id');

		$query->order($db->quoteName('pi.created') . ' DESC');

		/*echo $query->dump();
		exit;*/

		$db->setQuery($query);
		$packages = $db->loadObjectList();



		return $packages;
	}

	/*public function getTablesInvitations()
	{
		jdbo
	}*/
}
