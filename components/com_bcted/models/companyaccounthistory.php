<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelCompanyAccountHistory extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{
		$user = JFactory::getUser();
		$elementType = BctedHelper::getUserGroupType($user->id);
		$elementDetail = BctedHelper::getUserElementID($user->id);

		if($elementType != 'ServiceProvider')
		{
			return array();
		}

		if(!$elementDetail->company_id)
		{
			return array();
		}

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('sb.*')
			->from($db->quoteName('#__bcted_service_booking','sb'))
			->where($db->quoteName('sb.company_id') . ' = ' . $db->quote($elementDetail->company_id))
			->where($db->quoteName('sb.status') . ' = ' . $db->quote(5))
			->order($db->quoteName('sb.service_booking_id') . ' DESC');

		$query->select('up.last_name,up.phoneno,up.avatar,up.about')
			->join('LEFT','#__bcted_user_profile AS up ON up.userid=sb.user_id');

		$query->select('cs.service_price')
			->join('LEFT','#__bcted_company_services AS cs ON cs.service_id=sb.service_id ');

		return $query;
	}

	/*protected function populateState($ordering = null, $direction = null)
	{
		echo "populateState : ".$ordering . " || " . $direction;
		parent::populateState($ordering, $direction);
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	/*public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function getClubAccountHistory()
	{
		$user = JFactory::getUser();
		$elementType = BctedHelper::getUserGroupType($user->id);
		$elementDetail = BctedHelper::getUserElementID($user->id);

		if($elementType != 'Club')
		{
			return array();
		}

		if(!$elementDetail->venue_id)
		{
			return array();
		}

		/*echo "<pre>";
		print_r($elementDetail);
		echo "</pre>";
		exit;*/



		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'))
			->where($db->quoteName('vb.venue_id') . ' = ' . $db->quote($elementDetail->venue_id))
			->order($db->quoteName('vb.venue_booking_id') . ' DESC');

		$query->select('up.last_name,up.phoneno,up.avatar,up.about')
			->join('LEFT','#__bcted_user_profile AS up ON up.userid=vb.user_id');

		$query->select('vt.venue_table_price')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		/*$query->select('')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');*/

			//venue_table_id

		// Set the query and load the result.
		$db->setQuery($query);

		$accountHistories = $db->loadObjectList();


		return $accountHistories;


	}

}
