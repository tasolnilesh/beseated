<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Information Model
 *
 * @since  0.0.1
 */
class BctedModelCompanyServices extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	public function getCompanyServices()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		$serachType = '';
		$companyID = $input->get('company_id',0,'int');
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Set the query and load the result.
		$query->select('a.*')
			->from($db->quoteName('#__bcted_company_services','a'))
			->where($db->quoteName('a.service_active') . ' =  ' .  $db->quote(1))
			->where($db->quoteName('a.company_id') . ' =  ' .  $db->quote($companyID));

		$query->select('c.company_name,c.company_image,c.company_address,c.city,c.country,c.latitude,c.longitude,c.currency_code,c.currency_sign,c.company_about,c.company_signs,c.attire,c.restaurant,c.smoking,c.rating,c.drink')
			->join('LEFT','#__bcted_company AS c ON c.company_id=a.company_id');

		/*echo $query->dump();
		exit;*/

		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}


}
