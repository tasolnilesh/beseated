<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Ratings Model
 *
 * @since  0.0.1
 */
class BctedModelCompanyRatings extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	public function getCompanyRatings()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		/*echo "<pre>";
		print_r($input);
		echo "</pre>";
		exit;*/
		/*[view] => search
		[country] => Australia
		[club] => club
		*/
		$serachType = '';
		$venueID = $input->get('company_id',0,'int');



		$db    = JFactory::getDbo();
		$queryLU = $db->getQuery(true);
		$queryLU->select('id')
			->from($db->quoteName('#__users'))
			->where($db->quoteName('block') . ' = ' . $db->quote('0'));

		$db->setQuery($queryLU);
		$liveUsers = $db->loadColumn();

		$query = $db->getQuery(true);
		$query->select('a.*')
			->from($db->quoteName('#__bcted_ratings','a'))
			->where($db->quoteName('a.rating_type') . ' =  ' .  $db->quote('service'))
			->where($db->quoteName('a.rated_id') . ' =  ' .  $db->quote($venueID))
			->where($db->quoteName('a.user_id') . ' IN ('.implode(",", $liveUsers).')');

		$query->select('b.name,b.username')
			->join('LEFT','#__users AS b ON b.id=a.user_id');

		/*echo $query->dump();
		exit;*/

		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}


}
