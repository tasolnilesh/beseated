<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Beseated Club Request Model
 *
 * @since  0.0.1
 */
class BctedModelClubRequests extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Filter: like / search
		$search = $this->getState('filter.search');

		// Create the base select statement.
		$query->select('a.*,b.media,b.media_type,c.lib_name')
			->from($db->quoteName('#__heartdart_message','a'));

		$query->join('LEFT','#__heartdart_message_media as b on a.media_id = b.media_id');
		$query->join('LEFT','#__heartdart_library as c on a.library_id = c.library_id');
		$query->order($db->quoteName('a.msg_id') . ' DESC');

		if (!empty($search))
		{
			$like = $db->quote('%' . $search . '%');
			//$query->where('a.caption LIKE ' . $like);
		}

		// Filter by published state
		$published = $this->getState('filter.published');
		//$fullordering = $this->getState('list.fullordering');
		//echo "<br>fullordering : " . $fullordering;
		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.published IN (0, 1))');
		}

		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering', 'a.msg_id');
		$orderDirn 	= $this->state->get('list.direction', 'asc');

		$query->group('a.msg_id');

		//echo $orderCol . " || " . $orderDirn;
		$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));

		//$query->order($db->quoteName('a.post_id') . ' DESC');

		//echo $query->dump();exit;

		return $query;
	}

	/*protected function populateState($ordering = null, $direction = null)
	{
		echo "populateState : ".$ordering . " || " . $direction;
		parent::populateState($ordering, $direction);
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	/*public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function getClubBookings()
	{
		$user = JFactory::getUser();
		$elementType = BctedHelper::getUserGroupType($user->id);
		$elementDetail = BctedHelper::getUserElementID($user->id);

		if($elementType != 'Club')
		{
			return array();
		}

		if(!$elementDetail->venue_id)
		{
			return array();
		}

		/*echo "<pre>";
		print_r($elementDetail);
		echo "</pre>";
		exit;*/



		$db    = JFactory::getDbo();
		$queryV = $db->getQuery(true);

		$queryV->select('venue_booking_id')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($elementDetail->venue_id))
			->where($db->quoteName('deleted_by_venue') . ' = ' . $db->quote('0'))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'));

		$statusID = array();
		$statusID[] = BctedHelper::getStatusIDFromStatusName('Booked');
		//$statusID[] = BctedHelper::getStatusIDFromStatusName('Cancelled');

		$queryV->where($db->quoteName('status') . 'NOT IN (' . implode(",", $statusID) . ')');

		$queryV->order($db->quoteName('venue_booking_datetime') . ' ASC');

		$db->setQuery($queryV);
		$bookingIDs = $db->loadColumn();

		/*echo "<pre>";
		print_r($bookingIDs);
		echo "</pre>";
		exit;*/

		$query = $db->getQuery(true);

		$query->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'));

		if(!$bookingIDs)
		{
			return array();
		}
		else if(is_array($bookingIDs))
		{
			$bookingIDStr = implode(",", $bookingIDs);
			$query->where($db->quoteName('vb.venue_booking_id') . ' IN (' . $bookingIDStr . ')');
		}
		else
		{
			$query->where($db->quoteName('vb.venue_booking_id') . ' = ' . $db->quote($bookingIDs));
		}

		// Create the base select statement.

		$query->select('vt.premium_table_id,vt.venue_table_name,vt.custom_table_name,vt.venue_table_image,vt.venue_table_price,vt.venue_table_capacity,venue_table_description')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=vb.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=vb.user_status');

		$query->select('v.venue_name,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=vb.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=vb.user_id');

		$query->order($db->quoteName('vb.venue_booking_datetime') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);

		$bookings = $db->loadObjectList();

		/*echo "<pre>";
		print_r($bookings);
		echo "</pre>";
		exit;*/


		if(!$bookings)
		{
			return array();
		}
		return $bookings;
	}

	public function getClubPackageRequests()
	{
		$user = JFactory::getUser();
		$elementType = BctedHelper::getUserGroupType($user->id);
		$elementDetail = BctedHelper::getUserElementID($user->id);

		if($elementType != 'Club')
		{
			return array();
		}

		if(!$elementDetail->venue_id)
		{
			return array();
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$statusID = BctedHelper::getStatusIDFromStatusName('Booked');

		// Create the base select statement.
		$query->select('pp.*')
			->from($db->quoteName('#__bcted_package_purchased','pp'))
			->where($db->quoteName('pp.venue_id') . ' = ' . $db->quote($elementDetail->venue_id))
			->where($db->quoteName('pp.status') . ' <> ' . $db->quote($statusID))
			->order($db->quoteName('pp.package_datetime') . ' ASC');

		$query->select('p.package_name,p.package_image,p.package_details,p.package_price,p.currency_code,p.currency_sign,p.package_date')
			->join('LEFT','#__bcted_package AS p ON p.package_id=pp.package_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=pp.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=pp.user_status');


		$query->select('v.venue_name')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=pp.venue_id');

		$query->select('u.name,u.username')
			->join('LEFT','#__users AS u ON u.id=pp.user_id');

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;

	}

	public function summaryForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('count(status) as total_count,status')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
			->group($db->quoteName('status'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	public function getRevenueForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('sum(amount_payable) as revenue')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadResult();

		return $result;
	}


}
