<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelClubBookings extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Filter: like / search
		$search = $this->getState('filter.search');

		// Create the base select statement.
		$query->select('a.*,b.media,b.media_type,c.lib_name')
			->from($db->quoteName('#__heartdart_message','a'));

		$query->join('LEFT','#__heartdart_message_media as b on a.media_id = b.media_id');
		$query->join('LEFT','#__heartdart_library as c on a.library_id = c.library_id');
		$query->order($db->quoteName('a.msg_id') . ' DESC');

		if (!empty($search))
		{
			$like = $db->quote('%' . $search . '%');
			//$query->where('a.caption LIKE ' . $like);
		}

		// Filter by published state
		$published = $this->getState('filter.published');
		//$fullordering = $this->getState('list.fullordering');
		//echo "<br>fullordering : " . $fullordering;
		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.published IN (0, 1))');
		}

		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering', 'a.msg_id');
		$orderDirn 	= $this->state->get('list.direction', 'asc');

		$query->group('a.msg_id');

		//echo $orderCol . " || " . $orderDirn;
		$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));

		//$query->order($db->quoteName('a.post_id') . ' DESC');

		//echo $query->dump();exit;

		return $query;
	}

	/*protected function populateState($ordering = null, $direction = null)
	{
		echo "populateState : ".$ordering . " || " . $direction;
		parent::populateState($ordering, $direction);
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	/*public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function getClubBookings()
	{
		$user = JFactory::getUser();
		$elementType = BctedHelper::getUserGroupType($user->id);
		$elementDetail = BctedHelper::getUserElementID($user->id);

		if($elementType != 'Club')
		{
			return array();
		}

		if(!$elementDetail->venue_id)
		{
			return array();
		}

		/*echo "<pre>";
		print_r($elementDetail);
		echo "</pre>";
		exit;*/



		$db    = JFactory::getDbo();
		$queryV = $db->getQuery(true);

		$queryV->select('venue_booking_id')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($elementDetail->venue_id))
			->where($db->quoteName('deleted_by_venue') . ' = ' . $db->quote('0'));

		$statusID = array();
		$statusID[] = BctedHelper::getStatusIDFromStatusName('Booked');
		$statusID[] = BctedHelper::getStatusIDFromStatusName('Cancelled');

		$queryV->where($db->quoteName('status') . ' IN ('. implode(",", $statusID) .')');

		$queryV->order($db->quoteName('venue_booking_datetime') . ' ASC');

		$db->setQuery($queryV);
		$bookingIDs = $db->loadColumn();

		/*echo "<pre>";
		print_r($bookingIDs);
		echo "</pre>";
		exit;*/

		$query = $db->getQuery(true);

		$query->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'));

		if(!$bookingIDs)
		{
			return array();
		}
		else if(is_array($bookingIDs))
		{
			$bookingIDStr = implode(",", $bookingIDs);
			$query->where($db->quoteName('vb.venue_booking_id') . ' IN (' . $bookingIDStr . ')');
		}
		else
		{
			$query->where($db->quoteName('vb.venue_booking_id') . ' = ' . $db->quote($bookingIDs));
		}

		// Create the base select statement.

		$query->select('vt.premium_table_id,vt.venue_table_name,vt.custom_table_name,vt.venue_table_image,vt.venue_table_price,vt.venue_table_capacity,venue_table_description')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=vb.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=vb.user_status');

		$query->select('v.venue_name,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=vb.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=vb.user_id');

		$query->order($db->quoteName('vb.venue_booking_datetime') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);

		$bookings = $db->loadObjectList();

		/*echo "<pre>";
		print_r($bookings);
		echo "</pre>";
		exit;*/


		if(!$bookings)
		{
			return array();
		}
		return $bookings;


	}

	public function summaryForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('count(status) as total_count,status')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
			->group($db->quoteName('status'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	public function getRevenueForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('sum(amount_payable) as revenue')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadResult();

		return $result;
	}

	public function deleteBooking($bookingID,$userType)
	{
		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable',array());
		$user = JFactory::getUser();

		$tblVenuebooking->load($bookingID);

		if(!$tblVenuebooking->venue_booking_id)
		{
			//COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE
			return 400;
		}

		$status = array();
		$status[] = BctedHelper::getStatusIDFromStatusName('Booked');
		$status[] = BctedHelper::getStatusIDFromStatusName('Cancelled');
		$status[] = BctedHelper::getStatusIDFromStatusName('Decline');
		$status[] = BctedHelper::getStatusIDFromStatusName('Waiting List');
		$status[] = BctedHelper::getStatusIDFromStatusName('Unavailable');

		//echo "<pre/>";print_r($status);exit;

		//if($status != $tblVenuebooking->status)
		if(!in_array($tblVenuebooking->status, $status))
		{
			//COM_IJOOMERADV_VENUE_TABLE_BOOKING_STATUS_NOT_BOOKING

			return 400;
		}

		$tblVenue = JTable::getInstance('Venue', 'BctedTable',array());
		$tblVenue->load($tblVenuebooking->venue_id);

		/*if($tblVenue->userid != $user->id)
		{
			//COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED

			return 706;
		}*/

		//$tblVenuebooking->is_deleted = 1;
		if($userType == 'venue')
		{
			$tblVenuebooking->deleted_by_venue = 1;
		}
		else
		{
			$tblVenuebooking->deleted_by_user = 1;
		}

		if(BctedHelper::getStatusIDFromStatusName('Waiting List')==$tblVenuebooking->status)
		{
			$tblVenuebooking->deleted_by_user = 1;
			$tblVenuebooking->deleted_by_venue = 1;
		}


		if(!$tblVenuebooking->store())
		{
			return 500;
		}

		return 200;

	}

	public function sendnoshowmessage($bookingID,$userID)
	{
		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');
		$tblVenuebooking->load($bookingID);

		if(!$tblVenuebooking->venue_booking_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_BCTED_INVALID_BOOKING_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenuebooking->is_noshow = 1;
		$tblVenuebooking->store();

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_payment_status'))
			->where($db->quoteName('booked_element_id') . ' = ' . $db->quote($tblVenuebooking->venue_booking_id))
			->where($db->quoteName('booked_element_type') . ' = ' . $db->quote('venue'))
			->where($db->quoteName('paid_status') . ' = ' . $db->quote('1'));

		// Set the query and load the result.
		$db->setQuery($query);

		$paymentStatus = $db->loadObject();
		if($paymentStatus)
		{
			$queryLP = $db->getQuery(true);
			$queryLP->select('*')
				->from($db->quoteName('#__bcted_loyalty_point'))
				->where($db->quoteName('user_id') . ' = ' . $db->quote($tblVenuebooking->user_id))
				->where($db->quoteName('point_app') . ' = ' . $db->quote('purchase.venue'))
				->where($db->quoteName('cid') . ' = ' . $db->quote($paymentStatus->payment_id));

			// Set the query and load the result.
			$db->setQuery($queryLP);

			$loyaltyPointDetail = $db->loadObject();

			if($loyaltyPointDetail)
			{
				$query = $db->getQuery(true);

				// Create the base insert statement.
				$query->insert($db->quoteName('#__bcted_loyalty_point'))
					->columns(
						array(
							$db->quoteName('user_id'),
							$db->quoteName('earn_point'),
							$db->quoteName('point_app'),
							$db->quoteName('cid'),
							$db->quoteName('is_valid'),
							$db->quoteName('created'),
							$db->quoteName('time_stamp')
						)
					)
					->values(
						$db->quote($tblVenuebooking->user_id) . ', ' .
						$db->quote(($loyaltyPointDetail->earn_point * (-1))) . ', ' .
						$db->quote('venue.noshow') . ', ' .
						$db->quote($loyaltyPointDetail->cid) . ', ' .
						$db->quote(1) . ', ' .
						$db->quote(date('Y-m-d H:i:s')) . ', ' .
						$db->quote(time())
					);

				// Set the query and execute the insert.
				$db->setQuery($query);

				$db->execute();

			}
		}

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($tblVenuebooking->venue_id);

		$tblTable = JTable::getInstance('Table', 'BctedTable');
		$tblTable->load($tblVenuebooking->venue_table_id);

		$bookingUserID = $tblVenuebooking->user_id;
		$bookingUserDetail = JFactory::getUser($bookingUserID);
		if(!$bookingUserDetail->id)
		{
			return 500;
		}

		// Initialise variables.
		$app     = JFactory::getApplication();
		$config  = JFactory::getConfig();

		$site    = $config->get('sitename');
		$from    = $config->get('mailfrom');
		$sender  = $config->get('fromname');
		$email   = $bookingUserDetail->email; //$app->input->get('mailto');
		$subject =  JText::_('COM_BCTED_VENUE_NOSHOW_EMAIL_SUBJECT');//$app->input->get('subject');

		// Build the message to send.
		$msg     = JText::_('COM_BCTED_VENUE_NOSHOW_EMAIL_BODY');
		$body    = sprintf($msg, $site, $sender, $from);

		// Clean the email data.
		$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body);

		/*************** Send no Show email to site administrator ********/

		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$appHelper            = new bctedAppHelper;


		$email   = $appHelper->getAdministratorUsersEmail();
		$subject =  JText::_('COM_BESEATED_VENUE_NOSHOW_EMAIL_TO_ADMINISTRATOR_SUBJECT');//$app->input->get('subject');

		// Build the message to send.
		$msg     = JText::_('COM_BESEATED_VENUE_NOSHOW_EMAIL_TO_ADMINISTRATOR_BODY');
		$body    = sprintf($msg, $site, $sender, $from);

		// Clean the email data.
		$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body,true);

		/*************** End no show email to site administrator ************/

		$message     = JText::_('COM_BCTED_VENUE_NOSHOW_EMAIL_BODY');

		$connectionID = BctedHelper::sendMessage($tblVenue->venue_id,0,0,$tblTable->venue_table_id,$bookingUserID,$message,array(),'noshow');

		$pushID = $tblVenue->venue_name.';'.$connectionID.';venue';
		$jsonarray['pushNotificationData']['id']         = $pushID;
		$jsonarray['pushNotificationData']['to']         = $bookingUserID;
		$jsonarray['pushNotificationData']['message']    = $message;
		$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NOSHOW');
		$jsonarray['pushNotificationData']['configtype'] = '';

		BctedHelper::sendPushNotification($jsonarray);

		return 200;
	}
}
