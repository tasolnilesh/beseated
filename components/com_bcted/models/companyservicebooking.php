<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelCompanyServiceBooking extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'ServiceBooking', $prefix = 'BctedTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	public function bookCompanyService($data = array())
	{
		$tblServiceBooking = $this->getTable();

		$tblServiceBooking->load(0);

		$tblServiceBooking->bind($data);

		if(!$tblServiceBooking->store())
		{
			return 0;
		}

		$tblService = $this->getTable('Service');
		$tblCompany = $this->getTable('Company');
		$tblCompany->load($tblServiceBooking->company_id);
		$tblService->load($tblServiceBooking->service_id);

		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$appHelper            = new bctedAppHelper;

		$bookingData = $appHelper->getBookingDetailForServices($tblServiceBooking->service_booking_id,'Company');

		/*if(!empty($tblServiceBooking->service_booking_additional_info))
		{
			BctedHelper::sendMessage(0,$tblCompany->company_id,$tblService->service_id,0,$tblCompany->userid,$tblServiceBooking->service_booking_additional_info,$bookingData,$messageType='newservicebooking');
		}*/

		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "company";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$db = JFactory::getDbo();

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');

		/*echo "<pre>";
		print_r($obj);
		echo "</pre>";
		exit;*/
		if($obj->id)
		{
			$userProfile    = $appHelper->getUserProfile($tblCompany->userid);
			$receiveRequest = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveRequest = $params->settings->pushNotification->receiveRequest;
			}

			if($receiveRequest)
			{
				//$message = 'You have new Requst for '.$tblService->service_name;
				$message = JText::sprintf('PUSHNOTIFICATION_TYPE_NEWREQUESTRECEIVED_MESSAGE',$tblService->service_name);
				$jsonarray['pushNotificationData']['id']         = $obj->id;
				$jsonarray['pushNotificationData']['to']         = $tblCompany->userid;
				$jsonarray['pushNotificationData']['message']    = $message;
				$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWREQUESTRECEIVED');
				$jsonarray['pushNotificationData']['configtype'] = '';

				/*echo "<pre>";
				print_r($jsonarray);
				echo "</pre>";

				PUSHNOTIFICATION_TYPE_NEWREQUESTRECEIVED
				exit;*/

				BctedHelper::sendPushNotification($jsonarray);
			}
		}

		return 1;
	}
}
