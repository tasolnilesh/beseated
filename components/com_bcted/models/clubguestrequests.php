<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelClubGuestRequests extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{

	}

	/*protected function populateState($ordering = null, $direction = null)
	{
		echo "populateState : ".$ordering . " || " . $direction;
		parent::populateState($ordering, $direction);
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	/*public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function getGuestLists()
	{
		$user = JFactory::getUser();
		$elementType = BctedHelper::getUserGroupType($user->id);
		$elementDetail = BctedHelper::getUserElementID($user->id);

		$venueID = $elementDetail->venue_id;

		$db    = JFactory::getDbo();

		$queryLiveUsers = $db->getQuery(true);

		$queryLiveUsers->select('id')
			->from($db->quoteName('#__users'))
			->where($db->quoteName('block') . ' = ' . $db->quote('0'));

		// Set the query and load the result.
		$db->setQuery($queryLiveUsers);

		$liveUser = $db->loadColumn();

		$queryToday = $db->getQuery(true);

		$today = date('Y-m-d');

		// Create the base select statement.
		$queryToday->select('request_date,count(request_date) as guestListCount,sum(number_of_guest) as totalGuest')
			->from($db->quoteName('#__bcted_venue_guest_list_request'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('request_date') . ' = ' . $db->quote($today))
			->where($db->quoteName('user_status') . ' = ' . $db->quote(5))
			->where($db->quoteName('status') . ' = ' . $db->quote(5))
			->group($db->quoteName('request_date'))
			->order($db->quoteName('time_stamp') . ' ASC');

		if(count($liveUser)!=0)
		{
			$liveUserStr = implode(",", $liveUser);
			$queryToday->where($db->quoteName('user_id') . ' IN (' . $liveUserStr . ')');
		}



		// Set the queryToday and load the result.
		$db->setQuery($queryToday);

		$todaysGuestLists = $db->loadObjectList();

		$resultTodaysList = array();

		foreach ($todaysGuestLists as $key => $todaysGuest)
		{
			$tempData = array();
			$tempData['date'] = date('d-m-Y',strtotime($todaysGuest->request_date));
			//$tempData['listCount'] = $todaysGuest->guestListCount;
			$tempData['listCount'] = $todaysGuest->totalGuest;

			$resultTodaysList[] = $tempData;
		}

		$queryFuture = $db->getQuery(true);

		$today = date('Y-m-d');

		// Create the base select statement.
		$queryFuture->select('request_date,count(request_date) as guestListCount,sum(number_of_guest) as totalGuest')
			->from($db->quoteName('#__bcted_venue_guest_list_request'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('user_status') . ' = ' . $db->quote(5))
			->where($db->quoteName('status') . ' = ' . $db->quote(5))
			->where($db->quoteName('request_date') . ' > ' . $db->quote($today))
			->group($db->quoteName('request_date'))
			->order($db->quoteName('request_date') . ' DESC');

		if(count($liveUser)!=0)
		{
			$liveUserStr = implode(",", $liveUser);
			$queryFuture->where($db->quoteName('user_id') . ' IN (' . $liveUserStr . ')');
		}

		/*echo $queryToday->dump();
		exit;*/

		// Set the queryToday and load the result.
		$db->setQuery($queryFuture);

		$todaysGuestLists = $db->loadObjectList();

		$resultFuturesList = array();

		foreach ($todaysGuestLists as $key => $todaysGuest)
		{
			$tempData = array();
			$tempData['date'] = date('d-m-Y',strtotime($todaysGuest->request_date));
			//$tempData['listCount'] = $todaysGuest->guestListCount;
			$tempData['listCount'] = $todaysGuest->totalGuest;

			$resultFuturesList[] = $tempData;
		}

		$queryPast = $db->getQuery(true);

		$today = date('Y-m-d');

		// Create the base select statement.
		$queryPast->select('request_date,count(request_date) as guestListCount,sum(number_of_guest) as totalGuest')
			->from($db->quoteName('#__bcted_venue_guest_list_request'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('user_status') . ' = ' . $db->quote(5))
			->where($db->quoteName('status') . ' = ' . $db->quote(5))
			->where($db->quoteName('request_date') . ' < ' . $db->quote($today))
			->group($db->quoteName('request_date'))
			->order($db->quoteName('request_date') . ' DESC');

		if(count($liveUser)!=0)
		{
			$liveUserStr = implode(",", $liveUser);
			$queryPast->where($db->quoteName('user_id') . ' IN (' . $liveUserStr . ')');
		}

		/*echo $queryToday->dump();
		exit;*/

		// Set the queryToday and load the result.
		$db->setQuery($queryPast);

		$todaysGuestLists = $db->loadObjectList();

		$resultPastsList = array();

		foreach ($todaysGuestLists as $key => $todaysGuest)
		{
			$tempData = array();
			$tempData['date'] = date('d-m-Y',strtotime($todaysGuest->request_date));
			//$tempData['listCount'] = $todaysGuest->guestListCount;
			$tempData['listCount'] = $todaysGuest->totalGuest;

			$resultPastsList[] = $tempData;
		}

		$bookings = array();

		$bookings['requestToday']  = $resultTodaysList;
		$bookings['requestPast']   = $resultPastsList;
		$bookings['requestFuture'] = $resultFuturesList;

		return $bookings;
	}

	public function getClubPackageRequests()
	{
		$user = JFactory::getUser();
		$elementType = BctedHelper::getUserGroupType($user->id);
		$elementDetail = BctedHelper::getUserElementID($user->id);

		if($elementType != 'Club')
		{
			return array();
		}

		if(!$elementDetail->venue_id)
		{
			return array();
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$statusID = BctedHelper::getStatusIDFromStatusName('Booked');

		// Create the base select statement.
		$query->select('pp.*')
			->from($db->quoteName('#__bcted_package_purchased','pp'))
			->where($db->quoteName('pp.venue_id') . ' = ' . $db->quote($elementDetail->venue_id))
			->where($db->quoteName('pp.status') . ' <> ' . $db->quote($statusID))
			->order($db->quoteName('pp.package_datetime') . ' ASC');

		$query->select('p.package_name,p.package_image,p.package_details,p.package_price,p.currency_code,p.currency_sign,p.package_date')
			->join('LEFT','#__bcted_package AS p ON p.package_id=pp.package_id');

		$query->select('v.venue_name')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=pp.venue_id');

		$query->select('u.name,u.username')
			->join('LEFT','#__users AS u ON u.id=pp.user_id');

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;

	}

	public function summaryForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('count(status) as total_count,status')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
			->group($db->quoteName('status'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	public function getRevenueForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('sum(amount_payable) as revenue')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadResult();

		return $result;
	}


}
