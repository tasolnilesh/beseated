<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelJetServiceBooking extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'JetServiceBooking', $prefix = 'BctedTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	public function bookJetService($data = array())
	{
		$tblJetServiceBooking = $this->getTable();

		$tblJetServiceBooking->load(0);

		$tblJetServiceBooking->bind($data);

		if(!$tblJetServiceBooking->store())
		{
			return 0;
		}

		$tblService = $this->getTable('Service');
		$tblCompany = $this->getTable('Company');
		$tblCompany->load($tblJetServiceBooking->company_id);

		/*if(!empty($tblJetServiceBooking->additional_info))
		{

			$bookingData = array();
			BctedHelper::sendMessage(0,$tblCompany->company_id,0,0,$tblCompany->userid,$tblJetServiceBooking->additional_info,$bookingData,'newbookingrequestforprivatejet');
		}*/



		return 1;
	}
}
