<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Ratings Model
 *
 * @since  0.0.1
 */
class BctedModelCompanyProfile extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	public function getProfileData()
	{
		$user = JFactory::getUser();
		$elementType = BctedHelper::getUserGroupType($user->id);
		$elementDetail = BctedHelper::getUserElementID($user->id);

		return $elementDetail;
	}

	public function saveCompanyProfile($data,$id)
	{

		$tblCompany = JTable::getInstance('Company','BctedTable',array());
		$tblCompany->load($id);

		if(isset($data['company_image']) && !empty($data['company_image']))
		{
			if(!empty($tblCompany->company_image))
			{
				if(file_exists(JPATH_SITE.'/'.$tblCompany->company_image))
				{
					unlink(JPATH_SITE.'/'.$tblCompany->company_image);
				}
			}
		}

		$tblCompany->bind($data);

		if($tblCompany->store())
		{
			$user              = new JUser;
			$user->load($tblCompany->userid);

			if($user->id)
			{
				$user->name = trim($tblCompany->company_name);
				$user->save();
			}

			return true;
		}

		return false;

	}

	public function is_booking_in_company($company_id)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_service_booking'))
			->where($db->quoteName('company_id') . ' = ' . $db->quote($company_id));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return count($result);
	}


}
