<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Club Information Model
 *
 * @since  0.0.1
 */
class BctedModelPackageInformation extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	public function getPackageDetail()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		$packageID = $input->get('package_id',0,'int');

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Set the query and load the result.
		$query->select('a.*')
			->from($db->quoteName('#__bcted_package','a'))
			->where($db->quoteName('a.package_active') . ' =  ' .  $db->quote(1))
			->where($db->quoteName('a.package_id') . ' =  ' .  $db->quote($packageID));

		$query->select('b.venue_name')
			->join('LEFT','#__bcted_venue AS b ON b.venue_id=a.venue_id');

		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}


}
