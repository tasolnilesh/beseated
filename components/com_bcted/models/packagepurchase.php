<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelPackagePurchase extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'PackagePurchased', $prefix = 'BctedTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	public function purchasePackage($data = array())
	{
		$tblPackagePurchased = $this->getTable();

		$tblPackage = $this->getTable('Package');
		$tblPackage->load($data['package_id']);

		$data['total_price'] = $tblPackage->package_price * $data['package_number_of_guest'];

		$tblPackagePurchased->load(0);

		$tblPackagePurchased->bind($data);

		if(!$tblPackagePurchased->store())
		{
			return 0;
		}

		$newBookingID = $tblPackagePurchased->package_purchase_id;

		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$appHelper            = new bctedAppHelper;
		/*echo $newBookingID;
		exit;*/



		$bookingData = $appHelper->getBookingDetailForPackage($newBookingID,'Club');

		if(!empty($tblPackagePurchased->package_additional_info))
		{
			$tblVenue = $this->getTable('Venue');
			$tblVenue->load($tblPackagePurchased->venue_id);
			BctedHelper::sendMessage($tblPackagePurchased->venue_id,0,0,0,$tblVenue->userid,$tblPackagePurchased->package_additional_info,$bookingData,$messageType='newpackagebookingrequest');
		}

		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "Package";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$db = JFactory::getDbo();

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{
			if($tblPackage->venue_id)
			{
				$tblVenue = $this->getTable('Venue');
				$tblVenue->load($tblPackagePurchased->venue_id);
				$userProfile    = $appHelper->getUserProfile($tblVenue->userid);
				$userIDForPush = $tblVenue->userid;
			}
			/*else if($tblPackage->company_id)
			{
				$tblCompany = $this->getTable('Company');
				$tblCompany->load($tblPackagePurchased->company_id);
				$userProfile    = $appHelper->getUserProfile($tblCompany->userid);
				$userIDForPush = $tblCompany->userid;
			}*/


			/*echo "<pre>";
			print_r($userProfile);
			echo "</pre>";
			exit;*/

			$receiveRequest = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveRequest = $params->settings->pushNotification->receiveRequest;
			}

			//$receiveRequest = 1;

			if($receiveRequest)
			{
				//$message = 'You have new Request for '. $tblPackage->package_name;
				$message = JText::sprintf('PUSHNOTIFICATION_TYPE_NEWPACKAGEBOOKINGREQUESTRECEIVED_MESSAGE',$tblPackage->package_name);

				$jsonarray['pushNotificationData']['id']         = $obj->id;
				$jsonarray['pushNotificationData']['to']         = $userIDForPush;
				$jsonarray['pushNotificationData']['message']    = $message;
				$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWPACKAGEBOOKINGREQUESTRECEIVED');
				$jsonarray['pushNotificationData']['configtype'] = '';

				BctedHelper::sendPushNotification($jsonarray);
			}

		}

		return 1;
	}
}
