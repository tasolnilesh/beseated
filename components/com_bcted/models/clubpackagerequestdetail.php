<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted ClubRequestDetail Model
 *
 * @since  0.0.1
 */
class BctedModelClubPackageRequestDetail extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{

	}

	public function getClubBooking()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$requestID = $input->get('request_id',0,'int');

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		//$statusID = BctedHelper::getStatusIDFromStatusName('Booked');

		$query->select('pp.*')
			->from($db->quoteName('#__bcted_package_purchased','pp'))
			->where($db->quoteName('pp.package_purchase_id') . ' = ' . $db->quote($requestID))
			->order($db->quoteName('pp.package_datetime') . ' ASC');

		$query->select('p.package_name,p.package_image,p.package_details,p.package_price,p.currency_code,p.currency_sign,p.package_date')
			->join('LEFT','#__bcted_package AS p ON p.package_id=pp.package_id');

		$query->select('v.venue_name')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=pp.venue_id');

		$query->select('u.name,u.username')
			->join('LEFT','#__users AS u ON u.id=pp.user_id');

		//echo $query->dump();

		// Set the query and load the result.
		$db->setQuery($query);

		$bookings = $db->loadObject();

		/*echo "<pre>";
		print_r($bookings);
		echo "</pre>";
		exit;*/


		if(!$bookings)
		{
			return array();
		}
		return $bookings;


	}

	public function summaryForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('count(status) as total_count,status')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
			->group($db->quoteName('status'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	public function getRevenueForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('sum(amount_payable) as revenue')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadResult();

		return $result;
	}

	public function changeRequestStatus($requestID,$status,$owner_message)
	{
		$tblPackagePurchased = JTable::getInstance('PackagePurchased','BctedTable',array());
		$tblPackagePurchased->load($requestID);

		$tblVenue = JTable::getInstance('Venue','BctedTable',array());
		$tblPackage = JTable::getInstance('Package','BctedTable',array());

		if(!$tblPackagePurchased->package_purchase_id)
		{
			return 0;
		}

		$tblVenue->load($tblPackagePurchased->venue_id);
		$tblPackage->load($tblPackagePurchased->package_id);

		if($status == 'cancel')
		{
			$tblPackagePurchased->status = BctedHelper::getStatusIDFromStatusName('Decline');
			$tblPackagePurchased->user_status = BctedHelper::getStatusIDFromStatusName('Unavailable');

			/*$message = $tblVenue->venue_name . ' has reject your request for '. $tblPackage->package_name;
			$messageType = 'PackageRequestRejected';*/

			$message = JText::sprintf('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTACCEPTED_MESSAGE',$tblVenue->venue_name,$tblPackage->package_name);
			$messageType = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTACCEPTED');

		}
		else if ($status == 'waiting')
		{
			$tblPackagePurchased->status = BctedHelper::getStatusIDFromStatusName('Waiting List');
			$tblPackagePurchased->user_status = BctedHelper::getStatusIDFromStatusName('Waiting List');
		}
		else if ($status == 'ok')
		{
			$tblPackagePurchased->status = BctedHelper::getStatusIDFromStatusName('Awaiting Payment');
			$tblPackagePurchased->user_status = BctedHelper::getStatusIDFromStatusName('Available');

			/*$message = $tblVenue->venue_name . ' has accept your request for '. $tblPackage->package_name;
			$messageType = 'PackageRequestAccepted';*/

			$message = JText::sprintf('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREJECTED_MESSAGE',$tblVenue->venue_name,$tblPackage->package_name);
			$messageType = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREJECTED');
		}

		$tblPackagePurchased->owner_message = $owner_message;



		/*echo "<pre>";
		print_r($bookingData);
		echo "</pre>";
		exit;*/

		//$bookingData = $this->helper->getBookingDetailForVenueTable($tblVenuebooking->venue_booking_id,'User');

		$returnVal = 0;

		//exit;

		if(!$tblPackagePurchased->store())
		{
			return 0;
		}

		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$appHelper            = new bctedAppHelper;

		$bookingData = $appHelper->getBookingDetailForPackage($tblPackagePurchased->package_purchase_id,'User');

		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "package";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$db = JFactory::getDbo();

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');



		$jsonarray['pushNotificationData']['id']         = $obj->id;
		$jsonarray['pushNotificationData']['to']         = $tblPackagePurchased->user_id;
		$jsonarray['pushNotificationData']['message']    = $message;
		$jsonarray['pushNotificationData']['type']       = $messageType;
		$jsonarray['pushNotificationData']['configtype'] = '';

		/*echo "<pre>";
		print_r($jsonarray);
		echo "</pre>";
		exit;*/

		BctedHelper::sendPushNotification($jsonarray);



		return 1;

	}


}
