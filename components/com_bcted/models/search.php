<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelSearch extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		/*echo "<pre>";
		print_r($input);
		echo "</pre>";*/
		/*[view] => search
		[country] => Australia
		[club] => club
inner_search
		[caption] => aaa
        [country] => London
        [is_smart] => 1
        [is_casual] => 0
        [is_food] => 0
        [is_drink] => 1
        [is_smoking] => 0
        [is_ratting] => 1
        [is_costly] => 2
        [near_me] => 2
        [club] => club
		exit;*/

		$serachType = '';
		$clubSearch = $input->get('club','','string');
		$serviceSearch = $input->get('service','','string');
		$countrySearch = $input->get('country','','string');
		$inner_search = $input->get('inner_search', 0, 'int');

		if($clubSearch == 'club' && $inner_search == 1)
		{
			$caption    = $input->get('caption','','string');
			$is_smart   = $input->get('is_smart', 0, 'int');
			$is_casual  = $input->get('is_casual', 0, 'int');
			$is_food    = $input->get('is_food', 0, 'int');
			$is_drink   = $input->get('is_drink', 0, 'int');
			$is_smoking = $input->get('is_smoking', 0, 'int');
			$near_me    = $input->get('near_me', 0, 'int');
			$is_ratting = $input->get('is_ratting', 0, 'int');
			$is_costly  = $input->get('is_costly', 0, 'int');

		}

		if($serviceSearch == 'service' && $inner_search == 1)
		{
			$caption    = $input->get('caption','','string');
			/*$is_smart   = $input->get('is_smart', 0, 'int');
			$is_casual  = $input->get('is_casual', 0, 'int');
			$is_food    = $input->get('is_food', 0, 'int');
			$is_drink   = $input->get('is_drink', 0, 'int');
			$is_smoking = $input->get('is_smoking', 0, 'int');
			$near_me    = $input->get('near_me', 0, 'int');
			$is_ratting = $input->get('is_ratting', 0, 'int');
			$is_costly  = $input->get('is_costly', 0, 'int');*/

		}


		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		if($clubSearch == 'club')
		{
			$query->select('a.*')
				->from($db->quoteName('#__bcted_venue','a'))
				->where($db->quoteName('a.venue_active') . ' =  ' .  $db->quote(1));
			if($inner_search ==1)
			{
				if(!empty($caption))
				{
					$query->where($db->quoteName('a.venue_name') . ' LIKE  ' .  $db->quote('%'.$caption.'%'));
				}

				if($is_drink)
				{
					$query->where($db->quoteName('a.is_drink') . ' = ' . $db->quote(1));
				}

				if($is_food)
				{
					$query->where($db->quoteName('a.is_food') . ' = ' . $db->quote(1));
				}

				if($is_smart)
				{
					$query->where($db->quoteName('a.is_smart') . ' = ' . $db->quote(1));
				}
				if($is_casual)
				{
					$query->where($db->quoteName('a.is_casual') . ' = ' . $db->quote(1));
				}

				if($is_smoking)
				{
					$query->where($db->quoteName('a.is_smoking') . ' = ' . $db->quote($is_smoking));
				}

				if($is_ratting)
				{
					$query->where($db->quoteName('a.venue_rating') . ' >= ' . $db->quote($is_ratting));
				}

				if($is_costly)
				{
					$query->where($db->quoteName('a.venue_signs') . ' >= ' . $db->quote($is_costly));
				}
			}

				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'))
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'));
		}
		else if($serviceSearch == 'service')
		{
			$query->select('a.*')
				->from($db->quoteName('#__bcted_company','a'))
				->where($db->quoteName('a.company_active') . ' =  ' .  $db->quote(1));
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'))
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'));

			if($inner_search ==1)
			{
				if(!empty($caption))
				{
					$query->where($db->quoteName('a.company_name') . ' LIKE  ' .  $db->quote('%'.$caption.'%'));
				}
			}
		}
		else
		{
			$query->select('a.*')
				->from($db->quoteName('#__bcted_venue','a'))
				->where($db->quoteName('a.venue_active') . ' =  ' .  $db->quote(1));
		}

		if(empty($countrySearch))
		{
			$app->input->cookie->set('search_in_city', '');
			$search_in_city = '';
		}

		$oldCity = $app->input->cookie->get('search_in_city', '');

		if(!empty($countrySearch))
		{
			$app->input->cookie->set('search_in_city', $countrySearch);
			$search_in_city = $app->input->cookie->get('search_in_city', '');
		}
		else if (empty($countrySearch) && !empty($oldCity))
		{
			$search_in_city = $app->input->cookie->get('search_in_city', '');
		}

		if(!empty($search_in_city))
		{
			$query->where($db->quoteName('a.city') . ' LIKE  ' .  $db->quote('%'.$search_in_city.'%'));
		}

		$query->order($db->quoteName('a.time_stamp') . ' DESC');

		//echo $query->dump();
		//exit;*/

		return $query;
	}


}
