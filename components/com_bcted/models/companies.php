<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Companies Model
 *
 * @since  0.0.1
 */
class BctedModelCompanies extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		/*echo "<pre>";
		print_r($input);
		echo "</pre>";
		[view] => search
		[country] => Australia
		[club] => club
		exit;*/
		$serachType = '';
		$clubSearch = $input->get('club','','string');
		$serviceSearch = $input->get('service','service','string');
		$countrySearch = $input->get('country','','string');


		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		if($clubSearch == 'club')
		{
			$queryVT = $db->getQuery(true);
			$queryVT->select('venue_id')
				->from($db->quoteName('#__bcted_venue_table'))
				->where($db->quoteName('venue_table_active') . ' = ' . $db->quote('1'));

			$db->setQuery($queryVT);
			$venuesHasTable = $db->loadColumn();

			$venuesHasTable = array_unique($venuesHasTable);

			$query->select('a.*')
				->from($db->quoteName('#__bcted_venue','a'))
				->where($db->quoteName('a.venue_active') . ' =  ' .  $db->quote(1))
				->where($db->quoteName('a.venue_id') . ' IN (' . implode(",", $venuesHasTable) .')');
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'))
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'));

				$query->order($db->quoteName('a.venue_name') . ' ASC');
		}
		else if($serviceSearch == 'service')
		{
			$queryCS = $db->getQuery(true);
			$queryCS->select('company_id')
				->from($db->quoteName('#__bcted_company_services'))
				->where($db->quoteName('service_active') . ' = ' . $db->quote('1'));

			$db->setQuery($queryCS);
			$companyHasService = $db->loadColumn();

			$companyHasService = array_unique($companyHasService);

			$queryJetService = $db->getQuery(true);
			$queryJetService->select('company_id')
				->from($db->quoteName('#__bcted_company'))
				->where($db->quoteName('company_type') . ' = ' . $db->quote('jet'))
				->where($db->quoteName('company_active') . ' = ' . $db->quote('1'));

			/*echo $queryJetService->dump();
			exit;*/

			$db->setQuery($queryJetService);
			$jetServices = $db->loadColumn();

			if(count($jetServices) != 0)
			{
				$companyHasService = array_merge($companyHasService,$jetServices);
			}

			$query->select('a.*')
				->from($db->quoteName('#__bcted_company','a'))
				->where($db->quoteName('a.company_active') . ' =  ' .  $db->quote(1))
				->where($db->quoteName('a.company_id') . ' IN (' . implode(",", $companyHasService) . ')');
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'))
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'));

			$caption = $input->get('caption','','string');
			$inner_search = $input->get('inner_search',0,'int');

			if($inner_search == 1 && !empty($caption))
			{
				$query->where($db->quoteName('a.company_name') . ' LIKE ' .  $db->quote('%'.$caption.'%'));
			}

			$query->order($db->quoteName('a.company_name') . ' ASC');
		}
		else
		{
			$query->select('a.*')
				->from($db->quoteName('#__bcted_company','a'))
				->where($db->quoteName('a.company_active') . ' =  ' .  $db->quote(1));

			$query->order($db->quoteName('a.company_name') . ' ASC');
		}

		$oldCity = $app->input->cookie->get('search_in_city', '');

		if(!empty($countrySearch))
		{
			$app->input->cookie->set('search_in_city', $countrySearch);
			$search_in_city = $app->input->cookie->get('search_in_city', '');
		}
		else if (empty($countrySearch) && !empty($oldCity))
		{
			$search_in_city = $app->input->cookie->get('search_in_city', '');
		}

		if(!empty($search_in_city))
		{
			$query->where($db->quoteName('a.city') . ' LIKE  ' .  $db->quote('%'.$search_in_city.'%'));
		}

		///$query->order($db->quoteName('a.time_stamp') . ' DESC');

		$this->setState('list.limit', 21);

		//echo "<pre>".$query->dump()."</pre>";
		//exit;*/

		return $query;
	}


}
