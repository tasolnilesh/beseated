<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelCompanyBookings extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{

	}

	/*protected function populateState($ordering = null, $direction = null)
	{
		echo "populateState : ".$ordering . " || " . $direction;
		parent::populateState($ordering, $direction);
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	/*public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}*/

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */

	public function getCompanyBookings()
	{
		$user = JFactory::getUser();
		$elementType = BctedHelper::getUserGroupType($user->id);
		$elementDetail = BctedHelper::getUserElementID($user->id);

		if($elementType != 'ServiceProvider')
		{
			return array();
		}

		if(!$elementDetail->company_id)
		{
			return array();
		}

		/*echo "<pre>";
		print_r($elementDetail);
		echo "</pre>";
		exit;*/



		$db    = JFactory::getDbo();
		$queryV = $db->getQuery(true);

		$queryV->select('service_booking_id')
			->from($db->quoteName('#__bcted_service_booking'))
			->where($db->quoteName('company_id') . ' = ' . $db->quote($elementDetail->company_id))
			->where($db->quoteName('deleted_by_company') . ' = ' . $db->quote('0'))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'));

		$statusID = array();
		$statusID[] = BctedHelper::getStatusIDFromStatusName('Booked');
		$statusID[] = BctedHelper::getStatusIDFromStatusName('Cancelled');

		$queryV->where($db->quoteName('status') . ' IN ('.implode(",", $statusID).')');

		$queryV->order($db->quoteName('service_booking_datetime') . ' ASC');

		$db->setQuery($queryV);
		$bookingIDs = $db->loadColumn();

		/*echo "<pre>";
		print_r($bookingIDs);
		echo "</pre>";
		exit;*/

		$query = $db->getQuery(true);

		$query->select('sb.*')
			->from($db->quoteName('#__bcted_service_booking','sb'));

		if(!$bookingIDs)
		{
			return array();
		}
		else if(is_array($bookingIDs))
		{
			$bookingIDStr = implode(",", $bookingIDs);
			$query->where($db->quoteName('sb.service_booking_id') . ' IN (' . $bookingIDStr . ')');
		}
		else
		{
			$query->where($db->quoteName('sb.service_booking_id') . ' = ' . $db->quote($bookingIDs));
		}

		// Create the base select statement.

		$query->select('cs.service_name,cs.service_image,cs.service_description,cs.service_price')
			->join('LEFT','#__bcted_company_services AS cs ON cs.service_id=sb.service_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=sb.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=sb.user_status');

		$query->select('c.company_name,c.company_image,c.company_address,c.company_about')
			->join('LEFT','#__bcted_company AS c ON c.company_id=sb.company_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=sb.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=sb.user_id');

		$query->order($db->quoteName('sb.service_booking_datetime') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);

		$bookings = $db->loadObjectList();

		/*echo "<pre>";
		print_r($bookings);
		echo "</pre>";
		exit;*/


		if(!$bookings)
		{
			return array();
		}
		return $bookings;


	}

	public function summaryForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('count(status) as total_count,status')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
			->group($db->quoteName('status'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	public function getRevenueForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('sum(amount_payable) as revenue')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadResult();

		return $result;
	}

	public function deleteBooking($bookingID,$userType)
	{
		$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable',array());
		$user = JFactory::getUser();

		$tblServiceBooking->load($bookingID);

		if(!$tblServiceBooking->service_booking_id)
		{
			//COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE
			return 400;
		}

		$status = array();
		$status[] = BctedHelper::getStatusIDFromStatusName('Booked');
		$status[] = BctedHelper::getStatusIDFromStatusName('Cancelled');



		//if($status != $tblVenuebooking->status)
		if(!in_array($tblServiceBooking->status, $status))
		{
			//COM_IJOOMERADV_VENUE_TABLE_BOOKING_STATUS_NOT_BOOKING

			return 400;
		}

		$tblCompany = JTable::getInstance('Company', 'BctedTable',array());
		$tblCompany->load($tblServiceBooking->company_id);

		if($tblCompany->userid == $user->id)
		{
			$tblServiceBooking->deleted_by_company = 1;
		}
		elseif($tblServiceBooking->user_id == $user->id)
		{
			$tblServiceBooking->deleted_by_user = 1;
		}

		//$tblServiceBooking->is_deleted = 1;

		if(!$tblServiceBooking->store())
		{
			return 500;
		}

		return 200;



	}
}
