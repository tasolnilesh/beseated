<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted ClubRequestDetail Model
 *
 * @since  0.0.1
 */
class BctedModelMessageDetail extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$connectionID = $input->get('connection_id',0,'int');
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$queryMsg = $db->getQuery(true);

		$user = JFactory::getUser();

		// Create the base select statement.
		$queryMsg->select('msg.*')
			->from($db->quoteName('#__bcted_message','msg'))
			->where($db->quoteName('msg.connection_id') . ' = ' . $db->quote($connectionID))
			->where( '((' .
					$db->quoteName('from_userid') . ' = ' . $db->quote($user->id) .' AND '.$db->quoteName('deleted_by_from').' = '.$db->quote(0).') OR ('.
					$db->quoteName('to_userid') . ' = ' . $db->quote($user->id) .' AND '.$db->quoteName('deleted_by_to').' = '.$db->quote(0) .'))'
				)
			->order($db->quoteName('msg.time_stamp') . ' ASC');

		//echo $queryMsg->dump();
			//->group($db->quoteName('msg.connection_id'));

		//$queryMsg .= ' HAVING MAX(`id`) ORDER BY `msg`.`time_stamp` DESC';

		// Set the query and load the result.
		//$db->setQuery($query);

		/*try
		{
			$result = $db->loadColumn();
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}*/

		/*echo "<pre>";
		print_r($queryMsg);
		echo "</pre>";*/

		return $queryMsg;
	}

	public function saveMessage($data)
	{
		$tblMessage = JTable::getInstance('Message', 'BctedTable',array());
		$tblMessage->load(0);
		$tblMessage->bind($data);

		if(!$tblMessage->store())
		{
			return 500;
		}

		return 200;
	}

}
