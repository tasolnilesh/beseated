<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Favourites Model
 *
 * @since  0.0.1
 */
class BctedModelFavourites extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		$user  = JFactory::getUser();
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('a.favourite_id,a.favourite_type,a.favourited_id')
			->from($db->quoteName('#__bcted_favourites','a'))
			->where($db->quoteName('a.user_id') . ' =  ' .  $db->quote($user->id));

		$query->select('v.venue_name,v.city as veneu_city')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=a.favourited_id AND a.favourite_type="Venue"');

		$query->select('c.company_name,c.city as company_city')
			->join('LEFT','#__bcted_company AS c ON c.company_id=a.favourited_id AND a.favourite_type="Service"');

		$db->setQuery($query);
		$favouriteList = $db->loadObjectList();
		$allNames = array();
		foreach ($favouriteList as $key => $favourite)
		{
			if(strtolower($favourite->favourite_type) == 'service')
			{
				$allNames[] = strtolower($favourite->company_name);
			}
			else if(strtolower($favourite->favourite_type) == 'venue')
			{
				$allNames[] = strtolower($favourite->venue_name);
			}
		}

		sort($allNames);

		/*echo "<pre>";
		print_r($allNames);
		echo "</pre>";
		exit;*/

		$allNamesStr = implode("','", $allNames);
		$allNamesStr = "'".$allNamesStr."'";


		$query = $db->getQuery(true);
		$query->select('a.favourite_id,a.favourite_type,a.favourited_id')
			->from($db->quoteName('#__bcted_favourites','a'))
			->where($db->quoteName('a.user_id') . ' =  ' .  $db->quote($user->id));

		$query->select('v.*,v.city as veneu_city')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=a.favourited_id AND a.favourite_type="Venue"');

		$query->select('c.*,c.city as company_city')
			->join('LEFT','#__bcted_company AS c ON c.company_id=a.favourited_id AND a.favourite_type="Service"');

		$query->order('FIELD(c.company_name, '.$allNamesStr.')');
		$query->order('FIELD(v.venue_name, '.$allNamesStr.')');

		//$query->order(' IF( ISNULL( c.company_name ), 1, 0 ), c.company_name, IF( v.venue_name="", 1, 0 ), v.venue_name ASC');
		//$query->order($db->quoteName('c.company_name') . ' ASC');

		/*echo "<pre>";
		print_r($query->dump());
		echo "</pre>";
		exit;*/


		return $query;
	}


}
