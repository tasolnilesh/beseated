<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelClubTableBooking extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   0.0.1
	 */
	public function getTable($type = 'Venuebooking', $prefix = 'BctedTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	public function bookVenueTable($data = array())
	{
		$tblVenuebooking = $this->getTable();

		$tblVenuebooking->load(0);

		$tblVenuebooking->bind($data);

		if(!$tblVenuebooking->store())
		{
			return 0;
		}

		$newBookingID = $tblVenuebooking->venue_booking_id;

		$tblVenue = $this->getTable('Venue');
		$tblTable = $this->getTable('Table');
		$tblVenue->load($tblVenuebooking->venue_id);
		$tblTable->load($tblVenuebooking->venue_table_id);





		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$appHelper            = new bctedAppHelper;

		$bookingData = $appHelper->getBookingDetailForVenueTable($newBookingID,'Venue');

		/*if(!empty($tblVenuebooking->venue_booking_additional_info))
		{
			BctedHelper::sendMessage($tblVenuebooking->venue_id,0,0,$tblVenuebooking->venue_table_id,$tblVenue->userid,$tblVenuebooking->venue_booking_additional_info,$bookingData,$messageType='newtablebooking');
		}*/

		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "venue";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$db = JFactory::getDbo();

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');

		if($obj->id)
		{
			$userProfile    = $appHelper->getUserProfile($tblVenue->userid);
			$receiveRequest = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveRequest = $params->settings->pushNotification->receiveRequest;
			}

			if($receiveRequest)
			{
				if($tblTable->premium_id)
				{
					//$message = 'You have new Requst for '.$tblTable->venue_table_name;
					$message = JText::sprintf('PUSHNOTIFICATION_TYPE_NEWREQUESTRECEIVED_MESSAGE',$tblTable->venue_table_name);
				}
				else
				{
					//$message = 'You have new Requst for '.$tblTable->custom_table_name;
					$message = JText::sprintf('PUSHNOTIFICATION_TYPE_NEWREQUESTRECEIVED_MESSAGE',$tblTable->custom_table_name);
				}


				$jsonarray['pushNotificationData']['id']         = $obj->id;
				$jsonarray['pushNotificationData']['to']         = $tblVenue->userid;
				$jsonarray['pushNotificationData']['message']    = $message;
				$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWREQUESTRECEIVED'); //'NewRequestReceived';
				$jsonarray['pushNotificationData']['configtype'] = '';

				/*echo "<pre>";
				print_r($jsonarray);
				echo "</pre>";
				exit;*/

				BctedHelper::sendPushNotification($jsonarray);
			}

		}

		return 1;
	}
}
