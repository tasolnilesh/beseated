<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Model
 *
 * @since  0.0.1
 */
class BctedModelSearch extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JModelList
	 * @since   0.0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array();
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		/*echo "<pre>";
		print_r($input);
		echo "</pre>";
		[view] => search
		[country] => Australia
		[club] => club
		exit;*/

		$serachType = '';
		$clubSearch = $input->get('club','','string');
		$serviceSearch = $input->get('service','','string');
		$countrySearch = $input->get('country','','string');


		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		if($clubSearch == 'club')
		{
			$query->select('a.*')
				->from($db->quoteName('#__bcted_venue','a'))
				->where($db->quoteName('a.venue_active') . ' =  ' .  $db->quote(1));
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'))
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'));
		}
		else if($serviceSearch == 'service')
		{
			$query->select('a.*')
				->from($db->quoteName('#__bcted_company','a'))
				->where($db->quoteName('a.company_active') . ' =  ' .  $db->quote(1));
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'))
				//->where($db->quoteName('a.country') . ' LIKE  ' .  $db->quote('%'.$countrySearch.'%'));
		}
		else
		{
			$query->select('a.*')
				->from($db->quoteName('#__bcted_venue','a'))
				->where($db->quoteName('a.venue_active') . ' =  ' .  $db->quote(1));
		}

		if(empty($countrySearch))
		{
			$app->input->cookie->set('search_in_city', '');
			$search_in_city = '';
		}

		$oldCity = $app->input->cookie->get('search_in_city', '');

		if(!empty($countrySearch))
		{
			$app->input->cookie->set('search_in_city', $countrySearch);
			$search_in_city = $app->input->cookie->get('search_in_city', '');
		}
		else if (empty($countrySearch) && !empty($oldCity))
		{
			$search_in_city = $app->input->cookie->get('search_in_city', '');
		}

		if(!empty($search_in_city))
		{
			$query->where($db->quoteName('a.city') . ' LIKE  ' .  $db->quote('%'.$search_in_city.'%'));
		}

		$query->order($db->quoteName('a.time_stamp') . ' DESC');

		$this->setState('list.limit', 21);

		//echo "<pre>".$query->dump()."</pre>";
		//exit;*/

		return $query;
	}


}
