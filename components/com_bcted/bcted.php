<?php
/**
 * @package     Heartdart.site
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

JLoader::registerPrefix('Bcted', __DIR__);

JLoader::registerPrefix('Bcted', JPATH_ADMINISTRATOR . '/components/com_bcted/');

// Require helper file
JLoader::register('BctedHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'bcted.php');

JFactory::getDocument()->addStyleSheet(Juri::base() . 'components/com_heartdart/assets/css/com_bcted_style.css');

JFormHelper::addFieldPath(JPATH_ADMINISTRATOR . '/components/com_bcted/models/fields');
JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_bcted/tables');

// Get an instance of the controller prefixed by Heartdart
$controller = JControllerLegacy::getInstance('Bcted');

// Perform the Request task
$input = JFactory::getApplication()->input;
$controller->execute($input->getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
