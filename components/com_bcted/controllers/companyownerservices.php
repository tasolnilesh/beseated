<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerCompanyOwnerServices extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'CompanyOwnerServiceEdit', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function save()
	{
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$input = $app->input;

		$file = $input->files->get('jform');

		/*echo "<pre>";
		print_r($file);
		echo "</pre>";*/
		/*exit;*/

		$data = $input->get('jform',array(),'array');

		$serviceID = $input->get('service_id',0,'int');
		/*echo "<pre>";
		print_r($input);
		echo "</pre>";
		exit;*/

		   /*[venue_table_id] => 8
    [venue_id] => 4
    [venue_table_name] => Gold Table
    [custom_table_name] => aa
    [venue_table_price] => 1000.00
    [venue_table_capacity] => 8
    [venue_table_description] => This gold table is perfect for an awesome night out
		*/
		$imagePath = BctedHelper::uplaodFile($file['service_image'],'service',$user->id);

		if(!empty($imagePath))
		{
			$postData['service_image'] = $imagePath;
		}

		if(!empty($data['service_name']))
		{
			$postData['service_name']        = $data['service_name'];
		}

		if(!empty($data['service_price']))
		{
			$postData['service_price']        = $data['service_price'];
		}

		if(!empty($data['service_description']))
		{
			$postData['service_description']        = $data['service_description'];
		}


		$postData['service_id'] = $data['service_id'];
		$postData['company_id'] = $data['company_id'];
		$postData['user_id'] = $user->id;
		$postData['service_active'] = 1;
		$postData['service_created'] = date('Y-m-d H:i:s');
		$postData['service_modified'] = date('Y-m-d H:i:s');
		$postData['time_stamp'] = time();

		//$data['is_smoking']    = $is_smoking;

		$model = $this->getModel();
		$result = $model->save($postData,$serviceID);


		$menuItem = BctedHelper::getBctedMenuItem('company-self-services');


		$Itemid =$menuItem->id;
		$link = $menuItem->link.'&Itemid='.$Itemid;

		/*$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$link = $menu->getItem($itemid)->link;

		echo 'Itemid : ' . $itemid . ' || Link :'. JRoute::_($link);

		echo "<pre>";
		print_r($menu);
		echo "</pre>";
		exit;*/

		$this->setRedirect($link,'Service Detail Saved');

	}
}

