<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerMessage extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'Message', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function send_message1()
	{
		/*$app = JFactory::getApplication();
		$menu = $app->getMenu()->getActive()->link;
		$url = JRoute::_($menu);*/

		/*$menu = JFactory::getApplication()->getMenu();
		$parent = $menu->getActive();
		echo "<pre>";
		print_r($parent);
		echo "</pre>";
		exit;*/

		/*echo "hello";
		exit;*/

		$menuItem = BctedHelper::getBctedMenuItem('user-clubguestlist');
		/*echo "<pre>";
		print_r($menuItem);
		echo "</pre>";*/

		$Itemid =$menuItem->id;
		$link = $menuItem->link.'&Itemid='.$Itemid;

		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$link = $menu->getItem($itemid)->link;

		echo 'Itemid : ' . $itemid . ' || Link :'. JRoute::_($link);

		echo "<pre>";
		print_r($menu);
		echo "</pre>";
		exit;

	}

	public function send_message()
	{
		$app  = JFactory::getApplication();
		$input = $app->input;


		$otherUserID = $input->get('other_user_id',0,'int');
		$connectionID = $input->get('connection_id',0,'int');
		$message = $input->get('message','','string');

		$otherUserType = BctedHelper::getUserGroupType($otherUserID);


		$loginUser = JFactory::getUser();

		if(!$loginUser->id)
		{
			echo "704";
			exit;
		}

		$loginUserTyep = BctedHelper::getUserGroupType($loginUser->id);

		/*echo $loginUserTyep;
		exit;*/
		//$loginUserTyep = BctedHelper::getUserGroupType($loginUser->id);
		$venueID = 0;
		$companyID = 0;
		$data = array();
		if($loginUserTyep == 'Club' && $otherUserType == 'Registered')
		{
			$veneuDetail = BctedHelper::getUserElementID($loginUser->id);
			$data['venue_id']   = $veneuDetail->venue_id;
			$data['table_id']   = 0;
			$elementName = $veneuDetail->venue_name;
			$elementType = 'Venue';
			$venueID = $veneuDetail->venue_id;
		}
		else if ($loginUserTyep == 'ServiceProvider')
		{
			$companyDetail = BctedHelper::getUserElementID($loginUser->id);
			$data['company_id'] = $companyDetail->company_id;
			$data['service_id'] = 0;
			$elementName = $companyDetail->company_name;
			$elementType = 'Company';
			$companyID = $companyDetail->company_id;

		}
		else if ($loginUserTyep == 'Registered')
		{
			echo "706";
			exit;
		}

		$model = $this->getModel();

		$data['userid']       = $otherUserID;
		$data['to_userid']    = $otherUserID;
		$data['from_userid']  = $loginUser->id;
		$data['message']      = $message;
		$data['message_type'] = 'msg';

		$date = JFactory::getDate();
		$config = JFactory::getConfig();
		$date->setTimezone(new DateTimeZone($config->get('offset')));
		$currentdate = $date->format('Y-m-d H:i:s');

		$data['created']      = $currentdate; //date('Y-m-d H:i:s');
		$data['time_stamp']   = strtotime($currentdate);

		$connectionID = BctedHelper::getMessageConnection($data['from_userid'],$data['to_userid']);

		if(!$connectionID)
		{
			echo "400";
			exit;
		}

		$data['connection_id']   = $connectionID;

		$model = JModelLegacy::getInstance('MessageDetail', 'BctedModel',array('ignore_request' => true));
		$result = $model->saveMessage($data);

		if($result == 200)
		{
			require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
			$appHelper            = new bctedAppHelper;

			$userProfile    = $appHelper->getUserProfile($otherUserID);
			$receiveMessage = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveMessage = $params->settings->pushNotification->receiveMessage;
			}

			if($receiveMessage)
			{
				//$message = $elementName . ' sent you a new message';
				$message = JText::sprintf('PUSHNOTIFICATION_TYPE_NEWMESSAGE_MESSAGE',$elementName);

				$jsonarray['pushNotificationData']['to']         = $otherUserID;
				$jsonarray['pushNotificationData']['message']    = $message;

				if($companyID)
				{
					$jsonarray['pushNotificationData']['id']         = $elementName.';'.$connectionID.';'.$elementType; //$obj->id;
					$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWMESSAGEFROMCOMPANY'); //'NewMessageFromCompany';
				}
				else if($venueID)
				{
					$jsonarray['pushNotificationData']['id']         = $elementName.';'.$connectionID.';'.$elementType; //$obj->id;
					$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWMESSAGEFROMVENUE'); //'NewMessageFromVenue';
				}

				$jsonarray['pushNotificationData']['configtype'] = '';

				BctedHelper::sendPushNotification($jsonarray);
			}
		}

		echo $result;
		exit;



	}

	public function action_addme_at_venue_table()
	{
		$input = JFactory::getApplication()->input;

		$inviteID   = $input->get('invite_id',0,'int'); //IJReq::getTaskData('inviteID',0,'int');
		$actionBy   = $input->get('action_by','','string'); //IJReq::getTaskData('actionBy','user','string');
		$actionType = $input->get('action_type','','string'); //IJReq::getTaskData('actionType','accept','string');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenuetableinvite = JTable::getInstance('Venuetableinvite', 'BctedTable');
		$tblVenuetableinvite->load($inviteID);

		if(!$tblVenuetableinvite->invite_id)
		{
			echo "400";
			exit;
		}

		if($actionBy == 'user')
		{
			if($actionType == 'accept')
			{
				$tblVenuetableinvite->user_action = 'accept';
				//$this->sendAddMeVenueTableMessage($tblVenuetableinvite->venue_id,0,0,$$tblVenuetableinvite->table_id,$userID,$message,$messageType='tableaddme');
			}
			else if($actionType == 'rejcet')
			{
				$tblVenuetableinvite->user_action = 'rejcet';
			}
		}
		else if($actionBy == 'venue')
		{
			if($actionType == 'accept')
			{
				$tblVenuetableinvite->venue_action = 'accept';
			}
			else if($actionType == 'rejcet')
			{
				$tblVenuetableinvite->venue_action = 'rejcet';
			}
		}



		if(!$tblVenuetableinvite->store())
		{
			echo "500";
			exit;
			/*IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_ADD_ME_ERROR_INVITE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;*/
		}

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblTable = JTable::getInstance('Table', 'BctedTable');
		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');

		$tblVenue->load($tblVenuetableinvite->venue_id);
		$tblTable->load($tblVenuetableinvite->table_id);
		$tblVenuebooking->load($tblVenuetableinvite->booking_id);

		$userBookingOwner = JFactory::getUser($tblVenuebooking->user_id);
		$userRequested = JFactory::getUser($tblVenuetableinvite->user_id);

		//$this->jsonarray['code'] = 200;

		if($actionBy == 'user' && $actionType=='accept')
		{
			$message = JText::sprintf('COM_BESEATED_VENUE_BOOKING_INVITE_ACCEPT_BY_USER',$userBookingOwner->name,$userRequested->name, $tblTable->venue_table_name,$tblVenue->venue_name,date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime)));
			//$message = $userBookingOwner->name .' want to add ' . $userRequested->name . ' for ' . $tblTable->venue_table_name .' at '. $tblVenue->venue_name . ' on '.$tblVenuebooking->venue_booking_datetime;

			//<booking Owner Name>  want to add <requested user name> at ' . $tblVenue->venue_name . ';'.$tblTable->venue_table_name.';'.$tblVenuebooking->venue_booking_datetime;

			$jsonarray['pushNotificationData']['id']         = $tblVenuetableinvite->invite_id;
			$jsonarray['pushNotificationData']['to']         = $tblVenue->userid;
			$jsonarray['pushNotificationData']['message']    = $message;
			$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_REQUESTFORADDMEATTABLETOCLUB'); //'RequestForAddMeAtTableToClub';
			$jsonarray['pushNotificationData']['configtype'] = '';

			$extraParam = array();
			$extraParam['inviteID'] = $tblVenuetableinvite->invite_id;
			//$this->sendAddMeVenueTableMessage($tblVenuetableinvite->venue_id,0,0,$$tblVenuetableinvite->table_id,$tblVenue->userid,$message,$extraParam,$messageType='tableaddme');
			BctedHelper::sendMessage($tblVenuetableinvite->venue_id,0,0,$tblVenuetableinvite->table_id,$tblVenue->userid,$message,$extraParam,'tableaddme');
			BctedHelper::sendPushNotification($jsonarray);

			echo "200";
			exit;
		}

		if($actionBy == 'venue' && $actionType=='accept')
		{
			$message = JText::sprintf('COM_BESEATED_VENUE_BOOKING_INVITE_ACCEPT_BY_CLUB',$tblTable->venue_table_name, $userBookingOwner->name, $tblVenue->venue_name, date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime)));
			//$message = 'you can join '.$tblTable->venue_table_name.' table with' . $userBookingOwner->name . ' at ' . $tblVenue->venue_name . ' on '.$tblVenuebooking->venue_booking_datetime;

			$jsonarray['pushNotificationData']['id']         = $tblVenuetableinvite->invite_id;
			$jsonarray['pushNotificationData']['to']         = $userRequested->id;
			$jsonarray['pushNotificationData']['message']    = $message;
			$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_VENUETABLEADDMEREQUESTCONFIRM'); //'VenueTableAddMeRequestConfirm';
			$jsonarray['pushNotificationData']['configtype'] = '';

			//BctedHelper::sendMessage($tblVenuetableinvite->venue_id,0,0,$tblVenuetableinvite->table_id,$tblVenue->userid,$message,$extraParam,'tableaddme');
			BctedHelper::sendPushNotification($jsonarray);

			echo "200";
			exit;
		}

		echo "500";
		exit;
	}

	public function delete_message()
	{
		$input = JFactory::getApplication()->input;

		$deletedBy     = $input->get('delete_by','','string');
		$entrVenueID   = $input->get('venue_id',0,'int');
		$entrCompanyID = $input->get('company_id',0,'int');
		$entrUserID    = $input->get('userid',0,'int');
		$loginUser     = JFactory::getUser();


		/*echo $deletedBy."<br />";
		echo $entrVenueID."<br />";
		echo $entrCompanyID."<br />";
		echo $entrUserID."<br />";
		echo $loginUser->id."<br />";
		exit;*/


		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query2 = $db->getQuery(true);

		if($deletedBy == 'user')
		{
			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_to') . ' = ' . $db->quote(1))
				->where($db->quoteName('to_userid') . ' = ' . $db->quote($loginUser->id));

			if($entrVenueID)
			{
				$query->where($db->quoteName('venue_id') . ' = ' . $db->quote($entrVenueID));
			}
			else if($entrCompanyID)
			{
				$query->where($db->quoteName('company_id') . ' = ' . $db->quote($entrCompanyID));
			}

			$db->setQuery($query);
			$db->execute();

			$query2->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_from') . ' = ' . $db->quote(1))
				->where($db->quoteName('from_userid') . ' = ' . $db->quote($loginUser->id));

			if($entrVenueID)
			{
				$query2->where($db->quoteName('venue_id') . ' = ' . $db->quote($entrVenueID));
			}
			else if($entrCompanyID)
			{
				$query2->where($db->quoteName('company_id') . ' = ' . $db->quote($entrCompanyID));
			}

			$db->setQuery($query2);
			$db->execute();

			/*echo $query2->dump();
			echo $query->dump();
			exit;*/
		}
		else if($deletedBy == 'venue')
		{
			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_from') . ' = ' . $db->quote(1));

			if($entrUserID)
			{
				//$userVenueID = $this->helper->getUserVenueID($this->IJUserID);

				$query->where($db->quoteName('to_userid') . '   = ' . $db->quote($entrUserID));
				$query->where($db->quoteName('from_userid') . ' = ' . $db->quote($loginUser->id));
				//$query->where($db->quoteName('venue_id') . ' = ' . $db->quote($userVenueID));
			}

			/*echo $query->dump();
			exit;*/

			$db->setQuery($query);
			$db->execute();


			$query2->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_to') . ' = ' . $db->quote(1));

			if($entrUserID)
			{
				//$userVenueID = $this->helper->getUserVenueID($this->IJUserID);

				$query2->where($db->quoteName('to_userid') . ' = ' . $db->quote($loginUser->id));
				$query2->where($db->quoteName('from_userid') . ' = ' . $db->quote($entrUserID));
			}

			$db->setQuery($query2);
			$db->execute();
		}
		else if($deletedBy == 'company')
		{
			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_from') . ' = ' . $db->quote(1));

			if($entrUserID)
			{
				//$userCompanytID = $this->helper->getUserCompanyID($this->IJUserID);
				$query->where($db->quoteName('to_userid') . ' = ' . $db->quote($entrUserID));
				$query->where($db->quoteName('from_userid') . ' = ' . $db->quote($loginUser->id));
			}

			$db->setQuery($query);
			$db->execute();

			// Create the base update statement.
			$query2->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_to') . ' = ' . $db->quote(1));

			if($entrUserID)
			{
				//$userCompanytID = $this->helper->getUserCompanyID($this->IJUserID);
				$query2->where($db->quoteName('to_userid') . ' = ' . $db->quote($loginUser->id));
				$query2->where($db->quoteName('from_userid') . ' = ' . $db->quote($entrUserID));
			}

			$db->setQuery($query2);
			$db->execute();
		}

		echo "200";
		exit;
	}
}
