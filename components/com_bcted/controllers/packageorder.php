<?php
/**
 * @package     Diff.Site
 * @subpackage  com_Diff
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Diff Order Controller
 *
 * @since  0.0.1
 */
class BctedControllerPackageorder extends JControllerForm
{
	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   string  $name  	 The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array  	$config  Configuration array for model. Optional.
	 *
	 * @return object The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = '', $prefix = '', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	/**
	 * Method to Cancel event creation action
	 *
	 * @param   int  $key  key column for table
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	public function cancel($key = 'dish_id')
	{
		$app = JFactory::getApplication();
		$app->redirect('index.php?option=com_diff&view=frontdishes');
		$app->close();
	}

	/**
	 * makeOffer to particular restaurant.
	 *
	 * @since   0.0.1
	 */
	public function makeOrder()
	{
		$app   = JFactory::getApplication();
		$model = $this->getModel('Order', 'DiffModel');

		$result = $model->makeOrder();

		echo $result;
		$app->close();
		//return $result;

		/*$app->redirect('index.php?option=com_diff&view=frontdishes&Itemid=106');
		$app->close();*/
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function packagePurchased()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$booking_id = $input->get('booking_id',0);
		$booking_type = $input->get('booking_type','');

		if(strtolower($booking_type) == 'venue')
		{
			$bookingID = $booking_id;

			JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
			$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');
			$tblVenuebooking->load($bookingID);

			if(!$tblVenuebooking->venue_booking_id)
			{
				/*IJReq::setResponseCode(400);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_BOOKING_DATA'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;*/
			}

			$currentBookingDate = $tblVenuebooking->venue_booking_datetime;
			$datesArray         = array();
			$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' -1 day'));
			$datesArray[]       = $tblVenuebooking->venue_booking_datetime;
			$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' +1 day'));
			$currentBookingFrom = $tblVenuebooking->booking_from_time;
			$currentBookingTo   = $tblVenuebooking->booking_to_time;

			if (strtotime($currentBookingFrom)>strtotime($currentBookingTo))
			{
				$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
				$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo.' +1 day'));
			} else {
				$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
				$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo));
			}

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('*')
				->from($db->quoteName('#__bcted_venue_booking'))
				->where($db->quoteName('venue_id') . ' = ' . $db->quote($tblVenuebooking->venue_id))
				->where($db->quoteName('venue_table_id') . ' = ' . $db->quote($tblVenuebooking->venue_table_id))
				->where($db->quoteName('venue_booking_id') . ' <> ' . $db->quote($tblVenuebooking->venue_booking_id))
				->where($db->quoteName('status') . ' = ' . $db->quote(5))
				->where($db->quoteName('venue_booking_datetime') . ' IN (\''. implode("','", $datesArray) .'\')' );

			// Set the query and load the result.
			$db->setQuery($query);

			$slotBooked = 0;
			$bookingsOnSameDate = $db->loadObjectList();
			$timeSlots = array();

			foreach ($bookingsOnSameDate as $key => $booking)
			{
				$bookingDate = $booking->venue_booking_datetime;
				$bookingFrom = $booking->booking_from_time;
				$bookingTo = $booking->booking_to_time;

				if (strtotime($bookingDate.' '.$bookingFrom)>strtotime($bookingDate.' '.$bookingTo)) {
					$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
					$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo.' +1 days'));
				} else {
					$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
					$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo));
				}


				$numbercurrentBookingFrom = strtotime($currentBookingFrom);
				$numbercurrentBookingTo   = strtotime($currentBookingTo);
				$numberbookingFrom        = strtotime($bookingFrom);
				$numberbookingTo          = strtotime($bookingTo);

				/*echo "currentBookingFrom:" . $currentBookingFrom . " || ".$numbercurrentBookingFrom."<br />";
				echo "currentBookingTo:" . $currentBookingTo . " || ".$numbercurrentBookingTo."<br /><br />";
				echo "bookingFrom:" . $bookingFrom . " || ".$numberbookingFrom."<br />";
				echo "bookingTo:" . $bookingTo . " || ".$numberbookingTo."<br /><br />";*/

				if(($numberbookingFrom < $numbercurrentBookingFrom) && ($numbercurrentBookingFrom < $numberbookingTo))
				{
					$slotBooked = $slotBooked + 1;
				}
				else if(($numberbookingFrom < $numbercurrentBookingTo) && ($numbercurrentBookingTo < $numberbookingTo))
				{
					$slotBooked = $slotBooked + 1;
				}
				else if(($numberbookingTo == $numbercurrentBookingTo) && ($numbercurrentBookingFrom == $numberbookingFrom))
				{
					$slotBooked = $slotBooked + 1;
				}
			}

			if($slotBooked)
			{
				$menu = $app->getMenu();
				//index.php?option=com_bcted&view=userbookings&Itemid=188
				$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=userbookings', true );

				$redirectLink = 'index.php?option=com_bcted&view=userbookingdetail&booking_type=table&booking_id='.$booking_id.'&Itemid='.$menuItem->id;
				$app->redirect($redirectLink,JText::_('COM_BCTED_TABLE_TIME_SLOT_ALREADY_BOOKED_DESC'));
			}
		}

		//&booking_id=49&booking_type=venue
		JPluginHelper::importPlugin('bct');
		$dispatcher = JDispatcher::getInstance('paypal');
		$results = $dispatcher->trigger('onPreparePaymentPaypal', array());
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function packageInvitePayment()
	{
		/*echo "call";
		exit;*/
		JPluginHelper::importPlugin('bct');
		$dispatcher = JDispatcher::getInstance('paypal');
		$results = $dispatcher->trigger('onPackageInvitePaymentPaypal', array());
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function placeRequest()
	{
		JPluginHelper::importPlugin('dreamyFlavours');
		$dispatcher = JDispatcher::getInstance('paypal');
		$results = $dispatcher->trigger('onPreparePaymentPaypalForRequest', array());
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function paypalresponse()
	{

	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function paymentCancel()
	{

	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function paymentReturn()
	{
		$app->redirect("index.php?option=com_diff&view=dish");
		$app->close();

	}

	public function ipninvite()
	{
		mail("fbbcted.developer@gmail.com", "Get", json_encode($_GET));
		mail("fbbcted.developer@gmail.com", "Post", json_encode($_POST));
		mail("fbbcted.developer@gmail.com", "Request", json_encode($_REQUEST));


		$app = JFactory::getApplication();

		$input = $app->input;

		$response_data = $input->get('encResp');

		$bctParams = $this->getExtensionParam();

		$rcvdString = $this->decrypt($response_data, $bctParams->encryptionKey);
		$decryptValues=explode('&', $rcvdString);
		$dataSize=sizeof($decryptValues);
		$response_array		= array();
		for($i = 0; $i < count($decryptValues); $i++)
		{
	  		$information	= explode('=',$decryptValues[$i]);
			if(count($information)==2)
			{
				$response_array[$information[0]] = urldecode($information[1]);
			}
		}

		mail("bcted.developer@gmail.com", "CCresponse ".time(), json_encode($response_array));

		$paymentEntryID = $input->get('payment_entry_id',0,'int');

		$tblPaymentStatus = JTable::getInstance('PaymentStatus', 'BctedTable',array());
		$tblPaymentStatus->load($paymentEntryID);


		if(!$tblPaymentStatus->payment_id)
		{
			return;
		}

		$elementID = $tblPaymentStatus->booked_element_id;
		$elementType = $tblPaymentStatus->booked_element_type;

		$paymentGross = $response_array['amount'];
		$paymentFee   = 0;
		$txnID        = $response_array['tracking_id'];
		$paymentStatusText  = $response_array['order_status'];

		$tblPaymentStatus->payment_fee = $paymentFee;
		$tblPaymentStatus->txn_id = $txnID;
		$tblPaymentStatus->payment_status = $paymentStatusText;

		// Edited By nilesh
		// if($paymentStatusText == 'Completed' || $response_array['vault'] == 'Y')

		if($paymentStatusText == 'Completed' || $response_array['order_status'] == 'Success')
		{

			$tblPackageInvite = JTable::getInstance('PackageInvite', 'BctedTable');
			$tblPackageInvite->load($elementID);

			$tblPackageInvite->payment_status = strtolower($paymentStatusText);
			$tblPackageInvite->status = 5;
			$tblPackageInvite->store();


			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_loyalty_point'))
				->set($db->quoteName('is_valid') . ' = ' . $db->quote(1))
				->where($db->quoteName('cid') . ' = ' . $db->quote($paymentEntryID));

			// Set the query and execute the update.
			$db->setQuery($query);
			$db->execute();

			$tblVenue = JTable::getInstance('Venue', 'BctedTable');
			$tblVenue->load($tblPaymentStatus->venue_id);
			$toUserID = $tblVenue->userid;

			$this->sendPushNotification($toUserID,$elementID,$elementType);

			$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
			$tblPackagePurchased->load($tblPackageInvite->package_purchase_id);

			if($this->checkForInvitedPaymentDone($tblPackageInvite->package_id,$tblPackageInvite->package_purchase_id))
			{

				$tblPackagePurchased->status = 6;
				$tblPackagePurchased->user_status = 3;

			}
			else
			{
				$tblPackagePurchased->status = 5;
				$tblPackagePurchased->user_status = 5;

			}


			//$tblPackagePurchased->booked_user_paid = 5;
			$tblPackagePurchased->can_invite = 0;
			$tblPackagePurchased->store();


		}

		$tblPaymentStatus->store();

		// Edited by nilesh
		// if($response_array['vault'] == 'Y' || $paymentStatusText == 'Completed')

		if($response_array['order_status'] == 'Success' || $paymentStatusText == 'Completed')
		{
			$app = JFactory::getApplication();
			$app->redirect("index.php?success=true&payment_entry_id={$paymentEntryID}");
			$app->close();

		}else
		{
			$app = JFactory::getApplication();
			$app->redirect("index.php?success=false");
			$app->close();
		}
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function ipn()
	{
		/*echo "call";
		exit;*/
		mail("bcted.developer@gmail.com", "CCGet ".time(), json_encode($_GET));
		mail("bcted.developer@gmail.com", "CCPost ".time(), json_encode($_POST));
		mail("bcted.developer@gmail.com", "CCRequest ".time(), json_encode($_REQUEST));

		$app = JFactory::getApplication();

		$input = $app->input;

		$response_data = $input->get('encResp');

		$bctParams = $this->getExtensionParam();

		$rcvdString = $this->decrypt($response_data, $bctParams->encryptionKey);
		$decryptValues=explode('&', $rcvdString);
		$dataSize=sizeof($decryptValues);
		$response_array		= array();
		for($i = 0; $i < count($decryptValues); $i++)
		{
	  		$information	= explode('=',$decryptValues[$i]);
			if(count($information)==2)
			{
				$response_array[$information[0]] = urldecode($information[1]);
			}
		}

		mail("bcted.developer@gmail.com", "CCresponse ".time(), json_encode($response_array));

		/*$model = $this->getModel();

		$model->savePaymentData()*/

		$app = JFactory::getApplication();

		$input = $app->input;

		$paymentEntryID = $input->get('payment_entry_id',0,'int');
		$full_payment = $input->get('full_payment',0,'int');

		/*echo $full_payment;
		exit;*/


		$tblPaymentStatus = JTable::getInstance('PaymentStatus', 'BctedTable',array());
		$tblPaymentStatus->load($paymentEntryID);

		/*echo "<pre>";
		print_r($tblPaymentStatus);
		echo "</pre>";
		exit;*/

		if(!$tblPaymentStatus->payment_id)
		{
			return;
		}

		$elementID = $tblPaymentStatus->booked_element_id;
		$elementType = $tblPaymentStatus->booked_element_type;

		$paymentGross = $response_array['amount'];
		$paymentFee   = 0;
		$txnID        = $response_array['tracking_id'];
		$paymentStatusText  = $response_array['order_status'];

		$tblPaymentStatus->payment_fee = $paymentFee;
		$tblPaymentStatus->txn_id = $txnID;
		$tblPaymentStatus->payment_status = $paymentStatusText;

		$tblPaymentStatus->cc_tracking_id      = $response_array['tracking_id'];
		$tblPaymentStatus->cc_bank_ref_no      = $response_array['bank_ref_no'];
		$tblPaymentStatus->cc_order_status     = $response_array['order_status'];
		$tblPaymentStatus->cc_failure_message  = $response_array['failure_message'];
		$tblPaymentStatus->cc_payment_mode     = $response_array['payment_mode'];
		$tblPaymentStatus->cc_card_name        = $response_array['card_name'];
		$tblPaymentStatus->cc_status_code      = $response_array['status_code'];
		$tblPaymentStatus->cc_status_message   = $response_array['status_message'];
		$tblPaymentStatus->cc_currency         = $response_array['currency'];
		$tblPaymentStatus->cc_amount           = $response_array['amount'];
		$tblPaymentStatus->cc_billing_name     = $response_array['billing_name'];
		$tblPaymentStatus->cc_billing_address  = $response_array['billing_address'];
		$tblPaymentStatus->cc_billing_city     = $response_array['billing_city'];
		$tblPaymentStatus->cc_billing_state    = $response_array['billing_state'];
		$tblPaymentStatus->cc_billing_zip      = $response_array['billing_zip'];
		$tblPaymentStatus->cc_billing_country  = $response_array['billing_country'];
		$tblPaymentStatus->cc_billing_tel      = $response_array['billing_tel'];
		$tblPaymentStatus->cc_billing_email    = $response_array['billing_email'];
		$tblPaymentStatus->cc_delivery_name    = $response_array['delivery_name'];
		$tblPaymentStatus->cc_delivery_address = $response_array['delivery_address'];
		$tblPaymentStatus->cc_delivery_city    = $response_array['delivery_city'];
		$tblPaymentStatus->cc_delivery_state   = $response_array['delivery_state'];
		$tblPaymentStatus->cc_delivery_zip     = $response_array['delivery_zip'];
		$tblPaymentStatus->cc_delivery_country = $response_array['delivery_country'];
		$tblPaymentStatus->cc_delivery_tel     = $response_array['delivery_tel'];
		$tblPaymentStatus->cc_merchant_param1  = $response_array['merchant_param1'];
		$tblPaymentStatus->cc_merchant_param2  = $response_array['merchant_param2'];
		$tblPaymentStatus->cc_merchant_param3  = $response_array['merchant_param3'];
		$tblPaymentStatus->cc_merchant_param4  = $response_array['merchant_param4'];
		$tblPaymentStatus->cc_merchant_param5  = $response_array['merchant_param5'];
		$tblPaymentStatus->cc_vault            = $response_array['vault'];
		$tblPaymentStatus->cc_offer_type       = $response_array['offer_type'];
		$tblPaymentStatus->cc_offer_code       = $response_array['offer_code'];
		$tblPaymentStatus->cc_discount_value   = $response_array['discount_value'];


		// Edited by nilesh
		// if($paymentStatusText == 'Completed' || $response_array['vault'] == 'Y')


		if($paymentStatusText == 'Completed' || $response_array['order_status'] == 'Success')
		{
			$tblPaymentStatus->paid_status = 1;

			if($elementType == 'service')
			{
				$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');
				$tblServiceBooking->load($elementID);

				$tblCompany = JTable::getInstance('Company', 'BctedTable');
				$tblCompany->load($tblServiceBooking->company_id);
				$toUserID = $tblCompany->userid;

				$tblServiceBooking->status = 5;
				$tblServiceBooking->user_status = 5;

				//$tblServiceBooking->total_price = $tblPaymentStatus->element_price;
				$tblServiceBooking->deposit_amount = $tblPaymentStatus->platform_income;
				//$tblServiceBooking->amount_payable = $tblPaymentStatus->element_price - $tblPaymentStatus->platform_income;
				$tblServiceBooking->deposit_paid_date = date('Y-m-d');
				$tblServiceBooking->store();

				$this->sendPushNotification($toUserID,$elementID,$elementType);

				$this->checkForFirstPurchase($tblServiceBooking->user_id);
			}
			else if($elementType == 'venue')
			{
				$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');
				$tblVenuebooking->load($elementID);

				$tblVenue = JTable::getInstance('Venue', 'BctedTable');
				$tblVenue->load($tblVenuebooking->venue_id);
				$toUserID = $tblVenue->userid;

				$tblVenuebooking->status = 5;
				$tblVenuebooking->user_status = 5;

				$tblVenuebooking->total_price = $tblPaymentStatus->element_price;
				$tblVenuebooking->deposit_amount = $tblPaymentStatus->platform_income;
				$tblVenuebooking->amount_payable = $tblPaymentStatus->element_price - $tblPaymentStatus->platform_income;
				$tblVenuebooking->deposit_paid_date = date('Y-m-d');
				$tblVenuebooking->store();

				$this->sendPushNotification($toUserID,$elementID,$elementType);

				$this->checkForFirstPurchase($tblVenuebooking->user_id);


			}
			else if($elementType == 'package')
			{
				$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
				$tblPackagePurchased->load($elementID);

				/*echo "<pre>";
				print_r($tblPackagePurchased);
				echo "</pre>";
				echo "<pre>";
				print_r($tblPaymentStatus);
				echo "</pre>";
				exit;*/
				/*$tblPackagePurchased->status = 5;
				$tblPackagePurchased->user_status = 5;
				$tblPackagePurchased->store();*/

				if($tblPaymentStatus->full_payment == 1)
				{
					$this->packageFullPaymentChangeInviteStatus($tblPackagePurchased->package_id,$tblPackagePurchased->package_purchase_id);
				}

				$this->checkForFirstPurchase($tblPackagePurchased->user_id);

				$tblVenue = JTable::getInstance('Venue', 'BctedTable');
				$tblCompany = JTable::getInstance('Company', 'BctedTable');
				$tblVenue->load($tblPackagePurchased->venue_id);
				$tblCompany->load($tblPackagePurchased->company_id);

				if($packagePurchased->venue_id)
				{
					$toUserID = $tblVenue->userid;
				}
				else if($packagePurchased->company_id)
				{
					$toUserID = $tblCompany->userid;
				}

				$this->sendPushNotification($toUserID,$elementID,$elementType);
				$this->sendMessageForPackageService($tblPackagePurchased->package_id,$tblPackagePurchased->package_purchase_id);
				/*echo $this->checkForInvitedPaymentDone($tblPackagePurchased->package_id,$tblPackagePurchased->package_purchase_id);
				exit;*/
				if($this->checkForInvitedPaymentDone($tblPackagePurchased->package_id,$tblPackagePurchased->package_purchase_id))
				{

					$tblPackagePurchased->status = 6;
					$tblPackagePurchased->user_status = 3;

				}
				else
				{
					$tblPackagePurchased->status = 5;
					$tblPackagePurchased->user_status = 5;

				}

				$tblPackagePurchased->booked_user_paid = 1;
				$tblPackagePurchased->can_invite = 0;
				$tblPackagePurchased->store();
			}

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_loyalty_point'))
				->set($db->quoteName('is_valid') . ' = ' . $db->quote(1))
				->where($db->quoteName('cid') . ' = ' . $db->quote($paymentEntryID));

			// Set the query and execute the update.
			$db->setQuery($query);
			$db->execute();


		}

		$tblPaymentStatus->store();

		// Edited by nilesh
		// if($response_array['vault'] == 'Y' || $paymentStatusText == 'Completed')

		/*echo "<pre>";
		print_r($response_array);
		echo "</pre>";
		exit;*/

		if($response_array['order_status'] == 'Success' || $paymentStatusText == 'Completed')
		{
			$app = JFactory::getApplication();
			$app->redirect("index.php?success=true&payment_entry_id={$paymentEntryID}");
			$app->close();

		}else
		{
			$app = JFactory::getApplication();
			$app->redirect("index.php?success=false");
			$app->close();
		}


		/*if($tblPaymentStatus->store())
		{
			$jsonarray['code']= 200;
			echo json_encode($jsonarray);
			exit;
		}
		else
		{
			$jsonarray['code']= 500;
			echo json_encode($jsonarray);
			exit;
		}*/

		/*echo "called";
		exit;*/


	}

	public 	function decrypt($encryptedText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText=$this->hextobin($encryptedText);
		$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		mcrypt_generic_init($openMode, $secretKey, $initVector);
		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
		$decryptedText = rtrim($decryptedText, "\0");
		mcrypt_generic_deinit($openMode);
		return $decryptedText;
	}

	public function hextobin($hexString)
	{
		$length = strlen($hexString);
		$binString="";
		$count=0;
		while($count<$length)
		{
			$subString =substr($hexString,$count,2);
			$packedString = pack("H*",$subString);
			if ($count==0)
			{
				$binString=$packedString;
			}
			else
			{
				$binString.=$packedString;
			}
			$count+=2;
		}
		return $binString;
	}

	public function checkForInvitedPaymentDone($packageID,$packagePurchaseID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_package_invite'))
			->where($db->quoteName('package_id') . ' = ' . $db->quote($packageID))
			->where($db->quoteName('package_purchase_id') . ' = ' . $db->quote($packagePurchaseID))
			->where($db->quoteName('status') . ' = ' . $db->quote('2'));
		//echo $query->dump();
		/*exit;*/

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		echo $query->dump();
		exit;*/

		if(count($result))
		{
			return 1;
		}

		return 0;
	}

	public function packageFullPaymentChangeInviteStatus($packageID,$packagePurchaseID)
	{

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base update statement.
		$query->update($db->quoteName('#__bcted_package_invite'))
			->set($db->quoteName('status') . ' = ' . $db->quote(5))
			->set($db->quoteName('payment_status') . ' = ' . $db->quote('success'))
			->where($db->quoteName('package_id') . ' = ' . $db->quote($packageID))
			->where($db->quoteName('package_purchase_id') . ' = ' . $db->quote($packagePurchaseID));

		// Set the query and execute the update.
		$db->setQuery($query);

		/*echo $query->dump();
		exit;*/

		$db->execute();

	}

	public function checkForFirstPurchase($userID)
	{
		$fpUser = JFactory::getUser($userID);
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_user_refer'))
			->where($db->quoteName('is_registered') . ' = ' . $db->quote('1'))
			->where($db->quoteName('ref_user_id') . ' = ' . $db->quote($fpUser->id))
			->where($db->quoteName('refer_email') . ' = ' . $db->quote($fpUser->email))
			->order($db->quoteName('time_stamp') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);
		$userReferObj = $db->loadObject();

		if($userReferObj)
		{
			JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
			$tblRefer = JTable::getInstance('Refer', 'BctedTable');
			$tblRefer->load($userReferObj->refer_id);

			$bctParams = $this->getExtensionParam();
			$points = ($bctParams->friends_referral)?$bctParams->friends_referral:50;

			$tblLoyaltyPoint = JTable::getInstance('LoyaltyPoint', 'BctedTable');
			$tblLoyaltyPoint->load(0);

			$lpPost['user_id']    = $userReferObj->userid;
			$lpPost['earn_point'] = $points;
			$lpPost['point_app']  = 'inviteduserfp';
			$lpPost['cid']        = $userReferObj->refer_id;
			$lpPost['is_valid']   = '1';
			$lpPost['created']    = date('Y-m-d H:i:s');
			$lpPost['time_stamp'] = time();


			$tblLoyaltyPoint->bind($lpPost);

			if($tblLoyaltyPoint->store())
			{
				$tblRefer->is_fp_done = 1;
				$tblRefer->fp_date = date('Y-m-d');
				if($tblRefer->store())
				{
					$query = $db->getQuery(true);

					// Create the base delete statement.
					$query->delete()
						->from($db->quoteName('#__bcted_user_refer'))
						->where($db->quoteName('ref_user_id') . ' = ' . $db->quote((int) $userReferObj->ref_user_id))
						->where($db->quoteName('refer_email') . ' = ' . $db->quote((int) $userReferObj->ref_user_id))
						->where($db->quoteName('refer_id') . ' <> ' . $db->quote((int) $userReferObj->refer_id));

					// Set the query and execute the delete.
					$db->setQuery($query);
					$db->execute();
				}
			}
		}


	}

	public function getExtensionParam()
	{
		$app    = JFactory::getApplication();

		$option = "com_bcted";
		$db     = JFactory::getDbo();

		$option = '%' . $db->escape($option, true) . '%';

		// Initialiase variables.
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->qn('#__extensions'))
			->where($db->qn('name') . ' LIKE ' . $db->q($option))
			->where($db->qn('type') . ' = ' . $db->q('component'))
			->order($db->qn('ordering') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadObject();

			$params = json_decode($result->params);
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), 500);
		}

		return $params;
	}

	public function sendPushNotification($toUserID,$BookedElementID,$elementType)
	{
		//$toUserID = 114;
		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$bctHelper            = new bctedAppHelper;

		$pushcontentdata=array();
		$changedFor = '';

		if($elementType == 'venue')
		{
			$bookingData = $bctHelper->getBookingDetailForVenueTable($BookedElementID,'Venue');
			$pushcontentdata['elementType'] = "Venue";

			if($bookingData[0]['premiumTableID'])
			{
				$changedFor = $bookingData[0]['tableName'];
			}
			else
			{
				$changedFor = $bookingData[0]['customTableName'];
			}

		}
		else if($elementType == 'service')
		{
			$bookingData = $bctHelper->getBookingDetailForServices($BookedElementID,'Service');
			$pushcontentdata['elementType'] = "Company";
			$changedFor = $bookingData[0]['serviceName'];
		}
		else if($elementType == 'package')
		{
			$bookingData = $bctHelper->getBookingDetailForPackage($BookedElementID,'Venue');
			$pushcontentdata['elementType'] = "Venue";
			$changedFor = $bookingData[0]['packageName'];
		}

		$pushcontentdata['data'] = $bookingData;


		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$db = JFactory::getDbo();

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');

		/*echo "<pre>";
		print_r($obj);
		echo "</pre>";
		exit;*/

		if($obj->id)
		{
			$userProfile    = $bctHelper->getUserProfile($toUserID);

			/*echo "<pre>";
			print_r($userProfile);
			echo "</pre>";
			exit;*/
			$updateMyBookingStatus = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$updateMyBookingStatus = $params->settings->pushNotification->updateMyBookingStatus;

				//$updateMyBookingStatus = 1;
			}

			/*echo $updateMyBookingStatus;
			exit;*/

			if($updateMyBookingStatus)
			{
				$toUserDetail = JFactory::getUser($toUserID);
				//$message = 'Booking Status has been changed';
				$message = JText::sprintf('PUSHNOTIFICATION_TYPE_REQUESTSTATUSCHANGED_MESSAGE_IPN',$changedFor);

				$jsonarray['pushNotificationData']['id']         = $obj->id;
				$jsonarray['pushNotificationData']['to']         = $toUserID;
				$jsonarray['pushNotificationData']['message']    = $message;
				if($elementType == 'package')
				{
					$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEPAYMENTRECEIVED'); //'PackagePaymentReceived';
				}
				else
				{
					$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_REQUESTSTATUSCHANGED'); //'RequestStatusChanged';
				}

				$jsonarray['pushNotificationData']['configtype'] = '';


				/*echo "<pre>";
				print_r($jsonarray);
				echo "</pre>";
				exit;*/

				mail('bcted.developer@gmail.com', 'Payment Push notification', json_encode($jsonarray));

				$this->goForPushNotification($jsonarray);
			}

		}

		/*echo "<pre>";
		print_r($bookingData);
		echo "</pre>";*/


		//require_once 'file';
		//$bookingData = $this->helper->getBookingDetailForVenueTable($$tblPaymentStatus->booked_element_id,'Venue');
	}

	public function sendMessageForPackageService($packageID,$BookedID)
	{
		//$toUserID = 114;
		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$bctHelper            = new bctedAppHelper;
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');

		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable',array());
		$tblPackagePurchased->load($BookedID);
		$tblPackage = JTable::getInstance('Package', 'BctedTable',array());
		$tblPackage->load($packageID);

		/*echo "<pre>";
		print_r($tblPackage);
		echo "</pre>";
		exit;*/

		$serviceIDs = $tblPackage->company_ids;
		$companyArray= array();
		if(!empty($serviceIDs))
		{
			$serviceArray = explode(",", $serviceIDs);
			foreach ($serviceArray as $key => $service)
			{
				$tblService = JTable::getInstance('Service', 'BctedTable',array());
				$tblService->load($service);
				if($tblService->service_id)
				{
					$tmpArray['serviceName'] = $tblService->service_name;
					$companyArray[$tblService->company_id][] = $tmpArray;

					//$bctHelper->sendMessage(0,$tblService->company_id,$tblService->service_id,0,$tblService->user_id,$message,$extraParam = array(),$messageType='tableaddme');
				}
			}

			/*echo "<pre>";
			print_r($companyArray);
			echo "</pre>";
			exit;*/

			if(count($companyArray)!=0)
			{
				foreach ($companyArray as $companyID => $servicesArray)
				{
					$servicesNames = array();
					foreach ($servicesArray as $key => $services)
					{
						$servicesNames[] = $services['serviceName'];
					}

					$tblCompany = JTable::getInstance('Company', 'BctedTable',array());
					$tblCompany->load($companyID);
					$user = JFactory::getUser();
					$message = $tblPackage->package_name.' is booked by '.$user->name.' for your '.implode(",", $servicesNames).' at '.$tblPackagePurchased->package_location.' on '.$tblPackagePurchased->package_datetime.':'.$tblPackagePurchased->package_time;
					/*echo "<pre>";
					print_r($tblCompany);
					echo "</pre>";*/
					//exit;
					$bctHelper->sendMessage(0,$tblCompany->company_id,0,0,$tblCompany->userid,$message,array(),$messageType='packageservicebooking');
				}

				//exit;
			}
		}




	}

	public function goForPushNotification($jsonarray)
	{
		if (!empty($jsonarray['pushNotificationData']))
		{
			require_once JPATH_SITE.'/components/com_ijoomeradv/models/ijoomeradv.php';
			require_once JPATH_SITE.'/components/com_ijoomeradv/helpers/helper.php';

			$ijoomeradvModel = new IjoomeradvModelijoomeradv();
			$result = $ijoomeradvModel->getApplicationConfig();

			foreach ($result as $value)
			{
				defined($value->name) or define($value->name, $value->value);
			}


			$db = JFactory::getDbo();

			$memberlist = $jsonarray['pushNotificationData']['to'];

			if ($memberlist)
			{
				$query = $db->getQuery(true);

				// Create the base select statement.
				$query->select('a.userid, a.jomsocial_params, a.device_token, a.device_type')
					->from($db->qn('#__ijoomeradv_users','a'))
					->where($db->qn('a.userid') . ' = ' . $db->q($memberlist));
				$query->select('b.user_type_for_push AS user_type')
					->join('LEFT','#__bcted_user_profile AS b ON b.userid=a.userid');

				// Set the query and load the result.
				$db->setQuery($query);

				$puserlist = $db->loadObjectList();


				foreach ($puserlist as $puser)
				{
					// Check config allow for jomsocial
					/*echo "<pre>";
					print_r($puser);
					echo "</pre>";*/

					if (!empty($jsonarray['pushNotificationData']['configtype']) and $jsonarray['pushNotificationData']['configtype'] != '')
					{
						$ijparams = json_decode($puser->jomsocial_params);
						$configallow = $jsonarray['pushNotificationData']['configtype'];
					}
					else
					{
						$configallow = 1;
					}

					if ($configallow && !empty($puser))
					{
						/*$params = json_decode($puser->params);

						if(!$params->pushNotification->updateMyBookingStatus)
						{
							continue;
						}*/

						if (IJOOMER_PUSH_ENABLE_IPHONE == 1 && $puser->device_type == 'iphone')
						{
							$options = array();
							$options['device_token'] = $puser->device_token;
							$options['live'] = intval(IJOOMER_PUSH_DEPLOYMENT_IPHONE);
							$options['aps']['alert'] = strip_tags($jsonarray['pushNotificationData']['message']);
							$options['aps']['type'] = $jsonarray['pushNotificationData']['type'];
							$options['aps']['id'] = ($jsonarray['pushNotificationData']['id'] != 0) ? $jsonarray['pushNotificationData']['id'] : $jsonarray['pushNotificationData']['multiid'][$puser->userid];
							IJPushNotif::sendIphonePushNotification($options,$puser->user_type);
						}

						if (IJOOMER_PUSH_ENABLE_ANDROID == 1 && $puser->device_type == 'android')
						{
							$options = array();
							$options['registration_ids'] = array($puser->device_token);
							$options['data']['message'] = strip_tags($jsonarray['pushNotificationData']['message']);
							$options['data']['type'] = $jsonarray['pushNotificationData']['type'];
							$options['data']['id'] = ($jsonarray['pushNotificationData']['id'] != 0) ? $jsonarray['pushNotificationData']['id'] : $jsonarray['pushNotificationData']['multiid'][$puser->userid];
							IJPushNotif::sendAndroidPushNotification($options);
						}
					}
				}
			}

			unset($jsonarray['pushNotificationData']);
		}
	}
}
