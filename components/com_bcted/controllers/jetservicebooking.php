<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerJetServiceBooking extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'JetServiceBooking', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}


	public function bookJetService()
	{
		$app  = JFactory::getApplication();
		$input = $app->input;

		$menu = $app->getMenu();

		$company_id       = $input->get('company_id',0,'int');
		$date_of_flight   = $input->get('date_of_flight','','string');
		$flying_from      = $input->get('flying_from','','string');
		$flying_to        = $input->get('flying_to','','string');
		$additional_stops = $input->get('additional_stops','','string');
		$number_of_people      = $input->get('guest_count',0,'int');
		$additional_info  = $input->get('additional_info','','string');

		$user = JFactory::getUser();

		/*$menuItem = BctedHelper::getBctedMenuItem('user-login');
		$Itemid = $menuItem->id;*/

		$menuItem = $menu->getItems( 'link', 'index.php?option=com_users&view=login', true );
		$Itemid = $menuItem->id;
		$link = $menuItem->link.'&Itemid='.$Itemid;

		if(!$user->id)
		{
			$msg = JText::_('COM_BCTED_USER_SESSTION_NOT_FOUND');
			$app->redirect($link,$msg);
		}

		/*$menuItem = BctedHelper::getBctedMenuItem('user-company-services');
		$Itemid =$menuItem->id;*/

		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=companyinformation', true );
		$Itemid = $menuItem->id;
		$link = 'index.php?option=com_bcted&view=companyinformation'.'&company_id='.$company_id.'&Itemid='.$Itemid;

		/*echo $company_id . "||". $date_of_flight." || ".$number_of_people;
		exit;*/

		if(!$company_id || empty($date_of_flight) || !$number_of_people)
		{
			$msg = JText::_('COM_BCTED_VENUE_TABLE_BOOKING_REQUST_INVALID_DATA');
			$app->redirect($link,$msg);
		}

		$model = $this->getModel();
		$tblCompany = $model->getTable('Company');
		$tblCompany->load($company_id);

		$postData['company_id']       = $company_id;
		$postData['date_of_flight']   = $date_of_flight;
		$postData['user_id']          = $user->id;
		$postData['flying_from']      = $flying_from;
		$postData['flying_to']        = $flying_to;
		$postData['additional_stops'] = $additional_stops;
		$postData['number_of_people'] = $number_of_people;
		$postData['additional_info']  = $additional_info;

		$userDetail = $this->getServiceDetail();
		$postData['parent_id']        = 0;
		$postData['name']             = $userDetail->name.' '.$userDetail->last_name;
		$postData['email']            = $userDetail->email;
		$postData['phone']            = $userDetail->phoneno;
		$postData['time_stamp'] = time();
		$postData['created'] = date('Y-m-d H:i:s');

		$response = $model->bookJetService($postData);

		if($response)
		{
			// Initialise variables.
		$app     = JFactory::getApplication();
		$config  = JFactory::getConfig();

		$site    = $config->get('sitename');
		$from    = $config->get('mailfrom');
		$sender  = $config->get('fromname');
		$emailToSend   = $tblCompany->email_address; //$app->input->get('mailto');
		$subject = JText::_('COM_BESEATED_COMPANY_BOOK_PRIVATE_JET_SERVICE_EMAIL_SUBJECT'); //$app->input->get('subject');

		/*<thead>
				<tr>
					<td>Name</td>
					<td>Email</td>
					<td>Phone</td>
				</tr>
			</thead>*/
		$htmlDetail = '
		<table  align="left">
			<tr>
				<td>Name:</td>
				<td>'.$userDetail->name.' '.$userDetail->last_name.'</td>
			</tr>
			<tr>
				<td>Email:</td>
				<td>'.$userDetail->email.'</td>
			</tr>
			<tr>
				<td>Phone:</td>
				<td>'.$userDetail->phoneno.'</td>
			</tr>
			<tr>
				<td>Date of	flight:</td>
				<td>'.date('d-m-Y',strtotime($date_of_flight)).'</td>
			</tr>
			<tr>
				<td>Flying From:</td>
				<td>'.$flying_from.'</td>
			</tr>
			<tr>
				<td>Flying To:</td>
				<td>'.$flying_to.'</td>
			</tr>
			<tr>
				<td>Additional Stops:</td>
				<td>'.$additional_stops.'</td>
			</tr>
			<tr>
				<td>Number of people:</td>
				<td>'.$number_of_people.'</td>
			</tr>
			<tr>
				<td>Additional Information:</td>
				<td>'.$additional_info.'</td>
			</tr>
		</table>';

		$user= JFactory::getUser();

		// Build the message to send.
		$msg     = JText::_('COM__SEND_EMAIL_MSG');
		$msg = JText::sprintf('COM_BESEATED_COMPANY_BOOK_PRIVATE_JET_SERVICE_EMAIL_BODY',$user->name,$htmlDetail);
		$body    = sprintf($msg, $site, $sender, $from);

		// Clean the email data.
		$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $emailToSend, $subject, $body,true);

			$msg = JText::_('COM_BCTED_CLUB_SERVICE_BOOKING_REQUST_SUCCESS');
			$app->redirect($link,$msg);
		}
		else
		{
			$msg = JText::_('COM_BCTED_CLUB_SERVICE_BOOKING_REQUST_ERRORS');
			$app->redirect($link,$msg);
		}

		return true;
	}

	public function getServiceDetail()
	{
		$user = JFactory::getUser();

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('id,name,username,email')
			->from($db->quoteName('#__users','u'))
			->where($db->quoteName('id') . ' = ' . $db->quote($user->id));

		$query->select('last_name,phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=u.id');

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObject();
		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/

		return $result;
	}


}
