<?php
/**
 * @package     Diff.Site
 * @subpackage  com_Diff
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Diff Order Controller
 *
 * @since  0.0.1
 */
class BctedControllerPackageorder extends JControllerForm
{
	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   string  $name  	 The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array  	$config  Configuration array for model. Optional.
	 *
	 * @return object The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = '', $prefix = '', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	/**
	 * Method to Cancel event creation action
	 *
	 * @param   int  $key  key column for table
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	public function cancel($key = 'dish_id')
	{
		$app = JFactory::getApplication();
		$app->redirect('index.php?option=com_diff&view=frontdishes');
		$app->close();
	}

	/**
	 * makeOffer to particular restaurant.
	 *
	 * @since   0.0.1
	 */
	public function makeOrder()
	{
		$app   = JFactory::getApplication();
		$model = $this->getModel('Order', 'DiffModel');

		$result = $model->makeOrder();

		echo $result;
		$app->close();
		//return $result;

		/*$app->redirect('index.php?option=com_diff&view=frontdishes&Itemid=106');
		$app->close();*/
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function packagePurchased()
	{

		JPluginHelper::importPlugin('bct');
		$dispatcher = JDispatcher::getInstance('paypal');
		$results = $dispatcher->trigger('onPreparePaymentPaypal', array());
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function packageInvitePayment()
	{
		/*echo "call";
		exit;*/
		JPluginHelper::importPlugin('bct');
		$dispatcher = JDispatcher::getInstance('paypal');
		$results = $dispatcher->trigger('onPackageInvitePaymentPaypal', array());
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function placeRequest()
	{
		JPluginHelper::importPlugin('dreamyFlavours');
		$dispatcher = JDispatcher::getInstance('paypal');
		$results = $dispatcher->trigger('onPreparePaymentPaypalForRequest', array());
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function paypalresponse()
	{

	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function paymentCancel()
	{

	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function paymentReturn()
	{
		$app->redirect("index.php?option=com_diff&view=dish");
		$app->close();

	}

	public function ipninvite()
	{
		mail("fbbcted.developer@gmail.com", "Get", json_encode($_GET));
		mail("fbbcted.developer@gmail.com", "Post", json_encode($_POST));
		mail("fbbcted.developer@gmail.com", "Request", json_encode($_REQUEST));

		$app = JFactory::getApplication();
		$input = $app->input;
		$paymentEntryID = $input->get('payment_entry_id',0,'int');

		$tblPaymentStatus = JTable::getInstance('PaymentStatus', 'BctedTable',array());
		$tblPaymentStatus->load($paymentEntryID);

		/*echo "<pre>";
		print_r($tblPaymentStatus);
		echo "</pre>";
		exit;*/

		if(!$tblPaymentStatus->payment_id)
		{
			return;
		}

		$elementID = $tblPaymentStatus->booked_element_id;
		$elementType = $tblPaymentStatus->booked_element_type;

		$paymentGross = $input->get('payment_gross',0.00);
		$paymentFee   = $input->get('payment_fee',0.00);
		$txnID        = $input->get('txn_id','','string');
		$paymentStatusText  = $input->get('payment_status','','string');

		$tblPaymentStatus->payment_fee = $paymentFee;
		$tblPaymentStatus->txn_id = $txnID;
		$tblPaymentStatus->payment_status = $paymentStatusText;

		if($paymentStatusText == 'Completed')
		{
			//$tblPaymentStatus->paid_status = 1;
			/*$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
			$tblPackagePurchased->load($elementID);
			$tblPackagePurchased->status = 5;
			$tblPackagePurchased->user_status = 5;
			$tblPackagePurchased->store();*/

			$tblPackageInvite = JTable::getInstance('PackageInvite', 'BctedTable');
			$tblPackageInvite->load($elementID);

			$tblPackageInvite->payment_status = strtolower($paymentStatusText);
			$tblPackageInvite->status = 5;
			$tblPackageInvite->store();

			$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
			$tblPackagePurchased->load($tblPackageInvite->package_purchase_id);

			if($this->checkForInvitedPaymentDone($tblPackageInvite->package_id,$tblPackageInvite->package_purchase_id))
			{
				$tblPackagePurchased->status = 6;
				$tblPackagePurchased->user_status = 6;
			}
			else
			{
				$tblPackagePurchased->status = 5;
				$tblPackagePurchased->user_status = 5;
			}

			$tblPackagePurchased->store();


			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_loyalty_point'))
				->set($db->quoteName('is_valid') . ' = ' . $db->quote(1))
				->where($db->quoteName('cid') . ' = ' . $db->quote($paymentEntryID));

			// Set the query and execute the update.
			$db->setQuery($query);
			$db->execute();

			$tblVenue = JTable::getInstance('Venue', 'BctedTable');
			$tblVenue->load($tblPaymentStatus->venue_id);
			$toUserID = $tblVenue->userid;

			$this->sendPushNotification($toUserID,$elementID,$elementType);


		}

		$tblPaymentStatus->store();
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function ipn()
	{
		mail("bcted.developer@gmail.com", "Get ".time(), json_encode($_GET));
		mail("bcted.developer@gmail.com", "Post ".time(), json_encode($_POST));
		mail("bcted.developer@gmail.com", "Request ".time(), json_encode($_REQUEST));

		/*$model = $this->getModel();

		$model->savePaymentData()*/

		$app = JFactory::getApplication();

		$input = $app->input;

		$paymentEntryID = $input->get('payment_entry_id',0,'int');
		/*
		{
		  "option": "com_bcted",
		  "order_id": "8",
		  "task": "ipnpackage",
		  "mc_gross": "3.00",
		  "protection_eligibility": "Eligible",
		  "address_status": "confirmed",
		  "payer_id": "PKCET9PAVETAW",
		  "tax": "0.00",
		  "address_street": "1 Main St",
		  "payment_date": "02:39:39 Feb 09, 2015 PST",
		  "payment_status": "Completed",
		  "charset": "windows-1252",
		  "address_zip": "95131",
		  "first_name": "guest",
		  "mc_fee": "0.42",
		  "address_country_code": "US",
		  "address_name": "guest one",
		  "notify_version": "3.8",
		  "custom": "",
		  "payer_status": "verified",
		  "business": "packageowner@gamil.com",
		  "address_country": "United States",
		  "address_city": "San Jose",
		  "quantity": "1",
		  "verify_sign": "AAYxTx9oUjWVyxCW6Z8au0jbGj9NAayS2ToFtxr0NbtTdHtXkQ1hEaOd",
		  "payer_email": "guestone@gmail.com",
		  "txn_id": "57A695569B3796228",
		  "payment_type": "instant",
		  "last_name": "one",
		  "address_state": "CA",
		  "receiver_email": "packageowner@gamil.com",
		  "payment_fee": "0.42",
		  "receiver_id": "WG7QNN4ABAP8C",
		  "txn_type": "web_accept",
		  "item_name": "limo service",
		  "mc_currency": "USD",
		  "item_number": "5",
		  "residence_coun try": "US",
		  "test_ipn": "1",
		  "handling_amount": "0.00",
		  "transaction_subject": "",
		  "payment_gross": "3.00",
		  "shipping": "0.00",
		  "ipn_track_id": "fe6ea83fdc45e",
		  "Itemid": null
		} */

		$tblPaymentStatus = JTable::getInstance('PaymentStatus', 'BctedTable',array());
		$tblPaymentStatus->load($paymentEntryID);

		/*echo "<pre>";
		print_r($tblPaymentStatus);
		echo "</pre>";
		exit;*/

		if(!$tblPaymentStatus->payment_id)
		{
			return;
		}

		$elementID = $tblPaymentStatus->booked_element_id;
		$elementType = $tblPaymentStatus->booked_element_type;

		$paymentGross = $input->get('payment_gross',0.00);
		$paymentFee   = $input->get('payment_fee',0.00);
		$txnID        = $input->get('txn_id','','string');
		$paymentStatusText  = $input->get('payment_status','','string');

		$tblPaymentStatus->payment_fee = $paymentFee;
		$tblPaymentStatus->txn_id = $txnID;
		$tblPaymentStatus->payment_status = $paymentStatusText;

		if($paymentStatusText == 'Completed')
		{
			$tblPaymentStatus->paid_status = 1;

			if($elementType == 'service')
			{
				$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');
				$tblServiceBooking->load($elementID);

				$tblCompany = JTable::getInstance('Company', 'BctedTable');
				$tblCompany->load($tblServiceBooking->company_id);
				$toUserID = $tblCompany->userid;

				$this->sendPushNotification($toUserID,$elementID,$elementType);

				$tblServiceBooking->status = 5;
				$tblServiceBooking->user_status = 5;

				$tblServiceBooking->total_price = $tblPaymentStatus->element_price;
				$tblServiceBooking->deposit_amount = $tblPaymentStatus->platform_income;
				$tblServiceBooking->amount_payable = $tblPaymentStatus->element_price - $tblPaymentStatus->platform_income;
				$tblServiceBooking->deposit_paid_date = date('Y-m-d');
				$tblServiceBooking->store();

				$this->checkForFirstPurchase($tblServiceBooking->user_id);
			}
			else if($elementType == 'venue')
			{
				$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');
				$tblVenuebooking->load($elementID);

				$tblVenue = JTable::getInstance('Venue', 'BctedTable');
				$tblVenue->load($tblVenuebooking->venue_id);
				$toUserID = $tblVenue->userid;

				$this->sendPushNotification($toUserID,$elementID,$elementType);

				$tblVenuebooking->status = 5;
				$tblVenuebooking->user_status = 5;

				$tblVenuebooking->total_price = $tblPaymentStatus->element_price;
				$tblVenuebooking->deposit_amount = $tblPaymentStatus->platform_income;
				$tblVenuebooking->amount_payable = $tblPaymentStatus->element_price - $tblPaymentStatus->platform_income;
				$tblVenuebooking->deposit_paid_date = date('Y-m-d');
				$tblVenuebooking->store();

				$this->checkForFirstPurchase($tblVenuebooking->user_id);


			}
			else if($elementType == 'package')
			{
				$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
				$tblPackagePurchased->load($elementID);

				$elementID = $tblPaymentStatus->booked_element_id;

				$invited_ids = $input->get('invited_ids','','string');

				if(!empty($invited_ids))
				{
					// Initialiase variables.
					$db    = JFactory::getDbo();
					$query = $db->getQuery(true);

					// Create the base update statement.
					$query->update($db->quoteName('#__bcted_package_invite'))
						->set($db->quoteName('status') . ' = ' . $db->quote(5))
						->set($db->quoteName('payment_status') . ' = ' . $db->quote('Completed'))
						->where($db->quoteName('package_invite_id') . ' IN (' . $invited_ids . ')');

					/*echo $query->dump();
					exit;*/

					// Set the query and execute the update.
					$db->setQuery($query);

					$db->execute();

				}

				if($this->checkForInvitedPaymentDone($tblPackagePurchased->package_id,$tblPackagePurchased->package_purchase_id))
				{
					$tblPackagePurchased->status = 6;
					$tblPackagePurchased->user_status = 6;
				}
				else
				{
					$tblPackagePurchased->status = 5;
					$tblPackagePurchased->user_status = 5;
				}


				$tblPackagePurchased->store();

				$this->checkForFirstPurchase($tblPackagePurchased->user_id);

				$tblVenue = JTable::getInstance('Venue', 'BctedTable');
				$tblCompany = JTable::getInstance('Company', 'BctedTable');
				$tblVenue->load($tblPackagePurchased->venue_id);
				$tblCompany->load($tblPackagePurchased->company_id);

				if($packagePurchased->venue_id)
				{
					$toUserID = $tblVenue->userid;
				}
				else if($packagePurchased->company_id)
				{
					$toUserID = $tblCompany->userid;
				}



				$this->sendPushNotification($toUserID,$elementID,$elementType);
			}

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_loyalty_point'))
				->set($db->quoteName('is_valid') . ' = ' . $db->quote(1))
				->where($db->quoteName('cid') . ' = ' . $db->quote($paymentEntryID));

			// Set the query and execute the update.
			$db->setQuery($query);
			$db->execute();


		}

		$tblPaymentStatus->store();
		/*if($tblPaymentStatus->store())
		{
			$jsonarray['code']= 200;
			echo json_encode($jsonarray);
			exit;
		}
		else
		{
			$jsonarray['code']= 500;
			echo json_encode($jsonarray);
			exit;
		}*/

		/*echo "called";
		exit;*/


	}

	public function checkForInvitedPaymentDone($packageID,$packagePurchaseID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_package_invite'))
			->where($db->quoteName('package_id') . ' = ' . $db->quote($packageID))
			->where($db->quoteName('package_purchase_id') . ' = ' . $db->quote($packagePurchaseID))
			->where($db->quoteName('status') . ' = ' . $db->quote('2'));
		//echo $query->dump();
		/*exit;*/

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/

		if($result)
		{
			return 1;
		}

		return 0;
	}

	public function checkForFirstPurchase($userID)
	{
		$fpUser = JFactory::getUser($userID);
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_user_refer'))
			->where($db->quoteName('is_registered') . ' = ' . $db->quote('1'))
			->where($db->quoteName('ref_user_id') . ' = ' . $db->quote($fpUser->id))
			->where($db->quoteName('refer_email') . ' = ' . $db->quote($fpUser->email))
			->order($db->quoteName('time_stamp') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);
		$userReferObj = $db->loadObject();

		if($userReferObj)
		{
			JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
			$tblRefer = JTable::getInstance('Refer', 'BctedTable');
			$tblRefer->load($userReferObj->refer_id);

			$bctParams = $this->getExtensionParam();
			$points = ($bctParams->friends_referral)?$bctParams->friends_referral:50;

			$tblLoyaltyPoint = JTable::getInstance('LoyaltyPoint', 'BctedTable');
			$tblLoyaltyPoint->load(0);

			$lpPost['user_id']    = $userReferObj->userid;
			$lpPost['earn_point'] = $points;
			$lpPost['point_app']  = 'inviteduserfp';
			$lpPost['cid']        = $userReferObj->refer_id;
			$lpPost['is_valid']   = '1';
			$lpPost['created']    = date('Y-m-d H:i:s');
			$lpPost['time_stamp'] = time();


			$tblLoyaltyPoint->bind($lpPost);

			if($tblLoyaltyPoint->store())
			{
				$tblRefer->is_fp_done = 1;
				$tblRefer->fp_date = date('Y-m-d');
				if($tblRefer->store())
				{
					$query = $db->getQuery(true);

					// Create the base delete statement.
					$query->delete()
						->from($db->quoteName('#__bcted_user_refer'))
						->where($db->quoteName('ref_user_id') . ' = ' . $db->quote((int) $userReferObj->ref_user_id))
						->where($db->quoteName('refer_email') . ' = ' . $db->quote((int) $userReferObj->ref_user_id))
						->where($db->quoteName('refer_id') . ' <> ' . $db->quote((int) $userReferObj->refer_id));

					// Set the query and execute the delete.
					$db->setQuery($query);
					$db->execute();
				}
			}
		}


	}

	public function getExtensionParam()
	{
		$app    = JFactory::getApplication();

		$option = "com_bcted";
		$db     = JFactory::getDbo();

		$option = '%' . $db->escape($option, true) . '%';

		// Initialiase variables.
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->qn('#__extensions'))
			->where($db->qn('name') . ' LIKE ' . $db->q($option))
			->where($db->qn('type') . ' = ' . $db->q('component'))
			->order($db->qn('ordering') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadObject();

			$params = json_decode($result->params);
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), 500);
		}

		return $params;
	}

	public function sendPushNotification($toUserID,$BookedElementID,$elementType)
	{
		//$toUserID = 114;
		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$bctHelper            = new bctedAppHelper;

		$pushcontentdata=array();

		if($elementType == 'venue')
		{
			$bookingData = $bctHelper->getBookingDetailForVenueTable($BookedElementID,'Venue');
			/*echo "<pre>";
			print_r($bookingData);
			echo "</pre>";
			exit;*/
			$pushcontentdata['elementType'] = "Venue";
		}
		else if($elementType == 'service')
		{
			$bookingData = $bctHelper->getBookingDetailForServices($BookedElementID,'Service');
			$pushcontentdata['elementType'] = "Company";
		}
		else if($elementType == 'package')
		{
			$bookingData = $bctHelper->getBookingDetailForPackage($BookedElementID,'Venue');
			$pushcontentdata['elementType'] = "Venue";
		}

		$pushcontentdata['data'] = $bookingData;


		$pushOptions['detail']['content_data'] = $pushcontentdata;

		/*echo "<pre>";
		print_r($pushOptions);
		echo "</pre>";
		exit;*/
		$pushOptions = gzcompress(json_encode($pushOptions));

		$db = JFactory::getDbo();

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');

		/*echo "<pre>";
		print_r($obj);
		echo "</pre>";
		exit;*/

		if($obj->id)
		{
			$userProfile    = $bctHelper->getUserProfile($toUserID);

			/*echo "<pre>";
			print_r($userProfile);
			echo "</pre>";
			exit;*/
			$updateMyBookingStatus = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$updateMyBookingStatus = $params->settings->pushNotification->updateMyBookingStatus;

				//$updateMyBookingStatus = 1;
			}

			/*echo $updateMyBookingStatus;
			exit;*/

			if($updateMyBookingStatus)
			{
				$toUserDetail = JFactory::getUser($toUserID);
				$message = 'Booking Status has been changed';

				$jsonarray['pushNotificationData']['id']         = $obj->id;
				$jsonarray['pushNotificationData']['to']         = $toUserID;
				$jsonarray['pushNotificationData']['message']    = $message;
				if($elementType == 'package')
				{
					$jsonarray['pushNotificationData']['type']       = 'PackagePaymentReceived';
				}
				else
				{
					$jsonarray['pushNotificationData']['type']       = 'RequestStatusChanged';
				}

				$jsonarray['pushNotificationData']['configtype'] = '';


				/*echo "<pre>";
				print_r($jsonarray);
				echo "</pre>";
				exit;*/

				mail('bcted.developer@gmail.com', 'Payment Push notification', json_encode($jsonarray));

				$this->goForPushNotification($jsonarray);
			}

		}

		/*echo "<pre>";
		print_r($bookingData);
		echo "</pre>";*/


		//require_once 'file';
		//$bookingData = $this->helper->getBookingDetailForVenueTable($$tblPaymentStatus->booked_element_id,'Venue');
	}

	public function goForPushNotification($jsonarray)
	{
		if (!empty($jsonarray['pushNotificationData']))
		{
			require_once JPATH_SITE.'/components/com_ijoomeradv/models/ijoomeradv.php';
			require_once JPATH_SITE.'/components/com_ijoomeradv/helpers/helper.php';

			$ijoomeradvModel = new IjoomeradvModelijoomeradv();
			$result = $ijoomeradvModel->getApplicationConfig();

			foreach ($result as $value)
			{
				defined($value->name) or define($value->name, $value->value);
			}


			$db = JFactory::getDbo();

			$memberlist = $jsonarray['pushNotificationData']['to'];

			if ($memberlist)
			{
				$query = $db->getQuery(true);

				// Create the base select statement.
				$query->select('a.userid, a.jomsocial_params, a.device_token, a.device_type')
					->from($db->qn('#__ijoomeradv_users','a'))
					->where($db->qn('a.userid') . ' = ' . $db->q($memberlist));
				$query->select('b.user_type_for_push AS user_type')
					->join('LEFT','#__bcted_user_profile AS b ON b.userid=a.userid');

				// Set the query and load the result.
				$db->setQuery($query);

				$puserlist = $db->loadObjectList();


				foreach ($puserlist as $puser)
				{
					// Check config allow for jomsocial
					/*echo "<pre>";
					print_r($puser);
					echo "</pre>";*/

					if (!empty($jsonarray['pushNotificationData']['configtype']) and $jsonarray['pushNotificationData']['configtype'] != '')
					{
						$ijparams = json_decode($puser->jomsocial_params);
						$configallow = $jsonarray['pushNotificationData']['configtype'];
					}
					else
					{
						$configallow = 1;
					}

					if ($configallow && !empty($puser))
					{
						/*$params = json_decode($puser->params);

						if(!$params->pushNotification->updateMyBookingStatus)
						{
							continue;
						}*/

						if (IJOOMER_PUSH_ENABLE_IPHONE == 1 && $puser->device_type == 'iphone')
						{
							$options = array();
							$options['device_token'] = $puser->device_token;
							$options['live'] = intval(IJOOMER_PUSH_DEPLOYMENT_IPHONE);
							$options['aps']['alert'] = strip_tags($jsonarray['pushNotificationData']['message']);
							$options['aps']['type'] = $jsonarray['pushNotificationData']['type'];
							$options['aps']['id'] = ($jsonarray['pushNotificationData']['id'] != 0) ? $jsonarray['pushNotificationData']['id'] : $jsonarray['pushNotificationData']['multiid'][$puser->userid];
							IJPushNotif::sendIphonePushNotification($options,$puser->user_type);
						}

						if (IJOOMER_PUSH_ENABLE_ANDROID == 1 && $puser->device_type == 'android')
						{
							$options = array();
							$options['registration_ids'] = array($puser->device_token);
							$options['data']['message'] = strip_tags($jsonarray['pushNotificationData']['message']);
							$options['data']['type'] = $jsonarray['pushNotificationData']['type'];
							$options['data']['id'] = ($jsonarray['pushNotificationData']['id'] != 0) ? $jsonarray['pushNotificationData']['id'] : $jsonarray['pushNotificationData']['multiid'][$puser->userid];
							IJPushNotif::sendAndroidPushNotification($options);
						}
					}
				}
			}

			unset($jsonarray['pushNotificationData']);
		}
	}
}
