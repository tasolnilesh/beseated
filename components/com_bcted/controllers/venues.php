<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Categories Controller
 *
 * @since  0.0.1
 */
class BctedControllerVenues extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'Venue', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	/**
	 * Function to delete Category
	 *
	 * @return  boolean
	 *
	 * @since   0.0.1
	 */
	public function addtofavourite()
	{

		// Get items to remove from the request.
		$venueID = JFactory::getApplication()->input->get('venue_id', 0, 'int');
		$userID = JFactory::getApplication()->input->get('user_id', 0, 'int');

		if (!$venueID || !$userID)
		{
			//JLog::add(JText::_($this->text_prefix . '_NO_ITEM_SELECTED'), JLog::WARNING, 'jerror');
			echo "3";
			exit;
		}
		else
		{

			// Get the model.
			$model = $this->getModel();
			$result = $model->addtofavourite($venueID,$userID);

			echo $result;
			exit;

			//if ($result)
				//JFactory::getApplication()->enqueueMessage('Library deleted');
			//$this->setRedirect('index.php?option=com_heartdart&view=libraries');
		}

		echo 0;
		exit;
	}

	/**
	 * Function to delete Category
	 *
	 * @return  boolean
	 *
	 * @since   0.0.1
	 */
	public function removefromfavourite()
	{

		// Get items to remove from the request.
		$venueID = JFactory::getApplication()->input->get('venue_id', 0, 'int');
		$userID = JFactory::getApplication()->input->get('user_id', 0, 'int');

		if (!$venueID || !$userID)
		{
			//JLog::add(JText::_($this->text_prefix . '_NO_ITEM_SELECTED'), JLog::WARNING, 'jerror');
			echo "3";
			exit;
		}
		else
		{

			// Get the model.
			$model = $this->getModel();
			$result = $model->removefromfavourite($venueID,$userID);

			echo $result;
			exit;

			//if ($result)
				//JFactory::getApplication()->enqueueMessage('Library deleted');
			//$this->setRedirect('index.php?option=com_heartdart&view=libraries');
		}

		echo 0;
		exit;
	}

}
