<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Controller
 *
 * @since  0.0.1
 */
class BctedControllerUserProfile extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'UserProfile', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function save()
	{
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$input = $app->input;
		$data = $input->get('jform',array(),'array');



		$email = trim($data['email']);
		$password = trim($data['password']);
		$password2 = trim($data['password2']);
		$flg = 0;

		if(!empty($password) && !empty($password2))
		{
			if($password === $password2)
			{
				$flg = 1;
			}

			if($flg == 0)
			{
				$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=userprofile', true );

				$profileLink = $menuItem->link.'&Itemid='.$menuItem->id;
				$app->redirect($profileLink,JText::_('COM_BCTED_USER_PROFILE_NOT_UPDATED'));
			}
		}

		/*echo $flg;
		exit;*/



		$model = $this->getModel();
		$response = $model->save($data);

		/*echo $response;
		exit;*/

		$profileMenu = BctedHelper::getBctedMenuItem('user-profile');

		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=userprofile', true );

		$profileLink = $menuItem->link.'&Itemid='.$menuItem->id;

		if($response ==1)
		{
			$app->redirect($profileLink,JText::_('COM_BCTED_USER_PROFILE_UPDATED_SUCCESSFULLY'));
			//exit;
		}
		else if($response == 709)
		{
			$app->redirect($profileLink,JText::_('COM_BCTED_REGISTRATION_PHONE_NUMBER_ALREADY_REGISTERED'));
			//exit;
		}
		else if($response == 701)
		{
			$app->redirect($profileLink,JText::_('COM_BCTED_REGISTRATION_USERNAME_ALREADY_REGISTERED'));
			//exit;
		}
		else if($response == 702)
		{
			$app->redirect($profileLink,JText::_('COM_BCTED_REGISTRATION_EMAIL_ALREADY_REGISTERED'));
			//exit;
		}
		else
		{
			$app->redirect($profileLink,JText::_('COM_BCTED_USER_PROFILE_NOT_UPDATED'));
			//exit;
		}

		/*echo $response . " End of registration";
		exit;*/

		// 709 Phone number exists
		// 701 username exists
		// 702 email
		// 500 server error

	}

	public function updateAverageRatingOfVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('rating')
			->from($db->quoteName('#__bcted_ratings'))
			->where($db->quoteName('rating_type') . ' = ' . $db->quote('venue'))
			->where($db->quoteName('rated_id') . ' = ' . $db->quote($venueID));

		// Set the query and load the result.
		$db->setQuery($query);

		$rating = $db->loadColumn();

		$sum = array_sum($rating);

		$totalEntry = count($rating);

		$avg = $sum / $totalEntry;

		$avg = number_format($avg , 2);

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($venueID);
		$tblVenue->venue_rating = $avg;
		//$tblVenue->rating = "1";

		$tblVenue->store();

	}

	public function updateAverageRatingOfCompany($companyID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('rating')
			->from($db->quoteName('#__bcted_ratings'))
			->where($db->quoteName('rating_type') . ' = ' . $db->quote('service'))
			->where($db->quoteName('rated_id') . ' = ' . $db->quote($companyID));

		// Set the query and load the result.
		$db->setQuery($query);

		$rating = $db->loadColumn();

		$sum = array_sum($rating);

		$totalEntry = count($rating);

		$avg = $sum / $totalEntry;

		$avg = number_format($avg , 2);

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblCompany->load($companyID);
		$tblCompany->company_rating = $avg;
		//$tblCompany->rating = "1";
		/*echo "<pre>";
		print_r($tblCompany);
		echo "</pre>";
		exit;*/
		$tblCompany->store();

	}

	public function giverating()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		/*
			[option] => com_bcted
			[task] => giverating
			[rate_value] => 3
			[element_type] => venue
			[rated_user_id] => 142
			[booking_id] => 22
			[Itemid] =>
			"rate_value="+rateValue+"&element_type="+elementType+"&rated_user_id="+ratedUserID+"&booking_id="+bookingID+
			"&user_message="+userMessage+"&element_id="+elementID,
		 */

		$rating      = $input->get('rate_value',0,'int');
		$bookingID   = $input->get('booking_id',0,'int');
		$elementID   = $input->get('element_id',0,'int');
		$userID      = $input->get('rated_user_id',0,'int');
		$elementType = $input->get('element_type','','string');
		$comment     = $input->get('user_message','','string');

		if(!$elementID || empty($comment) || !$rating || $rating > 5 || empty($elementType))
		{
			echo "400";
			exit;
		}

		//JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblRating = JTable::getInstance('Rating', 'BctedTable',array());
		$tblRating->load(0);

		$data = array();

		$data['rated_id']        = $elementID;
		$data['rating_type']     = $elementType;
		$data['user_id']         = $userID;
		$data['rating']          = $rating;
		$data['rating_comment']  = $comment;
		$data['rating_datetime'] = date('Y-m-d h:i:s');
		$data['time_stamp']      = time();


		$tblRating->bind($data);

		if(!$tblRating->store())
		{
			echo "500";
			exit;
		}

		if($elementType == "venue")
		{
			$this->updateAverageRatingOfVenue($elementID);

			$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable',array());
			$tblVenuebooking->load($bookingID);
			if($tblVenuebooking->venue_booking_id)
			{
				$tblVenuebooking->is_rated = 1;
				$tblVenuebooking->store();
			}

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('*')
				->from($db->quoteName('#__bcted_venue_booking'))
				->where($db->quoteName('venue_id') . ' = ' . $db->quote($tblVenuebooking->venue_id))
				->where($db->quoteName('status') . ' = ' . $db->quote(5))
				->where($db->quoteName('user_id') . ' = ' . $db->quote($tblVenuebooking->user_id));

				//->where($db->quoteName('venue_table_id') . ' = ' . $db->quote($tblVenuebooking->venue_table_id))

			// Set the query and load the result.
			$db->setQuery($query);

			$bookingOfSameTables = $db->loadObjectList();

			foreach ($bookingOfSameTables as $key => $booking)
			{
				$currentTime = time();
				$timestamp = strtotime($booking->venue_booking_datetime);
				$BookedTimestampAfert24 = strtotime('+1 day', $timestamp);

				//echo "<br>".$currentTime . ' || ' . $BookedTimestampAfert24;

				if($currentTime >= $BookedTimestampAfert24)
				{

					$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable',array());
					$tblVenuebooking->load($booking->venue_booking_id);
					if($tblVenuebooking->venue_booking_id)
					{
						$tblVenuebooking->is_rated = 1;
						$tblVenuebooking->store();
					}
				}
			}
			//exit;

		}
		else if($elementType == "company" || $elementType == "service")
		{
			$this->updateAverageRatingOfCompany($elementID);

			$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable',array());
			$tblServiceBooking->load($bookingID);
			if($tblServiceBooking->service_booking_id)
			{
				$tblServiceBooking->is_rated = 1;
				$tblServiceBooking->store();
			}

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('*')
				->from($db->quoteName('#__bcted_service_booking'))
				->where($db->quoteName('company_id') . ' = ' . $db->quote($tblServiceBooking->company_id))
				->where($db->quoteName('status') . ' = ' . $db->quote(5))
				->where($db->quoteName('user_id') . ' = ' . $db->quote($tblServiceBooking->user_id));

				//->where($db->quoteName('service_id') . ' = ' . $db->quote($tblServiceBooking->service_id))

			// Set the query and load the result.
			$db->setQuery($query);

			$bookingOfSameTables = $db->loadObjectList();
			foreach ($bookingOfSameTables as $key => $booking)
			{
				$currentTime = time();
				$timestamp = strtotime($booking->service_booking_datetime);
				$BookedTimestampAfert24 = strtotime('+1 day', $timestamp);

				if($currentTime >= $BookedTimestampAfert24)
				{
					$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable',array());
					$tblServiceBooking->load($bookingID);
					if($tblServiceBooking->service_booking_id)
					{
						$tblServiceBooking->is_rated = 1;
						$tblServiceBooking->store();
					}
				}
			}
		}

		echo "200";
		exit;
	}
}
