<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerPackagePurchase extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'PackagePurchase', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function purchasePackage()
	{
		$app  = JFactory::getApplication();
		$input = $app->input;

		$packageId = $input->get('package_id',0,'int');
		$venueId   = $input->get('venue_id',0,'int');
		$companyId = $input->get('company_id',0,'int');
		$parentItemid = $input->get('Itemid',0,'int');

		$requested_date         = $input->get('requested_date','','string');
		$reqFromTime            = $input->get('requested_from_time', '', 'string');
		$reqToTime              = $input->get('requested_to_time', '', 'string');
		$requestedLocation      = $input->get('requested_location', '', 'string');
		$guest_count            = $input->get('guest_count',0,'int');
		$male_count             = $input->get('male_count',0,'int');
		$female_count           = $input->get('female_count',0,'int');
		$additional_information = $input->get('additional_information','','string');

		$fromTimeArray = explode(":", $reqFromTime);
		$toTimeArray = explode(":", $reqToTime);

		if(count($fromTimeArray)==1)
		{
			$reqFromTime = $fromTimeArray[0].":"."00:00";
		}
		else if(count($fromTimeArray)==2)
		{
			$reqFromTime = $fromTimeArray[0].":".$fromTimeArray[1].":00";
		}
		else if(count($fromTimeArray)>=3)
		{
			$reqFromTime = $fromTimeArray[0].":".$fromTimeArray[1].":".$fromTimeArray[2];
		}

		if(count($toTimeArray)==1)
		{
			$reqToTime = $toTimeArray[0].":"."00:00";
		}
		else if(count($toTimeArray)==2)
		{
			$reqToTime = $toTimeArray[0].":".$toTimeArray[1].":00";
		}
		else if(count($toTimeArray)>=3)
		{
			$reqToTime = $toTimeArray[0].":".$toTimeArray[1].":".$toTimeArray[2];
		}

		$user = JFactory::getUser();

		$menu = $app->getMenu();
		$menuItem = $menu->getItems( 'link', 'index.php?option=com_users&view=login', true );
		$Itemid = $menuItem->id;
		$link = 'index.php?option=com_users&view=login&Itemid='.$Itemid;


		if(!$user->id)
		{
			$msg = JText::_('COM_BCTED_USER_SESSTION_NOT_FOUND');
			$app->redirect($link,$msg);
		}

		$postData['package_id']                    = $packageId;
		$postData['user_id']                       = $user->id;
		$postData['venue_id']                      = $venueId;
		$postData['company_id']                    = $companyId;
		$postData['package_datetime']        	   = date('Y-m-d',strtotime($requested_date));
		$postData['package_time']                  = $reqFromTime;
		$postData['package_number_of_guest']       = $guest_count;
		$postData['male_count']                    = $male_count;
		$postData['female_count']                  = $female_count;
		$postData['package_location']              = $requestedLocation;
		$postData['package_additional_info']       = $additional_information;
		$postData['can_invite']       = 1;

		$tblPackage = JTable::getInstance('Package', 'BctedTable');
		$tblPackage->load($packageId);

		$postData['booked_currency_code']              = $tblPackage->currency_code;
		$postData['booked_currency_sign']              = $tblPackage->currency_sign;

		if($venueId)
		{
			$postData['status']                        = '1';
			$postData['user_status']                   = '2';
		}
		else if($companyId)
		{
			$postData['status']                        = '6';
			$postData['user_status']                   = '3';
		}

		$postData['package_purchased_datetime'] = date('Y-m-d H:i:s');
		$postData['time_stamp'] = time();

		$model = $this->getModel();

		$tblPackage = $model->getTable('Package');
		$tblPackage->load($packageId);
		$postData['total_price'] = $tblPackage->package_price * $guest_count;

		$response = $model->purchasePackage($postData);



		if($response)
		{
			//$Item = BctedHelper::getBctedMenuItem('user-packages');

			/*echo "<pre>";
			print_r($Item);
			echo "</pre>";
			exit;*/

			$menu = $app->getMenu();
			$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=packages', true );
			$Itemid = $menuItem->id;
			$link = 'index.php?option=com_bcted&view=packages&Itemid='.$parentItemid;

			$msg = JText::_('COM_BCTED_VENUE_PAKAGES_PURCHASE_REQUST_SUCCESS');
			//$link = JRoute::_('index.php?option=com_bcted&view=userbookings&Itemid='.$Item->id);
			//$link = $Item->link.'&Itemid='.$Item->id;
			$app->redirect($link,$msg);
		}
		else
		{
			$msg = JText::_('COM_BCTED_VENUE_PAKAGES_PURCHASE_REQUST_ERROR');
			$menu = $app->getMenu();
			$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=packagepurchase', true );
			$Itemid = $menuItem->id;
			$link = 'index.php?option=com_bcted&view=packagepurchase&Itemid='.$parentItemid;

			//$link = JRoute::_('index.php?option=com_bcted&view=packagepurchase&package_id='.$packageId);
			$app->redirect($link,$msg);
		}
	}


	public function setUnpublished()
	{
		$input  = JFactory::getApplication()->input;
		$cid    = $input->get('cid', 0, 'int');
		$itemid = $input->get('Itemid', 0, 'int');

		if($cid)
		{
			$model = $this->getModel();
			$result = $model->setUnpublished($cid);

			//JFactory::getApplication()->enqueueMessage('Default media set successfully');

		}

		$this->setRedirect('index.php?option=com_heartdart&view=messages&Itemid='.$itemid);
	}
}

