<?php
/**
 * @package     Diff.Site
 * @subpackage  com_Diff
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Diff Order Controller
 *
 * @since  0.0.1
 */
class BctedControllerVenueorder extends JControllerForm
{
	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   string  $name  	 The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array  	$config  Configuration array for model. Optional.
	 *
	 * @return object The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = '', $prefix = '', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	/**
	 * Method to Cancel event creation action
	 *
	 * @param   int  $key  key column for table
	 *
	 * @return  void
	 *
	 * @since   0.0.1
	 */
	public function cancel($key = 'dish_id')
	{
		$app = JFactory::getApplication();
		$app->redirect('index.php?option=com_diff&view=frontdishes');
		$app->close();
	}

	/**
	 * makeOffer to particular restaurant.
	 *
	 * @since   0.0.1
	 */
	public function makeOrder()
	{
		$app   = JFactory::getApplication();
		$model = $this->getModel('Order', 'DiffModel');

		$result = $model->makeOrder();

		echo $result;
		$app->close();
		//return $result;

		/*$app->redirect('index.php?option=com_diff&view=frontdishes&Itemid=106');
		$app->close();*/
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function bookVenue()
	{
		JPluginHelper::importPlugin('bct');
		$dispatcher = JDispatcher::getInstance('paypal');
		$results = $dispatcher->trigger('onPreparePaymentPaypal', array());
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function placeRequest()
	{
		JPluginHelper::importPlugin('dreamyFlavours');
		$dispatcher = JDispatcher::getInstance('paypal');
		$results = $dispatcher->trigger('onPreparePaymentPaypalForRequest', array());
	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function paypalresponse()
	{

	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function paymentCancel()
	{

	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function paymentReturn()
	{
		$app->redirect("index.php?option=com_diff&view=dish");
		$app->close();

	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function notifyoffer()
	{
		$model = $this->getModel('Order', 'DiffModel');

		$model->notifyoffer();

	}

	/**
	 * Method to Place order
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   0.0.1
	 */
	public function notifyofferNotTip()
	{
		$model = $this->getModel('Order', 'DiffModel');

		$model->notifyofferNotTip();

	}

	public function sendNotifi()
	{
		$model = $this->getModel('Order', 'DiffModel');

		$model->sendPushNotification(758, 100, 1,"Meal request confirmed by customer.",105);

		echo "Pusho notification send";
		die();

	}


}
