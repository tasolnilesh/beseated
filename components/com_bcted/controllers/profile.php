<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerProfile extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'Profile', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function contactadmin()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('email')
			->from($db->quoteName('#__users','u'))
			->where($db->quoteName('email') . ' <> ""');
		$query->join('RIGHT','#__user_usergroup_map AS ugm ON ugm.user_id=u.id AND ugm.group_id=8');


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadColumn();

		// Initialise variables.
		$app     = JFactory::getApplication();
		$config  = JFactory::getConfig();

		$site    = $config->get('sitename');
		$from    = $config->get('mailfrom');
		$sender  = $config->get('fromname');
		$email   = $result; //$app->input->get('mailto');
		$subject = JText::_('COM_BCTED_CONATACT_EMAIL_SUBJECT');//$app->input->get('subject');

		$conatctName = $app->input->get('contact_name');
		$conatctEmail = $app->input->get('contact_email');
		$conatctMobile = $app->input->get('contact_mobile');
		$conatctMessage = $app->input->get('contact_message');

		/*$user = JFactory::getUser();
		if($user->id)
		{
			$userName= $user->name;
		}
		else
		{
			$userName= 'Guest';
		}*/

		// Build the message to send.
		//$msg     = JText::_('COM__SEND_EMAIL_MSG');
		$body    = JText::sprintf('COM_BCTED_CONATACT_EMAIL_BODY', $conatctName,$conatctName, $conatctEmail, $conatctMobile,$conatctMessage);

		// Clean the email data.
		/*$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);*/

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body, true);

		// Check for an error.
		if ($return !== true)
		{
			return new JException(JText::_('COM__SEND_MAIL_FAILED'), 500);
		}
	}

	public function change_currency()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$venue_id = $input->get('venue_id',0,'int');
		$currency_code = $input->get('currency_code','','string');

		if(!$venue_id || empty($currency_code))
		{
			echo "400";
			exit;
		}

		$data = array();

		if($currency_code == 'EUR')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "€";
		}
		else if ($currency_code == 'GBP')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "£";
		}
		else if ($currency_code == 'AED')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "AED";
		}
		else if ($currency_code == 'USD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}
		else if ($currency_code == 'CAD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}
		else if ($currency_code == 'AUD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}

		$model = $this->getModel();

		if($model->is_booking_in_venue($venue_id) == 0)
		{
			$result = $model->saveVenueProfile($data,$venue_id);

			if($result)
			{
				echo "200";
				exit;
			}

			echo "500";
			exit;
		}


		echo "707";
		exit;

	}



	public function save()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		/*echo "<pre>";
		print_r($input);
		echo "</pre>";
		exit;*/
		$user = JFactory::getUser();



		/*echo "cakk<pre>";
		print_r($file);
		echo "</pre>";
		exit;*/


		$c1 = $input->get('c1',0,'int');
		$c2 = $input->get('c2',0,'int');
		$c3 = $input->get('c3',0,'int');
		$c4 = $input->get('c4',0,'int');
		$c5 = $input->get('c5',0,'int');
		$c6 = $input->get('c6',0,'int');
		$c7 = $input->get('c7',0,'int');

		$venue_id      = $input->get('venue_id','','string');
		$venue_name    = $input->get('venue_name','','string');
		$venue_address = $input->get('venue_address','','string');

		$city    = $input->get('city','','string');

		$fromTime        = $input->get('from_time','','string');
		$toTime        = $input->get('to_time','','string');

		$venue_about   = $input->get('venue_about','','string');
		$is_smart      = $input->get('is_smart','','string');
		$is_casual     = $input->get('is_casual','','string');
		$is_food       = $input->get('is_food','','string');
		$is_drink      = $input->get('is_drink','','string');
		$is_smoking    = $input->get('is_smoking','','string');

		$latitude    = $input->get('latitude','','string');
		$longitude    = $input->get('longitude','','string');

		$latlong = BctedHelper::getLatitudeAndLongitude($venue_address);
		$city = BctedHelper::getAddressDetail($venue_address);
		//echo $latlong;
		$latlongArray = explode("|", $latlong);
		$latitude = $latlongArray[0];
		$longitude = $latlongArray[1];


		/*$currency_code    = $input->get('currency_code','','string');

		if($currency_code == 'EUR')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "€";
		}
		else if ($currency_code == 'GBP')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "£";
		}
		else if ($currency_code == 'AED')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "AED";
		}
		else if ($currency_code == 'USD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}
		else if ($currency_code == 'CAD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}
		else if ($currency_code == 'AUD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}*/

		$data['venue_name']    = $venue_name;
		$data['venue_address'] = $venue_address;

		if(!empty($latitude))
		{
			$data['latitude'] = $latitude;
		}

		if(!empty($city))
		{
			$data['city'] = $city;
		}

		if(!empty($longitude))
		{
			$data['longitude'] = $longitude;
		}

		//$timeing = explode("-", $timingString);
		$singleTimeValue = array();

		$data['from_time'] = $fromTime;
		$data['to_time'] = $toTime;


		$file = $input->files->get('venue_image');
		/*echo "<pre>";
		print_r($file);
		echo "</pre>";
		exit;*/
		$imagePath = BctedHelper::uplaodFile($file,'venue',$user->id);

		$fileTypeArray = explode("/", $file['type']);


		if(in_array('video', $fileTypeArray))
		{
			$venueVideo = $imagePath;
			if(!empty($venueVideo))
			{
				$storageImage = getcwd().'/'.$venueVideo;
				$videoOut = getcwd().'/images/bcted/venue/'.$user->id.'/';
				$convertedvideo = BctedHelper::convertVideo2($storageImage, $videoOut, '400x300', false);

				if(!empty($convertedvideo))
				{
					$venueVideo	= 'images/bcted/venue/'.$user->id.'/'.$convertedvideo;

					$videoPath = getcwd() . '/' . $venueVideo;
					$videoThumb	= getcwd().'/images/bcted/venue/'.$user->id.'/';
					$imagePath = BctedHelper::createVideoThumb($videoPath, $videoThumb);

					$imagePath = 'images/bcted/venue/'.$user->id.'/'.$imagePath;

					/*echo $imagePath;
					exit;*/
					$data['venue_video'] = $venueVideo;
				}
				else
				{
					$venueVideo = '';
				}
			}


			//$imagePath = '';
		}

		/*echo $imagePath;
		exit;*/

		if(!empty($imagePath))
		{
			$data['venue_image'] = $imagePath;
		}

		$data['venue_about']   = $venue_about;
		$data['is_smart']      = $is_smart;
		$data['is_casual']     = $is_casual;
		$data['is_food']       = $is_food;
		$data['is_drink']      = $is_drink;
		$data['is_smoking']    = $is_smoking;


		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;*/
		/*[option] => com_bcted
		[view] => profile
		[Itemid] =>
		[venue_name] => Crystal
		[venue_address] => United Arab Emirates
		[timing] =>
		[venue_about] => This all about crystal club added in by Roger
		[c1] => 1
		[c3] => 3
		[c5] => 5
		[c6] => 6
		[c7] => 7
		[is_smart] => 0
		[is_casual] => 1
		[is_food] => 0
		[is_drink] => 1
		[is_smoking] => 0
		[task] => save*/

		$model = $this->getModel();
		$days = array();
		//$model->saveVenueProfile($data);

		if($c1 == 1)
			$days[] = 1;

		if($c2 == 2)
			$days[] = 2;

		if($c3 == 3)
			$days[] = 3;

		if($c4 == 4)
			$days[] = 4;

		if($c5 == 5)
			$days[] = 5;

		if($c6 == 6)
			$days[] = 6;

		if($c7 == 7)
			$days[] = 7;



		if(count($days) != 0)
		{
			$data['working_days']    = implode(",", $days);

		}

		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;*/

		if($venue_id)
		{
			$data['venue_modified'] = date('Y-m-d H:i:s');
		}

		if(count($data) != 0)
		{
			/*echo $venue_id."<pre>";
			print_r($data);
			echo "</pre>";
			exit;*/
			$result = $model->saveVenueProfile($data,$venue_id);
		}
		else
		{
			$result = false;
		}


		/*echo "<pre>";
		print_r($input);
		echo "</pre>";*/
		//exit;
		/*$app = JFactory::getApplication();
		$menu = $app->getMenu()->getActive()->link;
		$url = JRoute::_($menu);*/

		/*$menu = JFactory::getApplication()->getMenu();
		$parent = $menu->getActive();
		echo "<pre>";
		print_r($parent);
		echo "</pre>";
		exit;*/

		/*echo "hello";
		exit;*/

		$menuItem = BctedHelper::getBctedMenuItem('club-profile');


		$Itemid =$menuItem->id;
		$link = $menuItem->link.'&Itemid='.$Itemid;

		/*$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$link = $menu->getItem($itemid)->link;

		echo 'Itemid : ' . $itemid . ' || Link :'. JRoute::_($link);

		echo "<pre>";
		print_r($menu);
		echo "</pre>";
		exit;*/

		$this->setRedirect($link,'Profile Saved');

	}

	public function uploadImage()
	{
		$input = JFactory::getApplication()->input;
		$file = $input->files->get('image');
		$user = JFactory::getUser();

		$imagePath = BctedHelper::uplaodFile($file,'venue',$user->id);
		$club = BctedHelper::getUserElementID($user->id);

		$source        = JPATH_SITE.'/'.$imagePath;
		$orignalFile   = pathinfo($source);
		$videoExtAllow = array('mov','mp4');
		$storeImg      = $imagePath;
		$storeFlv      = "";
		$storeMp4      = "";
		$storeWebm     = "";
		$hasVideo = 0;

		if(!empty($imagePath) && in_array($orignalFile['extension'], $videoExtAllow))
		{
			$hasVideo  = 1;
			$destPng = $orignalFile['dirname'].'/'.$orignalFile['filename'].'_png.png';
			$storeImg =  'images/bcted/venue/'.$user->id.'/'.$orignalFile['filename'].'_png.png';
			$command = "/usr/bin/ffmpeg -i $source -r 1 -s 700x600 -f image2 $destPng";
			$output = shell_exec($command);

			$destFlv = $orignalFile['dirname'].'/'.$orignalFile['filename'].'_flv.flv';
			$storeFlv =  'images/bcted/venue/'.$user->id.'/'.$orignalFile['filename'].'_flv.flv';
			$command = "/usr/bin/ffmpeg -y -i $source -g 30 -vcodec copy -acodec copy $destFlv 2>".JPATH_SITE."/ffmpeg_test1.txt";
			$output = shell_exec($command);

			$destMp4 = $orignalFile['dirname'].'/'.$orignalFile['filename'].'_mp4.mp4';
			$storeMp4 =  'images/bcted/venue/'.$user->id.'/'.$orignalFile['filename'].'_mp4.mp4';
			$command = "/usr/bin/ffmpeg -i ".$destFlv." -ar 22050 ".$destMp4; //Wroking
			$output = shell_exec($command);

			$destWebm = $orignalFile['dirname'].'/'.$orignalFile['filename'].'_webm.webm';
			$storeWebm =  'images/bcted/venue/'.$user->id.'/'.$orignalFile['filename'].'_webm.webm';
			$command = "/usr/bin/ffmpeg -i ".$source." -acodec libvorbis -ac 2 -ab 96k -ar 44100 -b 345k -s 640x360 ".$destWebm; //Wroking
			$output = shell_exec($command);
		}

		if(!empty($imagePath))
		{
			$tblVenue = JTable::getInstance('Venue','BctedTable',array());
			$tblVenue->load($club->venue_id);

			$tblVenue->venue_image      = $storeImg;

			if($hasVideo == 1)
			{
				$tblVenue->venue_video      = $imagePath;
				$tblVenue->venue_video_flv  = $storeFlv;
				$tblVenue->venue_video_mp4  = $storeMp4;
				$tblVenue->venue_video_webm = $storeWebm;
			}
			else
			{
				$tblVenue->venue_video      = "";
				$tblVenue->venue_video_flv  = "";
				$tblVenue->venue_video_mp4  = "";
				$tblVenue->venue_video_webm = "";
			}


			if($tblVenue->store())
			{
				echo "200";
			}
			else
			{
				echo "500";
			}
		}
		else{
			echo "500";
		}

		exit;
	}
}
