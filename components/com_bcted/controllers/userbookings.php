<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerUserbookings extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'UserBookings', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function inviteUserInPackage()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		/*echo "<pre>";
		print_r($input);
		echo "</pre>";
		[invite_user] => nilesh@gmail.com,nilesh@tasol.com,nilesh@yahoo.com
        [package_purchase_id] => 43
		exit;*/

		$packagePurchaseID = $input->get('package_purchase_id',0,'int');
		$usersEmail = $input->get('invite_user','','string');
		$Itemid = $input->get('Itemid',0,'int');

		if(!$packagePurchaseID || empty($usersEmail))
		{
			$app->redirect('index.php');
			$app->close();
		}

		$model = $this->getModel();
		$result = $model->invite_user_in_package($packagePurchaseID,$usersEmail);

		$result = explode("||", $result);

		$menu = $app->getMenu();
		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=userbookingdetail', true );

		if($result[0] == 101)
		{
			$app->redirect('index.php?option=com_bcted&view=userbookingdetail&booking_type=package&purchase_id='.$packagePurchaseID.'&Itemid='.$Itemid,JText::sprintf('COM_BCTED_INVITE_USER_GREATER_THEN_NUMBER_OF_GUEST',$result[1]));
			$app->close();
		}

		/*echo $result;
		exit;*/


		//$Itemid = $menuItem->id;
		//Itemid
		if($result[0] == 200)
		{
			$app->redirect('index.php?option=com_bcted&view=userbookingdetail&booking_type=package&purchase_id='.$packagePurchaseID.'&Itemid='.$Itemid,JText::_('COM_BCTED_INVITE_USER_IN_MY_PURCHASED_PACKAGE_SUCCESS'));
			$app->close();
		}

		$app->redirect('index.php?option=com_bcted&view=userbookingdetail&booking_type=package&purchase_id='.$packagePurchaseID.'&Itemid='.$Itemid,JText::_('COM_BCTED_INVITE_USER_IN_MY_PURCHASED_PACKAGE_FAIL'));
		$app->close();
	}

	public function send_request_refund()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		$Itemid = $input->get('Itemid', 0, 'int');
		$bookingID = $input->get('booking_id', 0, 'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackagePurchased->load($bookingID);

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblPackage = JTable::getInstance('Package', 'BctedTable');

		$tblVenue->load($tblPackagePurchased->venue_id);
		$tblPackage->load($tblPackagePurchased->package_id);

		$loginUser = JFactory::getUser();

		$tblPackagePurchased->status = 10;
		$tblPackagePurchased->user_status = 10;

		//$message = $loginUser->username . ' has requested refund for '. $tblPackage->package_name;
		$message = JText::sprintf('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREFUND_MESSAGE',$loginUser->username,$tblPackage->package_name);
		$messageType = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREFUND'); //'PackageRequestRefund';

		//$link = 'index.php?option=com_bcted&view=userbookingdetail&booking_type=package&purchase_id='.$bookingID.'&Itemid='.$Itemid;
		$link = 'index.php?option=com_bcted&view=userbookings&type=packages&Itemid='.$Itemid;

		$tblPackagePurchased->is_request_refund = 1;

		if(!$tblPackagePurchased->store())
		{
			$app->redirect($link,JText::_('COM_BCTED_REQUEST_REFUND_NOT_DONE'));
			$app->close();
		}

		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$appHelper            = new bctedAppHelper;


		$bookingData = $appHelper->getBookingDetailForPackage($tblPackagePurchased->package_purchase_id,'Club');
		$pushcontentdata=array();
		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "package";
		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$db = JFactory::getDbo();

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');

		if($packagePurchased->venue_id)
		{
			$pushUserID=$tblVenue->userid;
		}
		else if($packagePurchased->company_id)
		{
			$pushUserID=$tblCompany->userid;
		}

		$jsonarray['pushNotificationData']['id']         = $obj->id;
		$jsonarray['pushNotificationData']['to']         = $pushUserID;
		$jsonarray['pushNotificationData']['message']    = $message;
		$jsonarray['pushNotificationData']['type']       = $messageType;
		$jsonarray['pushNotificationData']['configtype'] = '';

		BctedHelper::sendPushNotification($jsonarray);

		$app->redirect($link,JText::_('COM_BCTED_REQUEST_REFUND_DONE'));
		$app->close();

	}

	public function send_invited_request_refund()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		$packageInviteID = $input->get('package_invite_id',0,'int');
		$Itemid = $input->get('Itemid',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblPackageInvite = JTable::getInstance('PackageInvite', 'BctedTable');
		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackageInvite->load($packageInviteID);

		$link = "index.php?option=com_bcted&view=userpackageinvitedetail&package_invite_id=".$packageInviteID."&Itemid=".$Itemid;

		if(!$tblPackageInvite->package_invite_id)
		{
			$app->redirect($link,JText::_('COM_BCTED_REQUEST_REFUND_NOT_DONE'));
			$app->close();
		}

		$tblPackagePurchased->load($tblPackageInvite->package_purchase_id);

		if(!$tblPackagePurchased->package_purchase_id)
		{
			$app->redirect($link,JText::_('COM_BCTED_REQUEST_REFUND_NOT_DONE'));
			$app->close();
		}

		if($tblPackageInvite->status==10)
		{
			$app->redirect($link,JText::_('COM_BCTED_REQUEST_REFUND_NOT_DONE'));
			$app->close();
		}
		$bookingDT = $tblPackagePurchased->package_datetime . ' ' . $tblPackagePurchased->package_time;
		$bookingTS = strtotime($bookingDT);
		$before24 = strtotime("-24 hours", strtotime($bookingDT));
		$currentTime = time();

		if($currentTime<=$before24)
		{

		}
		else
		{
			$app->redirect($link,JText::_('COM_BCTED_REQUEST_REFUND_NOT_DONE'));
			$app->close();
		}

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblPackage = JTable::getInstance('Package', 'BctedTable');
		$tblVenue->load($tblPackagePurchased->venue_id);
		$tblCompany->load($tblPackagePurchased->company_id);
		$tblPackage->load($tblPackagePurchased->package_id);
		$loginUser = JFactory::getUser();
		$tblPackageInvite->status = 10;
		//$message = $loginUser->username . ' has requested refund for '. $tblPackage->package_name;
		//$messageType = 'PackageRequestRefund';

		$message = JText::sprintf('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREFUND_MESSAGE',$loginUser->username,$tblPackage->package_name);
		$messageType = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREFUND'); //'PackageRequestRefund';

		if(!$tblPackageInvite->store())
		{
			$app->redirect($link,JText::_('COM_BCTED_REQUEST_REFUND_NOT_DONE'));
			$app->close();
		}

		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$appHelper            = new bctedAppHelper;

		$bookingData = $appHelper->getBookingDetailForPackage($tblPackagePurchased->package_purchase_id,'Club');
		$pushcontentdata=array();
		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "package";
		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));
		$db = JFactory::getDbo();
		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');

		if($packagePurchased->venue_id)
		{
			$pushUserID=$tblVenue->userid;
		}
		else if($packagePurchased->company_id)
		{
			$pushUserID=$tblCompany->userid;
		}

		$jsonarray['pushNotificationData']['id']         = $obj->id;
		$jsonarray['pushNotificationData']['to']         = $pushUserID;
		$jsonarray['pushNotificationData']['message']    = $message;
		$jsonarray['pushNotificationData']['type']       = $messageType;
		$jsonarray['pushNotificationData']['configtype'] = '';

		BctedHelper::sendPushNotification($jsonarray);

		$app->redirect($link,JText::_('COM_BCTED_REQUEST_REFUND_DONE'));
		$app->close();

	}

	public function cancelBooking()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		$bookingID = $input->get('booking_id',0,'int');
		$bookingType = $input->get('bookingType','','string');
		$Itemid = $input->get('Itemid',0,'int');


		if($bookingType == 'venue')
		{
			$tblBooking = JTable::getInstance('Venuebooking','BctedTable');
			$tblBooking->load($bookingID);

			if(!$tblBooking->venue_booking_id)
			{
				$app->redirect('index.php?option=com_bcted&view=userbookings&Itemid='.$Itemid);
			}


		}
		else if($bookingType == 'service')
		{
			$tblBooking = JTable::getInstance('ServiceBooking','BctedTable');
			$tblBooking->load($bookingID);

			if(!$tblBooking->service_booking_id)
			{
				$app->redirect('index.php?option=com_bcted&view=userbookings&Itemid='.$Itemid);
			}
		}
		else
		{
			$app->redirect('index.php?option=com_bcted&view=userbookings&Itemid='.$Itemid);
		}

		$tblBooking->status = 11;
		$tblBooking->user_status = 11;
		$userID = $tblBooking->user_id;

		$tblBooking->store();

		$this->decreasLoyalty($bookingID,$bookingType,$userID);

		require_once JPATH_SITE.'/components/com_ijoomeradv/extensions/bcted/helper.php';
		$appHelper            = new bctedAppHelper;
		$user = JFactory::getUser();

		if($bookingType == 'venue')
		{
			$newBookingID = $tblBooking->venue_booking_id;
			$bookingData = $appHelper->getBookingDetailForVenueTable($newBookingID,'Venue');
			$tblTable = JTable::getInstance('Table', 'BctedTable');
			$tblTable->load($tblBooking->venue_table_id);
			$tblVenue = JTable::getInstance('Venue', 'BctedTable');
			$tblVenue->load($tblBooking->venue_id);

			$venueID = $tblVenue->venue_id;
			$tableID = $tblTable->venue_table_id;
			$companyID = 0;
			$serviceID = 0;
			$toUserID = $tblVenue->userid;
			$messageType = "tablebookingcancel";
			$cancelMessage = JText::sprintf('COM_BESEATED_COMPANY_VENUE_BOOKING_CANCEL_MESSAGE',$user->name,($tblTable->premium_table_id)?$tblTable->venue_table_name:$tblTable->custom_table_name);

			$messageType = JText::_('PUSHNOTIFICATION_TYPE_TABLEBOOKINGCANCEL');

		}
		else if($bookingType == 'service')
		{
			$newBookingID = $tblBooking->service_booking_id;
			$bookingData = $appHelper->getBookingDetailForServices($newBookingID,'Company');

			$tblService = JTable::getInstance('Service', 'BctedTable');
			$tblService->load($tblBooking->service_id);

			$tblCompany = JTable::getInstance('Company', 'BctedTable');
			$tblCompany->load($tblBooking->company_id);

			$venueID = 0;
			$tableID = 0;
			$companyID = $tblCompany->company_id;
			$serviceID = $tblService->service_id;
			$toUserID = $tblCompany->userid;
			$messageType = "servicebookingcancel";
			$cancelMessage = JText::sprintf('COM_BESEATED_COMPANY_SERVICE_BOOKING_CANCEL_MESSAGE',$user->name,$tblService->service_name);
			$messageType = JText::_('PUSHNOTIFICATION_TYPE_SERVICEBOOKINGCANCEL');
		}

		$appHelper->sendMessage($venueID,$companyID,$serviceID,$tableID,$toUserID,$cancelMessage,$bookingData,$messageType);

		$pushcontentdata=array();
		$pushcontentdata['data'] = $bookingData;

		if($bookingType == 'venue')
		{
			$pushcontentdata['elementType'] = "venue";
		}
		else if($bookingType == 'service')
		{
			$pushcontentdata['elementType'] = "company";
		}


		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$db = JFactory::getDbo();

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{
			$userProfile    = $appHelper->getUserProfile($toUserID);
			$receiveRequest = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveRequest = $params->settings->pushNotification->receiveRequest;
			}

			$receiveRequest = 1;

			if($receiveRequest)
			{
				$message = $cancelMessage;
				$jsonarray['pushNotificationData']['id']         = $obj->id;
				$jsonarray['pushNotificationData']['to']         = $toUserID;
				$jsonarray['pushNotificationData']['message']    = $message;
				$jsonarray['pushNotificationData']['type']       = $messageType;
				$jsonarray['pushNotificationData']['configtype'] = '';

				BctedHelper::sendPushNotification($jsonarray);
			}



		}


		$app->redirect('index.php?option=com_bcted&view=userbookings&Itemid='.$Itemid);


		/*[option] => com_bcted
		[task] => cancelBooking
		[bookingType] => venue
		[booking_id] => 71
		[Itemid] => 188*/
	}

	public function cancelPacakgeBooking()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		$packageBookingID = $input->get('package_booking_id',0,'int');
		$Itemid = $input->get('Itemid',0,'int');

		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackagePurchased->load($packageBookingID);

		if(!$tblPackagePurchased->package_purchase_id)
		{
			$app->redirect('index.php?option=com_bcted&view=userbookingdetail&booking_type=package&purchase_id='.$tblPackagePurchased->package_id.'&Itemid='.$Itemid);
		}

		$tblPackagePurchased->status = 11;
		$tblPackagePurchased->user_status = 11;

		/*echo "<pre>";
		print_r($tblPackagePurchased);
		echo "</pre>";
		exit;*/

		if(!$tblPackagePurchased->store())
		{
			$app->redirect('index.php?option=com_bcted&view=userbookingdetail&booking_type=package&purchase_id='.$tblPackagePurchased->package_id.'&Itemid='.$Itemid);
		}

		$app->redirect('index.php?option=com_bcted&view=userbookingdetail&booking_type=package&purchase_id='.$tblPackagePurchased->package_id.'&Itemid='.$Itemid);



	}

	public function decreasLoyalty($bookingID,$bookingType,$userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_payment_status'))
			->where($db->quoteName('booked_element_id') . ' = ' . $db->quote($bookingID));

		if($bookingType == 'venue')
		{
			$query->where($db->quoteName('booked_element_type') . ' = ' . $db->quote('venue'));
		}
		else if($bookingType == 'service')
		{
			$query->where($db->quoteName('booked_element_type') . ' = ' . $db->quote('service'));
		}
		else
		{
			return false;
		}

		$query->where($db->quoteName('paid_status') . ' = ' . $db->quote('1'));

		// Set the query and load the result.
		$db->setQuery($query);

		//echo $query->dump();

		$paymentStatus = $db->loadObject();

		/*echo "<pre>";
		print_r($paymentStatus);
		echo "</pre>";
		exit;*/

		if($paymentStatus)
		{
			$queryLP = $db->getQuery(true);
			$queryLP->select('*')
				->from($db->quoteName('#__bcted_loyalty_point'))
				->where($db->quoteName('user_id') . ' = ' . $db->quote($userID));

			if($bookingType == 'venue')
			{
				$queryLP->where($db->quoteName('point_app') . ' = ' . $db->quote('purchase.venue'));
			}
			else if($bookingType == 'service')
			{
				$queryLP->where($db->quoteName('point_app') . ' = ' . $db->quote('purchase.service'));
			}
			else
			{
				return false;
			}

			$queryLP->where($db->quoteName('cid') . ' = ' . $db->quote($paymentStatus->payment_id));

			// Set the query and load the result.
			$db->setQuery($queryLP);

			//echo $queryLP->dump();

			$loyaltyPointDetail = $db->loadObject();

			/*echo "<pre>";
			print_r($loyaltyPointDetail);
			echo "</pre>";
			exit;*/

			if($loyaltyPointDetail)
			{
				$query = $db->getQuery(true);

				if($bookingType == 'venue')
				{
					$point_app = 'venue.cancelbooking';
				}
				else if($bookingType == 'service')
				{
					$point_app = 'service.cancelbooking';
				}
				else
				{
					return false;
				}

				// Create the base insert statement.
				$query->insert($db->quoteName('#__bcted_loyalty_point'))
					->columns(
						array(
							$db->quoteName('user_id'),
							$db->quoteName('earn_point'),
							$db->quoteName('point_app'),
							$db->quoteName('cid'),
							$db->quoteName('is_valid'),
							$db->quoteName('created'),
							$db->quoteName('time_stamp')
						)
					)
					->values(
						$db->quote($userID) . ', ' .
						$db->quote(($loyaltyPointDetail->earn_point * (-1))) . ', ' .
						$db->quote($point_app) . ', ' .
						$db->quote($loyaltyPointDetail->cid) . ', ' .
						$db->quote(1) . ', ' .
						$db->quote(date('Y-m-d H:i:s')) . ', ' .
						$db->quote(time())
					);

				// Set the query and execute the insert.
				$db->setQuery($query);

				$db->execute();

			}
		}
	}

}
