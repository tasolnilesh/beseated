<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerClubTableBooking extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'ClubTableBooking', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function testing()
	{
		/*$app = JFactory::getApplication();
		$menu = $app->getMenu()->getActive()->link;
		$url = JRoute::_($menu);*/

		/*$menu = JFactory::getApplication()->getMenu();
		$parent = $menu->getActive();
		echo "<pre>";
		print_r($parent);
		echo "</pre>";
		exit;*/

		/*echo "hello";
		exit;*/

		$menuItem = BctedHelper::getBctedMenuItem('user-clubguestlist');
		/*echo "<pre>";
		print_r($menuItem);
		echo "</pre>";*/

		$Itemid =$menuItem->id;
		$link = $menuItem->link.'&Itemid='.$Itemid;

		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$link = $menu->getItem($itemid)->link;

		echo 'Itemid : ' . $itemid . ' || Link :'. JRoute::_($link);

		echo "<pre>";
		print_r($menu);
		echo "</pre>";
		exit;

	}

	public function bookVenueTable()
	{
		$app  = JFactory::getApplication();
		$input = $app->input;

		//echo "<pre/>";print_r($input);exit;

		$tableID = $input->get('table_id',0,'int');
		$clubID = $input->get('club_id',0,'int');

		$requested_date         = $input->get('requested_date','','string');
		$reqFromTime            = $input->get('requested_from_time', '', 'string');
		$reqToTime              = $input->get('requested_to_time', '', 'string');
		$guest_count            = $input->get('guest_count',0,'int');
		$male_count             = $input->get('male_count',0,'int');
		$female_count           = $input->get('female_count',0,'int');
		$additional_information = $input->get('additional_information','','string');

		$user = JFactory::getUser();

		/*$menuItem = BctedHelper::getBctedMenuItem('user-login');
		$Itemid =$menuItem->id;
		$link = $menuItem->link.'&Itemid='.$Itemid;*/
		$menu = $app->getMenu();

		$menuItem = $menu->getItems( 'link', 'index.php?option=com_users&view=login', true );
		$itemid = $menuItem->id;
		$link = $menuItem->link.'&Itemid='.$itemid;

		if(!$user->id)
		{
			$msg = JText::_('COM_BCTED_USER_SESSTION_NOT_FOUND');
			$app->redirect($link,$msg);
		}

		/*$menuItem = BctedHelper::getBctedMenuItem('user-clubtables');
		$Itemid =$menuItem->id;
		$link = $menuItem->link.'&club_id='.$clubID.'&Itemid='.$Itemid;*/

		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=clubtables', true );
		$itemid = $menuItem->id;
		$link = 'index.php?option=com_bcted&view=clubtables'.'&club_id='.$clubID.'&Itemid='.$itemid;

		/*echo $tableID . " | " . $clubID . " | " . $requested_date . " | " . $reqFromTime . " | " . $reqToTime . " | " . $guest_count;
		exit;*/

		if(!$tableID || !$clubID || empty($requested_date) || empty($reqFromTime) || empty($reqToTime) || !$guest_count)
		{
			$msg = JText::_('COM_BCTED_VENUE_TABLE_BOOKING_REQUST_INVALID_DATA');
			$app->redirect($link,$msg);
		}

		$fromTimeArray = explode(":", $reqFromTime);
		$toTimeArray = explode(":", $reqToTime);

		if(count($fromTimeArray)==1)
		{
			$reqFromTime = $fromTimeArray[0].":"."00:00";
		}
		else if(count($fromTimeArray)==2)
		{
			$reqFromTime = $fromTimeArray[0].":".$fromTimeArray[1].":00";
		}
		else if(count($fromTimeArray)>=3)
		{
			$reqFromTime = $fromTimeArray[0].":".$fromTimeArray[1].":".$fromTimeArray[2];
		}

		if(count($toTimeArray)==1)
		{
			$reqToTime = $toTimeArray[0].":"."00:00";
		}
		else if(count($toTimeArray)==2)
		{
			$reqToTime = $toTimeArray[0].":".$toTimeArray[1].":00";
		}
		else if(count($toTimeArray)>=3)
		{
			$reqToTime = $toTimeArray[0].":".$toTimeArray[1].":".$toTimeArray[2];
		}

		$postData['venue_id']                      = $clubID;
		$postData['venue_table_id']                = $tableID;
		$postData['user_id']                       = $user->id;
		$postData['venue_booking_table_privacy']   = 0;
		$postData['venue_booking_datetime']        = date('Y-m-d',strtotime($requested_date));
		$postData['booking_from_time']             = $reqFromTime;
		$postData['booking_to_time']               = $reqToTime;
		$postData['status']                        = '1';
		$postData['user_status']                   = '2';
		$postData['is_new']                   = '1';
		$postData['venue_booking_number_of_guest'] = $guest_count;
		$postData['male_count']                    = $male_count;
		$postData['female_count']                  = $female_count;
		$postData['venue_booking_additional_info'] = $additional_information;

		$postData['venue_booking_created'] = date('Y-m-d H:i:s');
		$postData['time_stamp'] = time();

		$model = $this->getModel();

		$response = $model->bookVenueTable($postData);

		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=userbookings', true );
		$itemid = $menuItem->id;

		$link = 'index.php?option=com_bcted&view=userbookings&Itemid='.$itemid;

		/*echo $response;
		exit;*/

		if($response)
		{


			$msg = JText::_('COM_BCTED_VENUE_TABLE_BOOKING_REQUST_SUCCESS');
			$app->redirect($link,$msg);
		}
		else
		{
			$msg = JText::_('COM_BCTED_VENUE_TABLE_BOOKING_REQUST_ERRORS');
			$app->redirect($link,$msg);
		}




		/*$app = JFactory::getApplication();
		$menu = $app->getMenu()->getActive()->link;
		$url = JRoute::_($menu);
		----------------------------------------------
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$menuItem = $menu->getItems( 'link', 'index.php?option=com_example&view=reporting', true );
		echo JRoute::_('index.php?Itemid='.$menuItem->id);

		*/







		if(empty($reqDate) || empty($reqTime) || empty($additionalInfo))
		{
			$erMsg = JText::_('COM_BCTED_INVALIED_GUEST_LIST_REQUST_PARAMETERS');
			$this->setRedirect($link,$erMsg);

			return true;
		}

		if(!$clubID || !$guestCount)
		{
			$erMsg = JText::_('COM_BCTED_INVALIED_GUEST_LIST_REQUST_PARAMETERS');
			$this->setRedirect($link,$erMsg);

			return true;
		}

		$arrayData['venue_id']        = $clubID;
		$dateTime                     = date('Y-m-d H:i:s', strtotime($reqDate . ' ' . $reqTime));
		$arrayData['request_date']    = $dateTime;
		$arrayData['user_id']         = $user->id;
		$arrayData['number_of_guest'] = $guestCount;
		$arrayData['male_count']      = $maleCount;
		$arrayData['female_count']    = $femaleCount;
		$arrayData['additional_info'] = $additionalInfo;
		$arrayData['time_stamp']      = time();
		$arrayData['created']         = date('Y-m-d H:i:s');

		$result = $model->sendGuestListRequest($arrayData);

		/*echo $result;
		exit;*/

		if($result)
		{
			$msg = JText::_('COM_BCTED_GUEST_LIST_REQUST_SUCCESS');
			$this->setRedirect($link,$msg);

			return true;
		}

		$msg = JText::_('COM_BCTED_GUEST_LIST_REQUST_ERROR');
		$this->setRedirect($link,$msg);

		return true;


		/*echo "<pre>";
		print_r($input);
		echo "</pre>In Guest list controller";
		data:protected] => Array
        (
            [option] => com_bcted
            [view] => clubguestlist
            [club_id] => 1
            [Itemid] => 129
            [requested_date] => 03/11/2015
            [requested_time] => 15:00:00
            [guest_count] => 6
            [male_count] => 3
            [female_count] => 3
            [additional_information] => this is additional information
            [task] => addGuestListRequest
            [15b5858306acc38f4d5d30d0a09a7938] => hookhoko37f32h9m8l33l0dkt4
        )
		exit;*/



		//$cid    = $input->get('cid', 0, 'int');
		//$itemid = $input->get('Itemid', 0, 'int');




	}

	public function getTableDetail()
	{
		$input  = JFactory::getApplication()->input;
		$tableID    = $input->get('table_id', 0, 'int');

		if(!$tableID)
		{
			echo "";
			exit;
		}

		$tableDetail = BctedHelper::getVenueTableDetail($tableID);

		if($tableDetail->premium_table_id)
		{
			$tableName = ucfirst($tableDetail->venue_table_name);
		}
		else
		{
			$tableName = ucfirst($tableDetail->custom_table_name);
		}

		$tableCapacity = $tableDetail->venue_table_capacity;

		echo $tableName."|".$tableCapacity;
		exit;
	}

	public function checkForTableAvaibility()
	{
		$input  = JFactory::getApplication()->input;
		$tableID    = $input->get('table_id', 0, 'int');
		$requested_date    = $input->get('requested_date', '', 'string');
		$from_time    = $input->get('from_time', '', 'string');
		$to_time    = $input->get('to_time', '', 'string');


		if(!$tableID)
		{
			echo "";
			exit;
		}

		$tableDetail = BctedHelper::getVenueTableDetail($tableID);

		if(!$tableDetail->venue_table_id)
		{
			echo "";
			exit;
		}

		$result = $this->checkForVenueTableAvaibility($tableDetail->venue_id,$tableDetail->venue_table_id,$requested_date,$from_time,$to_time);

		echo $result;
		exit;


	}

	public function checkForVenueTableAvaibility($venueID,$tableID,$date,$fromTime,$toTime)
	{
		$currentBookingDate = $date; //$tblVenuebooking->venue_booking_datetime;
		/*echo $currentBookingDate;
		exit;*/
		$datesArray         = array();
		$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' -1 day'));
		$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate)); //$tblVenuebooking->venue_booking_datetime;
		$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' +1 day'));
		$currentBookingFrom = $fromTime; // $tblVenuebooking->booking_from_time;
		$currentBookingTo   = $toTime; //$tblVenuebooking->booking_to_time;

		if (strtotime($currentBookingFrom)>strtotime($currentBookingTo))
		{
			$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
			$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo.' +1 day'));
		} else {
			$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
			$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo));
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('venue_table_id') . ' = ' . $db->quote($tableID))
			->where($db->quoteName('status') . ' = ' . $db->quote(5))
			->where($db->quoteName('venue_booking_datetime') . ' IN (\''. implode("','", $datesArray) .'\')' );
			// ->where($db->quoteName('venue_booking_id') . ' <> ' . $db->quote($tblVenuebooking->venue_booking_id))

		// Set the query and load the result.
		$db->setQuery($query);

		/*echo $query->dump();
		exit;*/

		$slotBooked = 0;
		$bookingsOnSameDate = $db->loadObjectList();
		$timeSlots = array();

		foreach ($bookingsOnSameDate as $key => $booking)
		{
			$bookingDate = $booking->venue_booking_datetime;
			$bookingFrom = $booking->booking_from_time;
			$bookingTo = $booking->booking_to_time;

			/*echo "<br />Date Time : " . $bookingDate .' '.$bookingTo;
			echo "<br />Combine : ".date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo.' +1 days'));
			exit;


			echo "<br />Date : " . $bookingDate;
			echo "<br />From : ".strtotime($bookingDate.' '.$bookingFrom);
			echo "<br />To : ".strtotime($bookingDate.' '.$bookingTo);*/
			//exit;

			if (strtotime($bookingDate.' '.$bookingFrom)>strtotime($bookingDate.' '.$bookingTo)) {
				$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
				$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo.' +1 days'));
			} else {
				$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
				$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo));
			}

			/*echo "<br /> Booking To Last : " .$bookingTo;
			exit;*/

			$numbercurrentBookingFrom = strtotime($currentBookingFrom);
			$numbercurrentBookingTo   = strtotime($currentBookingTo);
			$numberbookingFrom        = strtotime($bookingFrom);
			$numberbookingTo          = strtotime($bookingTo);

			/*echo "currentBookingFrom:" . $currentBookingFrom . " || ".$numbercurrentBookingFrom."<br />";
			echo "currentBookingTo:" . $currentBookingTo . " || ".$numbercurrentBookingTo."<br /><br />";
			echo "bookingFrom:" . $bookingFrom . " || ".$numberbookingFrom."<br />";
			echo "bookingTo:" . $bookingTo . " || ".$numberbookingTo."<br /><br />";*/

			if(($numberbookingFrom < $numbercurrentBookingFrom) && ($numbercurrentBookingFrom < $numberbookingTo))
			{
				$slotBooked = $slotBooked + 1;
			}
			else if(($numberbookingFrom < $numbercurrentBookingTo) && ($numbercurrentBookingTo < $numberbookingTo))
			{
				$slotBooked = $slotBooked + 1;
			}
			else if(($numberbookingTo == $numbercurrentBookingTo) && ($numbercurrentBookingFrom == $numberbookingFrom))
			{
				$slotBooked = $slotBooked + 1;
			}
		}

		/*echo $slotBooked;
		exit;*/

		if($slotBooked)
		{
			return 601;
		}

		return 200;
	}

	public function setUnpublished()
	{
		$input  = JFactory::getApplication()->input;
		$cid    = $input->get('cid', 0, 'int');
		$itemid = $input->get('Itemid', 0, 'int');

		if($cid)
		{
			$model = $this->getModel();
			$result = $model->setUnpublished($cid);

			//JFactory::getApplication()->enqueueMessage('Default media set successfully');

		}

		$this->setRedirect('index.php?option=com_heartdart&view=messages&Itemid='.$itemid);
	}
}
