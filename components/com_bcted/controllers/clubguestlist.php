<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerClubGuestList extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'ClubGuestList', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function updateremainingguest()
	{
		$input = JFactory::getApplication()->input;

		$requestID = $input->get('guestlist_request_id',0,'int');
		$guestCount = $input->get('updated_guest',0,'int');

		if(!$guestCount)
		{
			echo "0";
			exit;
		}

		$tblVenueguestlist = JTable::getInstance('Venueguestlist','BctedTable',array());
		$tblVenueguestlist->load($requestID);

		$tblVenueguestlist->remaining_total  = $tblVenueguestlist->remaining_total - $guestCount;

		if(!$tblVenueguestlist->store())
		{
			echo "0";
			exit;
		}

		echo "1";
		exit;
	}

	public function testing()
	{
		/*$app = JFactory::getApplication();
		$menu = $app->getMenu()->getActive()->link;
		$url = JRoute::_($menu);*/

		/*$menu = JFactory::getApplication()->getMenu();
		$parent = $menu->getActive();
		echo "<pre>";
		print_r($parent);
		echo "</pre>";
		exit;*/

		/*echo "hello";
		exit;*/

		$menuItem = BctedHelper::getBctedMenuItem('user-clubguestlist');
		/*echo "<pre>";
		print_r($menuItem);
		echo "</pre>";*/

		$Itemid =$menuItem->id;
		$link = $menuItem->link.'&Itemid='.$Itemid;

		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$link = $menu->getItem($itemid)->link;

		echo 'Itemid : ' . $itemid . ' || Link :'. JRoute::_($link);

		echo "<pre>";
		print_r($menu);
		echo "</pre>";
		exit;

	}

	public function addGuestListRequest()
	{
		$app  = JFactory::getApplication();
		$input = $app->input;
		/*echo "call";
		exit;*/

		$model = $this->getModel();



		/*$app = JFactory::getApplication();
		$menu = $app->getMenu()->getActive()->link;
		$url = JRoute::_($menu);
		----------------------------------------------
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$menuItem = $menu->getItems( 'link', 'index.php?option=com_example&view=reporting', true );
		echo JRoute::_('index.php?Itemid='.$menuItem->id);

		*/

		$user = JFactory::getUser();


		$clubID         = $input->get('club_id', 0, 'int');
		$reqDate        = $input->get('requested_date', '', 'string');
		$reqFromTime    = $input->get('requested_from_time', '', 'string');
		$reqToTime      = $input->get('requested_to_time', '', 'string');
		$guestCount     = $input->get('guest_count', 0, 'int');
		$maleCount      = $input->get('male_count', 0, 'int');
		$femaleCount    = $input->get('female_count', 0, 'int');
		$additionalInfo = $input->get('additional_information', '', 'string');


		/*$menuItem = BctedHelper::getBctedMenuItem('user-clubguestlist');

		$Itemid =$menuItem->id;
		$link = $menuItem->link.'&club_id='.$clubID.'&Itemid='.$Itemid;*/



		$menu = $app->getMenu();

		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=clubguestlist', true );
		$itemid = $menuItem->id;
		$link = $menuItem->link.'&club_id='.$clubID.'&Itemid='.$itemid;



		if(empty($reqDate))
		{
			$erMsg = JText::_('COM_BCTED_INVALIED_GUEST_LIST_REQUST_PARAMETERS');
			$this->setRedirect($link,$erMsg);

			return true;
		}

		if(!$clubID || !$guestCount)
		{
			$erMsg = JText::_('COM_BCTED_INVALIED_GUEST_LIST_REQUST_PARAMETERS');
			$this->setRedirect($link,$erMsg);

			return true;
		}

		$arrayData['venue_id']          = $clubID;
		$dateTime                       = date('Y-m-d', strtotime($reqDate));
		$arrayData['request_date']      = $dateTime;
		$arrayData['booking_from_time'] = $reqFromTime;
		$arrayData['booking_to_time']   = $reqToTime;
		$arrayData['user_id']           = $user->id;
		$arrayData['number_of_guest']   = $guestCount;
		$arrayData['male_count']        = $maleCount;
		$arrayData['female_count']      = $femaleCount;

		$arrayData['status']          = 1;
		$arrayData['user_status']     = 2;

		$arrayData['remaining_total']  = $guestCount;
		$arrayData['remaining_male']   = $maleCount;
		$arrayData['remaining_female'] = $femaleCount;

		$arrayData['additional_info']   = $additionalInfo;
		$arrayData['time_stamp']        = time();
		$arrayData['created']           = date('Y-m-d H:i:s');

		$result = $model->sendGuestListRequest($arrayData);

		/*echo $result;
		exit;*/

		if($result)
		{
			$msg = JText::_('COM_BCTED_GUEST_LIST_REQUST_SUCCESS');
			$this->setRedirect($link,$msg);

			return true;
		}

		$msg = JText::_('COM_BCTED_GUEST_LIST_REQUST_ERROR');
		$this->setRedirect($link,$msg);

		return true;


		/*echo "<pre>";
		print_r($input);
		echo "</pre>In Guest list controller";
		data:protected] => Array
        (
            [option] => com_bcted
            [view] => clubguestlist
            [club_id] => 1
            [Itemid] => 129
            [requested_date] => 03/11/2015
            [requested_time] => 15:00:00
            [guest_count] => 6
            [male_count] => 3
            [female_count] => 3
            [additional_information] => this is additional information
            [task] => addGuestListRequest
            [15b5858306acc38f4d5d30d0a09a7938] => hookhoko37f32h9m8l33l0dkt4
        )
		exit;*/



		//$cid    = $input->get('cid', 0, 'int');
		//$itemid = $input->get('Itemid', 0, 'int');




	}

	public function setUnpublished()
	{
		$input  = JFactory::getApplication()->input;
		$cid    = $input->get('cid', 0, 'int');
		$itemid = $input->get('Itemid', 0, 'int');

		if($cid)
		{
			$model = $this->getModel();
			$result = $model->setUnpublished($cid);

			//JFactory::getApplication()->enqueueMessage('Default media set successfully');

		}

		$this->setRedirect('index.php?option=com_heartdart&view=messages&Itemid='.$itemid);
	}
}
