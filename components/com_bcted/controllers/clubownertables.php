<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerClubOwnerTables extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'ClubOwnerTableEdit', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function save()
	{
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$input = $app->input;

		$file = $input->files->get('jform');

		/*echo "<pre>";
		print_r($file);
		echo "</pre>";*/
		/*exit;*/

		$data = $input->get('jform',array(),'array');

		$tableID = $input->get('venue_table_id',0,'int');
		$venueID = $input->get('venue_id',0,'int');
		/*echo "<pre>";
		print_r($input);
		echo "</pre>";
		exit;*/

		   /*[venue_table_id] => 8
    [venue_id] => 4
    [venue_table_name] => Gold Table
    [custom_table_name] => aa
    [venue_table_price] => 1000.00
    [venue_table_capacity] => 8
    [venue_table_description] => This gold table is perfect for an awesome night out

		*/
		$imagePath = BctedHelper::uplaodFile($file['venue_table_image'],'table',$user->id);

		if(!empty($imagePath))
		{
			$postData['venue_table_image'] = $imagePath;
		}

		if(!empty($data['venue_table_name']))
		{
			$postData['venue_table_name']        = $data['venue_table_name'];
		}

		if(!empty($data['custom_table_name']))
		{
			$postData['custom_table_name']        = $data['custom_table_name'];
		}

		if(empty($data['venue_table_name']))
		{
			$postData['venue_table_name']        = $data['custom_table_name'];
		}

		if(!empty($data['venue_table_price']))
		{
			$postData['venue_table_price']        = $data['venue_table_price'];
		}

		if(!empty($data['venue_table_capacity']))
		{
			$postData['venue_table_capacity']        = $data['venue_table_capacity'];
		}

		if(!empty($data['venue_table_description']))
		{
			$postData['venue_table_description']        = $data['venue_table_description'];
		}


		$postData['venue_table_id'] = $data['venue_table_id'];

		if($venueID)
		{
			$postData['venue_id'] = $venueID;
		}
		else
		{
			$postData['venue_id'] = $data['venue_id'];
		}

		$postData['user_id'] = $user->id;
		$postData['venue_table_active'] = 1;
		$postData['venue_table_created'] = date('Y-m-d H:i:s');
		$postData['venue_table_modified'] = date('Y-m-d H:i:s');
		$postData['time_stamp'] = time();


		//$data['is_smoking']    = $is_smoking;

		$model = $this->getModel();
		$result = $model->save($postData,$tableID);


		$menuItem = BctedHelper::getBctedMenuItem('club-owner-table');


		$Itemid =$menuItem->id;
		$link = $menuItem->link.'&Itemid='.$Itemid;

		/*$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$link = $menu->getItem($itemid)->link;

		echo 'Itemid : ' . $itemid . ' || Link :'. JRoute::_($link);

		echo "<pre>";
		print_r($menu);
		echo "</pre>";
		exit;*/

		$this->setRedirect($link,'Table Saved');

	}

	public function deleteTable()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		$tableID = $input->get('table_id',0,'int');
		$premiumID = $input->get('premium_id',0,'int');

		if(!$tableID)
		{
			echo "400";
			exit;
		}

		$model = $this->getModel();

		$result = $model->deleteTable($tableID,$premiumID);

		echo $result;
		$app->close();
		exit;

	}
}
