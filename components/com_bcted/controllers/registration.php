<?php
/**
 * @package     Heartdart.Administrator
 * @subpackage  com_heartdart
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Heartdart Messages Controller
 *
 * @since  0.0.1
 */
class BctedControllerRegistration extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'Registration', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function save()
	{
		/*$this->setRedirect('index.php?option=com_users&view=login',JText::_('COM_BCTED_REGISTRATION_CREATED_SUCCESSFULLY'));


		$app = JFactory::getApplication();
		$app->redirect('index.php?option=com_users&view=login',JText::_('COM_BCTED_REGISTRATION_CREATED_SUCCESSFULLY'));

		echo "hello";
		exit;*/
		$app = JFactory::getApplication();
		$input = $app->input;
		$data = $input->get('jform',array(),'array');
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// exit;
		/*
		Array
		(
		    [first_name] => a
		    [last_name] => b
		    [email] => seller1@gmail.com
		    [phoneno] => admin
		    [password] => admin123
		    [password2] => sdf
		)

		 */

		$email = trim($data['email']);
		$password = trim($data['password']);
		$password2 = trim($data['password2']);
		$flg = 0;

		if($password === $password2)
		{
			$flg = 1;
		}

		if($flg == 0)
		{
			// Redirect
		}

		$model = $this->getModel();
		$response = $model->save($data);

		if($response ==1)
		{
			$app = JFactory::getApplication();
			$menu = $app->getMenu();
			$menuItem = $menu->getItems( 'link', 'index.php?option=com_users&view=login', true );
			$app->redirect('index.php?option=com_users&view=login&Itemid='.$menuItem->id,JText::_('COM_BCTED_REGISTRATION_CREATED_SUCCESSFULLY'));
			//exit;
		}
		/*else if($response == 709)
		{
			$app = JFactory::getApplication();
			$menu = $app->getMenu();
			$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=registration', true );
			$app->redirect('index.php?option=com_bcted&view=registration&Itemid='.$menuItem->id,JText::_('COM_BCTED_REGISTRATION_PHONE_NUMBER_ALREADY_REGISTERED'));
			//exit;
		}*/
		else if($response == 701)
		{
			$app = JFactory::getApplication();
			$menu = $app->getMenu();
			$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=registration', true );
			$app->redirect('index.php?option=com_bcted&view=registration&Itemid='.$menuItem->id,JText::_('COM_BCTED_REGISTRATION_USERNAME_ALREADY_REGISTERED'));
			//exit;
		}
		else if($response == 702)
		{
			$app = JFactory::getApplication();
			$menu = $app->getMenu();
			$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=registration', true );

			$app->redirect('index.php?option=com_bcted&view=registration&Itemid='.$menuItem->id,JText::_('COM_BCTED_REGISTRATION_EMAIL_ALREADY_REGISTERED'));
			//exit;
		}
		else
		{
			$app = JFactory::getApplication();
			$menu = $app->getMenu();
			$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=registration', true );
			$app->redirect('index.php?option=com_bcted&view=registration&Itemid='.$menuItem->id,JText::_('COM_BCTED_REGISTRATION_NOT_REGISTERED'));
			//exit;
		}

		/*echo $response . " End of registration";
		exit;*/

		// 709 Phone number exists
		// 701 username exists
		// 702 email
		// 500 server error

	}


}
