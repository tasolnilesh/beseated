<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerCompanyServiceBooking extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'CompanyServiceBooking', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function testing()
	{
		/*$app = JFactory::getApplication();
		$menu = $app->getMenu()->getActive()->link;
		$url = JRoute::_($menu);*/

		/*$menu = JFactory::getApplication()->getMenu();
		$parent = $menu->getActive();
		echo "<pre>";
		print_r($parent);
		echo "</pre>";
		exit;*/

		/*echo "hello";
		exit;*/

		$menuItem = BctedHelper::getBctedMenuItem('user-clubguestlist');
		/*echo "<pre>";
		print_r($menuItem);
		echo "</pre>";*/

		$Itemid =$menuItem->id;
		$link = $menuItem->link.'&Itemid='.$Itemid;

		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$link = $menu->getItem($itemid)->link;

		echo 'Itemid : ' . $itemid . ' || Link :'. JRoute::_($link);

		echo "<pre>";
		print_r($menu);
		echo "</pre>";
		exit;

	}

	public function bookCompanyService()
	{
		$app  = JFactory::getApplication();
		$input = $app->input;

		$menu = $app->getMenu();

		/*echo "<pre>";
		print_r($menuItem);
		echo "</pre>";
		exit;*/

		$companyID = $input->get('company_id',0,'int');
		$serviceID = $input->get('service_id',0,'int');

		$requested_date         = $input->get('requested_date','','string');
		$timepicker_from         = $input->get('timepicker_from','','string');
		$timepicker_to         = $input->get('timepicker_to','','string');
		$location               = $input->get('location','','string');
		$guest_count            = $input->get('guest_count',0,'int');
		$male_count             = $input->get('male_count',0,'int');
		$female_count           = $input->get('female_count',0,'int');
		$additional_information = $input->get('additional_information','','string');

		$user = JFactory::getUser();

		/*$menuItem = BctedHelper::getBctedMenuItem('user-login');
		$Itemid = $menuItem->id;*/

		$menuItem = $menu->getItems( 'link', 'index.php?option=com_users&view=login', true );
		$itemid = $menuItem->id;
		$link = $menuItem->link.'&Itemid='.$Itemid;

		if(!$user->id)
		{
			$msg = JText::_('COM_BCTED_USER_SESSTION_NOT_FOUND');
			$app->redirect($link,$msg);
		}

		/*$menuItem = BctedHelper::getBctedMenuItem('user-company-services');
		$Itemid =$menuItem->id;*/

		$menuItem = $menu->getItems( 'link', 'index.php?option=com_bcted&view=companyservices', true );
		$Itemid = $menuItem->id;
		$link = 'index.php?option=com_bcted&view=userbookings&type=companyservices&Itemid='.$Itemid;
		//$link = 'index.php?option=com_bcted&view=companyservices'.'&company_id='.$companyID.'&Itemid='.$Itemid;



		if(!$serviceID || !$companyID || empty($requested_date) || empty($timepicker_from) || empty($timepicker_to) || !$guest_count)
		{
			$msg = JText::_('COM_BCTED_VENUE_TABLE_BOOKING_REQUST_INVALID_DATA');
			$app->redirect($link,$msg);
		}

		/*$fromTimeArray = explode(":", $timepicker_from);
		$toTimeArray = explode(":", $timepicker_to);

		if(count($fromTimeArray)==1)
		{
			$timepicker_from = $fromTimeArray[0].":"."00:00";
		}
		else if(count($fromTimeArray)==2)
		{
			$timepicker_from = $fromTimeArray[0].":".$fromTimeArray[1].":00";
		}
		else if(count($fromTimeArray)>=3)
		{
			$timepicker_from = $fromTimeArray[0].":".$fromTimeArray[1].":".$fromTimeArray[2];
		}

		if(count($toTimeArray)==1)
		{
			$timepicker_to = $toTimeArray[0].":"."00:00";
		}
		else if(count($toTimeArray)==2)
		{
			$timepicker_to = $toTimeArray[0].":".$toTimeArray[1].":00";
		}
		else if(count($toTimeArray)>=3)
		{
			$timepicker_to = $toTimeArray[0].":".$toTimeArray[1].":".$toTimeArray[2];
		}*/

		$timepicker_from = BctedHelper::convertToHMS($timepicker_from);
		$timepicker_to = BctedHelper::convertToHMS($timepicker_to);

		$totalHours = BctedHelper::differenceInHours($timepicker_from,$timepicker_to);

		/*echo $totalHours;
		exit;*/

		$model = $this->getModel();
		$tblService = $model->getTable('Service');
		$tblCompany = $model->getTable('Company');

		$tblService->load($serviceID);
		$tblCompany->load($companyID);


		/*$service_id
		echo "<pre>";
		print_r($tblService);
		echo "</pre>";
		exit;*/

		$priceInHours = $tblService->service_price * $totalHours;
		$extParam = BctedHelper::getExtensionParam_2();


		if($tblCompany->commission_rate == 0)
		{
			$commissionRate = $extParam->deposit_per;
		}
		else
		{
			$commissionRate = $tblCompany->commission_rate;
		}


		$postData['service_id']   = $serviceID;
		$postData['company_id']   = $companyID;
		$postData['user_id']      = $user->id;
		$postData['male_count']   = $male_count;
		$postData['female_count'] = $female_count;
		$postData['status']       = "1";
		$postData['user_status']  = "2";
		$postData['is_new']  = "1";
		$postData['longitude']    = '0.00';
		$postData['latitude']     = '0.00';

		$postData['service_booking_additional_info'] = $additional_information;
		$postData['service_booking_number_of_guest'] = $guest_count;
		$postData['service_booking_datetime']        = date('Y-m-d',strtotime($requested_date));
		$postData['booking_from_time']               = $timepicker_from;
		$postData['booking_to_time']                 = $timepicker_to;
		$postData['service_location']                = $location;
		$postData['service_booking_created']         = date('Y-m-d H:i:s');
		$postData['time_stamp']                      = time();

		$deposit_amount = (($commissionRate * $priceInHours)/100);

		$postData['total_price']    = $priceInHours;
		$postData['deposit_amount'] = $deposit_amount;
		$postData['amount_payable'] = $priceInHours - $deposit_amount;

		/*echo "<pre>";
		print_r($postData);
		echo "</pre>";
		exit;*/




		$response = $model->bookCompanyService($postData);

		/*echo $response;
		exit;*/

		if($response)
		{

			$msg = JText::_('COM_BCTED_CLUB_SERVICE_BOOKING_REQUST_SUCCESS');
			$app->redirect($link,$msg);
		}
		else
		{
			$msg = JText::_('COM_BCTED_CLUB_SERVICE_BOOKING_REQUST_ERRORS');
			$app->redirect($link,$msg);
		}


		return true;


		/*echo "<pre>";
		print_r($input);
		echo "</pre>In Guest list controller";
		data:protected] => Array
        (
            [option] => com_bcted
            [view] => clubguestlist
            [club_id] => 1
            [Itemid] => 129
            [requested_date] => 03/11/2015
            [requested_time] => 15:00:00
            [guest_count] => 6
            [male_count] => 3
            [female_count] => 3
            [additional_information] => this is additional information
            [task] => addGuestListRequest
            [15b5858306acc38f4d5d30d0a09a7938] => hookhoko37f32h9m8l33l0dkt4
        )
		exit;*/



		//$cid    = $input->get('cid', 0, 'int');
		//$itemid = $input->get('Itemid', 0, 'int');




	}

	public function getServiceDetail()
	{
		$input  = JFactory::getApplication()->input;
		$serviceID    = $input->get('service_id', 0, 'int');

		if(!$serviceID)
		{
			echo "";
			exit;
		}

		$serviceDetail = BctedHelper::getCompanyServiceDetail($serviceID);

		$serviceName = ucfirst($serviceDetail->service_name);


		//$tableCapacity = $serviceDetail->venue_table_capacity;

		echo $serviceName."|8";
		exit;
	}


}
