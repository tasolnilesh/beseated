<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerCompanyProfile extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'CompanyProfile', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function change_currency()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$company_id = $input->get('company_id',0,'int');
		$currency_code = $input->get('currency_code','','string');

		if(!$company_id || empty($currency_code))
		{
			echo "400";
			exit;
		}

		$data = array();

		if($currency_code == 'EUR')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "€";
		}
		else if ($currency_code == 'GBP')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "£";
		}
		else if ($currency_code == 'AED')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "AED";
		}
		else if ($currency_code == 'USD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}
		else if ($currency_code == 'CAD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}
		else if ($currency_code == 'AUD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}

		$model = $this->getModel();

		if($model->is_booking_in_company($company_id) == 0)
		{
			$result = $model->saveCompanyProfile($data,$company_id);

			if($result)
			{
				echo "200";
				exit;
			}

			echo "500";
			exit;
		}


		echo "707";
		exit;

	}

	public function save()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		$model = $this->getModel();


		$user = JFactory::getUser();

		$company_id      = $input->get('company_id','','string');
		$company_name    = $input->get('company_name','','string');
		$company_address = $input->get('company_address','','string');
		$city = $input->get('city','','string');
		$company_about   = $input->get('company_about','','string');
		$latitude        = $input->get('latitude','','string');
		$longitude       = $input->get('longitude','','string');
		//$currency_code   = $input->get('currency_code','','string');

		$latlong = BctedHelper::getLatitudeAndLongitude($company_address);
		$city = BctedHelper::getAddressDetail($company_address);
		//exit;
		//echo $latlong;
		$latlongArray = explode("|", $latlong);
		$latitude = $latlongArray[0];
		$longitude = $latlongArray[1];
		//exit;

		$data = array();

		/*if($currency_code == 'EUR')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "€";
		}
		else if ($currency_code == 'GBP')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "£";
		}
		else if ($currency_code == 'AED')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "AED";
		}
		else if ($currency_code == 'USD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}
		else if ($currency_code == 'CAD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}
		else if ($currency_code == 'AUD')
		{
			$data['currency_code'] = $currency_code;
			$data['currency_sign'] = "$";
		}*/

		//$data['venue_name']    = $venue_name;
		//$data['venue_address'] = $venue_address;



		$file = $input->files->get('company_image');
		$imagePath = BctedHelper::uplaodFile($file,'company',$user->id);

		if(!empty($imagePath))
		{
			$data['company_image'] = $imagePath;
		}

		if(!empty($city))
		{
			$data['city'] = $city;
		}

		$data['company_name']    = $company_name;
		$data['company_address'] = $company_address;
		$data['company_about']   = $company_about;
		$data['latitude']        = $latitude;
		$data['longitude']       = $longitude;
		//$data['currency_code']   = $currency_code;




		if($company_id)
		{
			$data['company_modified'] = date('Y-m-d H:i:s');
		}

		if(count($data) != 0)
		{
			/*echo $venue_id."<pre>";
			print_r($data);
			echo "</pre>";
			exit;*/
			$result = $model->saveCompanyProfile($data,$company_id);
		}
		else
		{
			$result = false;
		}


		/*echo "<pre>";
		print_r($input);
		echo "</pre>";*/
		//exit;
		/*$app = JFactory::getApplication();
		$menu = $app->getMenu()->getActive()->link;
		$url = JRoute::_($menu);*/

		/*$menu = JFactory::getApplication()->getMenu();
		$parent = $menu->getActive();
		echo "<pre>";
		print_r($parent);
		echo "</pre>";
		exit;*/

		/*echo "hello";
		exit;*/

		$menuItem = BctedHelper::getBctedMenuItem('company-profile');


		$Itemid =$menuItem->id;
		$link = $menuItem->link.'&Itemid='.$Itemid;

		/*$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$link = $menu->getItem($itemid)->link;

		echo 'Itemid : ' . $itemid . ' || Link :'. JRoute::_($link);

		echo "<pre>";
		print_r($menu);
		echo "</pre>";
		exit;*/

		$this->setRedirect($link,'Profile Saved');

	}
}
