<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerClubFriendsAttending extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'ClubFriendsAttending', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function addMeForVenueTable()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		/*echo "<pre>";
		print_r($input);
		echo "</pre>";*/

		$user = JFactory::getUser();

		$bookingID = $input->get('booking_id',0,'int');
		/*$tableID   = $input->get('',0,'int');
		$userID    = $input->get('',0,'int');
		$venueID   = $input->get('',0,'int');*/
		$message   = $input->get('message','','string');

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblTable = JTable::getInstance('Table', 'BctedTable');
		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');
		$tblVenuebooking->load($bookingID);

		/*echo "<pre>";
		print_r($tblVenuebooking);
		echo "</pre>";*/
		$venueID = $tblVenuebooking->venue_id;
		$tableID = $tblVenuebooking->venue_table_id;
		$userID = $tblVenuebooking->user_id;

		$tblVenue->load($venueID);
		$tblTable->load($tableID);

		if(empty($message) || !$userID)
		{
			echo "400";
			exit;
		}

		$tblVenuetableinvite = JTable::getInstance('Venuetableinvite', 'BctedTable');
		$tblVenuetableinvite->load(0);

		$postData['booking_id']   = $bookingID;
		$postData['table_id']   = $tableID;
		$postData['venue_id']   = $venueID;
		$postData['user_id']    = $user->id;
		$postData['message']    = $message;
		$postData['time_stamp'] = time();
		$postData['created']    = date('Y-m-d H:i:s');

		$tblVenuetableinvite->bind($postData);

		if(!$tblVenuetableinvite->store())
		{
			echo "500";
			exit;
		}

		$extraParam = array();
		$extraParam['inviteID'] = $tblVenuetableinvite->invite_id;

		if($tblTable->premium_table_id)
		{
			$extraParam['tableName'] = $tblTable->venue_table_name;
			$message = JText::sprintf('COM_BESEATED_VENUE_INVITE_ME_TO_VENUE_TABLE',$user->name,$tblTable->venue_table_name,$tblVenue->venue_name,date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime)));
			$messageSend = $message;
			$message .= ";".$tblTable->venue_table_name.";".date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime));
		}
		else
		{
			$extraParam['tableName'] = $tblTable->custom_table_name;
			$message = JText::sprintf('COM_BESEATED_VENUE_INVITE_ME_TO_VENUE_TABLE',$user->name,$tblTable->custom_table_name,$tblVenue->venue_name,date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime)));
			$messageSend = $message;
			$message .= ";".$tblTable->custom_table_name.";".date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime));
		}

		BctedHelper::sendMessage($tblVenue->venue_id,0,0,$tblTable->venue_table_id,$userID,$messageSend,$extraParam,'tableaddme');

		$jsonarray['pushNotificationData']['id']         = $tblVenuetableinvite->invite_id;
		$jsonarray['pushNotificationData']['to']         = $userID;
		$jsonarray['pushNotificationData']['message']    = $message;
		$jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_REQUESTFORADDMEATTABLE'); //'RequestForAddMeAtTable';
		$jsonarray['pushNotificationData']['configtype'] = '';

		BctedHelper::sendPushNotification($jsonarray);

		echo "200";
		exit;
	}
}
