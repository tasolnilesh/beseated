<?php
/**
 * @package     Bcted.Administrator
 * @subpackage  com_bcted
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bcted Guest List Controller
 *
 * @since  0.0.1
 */
class BctedControllerCompanyRequestDetail extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.
	 * @return  ContentControllerArticles
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		//$this->registerTask('unfeatured',	'featured');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   0.0.1
	 */
	public function getModel($name = 'CompanyRequestDetail', $prefix = 'BctedModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function changeRequstStatus()
	{
		$app  = JFactory::getApplication();
		$input = $app->input;

		$model = $this->getModel();



		/*$app = JFactory::getApplication();
		$menu = $app->getMenu()->getActive()->link;
		$url = JRoute::_($menu);
		----------------------------------------------
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$menuItem = $menu->getItems( 'link', 'index.php?option=com_example&view=reporting', true );
		echo JRoute::_('index.php?Itemid='.$menuItem->id);

		*/

		$user = JFactory::getUser();


		$requestID      = $input->get('request_id', 0, 'int');
		$status        = $input->get('status', '', 'string');
		$owner_message        = $input->get('owner_message', '', 'string');

		if(empty($status))
		{
			$erMsg = JText::_('COM_BCTED_INVALIED_GUEST_LIST_REQUST_PARAMETERS');
			//$this->setRedirect($link,$erMsg);
			echo "0";
			exit;
		}

		if(!$requestID)
		{
			echo "0";
			exit;
		}

		$result = $model->changeRequestStatus($requestID,$status,$owner_message);

		if($result)
		{
			echo "1";
			exit;
		}

		echo "0";
		exit;
	}

	public function deletePastBooking()
	{
		$app = JFactory::getApplication();
		$input = $app->input;

		$bookingID = $input->get('booking_id',0,'int');
		$user_type = $input->get('user_type','','string');

		if(empty($user_type))
		{
			echo "400";
			exit;
		}

		$model = $this->getModel();
		$response = $model->deleteBooking($bookingID,$user_type);

		echo $response;
		exit;
	}
}
