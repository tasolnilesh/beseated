<?php
/**
 * @package     IJoomer.Frontend
 * @subpackage  com_ijoomeradv.models
 *
 * @copyright   Copyright (C) 2010 - 2014 Tailored Solutions PVT. Ltd. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die;

/**
 * The Class For The IJoomeradvModelijoomeradv which will extends JModelLegacy
 *
 * @package     IJoomer.Frontend
 * @subpackage  com_ijoomeradv.model
 * @since       1.0
 */
class IjoomeradvModelijoomeradv extends JModelLegacy
{
	private $db;

	private $mainframe;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->db = JFactory::getDBO();
		$this->mainframe = JFactory::getApplication();
	}

	/**
	 * fetches ijoomeradv global config
	 *
	 * @return  it will return loadobjectlist
	 */
	public function getApplicationConfig()
	{
		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('name, value')
			->from($this->db->qn('#__ijoomeradv_config'));

		// Set the query and load the result.
		$this->db->setQuery($query);

		try
		{
			$result = $this->db->loadObjectList();

			return $result;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Fetches All Published Extensions
	 *
	 * @return  it will return the value of components
	 */
	public function getExtensions()
	{
		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('name,options,classname')
			->from($this->db->qn('#__ijoomeradv_extensions'))
			->where($this->db->qn('published') . ' = ' . $this->db->q(1));
		/*echo $query->dump();
		exit;*/

		// Set the query and load the result.
		$this->db->setQuery($query);

		try
		{
			$components = $this->db->loadObjectList();

			return $components;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Method to get the available viewnames.
	 *
	 * @return   array    Array of viewnames.
	 *
	 * @since    1.0
	 */
	public function getViewNames()
	{
		jimport('joomla.filesystem.file');

		$components = $this->getExtensions();

		foreach ($components as $component)
		{
			$mainXML = JPATH_SITE . '/components/com_ijoomeradv/extensions/' . $component->classname . '.xml';

			if (is_file($mainXML))
			{
				$options[$component->classname] = $this->getTypeOptionsFromXML($mainXML);
			}
		}

		return $options;
	}

	/**
	 * Get Type Options FromXML
	 *
	 * @param   [type]  $file  contains the value of file
	 *
	 * @return  array  $options
	 */
	private function getTypeOptionsFromXML($file)
	{
		$options = array();

		if ($xml = simplexml_load_file($file))
		{
			$views = $xml->xpath('views');

			if (!empty($views))
			{
				foreach ($views[0]->view as $value)
				{
					$options[] = (string) $value->remoteTask;
				}
			}
		}

		return $options;
	}

	/**
	 * Fetches ijoomeradv Menu items
	 *
	 * @return array it will return Menu Array
	 */
	public function getMenus()
	{
		$menuArray = array();
		$positionScreens = array();
		$user = JFactory::getUser();
		$device = IJReq::getTaskData('device');

		if ($device == 'android')
		{
			$menudevice = 2;
			$device_type = IJReq::getTaskData('type', 'hdpi');
		}
		elseif ($device == 'iphone')
		{
			$menudevice = 3;
			$device_type = IJReq::getTaskData('type', '3');

			if ($device_type == 5)
			{
				$device_type = 4;
			}
		}

		$groups = implode(',', $user->getAuthorisedViewLevels());

		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($this->db->qn('#__ijoomeradv_menu_types'));

		// Set the query and load the result.
		$this->db->setQuery($query);

		$menus = $this->db->loadObjectList();

		$i = 0;

		if (!empty($menus))
		{
			foreach ($menus as $value)
			{
				if ($value->position == 1)
				{
					$screennames = json_decode('[]');
				}
				else
				{
					$screens = json_decode($value->screen);
					$screennames = array();

					if ($screens)
					{
						foreach ($screens as $val)
						{
							foreach ($val as $screen)
							{
								$screenname = (explode('.', $screen));
								$screennames[] = $screenname[2];
								$positionScreens[$value->position][] = $screenname[2];
							}
						}
					}
				}

				if (($screennames && $value->position > 1) || $value->position == 1)
				{
					$menuArray[$i] = array("menuid" => $value->id,
						"menuname" => $value->title,
						"menuposition" => $value->position,
						"screens" => $screennames
					);

					// Add IF condition for if menuitem for specific device avail or not
					// if global selected then check avaibility in menu
					$query = $this->db->getQuery(true);

					// Create the base select statement.
					$query->select('*')
						->from($this->db->qn('#__ijoomeradv_menu'))
						->where($this->db->qn('menutype') . ' = ' . $this->db->q($value->id))
						->where($this->db->qn('published') . ' = ' . $this->db->q('1'))
						->where($this->db->qn('access') . ' IN ( ' . $groups . ')')
						->order($this->db->qn('ordering') . ' ASC');

					// Set the query and load the result.
					$this->db->setQuery($query);

					$menuitems = $this->db->loadObjectList();

					$k = 0;
					$menuArray[$i]["menuitem"] = array();

					if (!empty($menuitems))
					{
						foreach ($menuitems as $value1)
						{
							$viewname = explode('.', $value1->views);

							$remotedata = json_decode($value1->menuoptions);

							if ($remotedata)
							{
								$remotedata = $remotedata->remoteUse;
							}
							else
							{
								$remotedata = '';
							}

							$menuArray[$i]["menuitem"][$k] = array("itemid" => $value1->id,
								"itemcaption" => $value1->title,
								"itemview" => $viewname[3],
								"itemdata" => $remotedata
							);

							if (( $value->position == 1 or $value->position == 2) && ( $value1->itemimage))
							{
								$menuArray[$i]["menuitem"][$k]["icon"] = JURI::base() . 'administrator/components/com_ijoomeradv/theme/custom/' . $device . '/' . $device_type . '/' . $value1->itemimage . '_icon.png';
							}
							elseif ($value->position == 3 && $value1->itemimage)
							{
								$menuArray[$i]["menuitem"][$k]["tab"] = JURI::base() . 'administrator/components/com_ijoomeradv/theme/custom/' . $device . '/' . $device_type . '/' . $value1->itemimage . '_tab.png';
								$menuArray[$i]["menuitem"][$k]["tab_active"] = JURI::base() . 'administrator/components/com_ijoomeradv/theme/custom/' . $device . '/' . $device_type . '/' . $value1->itemimage . '_tab_active.png';
							}

							$k++;
						}
					}

					$i++;
				}
			}
		}

		return $menuArray;
	}

	/**
	 * Set request variable from menu id
	 *
	 * @param   [type]  $menuid  contains menu id
	 *
	 * @return boolean it will return a value in true or false
	 */
	public function setMenuRequest($menuid)
	{
		$mainframe = JFactory::getApplication();

		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($this->db->qn('#__ijoomeradv_menu'))
			->where($this->db->qn('id') . ' = ' . $this->db->q($menuid));

		// Set the query and load the result.
		$this->db->setQuery($query);

		$menuobject = $this->db->loadObject();

		if ($menuobject)
		{
			// Set reqObject as per menuid request
			$views = explode('.', $menuobject->views);
			$mainframe->IJObject->reqObject->extName = $views[0];
			$mainframe->IJObject->reqObject->extView = $views[1];
			$mainframe->IJObject->reqObject->extTask = $views[2];

			// Set required data for menu request
			$menuoptions = json_decode($menuobject->menuoptions);

			foreach ($menuoptions->remoteUse as $key => $value)
			{
				$mainframe->IJObject->reqObject->taskData->$key = $value;
			}
		}

		return true;
	}

	/**
	 * Fetches IJoomeradv Global Config
	 *
	 * @param   [type]  $extName  contains the ExtName
	 *
	 * @return  it will return loadobjectlist
	 */
	public function getExtensionConfig($extName)
	{
		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('name, value')
			->from($this->db->qn('#__ijoomeradv_' . $extName . '_config'));

		// Set the query and load the result.
		$this->db->setQuery($query);

		try
		{
			$result = $this->db->loadObjectList();

			// return config list
			return $result;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Fetches IJoomeradv Custom Views Detail
	 *
	 * @return  it will return the value of $customeView
	 */
	public function getCustomView()
	{
		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($this->db->qn('#__ijoomeradv_menu'))
			->where($this->db->qn('published') . ' = ' . $this->db->q('1'))
			->where($this->db->qn('type') . ' = ' . $this->db->q('Custom'));

		// Set the query and load the result.
		$this->db->setQuery($query);

		try
		{
			$customView = $this->db->loadObjectList();

			return $customView;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Fetches IJoomeradv Default Home Views
	 *
	 * @return  it will return loadobject
	 */
	public function getHomeMenu()
	{
		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($this->db->qn('#__ijoomeradv_menu'))
			->where($this->db->qn('published') . ' = ' . $this->db->q('1'))
			->where($this->db->qn('home') . ' = ' . $this->db->q('1'));

		// Set the query and load the result.
		$this->db->setQuery($query);

		try
		{
			$result = $this->db->loadObjectList();

			return $result;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Check Ioomer Extension And Related Joomla Component If Installed And Enabled
	 *
	 * @param   [type]  $extName  contains the value of Extension Name
	 *
	 * @return  boolean it will return the value in true or false
	 */
	public function checkIJExtension($extName)
	{
		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('options')
			->from($this->db->qn('#__ijoomeradv_extensions'))
			->where($this->db->qn('classname') . ' = ' . $this->db->q($extName));

		$this->db->setQuery($query);

		$option = $this->db->loadResult(); // get component name from the extension name

		if (!$option)
		{
			IJReq::setResponseCode(404);

			return false;
		}
		else
		{
			// Create hepler object
			$IJHelperObj = new ijoomeradvHelper;

			if (!$IJHelperObj->getComponent($option))
			{
				IJReq::setResponseCode(404);

				return false;
			}
		}

		return true;
	}

	/**
	 * The LoginProccess Function
	 *
	 * @return  array it will return jsson array
	 */
	public function loginProccess()
	{
		$data['latitude'] = IJReq::getTaskData('lat');
		$data['longitude'] = IJReq::getTaskData('long');
		$data['device_token'] = IJReq::getTaskData('devicetoken');
		$data['device_type'] = IJReq::getTaskData('type');

		// Get current user
		$my = JFactory::getUser();

		// @TODO : extension levels default params
		$defaultParams = '{"pushnotif_profile_activity_add_comment":"1","pushnotif_profile_activity_reply_comment":"1","pushnotif_profile_status_update":"1","pushnotif_profile_like":"1","pushnotif_profile_stream_like":"1","pushnotif_friends_request_connection":"1","pushnotif_friends_create_connection":"1","pushnotif_inbox_create_message":"1","pushnotif_groups_invite":"1","pushnotif_groups_discussion_reply":"1","pushnotif_groups_wall_create":"1","pushnotif_groups_create_discussion":"1","pushnotif_groups_create_news":"1","pushnotif_groups_create_album":"1","pushnotif_groups_create_video":"1","pushnotif_groups_create_event":"1","pushnotif_groups_sendmail":"1","pushnotif_groups_member_approved":"1","pushnotif_groups_member_join":"1","pushnotif_groups_notify_creator":"1","pushnotif_groups_discussion_newfile":"1","pushnotif_events_invite":"1","pushnotif_events_invitation_approved":"1","pushnotif_events_sendmail":"1","pushnotif_event_notify_creator":"1","pushnotif_event_join_request":"1","pushnotif_videos_submit_wall":"1","pushnotif_videos_reply_wall":"1","pushnotif_videos_tagging":"1","pushnotif_videos_like":"1","pushnotif_photos_submit_wall":"1","pushnotif_photos_reply_wall":"1","pushnotif_photos_tagging":"1","pushnotif_photos_like":"1"}';

		if(!empty($data['device_token']))
		{
			$query = $this->db->getQuery(true);

			// Create the base delete statement.
			$query->delete()
				->from($this->db->quoteName('#__ijoomeradv_users'))
				->where($this->db->quoteName('device_token') . ' = ' . $this->db->quote($data['device_token']));

			// Set the query and execute the delete.
			$this->db->setQuery($query);
			$this->db->execute();
		}



		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('count(1)')
			->from($this->db->qn('#__ijoomeradv_users'))
			->where($this->db->qn('userid') . ' = ' . $this->db->q($my->id));

		// Set the query and load the result.
		$this->db->setQuery($query);

		$user = $this->db->loadResult();

		$query = $this->db->getQuery(true);

		if ($user)
		{
			// Create the base update statement.
			$query->update($this->db->qn('#__ijoomeradv_users'))
				->set($this->db->qn('device_token') . ' = ' . $this->db->q($data['device_token']))
				->set($this->db->qn('device_type') . ' = ' . $this->db->q($data['device_type']))
				->where($this->db->qn('userid') . ' = ' . $this->db->q($my->id));
		}
		else
		{
			// Create the base insert statement.
			$query->insert($this->db->qn('#__ijoomeradv_users'))
				->columns(
					array(
						$this->db->qn('userid'),
						$this->db->qn('jomsocial_params'),
						$this->db->qn('device_token'),
						$this->db->qn('device_type')
						)
					)
				->values(
					$this->db->q($my->id) . ', ' .
					$this->db->q($defaultParams) . ', ' .
					$this->db->q($data['device_token']) . ', ' .
					$this->db->q($data['device_type'])
					);
		}

		$this->db->setQuery($query);
		$this->db->execute();

		$jsonarray['code'] = 200;
		$jsonarray['profile'] = IJOOMER_GC_REGISTRATION;

		if (strtolower(IJOOMER_GC_REGISTRATION) === 'jomsocial' && file_exists(JPATH_ROOT . '/components/com_community/libraries/core.php'))
		{
			require_once JPATH_ROOT . '/components/com_community/libraries/core.php';

			// Update jomsocial latitude & longitude if not 0
			if ($data['latitude'] != 0 && $data['longitude'] != 0)
			{
				$query = $this->db->getQuery(true);

				// Create the base update statement.
				$query->update($this->db->qn('#__community_users'))
					->set($this->db->qn('latitude') . ' = ' . $this->db->q($data['latitude']))
					->set($this->db->qn('longitude') . ' = ' . $this->db->q($data['longitude']))
					->where($this->db->qn('userid') . ' = ' . $this->db->q($my->id));

				$this->db->setQuery($query);
				$this->db->execute();
			}
		}

		// Change for id based push notification
		$component = JComponentHelper::getComponent('com_community');

		if (!empty($component->id) && $component->enabled == 1)
		{
			$friendsModel = CFactory::getModel('friends');
			$friends = $friendsModel->getFriendIds($my->id);
			$pushOptions['detail'] = array();
			$pushOptions = gzcompress(json_encode($pushOptions));
			$message = JText::sprintf('COM_IJOOMERADV_USER_ONLINE', $my->name);
			$obj = new stdClass;
			$obj->id = null;
			$obj->detail = $pushOptions;
			$obj->tocount = count($friends);
			$this->db->insertObject('#__ijoomeradv_push_notification_data', $obj, 'id');

			if ($obj->id)
			{
				$jsonarray['pushNotificationData']['id'] = $obj->id;
				$jsonarray['pushNotificationData']['to'] = implode(',', $friends);
				$jsonarray['pushNotificationData']['message'] = $message;
				$jsonarray['pushNotificationData']['type'] = 'online';
				$jsonarray['pushNotificationData']['configtype'] = '';
			}
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblBctUserProfile       = JTable::getInstance('Profile', 'BctedTable');

		$profileID = $this->getUserProfileID($my->id);
		$tblBctUserProfile->load($profileID);

		$params = json_decode($tblBctUserProfile->params);

		$jsonarray['userData']['userID'] = $my->id;
		$jsonarray['userData']['profileID'] = $tblBctUserProfile->profile_id;
		$jsonarray['userData']['firstName'] = $my->name;
		$jsonarray['userData']['email'] = $my->email;
		$jsonarray['userData']['lastName']  = $tblBctUserProfile->last_name;
		$jsonarray['userData']['phoneno']   = $tblBctUserProfile->phoneno;
		$jsonarray['userData']['avatar']    = $tblBctUserProfile->avatar;
		$jsonarray['userData']['about']     = $tblBctUserProfile->about;

		if(isset($params->settings->social->connectToFacebook))
		{
			$jsonarray['settings']['connectToFacebook']     = (string) $params->settings->social->connectToFacebook;
		}

		if(isset($params->settings->pushNotification->receiveRequest))
		{
			$jsonarray['settings']['receiveRequest']     = (string) $params->settings->pushNotification->receiveRequest;
		}

		$jsonarray['settings']['receiveMessage']        = (string) $params->settings->pushNotification->receiveMessage;
		$jsonarray['settings']['updateMyBookingStatus'] = (string) $params->settings->pushNotification->updateMyBookingStatus;

		$jsonarray['timestamp'] = time();

		$bctedConfig = $this->getExtensionParam();

		$loyaltyConfig = array();

		$loyaltyConfig['lowest']['min']=$bctedConfig->loyalty_level_lowest_min;
		$loyaltyConfig['lowest']['max']=$bctedConfig->loyalty_level_lowest_max;
		$loyaltyConfig['lowest']['color']=$bctedConfig->loyalty_level_lowest_color;
		$loyaltyConfig['middle']['min']=$bctedConfig->loyalty_level_middle_min;
		$loyaltyConfig['middle']['max']=$bctedConfig->loyalty_level_middle_max;
		$loyaltyConfig['middle']['color']=$bctedConfig->loyalty_level_middle_color;
		$loyaltyConfig['highest']['min']=$bctedConfig->loyalty_level_highest;
		$loyaltyConfig['highest']['max']=$bctedConfig->loyalty_level_highest;
		$loyaltyConfig['highest']['color']=$bctedConfig->loyalty_level_highest_color;

		$jsonarray['loyalty'] = $loyaltyConfig;



		/*

		 [loyalty_level_lowest_min] => 1
    [loyalty_level_lowest_max] => 100000
    [loyalty_level_lowest_color] => #000000
    [loyalty_level_middle_min] => 100001
    [loyalty_level_middle_max] => 1000000
    [loyalty_level_middle_color] => #fbb829
    [loyalty_level_highest] => 1000000
    [loyalty_level_highest_color] => #ffffff
		 */

		$groups = $my->get('groups');

		if(in_array($bctedConfig->club, $groups))
		{
			$jsonarray['userType'] = 'Club';
		}
		else if(in_array($bctedConfig->service_provider, $groups))
		{
			$jsonarray['userType'] = 'ServiceProvider';
		}
		else if(in_array($bctedConfig->guest, $groups))
		{
			$jsonarray['userType'] = 'Registered';
		}

		//JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');


		if($jsonarray['userType'] == "Club")
		{
			$elementID = $this->getUserVenueID($my->id);
			$tblVenue       = JTable::getInstance('Venue', 'BctedTable');
			$tblVenue->load($elementID);
			$licenceType = $tblVenue->licence_type;

			$jsonarray['currencyCode'] = $tblVenue->currency_code;
			$jsonarray['currencySign'] = $tblVenue->currency_sign;
			$jsonarray['fromTime']     = ($tblVenue->from_time)?$tblVenue->from_time:"08:00:00";
			$jsonarray['toTime']       = ($tblVenue->to_time)?$tblVenue->to_time:"23:59:00";

		}
		else if($jsonarray['userType'] == "ServiceProvider")
		{
			$elementID = $this->getUserCompanyID($my->id);
			$tblCompany       = JTable::getInstance('Company', 'BctedTable');
			$tblCompany->load($elementID);
			$licenceType = $tblCompany->licence_type;

			$jsonarray['currencyCode'] = $tblCompany->currency_code;
			$jsonarray['currencySign'] = $tblCompany->currency_sign;
		}
		else
		{
			$elementID = $this->getUserProfileID($my->id);

			$ratingBooking = $this->getUserLastBookingDetail($my->id);

			$jsonarray['lastBookingRating'] = $ratingBooking;
			$licenceType = "";

			$fbID         = IJReq::getTaskData('fbID','');
			$fb           = IJReq::getTaskData('fb',0,'int');
			$friendsFbIds = IJReq::getTaskData('friendsFbIds','','string');

			if($fb)
			{
				$this->checkForFacebookUser($my->id,$fbID,$friendsFbIds);
			}

		}

		$jsonarray['elementID']   = $elementID;
		$jsonarray['licenceType'] = $licenceType;

		return $jsonarray;
	}

	public function checkForFacebookUser($userID,$fbID,$friendsFbIds)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__facebook_joomla_connect'))
			->where($db->quoteName('joomla_userid') . ' = ' . $db->quote($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		$facebookUser = $db->loadObject();

		if($facebookUser)
		{
			$updateFBUser = $db->getQuery(true);

			// Create the base update statement.
			$updateFBUser->update($db->quoteName('#__facebook_joomla_connect'))
				->set($db->quoteName('facebook_userid') . ' = ' . $db->quote($fbID))
				->set($db->quoteName('facebook_friends') . ' = ' . $db->quote($friendsFbIds))
				->set($db->quoteName('joined_date') . ' = ' . $db->quote(time()))
				->set($db->quoteName('linked') . ' = ' . $db->quote(1))
				->where($db->quoteName('joomla_userid') . ' = ' . $db->quote($userID));

			// Set the query and execute the update.
			$db->setQuery($updateFBUser);

			/*echo $updateFBUser->dump();
			exit;*/

			$db->execute();
		}
		else
		{
			$insertFBUser = $db->getQuery(true);

			// Create the base insert statement.
			$insertFBUser->insert($db->quoteName('#__facebook_joomla_connect'))
				->columns(
					array(
						$db->quoteName('joomla_userid'),
						$db->quoteName('facebook_userid'),
						$db->quoteName('facebook_friends'),
						$db->quoteName('joined_date'),
						$db->quoteName('linked')
					)
				)
				->values(
					$db->quote($userID) . ', ' .
					$db->quote($fbID) . ', ' .
					$db->quote($friendsFbIds) . ', ' .
					$db->quote(time()) . ', ' .
					$db->quote(1)
				);

			// Set the query and execute the insert.
			$db->setQuery($insertFBUser);

			$db->execute();

		}

	}
	/*public function getUserProfileID($userID)
	{
		// Initialiase variables.
		$profileID = 0;
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('profile_id')
			->from($db->quoteName('#__bcted_user_profile'))
			->where($db->quoteName('userid') . ' = ' . $db->quote($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		$profileID = $db->loadResult();

		return $profileID;
	}*/

	public function alreadyRatedServices($userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('rated_id')
			->from($db->quoteName('#__bcted_ratings'))
			->where($db->quoteName('user_id') . ' = ' . $db->quote($userID))
			->where($db->quoteName('rating_type') . ' = ' . $db->quote('service'));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadColumn();

		return $result;
	}

	public function alreadyRatedVenues($userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('rated_id')
			->from($db->quoteName('#__bcted_ratings'))
			->where($db->quoteName('user_id') . ' = ' . $db->quote($userID))
			->where($db->quoteName('rating_type') . ' = ' . $db->quote('venue'));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadColumn();

		return $result;
	}

	public function getUserLastBookingDetail($userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$currentDateTime = date('Y-m-d H:i:s');

		/*$currentTime = time();
		$strtotime('+1 day', $currentTime);*/

		$query->select('sb.*')
			->from($db->quoteName('#__bcted_service_booking','sb'))
			->where($db->quoteName('sb.user_id') . ' = ' . $db->quote($userID))
			->where($db->quoteName('sb.is_rated') . ' = ' . $db->quote('0'))
			->where($db->quoteName('sb.service_booking_datetime') . ' <= ' . $db->quote($currentDateTime))
			->where($db->quoteName('sb.is_deleted') . ' = ' . $db->quote('0'))
			->where($db->quoteName('sb.status') . ' = ' . $db->quote('5'))
			->where($db->quoteName('sb.user_status') . ' = ' . $db->quote('5'));



			//->order($db->quoteName('sb.venue_booking_datetime') . ' ASC');

		$query->select('s.service_name')
			->join('LEFT','#__bcted_company_services AS s ON s.service_id=sb.service_id');

		$query->select('c.company_name')
			->join('LEFT','#__bcted_company AS c ON c.company_id=sb.company_id');

		/*echo $query->dump();
		exit;*/

		/*$query = "SELECT *
			FROM `#__bcted_service_booking`
			WHERE `user_id` = '{$userID}' AND `is_rated` = '0' AND `service_booking_datetime` < '{$currentDateTime}' AND `is_deleted`='0' AND `status`='5' AND `user_status`='5'";*/

		// Create the base select statement.
		/*$query->select('*,max(time_stamp) as max_time')
			->from($db->quoteName('#__bcted_service_booking'))
			->where($db->quoteName('user_id') . ' = ' . $db->quote($userID));*/

		// Set the query and load the result.
		$db->setQuery($query);

		$bookedService = $db->loadObjectList();

		$rateToService = array();

		$processedService = array();

		if(count($bookedService) != 0)
		{
			foreach ($bookedService as $key => $bs)
			{
				$currentTime = time();
				$timestamp = strtotime($bs->service_booking_datetime);
				$BookedTimestampAfert24 = strtotime('+1 day', $timestamp);

				if($currentTime >= $BookedTimestampAfert24)
				{
					if(!in_array($bs->company_id, $processedService))
					{
						$processedService[] = $bs->company_id;
						$tempData = array();
						$tempData['bookingID'] = $bs->service_booking_id;
						$tempData['serviceID'] = $bs->service_id;
						$tempData['companyID'] = $bs->company_id;
						$tempData['serviceName'] = $bs->service_name;
						$tempData['companyName'] = $bs->company_name;
						$rateToService[] = $tempData;
					}

				}
			}
		}

		$query = $db->getQuery(true);

		$query->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'))
			->where($db->quoteName('vb.user_id') . ' = ' . $db->quote($userID))
			->where($db->quoteName('vb.is_rated') . ' = ' . $db->quote('0'))

			->where($db->quoteName('vb.is_deleted') . ' = ' . $db->quote('0'))
			->where($db->quoteName('vb.status') . ' = ' . $db->quote('5'))
			->where($db->quoteName('vb.user_status') . ' = ' . $db->quote('5'));

			//->where($db->quoteName('vb.venue_booking_datetime') . ' < ' . $db->quote($currentDateTime))

			//->order($db->quoteName('sb.venue_booking_datetime') . ' ASC');

		$query->select('vt.venue_table_name')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		$query->select('v.venue_name')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');

		/*echo $query->dump();
		exit;*/

		// Create the base select statement.
		/*$query = "SELECT *
			FROM `#__bcted_venue_booking`
			WHERE `user_id` = '{$userID}' AND `is_rated` = '0' AND `venue_booking_datetime` < '{$currentDateTime}' AND `is_deleted`='0' AND `status`='5' AND `user_status`='5'";*/

		// Set the query and load the result.
		$db->setQuery($query);

		$bookedVenueTable = $db->loadObjectList();

		$rateToVenue = array();
		$processedVenue = array();

		if(count($bookedVenueTable) != 0)
		{
			foreach ($bookedVenueTable as $key => $bv)
			{
				$currentTime = time();
				$timestamp = strtotime($bv->venue_booking_datetime);
				$BookedTimestampAfert24 = strtotime('+1 day', $timestamp);

				//echo $currentTime . " " . $BookedTimestampAfert24."<br />";

				if($currentTime >= $BookedTimestampAfert24)
				{
					if(!in_array($bv->venue_id, $processedVenue))
					{
						$processedVenue[] = $bv->venue_id;
						$tempData = array();
						$tempData['bookingID'] = $bv->venue_booking_id;
						$tempData['tableID']   = $bv->venue_table_id;
						$tempData['venueID']   = $bv->venue_id;
						$tempData['tableName'] = $bv->venue_table_name;
						$tempData['venueName'] = $bv->venue_name;
						$rateToVenue[] = $tempData;
					}

				}
			}
		}

		$ratingArras['service'] = $rateToService;
		$ratingArras['venues'] = $rateToVenue;

		return $ratingArras;

	}

	public function getUserCompanyID($userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('company_id')
			->from($db->quoteName('#__bcted_company'))
			->where($db->quoteName('userid') . ' = ' . $db->quote($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$company = $db->loadResult();

			if($company)
				return $company;
			else
				return 0;

		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}

	public function getVenueDetail($venueID)
	{
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($venueID);

		if(!$tblVenue->venue_id)
		{
			return array();
		}

		$venue = array();

		$venue['venueID']        = $tblVenue->venue_id;
		$venue['venueName']      = $tblVenue->venue_name;
		$venue['venueAddress']   = $tblVenue->venue_address;
		$venue['venueAbout']     = $tblVenue->venue_about;
		$venue['venueAmenities'] = $tblVenue->venue_amenities;
		$venue['venueSigns']     = $tblVenue->venue_signs;
		$venue['venueTimings']   = $tblVenue->venue_timings;
		$venue['venueImage']     = ($tblVenue->venue_image)?JUri::base().$tblVenue->venue_image:'';
		$venue['venueCreated']   = $tblVenue->venue_created;
		$venue['venueModified']  = $tblVenue->venue_modified;
		$venue['timeStamp']      = $tblVenue->time_stamp;

		return $venue;

	}

	public function getUserVenueID($userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('venue_id')
			->from($db->quoteName('#__bcted_venue'))
			->where($db->quoteName('userid') . ' = ' . $db->quote($userID));


		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$venue = $db->loadResult();

			if($venue)
				return $venue;
			else
				return 0;

		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * * get Params For Extension
	 *
	 * @return  object  $params   Global parameters for component
	 */
	public function getExtensionParam()
	{
		$app    = JFactory::getApplication();
		//$option = $app->input->get('option');
		$option = "com_bcted";
		$db     = JFactory::getDbo();

		$option = '%' . $db->escape($option, true) . '%';

		// Initialiase variables.
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->qn('#__extensions'))
			->where($db->qn('name') . ' LIKE ' . $db->q($option))
			->where($db->qn('type') . ' = ' . $db->q('component'))
			->order($db->qn('ordering') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadObject();

			$params = json_decode($result->params);
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), 500);
		}

		return $params;
	}


	private function getUserGroups($userID)
	{
		$user = JFactory::getUser($userID);
		$groups = $user->get('groups');
		$groupIDs = implode(",", $groups);

		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('title')
			->from($this->db->qn('#__usergroups'))
			->where($this->db->qn('id') . ' IN ('.$groupIDs.')');

		// Set the query and load the result.
		$this->db->setQuery($query);

		$resultGroup = $this->db->loadColumn();

		return $resultGroup;


	}

	private function getUserProfileID($userID)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('profile_id')
			->from($db->qn('#__bcted_user_profile'))
			->where($db->qn('userid') . ' = ' . $db->q($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		$profileID = $db->loadResult();

		return $profileID;
	}

	/**
	 * This Function is Use to Log Into with facebook
	 *
	 * @return it will return false value otherwise jasson array
	 */
	public function fblogin()
	{
		jimport('joomla.user.helper');

		$data['relname'] = IJReq::getTaskData('name');
		$data['user_nm'] = IJReq::getTaskData('username');
		$data['email'] = IJReq::getTaskData('email');
		$data['pic_big'] = IJReq::getTaskData('bigpic');
		$password_set = IJReq::getTaskData('password');
		$reg_opt = IJReq::getTaskData('regopt', 0, 'int');
		$fbid = IJReq::getTaskData('fbid');
		$time = time();

		if ($reg_opt === 0)
		{
			// first check if fbuser in db logged in
			$query = $this->db->getQuery(true);

			// Create the base select statement.
			$query->select('u.id,u.username')
				->from($this->db->qn('#__users AS u, #__community_connect_users AS cu'))
				->where($this->db->qn('u.id') . ' = ' . $this->db->q('cu.userid'))
				->where($this->db->qn('cu.connectid') . ' = ' . $this->db->q($password_set));

			// Set the query and load the result.
			$this->db->setQuery($query);

			$userinfo = $this->db->loadObject();

			if (isset($userinfo->id) && $userinfo->id > 0)
			{
				$salt = JUserHelper::genRandomPassword(32);
				$crypt = JUserHelper::getCryptedPassword($password_set . $time, $salt);
				$data['password'] = $crypt . ':' . $salt;

				$query = $this->db->getQuery(true);

				// Create the base update statement.
				$query->update($this->db->qn('#__users'))
					->set($this->db->qn('password') . ' = ' . $this->db->q($data['password']))
					->where($this->db->qn('id') . ' = ' . $this->db->q($userinfo->id));

				// Set the query and execute the update.
				$this->db->setQuery($query);

				$this->db->execute();

				$usersipass['username'] = $userinfo->username;
				$usersipass['password'] = $password_set . $time;

				if ($this->mainframe->login($usersipass) == '1')
				{
					$jsonarray = $this->loginProccess();

					return $jsonarray;
				}
				else
				{
					IJReq::setResponseCode(401);
					IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_UNABLE_TO_AUTHENTICATE'));

					return false;
				}
			}
			else
			{
				// Facebook user not found, need to create new user
				IJReq::setResponseCode(703);

				return false;
			}
		}
		elseif ($reg_opt === 1)
		{
			// Registration option 1 if already user
			$credentials = array();
			$credentials['username'] = $data['user_nm'];
			$credentials['password'] = $password_set;

			if ($this->mainframe->login($credentials) == '1' && $fbid != "")
			{
				// Connect fb user to site user...
				$user = JFactory::getUser();

				if (strtolower(IJOOMER_GC_REGISTRATION) === 'community' && file_exists(JPATH_ROOT . '/components/com_community/libraries/core.php'))
				{
					require_once JPATH_ROOT . '/components/com_community/libraries/core.php';

					$query = $this->db->getQuery(true);

					// Create the base insert statement.
					$query->insert($this->db->qn('#__community_connect_users'))
						->columns(
							array(
								$this->db->qn('userid'),
								$this->db->qn('connectid'),
								$this->db->qn('type'))
							)
						->values(
							$this->db->q($user->id) . ', ' .
							$this->db->q($fbid) . ', ' .
							$this->db->q('facebook')
							);

					// Set the query and execute the insert.
					$this->db->setQuery($query);

					$this->db->execute();

					$salt = JUserHelper::genRandomPassword(32);
					$crypt = JUserHelper::getCryptedPassword($password_set . $time, $salt);
					$data['password'] = $crypt . ':' . $salt;

					$query = $this->db->getQuery(true);

					// Create the base update statement.
					$query->update($this->db->qn('#__users'))
						->set($this->db->qn('password') . ' = ' . $this->db->q($data['password']))
						->where($this->db->qn('id') . ' = ' . $this->db->q($user->id));

					// Set the query and execute the update.
					$this->db->setQuery($query);

					$this->db->execute();

					// Store user image...
					CFactory::load('libraries', 'facebook');
					$facebook = new CFacebook;

					// Edited by Salim (Date: 08-09-2011)
					$data['pic_big'] = str_replace('profile.cc.fbcdn', 'profile.ak.fbcdn', $data['pic_big']);
					$data['pic_big'] = str_replace('hprofile-cc-', 'hprofile-ak-', $data['pic_big']);

					$facebook->mapAvatar($data['pic_big'], $user->id, $config->get('fbwatermark'));
				}

				$jsonarray = $this->loginProccess();
			}
			else
			{
				IJReq::setResponseCode(401);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_UNABLE_TO_AUTHENTICATE'));

				return false;
			}
		}
		else
		{
			$query = $this->db->getQuery(true);

			// Create the base select statement.
			$query->select('u.id')
				->from($this->db->qn('#__users', 'u'))
				->where($this->db->qn('u.email') . ' = ' . $this->db->q($data['email']));

			// Set the query and load the result.
			$this->db->setQuery($query);

			$uid = $this->db->loadResult();

			if ($uid > 0)
			{
				// if user exists with email address send email id already exists
				$query = $this->db->getQuery(true);

				// Create the base select statement.
				$query->select('u.id')
					->from($this->db->qn('#__users AS u, #__community_connect_users AS cu'))
					->where($this->db->qn('u.id') . ' = ' . $this->db->q('cu.userid'))
					->where($this->db->qn('u.email') . ' = ' . $this->db->q($data['email']))
					->where($this->db->qn('cu.connectid') . ' = ' . $this->db->q($password_set));

				$this->db->setQuery($query);
				$uid = $this->db->loadResult();

				if (empty($uid))
				{
					IJReq::setResponseCode(702);
					IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_EMAIL_ALREADY_EXIST'));

					return false;
				}
			}

			$query = $this->db->getQuery(true);

			// Create the base select statement.
			$query->select('id')
				->from($this->db->qn('#__users'))
				->where($this->db->qn('username') . ' = ' . $this->db->q($data['user_nm']));

			$this->db->setQuery($query);
			$uid = $this->db->loadResult();

			if ($uid > 0)
			{
				IJReq::setResponseCode(701);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_USERNAME_ALREADY_EXIST'));

				return false;
			}
			else
			{
				jimport('joomla.user.helper');
				$user = new JUser;
				$fbData = IJReq::getTaskData('fb');
				$data['name'] = $fbData->name;
				$data['username'] = trim(str_replace("\n", "", $data['user_nm']));
				$data['password1'] = $data['password2'] = trim(str_replace("\n", "", $password_set . $time));
				$data['email1'] = $data['email2'] = trim(str_replace("\n", "", $fbData->email));
				$data['latitude'] = IJReq::getTaskData('lat');
				$data['longitude'] = IJReq::getTaskData('long');

				$user->bind($data);

				if (!$user->save())
				{
					IJReq::setResponseCode(500);

					return false;
				}

				$aclval = $user->id;

				$query = $this->db->getQuery(true);

				// store usegroup for user...
				$query->insert($this->db->qn('#__user_usergroup_map'))
					->columns(
						array(
							$this->db->qn('group_id'),
							$this->db->qn('user_id')
							)
						)
					->values(
						$this->db->q('2') . ', ' .
						$this->db->q($aclval)
						);

				$this->db->setQuery($query);
				$this->db->execute();

				if (strtolower(IJOOMER_GC_REGISTRATION) === 'jomsocial' && file_exists(JPATH_ROOT . '/components/com_community/libraries/core.php'))
				{
					require_once JPATH_ROOT . '/components/com_community/libraries/core.php';

					$query = $this->db->getQuery(true);

					// Create the base insert statement.
					$query->insert($this->db->qn('#__community_connect_users'))
						->columns(
							array(
								$this->db->qn('userid'),
								$this->db->qn('connectid'),
								$this->db->qn('type'))
							)
						->values(
							$this->db->q($aclval) . ', ' .
							$this->db->q($password_set) . ', ' .
							$this->db->q('facebook')
							);

					$this->db->setQuery($query);
					$this->db->execute();

					$config = CFactory::getConfig();

					// Store user image...
					CFactory::load('libraries', 'facebook');
					$facebook = new CFacebook;

					// Edited by Salim (Date: 08-09-2011)
					$data['pic_big'] = str_replace('profile.cc.fbcdn', 'profile.ak.fbcdn', $data['pic_big']);
					$data['pic_big'] = str_replace('hprofile-cc-', 'hprofile-ak-', $data['pic_big']);

					$facebook->mapAvatar($data['pic_big'], $aclval, $config->get('fbwatermark'));
				}

				// Update password again...
				$salt = JUserHelper::genRandomPassword(32);
				$crypt = JUserHelper::getCryptedPassword($password_set . $time, $salt);
				$data['password'] = $crypt . ':' . $salt;

				$query = $this->db->getQuery(true);

				// Create the base update statement.
				$query->update($this->db->qn('#__users'))
					->set($this->db->qn('password') . ' = ' . $this->db->q($data['password']))
					->where($this->db->qn('id') . ' = ' . $this->db->q($aclval));

				// Set the query and execute the update.
				$this->db->setQuery($query);

				$this->db->execute();

				$usersipass['username'] = trim(str_replace("\n", "", $data['user_nm']));
				$usersipass['password'] = trim(str_replace("\n", "", $password_set . $time));

				if ($this->mainframe->login($usersipass) == '1')
				{
					if ($jsonarray = $this->loginProccess())
					{
						$this->fbFieldSet($aclval);

						return $jsonarray;
					}
					else
					{
						return false;
					}
				}
				else
				{
					IJReq::setResponseCode(401);
					IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_UNABLE_TO_AUTHENTICATE'));

					return false;
				}
			}
		}

		return $jsonarray;
	}

/**
 * The Function FB Field Set
 *
 * @param   [type]  $userid  contains the userid
 *
 * @return  void
 */
	private function fbFieldSet($userid)
	{
		$fb = IJReq::getTaskData('fb');
		$fieldConnection = array('FB_USERID' => (isset($fb->uid)) ? number_format($fb->uid, 0, '', '') : null,
			'FB_USERNAME' => (isset($fb->username)) ? $fb->username : null,
			'FB_3PARTY_ID' => (isset($fb->third_party_id)) ? $fb->third_party_id : null,
			'FB_FNAME' => (isset($fb->first_name)) ? $fb->first_name : null,
			'FB_MNAME' => (isset($fb->middle_name)) ? $fb->middle_name : null,
			'FB_LNAME' => (isset($fb->last_name)) ? $fb->last_name : null,
			'FB_PIC' => (isset($fb->pic)) ? $fb->pic : null,
			'FB_PIC_SMALL' => (isset($fb->pic_small)) ? $fb->pic_small : null,
			'FB_PIC_COVER' => (isset($fb->pic_cover->source)) ? $fb->pic_cover->source : null,
			'FB_VERIFIED' => (isset($fb->verified)) ? $fb->verified : null,
			'FB_SEX' => (isset($fb->sex)) ? $fb->sex : null,
			'FB_BIRTH_DATE' => (isset($fb->birthday_date)) ? $fb->birthday_date : null,
			'FB_STATUS' => (isset($fb->status->message)) ? $fb->status->message : null,
			'FB_ABOUT_ME' => (isset($fb->about_me)) ? $fb->about_me : null,
			'FB_TIMEZONE' => (isset($fb->timezone)) ? $fb->timezone : null,
			'FB_ISMINOR' => (isset($fb->is_minor)) ? $fb->is_minor : null,
			'FB_POLITICAL' => (isset($fb->political)) ? $fb->political : null,
			'FB_QUOTES' => (isset($fb->quotes)) ? $fb->quotes : null,
			'FB_RELATION_STATUS' => (isset($fb->relationship_status)) ? $fb->relationship_status : null,
			'FB_RELIGION' => (isset($fb->religion)) ? $fb->religion : null,
			'FB_TV_SHOW' => (isset($fb->tv)) ? $fb->tv : null,
			'FB_SPORTS' => (isset($fb->sports[0]->name)) ? $fb->sports[0]->name : null,
			'FB_WORK' => (isset($fb->work[0])) ? $fb->work[0] : null,
			'FB_EDUCATION' => (isset($fb->education[0]->school)) ? $fb->education[0]->school : null,
			'FB_EMAIL' => (isset($fb->email)) ? $fb->email : null,
			'FB_WEBSITE' => (isset($fb->website)) ? $fb->website : null,
			'FB_CURRENT_STREET' => (isset($fb->current_address->street)) ? $fb->current_address->street : null,
			'FB_CURRENT_CITY' => (isset($fb->current_address->city)) ? $fb->current_address->city : null,
			'FB_CURRENT_STATE' => (isset($fb->current_address->state)) ? $fb->current_address->state : null,
			'FB_CURRENT_COUNTRY' => (isset($fb->current_address->country)) ? $fb->current_address->country : null,
			'FB_CURRENT_ZIP' => (isset($fb->current_address->zip)) ? $fb->current_address->zip : null,
			'FB_CURRENT_LATITUDE' => (isset($fb->current_address->latitude)) ? $fb->current_address->latitude : null,
			'FB_CURRENT_LONGITUDE' => (isset($fb->current_address->longitude)) ? $fb->current_address->longitude : null,
			'FB_CURRENT_LOCATION_NAME' => (isset($fb->current_address->name)) ? $fb->current_address->name : null,
			'FB_HOMETOWN_STREET' => (isset($fb->hometown_location->street)) ? $fb->hometown_location->street : null,
			'FB_HOMETOWN_CITY' => (isset($fb->hometown_location->city)) ? $fb->hometown_location->city : null,
			'FB_HOMETOWN_STATE' => (isset($fb->hometown_location->state)) ? $fb->hometown_location->state : null,
			'FB_HOMETOWN_COUNTRY' => (isset($fb->hometown_location->country)) ? $fb->hometown_location->country : null,
			'FB_HOMETOWN_ZIP' => (isset($fb->hometown_location->zip)) ? $fb->hometown_location->zip : null,
			'FB_HOMETOWN_LATITUDE' => (isset($fb->hometown_location->latitude)) ? $fb->hometown_location->latitude : null,
			'FB_HOMETOWN_LONGITUDE' => (isset($fb->hometown_location->longitude)) ? $fb->hometown_location->longitude : null,
			'FB_HOMETOWN_LOCATION_NAME' => (isset($fb->hometown_location->name)) ? $fb->hometown_location->name : null,
		);

		foreach ($fieldConnection as $key => $value)
		{
			$query = $this->db->getQuery(true);

			// Create the base select statement.
			$query->select('value')
				->from($this->db->qn('#__ijoomeradv_jomsocial_config'))
				->where($this->db->qn('name') . ' = ' . $this->db->q($key));

			// Set the query and load the result.
			$this->db->setQuery($query);

			$fieldid = $this->db->loadResult();

			if ($fieldid)
			{
				$query = $this->db->getQuery(true);

				// Create the base select statement.
				$query->select('id')
					->from($this->db->qn('#__community_fields_values'))
					->where($this->db->qn('user_id') . ' = ' . $this->db->q($userid))
					->where($this->db->qn('field_id') . ' = ' . $this->db->q($fieldid));

				$this->db->setQuery($query);
				$field = $this->db->loadResult();

				$query = $this->db->getQuery(true);

				if ($field)
				{
					// Create the base update statement.
					$query->update($this->db->qn('#__community_fields_values'))
						->set($this->db->qn('value') . ' = ' . $this->db->q($value))
						->where($this->db->qn('id') . ' = ' . $this->db->q($field));
				}
				else
				{
					// Create the base insert statement.
					$query->insert($this->db->qn('#__community_fields_values'))
						->columns(
							array(
								$this->db->qn('user_id'),
								$this->db->qn('field_id'),
								$this->db->qn('value'),
								$this->db->qn('access')
								)
							)
						->values(
							$this->db->q($userid) . ', ' .
							$this->db->q($fieldid) . ', ' .
							$this->db->q($value) . ', ' .
							$this->db->q(0)
							);

				}

				$this->db->setQuery($query);
				$this->db->execute();

			}
		}

		$query = $this->db->getQuery(true);

		// Create the base update statement.
		$query->update($this->db->qn('#__community_users'))
			->set($this->db->qn('status') . ' = ' . $this->db->q($fieldConnection['FB_STATUS']))
			->where($this->db->qn('userid') . ' = ' . $this->db->q($userid));

		$this->db->setQuery($query);
		$this->db->execute();
	}

	/**
	 * This function is use to register a new user
	 *
	 * @example the json string will be like, :
	 *    {
	 *        "task":"registration",
	 *        "taskData": {
	 *            "name":"name",
	 *            "username":"username",
	 *            "password":"password",
	 *            "email":"email",
	 *            "full":"0/1",(if 0=jomsocial full form sent)
	 *            "type":"profile type (default is 'default')"
	 *        }
	 *    }
	 *
	 * @return it will return false value otherwise jsson array
	 */
	public function registration()
	{
		//mail("bcted.developer@gmail.com", "bcted registration request object", $_REQUEST['reqObject']);
		/*$post['relname'] = IJReq::getTaskData('name');
		$post['username'] = IJReq::getTaskData('username');
		$post['password'] = IJReq::getTaskData('password');
		$post['email'] = IJReq::getTaskData('email');
		$post['type'] = IJReq::getTaskData('type', 0, 'int');
		$Full_flag = IJReq::getTaskData('full', 0, 'int');
		$lang = JFactory::getLanguage();
		$lang->load('com_users');



		if (strtolower(IJOOMER_GC_REGISTRATION) === 'no')
		{
			// If registration not allowed
			IJReq::setResponseCode(401);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_REGISTRATION_NOT_ALLOW'));

			return false;
		}*/

		$uniqueUser = $this->getNewUserName(8);

		$post['username'] = IJReq::getTaskData('email');
		$username = IJReq::getTaskData('email');
		$post['last_name'] = IJReq::getTaskData('lastName');
		$post['phoneno']  = IJReq::getTaskData('phoneno');
		$post['fbid']     = IJReq::getTaskData('fbID','');
		$post['city']     = IJReq::getTaskData('city','');
		//$username         = IJReq::getTaskData('username');
		$post['email']    = IJReq::getTaskData('email');

		/*echo "<pre>";
		print_r($post);
		echo "</pre>";
		exit;*/

		if(empty($username))
		{
			$post['username'] = $post['email']; //$post['first_name'].'_'.$post['last_name'];
		}

		//$post['username'] = IJReq::getTaskData('username');
		$post['relname']  = IJReq::getTaskData('firstName');
		$post['password'] = IJReq::getTaskData('password');
		$post['token']    = IJReq::getTaskData('password');
		$post['type']     = IJReq::getTaskData('type', 0, 'int');
		$Full_flag        = IJReq::getTaskData('full', 0, 'int');
		$fb               = IJReq::getTaskData('fb',0,'int');

		$fbuid = '';

		if($fb==1)
		{
			$fbuid = $post['password'];
		}

		$lang = JFactory::getLanguage();
		$lang->load('com_users');

		if (strtolower(IJOOMER_GC_REGISTRATION) === 'no')
		{
			// If registration not allowed
			IJReq::setResponseCode(401);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_REGISTRATION_NOT_ALLOW'));

			return false;
		}

		/*if(strlen($post['phoneno']) < 12)
		{
			IJReq::setResponseCode(710);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_INVALID_PHONE'));
			return false;
		}

		$pos = strpos($post['phoneno'], "+");

		if ($pos === false)
		{

			IJReq::setResponseCode(710);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_INVALID_PHONE'));
			return false;
		}*/

		/*$query="SELECT COUNT(phoneno)
				FROM `#__bcted_user_profile`
				WHERE phoneno='".str_replace("\n","",trim($post['phoneno']))."'";
		$this->db->setQuery($query);

	 	if($this->db->loadResult() > 0)
	 	{
			IJReq::setResponseCode(709);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PHONE_ALREADY_EXIST'));
			return false;
		}*/

		$query = $this->db->getQuery(true);

		$username = str_replace("\n", "", trim($post['username']));

		// Create the base select statement.
		$query->select('id')
			->from($this->db->qn('#__users'))
			->where($this->db->qn('username') . ' = ' . $this->db->q($username));

		$this->db->setQuery($query);

		if ( $this->db->loadResult() > 0)
		{
			// Check if user already exist
			IJReq::setResponseCode(701);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_USERNAME_ALREADY_EXIST'));

			return false;
		}

		$query = $this->db->getQuery(true);

		$emails = str_replace("\n", "", trim($post['email']));

		// Create the base select statement.
		$query->select('id')
			->from($this->db->qn('#__users'))
			->where($this->db->qn('email') . ' = ' . $this->db->q($emails));

		$this->db->setQuery($query);

		if ($this->db->loadResult() > 0)
		{
			// Check if email id already exist
			IJReq::setResponseCode(702);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_EMAIL_ALREADY_EXIST'));

			return false;
		}



		$params         = JComponentHelper::getParams('com_users');
		$system         = $params->get('new_usertype', 2);
		$useractivation = $params->get('useractivation');
		$sendpassword   = $params->get('sendpassword', 1);

		// Initialise the table with JUser.
		$user              = new JUser;
		$post['name']      = trim(str_replace("\n", "", $post['relname']));
		$post['username']  = trim(str_replace("\n", "", $post['username']));
		$post['password']  = $post['password1'] = $post['password2'] = trim(str_replace("\n", "", $post['password']));
		$post['email']     = $post['email1'] = $post['email2'] = trim(str_replace("\n", "", $post['email']));
		$post['groups'][0] = $system;
		$post['phoneno']   = trim(str_replace("\n", "", $post['phoneno']));
		$post['city']      = trim(str_replace("\n", "", $post['city']));

		/*echo "<pre>";
		print_r($post);
		echo "</pre>";
		exit;*/

		// Check if the user needs to activate their account.
		if($fb == 0)
		{
			// Check if the user needs to activate their account.
			if (($useractivation == 1) || ($useractivation == 2))
			{
				/*$post['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
				$post['block'] = 1;*/

				$post['activation'] = "";
				$post['block'] = 0;
			}
		}
		else
		{
			$post['activation'] = "";
			$post['block'] = 0;
		}

		//echo "<pre/>";print_r($post);exit;
		$user->bind($post);

		if (!$user->save())
		{
			IJReq::setResponseCode(500);

			return false;
		}

		$aclval = $user->id;

		if (!$aclval)
		{
			IJReq::setResponseCode(500);

			return false;
		}

		// Compile the notification mail values.
		$data = $user->getProperties();
		$config = JFactory::getConfig();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');
		$data['siteurl'] = JUri::root();

		// Handle account activation/confirmation emails.
		if ($useractivation == 2)
		{
			// Set the link to confirm the user email.
			$uri = JURI::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base . JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $data['activation'], false);

			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',
					$data['name'],
					$data['sitename'],
					$data['siteurl'] . 'index.php?option=com_users&task=registration.activate&token=' . $data['activation'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['siteurl'] . 'index.php?option=com_users&task=registration.activate&token=' . $data['activation'],
					$data['siteurl'],
					$data['username']
				);
			}
		}
		elseif ($useractivation == 1)
		{
			// Set the link to activate the user account.
			$uri = JURI::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base . JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $data['activation'], false);

			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY',
					$data['name'],
					$data['sitename'],
					$data['siteurl'] . 'index.php?option=com_users&task=registration.activate&token=' . $data['activation'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['siteurl'] . 'index.php?option=com_users&task=registration.activate&token=' . $data['activation'],
					$data['siteurl'],
					$data['username']
				);
			}
		}
		else
		{
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			$emailBody = JText::sprintf(
				'COM_USERS_EMAIL_REGISTERED_BODY',
				$data['name'],
				$data['sitename'],
				$data['siteurl']
			);
		}

		//$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);
		// Send the registration email.
		if($fb == 0 )
		{

			/*$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody,true);
			if ($return !== true)
			{
				IJReq::setResponseCode(500);
				IJReq::setResponseMessage(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
				return false;
			}*/
			$return = true;
		}

		// Send Notification mail to administrators
		if (($params->get('useractivation') < 2) && ($params->get('mail_to_admin') == 1))
		{
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			$emailBodyAdmin = JText::sprintf(
				'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY',
				$data['name'],
				$data['username'],
				$data['siteurl']
			);

			// Get all admin users
			$query = 'SELECT name, email, sendEmail
					FROM #__users
					WHERE sendEmail=1';
			$this->db->setQuery($query);
			$rows = $this->db->loadObjectList();

			// Send mail to all superadministrators id
			foreach ($rows as $row)
			{
				$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBodyAdmin);

				// Check for an error.
				if ($return !== true)
				{
					IJReq::setResponseCode(500);
					IJReq::setResponseMessage(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));

					return false;
				}
			}
		}


		if($fb == 0)
		{
			// Check for an error.
			if ($return !== true)
			{
				$query = $this->db->getQuery(true);

				// Send a system message to administrators receiving system mails
				$query->select('id')
					->from($this->db->qn('#__users'))
					->where($this->db->qn('block') . ' = ' . $this->db->q('0'))
					->where($this->db->qn('sendEmail') . ' = ' . $this->db->q('1'));

				$this->db->setQuery($query);
				$sendEmail = $this->db->loadColumn();

				if (count($sendEmail) > 0)
				{
					$jdate = new JDate;

					$query = $this->db->getQuery(true);

					$messages = array();

					foreach ($sendEmail as $userid)
					{
						$messages[] = "({$userid}, {$userid}, '{$jdate->toSql()}', '" . JText::_('COM_USERS_MAIL_SEND_FAILURE_SUBJECT') . "', '" . JText::sprintf('COM_USERS_MAIL_SEND_FAILURE_BODY', $return, $data['username']) . "')";
					}

					// Build the query to add the messages
					$query->insert($this->db->qn('#__messages'))
						->columns(
							array(
								$this->db->qn('user_id_from'),
								$this->db->qn('user_id_to'),
								$this->db->qn('date_time'),
								$this->db->qn('subject'),
								$this->db->qn('message')
								)
							)
						->values(implode(',', $messages));

					//$query .= implode(',', $messages);

					$this->db->setQuery($query);
					$this->db->execute();
				}

				IJReq::setResponseCode(500);
				IJReq::setResponseMessage(JText::_('COM_USERS_REGISTRATION_SEND_MAIL_FAILED'));

				return false;
			}
		}

		$my			= JFactory::getUser($aclval);
		$userid		= $my->id;

		if($fb == 0)
		{
			$jsonarray['code'] = 200;
			return $jsonarray;
		}
		else
		{
			$credentials = array();
			$credentials['username'] = IJReq::getTaskData('email'); // get username
			$credentials['password'] = IJReq::getTaskData('password'); // get password

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('*')
				->from($db->quoteName('#__users'))
				->where($db->quoteName('email') . ' = binary ' . $db->quote($credentials['username']));

			// Set the query and load the result.
			$db->setQuery($query);

			$result = $db->loadObject();

			if($result && $result->block)
			{
				$jsonarray['code']=401;
				$jsonarray['message']=JText::_('COM_IJOOMERADV_UNABLE_TO_AUTHENTICATE_ACTIVE_ACCOUNT');
				// $this->outputJSON($jsonarray);
				return $jsonarray;
			}

			if(empty($result->username))
			{
				$jsonarray['code']=401;
				$jsonarray['message']=JText::_('COM_IJOOMERADV_UNABLE_TO_AUTHENTICATE_INVALID_USERNAME');

				return $jsonarray;
			}

			$credentials['username'] = $result->username;

			$mainframe = & JFactory::getApplication();
			if($mainframe->login($credentials) == '1')
			{
				$user = JFactory::getUser();
				if(!$user->id)
				{
					$jsonarray['code']=401;
					$jsonarray['message']=JText::_('COM_IJOOMERADV_UNABLE_TO_AUTHENTICATE');

					return $jsonarray;
				}

				$deviceType	= IJReq::getTaskData('deviceType');

				$jsonarray = $this->loginProccess();
			}
			else
			{
				$jsonarray = array();
				$jsonarray['code']=402;
				$jsonarray['message']=JText::_('COM_IJOOMERADV_UNABLE_TO_AUTHENTICATE_INVALID_PASSWORD');
			}

			return $jsonarray;
		}


		$jsonarray['code'] = 200;

		return $jsonarray;
	}

	/**
	 * Function To Request Token To Reset Password
	 *
	 * @return  it will return a false value otherwise jsson array
	 */
	public function retriveToken()
	{
		$email = IJReq::getTaskData('email');

		jimport('joomla.mail.helper');
		jimport('joomla.user.helper');

		if (!JMailHelper::isEmailAddress($email))
		{
			// Make sure the e-mail address is valid
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_INVALID_EMAIL'));

			return false;
		}

		// Build a query to find the user
		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('id')
			->from($this->db->qn('#__users'))
			->where($this->db->qn('email') . ' = ' . $this->db->q($email))
			->where($this->db->qn('block') . ' = ' . $this->db->q('0'));

		$this->db->setQuery($query);

		if (!($id = $this->db->loadResult()))
		{
			// Check if user exist of given email
			IJReq::setResponseCode(401);

			return false;
		}

		if (IJ_JOOMLA_VERSION === 1.5)
		{
			// Generate a new token
			$token = JApplication::getHash(JUserHelper::genRandomPassword());
			$salt = JUserHelper::getSalt('crypt-md5');
			$hashedToken = md5($token . $salt) . ':' . $salt;
		}
		else
		{
			// Set the confirmation token.
			$token = JApplication::getHash(JUserHelper::genRandomPassword());
			$salt = JUserHelper::getSalt('crypt-md5');
			$hashedToken = md5($token . $salt) . ':' . $salt;
		}

		$query = $this->db->getQuery(true);

		// Create the base update statement.
		$query->update($this->db->qn('#__users'))
			->set($this->db->qn('activation') . ' = ' . $this->db->q($hashedToken))
			->where($this->db->qn('id') . ' = ' . $this->db->q($id))
			->where($this->db->qn('block') . ' = ' . $this->db->q('0'));

		$this->db->setQuery($query);

		if (!$this->db->query())
		{
			// Save the token
			IJReq::setResponseCode(500);

			return false;
		}

		if (!$this->_sendConfirmationMail($email, $token))
		{
			// Send the token to the user via e-mail
			IJReq::setResponseCode(500);

			return false;
		}

		$jsonarray['code'] = 200;

		return $jsonarray;
	}

	/**
	 * Function To Validate Token Against Username
	 *
	 * @return  it will return the false value otherwise Jasson Array
	 */
	public function validateToken()
	{
		$token = IJReq::getTaskData('token');
		$username = IJReq::getTaskData('username');

		jimport('joomla.user.helper');

		if (strlen($token) != 32)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_INVALID_TOKEN'));

			return false;
		}

		$query = $this->db->getQuery(true);

		// Create the base select statement.
		$query->select('id, activation')
			->from($this->db->qn('#__users'))
			->where($this->db->qn('username') . ' = ' . $this->db->q($username))
			->where($this->db->qn('block') . ' = ' . $this->db->q('0'));

		$this->db->setQuery($query);

		if (!($row = $this->db->loadObject()))
		{
			// Verify the token
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_INVALID_TOKEN'));

			return false;
		}

		$parts = explode(':', $row->activation);
		$crypt = $parts[0];

		if (!isset($parts[1]))
		{
			IJReq::setResponseCode(401);

			return false;
		}

		$salt = $parts[1];
		$testcrypt = JUserHelper::getCryptedPassword($token, $salt);

		// Verify the token
		if (!($crypt == $testcrypt))
		{
			IJReq::setResponseCode(401);

			return false;
		}

		// Push the token and user id into the session
		$jsonarray['code'] = 200;
		$jsonarray['userid'] = $row->id;
		$jsonarray['crypt'] = $crypt . ':' . $salt;

		return $jsonarray;
	}

	public function bctContactUs()
	{
		$subject = IJReq::getTaskData('subject','','string');
		$message = IJReq::getTaskData('message','','string');

		// Initialise variables.
		$app     = JFactory::getApplication();
		$config  = JFactory::getConfig();

		$site    = $config->get('sitename');
		$from    = $config->get('mailfrom');
		$sender  = $config->get('fromname');
		$email   = 'bcted.developer@gmail.com'; //$app->input->get('mailto');
		$subject = $subject;//$app->input->get('subject');

		// Build the message to send.
		/*$msg     = JText::_('COM__SEND_EMAIL_MSG');
		$body    = sprintf($msg, $site, $sender, $from);*/

		$body = $message;

		// Clean the email data.
		$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body);

		// Check for an error.
		if ($return !== true)
		{
			$jsonarray['code'] = 500;
			$jsonarray['message'] = JText::_('COM_IJOOMERADV_CONTACT_US_EMAIL_SEND_FAILED');

			return $jsonarray;
		}

		$jsonarray['code'] = 200;
		return $jsonarray;
	}

	/**
	 * @uses
	 *
	 */
	/**
	 * Function Is Used To Reset Password
	 *
	 * @return  it will return a value in false or JssonArray
	 */
	public function resetPassword()
	{
		$token = IJReq::getTaskData('crypt');
		$userid = IJReq::getTaskData('userid', 0, 'int');
		$password1 = IJReq::getTaskData('password');

		// Make sure that we have a pasword
		if (!$token || !$userid || !$password1)
		{
			IJReq::setResponseCode(400);

			return false;
		}

		jimport('joomla.user.helper');

		// Get the necessary variables
		$salt = JUserHelper::genRandomPassword(32);
		$crypt = JUserHelper::getCryptedPassword($password1, $salt);
		$password = $crypt . ':' . $salt;

		// Get the user object
		$user = new JUser($userid);

		// Fire the onBeforeStoreUser trigger
		JPluginHelper::importPlugin('user');
		$dispatcher = JDispatcher::getInstance();
		$dispatcher->trigger('onBeforeStoreUser', array($user->getProperties(), false));

		$query = $this->db->getQuery(true);

		// Create the base update statement.
		$query->update($this->db->qn('#__users'))
			->set($this->db->qn('password') . ' = ' . $this->db->q($password))
			->set($this->db->qn('activation') . ' = ' . $this->db->q(''))
			->where($this->db->qn('id') . ' = ' . $this->db->q($userid))
			->where($this->db->qn('activation') . ' = ' . $this->db->q($token))
			->where($this->db->qn('block') . ' = ' . $this->db->q('0'));


		$this->db->setQuery($query);

		if (!$result = $this->db->execute())
		{
			// Save the password
			IJReq::setResponseCode(500);

			return false;
		}

		// Update the user object with the new values.
		$user->password = $password;
		$user->activation = '';
		$user->password_clear = $password1;

		if (IJ_JOOMLA_VERSION === 1.5)
		{
			// Fire the onAfterStoreUser trigger
			$dispatcher->trigger('onAfterStoreUser', array($user->getProperties(), false, $result, ''));
		}
		else
		{
			$app = JFactory::getApplication();

			if ( !$user->save(true))
			{
				IJReq::setResponseCode(500);

				return false;
			}

			// Flush the user data from the session.
			$app->setUserState('com_users.reset.token', null);
			$app->setUserState('com_users.reset.user', null);
		}

		$jsonarray['code'] = 200;

		return $jsonarray;
	}

	/**
	 * @uses function is used to forgot password
	 *
	 */
	function forgotPasswordTest()
	{
		$email = IJReq::getTaskData('email' ,'');

		if(empty($email))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_INVALID_EMAIL'));
			return false;
		}

		$query = $this->db->getQuery(true);

		$query->select('id')
			->from($this->db->quoteName('#__users'))
			->where($this->db->quoteName('email') . ' = ' . $this->db->quote($email));

		$this->db->setQuery($query);

		$userID = $this->db->loadResult();

		if($userID>0)
		{
			$password_set	= JUserHelper::genRandomPassword(6);

			$userDetail = JFactory::getUser($userID);

	 		$queryUPDT = $this->db->getQuery(true);

			$queryUPDT->update($this->db->quoteName('#__bcted_user_profile'))
				->set($this->db->quoteName('token') . ' = ' . $this->db->quote($password_set))
				->where($this->db->quoteName('userid') . ' = ' . $this->db->quote($userID));

			$this->db->setQuery($queryUPDT);
			$this->db->execute();

			$app     = JFactory::getApplication();
			$config  = JFactory::getConfig();

			$site    = $config->get('sitename');
			$from    = $config->get('mailfrom');
			$sender  = $config->get('fromname');

			$emailSubject	= JText::sprintf(
				'COM_IJOOMERADV_FORGOT_PASSWORD_EMAIL_SUBJECT'
			);

			$emailBody = JText::sprintf(
				'COM_IJOOMERADV_FORGOT_PASSWORD_EMAIL_BODY',
				$userDetail->name,
				$password_set
			);
			// Clean the email data.
			$sender  = JMailHelper::cleanAddress($sender);
			$subject = JMailHelper::cleanSubject($emailSubject);
			$body    = JMailHelper::cleanBody($emailBody);

			// Send the email.
			$return = JFactory::getMailer()->sendMail($from, $sender, $email, $emailSubject, $body,true);

			// Check for an error.
			if ($return !== true)
			{
				$jsonarray['code']	  = 500;
				return $jsonarray;
			}
			$jsonarray['code']	  = 200;
			return $jsonarray;
		}

		$jsonarray['code']	  = 400;
		return $jsonarray;
	}

	/**
	 * Function Use to Retrive Userid
	 *
	 * @return  it will return a false value or Jsson Array
	 */
	public function retriveUsername()
	{
		$email = IJReq::getTaskData('email');

		jimport('joomla.mail.helper');
		jimport('joomla.user.helper');

		// Make sure the e-mail address is valid
		if (!JMailHelper::isEmailAddress($email))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_INVALID_TOKEN'));

			return false;
		}

		// Build a query to find the user
		$query = $this->db->getQuery(true);

		// Build a query to find the user.
		$query->select('*')
			->from($this->db->qn('#__users'))
			->where($this->db->qn('email') . ' = ' . $this->db->q($email))
			->where($this->db->qn('block') . ' = ' . $this->db->q('0'));

		// Set the query and load the result.
		$this->db->setQuery($query);
		$user = $this->db->loadObject();

		// Set the e-mail parameters
		$lang = JFactory::getLanguage();
		$lang->load('com_users');
		$config = JFactory::getConfig();

		// Assemble the login link.
		include_once JPATH_ROOT . '/components/com_users/helpers/route.php';
		$itemid = UsersHelperRoute::getLoginRoute();
		$itemid = $itemid !== null ? '&Itemid=' . $itemid : '';
		$link = 'index.php?option=com_users&view=login' . $itemid;

		$mode = $config->get('force_ssl', 0) == 2 ? 1 : - 1;

		$data = JArrayHelper::fromObject($user);
		$fromname = $config->get('fromname');
		$mailfrom = $config->get('mailfrom');
		$sitename = $config->get('sitename');
		$link_text = JRoute::_($link, false, $mode);
		$username = $data['username'];
		$subject = JText::sprintf('COM_USERS_EMAIL_USERNAME_REMINDER_SUBJECT', $sitename);
		$body = JText::sprintf('COM_USERS_EMAIL_USERNAME_REMINDER_BODY', $sitename, $username, $link_text);

		// Send the token to the user via e-mail
		$return = JFactory::getMailer()->sendMail($mailfrom, $fromname, $email, $subject, $body);

		if ( !$return)
		{
			IJReq::setResponseCode(500);

			return false;
		}

		$jsonarray['code'] = 200;

		return $jsonarray;
	}

	/**
	 * The Send Confirmation Mail Function
	 *
	 * @param   [type]  $email  Contains The Email
	 * @param   [type]  $token  Contains The Token
	 *
	 * @return  boolean Returns The Value in True or False
	 */
	public function _sendConfirmationMail($email, $token)
	{
		$config = JFactory::getConfig();

		if (IJ_JOOMLA_VERSION === 1.5)
		{
			$url = JRoute::_('index.php?option=com_user&view=reset&layout=confirm', true, -1);
			$sitename = $config->getValue('sitename');

			// Set the e-mail parameters
			$lang = JFactory::getLanguage();
			$lang->load('com_user');

			$from = $config->getValue('mailfrom');
			$fromname = $config->getValue('fromname');
			$subject = sprintf(JText::_('PASSWORD_RESET_CONFIRMATION_EMAIL_TITLE'), $sitename);
			$body = sprintf(JText::_('PASSWORD_RESET_CONFIRMATION_EMAIL_TEXT'), $sitename, $token, $url);

			// Send the e-mail
			if (!JUtility::sendMail($from, $fromname, $email, $subject, $body))
			{
				return false;
			}
		}
		else
		{
			// Set the e-mail parameters
			$lang = JFactory::getLanguage();
			$lang->load('com_users');
			include_once JPATH_ROOT . '/components/com_users/helpers/route.php';

			$mode = $config->get('force_ssl', 0) == 2 ? 1 : - 1;
			$itemid = UsersHelperRoute::getLoginRoute();
			$itemid = $itemid !== null ? '&Itemid=' . $itemid : '';
			$link = 'index.php?option=com_users&view=reset&layout=confirm' . $itemid;

			$fromname = $config->get('fromname');
			$mailfrom = $config->get('mailfrom');
			$sitename = $config->get('sitename');
			$link_text = JRoute::_($link, false, $mode);

			$subject = JText::sprintf('COM_USERS_EMAIL_PASSWORD_RESET_SUBJECT', $sitename);
			$body = JText::sprintf('COM_USERS_EMAIL_PASSWORD_RESET_BODY', $sitename, $token, $link_text);

			// Send the password reset request email.
			$return = JFactory::getMailer()->sendMail($mailfrom, $fromname, $email, $subject, $body);

			if ( !$return)
			{
				return false;
			}
		}

		return true;
	}

	function getNewUserName($len = 8)
	{
		$username = JUserHelper::genRandomPassword(8);
		$query="SELECT id
				FROM `#__users`
				WHERE username='".str_replace("\n","",trim($username))."'";
		$this->db->setQuery($query);

	 	if($this->db->loadResult() > 0)
	 	{ // check if user already exist
			$this->getNewUserName($len);
		}

		return $username;
	}
}
