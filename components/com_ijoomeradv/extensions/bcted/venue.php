<?php
/**
 * @package     iJoomerAdv.Site
 * @subpackage  com_ijoomeradv
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.helper' );
class venue
{

	private $db;
	private $IJUserID;
	private $helper;
	private $jsonarray;

	function __construct()
	{
		$this->db                = JFactory::getDBO();
		$this->mainframe         =  JFactory::getApplication ();
		$this->IJUserID          = $this->mainframe->getUserState ( 'com_ijoomeradv.IJUserID', 0 ); //get login user id
		$this->my                = JFactory::getUser ( $this->IJUserID ); // set the login user object
		$this->helper            = new bctedAppHelper;
		$this->jsonarray         = array();
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"getPremiumTables",
	 * 		"taskData":{
	 *
	 * 		}
	 * 	}
	 */
	function getPremiumTables()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		// Initialiase variables.
		$db    = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->quoteName('#__bcted_premium_table'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote(0))
			->where($db->quoteName('venue_table_id') . ' = ' . $db->quote(0));

		$db->setQuery($query);
		$premiumTables = $db->loadObjectList();

		$resultTables = array();

		foreach ($premiumTables as $key => $premiumTable)
		{
			$tmpData=array();
			$tmpData['premiumTableID'] = $premiumTable->premium_id;
			$tmpData['premiumTableName'] = $premiumTable->p_table_name;
			$resultTables[] = $tmpData;
		}

		if(count($resultTables) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PREMIUM_TALBES_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($resultTables);
		$this->jsonarray['premiumTables'] = $resultTables;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"getVenues",
	 * 		"taskData":{
	 *
	 * 		}
	 * 	}
	 */
	function getVenues()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$city = IJReq::getTaskData('city','','string');

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$queryVT = $db->getQuery(true);
		$queryVT->select('venue_id')
			->from($db->quoteName('#__bcted_venue_table'))
			->where($db->quoteName('venue_table_active') . ' = ' . $db->quote('1'));
		$db->setQuery($queryVT);
		$venuesHasTable = $db->loadColumn();

		if(count($venuesHasTable) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$venuesHasTable = array_unique($venuesHasTable);
		$blacklistVenue = $this->helper->userBlackListBy($this->IJUserID,'Venue');

		$venuesHasTableNotBlacklist = array();
		foreach ($venuesHasTable as $key => $venueID)
		{
			if(in_array($venueID, $blacklistVenue))
			{
				continue;
			}
			$venuesHasTableNotBlacklist[] = $venueID;
		}

		$venuesHasTable = $venuesHasTableNotBlacklist;

		if(count($venuesHasTable) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$venuesHasTableStr = implode(",", $venuesHasTable);


		/*echo "<pre>";
		print_r($blacklistVenue);
		echo "</pre>";
		exit;*/



		// Create the base select statement.
		$query->select('venue_id')
			->from($db->quoteName('#__bcted_venue'))
			->where($db->quoteName('venue_active') . ' = ' . $db->quote('1'))
			->where($db->quoteName('venue_id') . ' IN (' . $venuesHasTableStr . ')');

		if(!empty($city))
		{
			$query->where($db->quoteName('city') . ' LIKE  ' . $db->quote('%'.$city.'%'));
		}



		$query->order($db->quoteName('venue_name') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		$resultVenue = array();

		try
		{
			$venues = $db->loadColumn();

			/*if(count($blacklistVenue))
			{
				$venues = array_diff($venues, $blacklistVenue);
			}*/

			/*echo "<pre>";
			print_r($venues);
			echo "</pre>";
			exit;*/

			$total     = count($venues);
			$pageNO    = IJReq::getTaskData('pageNO',0);
			$pageLimit = BCTED_VENUE_LIST_LIMIT;

			if($pageNO==0 || $pageNO==1){
				$startFrom=0;
			}else{
				$startFrom = $pageLimit*($pageNO-1);
			}

			$venuesArray =array_slice($venues,$startFrom,$pageLimit);

			foreach ($venuesArray as $key => $venue)
			{
				$resultVenue[] = $this->helper->getVenueDetail($venue);
			}

		}
		catch (RuntimeException $e)
		{
			//throw new RuntimeException($e->getMessage(), $e->getCode());
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_($e->getMessage() . ' - ' . $e->getCode()));
			return false;
		}

		if(count($resultVenue) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$this->jsonarray['code']      = 200;
		$this->jsonarray['total']     = $total;
		$this->jsonarray['pageLimit'] = $pageLimit;
		$this->jsonarray['venues']    = $resultVenue;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	{
		"extName":"bcted",
		"extView":"venue",
		"extTask":"noShow",
		"taskData":{
			"bookingID":"Number"
		}
	}
	 */
	function noShow()
	{
		/*$this->helper->getAdministratorUsersEmail();
		exit;*/
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$bookingID = IJReq::getTaskData('bookingID',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');
		$tblVenuebooking->load($bookingID);

		if(!$tblVenuebooking->venue_booking_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_BCTED_INVALID_BOOKING_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenuebooking->is_noshow = 1;
		$tblVenuebooking->store();

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($tblVenuebooking->venue_id);

		$tblTable = JTable::getInstance('Table', 'BctedTable');
		$tblTable->load($tblVenuebooking->venue_table_id);

		$bookingUserID = $tblVenuebooking->user_id;
		$bookingUserDetail = JFactory::getUser($bookingUserID);
		if(!$bookingUserDetail->id)
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_BCTED_INVALID_USER_DELETED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		// Initialise variables.
		$app     = JFactory::getApplication();
		$config  = JFactory::getConfig();

		$site    = $config->get('sitename');
		$from    = $config->get('mailfrom');
		$sender  = $config->get('fromname');
		$email   = $bookingUserDetail->email; //$app->input->get('mailto');
		$subject =  JText::_('COM_IJOOMERADV_VENUE_NOSHOW_EMAIL_SUBJECT');//$app->input->get('subject');

		// Build the message to send.
		$msg     = JText::_('COM_IJOOMERADV_VENUE_NOSHOW_EMAIL_BODY');
		$body    = sprintf($msg, $site, $sender, $from);

		// Clean the email data.
		$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body);
		$message     = JText::_('COM_IJOOMERADV_VENUE_NOSHOW_EMAIL_BODY');
		$this->sendAddMeVenueTableMessage($tblVenue->venue_id,0,0,$tblTable->venue_table_id,$bookingUserID,$message,array(),'noshow');

		$connectionID = $this->helper->getMessageConnection($this->IJUserID,$bookingUserID);
		$pushID = $tblVenue->venue_name.';'.$connectionID.';venue';

		/*************** Send no Show email to site administrator ********/

		$email   = $this->helper->getAdministratorUsersEmail();
		$subject =  JText::_('COM_IJOOMERADV_VENUE_NOSHOW_EMAIL_TO_ADMINISTRATOR_SUBJECT');//$app->input->get('subject');

		// Build the message to send.
		$msg     = JText::_('COM_IJOOMERADV_VENUE_NOSHOW_EMAIL_TO_ADMINISTRATOR_BODY');
		$body    = sprintf($msg, $site, $sender, $from);

		// Clean the email data.
		$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body,true);

		/*************** End no show email to site administrator ************/

		/*if(!$connectionID)
		{
			return 0;
		}*/

		$this->jsonarray['code'] = 200;
		$this->jsonarray['pushNotificationData']['id']         = $pushID;
		$this->jsonarray['pushNotificationData']['to']         = $bookingUserID;
		$this->jsonarray['pushNotificationData']['message']    = $message;
		$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NOSHOW'); //'NoShow';
		$this->jsonarray['pushNotificationData']['configtype'] = '';

		/*echo "<pre>";
		print_r($this->jsonarray);
		echo "</pre>";
		exit;*/

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_payment_status'))
			->where($db->quoteName('booked_element_id') . ' = ' . $db->quote($tblVenuebooking->venue_booking_id))
			->where($db->quoteName('booked_element_type') . ' = ' . $db->quote('venue'))
			->where($db->quoteName('paid_status') . ' = ' . $db->quote('1'));

		// Set the query and load the result.
		$db->setQuery($query);

		$paymentStatus = $db->loadObject();
		if($paymentStatus)
		{
			$queryLP = $db->getQuery(true);
			$queryLP->select('*')
				->from($db->quoteName('#__bcted_loyalty_point'))
				->where($db->quoteName('user_id') . ' = ' . $db->quote($tblVenuebooking->user_id))
				->where($db->quoteName('point_app') . ' = ' . $db->quote('purchase.venue'))
				->where($db->quoteName('cid') . ' = ' . $db->quote($paymentStatus->payment_id));

			// Set the query and load the result.
			$db->setQuery($queryLP);

			$loyaltyPointDetail = $db->loadObject();

			if($loyaltyPointDetail)
			{
				$query = $db->getQuery(true);

				// Create the base insert statement.
				$query->insert($db->quoteName('#__bcted_loyalty_point'))
					->columns(
						array(
							$db->quoteName('user_id'),
							$db->quoteName('earn_point'),
							$db->quoteName('point_app'),
							$db->quoteName('cid'),
							$db->quoteName('is_valid'),
							$db->quoteName('created'),
							$db->quoteName('time_stamp')
						)
					)
					->values(
						$db->quote($tblVenuebooking->user_id) . ', ' .
						$db->quote(($loyaltyPointDetail->earn_point * (-1))) . ', ' .
						$db->quote('venue.noshow') . ', ' .
						$db->quote($loyaltyPointDetail->cid) . ', ' .
						$db->quote(1) . ', ' .
						$db->quote(date('Y-m-d H:i:s')) . ', ' .
						$db->quote(time())
					);

				// Set the query and execute the insert.
				$db->setQuery($query);

				$db->execute();

			}
		}


		//updateLoyaltyPoints($userID,$activity,$totalPrice,$paymentEntryID,$isValid = 0)

		return $this->jsonarray;

	}



	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"getUserAttendingVenueTables",
	 * 		"taskData":{
	 * 			"venueID":"number",
	 * 			"emails":"string"
	 * 			"fbIDs":"string",
	 * 			"myFbId":"string"
	 * 		}
	 * 	}
	 */
	function getUserAttendingVenueTables()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$venueID = IJReq::getTaskData('venueID',0,'int');
		$emails  = IJReq::getTaskData('emails','','string');
		$fbIDs   = IJReq::getTaskData('fbIDs','','string');
		$myFbId  = IJReq::getTaskData('myFbId','','string');
		/*echo $fbIDs;
		exit;*/

		$myFbId = trim($myFbId);

		if(!empty($myFbId))
		{
			JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
			$tblProfile       = JTable::getInstance('Profile', 'BctedTable');
			$profileID = $this->helper->getUserProfileID($this->IJUserID);
			$tblProfile->load($profileID);

			$tblProfile->fbid = trim($myFbId);
			/*echo "<pre>";
			print_r($tblProfile);
			echo "</pre>";
			exit;*/

			$tblProfile->store();
		}

		if(empty($emails) && empty($fbIDs))
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_BCTED_USER_INVALID_USERLIST_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$emailFounds = array();
		$fbIDFounds  = array();

		if(!empty($emails))
		{
			$emails = explode(",", $emails);
			$emails = implode("','", $emails);
			$emails = "'".$emails."'";



			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('id')
				->from($db->quoteName('#__users'))
				->where($db->quoteName('email') . ' IN (' . $emails . ')')
				->where($db->quoteName('block') . ' = ' . $db->quote(0));

			// Set the query and load the result.
			$db->setQuery($query);

			$emailFounds = $db->loadColumn();
		}

		if(!empty($fbIDs))
		{
			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('userid')
				->from($db->quoteName('#__bcted_user_profile'))
				->where($db->quoteName('fbid') . ' IN (' . $fbIDs . ')')
				->where($db->quoteName('user_type_for_push') . ' = ' . $db->quote('guest'));

			/*echo $query->dump();
			exit;*/

			// Set the query and load the result.
			$db->setQuery($query);

			$fbIDFounds = $db->loadColumn();

			/*echo "<pre>";
			print_r($fbIDFounds);
			echo "</pre>";
			exit;*/
		}

		if(count($emailFounds) == 0 && count($fbIDFounds)==0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_BCTED_USER_USERS_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$foundArray = array();

		if($emailFounds)
		{
			$foundArray = array_merge($foundArray,$emailFounds);
		}
		if($fbIDFounds)
		{
			$foundArray = array_merge($foundArray,$fbIDFounds);
		}

		$foundArray = array_unique($foundArray);

		if(count($foundArray) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_BCTED_USER_USERS_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*echo "<pre>";
		print_r($foundArray);
		echo "</pre>";
		exit;*/
		$foundArray = implode(",", $foundArray);



		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'))
			->where($db->quoteName('vb.status') . ' = ' . $db->quote('5'))
			->where($db->quoteName('vb.user_status') . ' = ' . $db->quote('5'))
			->where($db->quoteName('vb.venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('vb.is_deleted') . ' = ' . $db->quote('0'))
			->where($db->quoteName('vb.venue_booking_datetime') . ' >= ' . $db->quote(date('Y-m-d')))
			->where($db->quoteName('vb.user_id') . ' IN (' . $foundArray . ')')
			->where($db->quoteName('vb.user_id') . ' <> ' .  $db->quote($this->IJUserID))
			->order($db->quoteName('vb.venue_booking_datetime') . ' ASC');

		$query->select('vt.premium_table_id,vt.venue_table_name,vt.custom_table_name')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		$query->select('u.name,u.username')
			->join('LEFT','#__users AS u ON u.id=vb.user_id');

		$query->select('bu.last_name,bu.fbid')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=vb.user_id');

		/*echo $query->dump();
		exit;*/

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		$userHasBooked = array();

		foreach ($result as $key => $userbooked)
		{
			/*echo "<pre>";
			print_r($userbooked);
			echo "</pre>";
			exit;*/
			$bookingDateTime =  trim($userbooked->venue_booking_datetime).' '.trim($userbooked->booking_to_time).':00';
			//$bookingDateTime = strtotime($bookingDateTime);

			//echo "Booking TD : ".$bookingDateTime;

			//$currentDateTime = date('Y-m-d H:i:s');

			//$currentDateTime = strtotime($currentDateTime);

			//echo "current TD : " . $currentDateTime;

			//exit;

			/*$user = JFactory::getUser();
			$db = JFactory::getDBO();*/
			//echo date_default_timezone_get();

			/*$config = JFactory::getConfig();
     		$offset = $config->get('config.offset');

     		echo "call<pre>";
     		print_r($offset);
     		echo "</pre>";*/
			//exit;

			//echo trim($userbooked->venue_booking_datetime).' '.trim($userbooked->booking_to_time) . ' || '.$userbooked->venue_booking_id.'<br />';
			//echo $bookingDateTime . ' || ' . $currentDateTime . ' || ' .$userbooked->venue_booking_id.'<br />';
			///if($bookingDateTime>$currentTime)
			//{
				$tmpData = array();
				$tmpData['bookingID']     = $userbooked->venue_booking_id;
				$tmpData['userID']        = $userbooked->user_id;
				$tmpData['username']      = $userbooked->name;
				$tmpData['tableID']       = $userbooked->venue_table_id;
				$tmpData['venueID']       = $userbooked->venue_id;

				if($userbooked->premium_table_id)
				{
					$tmpData['tableName']     = $userbooked->venue_table_name;
				}
				else
				{
					$tmpData['tableName']     = $userbooked->custom_table_name;
				}

				$fbImage = "http://graph.facebook.com/".$userbooked->fbid."/picture" ;


				$tmpData['dateAttending'] = date('d-m-Y',strtotime($userbooked->venue_booking_datetime));
				$tmpData['toTimeAttending'] = $userbooked->booking_to_time;
				$tmpData['facebookPicture'] = $fbImage;

				$userHasBooked[] = $tmpData;
			//}

		}

		if(count($userHasBooked)==0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_BCTED_USER_USERS_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($userHasBooked);
		$this->jsonarray['usersBooked'] = $userHasBooked;

		return $this->jsonarray;


	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"getVenueTables",
	 * 		"taskData":{
	 * 			"venueID":"number"
	 * 		}
	 * 	}
	 */
	function getVenueTables()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$venueID         = IJReq::getTaskData('venueID',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($venueID);

		if(!$tblVenue->venue_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_INVALID_VENUE'));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('venue_table_id')
			->from($db->quoteName('#__bcted_venue_table'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($tblVenue->venue_id))
			->where($db->quoteName('venue_table_active') . ' = ' . $db->quote(1))
			->order($db->quoteName('time_stamp') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);

		$resultTable = array();

		try
		{
			$venueTables = $db->loadColumn();

			/*echo "<pre>";
			print_r($venueTables);
			echo "</pre>";
			exit;*/

			$total     = count($venueTables);
			$pageNO    = IJReq::getTaskData('pageNO',0);
			$pageLimit = BCTED_VENUE_TABLE_LIST_LIMIT;

			if($pageNO==0 || $pageNO==1){
				$startFrom=0;
			}else{
				$startFrom = $pageLimit*($pageNO-1);
			}

			$venueTablesArray =array_slice($venueTables,$startFrom,$pageLimit);

			foreach ($venueTablesArray as $key => $table)
			{
				$resultTable[] = $this->helper->getVenueTableDetail($table);
			}

		}
		catch (RuntimeException $e)
		{
			//throw new RuntimeException($e->getMessage(), $e->getCode());
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_($e->getMessage() . ' - ' . $e->getCode()));
			return false;
		}

		if(count($resultTable) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$this->jsonarray['code']      = 200;
		$this->jsonarray['total']     = $total;
		$this->jsonarray['pageLimit'] = $pageLimit;
		$this->jsonarray['tables']    = $resultTable;

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"inviteToMyBookedTable",
	 * 		"taskData":{
	 * 			"bookingID":"number",
	 * 			"userID":"number",
	 * 			"message":"string"
	 * 		}
	 * 	}
	 */
	function inviteToMyBookedTable()
	{

		/*if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition*/

		$user = JFactory::getUser();

		$bookingID     = IJReq::getTaskData('bookingID',0,'int');
		$emails        = IJReq::getTaskData('emails', '', 'string');
		$enterEmails   = IJReq::getTaskData('emails', '', 'string');
		$message       = IJReq::getTaskData('message', '', 'string');
		$seletedEmails = IJReq::getTaskData('emails', '', 'string');

		if(empty($emails) || !$bookingID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_DETAIL_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$bctRegiEmails = $this->helper->filterEmails($emails);

		/*echo "<pre>";
		print_r($bctRegiEmails);
		echo "</pre>";
		exit;*/

		$emailsArray = explode(",", $emails);
		$filterEmails = array();

		foreach ($emailsArray as $key => $singleEmail)
		{
			if(in_array($singleEmail, $bctRegiEmails['allRegEmail']))
			{
				if(in_array($singleEmail, $bctRegiEmails['service'])){ continue; }
				if(in_array($singleEmail, $bctRegiEmails['venue'])){ continue; }
				if(in_array($singleEmail, $bctRegiEmails['guest']) && $this->my->email == $singleEmail){ continue; }
				//if(in_array($$singleEmail, $bctRegiEmails['service'])){ continue; }
			}

			$filterEmails[] = $singleEmail;
		}

		/*echo "<pre>";
		print_r($filterEmails);
		echo "</pre>";
		exit;*/

		if(count($filterEmails)==0)
		{
			IJReq::setResponseCode(300);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVITE_NOT_VALID_SELF_EMAIL_OR_CLUB_SERVICE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$seletedEmails = implode(',', $filterEmails);
		$emails = $seletedEmails;
		$enterEmails = $emails;


		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenue            = JTable::getInstance('Venue', 'BctedTable');
		$tblTable            = JTable::getInstance('Table', 'BctedTable');
		$tblVenuebooking     = JTable::getInstance('Venuebooking', 'BctedTable');
		$tblVenuetableinvite = JTable::getInstance('Venuetableinvite','BctedTable');


		$tblVenuebooking->load($bookingID);

		$tblVenue->load($tblVenuebooking->venue_id);
		$tblTable->load($tblVenuebooking->venue_table_id);

		if(!$tblVenuebooking->venue_booking_id || !$tblVenue->venue_id || !$tblTable->venue_table_id)
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_DETAIL_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if(!empty($emails))
		{
			$emails = explode(",", $emails);
			$emails = implode("','", $emails);
			$emails = "'".$emails."'";



			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('id')
				->from($db->quoteName('#__users'))
				->where($db->quoteName('email') . ' IN (' . $emails . ')')
				->where($db->quoteName('block') . ' = ' . $db->quote(0));

			// Set the query and load the result.
			$db->setQuery($query);

			$foundUsers = $db->loadColumn();

			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('email')
				->from($db->quoteName('#__users'))
				->where($db->quoteName('email') . ' IN (' . $emails . ')')
				->where($db->quoteName('block') . ' = ' . $db->quote(0));

			// Set the query and load the result.
			$db->setQuery($query);

			$foundEmails = $db->loadColumn();
		}

		/*echo "<pre>";
		print_r($foundEmails);
		echo "</pre>";
		exit;*/

		//forea

		/*$tblVenuetableinvite->load(0);

		$inviteData['booking_id'] = $tblVenuebooking->booking_id;
		$inviteData['table_id']   = $tblVenuebooking->table_id;
		$inviteData['venue_id']   = $tblVenuebooking->venue_id;
		$inviteData['user_id']    = $tblVenuebooking->user_id;
		$inviteData['message']    = $message;
		$inviteData['time_stamp'] = time();
		$inviteData['created'] = date('Y-m-d H:i:s');

		$tblVenuetableinvite->bind($inviteData);

		if(!$tblVenuetableinvite->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_DETAIL_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}*/

		$this->jsonarray['code'] = 200;

		if(count($foundUsers)!=0)
		{
			$message = JText::sprintf('COM_IJOOMERADV_VENUE_INVITE_TO_MY_BOOKING_TABLE',$user->name,$tblVenue->venue_name);
			$message = $user->name .' want to invite you at ' . $tblVenue->venue_name;// . ';'.$tblTable->venue_table_name.';'.$tblVenuebooking->venue_booking_datetime;

			//$this->helper->getMessageConnection($fromUser,$toUser);

			$this->jsonarray['pushNotificationData']['id']         = $bookingID;
			$this->jsonarray['pushNotificationData']['to']         = implode(",", $foundUsers);
			$this->jsonarray['pushNotificationData']['message']    = $message;
			$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_BOOKEDTABLEINVITETOUSER'); //'bookedTableInviteToUser';
			$this->jsonarray['pushNotificationData']['configtype'] = '';
		}

		if(count($foundEmails) != 0)
		{
			$notRegEmails = array_diff($seletedEmails, $foundEmails);

			/*echo "<pre>";
			print_r($notRegEmails);
			echo "</pre>";
			exit;*/
			if(count($notRegEmails) != 0)
			{
				// Initialise variables.
				$app     = JFactory::getApplication();
				$config  = JFactory::getConfig();

				$site    = $config->get('sitename');
				$from    = $config->get('mailfrom');
				$sender  = $config->get('fromname');
				$email   = $notRegEmails; //implode(",",$notRegEmails); //$app->input->get('mailto');
				$subject = JText::_('COM_IJOOMERADV_BOOKED_VENUE_TABLE_INVITATION_EMAIL_SUBJECT'); // $app->input->get('subject');

				// Build the message to send.
				//$msg     = JText::_('COM__SEND_EMAIL_MSG');
				//Hello, <br /><br />%s want to invite you to join Table %s at Club %s on %s<br /><br />Thank You,<br />BC-Ted Team"
				if($tblTable->premium_table_id)
				{
					$body    = JText::sprintf('COM_IJOOMERADV_BOOKED_VENUE_TABLE_INVITATION_EMAIL_BODY',$user->name,$tblTable->venue_table_name,$tblVenue->venue_name,date('d-m-Y H:i',strtotime($tblVenuebooking->venue_booking_datetime)));
				}
				else
				{
					$body    = JText::sprintf('COM_IJOOMERADV_BOOKED_VENUE_TABLE_INVITATION_EMAIL_BODY',$user->name,$tblTable->custom_table_name,$tblVenue->venue_name,date('d-m-Y H:i',strtotime($tblVenuebooking->venue_booking_datetime)));
				}


				// Clean the email data.
				$sender  = JMailHelper::cleanAddress($sender);
				$subject = JMailHelper::cleanSubject($subject);
				$body    = JMailHelper::cleanBody($body);

				// Send the email.
				$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body, true);

				// Check for an error.
				if ($return !== true)
				{
					//return new JException(JText::_('COM__SEND_MAIL_FAILED'), 500);
				}
			}

		}

		if(!empty($enterEmails))
		{
			$enterEmailArray = explode(",", $enterEmails);

			foreach ($enterEmailArray as $key => $singleEmail)
			{
				// Initialiase variables.
				$db    = JFactory::getDbo();
				$query = $db->getQuery(true);

				// Create the base select statement.
				$query->select('*')
					->from($db->quoteName('#__users'))
					->where($db->quoteName('email') . ' = ' . $db->quote($singleEmail));

				// Set the query and load the result.
				$db->setQuery($query);

				$userData = $db->loadObject();

				/*echo "<pre>";
				print_r($userData);
				echo "</pre>";*/

				if($userData)
				{
					$message = $user->name .' want to invite you at ' . $tblVenue->venue_name; // . ';'.$tblTable->venue_table_name.';'.$tblVenuebooking->venue_booking_datetime;
					$extraParam = array();
					$extraParam['venueBookingID'] = $tblVenuebooking->venue_booking_id;
					$this->sendMessageForInviteUserByOwner($tblVenue->venue_id,0,0,0,$userData->id,$message,$extraParam,$messageType='inviteToTable');
				}

				$app     = JFactory::getApplication();
				$config  = JFactory::getConfig();

				$site    = $config->get('sitename');
				$from    = $config->get('mailfrom');
				$sender  = $config->get('fromname');
				$email   = $singleEmail; //implode(",",$notRegEmails); //$app->input->get('mailto');
				$subject = JText::_('COM_IJOOMERADV_BOOKED_VENUE_TABLE_INVITATION_EMAIL_SUBJECT'); // $app->input->get('subject');

				// Build the message to send.
				//$msg     = JText::_('COM__SEND_EMAIL_MSG');
				//Hello, <br /><br />%s want to invite you to join Table %s at Club %s on %s<br /><br />Thank You,<br />BC-Ted Team"
				$body    = JText::sprintf('COM_IJOOMERADV_BOOKED_VENUE_TABLE_INVITATION_EMAIL_BODY',$user->name,$tblTable->venue_table_name,$tblVenue->venue_name,date('d-m-Y H:i',strtotime($tblVenuebooking->venue_booking_datetime)));

				// Clean the email data.
				$sender  = JMailHelper::cleanAddress($sender);
				$subject = JMailHelper::cleanSubject($subject);
				$body    = JMailHelper::cleanBody($body);

				/*echo $body;
				exit;*/

				//echo $email."<br />";

				// Send the email.
				$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body, true);

				// Check for an error.
				if ($return !== true)
				{
					return new JException(JText::_('COM__SEND_MAIL_FAILED'), 500);
				}

			}

			//exit;

		}

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"addMeForVenueTable",
	 * 		"taskData":{
	 * 			"tableID":"number",
	 * 			"userID":"number",
	 * 			"venueID":"string",
	 * 			"message":"string"
	 * 		}
	 * 	}
	 */
	function addMeForVenueTable()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$user = JFactory::getUser();

		$bookingID = IJReq::getTaskData('bookingID',0,'int');
		$tableID   = IJReq::getTaskData('tableID',0,'int');
		$userID    = IJReq::getTaskData('userID',0,'int');
		$venueID   = IJReq::getTaskData('venueID',0,'int');
		$message   = IJReq::getTaskData('message','','string');

		/*echo $venueID;
		exit;*/

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblTable = JTable::getInstance('Table', 'BctedTable');
		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');

		$tblVenue->load($venueID);
		$tblTable->load($tableID);
		$tblVenuebooking->load($bookingID);
		/*echo "<pre>";
		print_r($tblVenue);
		echo "</pre>";
		exit;*/

		if(empty($message) || !$userID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_DETAIL_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenuetableinvite = JTable::getInstance('Venuetableinvite', 'BctedTable');
		$tblVenuetableinvite->load(0);

		$postData['booking_id']   = $bookingID;
		$postData['table_id']   = $tableID;
		$postData['venue_id']   = $venueID;
		$postData['user_id']    = $this->IJUserID;
		$postData['message']    = $message;
		$postData['time_stamp'] = time();
		$postData['created']    = date('Y-m-d H:i:s');

		$tblVenuetableinvite->bind($postData);

		if(!$tblVenuetableinvite->store())
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_ADD_ME_ERROR_INVITE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}


		$this->jsonarray['code'] = 200;


		//$message = $user->name .' has requested to be added to your booking for ' . $tblTable->venue_table_name .' at '.$tblVenue->venue_name . ' on ' .$tblVenuebooking->venue_booking_datetime;
		$extraParam = array();
		$extraParam['inviteID'] = $tblVenuetableinvite->invite_id;

		if($tblTable->premium_table_id)
		{
			$extraParam['tableName'] = $tblTable->venue_table_name;
			$message = JText::sprintf('COM_IJOOMERADV_VENUE_INVITE_ME_TO_VENUE_TABLE',$user->name,$tblTable->venue_table_name,$tblVenue->venue_name,date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime)));
			$messageSend = $message;
			$message .= ";".$tblTable->venue_table_name.";".date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime));
		}
		else
		{
			$extraParam['tableName'] = $tblTable->custom_table_name;
			$message = JText::sprintf('COM_IJOOMERADV_VENUE_INVITE_ME_TO_VENUE_TABLE',$user->name,$tblTable->custom_table_name,$tblVenue->venue_name,date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime)));
			$messageSend = $message;
			$message .= ";".$tblTable->custom_table_name.";".date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime));
		}


		$this->jsonarray['pushNotificationData']['id']         = $tblVenuetableinvite->invite_id;
		$this->jsonarray['pushNotificationData']['to']         = $userID;
		$this->jsonarray['pushNotificationData']['message']    = $message;
		$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_REQUESTFORADDMEATTABLE'); //'RequestForAddMeAtTable';
		$this->jsonarray['pushNotificationData']['configtype'] = '';




		$extraParam['bookingDate'] = date('d-m-Y',strtotime($tblVenuetableinvite->venue_booking_datetime));

		$this->sendAddMeVenueTableMessage($venueID,0,0,$tableID,$userID,$messageSend,$extraParam,$messageType='tableaddme');


		return $this->jsonarray;
	}



	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"acceptAddMeRequestByUser",
	 * 		"taskData":{
	 * 			"inviteID":"number"
	 * 		}
	 * 	}
	 */
	function acceptAddMeRequest()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$inviteID   = IJReq::getTaskData('inviteID',0,'int');
		$actionBy   = IJReq::getTaskData('actionBy','user','string');
		$actionType = IJReq::getTaskData('actionType','accept','string');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenuetableinvite = JTable::getInstance('Venuetableinvite', 'BctedTable');
		$tblVenuetableinvite->load($inviteID);

		if(!$tblVenuetableinvite->invite_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_ADD_ME_ERROR_INVITE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($actionBy == 'user')
		{
			if($actionType == 'accept')
			{
				$tblVenuetableinvite->user_action = 'accept';
				//$this->sendAddMeVenueTableMessage($tblVenuetableinvite->venue_id,0,0,$$tblVenuetableinvite->table_id,$userID,$message,$messageType='tableaddme');
			}
			else if($actionType == 'rejcet')
			{
				$tblVenuetableinvite->user_action = 'rejcet';
			}
		}
		else if($actionBy == 'venue')
		{
			if($actionType == 'accept')
			{
				$tblVenuetableinvite->venue_action = 'accept';
			}
			else if($actionType == 'rejcet')
			{
				$tblVenuetableinvite->venue_action = 'rejcet';
			}
		}



		if(!$tblVenuetableinvite->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_ADD_ME_ERROR_INVITE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblTable = JTable::getInstance('Table', 'BctedTable');
		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');

		$tblVenue->load($tblVenuetableinvite->venue_id);
		$tblTable->load($tblVenuetableinvite->table_id);
		$tblVenuebooking->load($tblVenuetableinvite->booking_id);

		$userBookingOwner = JFactory::getUser($tblVenuebooking->user_id);
		$userRequested = JFactory::getUser($tblVenuetableinvite->user_id);

		$this->jsonarray['code'] = 200;

		if($actionBy == 'user' && $actionType=='accept')
		{
			$message = JText::sprintf('COM_IJOOMERADV_VENUE_BOOKING_INVITE_ACCEPT_BY_USER',$userBookingOwner->name,$userRequested->name, $tblTable->venue_table_name,$tblVenue->venue_name,date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime)));
			//$message = $userBookingOwner->name .' want to add ' . $userRequested->name . ' for ' . $tblTable->venue_table_name .' at '. $tblVenue->venue_name . ' on '.$tblVenuebooking->venue_booking_datetime;

			//<booking Owner Name>  want to add <requested user name> at ' . $tblVenue->venue_name . ';'.$tblTable->venue_table_name.';'.$tblVenuebooking->venue_booking_datetime;

			$this->jsonarray['pushNotificationData']['id']         = $tblVenuetableinvite->invite_id;
			$this->jsonarray['pushNotificationData']['to']         = $tblVenue->userid;
			$this->jsonarray['pushNotificationData']['message']    = $message;
			$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_REQUESTFORADDMEATTABLETOCLUB'); //'RequestForAddMeAtTableToClub';
			$this->jsonarray['pushNotificationData']['configtype'] = '';

			$extraParam = array();
			$extraParam['inviteID'] = $tblVenuetableinvite->invite_id;
			$this->sendAddMeVenueTableMessage($tblVenuetableinvite->venue_id,0,0,$$tblVenuetableinvite->table_id,$tblVenue->userid,$message,$extraParam,$messageType='tableaddme');
		}

		if($actionBy == 'venue' && $actionType=='accept')
		{
			$message = JText::sprintf('COM_IJOOMERADV_VENUE_BOOKING_INVITE_ACCEPT_BY_CLUB',$tblTable->venue_table_name, $userBookingOwner->name, $tblVenue->venue_name, date('d-m-Y',strtotime($tblVenuebooking->venue_booking_datetime)));
			//$message = 'you can join '.$tblTable->venue_table_name.' table with' . $userBookingOwner->name . ' at ' . $tblVenue->venue_name . ' on '.$tblVenuebooking->venue_booking_datetime;

			$this->jsonarray['pushNotificationData']['id']         = $tblVenuetableinvite->invite_id;
			$this->jsonarray['pushNotificationData']['to']         = $tblVenue->userid;
			$this->jsonarray['pushNotificationData']['message']    = $message;
			$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_VENUETABLEADDMEREQUESTCONFIRM'); //'VenueTableAddMeRequestConfirm';
			$this->jsonarray['pushNotificationData']['configtype'] = '';
		}



		return $this->jsonarray;
	}

	public function sendMessageForInviteUserByOwner($venueID,$companyID,$serviceID,$tableID,$TouserID,$message,$extraParam = array(),$messageType='tableaddme')
	{
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblMessage = JTable::getInstance('Message', 'BctedTable');
		$tblMessage->load(0);

		$data = array();

		$data['venue_id']   = $venueID;
		$data['company_id'] = $companyID;
		$data['table_id']   = $tableID;
		$data['service_id'] = $serviceID;
		$data['userid']     = $TouserID;
		$data['to_userid']  = $TouserID;
		$data['message']    = $message;
		if(count($extraParam)!=0)
		{
			$data['extra_params'] = json_encode($extraParam);
		}
		else
		{
			$data['extra_params'] = "";
		}

		$data['message_type']    = $messageType;
		$data['created']    = date('Y-m-d H:i:s');
		$data['time_stamp'] = time();

		/*if($companyID)
		{
			$tblCompany = JTable::getInstance('Company', 'BctedTable');
			$tblCompany->load($companyID);

			$elementName = $tblCompany->company_name;

			$data['from_userid'] = $tblCompany->userid;

		}
		else if($venueID)
		{
			$tblVenue = JTable::getInstance('Venue', 'BctedTable');
			$tblVenue->load($venueID);

			$elementName = $tblVenue->venue_name;

			$data['from_userid'] = $tblVenue->userid;
		}
		else
		{*/
			$data['from_userid'] = $this->IJUserID;
		//}
		$connectionID = $this->helper->getMessageConnection($data['from_userid'],$data['to_userid']);
		if(!$connectionID)
		{
			return 0;
		}

		$data['connection_id'] = $connectionID;
		$tblMessage->bind($data);

		$tblMessage->store();
	}

	public function sendAddMeVenueTableMessage($venueID,$companyID,$serviceID,$tableID,$TouserID,$message,$extraParam = array(),$messageType='tableaddme')
	{
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblMessage = JTable::getInstance('Message', 'BctedTable');
		$tblMessage->load(0);

		$data = array();

		$data['venue_id']   = $venueID;
		$data['company_id'] = $companyID;
		$data['table_id']   = $serviceID;
		$data['service_id'] = $tableID;
		$data['userid']     = $TouserID;
		$data['to_userid']  = $TouserID;
		$data['message']    = $message;
		if(count($extraParam)!=0)
		{
			$data['extra_params'] = json_encode($extraParam);
		}
		else
		{
			$data['extra_params'] = "";
		}

		$data['message_type']    = $messageType;
		$data['created']    = date('Y-m-d H:i:s');
		$data['time_stamp'] = time();

		/*if($companyID)
		{
			$tblCompany = JTable::getInstance('Company', 'BctedTable');
			$tblCompany->load($companyID);

			$elementName = $tblCompany->company_name;

			$data['from_userid'] = $tblCompany->userid;

		}
		else if($venueID)
		{
			$tblVenue = JTable::getInstance('Venue', 'BctedTable');
			$tblVenue->load($venueID);

			$elementName = $tblVenue->venue_name;

			$data['from_userid'] = $tblVenue->userid;
		}
		else
		{*/
			$data['from_userid'] = $this->IJUserID;
		//}
		$connectionID = $this->helper->getMessageConnection($data['from_userid'],$data['to_userid']);
		if(!$connectionID)
		{
			return 0;
		}

		$data['connection_id'] = $connectionID;
		$tblMessage->bind($data);

		$tblMessage->store();
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"addVenueTables",
	 * 		"taskData":{
	 * 			"tableID":"number",
	 * 			"tableName":"string",
	 * 			"customTableName":"string",
	 * 			"price":"string",
	 * 			"capacity":"number",
	 * 			"description":"string"
	 * 		}
	 * 	}
	 */
	function addVenueTables()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$tableID          = IJReq::getTaskData('tableID',0,'int');
		$premiumTableName = IJReq::getTaskData('tableName','','string');
		$customTableName  = IJReq::getTaskData('customTableName','','string');
		$price            = IJReq::getTaskData('price','','string');
		$premiumTableID   = IJReq::getTaskData('premiumTableID',0,'int');
		$capacity         = IJReq::getTaskData('capacity',0,'int');
		$description      = IJReq::getTaskData('description','','string');
		$venueID          = IJReq::getTaskData('venueID',0,'int');

		if(strtolower($premiumTableName) == 'select premium tablename')
		{
			$premiumTableName = "";
		}

		if(!$venueID || empty($price) || !$capacity)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_DETAIL_NOT_VALID1'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if(empty($premiumTableName) && empty($customTableName))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_DETAIL_NOT_VALID2'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}


		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblTable = JTable::getInstance('Table', 'BctedTable');
		$tblTable->load($tableID);

		$data['venue_table_name']          = $premiumTableName;
		$data['custom_table_name']         = $customTableName;
		$data['premium_table_id']          = $premiumTableID;
		$data['venue_id']                  = $venueID;
		$data['user_id']                   = $this->IJUserID;
		$data['venue_table_currency_code'] = $currencyCode;
		$data['venue_table_currency']      = $currency;
		$data['venue_table_price']         = $price;
		$data['venue_table_capacity']      = $capacity;
		$data['venue_table_description']   = $description;
		$data['venue_table_active']        = '1';

		if (!$tableID)
		{
			$data['venue_table_created']     = date('Y-m-d h:i:s');
		}

		$file = JRequest::getVar('image','','FILES','array');

		if(is_array($file) && isset($file['size']) && $file['size']>0)
		{
			$tableImage = $this->uplaodFile($file,'table',$this->IJUserID);

			if(!empty($tableImage))
			{
				if(!empty($tblTable->venue_table_image) && file_exists(JUri::root().$tblTable->venue_table_image))
				{
					unlink(JUri::root().$tblTable->venue_table_image);
				}

				$data['venue_table_image'] = $tableImage;
			}
		}

		//$data['venue_table_created']     = date('Y-m-d h:i:s');
		$data['venue_table_modified'] = date('Y-m-d h:i:s');
		$data['time_stamp']           = time();

		$tblTable->bind($data);

		if(!$tblTable->store())
		{
			$this->jsonarray['code'] = 500;

			return $this->jsonarray;
		}

		if($premiumTableID)
		{
			$tblPremiumTable = JTable::getInstance('PremiumTable','BctedTable');
			$tblPremiumTable->load($premiumTableID);
			$tblPremiumTable->venue_id = $venueID;
			$tblPremiumTable->venue_table_id = $tblTable->venue_table_id;
			$tblPremiumTable->store();
		}

		$this->jsonarray['code'] = 200;

		$tables[] = $this->helper->getVenueTableDetail($tblTable->venue_table_id);

		$this->jsonarray['tables'] = $tables;

		return $this->jsonarray;
	}

	function getFriendsAttending()
	{
		$user = JFactory::getUser();
		if(!$user->id)
		{
			return array();
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('facebook_friends')
			->from($db->quoteName('#__facebook_joomla_connect'))
			->where($db->quoteName('joomla_userid') . ' = ' . $db->quote($user->id));

		// Set the query and load the result.
		$db->setQuery($query);

		$fbFriends = $db->loadResult();

		if(empty($fbFriends))
		{
			return array();
		}

		$query1 = $db->getQuery(true);

		// Create the base select statement.
		$query1->select('userid')
			->from($db->quoteName('#__bcted_user_profile'))
			->where($db->quoteName('fbid') . ' IN (' . $fbFriends . ')');

		// Set the query and load the result.
		$db->setQuery($query1);

		$foundArray = $db->loadColumn();
		$foundArray = implode(",", $foundArray);

		$app = JFactory::getApplication();
		$input = $app->input;

		// Initialiase variables.
		$query3 = $db->getQuery(true);

		// Create the base select statement.
		$query3->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'))
			->where($db->quoteName('vb.status') . ' = ' . $db->quote('5'))
			->where($db->quoteName('vb.user_status') . ' = ' . $db->quote('5'))
			->where($db->quoteName('vb.is_deleted') . ' = ' . $db->quote('0'))
			->where($db->quoteName('vb.venue_booking_datetime') . ' > ' . $db->quote(date('Y-m-d H:i')))
			->where($db->quoteName('vb.user_id') . ' IN (' . $foundArray . ')')
			->order($db->quoteName('vb.venue_booking_datetime') . ' ASC');

		$query3->select('vt.premium_table_id,vt.venue_table_name,vt.custom_table_name')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		$query3->select('v.venue_name')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');

		$query3->select('u.name,u.username')
			->join('LEFT','#__users AS u ON u.id=vb.user_id');

		$query3->select('bu.fbid')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=vb.user_id');

		// Set the query and load the result.
		$db->setQuery($query3);

		$bookings = $db->loadObjectList();

		$venueIDs = array();
		foreach ($bookings as $key => $booking)
		{
			$venueIDs[] = $booking->venue_id;
		}

		$venueIDs = array_unique($venueIDs);

		return $venueIDs;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"filterVenue",
	 * 		"taskData":{
	 *			"venueName":"string",
	 *			"isSmart":"numner 0/1",
	 *			"isCasual":"numner 0/1",
	 *			"isFood":"numner 0/1",
	 *			"isDrink":"numner 0/1",
	 *			"isSmoking":"numner 0/1",
	 *			"rating":"numner",
	 *			"sign":"numner",
	 *			"otherAttrib":"numner 0/1",
	 *			"nearMe":"numner 0/1",
	 *			"longitude":"numner 0/1",
	 *			"latitude":"numner 0/1",
	 *			"recommendedByBCT:"number 0/1"
	 * 		}
	 * 	}
	 */
	function filterVenue()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$venueName   = IJReq::getTaskData('venueName','','string');

		/*echo "<pre>";
		print_r($this->mainframe);
		echo "</pre>";
		exit;*/

		$isSmart   = IJReq::getTaskData('isSmart',0,'int');
		$isCasual  = IJReq::getTaskData('isCasual',0,'int');
		$isFood    = IJReq::getTaskData('isFood',0,'int');
		$isDrink   = IJReq::getTaskData('isDrink',0,'int');
		$isSmoking = IJReq::getTaskData('isSmoking',0,'int');
		$friendsAttending = IJReq::getTaskData('friendsAttending',0,'int');

		$rating = IJReq::getTaskData('rating',0,'int');
		$sign   = IJReq::getTaskData('sign',0,'int');


		$otherAttrib = IJReq::getTaskData('otherAttrib',0,'int');

		$nearMe    = IJReq::getTaskData('nearMe',0,'int');
		$longitude = IJReq::getTaskData('longitude','0','string');
		$latitude  = IJReq::getTaskData('latitude','0','string');

		$recommendedByBCT = IJReq::getTaskData('recommendedByBCT',0,'int');
		$city             = IJReq::getTaskData('city', '', 'string');

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		/*
		$qry = "SELECT *,(((acos(sin((".$latitude."*pi()/180)) * sin((`Latitude`*pi()/180))+cos((".$latitude."*pi()/180)) * cos((`Latitude`*pi()/180)) * cos(((".$longitude."- `Longitude`)*pi()/180))))*180/pi())*60*1.1515) as distance
			FROM `MyTable`
		WHERE distance >= ".$distance */

		// Create the base select statement.
		$query->select('venue_id,(((acos(sin(('.$latitude.'*pi()/180)) * sin((`latitude`*pi()/180))+cos(('.$latitude.'*pi()/180)) * cos((`latitude`*pi()/180)) * cos((('.$longitude.'- `longitude`)*pi()/180))))*180/pi())*60*1.1515) as distance')
			->from($db->quoteName('#__bcted_venue'))
			->where($db->quoteName('venue_active') . ' = ' . $db->quote('1'));

		if(!empty($venueName))
		{
			$query->where($db->quoteName('venue_name') . ' LIKE  ' . $db->quote('%'.$venueName.'%'));
		}

		if(!empty($city))
		{
			$query->where($db->quoteName('city') . ' LIKE  ' . $db->quote('%'.$city.'%'));
		}

		if($recommendedByBCT)
		{
			$query->where($db->quoteName('r_bcted') . ' = ' . $db->quote(1));
		}

		if($otherAttrib)
		{
			$otherSql = array();
			if($isSmart)
			{
				$otherSql[] = '('.$db->quoteName('is_smart') . ' = ' . $db->quote(1) .')';
				//$query->where($db->quoteName('is_smart') . ' = ' . $db->quote(1));
			}

			if($isCasual)
			{
				$otherSql[] = '('.$db->quoteName('is_Casual') . ' = ' . $db->quote(1) .')';
				//$query->where($db->quoteName('is_Casual') . ' = ' . $db->quote(1));
			}

			if($isDrink)
			{
				$otherSql[] = '('.$db->quoteName('is_drink') . ' = ' . $db->quote(1) .')';
				//$query->where($db->quoteName('is_drink') . ' = ' . $db->quote(1));
			}

			if($isFood)
			{
				$otherSql[] = '('.$db->quoteName('is_food') . ' = ' . $db->quote(1) .')';
				//$query->where($db->quoteName('is_food') . ' = ' . $db->quote(1));
			}

			/*echo $isSmoking;
			exit;*/

			if($isSmoking == 1)
			{
				$otherSql[] = '('.$db->quoteName('is_smoking') . ' = ' . $db->quote(1) .')';
				//$query->where($db->quoteName('is_smoking') . ' = ' . $db->quote(1));
			}
			else if($isSmoking == 2)
			{
				$otherSql[] = '('.$db->quoteName('is_smoking') . ' = ' . $db->quote(0) .')';
			}



			if($rating)
			{
				$otherSql[] = '('.$db->quoteName('venue_rating') . ' = ' . $db->quote($rating) .')';
				//$query->where($db->quoteName('venue_rating') . ' >= ' . $db->quote($rating));
			}

			if($sign)
			{
				$otherSql[] = '('.$db->quoteName('venue_signs') . ' = ' . $db->quote($sign) .')';
				//$query->where($db->quoteName('venue_signs') . ' >= ' . $db->quote($sign));
			}

			/*echo "Call".$friendsAttending;
			exit;*/

			if($friendsAttending)
			{
				$friendsAttendingVenues = $this->getFriendsAttending();
				/*echo "<pre>";
				print_r($friendsAttendingVenues);
				echo "</pre>";
				exit;*/
				if($friendsAttendingVenues)
				{
					$otherSql[] = '('.$db->quoteName('venue_id').' IN ('.implode(",", $friendsAttendingVenues).'))';
				}
				else
				{
					IJReq::setResponseCode(204);
					IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_NOT_FOUND'));
					IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
					return false;
				}
			}

			if(count($otherSql) != 0)
			{
				$otherSqlImpload = implode(' AND ', $otherSql);
				$query->where('('.$otherSqlImpload.')');
			}
		}

		$query->order($db->quoteName('time_stamp') . ' DESC');

		/*echo $query->dump();
		exit;*/
		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$venues = $db->loadObjectList();

			$total     = count($venues);
			$pageNO    = IJReq::getTaskData('pageNO',0);
			$pageLimit = BCTED_VENUE_LIST_LIMIT;

			if($pageNO==0 || $pageNO==1){
				$startFrom=0;
			}else{
				$startFrom = $pageLimit*($pageNO-1);
			}

			$venuesArray =array_slice($venues,$startFrom,$pageLimit);

			$resultVenue =array();

			foreach ($venuesArray as $key => $venue)
			{
				if($nearMe)
				{
					if($venue->distance <= 10)
					{
						$resultVenue[] = $this->helper->getVenueDetail($venue->venue_id);
					}

				}
				else
				{
					$resultVenue[] = $this->helper->getVenueDetail($venue->venue_id);
				}

			}

		}
		catch (RuntimeException $e)
		{
			//throw new RuntimeException($e->getMessage(), $e->getCode());
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_($e->getMessage() . ' - ' . $e->getCode()));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		if(count($resultVenue) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}



		//$resultVenue = usort($resultVenue, 'sortByOrder');
		//$this->aasort($resultVenue,"venueName");
		/*echo "<pre>";
		print_r($resultVenue);
		echo "</pre>";*/
		for($i=0;$i<count($resultVenue);$i++)
		{
			for($j=$i+1;$j<count($resultVenue);$j++)
			{
				if(strtolower($resultVenue[$i]['venueName'])>strtolower($resultVenue[$j]['venueName']))
				{
					$temp = $resultVenue[$i];
					$resultVenue[$i] = $resultVenue[$j];
					$resultVenue[$j] = $temp;
				}
			}
		}
		/*echo "<pre>";
		print_r($resultVenue);
		echo "</pre>";
		exit;*/

		$this->jsonarray['code']      = 200;
		$this->jsonarray['total']     = $total;
		$this->jsonarray['pageLimit'] = $pageLimit;
		$this->jsonarray['venues']    = $resultVenue;

		return $this->jsonarray;
	}

	function aasort (&$array, $key) {
	    $sorter=array();
	    $ret=array();
	    reset($array);
	    foreach ($array as $ii => $va) {
	        $sorter[$ii]=$va[$key];
	    }
	    asort($sorter);
	    foreach ($sorter as $ii => $va) {
	        $ret[$ii]=$array[$ii];
	    }
	    $array=$ret;
	}




	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"deleteVenueTables",
	 * 		"taskData":{
	 * 			"tableID":"number"
	 * 		}
	 * 	}
	 */
	function deleteVenueTables()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$tableID         = IJReq::getTaskData('tableID',0,'int');

		if(!$tableID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblTable = JTable::getInstance('Table', 'BctedTable');
		$tblTable->load($tableID);

		if(!$tblTable->venue_table_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblTable->user_id != $this->IJUserID)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$bookingsAvailable = $this->checkForActiveBooking($tblTable->venue_table_id,$tblTable->venue_id);

		if(count($bookingsAvailable) != 0)
		{
			IJReq::setResponseCode(101);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_ACTIVE_BOOKING_AVAILABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblTable->venue_table_active = 0;
		if(!$tblTable->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_NOT_DELETED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;

	}

	function checkForActiveBooking($tableID,$venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('venue_booking_id')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('venue_table_id') . ' = ' . $db->quote($tableID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote(0))
			->where($db->quoteName('venue_booking_datetime') . ' >= ' . $db->quote(date('Y-m-d H:i')));

		// Set the query and load the result.
		$db->setQuery($query);

		/*echo $query->dump();
		exit;*/

		$result = $db->loadColumn();

		return $result;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"addRetingToVenue",
	 * 		"taskData":{
	 *   		"elementID":"number",
	 *   		"elementType":"string",
	 *   		"comment":"string",
	 *   		"rating":"number"
	 * 		}
	 * 	}
	 */
	function addReting()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$elementID = IJReq::getTaskData('elementID',0,'int');
		$elementType = IJReq::getTaskData('elementType','','string');
		$comment = IJReq::getTaskData('comment','','string');
		$rating  = IJReq::getTaskData('rating',0.0,'decimal');

		if(!$elementID || empty($comment) || !$rating || empty($elementType))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_RATING_DATA_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblRating = JTable::getInstance('Rating', 'BctedTable');
		$tblRating->load(0);

		$data = array();

		$data['rated_id']        = $elementID;
		$data['rating_type']     = $elementType;
		$data['user_id']         = $this->IJUserID;
		$data['rating']          = $rating;
		$data['rating_comment']  = $comment;
		$data['rating_datetime'] = date('Y-m-d h:i:s');
		$data['time_stamp']      = time();

		$tblRating->bind($data);

		if(!$tblRating->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_RATING_DATA_NOT_SAVE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}



		$this->jsonarray['code'] = 200;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"getAccountHistory",
	 * 		"taskData":{
	 *   		"venueID":"number",
	 *   		"pageNO":"number"
	 * 		}
	 * 	}
	 */
	function getAccountHistory()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$venueID   = IJReq::getTaskData('venueID',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');

		$tblVenue->load($venueID);

		/*echo "<pre>";
		print_r($tblVenue);
		echo "</pre>";
		exit;*/

		if(!$tblVenue->venue_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblVenue->userid != $this->IJUserID)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$blockedUsers = $this->helper->getBlackList($tblVenue->venue_id,'Venue');
		/*echo "<pre>";
		print_r($blockedUsers);
		echo "</pre>";
		exit;*/

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'))
			->where($db->quoteName('vb.venue_id') . ' = ' . $db->quote($tblVenue->venue_id))
			->where($db->quoteName('vb.status') . ' = ' . $db->quote(5))
			->order($db->quoteName('vb.venue_booking_id') . ' DESC');

		$query->select('up.last_name,up.phoneno,up.avatar,up.about')
			->join('LEFT','#__bcted_user_profile AS up ON up.userid=vb.user_id');

		$query->select('u.name,u.username')
			->join('LEFT','#__users AS u ON u.id=vb.user_id');

		$query->select('vt.venue_table_price')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		/*$query->select('')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');*/

			//venue_table_id

		// Set the query and load the result.
		$db->setQuery($query);

		/*echo $query->dump();
		exit;*/

		$accountHistories = $db->loadObjectList();

		if(count($accountHistories) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_BOOKING_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);
			return false;
		}

		$pageNO    = IJReq::getTaskData('pageNO',0);
		$pageLimit = BCTED_GENERAL_LIST_LIMIT;

		if($pageNO==0 || $pageNO==1){
			$startFrom=0;
		}else{
			$startFrom = $pageLimit*($pageNO-1);
		}

		$accountHistories =array_slice($accountHistories,$startFrom,$pageLimit);

		$historyResult = array();

		foreach ($accountHistories as $key => $history)
		{

			$tmpData                  = array();
			$tmpData['date']          = date('d-m-Y',strtotime($history->venue_booking_created));
			$tmpData['refernce']      = $history->venue_booking_id;
			$tmpData['amount']        = number_format($history->total_price);
			$tmpData['amountPayable'] = number_format($history->amount_payable);
			$tmpData['userID']        = $history->user_id;
			$bookUser = JFactory::getUser($history->user_id);

			if(!$bookUser->id)
			{
				$bookUser = $this->helper->getDeletedUserProfile($history->user_id);
				/*echo "<pre>";
				print_r($bookUser);
				echo "</pre>";
				exit;*/
				$tmpData['username']     = $bookUser->last_name;
			}
			else
			{
				$tmpData['username']     = $history->name;
			}

			$tmpData['venueID']      = $history->venue_id;
			$tmpData['venueTableID'] = $history->venue_table_id;
			$tmpData['guestsCount']  = $history->venue_booking_number_of_guest;
			$tmpData['userPhone']    = ($history->phoneno)?$history->phoneno:'';

			if($blockedUsers)
			{
				$tmpData['isBlocked'] = (in_array($history->user_id, $blockedUsers))?"1":"0";
			}
			else
			{
				$tmpData['isBlocked'] = "0";
			}


			$tmpData['connectionID'] = $this->helper->getMessageConnectionOnly($history->user_id,$tblVenue->userid);

			$historyResult[] = $tmpData;
		}

		if(count($historyResult) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_BOOKING_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);
			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($historyResult);
		$this->jsonarray['accountHistories'] = $historyResult;

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"bookVenueTableUsingBCD",
	 * 		"taskData":{
	 * 			"bookingID":"number",
	 * 			"BCD":"number"
	 * 		}
	 * 	}
	 */
	function bookVenueTableUsingBCD()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$bookingID = IJReq::getTaskData('bookingID',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblTable        = JTable::getInstance('Table', 'BctedTable');
		$tblVenue        = JTable::getInstance('Venue', 'BctedTable');
		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');

		$tblVenuebooking->load($bookingID);

		if(!$tblVenuebooking->venue_booking_id)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_BOOKING_BCD_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);
			return false;
		}

		$tblVenue->load($tblVenuebooking->venue_id);
		$tblTable->load($tblVenuebooking->venue_table_id);


		if(!$tblVenue->venue_id || !$tblTable->venue_table_id || !$tblTable->venue_table_active)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_BOOKING_BCD_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);
			return false;
		}
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"bookVenueTable",
	 * 		"taskData":{
	 * 			"tableID":"number",
	 * 			"venueID":"number",
	 * 			"date":"date",
	 * 			"time":"time",
	 * 			"isPublic":"number",
	 * 			"guestsCount":"number",
	 * 			"additionalInfo":"string"
	 * 		}
	 * 	}
	 */
	function bookVenueTable()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$tableID = IJReq::getTaskData('tableID',0,'int');
		$venueID = IJReq::getTaskData('venueID',0,'int');

		if(!$tableID || !$venueID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblTable = JTable::getInstance('Table', 'BctedTable');
		$tblTable->load($tableID);

		/*echo "<pre>";
		print_r($tblTable);
		echo "</pre>";
		exit;*/

		if(!$tblTable->venue_table_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($venueID);

		if(!$tblVenue->venue_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$blockedUsers = $this->helper->getBlackList($tblVenue->venue_id,'Venue');

		if(in_array($this->IJUserID, $blockedUsers))
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_USER_IN_BLOCK_LIST'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$date     = IJReq::getTaskData('date','','string');
		$time     = IJReq::getTaskData('time','','string');
		$fromtime = IJReq::getTaskData('fromtime','','string');
		$toTime   = IJReq::getTaskData('toTime','','string');
		$isPublic       = IJReq::getTaskData('isPublic',0, 'int');

		$maleCount   = IJReq::getTaskData('maleCount',0, 'int');
		$femaleCount = IJReq::getTaskData('femaleCount',0, 'int');
		$guestsCount = $maleCount + $femaleCount;

		if($guestsCount <= 0)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BOOKING_INVALID_BOOKING_DETAIL'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$additionalInfo = IJReq::getTaskData('additionalInfo','','string');

		if(empty($date) || empty($fromtime) || empty($toTime))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BOOKING_INVALID_BOOKING_DETAIL'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}


		$fromtime = $this->helper->convertToHMS($fromtime);
		$toTime   = $this->helper->convertToHMS($toTime);

		$isVenueTableAvailable = $this->helper->checkForVenueTableAvaibility($venueID,$tableID,$date,$fromtime,$toTime);
		/*echo $isVenueTableAvailable;
		exit;*/
		if($isVenueTableAvailable != 200)
		{
			IJReq::setResponseCode(601);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BOOKING_TIME_STOL_NOT_AVAILBLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');

		$tblVenuebooking->load(0);

		$bookingData['venue_id']       = $venueID;
		$bookingData['venue_table_id'] = $tableID;
		$bookingData['user_id']        = $this->IJUserID;
		$bookingData['status']         = '1';
		$bookingData['user_status']    = '2';
		$bookingData['is_new']    = '1';
		$bookingData['male_count']     = $maleCount;
		$bookingData['female_count']   = $femaleCount;

		/*$fromtime = $this->helper->convertToHMS($fromtime);
		$toTime   = $this->helper->convertToHMS($toTime);*/

		$bookingData['booking_from_time'] = $fromtime;
		$bookingData['booking_to_time']   = $toTime;

		$bookingData['time_stamp']     = time();

		$bookingData['venue_booking_table_privacy']   = $isPublic;
		$bookingData['venue_booking_datetime']        = $date . " " . $time;
		$bookingData['venue_booking_number_of_guest'] = $guestsCount;
		$bookingData['venue_booking_additional_info'] = $additionalInfo;
		$bookingData['venue_booking_created']         = date('Y-m-d H:i:s');


		$tblVenuebooking->bind($bookingData);

		if(!$tblVenuebooking->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_BOOKING_FAILED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$newBookingID = $tblVenuebooking->venue_booking_id;

		$bookingData = $this->helper->getBookingDetailForVenueTable($newBookingID,'Venue');

		/*if(!empty($tblVenuebooking->venue_booking_additional_info))
		{
			$this->helper->sendMessage($tblVenuebooking->venue_id,0,0,$tblVenuebooking->venue_table_id,$tblVenue->userid,$tblVenuebooking->venue_booking_additional_info,$bookingData,$messageType='newtablebooking');
		}*/

		$this->jsonarray['bookingDetail'] = $bookingData;

		$this->jsonarray['code'] = 200;

		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "venue";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{
			$userProfile    = $this->helper->getUserProfile($tblVenue->userid);
			$receiveRequest = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveRequest = $params->settings->pushNotification->receiveRequest;
			}

			if($receiveRequest)
			{
				/*echo "<pre>";
				print_r($tblTable);
				echo "</pre>";
				exit;*/
				if($tblTable->premium_table_id)
				{
					$message = JText::sprintf('COM_IJOOMERADV_VENUE_NEW_REQUEST_FOR_TABLE',$tblTable->venue_table_name);
					$message = 'You have new request for '.$tblTable->venue_table_name;
				}
				else
				{
					$message = JText::sprintf('COM_IJOOMERADV_VENUE_NEW_REQUEST_FOR_TABLE',$tblTable->custom_table_name);
					$message = 'You have new request for '.$tblTable->custom_table_name;
				}


				$this->jsonarray['pushNotificationData']['id']         = $obj->id;
				$this->jsonarray['pushNotificationData']['to']         = $tblVenue->userid;
				$this->jsonarray['pushNotificationData']['message']    = $message;
				$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWREQUESTRECEIVED'); //'NewRequestReceived';
				$this->jsonarray['pushNotificationData']['configtype'] = '';
			}

		}



		return $this->jsonarray;

		/*$this->jsonarray['code'] = 200;

		return $this->jsonarray;*/

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"checkBookingAvailability",
	 * 		"taskData":{
	 * 			"bookingID":"number"
	 * 		}
	 * 	}
	 */
	function checkBookingAvailability()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$bookingID = IJReq::getTaskData('bookingID',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');
		$tblVenuebooking->load($bookingID);

		if(!$tblVenuebooking->venue_booking_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_BOOKING_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}
		$currentBookingDate = $tblVenuebooking->venue_booking_datetime;
		$datesArray         = array();
		$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' -1 day'));
		$datesArray[]       = $tblVenuebooking->venue_booking_datetime;
		$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' +1 day'));
		$currentBookingFrom = $tblVenuebooking->booking_from_time;
		$currentBookingTo   = $tblVenuebooking->booking_to_time;

		if (strtotime($currentBookingFrom)>strtotime($currentBookingTo))
		{
			$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
			$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo.' +1 day'));
		} else {
			$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
			$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo));
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($tblVenuebooking->venue_id))
			->where($db->quoteName('venue_table_id') . ' = ' . $db->quote($tblVenuebooking->venue_table_id))
			->where($db->quoteName('venue_booking_id') . ' <> ' . $db->quote($tblVenuebooking->venue_booking_id))
			->where($db->quoteName('status') . ' = ' . $db->quote(5))
			->where($db->quoteName('venue_booking_datetime') . ' IN (\''. implode("','", $datesArray) .'\')' );

		// Set the query and load the result.
		$db->setQuery($query);

		$slotBooked = 0;
		$bookingsOnSameDate = $db->loadObjectList();
		$timeSlots = array();

		foreach ($bookingsOnSameDate as $key => $booking)
		{
			$bookingDate = $booking->venue_booking_datetime;
			$bookingFrom = $booking->booking_from_time;
			$bookingTo = $booking->booking_to_time;

			if (strtotime($bookingDate.' '.$bookingFrom)>strtotime($bookingDate.' '.$bookingTo)) {
				$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
				$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo.' +1 days'));
			} else {
				$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
				$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo));
			}

			$numbercurrentBookingFrom = strtotime($currentBookingFrom);
			$numbercurrentBookingTo   = strtotime($currentBookingTo);
			$numberbookingFrom        = strtotime($bookingFrom);
			$numberbookingTo          = strtotime($bookingTo);

			/*echo "currentBookingFrom:" . $currentBookingFrom . " || ".$numbercurrentBookingFrom."<br />";
			echo "currentBookingTo:" . $currentBookingTo . " || ".$numbercurrentBookingTo."<br /><br />";
			echo "bookingFrom:" . $bookingFrom . " || ".$numberbookingFrom."<br />";
			echo "bookingTo:" . $bookingTo . " || ".$numberbookingTo."<br /><br />";*/

			if(($numberbookingFrom < $numbercurrentBookingFrom) && ($numbercurrentBookingFrom < $numberbookingTo))
			{
				$slotBooked = $slotBooked + 1;
			}
			else if(($numberbookingFrom < $numbercurrentBookingTo) && ($numbercurrentBookingTo < $numberbookingTo))
			{
				$slotBooked = $slotBooked + 1;
			}
		}

		if($slotBooked)
		{
			IJReq::setResponseCode(601);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BOOKING_TIME_STOL_NOT_AVAILBLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"changeBookingStatus",
	 * 		"taskData":{
	 * 			"bookingID":"number",
	 * 			"venueID":"number",
	 * 			"action":"Decline/Waiting/Accept",
	 * 			"message":"string",
	 * 		}
	 * 	}
	 */
	function changeBookingStatus()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$venueID   = IJReq::getTaskData('venueID',0,'int');
		$bookingID = IJReq::getTaskData('bookingID',0,'int');
		$action    = IJReq::getTaskData('action','','string');
		$message   = IJReq::getTaskData('message','','string');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');

		$tblVenue->load($venueID);

		if(!$tblVenue->venue_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblVenue->userid != $this->IJUserID)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');
		$tblVenuebooking->load($bookingID);

		if(!$tblVenuebooking->venue_booking_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_BOOKING_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if(empty($action))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_BOOKING_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$newStatus = $tblVenuebooking->status;
		$newUserStatus = $tblVenuebooking->user_status;

		if($action == 'Decline')
		{
			$newStatus     = $this->helper->getStatusIDFromStatusName('Decline');
			$newUserStatus = $this->helper->getStatusIDFromStatusName('Unavailable');
		}
		else if($action == 'Waiting')
		{
			$newStatus = $this->helper->getStatusIDFromStatusName('Waiting List');
			$newUserStatus = $this->helper->getStatusIDFromStatusName('Waiting List');
		}
		else if($action == 'Accept')
		{
			$currentBookingDate = $tblVenuebooking->venue_booking_datetime;
			$datesArray         = array();
			$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' -1 day'));
			$datesArray[]       = $tblVenuebooking->venue_booking_datetime;
			$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' +1 day'));
			$currentBookingFrom = $tblVenuebooking->booking_from_time;
			$currentBookingTo   = $tblVenuebooking->booking_to_time;

			if (strtotime($currentBookingFrom)>strtotime($currentBookingTo))
			{
				$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
				$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo.' +1 day'));
			} else {
				$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
				$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo));
			}

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('*')
				->from($db->quoteName('#__bcted_venue_booking'))
				->where($db->quoteName('venue_id') . ' = ' . $db->quote($tblVenuebooking->venue_id))
				->where($db->quoteName('venue_table_id') . ' = ' . $db->quote($tblVenuebooking->venue_table_id))
				->where($db->quoteName('venue_booking_id') . ' <> ' . $db->quote($tblVenuebooking->venue_booking_id))
				->where($db->quoteName('status') . ' = ' . $db->quote(5))
				->where($db->quoteName('venue_booking_datetime') . ' IN (\''. implode("','", $datesArray) .'\')' );

			// Set the query and load the result.
			$db->setQuery($query);

			$slotBooked = 0;
			$bookingsOnSameDate = $db->loadObjectList();
			$timeSlots = array();

			foreach ($bookingsOnSameDate as $key => $booking)
			{
				$bookingDate = $booking->venue_booking_datetime;
				$bookingFrom = $booking->booking_from_time;
				$bookingTo = $booking->booking_to_time;

				/*echo "<br />Date Time : " . $bookingDate .' '.$bookingTo;
				echo "<br />Combine : ".date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo.' +1 days'));
				exit;


				echo "<br />Date : " . $bookingDate;
				echo "<br />From : ".strtotime($bookingDate.' '.$bookingFrom);
				echo "<br />To : ".strtotime($bookingDate.' '.$bookingTo);*/
				//exit;

				if (strtotime($bookingDate.' '.$bookingFrom)>strtotime($bookingDate.' '.$bookingTo)) {
					$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
					$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo.' +1 days'));
				} else {
					$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
					$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo));
				}

				/*echo "<br /> Booking To Last : " .$bookingTo;
				exit;*/

				$numbercurrentBookingFrom = strtotime($currentBookingFrom);
				$numbercurrentBookingTo   = strtotime($currentBookingTo);
				$numberbookingFrom        = strtotime($bookingFrom);
				$numberbookingTo          = strtotime($bookingTo);

				/*echo "currentBookingFrom:" . $currentBookingFrom . " || ".$numbercurrentBookingFrom."<br />";
				echo "currentBookingTo:" . $currentBookingTo . " || ".$numbercurrentBookingTo."<br /><br />";
				echo "bookingFrom:" . $bookingFrom . " || ".$numberbookingFrom."<br />";
				echo "bookingTo:" . $bookingTo . " || ".$numberbookingTo."<br /><br />";*/

				if(($numberbookingFrom < $numbercurrentBookingFrom) && ($numbercurrentBookingFrom < $numberbookingTo))
				{
					$slotBooked = $slotBooked + 1;
				}
				else if(($numberbookingFrom < $numbercurrentBookingTo) && ($numbercurrentBookingTo < $numberbookingTo))
				{
					$slotBooked = $slotBooked + 1;
				}
				else if(($numberbookingTo == $numbercurrentBookingTo) && ($numbercurrentBookingFrom == $numberbookingFrom))
				{
					$slotBooked = $slotBooked + 1;
				}
			}

			if($slotBooked)
			{
				IJReq::setResponseCode(601);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BOOKING_TIME_STOL_NOT_AVAILBLE'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

			$newStatus = $this->helper->getStatusIDFromStatusName('Awaiting Payment');
			$newUserStatus = $this->helper->getStatusIDFromStatusName('Available');
		}

		/*echo $newStatus;
		exit;*/

		/*$dataVTB['status'] = $newStatus;
		$dataVTB['owner_message'] = $message;*/

		$tblVenuebooking->status = $newStatus;
		$tblVenuebooking->user_status = $newUserStatus;
		$tblVenuebooking->is_new = '0';
		$tblVenuebooking->owner_message = $message;



		if(!$tblVenuebooking->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BOOKING_STATUS_NOT_CHANGED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		/*if(!empty($tblVenuebooking->owner_message))
		{
			$this->helper->sendMessage($tblVenuebooking->venue_id,0,0,$tblVenuebooking->venue_table_id,$tblVenue->userid,$tblVenuebooking->owner_message,$bookingData,$messageType='tablerequestaccepted');
		}*/

		$bookingData = $this->helper->getBookingDetailForVenueTable($tblVenuebooking->venue_booking_id,'User');

		/*echo "<pre>";
		print_r($bookingData);
		echo "</pre>";
		exit;*/


		$this->jsonarray['code'] = 200;

		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "venue";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{
			$userProfile    = $this->helper->getUserProfile($tblVenuebooking->user_id);
			$updateMyBookingStatus = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$updateMyBookingStatus = $params->settings->pushNotification->updateMyBookingStatus;
			}

			if($updateMyBookingStatus)
			{
				$message = JText::sprintf('PUSHNOTIFICATION_TYPE_REQUESTSTATUSCHANGED_MESSAGE',$tblVenue->venue_name);
				$message = 'Your booking Status has been changed by '.$tblVenue->venue_name;


				$this->jsonarray['pushNotificationData']['id']         = $obj->id;
				$this->jsonarray['pushNotificationData']['to']         = $tblVenuebooking->user_id;
				$this->jsonarray['pushNotificationData']['message']    = $message;
				$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_REQUESTSTATUSCHANGED'); //'Request Status Changed';
				$this->jsonarray['pushNotificationData']['configtype'] = '';
			}


		}

		return $this->jsonarray;

	}



	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"getMyBooking",
	 * 		"taskData":{
	 * 		}
	 * 	}
	 */
	function getMyBooking()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$venueID   = IJReq::getTaskData('venueID',0,'int');
		$companyID = IJReq::getTaskData('companyID',0,'int');
		$filter    = IJReq::getTaskData('filter','','string');

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$queryV = $db->getQuery(true);
		$queryC = $db->getQuery(true);

		$isOnlyVenue = 0;
		$isOnlyCompany = 0;

		$totalRequests = 0;
		$totalBooking = 0;
		$totalRevenue = 0;

		$summaries = array();

		if($venueID)
		{
			JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
			$tblVenue = JTable::getInstance('Venue', 'BctedTable');

			$tblVenue->load($venueID);

			if(!$tblVenue->venue_id)
			{
				IJReq::setResponseCode(400);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

			if($tblVenue->userid != $this->IJUserID)
			{
				IJReq::setResponseCode(706);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

			// Create the base select statement.
			$queryV->select('vb.venue_booking_id,vb.venue_table_id AS in_booking_table')
				->from($db->quoteName('#__bcted_venue_booking','vb'))
				->where($db->quoteName('vb.venue_id') . ' = ' . $db->quote($tblVenue->venue_id))
				->where($db->quoteName('vb.is_deleted') . ' = ' . $db->quote('0'))
				->where($db->quoteName('vb.deleted_by_venue') . ' = ' . $db->quote('0'));

			$queryV->select('vt.venue_table_id,vt.venue_table_name')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

			$queryV->where($db->quoteName('vt.venue_table_id') . ' <> ' . $db->quote(0));

			if($filter == 'Booked')
			{
				$statusID[] = $this->helper->getStatusIDFromStatusName('Booked');
				$statusID[] = $this->helper->getStatusIDFromStatusName('Cancelled');

				$statusStr = implode(",", $statusID);

				$queryV->where($db->quoteName('vb.status') . ' IN ('.$statusStr.')');
			}
			else if($filter == 'Request')
			{
				$statusID[] = $this->helper->getStatusIDFromStatusName('Booked');
				$statusID[] = $this->helper->getStatusIDFromStatusName('Cancelled');

				$statusStr = implode(",", $statusID);

				$queryV->where($db->quoteName('vb.status') . ' NOT IN ('.$statusStr.')');
			}

			//$queryV->order($db->quoteName('venue_booking_datetime') . ' ASC');
			$queryV->order($db->quoteName('vb.time_stamp') . ' DESC');

			/*echo "<pre>";
			print_r($queryV->dump());
			echo "</pre>";
			exit;*/

			$isOnlyVenue = 1;

			$summaries = $this->helper->summaryForVenue($venueID);
			/*echo "<pre>";
			print_r($summaries);
			echo "</pre>";
			exit;*/

			foreach ($summaries as $key => $summary)
			{

				$statusText = $this->helper->getStatusNameFromStatusID($summary->status);
				if($statusText == 'Request')
				{
					$totalRequests = $summary->total_count;
				}
				else if($statusText == 'Booked')
				{
					$totalBooking = $summary->total_count;
				}

				$statusText = "";
			}
			$totalRequests = $this->helper->getTotalRequestForVenue($venueID);
			$totalRevenue = $this->helper->getRevenueForVenue($venueID);
		}
		else if($companyID)
		{
			JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
			$tblCompany = JTable::getInstance('Company', 'BctedTable');

			$tblCompany->load($companyID);

			if(!$tblCompany->company_id)
			{
				IJReq::setResponseCode(400);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_INVALID_COMPANY'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

			if($tblCompany->userid != $this->IJUserID)
			{
				IJReq::setResponseCode(706);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PERMISSION_DENIED'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

			// Create the base select statement.
			$queryC->select('service_booking_id')
				->from($db->quoteName('#__bcted_service_booking'))
				->where($db->quoteName('company_id') . ' = ' . $db->quote($tblCompany->company_id))
				->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
				->where($db->quoteName('deleted_by_company') . ' = ' . $db->quote('0'));

			if($filter == 'Booked')
			{
				$statusID[] = $this->helper->getStatusIDFromStatusName('Booked');
				$statusID[] = $this->helper->getStatusIDFromStatusName('Cancelled');

				$statusStr = implode(",", $statusID);

				$queryC->where($db->quoteName('status') . ' IN ('.$statusStr.')');
			}
			else if($filter == 'Request')
			{
				$statusID[] = $this->helper->getStatusIDFromStatusName('Booked');
				$statusID[] = $this->helper->getStatusIDFromStatusName('Cancelled');

				$statusStr = implode(",", $statusID);

				$queryC->where($db->quoteName('status') . ' NOT IN ('.$statusStr.')');
			}

			//$queryC->order($db->quoteName('service_booking_datetime') . ' ASC');
			$queryC->order($db->quoteName('time_stamp') . ' DESC');

			$isOnlyCompany = 1;

			$summaries = $this->helper->summaryForCompany($companyID);

			/*echo "<pre>";
			print_r($summaries);
			echo "</pre>";*/

			foreach ($summaries as $key => $summary)
			{
				/*echo "<pre>";
				print_r($summary);
				echo "</pre>";*/
				$statusText = $this->helper->getStatusNameFromStatusID($summary->status);

				//echo $statusText . "<br >";

				if($statusText == 'Request')
				{
					$totalRequests = $summary->total_count;
				}
				else if($statusText == 'Booked')
				{
					/*echo $statusText;
					exit;*/
					$totalBooking = $summary->total_count;
				}

				$statusText = "";

			}

			$totalRequests = $this->helper->getTotalRequestForCompany($companyID);
			$totalRevenue = $this->helper->getRevenueForCompnay($companyID);
		}
		else
		{
			// Create the base select statement.
			$queryV->select('venue_booking_id')
				->from($db->quoteName('#__bcted_venue_booking'))
				->where($db->quoteName('user_id') . ' = ' . $db->quote($this->IJUserID))
				->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
				->where($db->quoteName('deleted_by_user') . ' = ' . $db->quote('0'))
				->order($db->quoteName('time_stamp') . ' DESC');
				//->order($db->quoteName('venue_booking_datetime') . ' ASC');

				//$queryC->order($db->quoteName('time_stamp') . ' DESC');

			// Create the base select statement.
			$queryC->select('service_booking_id')
				->from($db->quoteName('#__bcted_service_booking'))
				->where($db->quoteName('user_id') . ' = ' . $db->quote($this->IJUserID))
				->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
				->where($db->quoteName('deleted_by_user') . ' = ' . $db->quote('0'))
				->order($db->quoteName('time_stamp') . ' DESC');
				//->order($db->quoteName('service_booking_datetime') . ' ASC');

			$isOnlyCompany = 1;
			$isOnlyVenue = 1;
		}

		/*echo $queryV->dump();
		exit;*/

		// Set the query and load the result.

		if($isOnlyVenue)
		{
			$db->setQuery($queryV);
			$myBookingIDs = $db->loadColumn();

			/*echo "<pre>";
			print_r($myBookingIDs);
			echo "</pre>";
			exit;*/
		}
		else
		{
			$myBookingIDs = array();
		}


		/*echo "<pre>";
		print_r($queryV->dump());
		echo "</pre>";
		exit;*/



		/*echo "<pre>";
		print_r($myBookingIDs);
		echo "</pre>";
		exit;*/

		$total     = count($myBookingIDs);

		if($isOnlyCompany)
		{

			/*echo $queryC->dump();
			exit;*/

			$db->setQuery($queryC);

			$myBookingServiceIDs = $db->loadColumn();

			/*echo "<pre>";
			print_r($myBookingServiceIDs);
			echo "</pre>";
			exit;*/
		}
		else
		{
			$myBookingServiceIDs = array();
		}



		//$total     = count($myBookingIDs);

		if(count($myBookingIDs) == 0 && count($myBookingServiceIDs) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_BOOKING_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);
			return false;
		}

		$pageNO    = IJReq::getTaskData('pageNO',0);
		$pageLimit = BCTED_GENERAL_LIST_LIMIT;

		if($pageNO==0 || $pageNO==1){
			$startFrom=0;
		}else{
			$startFrom = $pageLimit*($pageNO-1);
		}

		//$myBookingIDs =array_slice($myBookingIDs,$startFrom,$pageLimit);

		/*if(count($myBookingIDs) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_BOOKING_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);
			return false;
		}*/

		$myBookedVT = array();
		$myBookedCS = array();

		if(count($myBookingIDs))
		{
			if($isOnlyVenue == 1 && $isOnlyCompany == 1)
			{
				$myBookedVT = $this->helper->getBookingDetailForVenueTable($myBookingIDs,'User');
			}
			else
			{
				$myBookedVT = $this->helper->getBookingDetailForVenueTable($myBookingIDs,'Venue');
			}

		}

		if(count($myBookingServiceIDs))
		{
			if($isOnlyCompany == 1 && $isOnlyVenue == 1)
			{
				$myBookedCS = $this->helper->getBookingDetailForServices($myBookingServiceIDs,'User');
			}
			else
			{
				$myBookedCS = $this->helper->getBookingDetailForServices($myBookingServiceIDs,'Company');
			}

		}

		$total = count($myBookedVT) + count($myBookedCS);

		if(count($myBookedVT) == 0 && count($myBookedCS) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_BOOKING_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);
			return false;
		}

		$this->jsonarray['code']           = 200;
		$this->jsonarray['total']          = $total;
		$this->jsonarray['pageLimit']      = $pageLimit;
		$this->jsonarray['bookedTables']   = $myBookedVT;
		$this->jsonarray['bookedServices'] = $myBookedCS;



		$this->jsonarray['totalRequests'] = $totalRequests;
		$this->jsonarray['totalBooking']  = $totalBooking;
		$this->jsonarray['totalRevenue']  = $totalRevenue;

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"deleteConfirmedBooking",
	 * 		"taskData":{
	 * 			"venueBookingID":"number"
	 * 		}
	 * 	}
	 */
	function deleteConfirmedBooking()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$BookingID = IJReq::getTaskData('venueBookingID',0,'int');

		/*echo $BookingID;
		exit;*/

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenuebooking     = JTable::getInstance('Venuebooking', 'BctedTable');

		$tblVenuebooking->load($BookingID);

		/*echo "call<pre>";
		print_r($Venuebooking);
		echo "</pre>";
		exit;*/

		if(!$tblVenuebooking->venue_booking_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*echo "<pre>";
		print_r($tblVenuebooking);
		echo "</pre>";
		exit;*/

		$statusID[] = $this->helper->getStatusIDFromStatusName('Booked');
		$statusID[] = $this->helper->getStatusIDFromStatusName('Cancelled');
		$statusID[] = $this->helper->getStatusIDFromStatusName('Decline');
		$statusID[] = $this->helper->getStatusIDFromStatusName('Unavailable');
		$statusID[] = $this->helper->getStatusIDFromStatusName('Waiting List');

		//if($statusID != $tblVenuebooking->status)
		if(!in_array($tblVenuebooking->status, $statusID))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_BOOKING_STATUS_NOT_BOOKING'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$bookingDate = date('Y-m-d',strtotime($tblVenuebooking->venue_booking_datetime));

		//strtotime("-30 days")
		$bookingTime = strtotime($bookingDate);

		$currentDate = date('Y-m-d');
		$currentTime = strtotime($currentDate);

		$currentTime = $currentTime + 10;

		/*echo "start Date : " .$bookingDate . ' - ' . $currentDate.'<br />';
		echo "start : " .$bookingTime . ' - ' . $currentTime.'<br />';*/
		//start : 1432060200 - 1429468200<br /
		/*echo $this->helper->getStatusIDFromStatusName('Cancelled').'||'.$tblVenuebooking->status;
		exit;*/
		if($this->helper->getStatusIDFromStatusName('Cancelled')==$tblVenuebooking->status)
		{

		}
		else if($this->helper->getStatusIDFromStatusName('Booked')==$tblVenuebooking->status)
		{
			if($bookingTime>=$currentTime)
			{
				IJReq::setResponseCode(706);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_BOOKING_NOT_PAST_BOOKING'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}
		}


		/*echo "End". $bookingDate . ' - ' . $currentDate;
		exit;*/

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($tblVenuebooking->venue_id);

		/*echo $tblVenue->userid . " || " . $this->IJUserID;
		exit;*/

		if($tblVenue->userid == $this->IJUserID)
		{
			/*IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;*/
			$tblVenuebooking->deleted_by_venue = 1;
		}
		elseif($tblVenuebooking->user_id == $this->IJUserID)
		{
			$tblVenuebooking->deleted_by_user = 1;
		}
		elseif($this->helper->getStatusIDFromStatusName('Waiting List')==$tblVenuebooking->status)
		{
			$tblVenuebooking->deleted_by_user = 1;
			$tblVenuebooking->deleted_by_venue = 1;
		}


		//$tblVenuebooking->is_deleted = 1;

		if(!$tblVenuebooking->store())
		{
			$this->jsonarray['code'] = 500;

			return $this->jsonarray;
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	  	{
	  		"extName":"bcted",
	  		"extView":"venue",
	  		"extTask":"cancelBooking",
	  		"taskData":{
	  			"venueBookingID":"number";
	  		}
	  	}
	 */
	function cancelBooking()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$bookingID = IJReq::getTaskData('venueBookingID',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenuebooking     = JTable::getInstance('Venuebooking', 'BctedTable');

		$tblVenuebooking->load($bookingID);

		if(!$tblVenuebooking->venue_booking_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_BOOKING_ID_INVALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*echo $this->IJUserID."<pre>";
		print_r($tblVenuebooking);
		echo "</pre>";
		exit;*/

		if($tblVenuebooking->user_id  != $this->IJUserID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_BOOKING_NOT_YOUR'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$statusID = $this->helper->getStatusIDFromStatusName('Cancelled');
		$tblVenuebooking->status=$statusID;
		$tblVenuebooking->user_status=$statusID;

		if(!$tblVenuebooking->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_BOOKING_NOT_CANCELED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_payment_status'))
			->where($db->quoteName('booked_element_id') . ' = ' . $db->quote($tblVenuebooking->venue_booking_id))
			->where($db->quoteName('booked_element_type') . ' = ' . $db->quote('venue'))
			->where($db->quoteName('paid_status') . ' = ' . $db->quote('1'));

		// Set the query and load the result.
		$db->setQuery($query);

		$paymentStatus = $db->loadObject();
		if($paymentStatus)
		{
			$queryLP = $db->getQuery(true);
			$queryLP->select('*')
				->from($db->quoteName('#__bcted_loyalty_point'))
				->where($db->quoteName('user_id') . ' = ' . $db->quote($tblVenuebooking->user_id))
				->where($db->quoteName('point_app') . ' = ' . $db->quote('purchase.venue'))
				->where($db->quoteName('cid') . ' = ' . $db->quote($paymentStatus->payment_id));

			// Set the query and load the result.
			$db->setQuery($queryLP);

			$loyaltyPointDetail = $db->loadObject();

			if($loyaltyPointDetail)
			{
				$query = $db->getQuery(true);

				// Create the base insert statement.
				$query->insert($db->quoteName('#__bcted_loyalty_point'))
					->columns(
						array(
							$db->quoteName('user_id'),
							$db->quoteName('earn_point'),
							$db->quoteName('point_app'),
							$db->quoteName('cid'),
							$db->quoteName('is_valid'),
							$db->quoteName('created'),
							$db->quoteName('time_stamp')
						)
					)
					->values(
						$db->quote($tblVenuebooking->user_id) . ', ' .
						$db->quote(($loyaltyPointDetail->earn_point * (-1))) . ', ' .
						$db->quote('venue.cancelbooking') . ', ' .
						$db->quote($loyaltyPointDetail->cid) . ', ' .
						$db->quote(1) . ', ' .
						$db->quote(date('Y-m-d H:i:s')) . ', ' .
						$db->quote(time())
					);

				// Set the query and execute the insert.
				$db->setQuery($query);

				$db->execute();

			}
		}

		$this->jsonarray['code'] = 200;

		$newBookingID = $tblVenuebooking->venue_booking_id;

		$bookingData = $this->helper->getBookingDetailForVenueTable($newBookingID,'Venue');

		//COM_IJOOMERADV_COMPANY_SERVICE_BOOKING_CANCEL_MESSAGE="User %s has cancelled their booking for %s"

		$tblTable = JTable::getInstance('Table', 'BctedTable');
		$tblTable->load($tblVenuebooking->venue_table_id);
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($tblVenuebooking->venue_id);

		$user = JFactory::getUser();

		$cancelMessage = JText::sprintf('COM_IJOOMERADV_COMPANY_VENUE_BOOKING_CANCEL_MESSAGE',$user->name,($tblTable->premium_table_id)?$tblTable->venue_table_name:$tblTable->custom_table_name);

		$this->helper->sendMessage($tblVenuebooking->venue_id,0,0,$tblVenuebooking->venue_table_id,$tblVenue->userid,$cancelMessage,$bookingData,$messageType='tablebookingcancel');


		$this->jsonarray['bookingDetail'] = $bookingData;

		$this->jsonarray['code'] = 200;

		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "venue";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{
			$userProfile    = $this->helper->getUserProfile($tblVenue->userid);
			$receiveRequest = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveRequest = $params->settings->pushNotification->receiveRequest;
			}

			$receiveRequest = 1;

			if($receiveRequest)
			{
				$message = $cancelMessage;
				$this->jsonarray['pushNotificationData']['id']         = $obj->id;
				$this->jsonarray['pushNotificationData']['to']         = $tblVenue->userid;
				$this->jsonarray['pushNotificationData']['message']    = $message;
				$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_TABLEBOOKINGCANCEL'); //'tablebookingcancel';
				$this->jsonarray['pushNotificationData']['configtype'] = '';
			}

		}

		return $this->jsonarray;

	}

	/*function updateLoyaltyPoints($userID,$activity,$totalPrice,$paymentEntryID,$isValid = 0)
	{
		$tblLoyaltyPoint = JTable::getInstance('LoyaltyPoint', 'BctedTable');
		$tblLoyaltyPoint->load(0);

		$lpPost['user_id']    = $userID;
		$lpPost['earn_point'] = $totalPrice;
		$lpPost['point_app']  = $activity;
		$lpPost['cid']        = $paymentEntryID;
		$lpPost['is_valid']   = $isValid;
		$lpPost['created']    = date('Y-m-d H:i:s');
		$lpPost['time_stamp'] = time();


		$tblLoyaltyPoint->bind($lpPost);
		$tblLoyaltyPoint->store();
	}*/

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"deleteBooking",
	 * 		"taskData":{
	 * 			"venueBookingID":"number"
	 * 		}
	 * 	}
	 */
	function deleteBooking()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$BookingID = IJReq::getTaskData('venueBookingID',0,'int');

		/*echo $BookingID;
		exit;*/

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenuebooking     = JTable::getInstance('Venuebooking', 'BctedTable');

		$tblVenuebooking->load($BookingID);

		/*echo "call<pre>";
		print_r($Venuebooking);
		echo "</pre>";
		exit;*/

		if(!$tblVenuebooking->venue_booking_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*echo "<pre>";
		print_r($tblVenuebooking);
		echo "</pre>";
		exit;*/

		$statusID = $this->helper->getStatusIDFromStatusName('Request');

		if($statusID != $tblVenuebooking->status)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_BOOKING_STATUS_NOT_BOOKING'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($tblVenuebooking->venue_id);

		/*echo $tblVenue->userid . " || " . $this->IJUserID;
		exit;*/

		if($tblVenue->userid != $this->IJUserID)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenuebooking->is_deleted = 1;

		if(!$tblVenuebooking->store())
		{
			$this->jsonarray['code'] = 500;

			return $this->jsonarray;
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"requestVenueGuestList",
	 * 		"taskData":{
	 * 			"venueID":"number",
	 * 			"date":"string,"
	 *			"guestsCount":"string",
	 *			"additionalInfo":"string"
	 * 		}
	 * 	}
	 */
	function requestVenueGuestList()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition


		$venueID = IJReq::getTaskData('venueID',0,'int');

		if(!$venueID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($venueID);

		if(!$tblVenue->venue_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$date           = IJReq::getTaskData('date','','string');
		//$guestsCount    = IJReq::getTaskData('guestsCount',1, 'int');
		$maleCount    = IJReq::getTaskData('maleCount',0, 'int');
		$femaleCount    = IJReq::getTaskData('femaleCount',0, 'int');
		$guestsCount = $maleCount + $femaleCount;
		$additionalInfo = IJReq::getTaskData('additionalInfo','','string');

		if(empty($date))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_INVALID_GUEST_LIST_REQUEST_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenueguestlist = JTable::getInstance('Venueguestlist', 'BctedTable');
		$tblVenueguestlist->load(0);

		$guestListData['venue_id']        = $tblVenue->venue_id;
		$guestListData['user_id']         = $this->IJUserID;
		$guestListData['request_date']    = $date;
		$guestListData['status']          = 1;
		$guestListData['user_status']     = 2;
		$guestListData['number_of_guest'] = $guestsCount;
		$guestListData['male_count']      = $maleCount;
		$guestListData['female_count']    = $femaleCount;

		$guestListData['remaining_total']  = $guestsCount;
		$guestListData['remaining_male']   = $maleCount;
		$guestListData['remaining_female'] = $femaleCount;

		$guestListData['additional_info'] = $additionalInfo;
		$guestListData['time_stamp']      = time();
		$guestListData['created']         = date('Y-m-d H:i:s');

		$tblVenueguestlist->bind($guestListData);

		if(!$tblVenueguestlist->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_INVALID_GUEST_LIST_REQUEST_NOT_STORE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$extraParam = array();
		$extraParam['guestListRequstID'] = $tblVenueguestlist->venue_guest_list_id;
		$extraParam['requestedDate'] = date('d-m-Y',strtotime($tblVenueguestlist->request_date));
		$extraParam['maleCount'] = $tblVenueguestlist->male_count;
		$extraParam['femaleCount'] = $tblVenueguestlist->female_count;
		$extraParam['additionalInfo'] = $tblVenueguestlist->additional_info;

		$loginUser = JFactory::getUser();

		$message = $loginUser->name . ' has request for Guest list on '.date('d-m-Y',strtotime($tblVenueguestlist->request_date));

		//$this->sendAddMeVenueTableMessage($venueID,$companyID,$serviceID,$tableID,$TouserID,$message,$extraParam = array(),$messageType='tableaddme');
		$this->sendAddMeVenueTableMessage($tblVenue->venue_id,0,0,0,$tblVenue->userid,$message,$extraParam,$messageType='venueguestlistrequest');

		$pushcontentdata =array();
		$requestData     = $this->helper->getVenueGuestListRequest2($tblVenueguestlist->venue_guest_list_id);
		/*echo "<pre>";
		print_r($requestData);
		echo "</pre>";
		exit;*/
		$pushcontentdata['data']               = $requestData;
		$pushcontentdata['elementType']        = "venue";
		$pushOptions['detail']['content_data'] = $pushcontentdata;

		$pushOptions = gzcompress(json_encode($pushOptions));

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{

			$userProfile    = $this->helper->getUserProfile($tblVenue->userid);
			$receiveRequest = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveRequest = $params->settings->pushNotification->receiveRequest;
			}

			if($receiveRequest)
			{
				$message = 'You have new guest list request from ' . $loginUser->name;

				$this->jsonarray['pushNotificationData']['id']         = $obj->id;
				$this->jsonarray['pushNotificationData']['to']         = $tblVenue->userid;
				$this->jsonarray['pushNotificationData']['message']    = $message;
				$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWREQUESTRECEIVEDFORGUESTLIST'); //'NewRequestReceivedForGuestList';
				$this->jsonarray['pushNotificationData']['configtype'] = '';
			}
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"removeUserFromBlackList",
	 * 		"taskData":{
	 * 			"userID":"number"
	 * 		}
	 * 	}
	 */
	function removeUserFromBlackList()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$userID = IJReq::getTaskData('userID',0,'int');
		$blockUser = JFactory::getUser($userID);

		if(!$blockUser->id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BLACKLIST_INVALID_DATA1'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$userGroup = $this->helper->getUserGroup($this->IJUserID);
		if($userGroup == 'Club')
		{
			$elementID = $this->helper->getUserVenueID($this->IJUserID);
			$elementType = "Venue";
		}
		elseif($userGroup == 'ServiceProvider')
		{
			$elementID = $this->helper->getUserCompanyID($this->IJUserID);
			$elementType = "Company";
		}

		if(!$elementID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BLACKLIST_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base delete statement.
		$query->delete()
			->from($db->quoteName('#__bcted_blacklist'))
			->where($db->quoteName('element_id') . ' = ' . $db->quote((int) $elementID))
			->where($db->quoteName('element_type') . ' = ' . $db->quote($elementType))
			->where($db->quoteName('user_id') . ' = ' . $db->quote((int) $blockUser->id));

		// Set the query and execute the delete.
		$db->setQuery($query);

		$db->execute();

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"addUserToBlackList",
	 * 		"taskData":{
	 * 			"userID":"number"
	 * 		}
	 * 	}
	 */
	function addUserToBlackList()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$userID = IJReq::getTaskData('userID',0,'int');
		$blockUser = JFactory::getUser($userID);

		if(!$blockUser->id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BLACKLIST_INVALID_DATA1'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$userGroup = $this->helper->getUserGroup($this->IJUserID);
		if($userGroup == 'Club')
		{
			$elementID = $this->helper->getUserVenueID($this->IJUserID);
			$elementType = "Venue";
		}
		elseif($userGroup == 'ServiceProvider')
		{
			$elementID = $this->helper->getUserCompanyID($this->IJUserID);
			$elementType = "Company";
		}

		if(!$elementID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BLACKLIST_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base delete statement.
		$query->delete()
			->from($db->quoteName('#__bcted_favourites'))
			->where($db->quoteName('user_id') . ' = ' . $db->quote((int) $userID))
			->where($db->quoteName('favourite_type') . ' = ' . $db->quote($elementType))
			->where($db->quoteName('favourited_id') . ' = ' . $db->quote((int) $elementID));

		// Set the query and execute the delete.
		$db->setQuery($query);

		$db->execute();

		$checkResult = $this->helper->checkBlackList($userID,$elementID,$elementType);

		if($checkResult)
		{
			IJReq::setResponseCode(707);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BLACKLIST_USER_ALREADY_IN_YOUR_BLACKLIST'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblBlacklist = JTable::getInstance('Blacklist', 'BctedTable');
		$tblBlacklist->load(0);

		$data = array();
		$data['element_id']   = $elementID;
		$data['element_type'] = $elementType;
		$data['user_id']      = $userID;
		$data['created']      = date('Y-m-d H:i:s');

		$tblBlacklist->bind($data);

		if(!$tblBlacklist->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BLACKLIST_USER_NOT_ADDED_IN_YOUR_BLACKLIST'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"changeGuestListRequestStatus",
	 * 		"taskData":{
	 * 			"requestID":"number",
	 * 			"action":"accept/reject",
	 * 		}
	 * 	}
	 */
	function changeGuestListRequestStatus()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$requestID = IJReq::getTaskData('requestID',0,'int');
		$action = IJReq::getTaskData('action','','string');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');

		$tblVenueguestlist = JTable::getInstance('Venueguestlist', 'BctedTable');
		$tblVenueguestlist->load($requestID);

		$elementID = $this->helper->getUserVenueID($this->IJUserID);
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($elementID);

		if(!$tblVenue->venue_id || !$tblVenueguestlist->venue_guest_list_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_ACCEPT_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblVenue->venue_id != $tblVenueguestlist->venue_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_ACCEPT_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$message = '';
		$messageType = '';

		if($action == 'Accept')
		{
			$tblVenueguestlist->status = 5;
			$tblVenueguestlist->user_status = 5;

			$message = $tblVenue->venue_name . ' has added you to their Guest List for '. date('d-m-Y',strtotime($tblVenueguestlist->request_date));
			$messageType = 'VenueGuestListRequestAccepted';
		}
		else if($action == 'Reject')
		{
			$tblVenueguestlist->status = 9;
			$tblVenueguestlist->user_status = 9;



			//$message = $tblVenue->venue_name . ' has rejected your Guest List for '. date('d-m-Y',strtotime($tblVenueguestlist->request_date));
			$guestText = $tblVenueguestlist->number_of_guest .' ('.$tblVenueguestlist->male_count.' M/'.$tblVenueguestlist->female_count. ' F)';
			$message = "Your guestlist for ".$guestText." on Date ". date('d-m-Y',strtotime($tblVenueguestlist->request_date)) ." has been rejected";
			$messageType = 'VenueGuestListRequestRejected';
		}
		else
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_ACCEPT_INVALID_ACTION_TYPE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if(!$tblVenueguestlist->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_ACCEPT_ERROR'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		$extraParam = array();
		$extraParam['guestListID'] = $tblVenueguestlist->venue_guest_list_id;

		if($messageType == 'VenueGuestListRequestAccepted')
		{
			$messageType = JText::_('PUSHNOTIFICATION_TYPE_VENUEGUESTLISTREQUESTACCEPTED');
			//$msgMessage = "Your guest list is accepted for Number of guest ".$tblVenueguestlist->number_of_guest . "(". $tblVenueguestlist->male_count." M/".$tblVenueguestlist->female_count." F) on Date " . date('d-m-Y',strtotime($tblVenueguestlist->request_date));
			$msgMessage = JText::sprintf('PUSHNOTIFICATION_TYPE_VENUEGUESTLISTREQUESTACCEPTED_MESSAGE',$tblVenueguestlist->number_of_guest,$tblVenueguestlist->male_count,$tblVenueguestlist->female_count,date('d-m-Y',strtotime($tblVenueguestlist->request_date)));
		}
		else if($messageType == 'VenueGuestListRequestRejected')
		{
			$messageType = JText::_('PUSHNOTIFICATION_TYPE_VENUEGUESTLISTREQUESTREJECTED');
			//$msgMessage = "Your guest list is rejected for Number of guest ".$tblVenueguestlist->number_of_guest . "(". $tblVenueguestlist->male_count." M/".$tblVenueguestlist->female_count." F) on Date " . date('d-m-Y',strtotime($tblVenueguestlist->request_date));
			$msgMessage = JText::sprintf('PUSHNOTIFICATION_TYPE_VENUEGUESTLISTREQUESTREJECTED_MESSAGE',$tblVenueguestlist->number_of_guest,$tblVenueguestlist->male_count,$tblVenueguestlist->female_count,date('d-m-Y',strtotime($tblVenueguestlist->request_date)));
		}
		$this->sendMessageForInviteUserByOwner($tblVenueguestlist->venue_id,0,0,0,$tblVenueguestlist->user_id,$msgMessage,$extraParam = array(),$messageType);

		$connectionID = $this->helper->getMessageConnectionOnly($this->IJUserID,$tblVenueguestlist->user_id);

		$this->jsonarray['pushNotificationData']['id']         = $tblVenue->venue_name.";".$connectionID.";Venue";
		$this->jsonarray['pushNotificationData']['to']         = $tblVenueguestlist->user_id;
		$this->jsonarray['pushNotificationData']['message']    = $message;
		$this->jsonarray['pushNotificationData']['type']       = $messageType;
		$this->jsonarray['pushNotificationData']['configtype'] = '';

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	  	{
	  		"extName":"bcted",
	  		"extView":"venue",
	  		"extTask":"updateGuestListRequestPerson",
	  		"taskData":{
	  			"requestID":"number",
	  			"guestCount":"number",
	  			"maleGuest":"number",
	  			"femaleGuest":"number"
	  		}
	  	}
	 */
	function updateGuestListRequestPerson()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$requestID    = IJReq::getTaskData('requestID',0,'int');
		$guestCount   = IJReq::getTaskData('guestCount',0,'int');
		//$maleGuest    = IJReq::getTaskData('maleGuest',0,'int');
		//$femaleGuest = IJReq::getTaskData('femaleGuest',0,'int');
		// $action = IJReq::getTaskData('action','','string');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');

		$tblVenueguestlist = JTable::getInstance('Venueguestlist', 'BctedTable');
		$tblVenueguestlist->load($requestID);

		$elementID = $this->helper->getUserVenueID($this->IJUserID);
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($elementID);

		if(!$tblVenue->venue_id || !$tblVenueguestlist->venue_guest_list_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_ACCEPT_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*if(!$maleGuest && !$femaleGuest)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_ACCEPT_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}*/

		if($tblVenue->venue_id != $tblVenueguestlist->venue_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_ACCEPT_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblVenueguestlist->number_of_guest < $guestCount)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_ACCEPT_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenueguestlist->remaining_total  = $tblVenueguestlist->remaining_total - $guestCount;

		/*if(	$tblVenueguestlist->remaining_total>=$guestCount && $tblVenueguestlist->remaining_male>=$maleGuest && $tblVenueguestlist->remaining_female>=$femaleGuest )
		{
			$tblVenueguestlist->remaining_total  = $tblVenueguestlist->remaining_total - $guestCount;
			$tblVenueguestlist->remaining_male   = $tblVenueguestlist->remaining_male - $maleGuest;
			$tblVenueguestlist->remaining_female = $tblVenueguestlist->remaining_female - $femaleGuest;
		}
		else
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_ACCEPT_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}*/


		if(!$tblVenueguestlist->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_ACCEPT_ERROR'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;



		/*$this->jsonarray['pushNotificationData']['id']         = $tblVenueguestlist->venue_guest_list_id;
		$this->jsonarray['pushNotificationData']['to']         = $tblVenueguestlist->user_id;
		$this->jsonarray['pushNotificationData']['message']    = $message;
		$this->jsonarray['pushNotificationData']['type']       = $messageType;
		$this->jsonarray['pushNotificationData']['configtype'] = '';*/

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"venue",
	 * 		"extTask":"getVenueGuestListRequest",
	 * 		"taskData":{
	 * 			"requestID":"number"
	 * 		}
	 * 	}
	 */
	function getVenueGuestListRequest()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$requestID = IJReq::getTaskData('requestID',0,'int');

		$venueID = $this->helper->getUserVenueID($this->IJUserID);

		/*if(!$requestID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_REQUEST_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}*/

		$liveUser = $this->helper->getLiveUsers($this->IJUserID);


		// Initialiase variables.
		$db    = JFactory::getDbo();
		$queryToday = $db->getQuery(true);

		$today = date('Y-m-d');

		// Create the base select statement.
		$queryToday->select('request_date,count(request_date) as guestListCount,sum(number_of_guest) as totalGuest')
			->from($db->quoteName('#__bcted_venue_guest_list_request'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('status') . ' = ' . $db->quote(5))
			->where($db->quoteName('request_date') . ' = ' . $db->quote($today))
			->group($db->quoteName('request_date'))
			->order($db->quoteName('time_stamp') . ' ASC');

		if(count($liveUser)!=0)
		{
			$liveUserStr = implode(",", $liveUser);
			$queryToday->where($db->quoteName('user_id') . ' IN (' . $liveUserStr . ')');
		}

		//echo $queryToday->dump();
		/*exit;*/

		// Set the queryToday and load the result.
		$db->setQuery($queryToday);

		$todaysGuestLists = $db->loadObjectList();
		/*echo "<pre>";
		print_r($todaysGuestLists);
		echo "</pre>";
		exit;*/

		$resultTodaysList = array();

		foreach ($todaysGuestLists as $key => $todaysGuest)
		{
			$tempData = array();
			$tempData['date'] = date('d-m-Y',strtotime($todaysGuest->request_date));
			//$tempData['listCount'] = $todaysGuest->guestListCount;
			$tempData['listCount'] = $todaysGuest->totalGuest;

			$resultTodaysList[] = $tempData;
		}

		$queryFuture = $db->getQuery(true);

		$today = date('Y-m-d');

		// Create the base select statement.
		$queryFuture->select('request_date,count(request_date) as guestListCount,sum(number_of_guest) as totalGuest')
			->from($db->quoteName('#__bcted_venue_guest_list_request'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('status') . ' = ' . $db->quote(5))
			->where($db->quoteName('request_date') . ' > ' . $db->quote($today))
			->group($db->quoteName('request_date'))
			->order($db->quoteName('request_date') . ' DESC');

		if(count($liveUser)!=0)
		{
			$liveUserStr = implode(",", $liveUser);
			$queryFuture->where($db->quoteName('user_id') . ' IN (' . $liveUserStr . ')');
		}

		/*echo $queryToday->dump();
		exit;*/

		// Set the queryToday and load the result.
		$db->setQuery($queryFuture);

		$todaysGuestLists = $db->loadObjectList();

		$resultFuturesList = array();

		foreach ($todaysGuestLists as $key => $todaysGuest)
		{
			$tempData = array();
			$tempData['date'] = date('d-m-Y',strtotime($todaysGuest->request_date));
			//$tempData['listCount'] = $todaysGuest->guestListCount;
			$tempData['listCount'] = $todaysGuest->totalGuest;

			$resultFuturesList[] = $tempData;
		}

		$queryPast = $db->getQuery(true);

		$today = date('Y-m-d');

		// Create the base select statement.
		$queryPast->select('request_date,count(request_date) as guestListCount,sum(number_of_guest) as totalGuest')
			->from($db->quoteName('#__bcted_venue_guest_list_request'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('request_date') . ' < ' . $db->quote($today))
			->group($db->quoteName('request_date'))
			->order($db->quoteName('request_date') . ' DESC');

		if(count($liveUser)!=0)
		{
			$liveUserStr = implode(",", $liveUser);
			$queryPast->where($db->quoteName('user_id') . ' IN (' . $liveUserStr . ')');
		}

		/*echo $queryToday->dump();
		exit;*/

		// Set the queryToday and load the result.
		$db->setQuery($queryPast);

		$todaysGuestLists = $db->loadObjectList();

		$resultPastsList = array();

		foreach ($todaysGuestLists as $key => $todaysGuest)
		{
			$tempData = array();
			$tempData['date'] = date('d-m-Y',strtotime($todaysGuest->request_date));
			//$tempData['listCount'] = $todaysGuest->guestListCount;
			$tempData['listCount'] = $todaysGuest->totalGuest;

			$resultPastsList[] = $tempData;
		}



		/*$requestDataForToday  = $this->helper->getVenueGuestListRequest($requestID,$venueID,0);
		$requestDataForPast   = $this->helper->getVenueGuestListRequest($requestID,$venueID,-1);
		$requestDataForFuture = $this->helper->getVenueGuestListRequest($requestID,$venueID,1);

		if(count($requestDataForToday) == 0 && count($requestDataForPast) == 0 && count($requestDataForFuture) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_REQUEST_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}*/

		if(count($resultPastsList) == 0 && count($resultFuturesList) == 0 && count($resultTodaysList) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_REQUEST_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['requestToday']  = $resultTodaysList;
		$this->jsonarray['requestPast']   = $resultPastsList;
		$this->jsonarray['requestFuture'] = $resultFuturesList;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	  	{
	  		"extName":"bcted",
	  		"extView":"venue",
	  		"extTask":"guestListRequestByDate",
	  		"taskData":{
	  			"requestDate":"2015-02-28"
	  		}
	  	}
	 */
	function guestListRequestByDate()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$requestDate = IJReq::getTaskData('requestDate','','string');

		$venueID = $this->helper->getUserVenueID($this->IJUserID);

		if(empty($requestDate))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_REQUEST_INVALID_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}





		$requests  = $this->helper->getVenueGuestListRequestByDate($venueID,$requestDate);
		/*$requestDataForPast   = $this->helper->getVenueGuestListRequest($requestID,$venueID,-1);
		$requestDataForFuture = $this->helper->getVenueGuestListRequest($requestID,$venueID,1);

		if(count($requestDataForToday) == 0 && count($requestDataForPast) == 0 && count($requestDataForFuture) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_REQUEST_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}*/

		if(count($requests) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_GUEST_LIST_REQUEST_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['request']  = $requests;

		return $this->jsonarray;
	}

	function uplaodFile($file,$elementType,$userID)
	{
		//$file = JRequest::getVar('image','','FILES','array');
		$uploadedImage = "";
		$uploadLimit	= 8;
		$uploadLimit	= ( $uploadLimit * 1024 * 1024 );

		if(is_array($file) && isset($file['size']) && $file['size']>0)
		{
			jimport('joomla.filesystem.file');
			jimport('joomla.utilities.utility');

			if( filesize( $file['tmp_name'] ) > $uploadLimit && $uploadLimit != 0 )
			{
				/*IJReq::setResponseCode(416);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_IMAGE_FILE_SIZE_EXCEEDED'));
				return false;*/
				return '';
			} // End of if( filesize( $file['tmp_name'] ) > $uploadLimit && $uploadLimit != 0 )

			//$imageMaxWidth	= 160;
			$filename = JApplication::getHash( $file['tmp_name'] . time() );
			$hashFileName	= JString::substr( $filename , 0 , 24 );
			$info['extension'] = pathinfo($file['name'],PATHINFO_EXTENSION);
			$info['extension'] = '.'.$info['extension'];

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/"))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/");
			}

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/".$elementType."/"))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/".$elementType."/");
			}

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/".$elementType."/". $userID . '/'))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/".$elementType."/". $userID . '/');
			}

			$storage      = JPATH_ROOT . '/images/bcted/'.$elementType.'/'. $userID . '/';
			$storageImage = $storage . '/' . $hashFileName .  $info['extension'] ;
			$uploadedImage   = 'images/bcted/'.$elementType.'/' .$userID .'/'. $hashFileName . $info['extension'] ;

			if(!JFile::upload($file['tmp_name'], $storageImage))
		    {
				//$venueImage = "";
				return '';
		    }

		    return $uploadedImage;

		} // End of if(is_array($file) && $file['size']>0)

		return '';
	}


}