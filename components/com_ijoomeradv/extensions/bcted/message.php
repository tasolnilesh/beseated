<?php
/**
 * @package     iJoomerAdv.Site
 * @subpackage  com_ijoomeradv
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.helper' );
class message
{

	private $db;
	private $IJUserID;
	private $helper;
	private $jsonarray;

	function __construct()
	{
		$this->db                = JFactory::getDBO();
		$this->mainframe         =  JFactory::getApplication ();
		$this->IJUserID          = $this->mainframe->getUserState ( 'com_ijoomeradv.IJUserID', 0 ); //get login user id
		$this->my                = JFactory::getUser ( $this->IJUserID ); // set the login user object
		$this->helper            = new bctedAppHelper;
		$this->jsonarray         = array();
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"message",
	 * 		"extTask":"sendMessage",
	 * 		"taskData":{
	 *   		"venueID":"number",
	 *   		"companyID":"number",
	 *   		"serviceID":"number",
	 *   		"tableID":"number",
	 *   		"message":"number",
	 *   		"userID":"number"
	 * 		}
	 * 	}
	 */
	function sendMessage()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$venueID     = IJReq::getTaskData('venueID',0,'int');
		$companyID   = IJReq::getTaskData('companyID',0,'int');
		$serviceID   = IJReq::getTaskData('serviceID',0,'int');
		$tableID     = IJReq::getTaskData('tableID',0,'int');
		$message     = IJReq::getTaskData('message','','string');
		$userID      = IJReq::getTaskData('userID',0,'int');
		$messageType = IJReq::getTaskData('messageType','msg','string');



		if((!$venueID && !$companyID) || empty($message))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_MESSAGE_DATA_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblMessage = JTable::getInstance('Message', 'BctedTable');
		$tblMessage->load(0);

		$data = array();

		$data['venue_id']   = $venueID;
		$data['company_id'] = $companyID;
		$data['table_id']   = $serviceID;
		$data['service_id'] = $tableID;
		$data['userid']     = $userID;
		$data['to_userid']  = $userID;
		$data['message']    = $message;
		$data['message_type']    = $messageType;
		$data['created']    = date('Y-m-d H:i:s');
		$data['time_stamp'] = time();

		if($companyID)
		{
			$tblCompany = JTable::getInstance('Company', 'BctedTable');
			$tblCompany->load($companyID);

			$elementName = $tblCompany->company_name;
			$elementID = $tblCompany->company_id;
			$elementType = "Company";

			$data['from_userid'] = $tblCompany->userid;

		}
		else if($venueID)
		{
			$tblVenue = JTable::getInstance('Venue', 'BctedTable');
			$tblVenue->load($venueID);

			$elementName = $tblVenue->venue_name;
			$elementID = $tblVenue->venue_id;
			$elementType = "Venue";

			$data['from_userid'] = $tblVenue->userid;
		}

		if($data['from_userid'] == $data['to_userid'])
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_MESSAGE_DATA_NOT_VALID_SELFT_MESSAGE_NOT_ALLOWED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$connectionID = $this->helper->getMessageConnection($data['from_userid'],$data['to_userid']);

		if(!$connectionID)
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_MESSAGE_NOT_SEND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$data['connection_id'] = $connectionID;

		$tblMessage->bind($data);

		if(!$tblMessage->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_MESSAGE_NOT_SEND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['connectionID'] = $tblMessage->connection_id;

		//$elementName = "";



		/*$pushcontentdata=array();

		$pushcontentdata['posts']['msg_id'] = $tblPost->post_id;
		$pushcontentdata['posts']['userID'] = $tblPost->userid;
		$pushcontentdata['posts']['image1'] = ($tblPost->image1) ? JURI::root().$tblPost->image1 : '';
		$pushcontentdata['posts']['image2'] = ($tblPost->image2) ? JURI::root().$tblPost->image2 : '';
		$pushcontentdata['posts']['video']      = ($tblPost->video) ? JURI::root().$tblPost->video : '';
		$pushcontentdata['posts']['videoImage'] = ($tblPost->video_image) ? JURI::root().$tblPost->video_image : '';
		$pushcontentdata['posts']['hasVideo']   = $tblPost->has_video;

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$message = $user->name ." says: " . $caption;

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{
			$jsonarray['pushNotificationData']['id'] 		  = $tblPost->post_id; //$obj->id;
			$jsonarray['pushNotificationData']['to'] 		  = $newUserToPushStr;
			$jsonarray['pushNotificationData']['hasVideo'] 	  = $isVideo;
			$jsonarray['pushNotificationData']['message'] 	  = $message;
			$jsonarray['pushNotificationData']['type'] 		  = 'NewPostReceived';
			$jsonarray['pushNotificationData']['configtype']  = '';
		} */

		//receiveMessage

		$userProfile    = $this->helper->getUserProfile($userID);
		$receiveMessage = 0;

		if($userProfile)
		{
			$params = json_decode($userProfile->params);
			$receiveMessage = $params->settings->pushNotification->receiveMessage;
		}

		if($receiveMessage)
		{
			//$message                                               = $elementName . ' sent you a new message';
			$message = JText::sprintf('PUSHNOTIFICATION_TYPE_NEW_MESSAGE_FROM_COMPANY_MESSAGE',$elementName);
			$this->jsonarray['pushNotificationData']['id']         = $elementName.';'.$connectionID.';'.$elementType; //$obj->id;
			$this->jsonarray['pushNotificationData']['to']         = $userID;
			$this->jsonarray['pushNotificationData']['message']    = $message;

			if($companyID)
			{
				$this->jsonarray['pushNotificationData']['id']         = $elementName.';'.$connectionID.';'.$elementType; //$obj->id;
				$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWMESSAGEFROMCOMPANY'); //'NewMessageFromCompany';
			}
			else if($venueID)
			{
				$this->jsonarray['pushNotificationData']['id']         = $elementName.';'.$connectionID.';'.$elementType; //$obj->id;
				$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWMESSAGEFROMVENUE'); //'NewMessageFromVenue';
			}

			$this->jsonarray['pushNotificationData']['configtype'] = '';
		}

		/*echo "<pre>";
		print_r($this->jsonarray);
		echo "</pre>";
		exit;*/

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"message",
	 * 		"extTask":"getMessage",
	 * 		"taskData":{
	 *   		"showThread":"1"
	 * 		}
	 * 	}
	 */
	function getMessage()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		/*$app = JFactory::getApplication();
		echo "<pre>";
		print_r($app);
		echo "</pre>";
		exit;*/

		$showThread    = IJReq::getTaskData('showThread',0,'int');
		$entrCompanyID = IJReq::getTaskData('companyID',0,'int');
		$entrVenueID   = IJReq::getTaskData('venueID',0,'int');
		$entrUserID    = IJReq::getTaskData('userID',0,'int');

		$connectionID    = IJReq::getTaskData('connectionID',0,'int');

		$userGroup = $this->helper->getUserGroup($this->IJUserID);

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('msg.*')
			->from($db->quoteName('#__bcted_message','msg'))
			->order($db->quoteName('msg.time_stamp') . ' ASC');

		// Set the query and load the result. ->where($db->quoteName('state') . ' = ' . $db->quote('1'))

		if($userGroup == 'Club')
		{
			$venueID = $this->helper->getUserVenueID($this->IJUserID);

			//$query->where($db->quoteName('msg.venue_id') . ' = ' . $db->quote($venueID));
			//$query->where($db->quoteName('msg.from_userid') . ' = ' . $db->quote($this->IJUserID));
			//$query->where($db->quoteName('msg.deleted_by_from') . ' = ' . $db->quote(0));
			$query->where($db->quoteName('msg.connection_id') . ' = ' . $db->quote($connectionID));


			if($entrUserID)
			{
				//$query->where($db->quoteName('msg.userid') . ' = ' . $db->quote($entrUserID));
			}

			$query->select('up.last_name,up.phoneno,up.avatar,up.about')
				->join('LEFT','#__bcted_user_profile AS up ON up.userid=msg.userid');

			if($showThread)
			{
				$query = "SELECT max(id) AS id FROM nao4c_bcted_message WHERE venue_id = " . $venueID . " AND venue_id<>0 GROUP BY connection_id";
				$db->setQuery($query);

				//echo "Group By : ".$query . " <br />";
				$messagesIDsV = $db->loadColumn();

				$query = "SELECT `msg`.*,`up`.`last_name`,`up`.`phoneno`,`up`.`avatar`,`up`.`about`
					FROM `#__bcted_message` AS `msg`
					LEFT JOIN `#__bcted_user_profile` AS `up` ON `up`.`userid`=`msg`.`userid`
					WHERE `msg`.`venue_id` = ".$venueID ; //AND `msg`.`from_userid`=".$this->IJUserID;

				if($entrUserID)
				{
					$query .= " AND `msg`.`userid` = ".$entrUserID;
				}

				if(count($messagesIDsV) != 0)
				{
					$query .= " AND `msg`.id IN (". implode(",", $messagesIDsV) . ")";
				}

				/*$query .= "	GROUP  BY `msg`.`venue_id`,`msg`.`company_id` HAVING MAX(`msg`.`time_stamp`)
					ORDER BY `msg`.`time_stamp` DESC";*/
				$query .= "	ORDER BY `msg`.`time_stamp` DESC";

				/*echo $query;
				exit;*/
			}



			/*echo "Venue : " . $query->dump();
			exit;*/

			/*$query = "SELECT msg.*,up.last_name,up.phoneno,up.avatar,up.about
				FROM `nao4c_bcted_message` AS `msg`
				LEFT JOIN nao4c_bcted_user_profile AS up ON up.userid=msg.userid
				WHERE `msg`.`venue_id` = '1'
				GROUP  BY `msg`.`venue_id`,`msg`.`company_id` HAVING MAX(`msg`.`time_stamp`)
				ORDER BY `msg`.`time_stamp` DESC";
			*/



		}
		else if($userGroup == 'ServiceProvider')
		{
			$companyID = $this->helper->getUserCompanyID($this->IJUserID);

			//$query->where($db->quoteName('msg.company_id') . ' = ' . $db->quote($companyID));

			//$query->where($db->quoteName('msg.from_userid') . ' = ' . $db->quote($this->IJUserID));
			//$query->where($db->quoteName('msg.deleted_by_from') . ' = ' . $db->quote(0));

			if($entrUserID)
			{
				//$query->where($db->quoteName('msg.userid') . ' = ' . $db->quote($entrUserID));
			}

			$query->where($db->quoteName('msg.connection_id') . ' = ' . $db->quote($connectionID));

			$query->select('up.last_name,up.phoneno,up.avatar,up.about')
				->join('LEFT','#__bcted_user_profile AS up ON up.userid=msg.userid');

			/*echo $query->dump();
			exit;
			SELECT msg.*,up.last_name,up.phoneno,up.avatar,up.about
				FROM `nao4c_bcted_message` AS `msg`
				LEFT JOIN nao4c_bcted_user_profile AS up ON up.userid=msg.userid
				WHERE `msg`.`company_id` = '1'
				ORDER BY `msg`.`time_stamp` DESC
			*/

			if($showThread)
			{
				$query = "SELECT max(id) AS id FROM nao4c_bcted_message WHERE company_id = " . $companyID . " AND company_id<>0 GROUP BY connection_id";
				$db->setQuery($query);
				$messagesIDsC = $db->loadColumn();

				/*echo "<pre>";
				print_r($messagesIDsC);
				echo "</pre>";;*/

				$query = "SELECT msg.*,up.last_name,up.phoneno,up.avatar,up.about
					FROM `#__bcted_message` AS `msg`
					LEFT JOIN `#__bcted_user_profile` AS `up` ON `up`.`userid`=`msg`.`userid`
					WHERE `msg`.`company_id` = ".$companyID; //AND `msg`.`from_userid`=".$this->IJUserID;

				if($entrUserID)
				{
					$query .= " AND `msg`.`userid` = ".$entrUserID;
				}

				if(count($messagesIDsC) != 0)
				{
					$query .= " AND `msg`.`id` IN (".implode(",", $messagesIDsC).")";
				}

				/*$query .= " GROUP  BY `msg`.`company_id`,`msg`.`company_id` HAVING MAX(`msg`.`time_stamp`)
					ORDER BY `msg`.`time_stamp` DESC";*/
				$query .= " ORDER BY `msg`.`time_stamp` DESC";
			}


		}
		else if($userGroup == 'Registered')
		{
			$userID = $this->IJUserID;
			//$query->where($db->quoteName('msg.userid') . ' = ' . $db->quote($userID));

			if($entrCompanyID)
			{
				//$query->where($db->quoteName('msg.company_id') . ' = ' . $db->quote($entrCompanyID));
			}
			else if($entrVenueID)
			{
				//$query->where($db->quoteName('msg.venue_id') . ' = ' . $db->quote($entrVenueID));
			}

			$query->where($db->quoteName('msg.connection_id') . ' = ' . $db->quote($connectionID));

			//$query->where($db->quoteName('msg.to_userid') . ' = ' . $db->quote($userID));
			//$query->where($db->quoteName('msg.deleted_by_to') . ' = ' . $db->quote(0));

			$query->select('v.venue_name')
				->join('LEFT','#__bcted_venue AS v ON v.venue_id=msg.venue_id');

			$query->select('c.company_name')
				->join('LEFT','#__bcted_company AS c ON c.company_id=msg.company_id');


			/*echo $query->dump();
			exit;*/

			if($showThread)
			{
				$query = "SELECT max(id) AS id FROM nao4c_bcted_message WHERE (from_userid = " . $userID . " OR to_userid = ". $userID.") AND venue_id<>0 GROUP by connection_id";
				$db->setQuery($query);
				$messagesIDsV = $db->loadColumn();

				/*echo $query."<pre>";
				print_r($messagesIDsV);
				echo "</pre>";
				exit;*/


				$query = "SELECT max(id) AS id FROM nao4c_bcted_message WHERE (from_userid = " . $userID . " OR to_userid = ". $userID.") AND company_id<>0 GROUP by connection_id";
				$db->setQuery($query);
				$messagesIDsC = $db->loadColumn();

				/*echo "<pre>";
				print_r($messagesIDsC);
				echo "</pre>";
				exit;*/

				$query = "SELECT max(id) AS id FROM nao4c_bcted_message WHERE (from_userid = " . $userID . " OR to_userid = ". $userID.") AND message_type='system' GROUP by connection_id";
				$db->setQuery($query);
				$messagesIDsS = $db->loadColumn();

				/*echo "<pre>";
				print_r($messagesIDsS);
				echo "</pre>";
				exit;*/

				$msgIDs = array_merge($messagesIDsC,$messagesIDsV,$messagesIDsS);

				$msgIDs = array_unique($msgIDs);

				if(count($msgIDs)==0)
				{
					IJReq::setResponseCode(204);
					IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_MESSAGE_NOT_FOUND'));
					IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

					return false;
				}

				//SELECT max(id) AS id FROM nao4c_bcted_message WHERE company_id<>0 GROUP by company_id

				$query = "SELECT `msg`.*,`v`.`venue_name`,`c`.`company_name`
					FROM `#__bcted_message` AS `msg`
					LEFT JOIN `#__bcted_venue` AS `v` ON `v`.`venue_id`=`msg`.`venue_id`
					LEFT JOIN `#__bcted_company` AS `c` ON `c`.`company_id`=`msg`.`company_id`";

				/*if($entrCompanyID)
				{
					$query .= " AND `msg`.`company_id` = ".$entrCompanyID;
				}
				else if($entrVenueID)
				{
					$query .= " AND `msg`.`venue_id` = ".$entrVenueID;
				}*/



				if(count($msgIDs))
				{
					$query .= " WHERE `msg`.`id` IN (".implode(",", $msgIDs).")";
				}

				$query .= " ORDER BY `msg`.`time_stamp` DESC";

				/*echo $query;
				exit;*/

				/*$query .= " GROUP  BY `msg`.`company_id`,`msg`.`venue_id` HAVING MAX(`msg`.`time_stamp`)
					ORDER BY MAX(`msg`.`time_stamp`) DESC";*/
			}

			/*echo $query;
			exit;*/

		}
		else
		{
			IJReq::setResponse(400);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		/*echo $query->dump();
		exit;*/

		$db->setQuery($query);

		$messages = $db->loadObjectList();


		$messageResult = array();

		/*echo "<pre>";
		print_r($messages);
		echo "</pre>";
		exit;*/

		foreach ($messages as $key => $message)
		{
			/*echo "<pre>";
			print_r($message);
			echo "</pre>";*/
			$tempData = array();
			$tempData['messageID'] = $message->id;
			$tempData['venueID']   = ($message->venue_id)?$message->venue_id:'0';
			$tempData['companyID'] = ($message->company_id)?$message->company_id:'0';
			$tempData['tableID']   = ($message->table_id)?$message->table_id:'0';
			$tempData['serviceID'] = ($message->service_id)?$message->service_id:'0';
			$tempData['myUserID'] = $this->IJUserID;
			$tempData['toUserID'] = $message->to_userid;
			$tempData['deletedByTo'] = $message->deleted_by_to;
			$tempData['fromUserID'] = $message->from_userid;
			$tempData['deletedByFrom'] = $message->deleted_by_from;

			if($showThread)
			{
				$tempData['connectionID'] = $message->connection_id;
			}

			$tempData['message']   = $message->message;
			$tempData['created']   = date('d-m-Y H:i',strtotime($message->created));
			$tempData['messageCategory']   = $message->message_type;
			$tempData['extraParm']   = ($message->extra_params)?json_decode($message->extra_params):'';


			//$tempData['userID']    = $message->userid;

			if($message->to_userid == $this->IJUserID && $message->deleted_by_to == 1)
			{
				continue;
			}
			else if($message->from_userid == $this->IJUserID && $message->deleted_by_from == 1)
			{
				continue;
			}


			if($message->to_userid == $this->IJUserID)
			{
				$tempData['userID']    = $message->from_userid;
			}
			else if($message->from_userid == $this->IJUserID)
			{
				$tempData['userID']    = $message->to_userid;
			}

			/*echo "<pre>";
			print_r($userGroup);
			echo "</pre>";
			exit;*/

			if($userGroup == 'Registered')
			{
				$flg = 0;

				if($message->message_type == "inviteToTable")
				{
					$tempData['messageType'] = 'invite';
					$flg = 1;

					if($message->to_userid == $this->IJUserID)
					{
						$msgUser = JFactory::getUser($message->from_userid);
					}
					else if($message->from_userid == $this->IJUserID)
					{
						$msgUser = JFactory::getUser($message->to_userid);
					}

					$tempData['venueName'] = $msgUser->name;
				}
				else if($message->venue_id)
				{
					$tempData['messageType'] = 'Club';
					$flg = 1;
					$tempData['venueName'] = $message->venue_name;
				}
				else
				{
					$tempData['messageType'] = '';
					$tempData['venueName'] = "";
				}

				if($message->company_id)
				{
					$tempData['messageType'] = 'ServiceProvider';
					$flg = 1;
					$tempData['companyName']    = $message->company_name;
				}
				else
				{
					$tempData['messageType'] = "";
					$tempData['companyName'] = "";
				}

				if($message->message_type == 'system')
				{
					$flg = 1;
					$tempData['messageType'] = "system";
				}

				if(!$flg)
				{
					continue;
				}


			}
			else if($userGroup == 'Club' || $userGroup == 'ServiceProvider')
			{


				if($message->to_userid == $this->IJUserID)
				{
					/*echo '1';
					exit;*/
					$msgUser = JFactory::getUser($message->from_userid);

					if($msgUser->id)
					{
						$tempData['username'] = $msgUser->name;
						$tempData['lastName'] = $message->last_name;
					}
					else
					{
						continue;
					}
				}
				else if($message->from_userid == $this->IJUserID)
				{

					$msgUser = JFactory::getUser($message->to_userid);
					if($msgUser->id)
					{
						$tempData['username'] = $msgUser->name;
						$tempData['lastName'] = $message->last_name;
					}
					else
					{
						continue;
					}
				}
			}

			/*echo "<pre>";
			print_r($tempData);
			echo "</pre>";
			exit;*/

			$messageResult[] = $tempData;
		}

		if(count($messageResult)==0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_MESSAGE_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code']     = 200;
		$this->jsonarray['connectionID'] = $connectionID;
		$this->jsonarray['total']    = count($messageResult);
		$this->jsonarray['messages'] = $messageResult;

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"message",
	 * 		"extTask":"deleteMessage",
	 * 		"taskData":{
	 * 			"deletedBy":"user/company/venue",
	 * 			"companyID":"number",
	 * 			"venueID":"number",
	 * 			"userID":"number",
	 * 		}
	 * 	}
	 */
	function deleteMessage()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$deletedBy     = IJReq::getTaskData('deletedBy','','string');
		$entrCompanyID = IJReq::getTaskData('companyID',0,'int');
		$entrVenueID   = IJReq::getTaskData('venueID',0,'int');
		$entrUserID    = IJReq::getTaskData('userID',0,'int');

		//mail("bcted.developer@gmail.com", "delete Messsage", $_REQUEST['reqObject']);

		if(!$entrCompanyID && !$entrVenueID && !$entrUserID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_MESSAGE_DATA_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query2 = $db->getQuery(true);

		if($deletedBy == 'user')
		{
			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_to') . ' = ' . $db->quote(1))
				->where($db->quoteName('to_userid') . ' = ' . $db->quote($this->IJUserID));

			if($entrVenueID)
			{
				$query->where($db->quoteName('venue_id') . ' = ' . $db->quote($entrVenueID));
			}
			else if($entrCompanyID)
			{
				$query->where($db->quoteName('company_id') . ' = ' . $db->quote($entrCompanyID));
			}

			$db->setQuery($query);
			$db->execute();

			$query2->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_from') . ' = ' . $db->quote(1))
				->where($db->quoteName('from_userid') . ' = ' . $db->quote($this->IJUserID));

			if($entrVenueID)
			{
				$query2->where($db->quoteName('venue_id') . ' = ' . $db->quote($entrVenueID));
			}
			else if($entrCompanyID)
			{
				$query2->where($db->quoteName('company_id') . ' = ' . $db->quote($entrCompanyID));
			}

			$db->setQuery($query2);
			$db->execute();

			/*echo $query2->dump();
			echo $query->dump();
			exit;*/
		}
		else if($deletedBy == 'venue')
		{
			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_from') . ' = ' . $db->quote(1));

			if($entrUserID)
			{
				//$userVenueID = $this->helper->getUserVenueID($this->IJUserID);

				$query->where($db->quoteName('to_userid') . '   = ' . $db->quote($entrUserID));
				$query->where($db->quoteName('from_userid') . ' = ' . $db->quote($this->IJUserID));
				//$query->where($db->quoteName('venue_id') . ' = ' . $db->quote($userVenueID));
			}

			/*echo $query->dump();
			exit;*/

			$db->setQuery($query);
			$db->execute();


			$query2->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_to') . ' = ' . $db->quote(1));

			if($entrUserID)
			{
				//$userVenueID = $this->helper->getUserVenueID($this->IJUserID);

				$query2->where($db->quoteName('to_userid') . ' = ' . $db->quote($this->IJUserID));
				$query2->where($db->quoteName('from_userid') . ' = ' . $db->quote($entrUserID));
			}

			$db->setQuery($query2);
			$db->execute();
		}
		else if($deletedBy == 'company')
		{
			// Create the base update statement.
			$query->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_from') . ' = ' . $db->quote(1));

			if($entrUserID)
			{
				//$userCompanytID = $this->helper->getUserCompanyID($this->IJUserID);
				$query->where($db->quoteName('to_userid') . ' = ' . $db->quote($entrUserID));
				$query->where($db->quoteName('from_userid') . ' = ' . $db->quote($this->IJUserID));
			}

			$db->setQuery($query);
			$db->execute();

			// Create the base update statement.
			$query2->update($db->quoteName('#__bcted_message'))
				->set($db->quoteName('deleted_by_to') . ' = ' . $db->quote(1));

			if($entrUserID)
			{
				//$userCompanytID = $this->helper->getUserCompanyID($this->IJUserID);
				$query2->where($db->quoteName('to_userid') . ' = ' . $db->quote($this->IJUserID));
				$query2->where($db->quoteName('from_userid') . ' = ' . $db->quote($entrUserID));
			}

			$db->setQuery($query2);
			$db->execute();
		}

		/*echo $query->dump();
		exit;*/
		// Set the query and execute the update.
		$db->setQuery($query);

		try
		{
			//$db->execute();
			//$db->setQuery($query2);
			//$db->execute();

			$this->jsonarray['code'] = 200;

			return $this->jsonarray;
		}
		catch (RuntimeException $e)
		{
			//throw new RuntimeException($e->getMessage(), $e->getCode());

			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_($e->getMessage() . ' - ' . $e->getCode()));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$this->jsonarray['code'] = 500;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"message",
	 * 		"extTask":"messageTest",
	 * 		"taskData":{
	 * 			"fromUser":"number",
	 * 			"toUser":"number"
	 * 		}
	 * 	}
	 */
	function messageTest()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$fromUser = IJReq::getTaskData('fromUser',0,'int');
		$toUser   = IJReq::getTaskData('toUser',0,'int');

		$data = $this->helper->getMessageConnection($fromUser,$toUser);
		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;*/

		$this->jsonarray['code'] = 200;
		return $this->jsonarray;

	}
}