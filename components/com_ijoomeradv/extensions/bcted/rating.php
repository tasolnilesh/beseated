<?php
/**
 * @package     iJoomerAdv.Site
 * @subpackage  com_ijoomeradv
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.helper' );
class rating
{

	private $db;
	private $IJUserID;
	private $helper;
	private $jsonarray;

	function __construct()
	{
		$this->db                = JFactory::getDBO();
		$this->mainframe         =  JFactory::getApplication ();
		$this->IJUserID          = $this->mainframe->getUserState ( 'com_ijoomeradv.IJUserID', 0 ); //get login user id
		$this->my                = JFactory::getUser ( $this->IJUserID ); // set the login user object
		$this->helper            = new bctedAppHelper;
		$this->jsonarray         = array();
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"rating",
	 * 		"extTask":"addRetingToVenue",
	 * 		"taskData":{
	 *   		"elementID":"number",
	 *   		"elementType":"string",
	 *   		"comment":"string",
	 *   		"rating":"number"
	 * 		}
	 * 	}
	 */
	function addReting()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$elementID   = IJReq::getTaskData('elementID',0,'int');
		$bookingID   = IJReq::getTaskData('bookingID',0,'int');
		$elementType = IJReq::getTaskData('elementType','','string');
		$comment     = IJReq::getTaskData('comment','','string');
		$rating      = IJReq::getTaskData('rating',0.0,'decimal');

		if(!$elementID || empty($comment) || !$rating || $rating > 5 || empty($elementType))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_RATING_DATA_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblRating = JTable::getInstance('Rating', 'BctedTable');
		$tblRating->load(0);

		$data = array();

		$data['rated_id']        = $elementID;
		$data['rating_type']     = $elementType;
		$data['user_id']         = $this->IJUserID;
		$data['rating']          = $rating;
		$data['rating_comment']  = $comment;
		$data['rating_datetime'] = date('Y-m-d h:i:s');
		$data['time_stamp']      = time();


		$tblRating->bind($data);

		if(!$tblRating->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_RATING_DATA_NOT_SAVE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}



		if($elementType == "venue")
		{
			$this->helper->updateAverageRatingOfVenue($elementID);

			$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');
			$tblVenuebooking->load($bookingID);
			if($tblVenuebooking->venue_booking_id)
			{
				$tblVenuebooking->is_rated = 1;
				$tblVenuebooking->store();
			}

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('*')
				->from($db->quoteName('#__bcted_venue_booking'))
				->where($db->quoteName('venue_id') . ' = ' . $db->quote($tblVenuebooking->venue_id))
				->where($db->quoteName('status') . ' = ' . $db->quote(5))
				->where($db->quoteName('user_id') . ' = ' . $db->quote($tblVenuebooking->user_id));

				//->where($db->quoteName('venue_table_id') . ' = ' . $db->quote($tblVenuebooking->venue_table_id))

			// Set the query and load the result.
			$db->setQuery($query);

			$bookingOfSameTables = $db->loadObjectList();

			foreach ($bookingOfSameTables as $key => $booking)
			{
				$currentTime = time();
				$timestamp = strtotime($booking->venue_booking_datetime);
				$BookedTimestampAfert24 = strtotime('+1 day', $timestamp);

				if($currentTime >= $BookedTimestampAfert24)
				{

					$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable',array());
					$tblVenuebooking->load($booking->venue_booking_id);
					if($tblVenuebooking->venue_booking_id)
					{
						$tblVenuebooking->is_rated = 1;
						$tblVenuebooking->store();
					}
				}
			}
		}
		else if($elementType == "company" || $elementType == "service")
		{
			$this->helper->updateAverageRatingOfCompany($elementID);

			$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');
			$tblServiceBooking->load($bookingID);
			if($tblServiceBooking->service_booking_id)
			{
				$tblServiceBooking->is_rated = 1;
				$tblServiceBooking->store();
			}

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('*')
				->from($db->quoteName('#__bcted_service_booking'))
				->where($db->quoteName('company_id') . ' = ' . $db->quote($tblServiceBooking->company_id))
				->where($db->quoteName('status') . ' = ' . $db->quote(5))
				->where($db->quoteName('user_id') . ' = ' . $db->quote($tblServiceBooking->user_id));

				//->where($db->quoteName('service_id') . ' = ' . $db->quote($tblServiceBooking->service_id))

			// Set the query and load the result.
			$db->setQuery($query);

			$bookingOfSameTables = $db->loadObjectList();
			foreach ($bookingOfSameTables as $key => $booking)
			{
				$currentTime = time();
				$timestamp = strtotime($booking->service_booking_datetime);
				$BookedTimestampAfert24 = strtotime('+1 day', $timestamp);

				if($currentTime >= $BookedTimestampAfert24)
				{
					$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable',array());
					$tblServiceBooking->load($bookingID);
					if($tblServiceBooking->service_booking_id)
					{
						$tblServiceBooking->is_rated = 1;
						$tblServiceBooking->store();
					}
				}
			}
		}
		else if($elementType == "service")
		{
			/*$this->helper->updateAverageRatingOfService($elementID);

			$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');
			$tblServiceBooking->load($bookingID);
			if($tblServiceBooking->service_booking_id)
			{
				$tblServiceBooking->is_rated = 1;
				$tblServiceBooking->store();
			}*/

			$this->helper->updateAverageRatingOfCompany($elementID);

			$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');
			$tblServiceBooking->load($bookingID);
			if($tblServiceBooking->service_booking_id)
			{
				$tblServiceBooking->is_rated = 1;
				$tblServiceBooking->store();
			}
		}


		$this->jsonarray['code'] = 200;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"rating",
	 * 		"extTask":"getRating",
	 * 		"taskData":{
	 *   		"elementID":"number",
	 *   		"elementType":"string"
	 * 		}
	 * 	}
	 */
	function getRating()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$liveUser = $this->helper->getLiveUsers();

		$elementID   = IJReq::getTaskData('elementID',0,'int');
		$elementType = IJReq::getTaskData('elementType','','string');
		$sorting     = IJReq::getTaskData('sorting','latest','string');

		if(!$elementID || empty($elementType))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_RATING_DATA_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('r.*')
			->from($db->quoteName('#__bcted_ratings','r'))
			->where($db->quoteName('r.rated_id') . ' = ' . $db->quote($elementID))
			->where($db->quoteName('r.rating_type') . ' = ' . $db->quote($elementType))
			->where($db->quoteName('r.user_id') . ' IN ('.implode(",", $liveUser).')');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=r.user_id');

		if($sorting == 'rating')
		{
			$query->order($db->quoteName('r.rating') . ' DESC');
		}
		else
		{
			$query->order($db->quoteName('r.time_stamp') . ' DESC');
		}

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$ratings = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			//throw new RuntimeException($e->getMessage(), $e->getCode());
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage("Error:".$e->getMessage());
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$resultRatings = array();

		foreach ($ratings as $key => $rating)
		{
			$tmpRating               = array();
			$tmpRating['ratingID']   = $rating->rating_id;
			$tmpRating['elementID']  = $rating->rated_id;
			$tmpRating['userID']     = $rating->user_id;
			$tmpRating['userName']   = $rating->name;
			$tmpRating['rating']     = $rating->rating;
			$tmpRating['comment']    = $rating->rating_comment;
			$tmpRating['ratingDate'] = date('d-m-Y H:i:s',strtotime($rating->rating_datetime));
			$tmpRating['timeStamp']  = $rating->time_stamp;

			$resultRatings[] = $tmpRating;
		}

		if(count($resultRatings) == 0)
		{
			$this->jsonarray['code'] = 204;

			return $this->jsonarray;
		}

		$this->jsonarray['code']    = 200;
		$this->jsonarray['total']   = count($resultRatings);
		$this->jsonarray['ratings'] = $resultRatings;

		return $this->jsonarray;
	}
}