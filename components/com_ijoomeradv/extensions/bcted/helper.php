<?php
/**
 * @package     iJoomerAdv.Site
 * @subpackage  com_ijoomeradv
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
jimport('joomla.application.component.helper');

class bctedAppHelper
{
	private $date_now;

	private $IJUserID;

	private $mainframe;

	private $db;

	private $my;

	private $config;

	private $execFunction = null;

	private $ffmpeg;

	private $videoSize;

	private $defaultImage;

	private $FavouriteVenueIDs;

	private $FavouriteServiceIDs;


	function __construct()
	{
		$this->date_now  = JFactory::getDate();
		$this->mainframe = JFactory::getApplication();
		$this->db        = JFactory::getDbo();
		$this->IJUserID  = $this->mainframe->getUserState('com_ijoomeradv.IJUserID', 0);
		$this->my        = JFactory::getUser($this->IJUserID);
		$this->config    = JFactory::getConfig();
		$this->execFunction = null;
		$this->ffmpeg = $this->ffmpegPath();
		$this->videoSize = "480x360";
		$this->defaultImage = JUri::base().'images/bcted/default/banner.png';
		$this->FavouriteVenueIDs = $this->getUserFavouriteVenuesIDs();
		$this->FavouriteServiceIDs = $this->getUserFavouriteServiceIDs();
	}

	public function currencyFormat($currencySign,$amount,$allowDeciaml = 0)
	{
		$amount = number_format($amount,$allowDeciaml);

		return $currencySign.''.$amount;
	}

	public function convertCurrencyGoogle($amount = 1, $from, $to)
	{
		$url  = "https://www.google.com/finance/converter?a=$amount&from=$from&to=$to";
		$data = file_get_contents($url);
		preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
		$converted = preg_replace("/[^0-9.]/", "", $converted[1]);

		return round($converted, 4);
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getUserFavouriteVenuesIDs($userID)
	{
		if(!$userID)
		{
			$loginUser = JFactory::getUser();
		}
		else
		{
			$loginUser = JFactory::getUser($userID);
		}

		//$loginUser = JFactory::getUser();

		if(!$loginUser->id)
		{
			return array();
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('favourited_id')
			->from($db->quoteName('#__bcted_favourites'))
			->where($db->quoteName('user_id') . ' = ' . $db->quote($loginUser->id))
			->where($db->quoteName('favourite_type') . ' = ' . $db->quote('Venue'))
			->order($db->quoteName('time_stamp') . ' DESC');

		$db->setQuery($query);

		return $db->loadColumn();
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getUserFavouriteServiceIDs($userID = 0)
	{
		if(!$userID)
		{
			$loginUser = JFactory::getUser();
		}
		else
		{
			$loginUser = JFactory::getUser($userID);
		}


		/*echo "<pre>";
		print_r($loginUser);
		echo "</pre>";
		exit;*/

		if(!$loginUser->id)
		{
			return array();
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('favourited_id')
			->from($db->quoteName('#__bcted_favourites'))
			->where($db->quoteName('user_id') . ' = ' . $db->quote($loginUser->id))
			->where($db->quoteName('favourite_type') . ' = ' . $db->quote('Service'))
			->order($db->quoteName('time_stamp') . ' DESC');

		$db->setQuery($query);
		/*echo "<pre>";
		print_r($loginUser);
		echo "</pre>";
		echo "<pre>";
		print_r($db->loadColumn());
		echo "</pre>";
		exit;*/

		return $db->loadColumn();
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getUserVenueID($userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('venue_id')
			->from($db->quoteName('#__bcted_venue'))
			->where($db->quoteName('userid') . ' = ' . $db->quote($userID));


		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$venue = $db->loadResult();

			if($venue)
				return $venue;
			else
				return 0;

		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getUserCompanyID($userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('company_id')
			->from($db->quoteName('#__bcted_company'))
			->where($db->quoteName('userid') . ' = ' . $db->quote($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$company = $db->loadResult();

			if($company)
				return $company;
			else
				return 0;

		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getUserProfile($userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('JU.id,JU.name,JU.username,JU.email')
			->from($db->quoteName('#__users','JU'))
			->where($db->quoteName('JU.id') . ' = ' . $db->quote($userID));
		$query->select('BU.*')
			->join('LEFT','#__bcted_user_profile AS BU ON BU.userid=JU.id');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$bctedProfile = $db->loadObject();

			if($bctedProfile)
				return $bctedProfile;
			else
				return 0;

		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		return 0;
	}

	public function getDeletedUserProfile($userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_user_profile'))
			->where($db->quoteName('userid') . ' = ' . $db->quote($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$bctedProfile = $db->loadObject();

			if($bctedProfile)
				return $bctedProfile;
			else
				return 0;

		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		return 0;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function checkBlackList($userID,$elementID,$elementType)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_blacklist'))
			->where($db->quoteName('element_id') . ' = ' . $db->quote($elementID))
			->where($db->quoteName('element_type') . ' = ' . $db->quote($elementType))
			->where($db->quoteName('user_id') . ' = ' . $db->quote($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObject();

		return $result;
	}

	public function userBlackListBy($userID,$elementType)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('element_id')
			->from($db->quoteName('#__bcted_blacklist'))
			->where($db->quoteName('element_type') . ' = ' . $db->quote($elementType))
			->where($db->quoteName('user_id') . ' = ' . $db->quote($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadColumn();

		return $result;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getBookingDetailForVenueTable($bookingIDs,$userType)
	{
		/*echo "<pre>";
		print_r($bookingIDs);
		echo "</pre>";
		exit;*/
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$bctedConfig = $this->getExtensionParamCore();
		/*echo "<pre>";
		print_r($bctedConfig);
		echo "</pre>";*/

		$query->select('vb.*')
			->from($db->quoteName('#__bcted_venue_booking','vb'));

		if(!$bookingIDs)
		{

		}
		else if(is_array($bookingIDs))
		{
			$bookingIDStr = implode(",", $bookingIDs);
			$query->where($db->quoteName('vb.venue_booking_id') . ' IN (' . $bookingIDStr . ')');
		}
		else
		{
			$query->where($db->quoteName('vb.venue_booking_id') . ' = ' . $db->quote($bookingIDs));
		}

		// Create the base select statement.

		$query->select('vt.premium_table_id,vt.venue_table_name,vt.custom_table_name,vt.venue_table_image,vt.venue_table_price,vt.venue_table_capacity,venue_table_description')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		/*$query->select('pt.premium_id,pt.p_table_name')
			->join('LEFT','#bcted_premium_table AS pt ON pt.venue_table_id=vb.venue_table_id');*/

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=vb.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=vb.user_status');

		$query->select('v.userid AS venue_owner,v.venue_name,v.commission_rate,v.city,v.country,v.latitude,v.longitude,v.currency_code,v.currency_sign,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.from_time,v.to_time,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=vb.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('RIGHT','#__bcted_user_profile AS bu ON bu.userid=vb.user_id');

		//$query->order($db->quoteName('vb.venue_booking_datetime') . ' DESC');
		$query->order($db->quoteName('vb.time_stamp') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);


		/*echo $query->dump();
		exit;*/



		$bookings = $db->loadObjectList();

		$resultBooding = array();

		/*echo "<pre>";
		print_r($bookings);
		echo "</pre>";
		exit;*/

		//$userFavouriteVenues = $this->getUserFavouriteVenuesIDs();

		$bctParams = $this->getExtensionParam();

		//if($)

		$depositPer = ($bctParams->deposit_per)?$bctParams->deposit_per:20;
		$loyaltyPer = ($bctParams->loyalty_per)?$bctParams->loyalty_per:1;

		foreach ($bookings as $key => $bookingDetail)
		{
			$data = array();

			/*echo "<pre>";
			print_r($bookingDetail);
			echo "</pre>";*/

			$data['venueID']        = $bookingDetail->venue_id;
			$data['userID']         = $bookingDetail->user_id;
			$data['username']       = ($bookingDetail->name)?$bookingDetail->name:$bookingDetail->last_name;
			$data['lastName']       = $bookingDetail->last_name;
			$data['phoneno']        = $bookingDetail->phoneno;
			$data['maleCount']      = $bookingDetail->male_count;
			$data['femaleCount']    = $bookingDetail->female_count;
			$data['status']         = $bookingDetail->status;
			$data['isNew']          = $bookingDetail->is_new;
			$data['timestamp']      = $bookingDetail->time_stamp;
			$data['ownerMessage']   = $bookingDetail->owner_message;
			$data['venueBookingID'] = $bookingDetail->venue_booking_id;
			$data['venueTableID']   = $bookingDetail->venue_table_id;
			$data['isNoShow']       = $bookingDetail->is_noshow;

			$data['currencyCode']   = $bookingDetail->currency_code;
			$data['currencySign']   = $bookingDetail->currency_sign;

			$data['venueBookingTablePrivacy']   = $bookingDetail->venue_booking_table_privacy;
			$data['venueBookingDatetime']       = date('d-m-Y',strtotime($bookingDetail->venue_booking_datetime));
			$data['venueBookingNumberOfGuest']  = $bookingDetail->venue_booking_number_of_guest;


			$timeStampOfBookingDateTime = strtotime($bookingDetail->venue_booking_datetime.' '.$bookingDetail->booking_to_time);
			$currentTimeStamp = time();

			/*echo $timeStampOfBookingDateTime . " || " . $currentTimeStamp;
			exit;*/
			if($currentTimeStamp>$timeStampOfBookingDateTime && $bookingDetail->user_status != 5)
			{
				continue;
			}


			$data['venueBookingFromTime']  = $this->convertToHM($bookingDetail->booking_from_time);
			$data['venueBookingToTime']  = $this->convertToHM($bookingDetail->booking_to_time);

			$data['venueBookingAdditionalInfo'] = $bookingDetail->venue_booking_additional_info;
			$data['venueBookingCreated']        = $bookingDetail->venue_booking_created;

			$data['city']      = $bookingDetail->city;
			$data['country']   = $bookingDetail->country;
			$data['latitude']  = $bookingDetail->latitude;
			$data['longitude'] = $bookingDetail->longitude;

			if($bookingDetail->deposit_paid_date)
			{
				$data['depositDate'] = date('d-m-Y',strtotime($bookingDetail->deposit_paid_date));
			}
			else
			{
				$data['depositDate'] = "";
			}



			$data['connectionID'] = $this->getMessageConnectionOnly($bookingDetail->user_id,$bookingDetail->venue_owner);
			//$data['currency']     = $bookingDetail->venue_table_currency;

			if($userType == 'User')
			{

				$data['statusText'] = $bookingDetail->user_status_text;

				$bookingDT = $bookingDetail->venue_booking_datetime . ' ' . $bookingDetail->booking_from_time;


				$bookingTS = strtotime($bookingDT);
				$before24 = strtotime("-24 hours", strtotime($bookingDT));
				$dateBefore24 = date('Y-m-d H:i:s',strtotime("-24 hours", strtotime($bookingDT)));


				$currentTime = time();

				$emailBody = "Booking Date : " . $bookingDetail->venue_booking_datetime ."\n";
				$emailBody .= "Booking Time : " . $bookingDetail->booking_from_time ."\n";
				$emailBody .= "dateBefore24 : " . $dateBefore24 ."\n";
				$emailBody .= "Time Stamp for dateBefore24 : " . strtotime($dateBefore24) ."\n";
				$emailBody .= "currentTime : " . $currentTime."\n";
				//$emailBody .= "Booking Date : " . $bookingDetail->venue_booking_datetime."\n";
				//$emailBody .= "Booking Date : " . $bookingDetail->venue_booking_datetime."\n";

				mail('bcted.developer@gmail.com', 'My Table Booking', $emailBody);

				/*echo $bookingDT . ' || '.$bookingTS . ' || ' . $before24 . ' || ' .$currentTime;
				exit;*/

				/*echo date('Y-m-d H:i:s',$before24);
				exit;*/

				if($currentTime<=strtotime($dateBefore24) && $bookingDetail->status == 5)
				{
					$data['canCancel']  = "1";
				}
				else
				{
					$data['canCancel']  = "0";
				}


				if($bookingDetail->user_status == 3)
				{
					if($bookingDetail->commission_rate)
					{
						$depositPer = $bookingDetail->commission_rate;
					}

					/*echo $depositPer . ' || ' . $bookingDetail->venue_table_price;
					exit;*/

					$depositAmount = ($bookingDetail->venue_table_price * $depositPer) / 100;
					$loyaltyPoints = ($bookingDetail->venue_table_price * $loyaltyPer) / 100;

					$data['depositAmount'] = $this->currencyFormat('',$depositAmount);
					$data['loyaltyPoints'] = $loyaltyPoints;

					if($bctedConfig->auto_approve)
					{
						$data['paymentURL'] = JUri::base().'index.php?option=com_bcted&view=dummypayment&task=packageorder.packagePurchased&booking_id=' . $bookingDetail->venue_booking_id . '&booking_type=venue&auto_approve=1';
					}
					else
					{
						$data['paymentURL'] = JUri::base().'index.php?option=com_bcted&view=dummypayment&task=packageorder.packagePurchased&booking_id=' . $bookingDetail->venue_booking_id . '&booking_type=venue';
					}





				}
				else
				{
					$data['depositAmount'] = "";
					$data['loyaltyPoints'] = "";
					$data['paymentURL'] = '';
					//$data['isFavourite']  = 0;
					//$data['canCancel']  = "0";
				}

				$this->FavouriteVenueIDs = $this->getUserFavouriteVenuesIDs($bookingDetail->user_id);

				/*echo "dfgdfg<pre>";
				print_r($this->FavouriteVenueIDs);
				echo "</pre>";
				exit;*/
				if(in_array($bookingDetail->venue_id, $this->FavouriteVenueIDs))
				{
					$data['isFavourite']  = 1;
				}
				else
				{
					$data['isFavourite']  = 0;
				}
			}
			else
			{
				$data['statusText']    = $bookingDetail->status_text;
				$data['totalPrice']    = $this->currencyFormat('',$bookingDetail->total_price);
				$data['depositAmount'] = $this->currencyFormat('',$bookingDetail->deposit_amount);
				$data['amountPayable'] = $this->currencyFormat('',$bookingDetail->amount_payable);
			}


			$bookingTimeStamp = strtotime($bookingDetail->venue_booking_datetime);
			$currentDateTime = strtotime(date('Y-m-d'));

			//echo "<br />".$bookingTimeStamp . " || " . $currentDateTime;

			if($bookingTimeStamp<$currentDateTime)
			{
				$data['isPastRecord']  = 1;
			}
			else
			{
				$data['isPastRecord']  = 0;
			}

			//$data['tableID']         = $tblTable->venue_table_id;
			//$data['venueID']         = $tblTable->venue_id;
			//$data['tableTypeID']     = $tblTable->venue_table_type_id;
			//$data['userID']          = $tblTable->user_id;

			/*if(empty($bookingDetail->venue_table_name))
			{
				continue;
			}*/

			$data['tableID']         = $bookingDetail->venue_table_id;
			$data['tableName']       = $bookingDetail->venue_table_name;
			$data['customTableName'] = $bookingDetail->custom_table_name; //Custom Table Name
			$data['premiumTableID']  = ($bookingDetail->premium_table_id)?$bookingDetail->premium_table_id:"0";
			$data['tableImage']      = ($bookingDetail->venue_table_image)?JUri::base().$bookingDetail->venue_table_image:$this->defaultImage;


			$data['price']           = $this->currencyFormat('',$bookingDetail->venue_table_price);
			$data['capacity']        = $bookingDetail->venue_table_capacity;
			$data['description']     = $bookingDetail->venue_table_description;

			//$data['tableCreated']    = $bookingDetail->venue_table_created;
			//$data['tableModified']   = $bookingDetail->venue_table_modified;
			//$data['timeStamp']       = $bookingDetail->time_stamp;

			$data['venueName']      = $bookingDetail->venue_name;
			$data['venueAddress']   = $bookingDetail->venue_address;
			$data['venueAbout']     = $bookingDetail->venue_about;
			$data['venueAmenities'] = $bookingDetail->venue_amenities;
			$data['venueSigns']     = $bookingDetail->venue_signs;
			$data['venueRating']    = $bookingDetail->venue_rating;
			$data['venueTimings']   = $bookingDetail->venue_timings;

			if(!empty($bookingDetail->from_time))
			{
				$data['fromTime']   = $this->convertToHM($bookingDetail->from_time);
			}
			else{
				$data['fromTime'] = "";
			}

			if(!empty($bookingDetail->to_time))
			{
				$data['toTime']   = $this->convertToHM($bookingDetail->to_time);
			}
			else{
				$data['toTime'] = "";
			}


			$data['venueImage']     = ($bookingDetail->venue_image)?JUri::base().$bookingDetail->venue_image:'';

			$data['isSmart']     = $bookingDetail->is_smart;
			$data['isCasual']    = $bookingDetail->is_casual;
			$data['smoking']     = $bookingDetail->is_smoking;
			$data['isFood']      = $bookingDetail->is_food;
			//$data['price']     = $bookingDetail->price;
			$data['isDrink']     = $bookingDetail->is_drink;
			$data['workingDays'] = $bookingDetail->working_days;

			$data['venueImage']      = ($bookingDetail->venue_image)?JUri::base().$bookingDetail->venue_image:$this->defaultImage;
			$data['venueVideo']      = ($bookingDetail->venue_video)?JUri::base().$bookingDetail->venue_video:'';





			$resultBooding[] = $data;
		}

		return $resultBooding;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getVenueGuestListRequest2($requestIDs)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('vglr.*')
			->from($db->quoteName('#__bcted_venue_guest_list_request','vglr'));

		if(is_array($requestIDs))
		{
			$requestIDStr = implode(",", $requestIDs);
			$query->where($db->quoteName('vglr.venue_guest_list_id') . ' IN (' . $requestIDStr . ')');
		}
		else if($requestIDs)
		{
			$query->where($db->quoteName('vglr.venue_guest_list_id') . ' = ' . $db->quote($requestIDs));
		}

		$users = $this->getLiveUsers();
		if(count($users))
		{
			$liveUserStr = implode(",", $users);
			$query->where($db->quoteName('vglr.user_id') . ' IN (' . $liveUserStr . ')');
		}

		$query->select('vrs.status AS status_text')
			->join('LEFT','#__bcted_status AS vrs ON vrs.id=vglr.status');

		$query->select('vrus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS vrus ON vrus.id=vglr.user_status');

		$query->select('v.venue_name,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vglr.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=vglr.user_id');

		$query->select('vru.last_name,vru.phoneno')
			->join('LEFT','#__bcted_user_profile AS vru ON vru.userid=vglr.user_id');



		$query->order($db->quoteName('vglr.time_stamp') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);


		/*echo $query->dump();
		exit;*/

		$result = $db->loadObjectList();

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/
		/*[venue_guest_list_id] => 25
		[venue_id] => 4
		[user_id] => 142
		[request_date] => 2015-02-28
		[status] => 1
		[user_status] => 2
		[number_of_guest] => 2
		[male_count] => 1
		[female_count] => 1
		[additional_info] => this is additional information
		[time_stamp] => 1426057239
		[created] => 2015-03-11 02:00:39
		[status_text] => Request
		[user_status_text] => Pending
		[venue_name] => Crystal
		[venue_address] => United Arab Emirates
		[venue_about] => This all about crystal club added in by Roger
		[venue_amenities] =>
		[venue_signs] => 2
		[venue_rating] => 3.00
		[venue_timings] => 05:35:00
		[venue_image] => images/bcted/venue/147/5b4e415edb53df124f4c5149.jpg
		[is_smart] => 1
		[is_casual] => 0
		[is_food] => 0
		[is_drink] => 1
		[working_days] => 1,3,5,6,7
		[is_smoking] => 1
		[name] => guest1 Firstname
		[last_name] => guest1
		[phoneno] => +919033000000*/

		$resultRequests = array();

		foreach ($result as $key => $value)
		{
			$tempData = array();
			$tempData['requestID']      = $value->venue_guest_list_id;
			$tempData['requestedDate']  = date('d-m-Y',strtotime($value->request_date));
			$tempData['status']         = $value->status;
			$tempData['user_status']    = $value->user_status;
			$tempData['numberOfGuest']  = $value->number_of_guest;
			$tempData['maleCount']      = $value->male_count;
			$tempData['femaleCount']    = $value->female_count;
			$tempData['additionalInfo'] = $value->additional_info;

			$tempData['venueID']        = $value->venue_id;
			$tempData['venueName']      = $value->venue_name;
			$tempData['venueAddress']   = $value->venue_address;
			$tempData['venueAbout']     = $value->venue_about;
			$tempData['venueAmenities'] = $value->venue_amenities;
			$tempData['venueSigns']     = $value->venue_signs;
			$tempData['venueRating']    = $value->venue_rating;
			$tempData['venueTimings']   = $value->venue_timings;
			//$tempData['venueImage']     = ($value->venue_image)?JUri::base().$value->venue_image:'';
			$tempData['isSmart']        = $value->is_smart;
			$tempData['isCasual']       = $value->is_casual;
			$tempData['smoking']        = $value->is_smoking;
			$tempData['isFood']         = $value->is_food;
			$tempData['isDrink']        = $value->is_drink;
			$tempData['workingDays']    = $value->working_days;
			$tempData['venueImage']     = ($value->venue_image)?JUri::base().$value->venue_image:$this->defaultImage;
			$tempData['venueVideo']     = ($value->venue_video)?JUri::base().$value->venue_video:'';

			$tempData['userID']         = $value->user_id;
			$tempData['username']       = $value->name;
			$tempData['lastName']       = $value->last_name;

			/*$tempData['userID']         = $value->user_id;
			$tempData['username']       = $value->name;
			$tempData['lastName']       = $value->last_name;*/

			$tempData['phoneno']        = $value->phoneno;

			$resultRequests[] = $tempData;
		}

		return $resultRequests;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getVenueGuestListRequest($requestIDs,$venueID = 0, $filterType = 0)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('vglr.*')
			->from($db->quoteName('#__bcted_venue_guest_list_request','vglr'));

		if($venueID)
		{
			$query->where($db->quoteName('vglr.venue_id') . ' = ' . $db->quote($venueID));
		}
		else if(is_array($requestIDs))
		{
			$requestIDStr = implode(",", $requestIDs);
			$query->where($db->quoteName('vglr.venue_guest_list_id') . ' IN (' . $requestIDStr . ')');
		}
		else if($requestIDs)
		{
			$query->where($db->quoteName('vglr.venue_guest_list_id') . ' = ' . $db->quote($requestIDs));
		}

		$users = $this->getLiveUsers();
		if(count($users))
		{
			$liveUserStr = implode(",", $users);
			$query->where($db->quoteName('vglr.user_id') . ' IN (' . $liveUserStr . ')');
		}

		/*echo $query->dump();
		exit;*/

		/*echo "<pre>";
		print_r($query->dump());
		echo "</pre>";
		exit;*/

		// Create the base select statement.

		/*$query->select('vt.venue_table_name,vt.custom_table_name,vt.venue_table_image,vt.venue_table_price,vt.venue_table_capacity,venue_table_description')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');*/

		$query->select('vrs.status AS status_text')
			->join('LEFT','#__bcted_status AS vrs ON vrs.id=vglr.status');

		$query->select('vrus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS vrus ON vrus.id=vglr.user_status');

		$query->select('v.venue_name,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vglr.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=vglr.user_id');

		$query->select('vru.last_name,vru.phoneno')
			->join('LEFT','#__bcted_user_profile AS vru ON vru.userid=vglr.user_id');

		//$query->order($db->quoteName('vb.venue_booking_datetime') . ' DESC');
		if($filterType == 0)
		{
			$todayDate = date('Y-m-d');
			$query->where($db->quoteName('vglr.request_date') . ' = ' . $db->quote($todayDate));
		}
		else if($filterType == -1)
		{
			$todayDate = date('Y-m-d');
			$query->where($db->quoteName('vglr.request_date') . ' < ' . $db->quote($todayDate));
		}
		else if($filterType == 1)
		{
			$todayDate = date('Y-m-d');
			$query->where($db->quoteName('vglr.request_date') . ' > ' . $db->quote($todayDate));
		}

		$query->order($db->quoteName('vglr.time_stamp') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);


		/*echo $query->dump();
		exit;*/

		$result = $db->loadObjectList();

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/
		/*[venue_guest_list_id] => 25
		[venue_id] => 4
		[user_id] => 142
		[request_date] => 2015-02-28
		[status] => 1
		[user_status] => 2
		[number_of_guest] => 2
		[male_count] => 1
		[female_count] => 1
		[additional_info] => this is additional information
		[time_stamp] => 1426057239
		[created] => 2015-03-11 02:00:39
		[status_text] => Request
		[user_status_text] => Pending
		[venue_name] => Crystal
		[venue_address] => United Arab Emirates
		[venue_about] => This all about crystal club added in by Roger
		[venue_amenities] =>
		[venue_signs] => 2
		[venue_rating] => 3.00
		[venue_timings] => 05:35:00
		[venue_image] => images/bcted/venue/147/5b4e415edb53df124f4c5149.jpg
		[is_smart] => 1
		[is_casual] => 0
		[is_food] => 0
		[is_drink] => 1
		[working_days] => 1,3,5,6,7
		[is_smoking] => 1
		[name] => guest1 Firstname
		[last_name] => guest1
		[phoneno] => +919033000000*/

		$resultRequests = array();

		foreach ($result as $key => $value)
		{
			$tempData = array();
			$tempData['requestID']      = $value->venue_guest_list_id;
			$tempData['requestedDate']  = date('d-m-Y',strtotime($value->request_date));
			$tempData['status']         = $value->status;
			$tempData['user_status']    = $value->user_status;
			$tempData['numberOfGuest']  = $value->number_of_guest;
			$tempData['maleCount']      = $value->male_count;
			$tempData['femaleCount']    = $value->female_count;
			$tempData['additionalInfo'] = $value->additional_info;

			$tempData['venueID']        = $value->venue_id;
			$tempData['venueName']      = $value->venue_name;
			$tempData['venueAddress']   = $value->venue_address;
			$tempData['venueAbout']     = $value->venue_about;
			$tempData['venueAmenities'] = $value->venue_amenities;
			$tempData['venueSigns']     = $value->venue_signs;
			$tempData['venueRating']    = $value->venue_rating;
			$tempData['venueTimings']   = $value->venue_timings;
			//$tempData['venueImage']     = ($value->venue_image)?JUri::base().$value->venue_image:'';
			$tempData['isSmart']        = $value->is_smart;
			$tempData['isCasual']       = $value->is_casual;
			$tempData['smoking']        = $value->is_smoking;
			$tempData['isFood']         = $value->is_food;
			$tempData['isDrink']        = $value->is_drink;
			$tempData['workingDays']    = $value->working_days;
			$tempData['venueImage']     = ($value->venue_image)?JUri::base().$value->venue_image:$this->defaultImage;
			$tempData['venueVideo']     = ($value->venue_video)?JUri::base().$value->venue_video:'';

			$tempData['userID']         = $value->user_id;
			$tempData['username']       = $value->name;
			$tempData['lastName']       = $value->last_name;

			/*$tempData['userID']         = $value->user_id;
			$tempData['username']       = $value->name;
			$tempData['lastName']       = $value->last_name;*/

			$tempData['phoneno']        = $value->phoneno;

			$resultRequests[] = $tempData;
		}

		return $resultRequests;
	}

	// getVenueGuestListRequestByDate

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getVenueGuestListRequestByDate($venueID, $date)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$date = date('Y-m-d',strtotime($date));

		$query->select('vglr.*')
			->from($db->quoteName('#__bcted_venue_guest_list_request','vglr'));

		$query->where($db->quoteName('vglr.venue_id') . ' = ' . $db->quote($venueID));
		$query->where($db->quoteName('vglr.status') . ' = ' . $db->quote(5));
		$query->where($db->quoteName('vglr.user_status') . ' = ' . $db->quote(5));
		$query->where($db->quoteName('vglr.request_date') . ' = ' . $db->quote($date));


		$users = $this->getLiveUsers();
		if(count($users))
		{
			$liveUserStr = implode(",", $users);
			$query->where($db->quoteName('vglr.user_id') . ' IN (' . $liveUserStr . ')');
		}

		// Create the base select statement.

		/*$query->select('vt.venue_table_name,vt.custom_table_name,vt.venue_table_image,vt.venue_table_price,vt.venue_table_capacity,venue_table_description')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');*/

		$query->select('vrs.status AS status_text')
			->join('LEFT','#__bcted_status AS vrs ON vrs.id=vglr.status');

		$query->select('vrus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS vrus ON vrus.id=vglr.user_status');

		$query->select('v.venue_name,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vglr.venue_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=vglr.user_id');

		$query->select('vru.last_name,vru.phoneno')
			->join('LEFT','#__bcted_user_profile AS vru ON vru.userid=vglr.user_id');

		//$query->order($db->quoteName('vb.venue_booking_datetime') . ' DESC');
		/*if($filterType == 0)
		{
			$todayDate = date('Y-m-d');
			$query->where($db->quoteName('vglr.request_date') . ' = ' . $db->quote($todayDate));
		}
		else if($filterType == -1)
		{
			$todayDate = date('Y-m-d');
			$query->where($db->quoteName('vglr.request_date') . ' < ' . $db->quote($todayDate));
		}
		else if($filterType == 1)
		{
			$todayDate = date('Y-m-d');
			$query->where($db->quoteName('vglr.request_date') . ' > ' . $db->quote($todayDate));
		}*/

		$query->order($db->quoteName('vglr.time_stamp') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);


		/*echo $query->dump();
		exit;*/

		$result = $db->loadObjectList();

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/


		$resultRequests = array();

		foreach ($result as $key => $value)
		{
			$tempData = array();
			$tempData['requestID']      = $value->venue_guest_list_id;
			$tempData['requestedDate']  = date('d-m-Y',strtotime($value->request_date));
			$tempData['status']         = $value->status;
			$tempData['user_status']    = $value->user_status;

			$tempData['numberOfGuest']  = $value->number_of_guest;
			$tempData['maleCount']      = $value->male_count;
			$tempData['femaleCount']    = $value->female_count;

			$tempData['remainingGuest']  = $value->remaining_total;
			$tempData['remainingMale']   = $value->remaining_male;
			$tempData['remainingFemale'] = $value->remaining_female;

			$tempData['additionalInfo'] = $value->additional_info;

			$tempData['venueID']        = $value->venue_id;
			$tempData['venueName']      = $value->venue_name;
			$tempData['venueAddress']   = $value->venue_address;
			$tempData['venueAbout']     = $value->venue_about;
			$tempData['venueAmenities'] = $value->venue_amenities;
			$tempData['venueSigns']     = $value->venue_signs;
			$tempData['venueRating']    = $value->venue_rating;
			$tempData['venueTimings']   = $value->venue_timings;
			//$tempData['venueImage']     = ($value->venue_image)?JUri::base().$value->venue_image:'';
			$tempData['isSmart']        = $value->is_smart;
			$tempData['isCasual']       = $value->is_casual;
			$tempData['smoking']        = $value->is_smoking;
			$tempData['isFood']         = $value->is_food;
			$tempData['isDrink']        = $value->is_drink;
			$tempData['workingDays']    = $value->working_days;
			$tempData['venueImage']     = ($value->venue_image)?JUri::base().$value->venue_image:$this->defaultImage;
			$tempData['venueVideo']     = ($value->venue_video)?JUri::base().$value->venue_video:'';

			$tempData['userID']         = $value->user_id;
			$tempData['username']       = $value->name;
			$tempData['lastName']       = $value->last_name;



			$tempData['phoneno']        = $value->phoneno;

			$resultRequests[] = $tempData;
		}

		return $resultRequests;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getPakcagesDetail($packageID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('pckg.*')
			->from($db->quoteName('#__bcted_package','pckg'));

		if(!$packageID)
		{
			return array();
		}
		else if(is_array($packageID))
		{
			$packageIDStr = implode(",", $packageID);
			$query->where($db->quoteName('pckg.package_id') . ' IN (' . $packageIDStr . ')');
		}
		else
		{
			$query->where($db->quoteName('pckg.package_id') . ' = ' . $db->quote($packageID));
		}

		// Create the base select statement.

		/*$query->select('vt.venue_table_name,vt.custom_table_name,vt.venue_table_image,vt.venue_table_price,vt.venue_table_capacity,venue_table_description')
			->join('LEFT','#__bcted_venue_table AS vt ON vt.venue_table_id=vb.venue_table_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=vb.status')*/;

		$query->select('v.userid AS venue_owner,v.venue_name,v.city,v.country,v.latitude,v.longitude,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=pckg.venue_id');

		$query->select('c.company_name,c.company_image,c.company_address,c.city,c.country,c.latitude,c.longitude,c.company_about,c.company_signs,c.company_rating')
			->join('LEFT','#__bcted_company AS c ON c.company_id=pckg.company_id');

		/*$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=pckg.user_id');*/

		/*$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=vb.user_id');*/

		$query->order($db->quoteName('pckg.package_date') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);


		/*echo $query->dump();
		exit;*/



		$packages = $db->loadObjectList();

		/*echo "<pre>";
		print_r($packages);
		echo "</pre>";
		exit;*/

		$resultPackages = array();

		foreach ($packages as $key => $package)
		{
			$data = array();

			$data['packageID']      = $package->package_id;
			$data['packageName']    = $package->package_name;
			//$data['packageImage']   = ($package->package_image)?JUri::base().$package->package_image:'';
			$data['packageImage']   = ($package->package_image)?JUri::base().$package->package_image:$this->defaultImage;

			$data['packageDetail']  = $package->package_details;
			$data['packagePrice']   = $this->currencyFormat('',$package->package_price);
			$data['currencyCode']   = $package->currency_code;
			$data['currencySign']   = $package->currency_sign;

			$packageServices = $this->getServiceNameForPackage($package->company_ids);
			$data['services'] = implode(",", $packageServices);

			/*if($package->company_id)
			{
				$data['companyID']      = $package->company_id;
				$data['city']           = $package->city;
				$data['country']        = $package->country;
				$data['latitude']       = $package->latitude;
				$data['longitude']      = $package->longitude;
				$data['companyName']    = $package->company_name;
				$data['companyAddress'] = $package->company_address;
				$data['companyAbout']   = $package->company_about;
				$data['companySigns']   = $package->company_signs;
				$data['companyImage']   = ($package->company_image)?JUri::base().$package->company_image:$this->defaultImage;
			}*/

			if($package->venue_id)
			{
				$data['venueID']      = $package->venue_id;
				$data['venueName']      = $package->venue_name;
				$data['venueAddress']   = $package->venue_address;
				$data['venueAbout']     = $package->venue_about;
				$data['venueAmenities'] = $package->venue_amenities;
				$data['venueSigns']     = $package->venue_signs;
				$data['venueRating']    = $package->venue_rating;
				$data['venueTimings']   = $package->venue_timings;
				$data['isSmart']     = $package->is_smart;
				$data['isCasual']    = $package->is_casual;
				$data['smoking']     = $package->is_smoking;
				$data['isFood']      = $package->is_food;
				$data['isDrink']     = $package->is_drink;
				$data['workingDays'] = $package->working_days;
				$data['venueImage']      = ($package->venue_image)?JUri::base().$package->venue_image:$this->defaultImage;
				$data['venueVideo']     = ($package->venue_video)?JUri::base().$package->venue_video:'';
			}

			//$data['username']       = $package->name;
			$data['packageDate'] = date('d-m-Y',strtotime($package->package_date));


			$resultPackages[] = $data;
		}

		return $resultPackages;
	}

	public function getServiceNameForPackage($serviceIDs = '')
	{
		$result = array();
		if(!empty($serviceIDs))
		{
			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('service_name')
				->from($db->quoteName('#__bcted_company_services'))
				->where($db->quoteName('service_id') . ' IN (' . $serviceIDs . ')')
				->order($db->quoteName('service_name') . ' ASC');

			// Set the query and load the result.
			$db->setQuery($query);

			$result = $db->loadColumn();
		}

		return $result;
	}

	public function getBookingDetailForPackage($ids,$userType,$inviteData = 0,$removePast = 0)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('pp.*')
			->from($db->quoteName('#__bcted_package_purchased','pp'));

		if(!$ids)
		{
			return array();
		}
		else if(is_array($ids))
		{
			$packageIDStr = implode(",", $ids);
			$query->where($db->quoteName('pp.package_purchase_id') . ' IN (' . $packageIDStr . ')');
		}
		else
		{
			$query->where($db->quoteName('pp.package_purchase_id') . ' = ' . $db->quote($ids));
		}

		$query->select('pckg.package_name,pckg.package_image,pckg.package_details,pckg.package_price,pckg.currency_code AS package_current_currency_code,pckg.currency_sign AS package_current_currency_sign,pckg.package_date,pckg.company_ids')
			->join('LEFT','#__bcted_package AS pckg ON pckg.package_id=pp.package_id');

		$query->select('ps.currency_code AS package_currency_code,ps.currency_sign AS package_currency_sign')
			->join('LEFT','#__bcted_payment_status AS ps ON ps.booked_element_id=pp.package_purchase_id AND ps.booked_element_type="package"');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=pp.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=pp.user_status');

		$query->select('v.userid AS venue_owner,v.venue_name,v.city,v.country,v.latitude,v.longitude,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=pp.venue_id');

		/*$query->select('c.userid AS company_owner,c.company_name,c.city,c.country,c.latitude,c.longitude,c.company_address,c.company_about,c.company_signs,c.company_rating,c.	company_image')
			->join('LEFT','#__bcted_company AS c ON c.company_id=pp.company_id');*/

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=pp.user_id');

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=pp.user_id');

		$query->order($db->quoteName('pp.time_stamp') . ' DESC');

		/*echo $query->dump();
		exit;*/

		$db->setQuery($query);
		$packages = $db->loadObjectList();

		$bctParams = $this->getExtensionParam();

		$depositPer = ($bctParams->deposit_per)?$bctParams->deposit_per:20;
		$loyaltyPer = ($bctParams->loyalty_per)?$bctParams->loyalty_per:1;

		$resultPackages = array();

		foreach ($packages as $key => $package)
		{
			if($removePast)
			{
				$currentTimeStamp = time();
				$packageTimeStamp = strtotime($package->package_date.' '.$package->package_time);

				/*echo $packageTimeStamp;
				exit;*/

				if($currentTimeStamp>$packageTimeStamp)
				{
					continue;
				}
			}
			$data = array();
			$data['packageBookingID'] = $package->package_purchase_id;
			$data['packageID']      = $package->package_id;
			$data['packageName']    = $package->package_name;
			$data['payableAmount'] = $this->currencyFormat('',$package->total_price);
			//$data['packageImage']   = ($package->package_image)?JUri::base().$package->package_image:'';
			$data['packageImage']   = ($package->package_image)?JUri::base().$package->package_image:$this->defaultImage;

			$data['packageDetail'] = $package->package_details;
			$data['packagePrice']  = $this->currencyFormat('',$package->package_price * $package->package_number_of_guest);

			if($package->package_currency_code)
			{
				$data['currencyCode']  = $package->package_currency_code;
				$data['currencySign']  = $package->package_currency_sign;
			}
			else
			{
				$data['currencyCode']  = $package->package_current_currency_code;
				$data['currencySign']  = $package->package_current_currency_sign;
			}

			$data['ownerMessage']  = $package->owner_message;

			$data['packageBookingNumberOfGuest'] = $package->package_number_of_guest;
			$data['maleCount']                   = $package->male_count;
			$data['femaleCount']                 = $package->female_count;

			$data['userID']      = $package->user_id;
			$data['username']    = $package->name;
			$data['lastName']    = $package->last_name;

			$data['packageLocation'] = $package->package_location;
			$data['additionalInfo']  = $package->package_additional_info;
			/*$data['femaleCount']                 = $package->female_count;
			$data['femaleCount']                 = $package->female_count;*/

			//$data['username']       = $package->name;
			$data['packageDate'] = date('d-m-Y',strtotime($package->package_date));

			if($package->venue_id)
			{
				$data['venueID']      = $package->venue_id;
				$data['venueName']      = $package->venue_name;
				$data['venueAddress']   = $package->venue_address;
				$data['venueAbout']     = $package->venue_about;
				$data['venueAmenities'] = $package->venue_amenities;
				$data['venueSigns']     = $package->venue_signs;
				$data['venueRating']    = $package->venue_rating;
				$data['venueTimings']   = $package->venue_timings;
				//$data['venueImage']     = ($package->venue_image)?JUri::base().$package->venue_image:'';

				$data['isSmart']     = $package->is_smart;
				$data['isCasual']    = $package->is_casual;
				$data['smoking']     = $package->is_smoking;
				$data['isFood']      = $package->is_food;
				//$data['price']     = $bookingDetail->price;
				$data['isDrink']     = $package->is_drink;
				$data['workingDays'] = $package->working_days;

				$data['venueImage']      = ($package->venue_image)?JUri::base().$package->venue_image:$this->defaultImage;
				$data['venueVideo']     = ($package->venue_video)?JUri::base().$package->venue_video:'';
			}

			$packageServices = $this->getServiceNameForPackage($package->company_ids);
			$data['services'] = implode(",", $packageServices);

			/*else if($package->company_id)
			{
				$data['companyID']      = $package->company_id;
				$data['city']           = $package->city;
				$data['country']        = $package->country;
				$data['latitude']       = $package->latitude;
				$data['longitude']      = $package->longitude;
				$data['companyName']    = $package->company_name;
				$data['companyAddress'] = $package->company_address;
				$data['companyAbout']   = $package->company_about;
				$data['companySigns']   = $package->company_signs;
				$data['companyImage']   = ($package->company_image)?JUri::base().$package->company_image:$this->defaultImage;
			}*/


			if($userType == 'User')
			{
				$data['statusText'] = $package->user_status_text;

				if($package->user_status == 3)
				{
					//$depositAmount = ($package->package_price * $depositPer) / 100;
					$loyaltyPoints = ($package->package_price * $loyaltyPer) / 100;

					//$data['depositAmount'] = $depositAmount;
					$data['payableAmount'] = $this->currencyFormat('',$package->total_price);
					$data['loyaltyPoints'] = $loyaltyPoints;
					$bctParams = $this->getExtensionParam();
					if($bctParams->auto_approve)
					{
						$data['paymentURL']    = JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $package->package_purchase_id . '&booking_type=package&auto_approve=1';
					}
					else
					{
						$data['paymentURL']    = JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $package->package_purchase_id . '&booking_type=package';
					}


					/*if(in_array($package->service_id, $this->FavouriteServiceIDs))
					{
						$data['isFavourite']  = 1;
					}
					else
					{
						$data['isFavourite']  = 0;
					}*/
				}
				else
				{
					//$data['depositAmount'] = "";
					$data['loyaltyPoints'] = "";
					$data['paymentURL']    = JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $package->package_purchase_id . '&booking_type=package';
					$data['isFavourite']   = 0;
				}

				$data['bookedUserPaid']   = $package->booked_user_paid;
				$data['canInvite']   = $package->can_invite;

				if($package->status == 5 || $inviteData==1)
				{
					$data['invitedUserStatus'] = $this->getPackageInvitedUserDetail($package->package_purchase_id);
				}
			}
			else
			{
				$data['statusText']    = $package->status_text;
				$data['totalPrice']    = $this->currencyFormat('',$package->total_price);
				$data['depositAmount'] = $this->currencyFormat('',$package->deposit_amount);
				$data['amountPayable'] = $this->currencyFormat('',$package->amount_payable);
			}


			$resultPackages[] = $data;
		}

		return $resultPackages;
	}

	public function getPackageInvitedUserDetail($packagePurchaseID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('pi.*')
			->from($db->quoteName('#__bcted_package_invite','pi'));
			//->where($db->quoteName('pi.invited_user_id') . ' <> ' . $db->quote(0));

		$query->where($db->quoteName('pi.package_purchase_id') . ' = ' . $db->quote($packagePurchaseID));


		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=pi.status');

		/*$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=pi.user_status');*/

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=pi.invited_user_id');

		/*$query->select('u2.name AS pakcage_purchased_user_name')
			->join('LEFT','#__users AS u2 ON u2.id=pp.user_id');*/

		$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=pi.invited_user_id');

		$query->order($db->quoteName('pi.created') . ' DESC');

		/*echo $query->dump();
		exit;*/

		$db->setQuery($query);
		$invitations = $db->loadObjectList();

		/*echo "<pre>";
		print_r($invitations);
		echo "</pre>";

		exit;*/

		$invitationResult = array();

		foreach ($invitations as $key => $invitation)
		{
			/*echo "<pre>";
			print_r($invitation);
			echo "</pre>";
			exit;*/
			$tempData = array();
			$tempData['userEmail'] = $invitation->invited_email;
			$tempData['name'] = ($invitation->name)?$invitation->name:'';
			$tempData['lastName'] = ($invitation->last_name)?$invitation->last_name:'';


			if(!empty($invitation->payment_status) && ( strtolower($invitation->payment_status) == 'completed' || strtolower($invitation->payment_status) == 'success') )
			{
				$tempData['paymentStatus'] = 'Paid';
			}
			else
			{
				$tempData['paymentStatus'] = 'Not Paid';
			}

			$invitationResult[] = $tempData;
		}

		return $invitationResult;


	}


	public function getIviteDetailForPackage($ids,$userType)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('pi.*')
			->from($db->quoteName('#__bcted_package_invite','pi'));

		if(!$ids)
		{
			return array();
		}
		else if(is_array($ids))
		{
			$packageIDStr = implode(",", $ids);
			$query->where($db->quoteName('pi.package_invite_id') . ' IN (' . $packageIDStr . ')');
		}
		else
		{
			$query->where($db->quoteName('pi.package_invite_id') . ' = ' . $db->quote($ids));
		}

		$query->select('pp.user_id AS pakcage_purchased_user_id,pp.venue_id,pp.company_id,pp.package_datetime,pp.package_time,pp.package_number_of_guest,pp.male_count,pp.female_count,pp.package_location,pp.package_additional_info,pp.total_price,pp.deposit_amount,pp.amount_payable as pp_amount_payable')
			->join('LEFT','#__bcted_package_purchased AS pp ON pp.package_purchase_id=pi.package_purchase_id');

		$query->select('pckg.package_name,pckg.package_image,pckg.package_details,pckg.package_price,pckg.currency_code AS package_currency_code,pckg.currency_sign AS package_currency_sign,pckg.package_date')
			->join('LEFT','#__bcted_package AS pckg ON pckg.package_id=pi.package_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=pi.status');

		/*$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=pi.user_status');*/

		$query->select('v.userid AS venue_owner,v.venue_name,v.city,v.country,v.latitude,v.longitude,v.venue_address,v.venue_about,v.venue_amenities,v.venue_signs,v.venue_rating,v.venue_timings,v.venue_image,v.venue_video,v.is_smart,v.is_casual,v.is_food,v.is_drink,v.working_days,v.is_smoking')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=pp.venue_id');

		$query->select('c.userid AS company_owner,c.company_name,c.city,c.country,c.latitude,c.longitude,c.company_address,c.company_about,c.company_signs,c.company_rating,c.company_image')
			->join('LEFT','#__bcted_company AS c ON c.company_id=pp.company_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=pi.invited_user_id');

		$query->select('u2.name AS pakcage_purchased_user_name')
			->join('LEFT','#__users AS u2 ON u2.id=pp.user_id');

		$query->select('bu.last_name AS pakcage_purchased_user_last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=pp.user_id');

		$query->order($db->quoteName('pi.created') . ' DESC');

		/*echo $query->dump();
		exit;*/

		$db->setQuery($query);
		$packages = $db->loadObjectList();

		$bctParams = $this->getExtensionParam();

		$depositPer = ($bctParams->deposit_per)?$bctParams->deposit_per:20;
		$loyaltyPer = ($bctParams->loyalty_per)?$bctParams->loyalty_per:1;

		$resultPackages = array();

		/*echo "<pre>";
		print_r($packages);
		echo "</pre>";
		exit;*/

		foreach ($packages as $key => $package)
		{
			$currentTimeStamp = time();
			$packageTimeStamp = strtotime($package->package_date.' '.$package->package_time);
			if($currentTimeStamp>$packageTimeStamp)
			{
				continue;
			}

			$data = array();
			$data['packageInviteID'] = $package->package_invite_id;
			$data['packageBookingID'] = $package->package_purchase_id;
			$data['packageID']      = $package->package_id;
			$data['packageName']    = $package->package_name;
			//$data['packageImage']   = ($package->package_image)?JUri::base().$package->package_image:'';
			$data['packageImage']   = ($package->package_image)?JUri::base().$package->package_image:$this->defaultImage;

			$data['packageDetail']  = $package->package_details;
			$data['packagePrice']   = $package->package_price;
			$data['payablePrice']   = $this->currencyFormat('',$package->total_price);
			$data['currencyCode']   = $package->package_currency_code;
			$data['currencySign']   = $package->package_currency_sign;

			$data['packageBookingNumberOfGuest'] = $package->package_number_of_guest;
			$data['maleCount']                   = $package->male_count;
			$data['femaleCount']                 = $package->female_count;

			$data['packagePurchasedUserID']      = $package->pakcage_purchased_user_id;
			$data['username']    = $package->pakcage_purchased_user_name;
			$data['lastName']    = $package->pakcage_purchased_user_last_name;

			$data['packageLocation'] = $package->package_location;
			$data['additionalInfo']  = $package->package_additional_info;
			/*$data['femaleCount']                 = $package->female_count;
			$data['femaleCount']                 = $package->female_count;*/

			//$data['username']       = $package->name;
			$data['packageDate'] = date('d-m-Y',strtotime($package->package_date));

			if($package->venue_id)
			{
				$data['venueID']      = $package->venue_id;
				$data['venueName']      = $package->venue_name;
				$data['venueAddress']   = $package->venue_address;
				$data['venueAbout']     = $package->venue_about;
				$data['venueAmenities'] = $package->venue_amenities;
				$data['venueSigns']     = $package->venue_signs;
				$data['venueRating']    = $package->venue_rating;
				$data['venueTimings']   = $package->venue_timings;
				//$data['venueImage']     = ($package->venue_image)?JUri::base().$package->venue_image:'';

				$data['isSmart']     = $package->is_smart;
				$data['isCasual']    = $package->is_casual;
				$data['smoking']     = $package->is_smoking;
				$data['isFood']      = $package->is_food;
				//$data['price']     = $bookingDetail->price;
				$data['isDrink']     = $package->is_drink;
				$data['workingDays'] = $package->working_days;

				$data['venueImage']      = ($package->venue_image)?JUri::base().$package->venue_image:$this->defaultImage;
				$data['venueVideo']     = ($package->venue_video)?JUri::base().$package->venue_video:'';
			}
			else if($package->company_id)
			{
				$data['companyID']      = $package->company_id;
				$data['city']           = $package->city;
				$data['country']        = $package->country;
				$data['latitude']       = $package->latitude;
				$data['longitude']      = $package->longitude;
				$data['companyName']    = $package->company_name;
				$data['companyAddress'] = $package->company_address;
				$data['companyAbout']   = $package->company_about;
				$data['companySigns']   = $package->company_signs;
				$data['companyImage']   = ($package->company_image)?JUri::base().$package->company_image:$this->defaultImage;
			}

			if($userType == 'User')
			{
				$data['statusText'] = $package->status_text;
				$bctParams = $this->getExtensionParam();
				if($bctParams->auto_approve)
				{
					$data['paymentURL']    = JUri::base().'index.php?option=com_bcted&task=packageorder.packageInvitePayment&package_invite_id=' . $package->package_invite_id . '&booking_type=packageinvitation&auto_approve=1';
				}
				else
				{
					$data['paymentURL']    = JUri::base().'index.php?option=com_bcted&task=packageorder.packageInvitePayment&package_invite_id=' . $package->package_invite_id . '&booking_type=packageinvitation';
				}


				/*if($package->user_status == 3)
				{
					$depositAmount = ($package->package_price * $depositPer) / 100;
					$loyaltyPoints = ($package->package_price * $loyaltyPer) / 100;

					$data['depositAmount'] = $depositAmount;
					$data['loyaltyPoints'] = $loyaltyPoints;
					$data['paymentURL']    = JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $package->package_purchase_id . '&booking_type=service';

					/*if(in_array($package->service_id, $this->FavouriteServiceIDs))
					{
						$data['isFavourite']  = 1;
					}
					else
					{
						$data['isFavourite']  = 0;
					}*/
				/*}
				else
				{
					$data['depositAmount'] = "";
					$data['loyaltyPoints'] = "";
					$data['paymentURL']    = "";
					$data['isFavourite']   = 0;
				}*/
			}
			else
			{
				$data['statusText']    = $package->status_text;
				$data['totalPrice']    = $this->currencyFormat('',$package->total_price);
				$data['depositAmount'] = $this->currencyFormat('',$package->deposit_amount);
				$data['amountPayable'] = $this->currencyFormat('',$package->amount_payable);
			}


			$resultPackages[] = $data;
		}

		return $resultPackages;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getBookingDetailForServices($bookingIDs,$userType)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('sb.*')
			->from($db->quoteName('#__bcted_service_booking','sb'));

		if(!$bookingIDs)
		{

		}
		else if(is_array($bookingIDs))
		{
			$bookingIDStr = implode(",", $bookingIDs);
			$query->where($db->quoteName('sb.service_booking_id') . ' IN (' . $bookingIDStr . ')');
		}
		else
		{
			$query->where($db->quoteName('sb.service_booking_id') . ' = ' . $db->quote($bookingIDs));
		}

		// Create the base select statement.

		$query->select('cs.service_name,cs.service_image,cs.service_description,cs.service_price')
			->join('LEFT','#__bcted_company_services AS cs ON cs.service_id=sb.service_id');

		$query->select('bs.status AS status_text')
			->join('LEFT','#__bcted_status AS bs ON bs.id=sb.status');

		$query->select('bus.status AS user_status_text')
			->join('LEFT','#__bcted_status AS bus ON bus.id=sb.user_status');

		$query->select('c.company_name,c.company_image,c.company_address,c.city,c.country,c.latitude,c.longitude,c.currency_code,c.currency_sign,c.company_about,c.company_signs,c.company_rating,c.attire,c.restaurant,c.smoking,c.rating,c.drink,c.commission_rate')
			->join('LEFT','#__bcted_company AS c ON c.company_id=sb.company_id');

		$query->select('u.name')
			->join('LEFT','#__users AS u ON u.id=sb.user_id');

		/*$query->select('bu.last_name,bu.phoneno')
			->join('LEFT','#__bcted_user_profile AS bu ON bu.userid=sb.user_id');*/

		$query->select('bu.last_name,bu.phoneno')
			->join('RIGHT','#__bcted_user_profile AS bu ON bu.userid=sb.user_id');

		//$query->order($db->quoteName('sb.service_booking_datetime') . ' DESC');
		$query->order($db->quoteName('sb.time_stamp') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);

		$bookings = $db->loadObjectList();

		$resultBooding = array();

		$bctParams = $this->getExtensionParam();

		$depositPer = ($bctParams->deposit_per)?$bctParams->deposit_per:20;
		$loyaltyPer = ($bctParams->loyalty_per)?$bctParams->loyalty_per:1;

		foreach ($bookings as $key => $bookingDetail)
		{
			$data = array();


			$data['serviceBookingID'] = $bookingDetail->service_booking_id;
			$data['serviceID']        = $bookingDetail->service_id;
			$data['companyID']        = $bookingDetail->company_id;

			$data['userID']      = $bookingDetail->user_id;
			$data['username']    = ($bookingDetail->name)?$bookingDetail->name:$bookingDetail->last_name;
			$data['lastName']    = $bookingDetail->last_name;
			$data['phoneno']     = $bookingDetail->phoneno;
			$data['maleCount']   = $bookingDetail->male_count;
			$data['femaleCount'] = $bookingDetail->female_count;
			$data['latitude']    = $bookingDetail->latitude;
			$data['longitude']   = $bookingDetail->longitude;

			if(!empty($bookingDetail->deposit_paid_date))
			{
				$data['depositDate'] = date('d-m-Y',strtotime($bookingDetail->deposit_paid_date));
			}
			else
			{
				$data['depositDate'] = '';
			}

			$timeStampOfBookingDateTime = strtotime($bookingDetail->service_booking_datetime.' '.$bookingDetail->booking_to_time);
			$currentTimeStamp = time();

			/*echo $timeStampOfBookingDateTime . " || " . $currentTimeStamp;
			exit;*/
			if($currentTimeStamp>$timeStampOfBookingDateTime && $bookingDetail->user_status != 5)
			{
				/*echo "call";
				exit;*/
				continue;
			}



			$data['isNew']          = $bookingDetail->is_new;

			$data['serviceBookingDatetime']       = date('d-m-Y',strtotime($bookingDetail->service_booking_datetime));
			$data['fromTime']                     =  $this->convertToHM($bookingDetail->booking_from_time);
			$data['toTime']                       =  $this->convertToHM($bookingDetail->booking_to_time);
			$data['serviceLocation']              = $bookingDetail->service_location;
			$data['serviceCity']                  = $bookingDetail->service_city;
			$data['serviceBookingAdditionalInfo'] = $bookingDetail->service_booking_additional_info;
			$data['serviceBookingNumberOfGuest']  = $bookingDetail->service_booking_number_of_guest;
			$data['ownerMessage']                 = $bookingDetail->owner_message;
			$data['serviceBookingCreated']        = $bookingDetail->service_booking_created;
			$data['status']                       = $bookingDetail->status;

			$data['city']      = $bookingDetail->city;
			$data['country']   = $bookingDetail->country;
			$data['latitude']  = $bookingDetail->latitude;
			$data['longitude'] = $bookingDetail->longitude;

			$data['currencyCode'] = $bookingDetail->currency_code;
			$data['currencySign'] = $bookingDetail->currency_sign;


			if($userType == 'User')
			{
				$data['statusText'] = $bookingDetail->user_status_text;

				$bookingDT = $bookingDetail->service_booking_datetime . ' ' . $bookingDetail->booking_from_time;
				$bookingTS = strtotime($bookingDT);
				$before24 = strtotime("-24 hours", strtotime($bookingDT));
				$currentTime = time();

				if($currentTime<=$before24 && $bookingDetail->status==5)
				{
					$data['canCancel']  = "1";
				}
				else
				{
					$data['canCancel']  = "0";
				}

				if($bookingDetail->user_status == 3)
				{
					/*if($bookingDetail->commission_rate)
					{
						$depositPer = $bookingDetail->commission_rate;
					}
					$depositAmount = ($bookingDetail->service_price * $depositPer) / 100;*/
					$loyaltyPoints = ($bookingDetail->total_price * $loyaltyPer) / 100;

					$data['depositAmount'] = $this->currencyFormat('',$bookingDetail->deposit_amount);
					$data['loyaltyPoints'] = $loyaltyPoints;

					$bctParams = $this->getExtensionParam();
					if($bctParams->auto_approve)
					{
						$data['paymentURL']    = JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $bookingDetail->service_booking_id . '&booking_type=service&auto_approve=1';
					}
					else
					{
						$data['paymentURL']    = JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $bookingDetail->service_booking_id . '&booking_type=service';
					}



				}
				else
				{
					$loyaltyPoints = ($bookingDetail->total_price * $loyaltyPer) / 100;
					$data['depositAmount'] = $this->currencyFormat('',$bookingDetail->deposit_amount);
					$data['loyaltyPoints'] = "";
					$data['paymentURL']    = "";
					//$data['isFavourite']   = 0;
				}

				/*echo $this->IJUserID."<pre>";
				print_r($this->FavouriteServiceIDs);
				echo "</pre>";
				exit;*/
				$this->FavouriteServiceIDs = $this->getUserFavouriteServiceIDs($bookingDetail->user_id);
				if(in_array($bookingDetail->company_id, $this->FavouriteServiceIDs))
				{
					$data['isFavourite']  = 1;
				}
				else
				{
					$data['isFavourite']  = 0;
				}
			}
			else
			{
				$data['statusText']    = $bookingDetail->status_text;
				$data['totalPrice']    = $this->currencyFormat('',$bookingDetail->total_price);
				$data['depositAmount'] = $this->currencyFormat('',$bookingDetail->deposit_amount);
				$data['amountPayable'] = $this->currencyFormat('',$bookingDetail->amount_payable);
			}

			//$data['statusText']                   = $bookingDetail->status_text;

			$bookingTimeStamp = strtotime($bookingDetail->service_booking_datetime);
			$currentDateTime = strtotime(date('Y-m-d'));

			//echo $bookingTimeStamp . " || " . $currentDateTime;

			if($bookingTimeStamp<$currentDateTime)
			{
				$data['isPastRecord']  = 1;
			}
			else
			{
				$data['isPastRecord']  = 0;
			}

			/*$currentTime = strtotime(time)
			if()*/

			//$data['tableID']         = $tblTable->venue_table_id;
			//$data['venueID']         = $tblTable->venue_id;
			//$data['tableTypeID']     = $tblTable->venue_table_type_id;
			//$data['userID']          = $tblTable->user_id;
			$data['serviceName']  = $bookingDetail->service_name;
			$data['description']  = $bookingDetail->service_description; //Custom Table Name
			$data['serviceImage'] = ($bookingDetail->service_image)?JUri::base().$bookingDetail->service_image:'';

			$data['serviceImage'] = ($bookingDetail->service_image)?JUri::base().$bookingDetail->service_image:$this->defaultImage;

			$data['attire']        = $bookingDetail->attire;
			$data['restaurant']    = $bookingDetail->restaurant;
			$data['smoking']       = $bookingDetail->smoking;
			$data['rating']        = $bookingDetail->rating;
			//$data['price']         = $bookingDetail->price;
			$data['drink']         = $bookingDetail->drink;


			$data['price']        = $this->currencyFormat('',$bookingDetail->service_price) . '/hr';
			$data['totalPrice']        = $this->currencyFormat('',$bookingDetail->total_price);
			//$data['capacity']        = $bookingDetail->venue_table_capacity;
			//$data['description']     = $bookingDetail->venue_table_description;

			//$data['tableCreated']    = $bookingDetail->venue_table_created;
			//$data['tableModified']   = $bookingDetail->venue_table_modified;
			//$data['timeStamp']       = $bookingDetail->time_stamp;

			$data['companyName']    = $bookingDetail->company_name;
			$data['companyAddress'] = $bookingDetail->company_address;
			$data['companyAbout']   = $bookingDetail->company_about;
			$data['companySigns']   = $bookingDetail->company_signs;
			$data['companyImage']   = ($bookingDetail->company_image)?JUri::base().$bookingDetail->company_image:'';
			$data['companyRating'] = $bookingDetail->company_rating;
			$data['companyImage']   = ($bookingDetail->company_image)?JUri::base().$bookingDetail->company_image:$this->defaultImage;


			$resultBooding[] = $data;
		}




		return $resultBooding;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function summaryForCompany($companyID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('count(status) as total_count')
			->from($db->quoteName('#__bcted_service_booking'))
			->where($db->quoteName('company_id') . ' = ' . $db->quote($companyID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
			->where($db->quoteName('deleted_by_company') . ' = ' . $db->quote('0'))
			->group($db->quoteName('status'));

		/*echo $query->dump();
		exit;*/


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/

		return $result;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getTotalRequestForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$statusID = $this->getStatusIDFromStatusName('Request');

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
			->where($db->quoteName('deleted_by_venue') . ' = ' . $db->quote('0'))
			->where($db->quoteName('status') . ' = ' . $db->quote($statusID));

		// Set the query and load the result.
		$db->setQuery($query);

		$allRequests = $db->loadObjectList();
		$totalRequest = 0;

		foreach ($allRequests as $key => $request)
		{
			$bookingTimeStamp = strtotime($request->venue_booking_datetime.' '.$request->booking_from_time);
			$currentTime = time();

			if($currentTime>$bookingTimeStamp)
			{
				continue;
			}

			$totalRequest = $totalRequest + 1;
		}

		return $totalRequest;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getTotalRequestForCompany($companyID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$statusID = $this->getStatusIDFromStatusName('Request');

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_service_booking'))
			->where($db->quoteName('company_id') . ' = ' . $db->quote($companyID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
			->where($db->quoteName('deleted_by_company') . ' = ' . $db->quote('0'))
			->where($db->quoteName('status') . ' = ' . $db->quote($statusID));

		// Set the query and load the result.
		$db->setQuery($query);

		$allRequests = $db->loadObjectList();
		$totalRequest = 0;

		foreach ($allRequests as $key => $request)
		{
			$bookingTimeStamp = strtotime($request->service_booking_datetime.' '.$request->booking_from_time);
			$currentTime = time();

			if($currentTime>$bookingTimeStamp)
			{
				continue;
			}

			$totalRequest = $totalRequest + 1;
		}

		return $totalRequest;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getRevenueForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('sum(amount_payable) as revenue')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('status') . ' = ' . $db->quote(5))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadResult();

		return $this->currencyFormat('',$result);
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getRevenueForCompnay($companyID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('sum(amount_payable) as revenue')
			->from($db->quoteName('#__bcted_service_booking'))
			->where($db->quoteName('company_id') . ' = ' . $db->quote($companyID))
			->where($db->quoteName('status') . ' = ' . $db->quote(5))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadResult();

		return $this->currencyFormat('',$result);
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function summaryForVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('count(status) as total_count,status')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote('0'))
			->where($db->quoteName('deleted_by_venue') . ' = ' . $db->quote('0'))
			->group($db->quoteName('status'));


		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/

		return $result;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getVenueDetail($venueID)
	{
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($venueID);

		if(!$tblVenue->venue_id)
		{
			return array();
		}

		$venue = array();

		$venue['venueID']        = $tblVenue->venue_id;
		$venue['venueName']      = $tblVenue->venue_name;
		$venue['venueAddress']   = $tblVenue->venue_address;
		$venue['venueAbout']     = $tblVenue->venue_about;
		$venue['venueAmenities'] = $tblVenue->venue_amenities;
		$venue['venueSigns']     = $tblVenue->venue_signs;
		$venue['venueRating']    = $tblVenue->venue_rating;
		//$venue['venueTimings']   = $tblVenue->venue_timings;

		$venue['currencyCode']    = $tblVenue->currency_code;
		$venue['currencySign']   = $tblVenue->currency_sign;
		//$venue['venueImage']     = ($tblVenue->venue_image)?JUri::base().$tblVenue->venue_image:'';


		$venue['venueImage']     = ($tblVenue->venue_image)?JUri::base().$tblVenue->venue_image:$this->defaultImage;
		$venue['venueVideo']     = ($tblVenue->venue_video_mp4)?JUri::base().$tblVenue->venue_video_mp4:'';

		if(in_array($tblVenue->venue_id, $this->FavouriteVenueIDs))
		{
			$venue['isFavourite']  = 1;
		}
		else
		{
			$venue['isFavourite']  = 0;
		}


		$venue['venueCreated']  = $tblVenue->venue_created;
		$venue['venueModified'] = $tblVenue->venue_modified;

		$venue['isSmart']  = $tblVenue->is_smart;
		$venue['isCasual'] = $tblVenue->is_casual;
		$venue['isFood']   = $tblVenue->is_food;
		$venue['smoking']  = $tblVenue->is_smoking;

		//$venue['rating'] = $tblVenue->rating;
		$venue['city']      = $tblVenue->city;
		$venue['country']   = $tblVenue->country;
		$venue['latitude']  = $tblVenue->latitude;
		$venue['longitude'] = $tblVenue->longitude;

		$venue['isDrink']     = $tblVenue->is_drink;
		$venue['workingDays'] = $tblVenue->working_days;

		if(!empty($tblVenue->from_time) && !empty($tblVenue->to_time))
		{
			$venue['fromTime']    = $this->convertToHM($tblVenue->from_time);
			$venue['toTime']      = $this->convertToHM($tblVenue->to_time);
		}
		else
		{
			$venue['fromTime']    = "";
			$venue['toTime']      = "";
		}

		$venue['latitude']    = $tblVenue->latitude;
		$venue['longitude']   = $tblVenue->longitude;

		$venue['timeStamp']   = $tblVenue->time_stamp;

		return $venue;

	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getVenueTableDetail($tableID)
	{
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblTable = JTable::getInstance('Table', 'BctedTable');
		$tblTable->load($tableID);

		if(!$tblTable->venue_table_id)
		{
			return array();
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('vt.*')
			->from($db->quoteName('#__bcted_venue_table','vt'))
			->where($db->quoteName('vt.venue_table_id') . ' = ' . $db->quote($tableID))
			->where($db->quoteName('vt.venue_table_active') . ' = ' . $db->quote('1'));

		/*$query->select('pt.premium_id,pt.p_table_name')
			->join('LEFT','#__bcted_premium_table AS pt ON pt.venue_table_id=vt.venue_table_id');*/

		// Set the query and load the result.
		$db->setQuery($query);

		$tblTable = $db->loadObject();
		/*echo "call <pre>";
		print_r($result);
		echo "</pre>";
		exit;*/


		$tableData = array();

		$tableData['tableID']         = $tblTable->venue_table_id;
		$tableData['venueID']         = $tblTable->venue_id;

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($tblTable->venue_id);

		$tableData['currencySign'] = $tblVenue->currency_sign;
		$tableData['currencyCode'] = $tblVenue->currency_code;

		$tableData['tableTypeID']      = $tblTable->venue_table_type_id;
		$tableData['userID']           = $tblTable->user_id;
		$tableData['tableName']        = $tblTable->venue_table_name;
		$tableData['customTableName']  = $tblTable->custom_table_name; //Custom Table Name
		$tableData['premiumTableID']   = ($tblTable->premium_table_id)?$tblTable->premium_table_id:"0"; //Custom Table Name
		//$tableData['premiumTableName'] = ($tblTable->p_table_name)?$tblTable->p_table_name:""; //Custom Table Name

		//$tableData['tableImage']      = ($tblTable->venue_table_image)?JUri::base().$tblTable->venue_table_image:'';

		$tableData['tableImage']      = ($tblTable->venue_table_image)?JUri::base().$tblTable->venue_table_image:$this->defaultImage;


		//$tableData['currency']           = $tblTable->venue_table_currency;
		$tableData['price']           = $this->currencyFormat('',$tblTable->venue_table_price);
		$tableData['capacity']        = $tblTable->venue_table_capacity;
		$tableData['description']     = $tblTable->venue_table_description;
		$tableData['tableCreated']    = $tblTable->venue_table_created;
		$tableData['tableModified']   = $tblTable->venue_table_modified;
		$tableData['timeStamp']       = $tblTable->time_stamp;

		//$venue['venueImage']     = ($tblTable->venue_image)?JUri::base().$tblTable->venue_image:'';
		//$venue['active']  = $tblTable->venue_table_active;



		return $tableData;

	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getCompanyDetail($companyID)
	{
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblCompany->load($companyID);

		if(!$tblCompany->company_id)
		{
			return array();
		}

		$company = array();

		$company['companyID']          = $tblCompany->company_id;
		$company['companyName']        = $tblCompany->company_name;
		$company['companyAddress']     = $tblCompany->company_address;
		$company['companyAbout']       = $tblCompany->company_about;
		//$company['companyAmenities'] = $tblCompany->venue_amenities;
		$company['companySigns']       = $tblCompany->company_signs;
		//$company['companyImage']       = ($tblCompany->company_image)?JUri::base().$tblCompany->company_image:'';

		$company['companyImage']       = ($tblCompany->company_image)?JUri::base().$tblCompany->company_image:$this->defaultImage;

		if(in_array($tblCompany->company_id, $this->FavouriteServiceIDs))
		{
			$company['isFavourite']  = 1;
		}
		else
		{
			$company['isFavourite']  = 0;
		}

		if($tblCompany->company_type == 'jet')
		{
			$company['isJetService']  = 1;
		}
		else
		{
			$company['isJetService']  = 0;
		}


		$company['companyCreated']  = $tblCompany->company_created;
		$company['companyRating']   = $tblCompany->company_rating;
		$company['companyModified'] = $tblCompany->company_modified;

		$company['attire']          = $tblCompany->attire;
		$company['restaurant']      = $tblCompany->restaurant;
		$company['smoking']         = $tblCompany->smoking;
		$company['rating']          = $tblCompany->rating;
		$company['price']           = $tblCompany->price;
		$company['drink']           = $tblCompany->drink;

		$company['city']            = $tblCompany->city;
		$company['country']         = $tblCompany->country;
		$company['latitude']        = $tblCompany->latitude;
		$company['longitude']       = $tblCompany->longitude;

		$company['currencySign'] = $tblCompany->currency_sign;
		$company['currencyCode'] = $tblCompany->currency_code;

		$company['latitude']        = $tblCompany->latitude;
		$company['longitude']       = $tblCompany->longitude;

		$company['timeStamp']       = $tblCompany->time_stamp;

		return $company;

	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getCompanyServiceDetail($serviceID, $showCompanyDetail = 0)
	{
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblService = JTable::getInstance('Service', 'BctedTable');

		$tblComapny = JTable::getInstance('Company', 'BctedTable');


		$tblService->load($serviceID);

		if(!$tblService->service_id)
		{
			return array();
		}

		$serviceData = array();

		$serviceData['serviceID']       = $tblService->service_id;
		$serviceData['companyID']       = $tblService->company_id;

		if($showCompanyDetail == 1)
		{
			$tblComapny->load($tblService->company_id);

			if($tblComapny->company_id)
			{
				$serviceData['companyName']  = $tblComapny->company_name;
				$serviceData['companyImage'] = ($tblComapny->company_image)?JUri::base().$tblComapny->company_image:'';
				$serviceData['companyImage'] = ($tblComapny->company_image)?JUri::base().$tblComapny->company_image:$this->defaultImage;

				$serviceData['companySigns'] = $tblComapny->company_signs;

				$serviceData['currencySign'] = $tblComapny->currency_sign;
				$serviceData['currencyCode'] = $tblComapny->currency_code;
				/*$serviceData['attire']       = $tblComapny->attire;
				$serviceData['restaurant']   = $tblComapny->restaurant;
				$serviceData['smoking']      = $tblComapny->smoking;
				$serviceData['rating']       = $tblComapny->rating;
				$serviceData['price']        = $tblComapny->price;
				$serviceData['drink']        = $tblComapny->drink;*/

			}

		}

		if(in_array($tblService->service_id, $this->FavouriteServiceIDs))
		{
			$serviceData['isFavourite']  = 1;
		}
		else
		{
			$serviceData['isFavourite']  = 0;
		}

		$serviceData['userID']          = $tblService->user_id;
		$serviceData['serviceName']     = $tblService->service_name;
		$serviceData['serviceImage']    = ($tblService->service_image)?JUri::base().$tblService->service_image:'';

		$serviceData['serviceImage']    = ($tblService->service_image)?JUri::base().$tblService->service_image:$this->defaultImage;
		$serviceData['serviceRating']           = $tblService->service_rating;

		$serviceData['price']           = $this->currencyFormat('',$tblService->service_price) . '/hr';
		$serviceData['description']     = $tblService->service_description;
		$serviceData['serviceCreated']  = $tblService->service_created;
		$serviceData['serviceModified'] = $tblService->service_modified;
		$serviceData['timeStamp']       = $tblService->time_stamp;

		return $serviceData;

	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function formatProfileData($userProfile,$hasSetting = 0)
	{
		$userDetail = array();

		$phone = $userProfile->phoneno;
		$onlyPhone = substr($phone, -10);
		$countryCode = substr($phone, 0, -10);

		$userDetail['userID']    = $userProfile->id;
		$userDetail['firstName'] = $userProfile->name;
		$userDetail['username']  = $userProfile->username;
		$userDetail['email']     = $userProfile->email;
		//$userDetail['profileID']    = $userProfile->profile_id;
		//$userDetail['isPremium']    = $userProfile->is_premium_member;

		//$userDetail['firstName']       = $userProfile->heartdart_status;
		$userDetail['lastName'] = $userProfile->last_name;
		$userDetail['phoneno']  = $userProfile->phoneno;
		//$userDetail['avatar']   = $userProfile->avatar;
		$userDetail['about']    = $userProfile->about;
		$userDetail['city']    = $userProfile->city;

		/*if(!$this->IJUserID)
		{
			$userDetail['isFriend']    = "0";
			$userDetail['isFollowing'] = "0";
		}
		else if($this->IJUserID == $userProfile->id)
		{
			$userDetail['isFriend']    = "0";
			$userDetail['isFollowing'] = "0";

		}
		else
		{
			$userDetail['isFriend']    = (string)$this->checkForFriend($this->IJUserID, $userProfile->id);
			$userDetail['isFollowing'] = (string)$this->isFollowing($this->IJUserID, $userProfile->id);
		}*/



		//$userDetail['avatar']     = ($userProfile->avatar)?JUri::root().$userProfile->avatar:$this->defaultUserAvatar;
		$userDetail['avatar']     = ($userProfile->avatar)?JUri::root().$userProfile->avatar:'';

		//echo JUri::root().$userProfile->avatar;exit;

		$userDetail['avatar']     = ($userProfile->avatar)?JUri::root().$userProfile->avatar:$this->defaultImage;


		//$userDetail['coverImage'] = ($userProfile->cover)?JUri::root().$userProfile->cover:$this->defaultUserCover;

		if($hasSetting)
		{
			$params = json_decode($userProfile->params);

			/*echo "<pre>";
			print_r($userProfile);
			echo "</pre>";
			exit;*/

			if(isset($params->settings->social->connectToFacebook))
			{
				$userDetail['settings']['connectToFacebook']     = (string) $params->settings->social->connectToFacebook;
			}

			if(isset($params->settings->pushNotification->receiveRequest))
			{
				$userDetail['settings']['receiveRequest']     = (string) $params->settings->pushNotification->receiveRequest;
			}

			$userDetail['settings']['receiveMessage']        = (string) $params->settings->pushNotification->receiveMessage;
			$userDetail['settings']['updateMyBookingStatus'] = (string) $params->settings->pushNotification->updateMyBookingStatus;
		}


		return $userDetail;
	}

	public function getUserGroup($userID)
	{
		$bctedConfig = $this->getExtensionParam();

		$user = JFactory::getUser($userID);

		$groups = $user->get('groups');

		if(in_array($bctedConfig->club, $groups))
		{
			return 'Club';
		}
		else if(in_array($bctedConfig->service_provider, $groups))
		{
			return 'ServiceProvider';
		}
		else if(in_array($bctedConfig->guest, $groups))
		{
			return  'Registered';
		}

		return 'Registered';


	}


	private function getExtensionParam()
	{
		$app    = JFactory::getApplication();

		$option = "com_bcted";
		$db     = JFactory::getDbo();

		$option = '%' . $db->escape($option, true) . '%';

		// Initialiase variables.
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->qn('#__extensions'))
			->where($db->qn('name') . ' LIKE ' . $db->q($option))
			->where($db->qn('type') . ' = ' . $db->q('component'))
			->order($db->qn('ordering') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadObject();

			$params = json_decode($result->params);
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), 500);
		}

		return $params;
	}

	public function getExtensionParamCore()
	{
		$app    = JFactory::getApplication();

		$option = "com_bcted";
		$db     = JFactory::getDbo();

		$option = '%' . $db->escape($option, true) . '%';

		// Initialiase variables.
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->qn('#__extensions'))
			->where($db->qn('name') . ' LIKE ' . $db->q($option))
			->where($db->qn('type') . ' = ' . $db->q('component'))
			->order($db->qn('ordering') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadObject();

			$params = json_decode($result->params);
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), 500);
		}

		return $params;
	}

	public function getLiveUsers($userID = 0)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('id')
			->from($db->quoteName('#__users'))
			->where($db->quoteName('block') . ' = ' . $db->quote('0'));

		if($userID)
		{
			$query->where($db->quoteName('id') . ' <> ' . $db->quote($userID));
		}

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$liveUsers = $db->loadColumn();

			return $liveUsers;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		$liveUsers = array();

		return $liveUsers;
	}



	public function getUserProfileID($userID)
	{
		// Initialiase variables.
		$profileID = 0;
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('profile_id')
			->from($db->quoteName('#__bcted_user_profile'))
			->where($db->quoteName('userid') . ' = ' . $db->quote($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		$profileID = $db->loadResult();

		return $profileID;
	}

	public function updateAverageRatingOfVenue($venueID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('rating')
			->from($db->quoteName('#__bcted_ratings'))
			->where($db->quoteName('rating_type') . ' = ' . $db->quote('venue'))
			->where($db->quoteName('rated_id') . ' = ' . $db->quote($venueID));

		// Set the query and load the result.
		$db->setQuery($query);

		$rating = $db->loadColumn();

		$sum = array_sum($rating);

		$totalEntry = count($rating);

		$avg = $sum / $totalEntry;

		$avg = number_format($avg , 2);

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblVenue->load($venueID);
		$tblVenue->venue_rating = $avg;
		//$tblVenue->rating = "1";

		$tblVenue->store();

	}

	public function updateAverageRatingOfCompany($companyID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('rating')
			->from($db->quoteName('#__bcted_ratings'))
			->where($db->quoteName('rating_type') . ' = ' . $db->quote('service'))
			->where($db->quoteName('rated_id') . ' = ' . $db->quote($companyID));

		// Set the query and load the result.
		$db->setQuery($query);

		$rating = $db->loadColumn();

		$sum = array_sum($rating);

		$totalEntry = count($rating);

		$avg = $sum / $totalEntry;

		$avg = number_format($avg , 2);

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblCompany->load($companyID);
		$tblCompany->company_rating = $avg;
		//$tblCompany->rating = "1";
		/*echo "<pre>";
		print_r($tblCompany);
		echo "</pre>";
		exit;*/
		$tblCompany->store();

	}

	public function updateAverageRatingOfService($serviceID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('rating')
			->from($db->quoteName('#__bcted_ratings'))
			->where($db->quoteName('rating_type') . ' = ' . $db->quote('service'))
			->where($db->quoteName('rated_id') . ' = ' . $db->quote($serviceID));

		// Set the query and load the result.
		$db->setQuery($query);

		$rating = $db->loadColumn();

		$sum = array_sum($rating);

		$totalEntry = count($rating);

		$avg = $sum / $totalEntry;

		$avg = number_format($avg , 2);

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblService = JTable::getInstance('Service', 'BctedTable');
		$tblService->load($serviceID);
		$tblService->service_rating = $avg;
		//$tblService->rating = "1";

		$tblService->store();

	}


	public function updateUserMessageReceivedCount($profileID)
	{
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_heartdart/tables');
		$tblProfile = JTable::getInstance('Profile', 'HeartdartTable');
		$tblProfile->load($profileID);
		$tblProfile->total_msg_received = $tblProfile->total_msg_received + 1;
		$tblProfile->store();
	}

	public function getStatusIDFromStatusName($statusName)
	{
		if(empty($statusName))
		{
			return 0;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_status'))
			->where($db->quoteName('status') . ' = ' . $db->quote($statusName));

		/*echo $query->dump();
		exit;*/

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObject();

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/

		return $result->id;

	}

	public function getStatusNameFromStatusID($statusID)
	{
		if(!$statusID)
		{
			return '';
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_status'))
			->where($db->quoteName('id') . ' = ' . $db->quote($statusID));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObject();

		return $result->status;

	}


	function ffmpegPath()
	{
		$output = shell_exec("whereis ffmpeg");
		$outputArray = explode(" ", $output);

		if(isset($outputArray[1]) && strlen($outputArray[1])!=0)
		{
			//return $outputArray[1];
			//return "/usr/bin/ffmpeg";
			return "ffmpeg";
		}

		return "";

	}

	/**
	 * Convert a Video File into specified format according to the output file name
	 * If $videoOut is a path, a random file name will be generated
	 *
	 * @params string $videoIn video input path (including file name)
	 * @params string $videoOut video output path (file name optinal)
	 * @params string $videoSize in widthxheight format or any ffmpeg accepted format eg hd480
	 * @return string video file name or false if failed
	 * @since Jomsocial 1.2.0
	 */
	public function convertVideo($videoIn, $videoOut, $videoSize = '400x300', $deleteOriginal = false)
	{
		jimport('joomla.filesystem.file');
		jimport('joomla.utilities.utility');
		jimport( 'joomla.user.helper' );
		$qscale = "1";
		if (!JFile::exists($videoIn))
			return false;

		if (JFile::exists($videoOut)) {
			$videoFullPath = JFile::makeSafe($videoOut);
			$videoFileName = JFile::getName($videoFullPath);

			$videoFileName = 'ijadv_'.$videoFileName;


		} else {

			// It is a directory, not a file. Assigns file name
			//CFactory::load( 'helpers' , 'file' );

			//$videoFileName =  CFileHelper::getRandomFilename($videoOut, '', 'flv');
			$videoFileName = JApplication::getHash(JUserHelper::genRandomPassword(5) . time());
			$videoFileName = 'ijadv_'.$videoFileName;

			$videoFullPath = $videoOut .'/'. $videoFileName.".mp4";

			$fileExtenstion = JFile::getExt($videoFullPath);
			$returnFileName = $videoFileName.".".$fileExtenstion;
		}

		$videoFileNameWebm = JFile::getName($videoFullPath);

		//$videoFileNameWebm = JFile::getName($videoFullPath);
		// Build the ffmpeg command
		/*$cmd 	= array();
		$cmd[]	= $this->ffmpeg;
		$cmd[]	= '-y -i ' . $videoIn;
		$cmd[]	= '-vf "transpose=1"';
		$cmd[]	= '-g 30'; //group of picture size, for video streaming
		$cmd[]	= '-qscale ' . $qscale;
		$cmd[]	= '-vcodec flv -f flv -ar 44100';
		//$cmd[]	= '-s ' . $videoSize;
		//$cmd[]	= $config->get('customCommandForVideo');
		$cmd[]	= "";
		$cmd[]	= $videoFullPath;
		$cmd[]	= '2>&1';

		$command = implode(' ', $cmd);*/

		/*$cmd 	= array();
		$cmd[]	= $this->ffmpeg;
		$cmd[]	= '-y -i ' . $videoIn;
		$cmd[]	= '-vf "transpose=1"';
        $cmd[]	= '-vcodec libx264 -acodec aac -strict -2 -preset veryfast';
        //$cmd[]	= '-acodec aac -ab 64k';
		//$cmd[]  = '-strict -2';
		//$cmd[]  = '-crf 23';
		//$cmd[]	= '-s ' . $videoSize;
		//$cmd[]	= $config->get('customCommandForVideo');
		$cmd[]	= $videoFullPath;
		$cmd[]	= '2>/home/wwwijoo/public_html/heartdarts/outtwo.txt';
		$command = implode(' ', $cmd);*/

		$command = "ffmpeg -i ".$videoIn." -vf \"transpose=1\" -y -vcodec libx264 -s qvga -acodec libfaac -ab 96k -ac 2 -b 200K -threads 4 -flags +loop -cmp +chroma -partitions 0 -me_method epzs -subq 1 -trellis 0 -refs 1 -coder 0 -me_range 16 -g 300 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -maxrate 10M -bufsize 10M -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -level 30 ".$videoFullPath;

		$cmdOut	= $this->_runCommand($command);

		if (JFile::exists($videoFullPath) && filesize($videoFullPath) > 0)
		{
			if ($deleteOriginal)
			{
				JFile::delete($videoIn);
			}
			return $returnFileName;
		}
		else
		{
			//if ($this->debug)
			//{
				//echo '<pre>FFmpeg could not convert videos</pre>';
				//echo '<pre>' . $command . '</pre>';
				//echo '<pre>' . $cmdOut . '</pre>';
			//}
			return false;
		}
	}

	/**
	 * Convert a Video File into specified format according to the output file name
	 * If $videoOut is a path, a random file name will be generated
	 *
	 * @params string $videoIn video input path (including file name)
	 * @params string $videoOut video output path (file name optinal)
	 * @params string $videoSize in widthxheight format or any ffmpeg accepted format eg hd480
	 * @return string video file name or false if failed
	 * @since Jomsocial 1.2.0
	 */
	public function convertVideo2($videoIn, $videoOut, $videoSize = '400x300', $deleteOriginal = false)
	{
		jimport('joomla.filesystem.file');
		jimport('joomla.utilities.utility');
		jimport( 'joomla.user.helper' );
		$qscale = "11";

		if (!JFile::exists($videoIn)){
			/*echo "call file not found";
			exit;*/
			return false;
		}

		if (JFile::exists($videoOut)) {
			$videoFullPath = JFile::makeSafe($videoOut);
			$videoFileName = JFile::getName($videoFullPath);
		} else {

			// It is a directory, not a file. Assigns file name
			//CFactory::load( 'helpers' , 'file' );

			//$videoFileName =  CFileHelper::getRandomFilename($videoOut, '', 'flv');
			$videoFileName = JApplication::getHash(JUserHelper::genRandomPassword(5) . time());

			$videoFullPath = $videoOut .'/'. $videoFileName.".flv";
			$videoMp4Path = $videoOut .'/'. $videoFileName.'.mp4';

			$fileExtenstion = JFile::getExt($videoMp4Path);
			$returnFileName = $videoFileName.".".$fileExtenstion;
		}

		/*echo $videoFullPath;
		exit;*/
		$videoFileNameWebm = JFile::getName($videoFullPath);

		//$videoFileNameWebm = JFile::getName($videoFullPath);
		// Build the ffmpeg command
		$cmd 	= array();
		$cmd[]	= $this->ffmpeg;
		$cmd[]	= '-y -i ' . $videoIn;
		$cmd[]	= '-g 30'; //group of picture size, for video streaming
		//$cmd[]	= '-q ' . $qscale;
		$cmd[]	= '-vcodec flv -f flv -ar 44100';
		$cmd[]	= '-s ' . $videoSize;
		//$cmd[]	= $config->get('customCommandForVideo');
		$cmd[]	= "";
		$cmd[]	= $videoFullPath;
		//$cmd[]	= '2>/var/www/html/dev/ffmpeg_test.txt';
		$cmd[] = '2>/var/www/development/thebeseated/ffmpeg_test.txt';

		$command = implode(' ', $cmd);

		$cmdOut	= $this->_runCommand($command);




		$command = "/usr/bin/ffmpeg -i ".$videoFullPath." -vf \"transpose=1\" -ar 22050 ".$videoMp4Path; //Wroking
		$cmdOut	= $this->_runCommand($command);

		/*echo $cmdOut;
		exit;*/

		if (JFile::exists($videoFullPath) && filesize($videoFullPath) > 0)
		{
			if ($deleteOriginal)
			{
				JFile::delete($videoIn);
			}
			return $returnFileName;
		}
		else
		{
			//if ($this->debug)
			//{
				/*echo '<pre>FFmpeg could not convert videos</pre>';
				echo '<pre>' . $command . '</pre>';
				echo '<pre>' . $cmdOut . '</pre>';*/
			//}
			return false;
		}
	}

	public function _runCommand($command)
	{
		$output		= null;
		$return_var = null;

		if ($this->execFunction == null)
		{
			$disableFunctions	= explode(',', ini_get('disable_functions'));
			$execFunctions		= array('passthru', 'exec', 'shell_exec', 'system');

			foreach ($execFunctions as $execFunction)
			{
				if (is_callable($execFunction) && function_exists($execFunction) && !in_array($execFunction, $disableFunctions))
				{
					$this->execFunction = $execFunction;
					break;
				}
			}
		}

		switch ($this->execFunction)
		{
			case 'passthru':
				ob_start();
				@passthru($command, $return_var);
				$output = ob_get_contents();
				ob_end_clean();
				break;
			case 'exec':
				@exec($command, $output, $return_var);
				$output	= implode("\r\n", $output);
				break;
			case 'shell_exec':
				$output	= @shell_exec($command);
				break;
			case 'system':
				ob_start();
				@system($command, $return_var);
				$output = ob_get_contents();
				ob_end_clean();
				break;
			default:
				$output	= false;
		}

		// for debug use
		//print_r($disableFunctions);
		//echo '<br />' . $this->execFunction;
		//echo '<br />' . $output;
		//exit;

		return $output;
	}

	/*
	 * Create Thumbnail for a video file
	 *
	 * @params string $videoFile existing video's path + file name
	 * @params string $thumbFile new thumbnail's folder or filename
	 * @params string $videoFrame decide which frame to be taken as thumbnail
	 * @params string $thumbsize height x width of the thumbnail
	 * @return thumbnail's filename or false if failed
	 * @since Jomsosial 1.2.0
	 */
	public function createVideoThumb($videoFile, $thumbFile, $thumbSize='600x500')
	{
		jimport('joomla.filesystem.file');
		jimport('joomla.utilities.utility');
		jimport( 'joomla.user.helper' );

		$videoInfor = $this->getVideoInfo($videoFile);

		if (!JFile::exists($videoFile))
		{
			return false;
		}
		if (JFile::exists($thumbFile))
		{
			$thumbFullPath = JFile::makeSafe($thumbFile);
			$thumbFileName = JFile::getName($thumbFullPath);
		} else {
			//CFactory::load( 'helpers' , 'file' );

			$thumbFileName = JApplication::getHash(JUserHelper::genRandomPassword(5) . time());
			$thumbFileName = $thumbFileName.".jpg";
			//$thumbFileName =  CFileHelper::getRandomFilename($thumbFile, '', 'jpg');
			$thumbFullPath = JPath::clean($thumbFile .'/'. $thumbFileName);
		}

		$cmd	= $this->ffmpeg . ' -i ' . $videoFile . ' -ss ' . $videoFrame . ' -t 00:00:01 -s ' . $thumbSize . ' -r 1 -f mjpeg ' . $thumbFullPath;
		$cmdOut = $this->_runCommand($cmd);

		if (JFile::exists($thumbFullPath) && (filesize($thumbFullPath) > 0))
		{
			return $thumbFileName;
		}

		$cmd	= $this->ffmpeg . ' -i ' . $videoFile . ' -vcodec mjpeg -vframes 1 -an -f rawvideo ' .  $thumbFullPath;
		$cmdOut = $this->_runCommand($cmd);

		if (JFile::exists($thumbFullPath) && (filesize($thumbFullPath) > 0))
		{
			return $thumbFileName;
		} else {
			if ($this->debug)
			{
				echo '<pre>FFmpeg could not create video thumbs</pre>';
				echo '<pre>' . $cmd . '</pre>';
				echo '<pre>' . $cmdOut . '</pre>';
				if (!$cmdOut) { echo '<pre>Check video thumb folder\'s permission.</pre>'; }
			}
			return false;
		}
	}

	/*
	 * Return Video's information
	 * bitrate, duration, video and frame properties
	 *
	 * @params string $videoFilePath path to the Video
	 * @return array of video's info
	 * @since Jomsocial 1.2.0
	 */
	public function getVideoInfo($videoFile, $cmdOut = '')
	{
		$data = array();

		if (!is_file($videoFile) && empty($cmdOut))
			return $data;

		if (!$cmdOut) {
			//$cmd	= $this->converter . ' -v 10 -i ' . $videoFile . ' 2>&1';
			// Some FFmpeg version only accept -v value from -2 to 2
			$cmd	= $this->ffmpeg . ' -i ' . $videoFile . ' 2>&1';



			$cmdOut	= $this->_runCommand($cmd);
		}



		if (!$cmdOut) {
			return $data;
		}



		preg_match_all('/Duration: (.*)/', $cmdOut , $matches);



		if (count($matches) > 0 && isset($matches[1][0]))
		{
			//CFactory::load( 'helpers' , 'videos' );

			$parts = explode(', ', trim($matches[1][0]));

			$data['bitrate']			= intval(ltrim($parts[2], 'bitrate: '));
			$data['duration']['hms']	= substr($parts[0], 0, 8);
			$data['duration']['exact']	= $parts[0];
			$data['duration']['sec']	= $videoFrame = $this->formatDuration($data['duration']['hms'], 'seconds');
			$data['duration']['excess']	= intval(substr($parts[0], 9));
		}
		else
		{
			if ($this->debug) {
				echo '<pre>FFmpeg failed to read video\'s duration</pre>';
				echo '<pre>' . $cmd . '<pre>';
				echo '<pre>' . $cmdOut . '</pre>';
			}
			return false;
		}

		preg_match('/Stream(.*): Video: (.*)/', $cmdOut, $matches);
		if (count($matches) > 0 && isset($matches[0]) && isset($matches[2]))
		{
			$data['video']	= array();

			preg_match('/([0-9]{1,5})x([0-9]{1,5})/', $matches[2], $dimensions_matches);
			$data['video']['width']		= floatval($dimensions_matches[1]);
			$data['video']['height']	= floatval($dimensions_matches[2]);

			preg_match('/([0-9\.]+) (fps|tb)/', $matches[0], $fps_matches);

			if (isset($fps_matches[1]))
				$data['video']['frame_rate']= floatval($fps_matches[1]);

			preg_match('/\[PAR ([0-9\:\.]+) DAR ([0-9\:\.]+)\]/', $matches[0], $ratio_matches);
			if(count($ratio_matches))
			{
				$data['video']['pixel_aspect_ratio']	= $ratio_matches[1];
				$data['video']['display_aspect_ratio']	= $ratio_matches[2];
			}

			if (!empty($data['duration']) && !empty($data['video']))
			{
				$data['video']['frame_count'] = ceil($data['duration']['sec'] * $data['video']['frame_rate']);
				$data['frames']				= array();
				$data['frames']['total']	= $data['video']['frame_count'];
				$data['frames']['excess']	= ceil($data['video']['frame_rate'] * ($data['duration']['excess']/10));
				$data['frames']['exact']	= $data['duration']['hms'] . '.' . $data['frames']['excess'];
			}

			$parts			= explode(',', $matches[2]);
			$other_parts	= array($dimensions_matches[0], $fps_matches[0]);

			$formats = array();
			foreach ($parts as $key => $part)
			{
				$part = trim($part);
				if (!in_array($part, $other_parts))
					array_push($formats, $part);
			}
			$data['video']['pixel_format']	= $formats[1];
			$data['video']['codec']			= $formats[0];
		}

		return $data;
	}

	public function formatDuration($duration = 0, $format = 'HH:MM:SS')
	{
		if ($format == 'seconds' || $format == 'sec') {
			$arg = explode(":", $duration);

			$hour	= isset($arg[0]) ? intval($arg[0]) : 0;
			$minute	= isset($arg[1]) ? intval($arg[1]) : 0;
			$second	= isset($arg[2]) ? intval($arg[2]) : 0;

			$sec = ($hour*3600) + ($minute*60) + ($second);
			return (int) $sec;
		}

		if ($format == 'HH:MM:SS' || $format == 'hms') {
			$timeUnits = array
			(
				'HH' => $duration / 3600 % 24,
				'MM' => $duration / 60 % 60,
				'SS' => $duration % 60
			);

			$arg = array();
			foreach ($timeUnits as $timeUnit => $value) {
				$arg[$timeUnit] = ($value > 0) ? $value : 0;
			}

			$hms = '%02s:%02s:%02s';
			$hms = sprintf($hms, $arg['HH'], $arg['MM'], $arg['SS']);
			return $hms;
		}
	}

	public function getMessageConnectionOnly($fromUser,$toUser)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_message_connection'))
			->where($db->quoteName('from_userid') . ' IN (' . $fromUser.','.$toUser .')')
			->where($db->quoteName('to_userid') . ' IN (' . $fromUser.','.$toUser .')');

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObject();

		if($result)
			return $result->connection_id;
		else
			return 0;
	}

	public function sendMessage($venueID,$companyID,$serviceID,$tableID,$TouserID,$message,$extraParam = array(),$messageType='tableaddme')
	{
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblMessage = JTable::getInstance('Message', 'BctedTable');
		$tblMessage->load(0);

		$loginUser = JFactory::getUser();

		$data = array();

		$data['venue_id']   = $venueID;
		$data['company_id'] = $companyID;
		$data['table_id']   = $serviceID;
		$data['service_id'] = $tableID;
		$data['userid']     = $TouserID;
		$data['to_userid']  = $TouserID;
		$data['message']    = $message;
		if(count($extraParam)!=0)
		{
			$data['extra_params'] = json_encode($extraParam);
		}
		else
		{
			$data['extra_params'] = "";
		}

		$data['message_type']    = $messageType;
		$data['created']    = date('Y-m-d H:i:s');
		$data['time_stamp'] = time();

		$data['from_userid'] = $loginUser->id;

		$connectionID = $this->getMessageConnection($data['from_userid'],$data['to_userid']);
		if(!$connectionID)
		{
			return 0;
		}

		$data['connection_id'] = $connectionID;
		$tblMessage->bind($data);

		$tblMessage->store();

		if($messageType == 'noshow')
		{
			return $connectionID;
		}
	}


	public function getMessageConnection($fromUser,$toUser)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_message_connection'))
			->where($db->quoteName('from_userid') . ' IN (' . $fromUser.','.$toUser .')')
			->where($db->quoteName('to_userid') . ' IN (' . $fromUser.','.$toUser .')');

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObject();

		if($result)
		{
			return $result->connection_id;
		}
		else
		{
			JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
			$tblMessageconnection = JTable::getInstance('Messageconnection', 'BctedTable');

			/*echo "<pre>";
			print_r($tblMessageconnection);
			echo "</pre>";*/
			$data = array();
			$data['from_userid'] = $fromUser;
			$data['to_userid']   = $toUser;

			$tblMessageconnection->load(0);
			$tblMessageconnection->bind($data);
			if($tblMessageconnection->store())
			{
				return $tblMessageconnection->connection_id;
			}
			else
			{
				return 0;
			}
		}


	}

	public function isTime($time,$is24Hours=true,$seconds=false)
	{
		$pattern = "/^".($is24Hours ? "([1-2][0-3]|[01]?[1-9])" : "(1[0-2]|0?[1-9])").":([0-5]?[0-9])".($seconds ? ":([0-5]?[0-9])" : "")."$/";
		if (preg_match($pattern, $time))
		{
		    return true;
		}
		return false;
	}

	public function getAddressFromLatLong($lat, $long, $country = 1, $fullAddress = 0)
	{
		$geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$long.'&sensor=false');
		$object = json_decode($geocode);
		$status = trim($object->status);
		$getResult = "";

		if($status == 'OK')
		{
			foreach ($object->results as $key => $result)
			{
				if($country)
				{
					$addressCmp = $result->address_components;
					//echo "Key : " . $key . "<br />";

					for($i = count($addressCmp)-1; $i >= 0; $i--)
					{
						$singleCmp = $addressCmp[$i];
						//echo "i : " . $i . "<br />";

						if(in_array("country", $singleCmp->types))
						{
							$getResult = $singleCmp->long_name;
							break;
						}
					}

					if(!empty($getResult))
					{
						break;
					}
				}

				if($fullAddress)
				{
					$getResult = $result->formatted_address;

					if(!empty($getResult))
					{
						break;
					}
				}
			}
		}

		return $getResult;
	}

	/**
	 *  Method to get a table object, load it if necessary.
	 *
	 * @param   integer  $userID  User ID.
	 *
	 * @return JTable A JTable object
	 *
	 * @since    0.0.1
	 */
	public function getBlackList($elementID, $elementType)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('user_id')
			->from($db->quoteName('#__bcted_blacklist'))
			->where($db->quoteName('element_id') . ' = ' . $db->quote($elementID))
			->where($db->quoteName('element_type') . ' = ' . $db->quote($elementType));

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$users = $db->loadColumn();

			if($users)
				return $users;
			else
				return 0;
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}
	}

	function convertToHMS($time)
	{
		$timeFormat = explode(":", $time);

		if(count($timeFormat)==1)
		{
			$hmsTime = $timeFormat[0].":"."00:00";
		}
		else if(count($timeFormat)==2)
		{
			$hmsTime = $timeFormat[0].":".$timeFormat[1].":00";
		}
		else if(count($timeFormat)>=3)
		{
			$hmsTime = $timeFormat[0].":".$timeFormat[1].":".$timeFormat[2];
		}

		return $hmsTime;
	}

	function convertToHM($time)
	{
		$timeFormat = explode(":", $time);

		if(count($timeFormat)==1)
		{
			$hmsTime = $timeFormat[0].":"."00";
		}
		else if(count($timeFormat)==2)
		{
			$hmsTime = $timeFormat[0].":".$timeFormat[1];
		}
		else if(count($timeFormat)>=3)
		{
			$hmsTime = $timeFormat[0].":".$timeFormat[1];
		}

		return $hmsTime;
	}

	public function getAdministratorUsersEmail()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$queryGoup = $db->getQuery(true);

		$groups =array();
		$groups[] = 6;
		$groups[] = 7;
		$groups[] = 8;

		// Create the base select statement.
		$queryGoup->select('user_id')
			->from($db->quoteName('#__user_usergroup_map'))
			->where($db->quoteName('group_id') . ' IN ('.implode(",", $groups).')');

		// Set the query and load the result.
		$db->setQuery($queryGoup);
		$userIDS = $db->loadColumn();
		/*echo $queryGoup->dump()."<pre>";
		print_r($userIDS);
		echo "</pre>";
		exit;*/

		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('email')
			->from($db->quoteName('#__users'))
			->where($db->quoteName('id') . ' IN (' . implode(",", $userIDS) .')');

		// Set the query and load the result.
		$db->setQuery($query);
		$emails = array();
		try
		{
			$emails = $db->loadColumn();
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		/*echo "<pre>";
		print_r($emails);
		echo "</pre>";
		exit;*/
		return $emails;

	}

	function differenceInHours($start, $end)
	{
		$currentDate = date('Y-m-d');
		$nextDate = date('Y-m-d', strtotime(' +1 day'));

		if (strtotime($start)>strtotime($end)) {
			//start date is later then end date
			//end date is next day
			$s = new DateTime($currentDate.' '.$start);
			$e = new DateTime($nextDate.' '.$end);
		} else {
			//start date is earlier then end date
			//same day
			$s = new DateTime($currentDate.' '.$start);
			$e = new DateTime($currentDate.' '.$end);
		}

		$dateDiff = date_diff($s, $e);

		//$hour = $dateDiff->h;
		/*echo "<pre>";
		print_r($dateDiff);
		echo "</pre>";
		exit;*/

		if($dateDiff->h == 0)
		{
			$hour = 1;
		}

		if($dateDiff->h)
		{
			$hour = $dateDiff->h;
		}

		if($dateDiff->h ==0 && $dateDiff->i > 0)
		{
			$hour = 1;
		}else if($dateDiff->i > 0)
		{
			$hour = $hour+1;
		}

		/*echo $hour;
		exit;*/
		return $hour;

	}

	public function filterEmails($emailString)
	{
		$seletedEmails = explode(",", $emailString);
		$checkForValid = 0;
		if(count($seletedEmails)!=0)
		{
			$checkForValid = 1;
		}
		$seletedEmails = implode("','", $seletedEmails);
		$seletedEmails = "'".$seletedEmails."'";

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('id')
			->from($db->quoteName('#__users'))
			->where($db->quoteName('block') . ' = ' . $db->quote('0'))
			->where($db->quoteName('email') . ' IN ('.$seletedEmails.')');

		// Set the query and load the result.
		$db->setQuery($query);
		$userids = $db->loadColumn();

		$guestEmails   = array();
		$serviceEmails = array();
		$venueEmails   = array();
		$allRegEmail   = array();

		$invalidEmailflg = 0;

		for ($i = 0; $i < count($userids); $i++)
		{
			/*if($userids[$i] != $this->IJUserID)
		    {*/
				$query1 = $db->getQuery(true);

				// Create the base select statement.
				$query1->select('a.user_type_for_push,b.email')
					->from($db->quoteName('#__bcted_user_profile').'AS a')
					->where($db->quoteName('a.userid') . ' = ' . $db->quote($userids[$i]))
					->join('INNER', '#__users AS b ON b.id=a.userid');

				// Set the query and load the result.
				$db->setQuery($query1);
				$userData = $db->loadObject();

				$allRegEmail[] = $userData->email;

				if($userData->user_type_for_push == 'guest')
				{
					$guestEmails[] = $userData->email;
				}

				if($userData->user_type_for_push == 'service')
				{
					$serviceEmails[] = $userData->email;
				}

				if($userData->user_type_for_push == 'venue')
				{
					$venueEmails[] = $userData->email;
				}
			//}
		}

		$foundEmails                = array();
		$foundEmails['guest']       = $guestEmails;
		$foundEmails['service']     = $serviceEmails;
		$foundEmails['venue']       = $venueEmails;
		$foundEmails['allRegEmail'] = $allRegEmail;

		return $foundEmails;
	}

	public function getExtension($str) {
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}

	public function createThumb($mainImage,$thumbName,$new_w = 500,$new_h = 500)
	{
		//get image extension.
		$ext=$this->getExtension($mainImage);
		//creates the new image using the appropriate function from gd library
		if(!strcmp("jpg",$ext) || !strcmp("jpeg",$ext))
			$src_img=imagecreatefromjpeg($mainImage);

		if(!strcmp("png",$ext))
			$src_img=imagecreatefrompng($mainImage);

		//gets the dimmensions of the image
		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);

		// next we will calculate the new dimmensions for the thumbnail image
		// the next steps will be taken:
		// 1. calculate the ratio by dividing the old dimmensions with the new ones
		// 2. if the ratio for the width is higher, the width will remain the one define in WIDTH variable
		// and the height will be calculated so the image ratio will not change
		// 3. otherwise we will use the height ratio for the image
		// as a result, only one of the dimmensions will be from the fixed ones
		$ratio1=$old_x/$new_w;
		$ratio2=$old_y/$new_h;

		if($ratio1>$ratio2) {
			$thumb_w=$new_w;
			$thumb_h=$old_y/$ratio1;
		}
		else {
			$thumb_h=$new_h;
			$thumb_w=$old_x/$ratio2;
		}

		// we create a new image with the new dimmensions
		$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);

		// resize the big image to the new created one
		imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

		// output the created image to the file. Now we will have the thumbnail into the file named by $filename
		if(!strcmp("png",$ext))
			imagepng($dst_img,$thumbName);
		else
			imagejpeg($dst_img,$thumbName);

		//destroys source and destination images.
		imagedestroy($dst_img);
		imagedestroy($src_img);
	}

	public function is_booking_in_venue($venue_id)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();

		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venue_id))
			->where($db->quoteName('status') . ' = ' . $db->quote(5));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		if(count($result)!=0)
		{
			return 1;
		}


		$query = $db->getQuery(true);

		$statusArray = array(1,9);


		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venue_id))
			->where($db->quoteName('status') . ' IN (' . implode(",", $statusArray) .')');

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		$pastCount = 0;

		foreach ($result as $key => $value)
		{
			$bookingTime = strtotime($value->venue_booking_datetime);
			$currentTime = strtotime(date('Y-m-d'));

			if($currentTime > $bookingTime)
			{
				$pastCount = $pastCount + 1;
			}
		}

		if(count($result) == $pastCount)
		{
			return 0;
		}

		return 1;
	}

	public function is_booking_in_company($company_id)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();

		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_service_booking'))
			->where($db->quoteName('company_id') . ' = ' . $db->quote($company_id))
			->where($db->quoteName('status') . ' = ' . $db->quote(5));

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		if(count($result)!=0)
		{
			return 1;
		}


		$query = $db->getQuery(true);

		$statusArray = array(1,9);


		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_service_booking'))
			->where($db->quoteName('company_id') . ' = ' . $db->quote($company_id))
			->where($db->quoteName('status') . ' IN (' . implode(",", $statusArray) .')');

		// Set the query and load the result.
		$db->setQuery($query);

		$result = $db->loadObjectList();

		$pastCount = 0;

		foreach ($result as $key => $value)
		{
			$bookingTime = strtotime($value->venue_booking_datetime);
			$currentTime = strtotime(date('Y-m-d'));

			if($currentTime > $bookingTime)
			{
				$pastCount = $pastCount + 1;
			}
		}

		if(count($result) == $pastCount)
		{
			return 0;
		}

		return 1;
	}

	public function checkForVenueTableAvaibility($venueID,$tableID,$date,$fromTime,$toTime)
	{
		$currentBookingDate = $date; //$tblVenuebooking->venue_booking_datetime;
		$datesArray         = array();
		$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' -1 day'));
		$datesArray[]       = $date; //$tblVenuebooking->venue_booking_datetime;
		$datesArray[]       = date('Y-m-d', strtotime($currentBookingDate .' +1 day'));
		$currentBookingFrom = $fromTime; // $tblVenuebooking->booking_from_time;
		$currentBookingTo   = $toTime; //$tblVenuebooking->booking_to_time;

		if (strtotime($currentBookingFrom)>strtotime($currentBookingTo))
		{
			$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
			$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo.' +1 day'));
		} else {
			$currentBookingFrom = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingFrom));
			$currentBookingTo = date('Y-m-d H:i:s', strtotime($currentBookingDate .' '.$currentBookingTo));
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_venue_booking'))
			->where($db->quoteName('venue_id') . ' = ' . $db->quote($venueID))
			->where($db->quoteName('venue_table_id') . ' = ' . $db->quote($tableID))
			->where($db->quoteName('status') . ' = ' . $db->quote(5))
			->where($db->quoteName('venue_booking_datetime') . ' IN (\''. implode("','", $datesArray) .'\')' );
			// ->where($db->quoteName('venue_booking_id') . ' <> ' . $db->quote($tblVenuebooking->venue_booking_id))

		// Set the query and load the result.
		$db->setQuery($query);

		$slotBooked = 0;
		$bookingsOnSameDate = $db->loadObjectList();
		$timeSlots = array();

		foreach ($bookingsOnSameDate as $key => $booking)
		{
			$bookingDate = $booking->venue_booking_datetime;
			$bookingFrom = $booking->booking_from_time;
			$bookingTo = $booking->booking_to_time;

			/*echo "<br />Date Time : " . $bookingDate .' '.$bookingTo;
			echo "<br />Combine : ".date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo.' +1 days'));
			exit;


			echo "<br />Date : " . $bookingDate;
			echo "<br />From : ".strtotime($bookingDate.' '.$bookingFrom);
			echo "<br />To : ".strtotime($bookingDate.' '.$bookingTo);*/
			//exit;

			if (strtotime($bookingDate.' '.$bookingFrom)>strtotime($bookingDate.' '.$bookingTo)) {
				$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
				$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo.' +1 days'));
			} else {
				$bookingFrom = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingFrom));
				$bookingTo = date('Y-m-d H:i:s', strtotime($bookingDate .' '.$bookingTo));
			}

			/*echo "<br /> Booking To Last : " .$bookingTo;
			exit;*/

			$numbercurrentBookingFrom = strtotime($currentBookingFrom);
			$numbercurrentBookingTo   = strtotime($currentBookingTo);
			$numberbookingFrom        = strtotime($bookingFrom);
			$numberbookingTo          = strtotime($bookingTo);

			/*echo "currentBookingFrom:" . $currentBookingFrom . " || ".$numbercurrentBookingFrom."<br />";
			echo "currentBookingTo:" . $currentBookingTo . " || ".$numbercurrentBookingTo."<br /><br />";
			echo "bookingFrom:" . $bookingFrom . " || ".$numberbookingFrom."<br />";
			echo "bookingTo:" . $bookingTo . " || ".$numberbookingTo."<br /><br />";*/

			if(($numberbookingFrom < $numbercurrentBookingFrom) && ($numbercurrentBookingFrom < $numberbookingTo))
			{
				$slotBooked = $slotBooked + 1;
			}
			else if(($numberbookingFrom < $numbercurrentBookingTo) && ($numbercurrentBookingTo < $numberbookingTo))
			{
				$slotBooked = $slotBooked + 1;
			}
			else if(($numberbookingTo == $numbercurrentBookingTo) && ($numbercurrentBookingFrom == $numberbookingFrom))
			{
				$slotBooked = $slotBooked + 1;
			}
		}

		if($slotBooked)
		{
			return 601;
		}

		return 200;
	}

}
