<?php
/**
 * @package     iJoomerAdv.Site
 * @subpackage  com_ijoomeradv
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.helper' );
class company
{

	private $db;
	private $IJUserID;
	private $helper;
	private $jsonarray;

	function __construct()
	{
		$this->db                = JFactory::getDBO();
		$this->mainframe         =  JFactory::getApplication ();
		$this->IJUserID          = $this->mainframe->getUserState ( 'com_ijoomeradv.IJUserID', 0 ); //get login user id
		$this->my                = JFactory::getUser ( $this->IJUserID ); // set the login user object
		$this->helper            = new bctedAppHelper;
		$this->jsonarray         = array();
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"getCompanies",
	 * 		"taskData":{
	 *
	 * 		}
	 * 	}
	 */
	function getCompanies()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$city           = IJReq::getTaskData('city','','string');
		$latitude       = IJReq::getTaskData('latitude','0.00','string');
		$longitude      = IJReq::getTaskData('longitude','0.00','string');
		$withJetService = IJReq::getTaskData('withJetService',1,'int');
		$serviceID      = IJReq::getTaskData('serviceID',0,'int');
		$companyID      = IJReq::getTaskData('companyID',0,'int');

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);


		// Create the base select statement.
		$query->select('company_id')
			->from($db->quoteName('#__bcted_company_services'))
			->where($db->quoteName('service_active') . ' = ' . $db->quote('1'));

		if($serviceID)
		{
			$query->where($db->quoteName('service_id') . ' <> ' . $db->quote($serviceID));
		}
		if($companyID)
		{
			$query->where($db->quoteName('company_id') . ' <> ' . $db->quote($companyID));
		}

		$db->setQuery($query);
		$companyHasServices = $db->loadColumn();
		/*echo "<pre>";
		print_r($companyHasServices);
		echo "</pre>";
		exit;*/

		if(count($companyHasServices) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_NOT_FOUND'));
			return false;
		}



		if($withJetService)
		{
			$queryJetService = $db->getQuery(true);
			$queryJetService->select('company_id')
				->from($db->quoteName('#__bcted_company'))
				->where($db->quoteName('company_type') . ' = ' . $db->quote('jet'))
				->where($db->quoteName('company_active') . ' = ' . $db->quote('1'));

			/*echo $queryJetService->dump();
			exit;*/

			$db->setQuery($queryJetService);
			$jetServices = $db->loadColumn();

			if(count($jetServices) != 0)
			{
				$companyHasServices = array_merge($companyHasServices,$jetServices);
			}
		}

		$companyHasServicesStr = implode(",", $companyHasServices);

		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('company_id,(((acos(sin(('.$latitude.'*pi()/180)) * sin((`latitude`*pi()/180))+cos(('.$latitude.'*pi()/180)) * cos((`latitude`*pi()/180)) * cos((('.$longitude.'- `longitude`)*pi()/180))))*180/pi())*60*1.1515) as distance')
			->from($db->quoteName('#__bcted_company'))
			->where($db->quoteName('company_id') . ' IN ('.$companyHasServicesStr.')')
			->where($db->quoteName('company_active') . ' = ' . $db->quote(1));

		$blacklistCompany = $this->helper->userBlackListBy($this->IJUserID,'Company');

		$blacklistCompanyStr = "";

		if(count($blacklistCompany)!=0)
		{
			$blacklistCompanyStr = implode(",", $blacklistCompany);
		}

		/*echo "<pre>";
		print_r($blacklistCompany);
		echo "</pre>";
		exit;*/

		if(!empty($blacklistCompanyStr))
		{
			$query->where($db->quoteName('company_id') . ' NOT IN (' . $blacklistCompanyStr . ')');
		}

		if(!empty($city))
		{
			$query->where($db->quoteName('city') . ' LIKE ' . $db->quote($city));
		}

		$query->order($db->quoteName('company_name') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		$companies = $db->loadObjectList();
		$resultCompanies = array();

		foreach ($companies as $key => $company)
		{
			if($latitude != '0.00' && $longitude != '0.00')
			{
				if($company->distance > 20)
				{
					continue;
				}
			}
			$resultCompanies[] = $this->helper->getCompanyDetail($company->company_id);
		}

		if(count($resultCompanies) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_NOT_FOUND'));
			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($resultCompanies);
		$this->jsonarray['services'] = $resultCompanies;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"getServices",
	 * 		"taskData":{
	 *
	 * 		}
	 * 	}
	 */
	function getServices()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('cs.service_id')
			->from($db->quoteName('#__bcted_company_services','cs'))
			->where($db->quoteName('cs.service_active') . ' = ' . $db->quote('1'))
			->order($db->quoteName('cs.time_stamp') . ' DESC');


		$city      = IJReq::getTaskData('city','','string');

		$latitude  = IJReq::getTaskData('latitude','0.00','string');
		$longitude = IJReq::getTaskData('longitude','0.00','string');

		if(!empty($city))
		{
			$query->select('c.r_bcted,(((acos(sin(('.$latitude.'*pi()/180)) * sin((`latitude`*pi()/180))+cos(('.$latitude.'*pi()/180)) * cos((`latitude`*pi()/180)) * cos((('.$longitude.'- `longitude`)*pi()/180))))*180/pi())*60*1.1515) as distance')
				->join('LEFT','#__bcted_company AS c ON c.company_id=cs.company_id');
			$query->where($db->quoteName('c.city') . ' LIKE  ' . $db->quote('%'.$city.'%'));
			$query->where($db->quoteName('c.company_active') . ' =  ' . $db->quote(1));
		}

		/*echo $query->dump();
		exit;*/

		// Set the query and load the result.
		$db->setQuery($query);

		$resultService = array();

		try
		{
			$companyServices = $db->loadObjectList();

			/*if(count($companyServices) == 0)
			{

			}*/

			foreach ($companyServices as $key => $service)
			{
				if($latitude != '0.00' && $longitude != '0.00')
				{
					if($service->distance > 20)
					{
						continue;
					}
				}
				$resultService[] = $this->helper->getCompanyServiceDetail($service->service_id,1);
			}

		}
		catch (RuntimeException $e)
		{
			//throw new RuntimeException($e->getMessage(), $e->getCode());
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_($e->getMessage() . ' - ' . $e->getCode()));
			return false;
		}

		if(count($resultService) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICES_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($resultService);
		$this->jsonarray['services'] = $resultService;

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 *	  "extName": "bcted",
	 *	  "extView": "company",
	 *	  "extTask": "filterServices",
	 *	  "taskData": {
	 *	    "serviceName": "a",
	 *	    "attire": "0",
	 *	    "restaurant": "0",
	 *	    "smoking": "0",
	 *	    "rating": "0",
	 *	    "price": "0",
	 *	    "drink": "0",
	 *	    "otherAttrib": "0",
	 *	    "nearMe": "1",
	 *	    "longitude": "72.519038",
	 *	    "latitude": "23.0130501",
	 *	    "recommendedByBCT": "1"
	 *	  }
	 *	}
	 */
	function filterServices()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$serviceName = IJReq::getTaskData('serviceName','','string');
		$city        = IJReq::getTaskData('city','','string');

		$otherAttrib = 0;
		$recommendedByBCT = 0;
		/*$attire      = IJReq::getTaskData('attire',0,'int');
		$restaurant  = IJReq::getTaskData('restaurant',0,'int');
		$smoking     = IJReq::getTaskData('smoking',0,'int');
		$rating      = IJReq::getTaskData('rating',0,'int');
		$price       = IJReq::getTaskData('price',0,'int');
		$drink       = IJReq::getTaskData('drink',0,'int');
		$otherAttrib = IJReq::getTaskData('otherAttrib',0,'int');


		$recommendedByBCT       = IJReq::getTaskData('recommendedByBCT',0,'int');*/
		$longitude   = IJReq::getTaskData('longitude','0','string');
		$latitude    = IJReq::getTaskData('latitude','0','string');
		$nearMe       = IJReq::getTaskData('nearMe',0,'int');



		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('distinct(cs.company_id)')
			->from($db->quoteName('#__bcted_company_services','cs'))
			->where($db->quoteName('cs.service_active') . ' = ' . $db->quote('1'));

		$query->select('c.r_bcted,(((acos(sin(('.$latitude.'*pi()/180)) * sin((`latitude`*pi()/180))+cos(('.$latitude.'*pi()/180)) * cos((`latitude`*pi()/180)) * cos((('.$longitude.'- `longitude`)*pi()/180))))*180/pi())*60*1.1515) as distance')
			->join('LEFT','#__bcted_company AS c ON c.company_id=cs.company_id');

		if(!empty($serviceName))
		{
			$query->where($db->quoteName('c.company_name') . ' LIKE  "%'.$serviceName.'%"');
		}


		if(!empty($city))
		{
			$query->where($db->quoteName('c.city') . ' LIKE  ' . $db->quote('%'.$city.'%'));
		}

		if($otherAttrib)
		{
			$otherSql = array();
			if($attire)
			{
				$otherSql[] = '('.$db->quoteName('c.attire') . ' = ' . $db->quote($attire) .')';
			}

			if($restaurant)
			{
				$otherSql[] = '('.$db->quoteName('c.restaurant') . ' = ' . $db->quote($restaurant) .')';
			}

			if($smoking)
			{
				$otherSql[] = '('.$db->quoteName('c.smoking') . ' = ' . $db->quote($smoking) .')';
			}

			if($rating)
			{
				$otherSql[] = '('.$db->quoteName('c.rating') . ' = ' . $db->quote($rating) .')';
			}

			if($price)
			{
				$otherSql[] = '('.$db->quoteName('c.price') . ' = ' . $db->quote($price) .')';
			}

			if($drink)
			{
				$otherSql[] = '('.$db->quoteName('c.drink') . ' = ' . $db->quote($drink) .')';
			}

			if(count($otherSql) != 0)
			{
				$otherSqlImpload = implode(' OR ', $otherSql);
				$query->where('('.$otherSqlImpload.')');
			}
		}

		if($recommendedByBCT)
		{
			$query->where($db->quoteName('r_bcted') . ' = ' . $db->quote(1));
		}

		//$query->group($db->quoteName('c.company_id'));
		$query->order($db->quoteName('cs.time_stamp') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);

		/*echo $query->dump();
		exit;*/

		$resultService = array();

		$processedCompany = array();

		try
		{
			$companyServices = $db->loadObjectList();
			foreach ($companyServices as $key => $service)
			{
				//$resultService[] = $this->helper->getCompanyServiceDetail($service->service_id,1);

				if($nearMe)
				{
					if($service->distance <= 10)
					{
						//$resultService[] = $this->helper->getCompanyServiceDetail($service->service_id,1);
						$resultService[] = $this->helper->getCompanyDetail($service->company_id);
						$processedCompany[] = $service->company_id;
					}

				}
				else
				{
					$resultService[] = $this->helper->getCompanyDetail($service->company_id);
					$processedCompany[] = $service->company_id;
				}
			}

		}
		catch (RuntimeException $e)
		{
			//throw new RuntimeException($e->getMessage(), $e->getCode());
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_($e->getMessage() . ' - ' . $e->getCode()));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('company_id')
			->from($db->quoteName('#__bcted_company'))
			->where($db->quoteName('company_name') . ' LIKE  "%' . $serviceName .'%"')
			->where($db->quoteName('company_type') . ' = ' . $db->quote('jet'))
			->where($db->quoteName('company_active') . ' = ' . $db->quote(1));

		// Set the query and load the result.
		$db->setQuery($query);

		/*echo "<pre>";
		print_r($query->dump());
		echo "</pre>";
		exit;*/

		$jetCompanies = $db->loadColumn();
		/*echo "<pre>";
		print_r($jetCompanies);
		echo "</pre>";
		exit;*/
		foreach ($jetCompanies as $key => $jetCompany)
		{
			if(!in_array($jetCompany, $processedCompany))
			{
				$resultService[] = $this->helper->getCompanyDetail($jetCompany);
				$processedCompany[] = $jetCompany;
			}
		}


		if(count($resultService) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICES_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		for($i=0;$i<count($resultService);$i++)
		{
			for($j=$i+1;$j<count($resultService);$j++)
			{
				if(strtolower($resultService[$i]['companyName'])>strtolower($resultService[$j]['companyName']))
				{
					$temp = $resultService[$i];
					$resultService[$i] = $resultService[$j];
					$resultService[$j] = $temp;
				}
			}
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($resultService);
		$this->jsonarray['services'] = $resultService;

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"getCompanyServices",
	 * 		"taskData":{
	 * 			"companyID":"number"
	 * 		}
	 * 	}
	 */
	function getCompanyServices()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$companyID         = IJReq::getTaskData('companyID',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblCompany->load($companyID);

		if(!$tblCompany->company_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_COMPANY'));
			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('service_id')
			->from($db->quoteName('#__bcted_company_services'))
			->where($db->quoteName('company_id') . ' = ' . $db->quote($tblCompany->company_id))
			->order($db->quoteName('time_stamp') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);

		$resultService = array();

		try
		{
			$companyServices = $db->loadColumn();
			foreach ($companyServices as $key => $service)
			{
				$resultService[] = $this->helper->getCompanyServiceDetail($service,1);
			}

		}
		catch (RuntimeException $e)
		{
			//throw new RuntimeException($e->getMessage(), $e->getCode());
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_($e->getMessage() . ' - ' . $e->getCode()));
			return false;
		}

		if(count($resultService) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICES_NOT_FOUND'));
			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($resultService);
		$this->jsonarray['services'] = $resultService;

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"addCompanyService",
	 * 		"taskData":{
	 * 			"serviceID":"number",
	 * 			"companyID":"number",
	 * 			"serviceName":"string",
	 * 			"price":"string",
	 * 			"description":"string"
	 * 		}
	 * 	}
	 */
	function addCompanyService()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$serviceID   = IJReq::getTaskData('serviceID',0,'int');
		$companyID   = IJReq::getTaskData('companyID',0,'int');
		$serviceName = IJReq::getTaskData('serviceName','','string');
		$price       = IJReq::getTaskData('price','','string');
		$description = IJReq::getTaskData('description','','string');

		if(!$companyID || empty($serviceName) || empty($price))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_DETAIL_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}


		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblService = JTable::getInstance('Service', 'BctedTable');
		$tblService->load($serviceID);

		$data['company_id']          = $companyID;
		$data['service_name']        = $serviceName;
		$data['service_description'] = $description;
		$data['service_price']       = $price;
		$data['service_active']      = '1';

		$data['user_id']      = $this->IJUserID;

		if (!$serviceID)
		{
			$data['service_created']     = date('Y-m-d h:i:s');
		}

		$file = JRequest::getVar('image','','FILES','array');

		if(is_array($file) && isset($file['size']) && $file['size']>0)
		{
			$serviceImage = $this->uplaodFile($file,'services',$this->IJUserID);

			if(!empty($serviceImage))
			{
				if(!empty($tblService->service_image) && file_exists(JUri::root().$tblService->service_image))
				{
					unlink(JUri::root().$tblService->service_image);
				}

				$data['service_image'] = $serviceImage;
			}
		}

		//$data['venue_table_created']     = date('Y-m-d h:i:s');
		$data['service_modified'] = date('Y-m-d h:i:s');
		$data['time_stamp']       = time();

		$tblService->bind($data);

		if(!$tblService->store())
		{
			$this->jsonarray['code'] = 500;

			return $this->jsonarray;
		}

		$this->jsonarray['code'] = 200;

		$service[] = $this->helper->getCompanyServiceDetail($tblService->service_id);

		$this->jsonarray['service'] = $service;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"deleteCompanyService",
	 * 		"taskData":{
	 * 			"serviceID":"number"
	 * 		}
	 * 	}
	 */
	function deleteCompanyService()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$serviceID         = IJReq::getTaskData('serviceID',0,'int');

		if(!$serviceID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblService = JTable::getInstance('Service', 'BctedTable');
		$tblService->load($serviceID);

		if(!$tblService->service_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblService->user_id != $this->IJUserID)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_PERMISSION_DENIED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}
		$availableBooking = $this->checkForActiveBooking($tblService->service_id,$tblService->company_id);

		if(count($availableBooking) != 0)
		{
			IJReq::setResponseCode(101);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_HAS_ACTIVE_BOOKING'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblService->service_active = 0;

		if(!$tblService->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_NOT_DELETED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;

	}

	function checkForActiveBooking($serviceID,$companyID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('service_booking_id')
			->from($db->quoteName('#__bcted_service_booking'))
			->where($db->quoteName('service_id') . ' = ' . $db->quote($serviceID))
			->where($db->quoteName('company_id') . ' = ' . $db->quote($companyID))
			->where($db->quoteName('is_deleted') . ' = ' . $db->quote(0))
			->where($db->quoteName('service_booking_datetime') . ' >= ' . $db->quote(date('Y-m-d')));

		// Set the query and load the result.
		$db->setQuery($query);

		/*echo $query->dump();
		exit;*/

		$result = $db->loadColumn();

		return $result;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"bookCompanyService",
	 * 		"taskData":{
	 * 			"companyID":"number",
	 * 			"serviceID":"number",
	 * 			"date":"date",
	 * 			"time":"time",
	 * 			"location":"string",
	 * 			"additionalInfo":"string"
	 * 		}
	 * 	}
	 */
	function bookCompanyService()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$companyID = IJReq::getTaskData('companyID',0,'int');
		$serviceID = IJReq::getTaskData('serviceID',0,'int');



		if(!$companyID && !$serviceID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL1'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblService = JTable::getInstance('Service', 'BctedTable');
		$tblService->load($serviceID);

		if(!$tblService->service_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL2'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblCompany->load($companyID);

		if(!$tblCompany->company_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL3'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblCompany->company_id != $tblService->company_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL4'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$blockedUsers = $this->helper->getBlackList($tblCompany->company_id,'Company');

		if(in_array($this->IJUserID, $blockedUsers))
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL5'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$date           = IJReq::getTaskData('date','','string');
		$fromtime       = IJReq::getTaskData('fromTime','','string');
		$toTime         = IJReq::getTaskData('toTime','','string');
		$location       = IJReq::getTaskData('location','','string');
		$serviceCity       = IJReq::getTaskData('serviceCity','','string');
		$additionalInfo = IJReq::getTaskData('additionalInfo','','string');
		$maleCount      = IJReq::getTaskData('maleCount',0, 'int');
		$femaleCount    = IJReq::getTaskData('femaleCount',0, 'int');
		$latitude       = IJReq::getTaskData('latitude','0.00','string');
		$longitude      = IJReq::getTaskData('longitude','0.00','string');
		$guestsCount    = $maleCount + $femaleCount;

		if(empty($date) || empty($fromtime) || empty($toTime))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL6'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');
		$tblServiceBooking->load(0);

		$fromtime = $this->helper->convertToHMS($fromtime);
		$toTime = $this->helper->convertToHMS($toTime);

		$totalHours = $this->helper->differenceInHours($fromtime,$toTime);

		$priceInHours = $tblService->service_price * $totalHours;
		$extParam = $this->helper->getExtensionParamCore();

		if($tblCompany->commission_rate == 0)
		{
			$commissionRate = $extParam->deposit_per;
		}
		else
		{
			$commissionRate = $tblCompany->commission_rate;
		}

		/*echo "<pre>";
		print_r($extParam->deposit_per);
		echo "</pre>";
		exit;*/

		$csbdata                                    = array();
		$csbdata['service_id']                      = $tblService->service_id;
		$csbdata['company_id']                      = $tblCompany->company_id;
		$csbdata['user_id']                         = $this->IJUserID;
		$csbdata['service_booking_datetime']        = date('Y-m-d',strtotime($date));
		$csbdata['booking_from_time']               = $fromtime;
		$csbdata['booking_to_time']                 = $toTime;
		$csbdata['service_location']                = $location;
		$csbdata['service_city']                    = $serviceCity;
		$csbdata['service_booking_additional_info'] = $additionalInfo;
		$csbdata['service_booking_number_of_guest'] = $guestsCount;
		$csbdata['male_count']                      = $maleCount;
		$csbdata['female_count']                    = $femaleCount;
		$csbdata['latitude']                        = $latitude;
		$csbdata['longitude']                       = $longitude;
		$csbdata['status']                          = "1";
		$csbdata['user_status']                     = '2';
		$csbdata['is_new']                          = '1';
		$csbdata['service_booking_created']         = date('Y-m-d H:i:s');
		$csbdata['service_booking_is_cancelled']    = '0';
		$csbdata['time_stamp']                      = time();
		$csbdata['total_price']                     = $priceInHours;
		$csbdata['deposit_amount']                  = (($commissionRate * $priceInHours)/100);

		$deposit_amount = (($commissionRate * $priceInHours)/100);
		$csbdata['amount_payable'] = $priceInHours - $deposit_amount;

		$tblServiceBooking->bind($csbdata);

		if(!$tblServiceBooking->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_BOOKING_OF_COMPANY_SERVICE_FAILED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		$bookingData = $this->helper->getBookingDetailForServices($tblServiceBooking->service_booking_id,'Company');

		/*if(!empty($tblServiceBooking->service_booking_additional_info))
		{
			$this->helper->sendMessage(0,$tblCompany->company_id,$tblService->service_id,0,$tblCompany->userid,$tblServiceBooking->service_booking_additional_info,$bookingData,$messageType='newservicebooking');
		}*/

		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "company";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');

		/*echo "<pre>";
		print_r($obj);
		echo "</pre>";
		exit;*/
		if($obj->id)
		{
			$userProfile    = $this->helper->getUserProfile($tblCompany->userid);
			$receiveRequest = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveRequest = $params->settings->pushNotification->receiveRequest;
			}

			if($receiveRequest)
			{
				$message = JText::sprintf('COM_IJOOMERADV_COMPANY_NEW_REQUEST_FOR_BOOKING_OF_SERVICE',$tblService->service_name);
				$message = 'You have new Request for '.$tblService->service_name;

				$this->jsonarray['pushNotificationData']['id']         = $obj->id;
				$this->jsonarray['pushNotificationData']['to']         = $tblCompany->userid;
				$this->jsonarray['pushNotificationData']['message']    = $message;
				$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWREQUESTRECEIVED');
				$this->jsonarray['pushNotificationData']['configtype'] = '';
			}
		}


		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"inviteToMyBookedService",
	 * 		"taskData":{
	 * 			"bookingID":"number",
	 * 			"emails":"string",
	 * 			"message":"string"
	 * 		}
	 * 	}
	 */
	function inviteToMyBookedService()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$user = JFactory::getUser();

		$bookingID     = IJReq::getTaskData('bookingID',0,'int');
		$emails        = IJReq::getTaskData('emails', '', 'string');
		$enterEmails   = IJReq::getTaskData('emails', '', 'string');
		$message       = IJReq::getTaskData('message', '', 'string');
		$seletedEmails = IJReq::getTaskData('emails', '', 'string');

		if(empty($emails) || !$bookingID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_DETAIL_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$bctRegiEmails = $this->helper->filterEmails($emails);

		$emailsArray = explode(",", $emails);
		$filterEmails = array();

		foreach ($emailsArray as $key => $singleEmail)
		{
			if(in_array($singleEmail, $bctRegiEmails['allRegEmail']))
			{
				if(in_array($singleEmail, $bctRegiEmails['service'])){ continue; }
				if(in_array($singleEmail, $bctRegiEmails['venue'])){ continue; }
				if(in_array($singleEmail, $bctRegiEmails['guest']) && $this->my->email == $singleEmail){ continue; }
				//if(in_array($$singleEmail, $bctRegiEmails['service'])){ continue; }
			}

			$filterEmails[] = $singleEmail;
		}

		if(count($filterEmails)==0)
		{
			IJReq::setResponseCode(300);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVITE_NOT_VALID_SELF_EMAIL_OR_CLUB_SERVICE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$seletedEmails = implode(',', $filterEmails);
		$emails = $seletedEmails;
		$enterEmails = $emails;


		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblCompany            = JTable::getInstance('Company', 'BctedTable');
		$tblService            = JTable::getInstance('Service', 'BctedTable');
		$tblServiceBooking     = JTable::getInstance('ServiceBooking', 'BctedTable');
		//$tblVenuetableinvite = JTable::getInstance('Venuetableinvite','BctedTable');


		$tblServiceBooking->load($bookingID);

		$tblCompany->load($tblServiceBooking->company_id);
		$tblService->load($tblServiceBooking->service_id);

		if(!$tblServiceBooking->service_booking_id || !$tblCompany->company_id || !$tblService->service_id)
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_SERVICE_BOOKING_DETAIL_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if(!empty($emails))
		{
			$emails = explode(",", $emails);
			$emails = implode("','", $emails);
			$emails = "'".$emails."'";

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('id')
				->from($db->quoteName('#__users'))
				->where($db->quoteName('email') . ' IN (' . $emails . ')')
				->where($db->quoteName('block') . ' = ' . $db->quote(0));

			// Set the query and load the result.
			$db->setQuery($query);

			$foundUsers = $db->loadColumn();

			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('email')
				->from($db->quoteName('#__users'))
				->where($db->quoteName('email') . ' IN (' . $emails . ')')
				->where($db->quoteName('block') . ' = ' . $db->quote(0));

			// Set the query and load the result.
			$db->setQuery($query);

			$foundEmails = $db->loadColumn();
		}

		$this->jsonarray['code'] = 200;

		if(count($foundUsers)!=0)
		{
			$message = JText::sprintf('COM_IJOOMERADV_COMPANY_INVITE_TO_MY_BOOKING_SERVICE',$user->name,$tblCompany->company_name);
			$message = $user->name .' want to invite you at ' . $tblCompany->company_name;// . ';'.$tblTable->venue_table_name.';'.$tblVenuebooking->venue_booking_datetime;

			//$this->helper->getMessageConnection($fromUser,$toUser);

			$this->jsonarray['pushNotificationData']['id']         = $bookingID;
			$this->jsonarray['pushNotificationData']['to']         = implode(",", $foundUsers);
			$this->jsonarray['pushNotificationData']['message']    = $message;
			$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_BOOKEDSERVICEINVITETOUSER'); //'bookedTableInviteToUser';
			$this->jsonarray['pushNotificationData']['configtype'] = '';
		}

		if(count($foundEmails) != 0)
		{
			$notRegEmails = array_diff($seletedEmails, $foundEmails);

			/*echo "<pre>";
			print_r($notRegEmails);
			echo "</pre>";
			exit;*/
			if(count($notRegEmails) != 0)
			{
				// Initialise variables.
				$app     = JFactory::getApplication();
				$config  = JFactory::getConfig();

				$site    = $config->get('sitename');
				$from    = $config->get('mailfrom');
				$sender  = $config->get('fromname');
				$email   = $notRegEmails; //implode(",",$notRegEmails); //$app->input->get('mailto');
				$subject = JText::_('COM_IJOOMERADV_BOOKED_COMPANY_SERVICE_INVITATION_EMAIL_SUBJECT'); // $app->input->get('subject');

				// Build the message to send.
				//$msg     = JText::_('COM__SEND_EMAIL_MSG');
				//Hello, <br /><br />%s want to invite you to join Table %s at Club %s on %s<br /><br />Thank You,<br />BC-Ted Team"
				$body    = JText::sprintf('COM_IJOOMERADV_BOOKED_COMPANY_SERVICE_INVITATION_EMAIL_BODY',$user->name,$tblService->service_name,$tblCompany->company_name,date('d-m-Y H:i',strtotime($tblServiceBooking->service_booking_datetime)));

				// Clean the email data.
				$sender  = JMailHelper::cleanAddress($sender);
				$subject = JMailHelper::cleanSubject($subject);
				$body    = JMailHelper::cleanBody($body);

				// Send the email.
				$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body, true);

				// Check for an error.
				if ($return !== true)
				{
					//return new JException(JText::_('COM__SEND_MAIL_FAILED'), 500);
				}
			}

		}

		if(!empty($enterEmails))
		{
			$enterEmailArray = explode(",", $enterEmails);

			foreach ($enterEmailArray as $key => $singleEmail)
			{
				// Initialiase variables.
				$db    = JFactory::getDbo();
				$query = $db->getQuery(true);

				// Create the base select statement.
				$query->select('*')
					->from($db->quoteName('#__users'))
					->where($db->quoteName('email') . ' = ' . $db->quote($singleEmail));

				// Set the query and load the result.
				$db->setQuery($query);

				$userData = $db->loadObject();

				/*echo "<pre>";
				print_r($userData);
				echo "</pre>";*/

				if($userData)
				{
					$fromTime = explode(":", $tblServiceBooking->booking_from_time);

					$message = $user->name .' want to invite you for ' . $tblCompany->company_name.' at '.$tblServiceBooking->service_location.' on '.date('d-m-Y', strtotime($tblServiceBooking->service_booking_datetime)).' ' .$fromTime[0].":".$fromTime[1];
					$extraParam = array();
					$extraParam['serviceBookingID'] = $tblService->service_booking_id;
					$this->sendMessageForInviteUserByOwner(0,$tblCompany->company_id,$tblService->service_id,0,$userData->id,$message,$extraParam,$messageType='inviteToTable');
				}

				$app     = JFactory::getApplication();
				$config  = JFactory::getConfig();

				$site    = $config->get('sitename');
				$from    = $config->get('mailfrom');
				$sender  = $config->get('fromname');
				$email   = $singleEmail; //implode(",",$notRegEmails); //$app->input->get('mailto');
				$subject = JText::_('COM_IJOOMERADV_BOOKED_COMPANY_SERVICE_INVITATION_EMAIL_SUBJECT'); // $app->input->get('subject');

				// Build the message to send.
				//$msg     = JText::_('COM__SEND_EMAIL_MSG');
				//Hello, <br /><br />%s want to invite you to join Table %s at Club %s on %s<br /><br />Thank You,<br />BC-Ted Team"
				$body    = JText::sprintf('COM_IJOOMERADV_BOOKED_COMPANY_SERVICE_INVITATION_EMAIL_BODY',$user->name,$tblService->service_name,$tblCompany->company_name,date('d-m-Y H:i',strtotime($tblServiceBooking->service_booking_datetime)));

				// Clean the email data.
				$sender  = JMailHelper::cleanAddress($sender);
				$subject = JMailHelper::cleanSubject($subject);
				$body    = JMailHelper::cleanBody($body);

				/*echo $body;
				exit;*/

				//echo $email."<br />";

				// Send the email.
				$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body, true);

				// Check for an error.
				if ($return !== true)
				{
					return new JException(JText::_('COM__SEND_MAIL_FAILED'), 500);
				}

			}

			//exit;

		}

		return $this->jsonarray;
	}

	public function sendMessageForInviteUserByOwner($venueID,$companyID,$serviceID,$tableID,$TouserID,$message,$extraParam = array(),$messageType='invitetoservice')
	{
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblMessage = JTable::getInstance('Message', 'BctedTable');
		$tblMessage->load(0);

		$data = array();

		$data['venue_id']   = $venueID;
		$data['company_id'] = $companyID;
		$data['table_id']   = $tableID;
		$data['service_id'] = $serviceID;
		$data['userid']     = $TouserID;
		$data['to_userid']  = $TouserID;
		$data['message']    = $message;
		if(count($extraParam)!=0)
		{
			$data['extra_params'] = json_encode($extraParam);
		}
		else
		{
			$data['extra_params'] = "";
		}

		$data['message_type']    = $messageType;
		$data['created']    = date('Y-m-d H:i:s');
		$data['time_stamp'] = time();

		/*if($companyID)
		{
			$tblCompany = JTable::getInstance('Company', 'BctedTable');
			$tblCompany->load($companyID);

			$elementName = $tblCompany->company_name;

			$data['from_userid'] = $tblCompany->userid;

		}
		else if($venueID)
		{
			$tblVenue = JTable::getInstance('Venue', 'BctedTable');
			$tblVenue->load($venueID);

			$elementName = $tblVenue->venue_name;

			$data['from_userid'] = $tblVenue->userid;
		}
		else
		{*/
			$data['from_userid'] = $this->IJUserID;
		//}
		$connectionID = $this->helper->getMessageConnection($data['from_userid'],$data['to_userid']);
		if(!$connectionID)
		{
			return 0;
		}

		$data['connection_id'] = $connectionID;
		$tblMessage->bind($data);

		$tblMessage->store();
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"bookPrivateJetService",
	 * 		"taskData":{
	 * 			"companyID":"number",
	 * 			"name":"string",
	 * 			"email":"string",
	 * 			"phoneno":"string",
	 * 			"flightDate":"date",
	 * 			"flyingFrom":"string"
	 * 			"flyingTo":"string",
	 * 			"additionalStops":"string",
	 * 			"numberOfPeople":"number",
	 * 			"additionalInfo":"string",
	 * 		}
	 * 	}
	 */
	function bookPrivateJetService()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$companyID = IJReq::getTaskData('companyID',0,'int');
		$name      = IJReq::getTaskData('name','','string');
		$email     = IJReq::getTaskData('email','','string');
		$phoneno   = IJReq::getTaskData('phoneno','','string');

		$flightDate      = IJReq::getTaskData('flightDate','','string');
		$flyingFrom      = IJReq::getTaskData('flyingFrom','','string');
		$flyingTo        = IJReq::getTaskData('flyingTo','','string');
		$additionalStops = IJReq::getTaskData('additionalStops','','string');
		$numberOfPeople  = IJReq::getTaskData('numberOfPeople',0,'int');
		$additionalInfo  = IJReq::getTaskData('additionalInfo','','string');



		if(!$companyID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL1'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblCompany->load($companyID);

		if(!$tblCompany->company_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL3'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblCompany->company_type != 'jet')
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL3'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if(!$tblCompany->company_active)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL3'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*$blockedUsers = $this->helper->getBlackList($tblCompany->company_id,'Company');

		if(in_array($this->IJUserID, $blockedUsers))
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL5'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}*/

		// /if(empty($date) || empty($fromtime) || empty($toTime))
		if(empty($name) || empty($email) || empty($phoneno) || empty($flightDate) || empty($flyingFrom) || empty($flyingTo) || $numberOfPeople == 0)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_INVALID_SERVICE_DETAIL6'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblJetServiceBooking = JTable::getInstance('JetServiceBooking', 'BctedTable');
		$tblJetServiceBooking->load(0);

		/*$fromtime = $this->helper->convertToHMS($fromtime);
		$toTime = $this->helper->convertToHMS($toTime);

		$totalHours = $this->helper->differenceInHours($fromtime,$toTime);

		$priceInHours = $tblService->service_price * $totalHours;
		$extParam = $this->helper->getExtensionParamCore();

		if($tblCompany->commission_rate == 0)
		{
			$commissionRate = $extParam->deposit_per;
		}
		else
		{
			$commissionRate = $tblCompany->commission_rate;
		}*/

		/*echo "<pre>";
		print_r($extParam->deposit_per);
		echo "</pre>";
		exit;*/

		$csbdata                     = array();
		$csbdata['company_id']       = $tblCompany->company_id;
		$csbdata['user_id']          = $this->IJUserID;
		$csbdata['parent_id']        = 0;
		$csbdata['name']             = $name;
		$csbdata['email']            = $email;
		$csbdata['phone']            = $phoneno;
		$csbdata['date_of_flight']   = date('Y-m-d H:i:s',strtotime($flightDate));
		$csbdata['flying_from']      = $flyingFrom;
		$csbdata['flying_to']        = $flyingTo;
		$csbdata['additional_stops'] = $additionalStops;
		$csbdata['number_of_people'] = $numberOfPeople;
		$csbdata['additional_info']  = $additionalInfo;
		$csbdata['created']          = date('Y-m-d-H:i:s');
		$csbdata['time_stamp']       = time();
		$csbdata['is_deleted']       = '0';

		$tblJetServiceBooking->bind($csbdata);

		if(!$tblJetServiceBooking->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_BOOKING_OF_COMPANY_SERVICE_FAILED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*if(!empty($tblJetServiceBooking->additional_info))
		{
			$bookingData = array();
			$this->helper->sendMessage(0,$tblCompany->company_id,0,0,$tblCompany->userid,$tblJetServiceBooking->additional_info,$bookingData,'newbookingrequestforprivatejet');
		}*/

		$this->jsonarray['code'] = 200;

		// Initialise variables.
		$app     = JFactory::getApplication();
		$config  = JFactory::getConfig();

		$site    = $config->get('sitename');
		$from    = $config->get('mailfrom');
		$sender  = $config->get('fromname');
		$emailToSend   = $tblCompany->email_address; //$app->input->get('mailto');
		$subject = JText::_('COM_IJOOMERADV_COMPANY_BOOK_PRIVATE_JET_SERVICE_EMAIL_SUBJECT'); //$app->input->get('subject');

		/*<thead>
				<tr>
					<td>Name</td>
					<td>Email</td>
					<td>Phone</td>
				</tr>
			</thead>*/
		$htmlDetail = '
		<table  align="center">
			<tr>
				<td>Name</td>
				<td>'.$name.'</td>
			</tr>
			<tr>
				<td>Email</td>
				<td>'.$email.'</td>
			</tr>
			<tr>
				<td>Phone</td>
				<td>'.$phoneno.'</td>
			</tr>
			<tr>
				<td>Date of	flight</td>
				<td>'.date('d-m-Y',strtotime($flightDate)).'</td>
			</tr>
			<tr>
				<td>Flying From</td>
				<td>'.$flyingFrom.'</td>
			</tr>
			<tr>
				<td>Flying To</td>
				<td>'.$flyingTo.'</td>
			</tr>
			<tr>
				<td>Additional Stops</td>
				<td>'.$additionalStops.'</td>
			</tr>
			<tr>
				<td>Number of people</td>
				<td>'.$numberOfPeople.'</td>
			</tr>
			<tr>
				<td>Additional Information</td>
				<td>'.$additionalInfo.'</td>
			</tr>
		</table>';

		$user= JFactory::getUser();

		// Build the message to send.
		$msg     = JText::_('COM__SEND_EMAIL_MSG');
		$msg = JText::sprintf('COM_IJOOMERADV_COMPANY_BOOK_PRIVATE_JET_SERVICE_EMAIL_BODY',$user->name,$htmlDetail);
		$body    = sprintf($msg, $site, $sender, $from);

		// Clean the email data.
		$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $emailToSend, $subject, $body,true);

		// Check for an error.
		if ($return !== true)
		{
			return new JException(JText::_('COM__SEND_MAIL_FAILED'), 500);
		}

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"changeBookingStatus",
	 * 		"taskData":{
	 * 			"bookingID":"number",
	 * 			"companyID":"number",
	 * 			"action":"Decline/Accept/Waiting",
	 * 			"message":"string",
	 * 		}
	 * 	}
	 */
	function changeBookingStatus()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$companyID = IJReq::getTaskData('companyID',0,'int');
		$bookingID = IJReq::getTaskData('bookingID',0,'int');
		$action    = IJReq::getTaskData('action','Accept','string');
		$message   = IJReq::getTaskData('message','','string');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblCompany = JTable::getInstance('Company', 'BctedTable');

		$tblCompany->load($companyID);

		if(!$tblCompany->company_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_INVALID_COMPANY_DETAIL'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblCompany->userid != $this->IJUserID)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_PERMISSION_DENIED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');
		$tblServiceBooking->load($bookingID);

		if(!$tblServiceBooking->service_booking_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_INVALID_BOOKING_DATA1'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*echo "<pre>";
		print_r($tblServiceBooking);
		echo "</pre>";
		echo "<pre>";
		print_r($tblCompany);
		echo "</pre>";
		exit;*/
		if($tblServiceBooking->company_id != $tblCompany->company_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_INVALID_BOOKING_DATA2'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if(empty($action))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_INVALID_BOOKING_DATA3'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$newStatus = $tblServiceBooking->status;
		$newUserStatus = $tblServiceBooking->user_status;

		if($action == 'Decline')
		{
			$newStatus = $this->helper->getStatusIDFromStatusName('Decline');
			$newUserStatus = $this->helper->getStatusIDFromStatusName('Unavailable');
		}
		else if($action == 'Waiting')
		{
			$newStatus = $this->helper->getStatusIDFromStatusName('Waiting List');
			$newUserStatus = $this->helper->getStatusIDFromStatusName('Waiting List');
		}
		else if($action == 'Accept')
		{
			$newStatus = $this->helper->getStatusIDFromStatusName('Awaiting Payment');
			$newUserStatus = $this->helper->getStatusIDFromStatusName('Available');
		}

		/*$dataVTB['status'] = $newStatus;
		$dataVTB['owner_message'] = $message;*/

		$tblServiceBooking->status = $newStatus;
		$tblServiceBooking->user_status = $newUserStatus;
		$tblServiceBooking->is_new = '0';
		$tblServiceBooking->owner_message = $message;

		if(!$tblServiceBooking->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_CHANGE_STATUS_FAILED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;



		$bookingData = $this->helper->getBookingDetailForServices($tblServiceBooking->service_booking_id,'User');

		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "company";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{
			$userProfile    = $this->helper->getUserProfile($tblServiceBooking->user_id);
			$updateMyBookingStatus = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$updateMyBookingStatus = $params->settings->pushNotification->updateMyBookingStatus;
			}

			if($updateMyBookingStatus)
			{
				$message = JText::sprintf('COM_IJOOMERADV_COMPANY_SERVICE_BOOKIN_STATUS_CHANGED',$tblCompany->company_name);
				//$message = 'Your booking Status has been changed by '.$tblCompany->company_name;


				$this->jsonarray['pushNotificationData']['id']         = $obj->id; //$tblServiceBooking->service_booking_id; //$obj->id;
				$this->jsonarray['pushNotificationData']['to']         = $tblServiceBooking->user_id;
				$this->jsonarray['pushNotificationData']['message']    = $message;
				$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_REQUESTSTATUSCHANGED'); //'RequestStatusChanged';
				$this->jsonarray['pushNotificationData']['configtype'] = '';
			}
		}

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	  	{
	  		"extName":"bcted",
	  		"extView":"company",
	  		"extTask":"cancelBooking",
	  		"taskData":{
	  			"serviceBookingID":"number";
	  		}
	  	}
	 */
	function cancelBooking()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$bookingID = IJReq::getTaskData('serviceBookingID',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblServiceBooking     = JTable::getInstance('ServiceBooking', 'BctedTable');

		$tblServiceBooking->load($bookingID);

		if(!$tblServiceBooking->service_booking_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_BOOKING_ID_INVALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*echo $this->IJUserID."<pre>";
		print_r($tblVenuebooking);
		echo "</pre>";
		exit;*/

		if($tblServiceBooking->user_id  != $this->IJUserID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_BOOKING_NOT_YOUR'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$statusID = $this->helper->getStatusIDFromStatusName('Cancelled');
		$tblServiceBooking->status=$statusID;
		$tblServiceBooking->user_status=$statusID;

		if(!$tblServiceBooking->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_BOOKING_NOT_CANCELED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_payment_status'))
			->where($db->quoteName('booked_element_id') . ' = ' . $db->quote($tblServiceBooking->service_booking_id))
			->where($db->quoteName('booked_element_type') . ' = ' . $db->quote('service'))
			->where($db->quoteName('paid_status') . ' = ' . $db->quote('1'));

		// Set the query and load the result.
		$db->setQuery($query);

		$paymentStatus = $db->loadObject();
		if($paymentStatus)
		{
			$queryLP = $db->getQuery(true);
			$queryLP->select('*')
				->from($db->quoteName('#__bcted_loyalty_point'))
				->where($db->quoteName('user_id') . ' = ' . $db->quote($tblServiceBooking->user_id))
				->where($db->quoteName('point_app') . ' = ' . $db->quote('purchase.service'))
				->where($db->quoteName('cid') . ' = ' . $db->quote($paymentStatus->payment_id));

			// Set the query and load the result.
			$db->setQuery($queryLP);

			$loyaltyPointDetail = $db->loadObject();

			if($loyaltyPointDetail)
			{
				$query = $db->getQuery(true);

				// Create the base insert statement.
				$query->insert($db->quoteName('#__bcted_loyalty_point'))
					->columns(
						array(
							$db->quoteName('user_id'),
							$db->quoteName('earn_point'),
							$db->quoteName('point_app'),
							$db->quoteName('cid'),
							$db->quoteName('is_valid'),
							$db->quoteName('created'),
							$db->quoteName('time_stamp')
						)
					)
					->values(
						$db->quote($tblServiceBooking->user_id) . ', ' .
						$db->quote(($loyaltyPointDetail->earn_point * (-1))) . ', ' .
						$db->quote('service.cancelbooking') . ', ' .
						$db->quote($loyaltyPointDetail->cid) . ', ' .
						$db->quote(1) . ', ' .
						$db->quote(date('Y-m-d H:i:s')) . ', ' .
						$db->quote(time())
					);

				// Set the query and execute the insert.
				$db->setQuery($query);

				$db->execute();

			}
		}

		$tblService = JTable::getInstance('Service', 'BctedTable');
		$tblService->load($tblServiceBooking->service_id);

		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblCompany->load($tblServiceBooking->company_id);

		$this->jsonarray['code'] = 200;

		$bookingData = $this->helper->getBookingDetailForServices($tblServiceBooking->service_booking_id,'Company');
		$user = JFactory::getUser();
		$cancelMessage = JText::sprintf('COM_IJOOMERADV_COMPANY_SERVICE_BOOKING_CANCEL_MESSAGE',$user->name,$tblService->service_name);
		$this->helper->sendMessage(0,$tblCompany->company_id,$tblService->service_id,0,$tblCompany->userid,$cancelMessage,$bookingData,$messageType='servicebookingcancel');


		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "company";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');

		/*echo "<pre>";
		print_r($obj);
		echo "</pre>";
		exit;*/
		if($obj->id)
		{
			$userProfile    = $this->helper->getUserProfile($tblCompany->userid);
			$receiveRequest = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveRequest = $params->settings->pushNotification->receiveRequest;
				$receiveRequest = 1;
			}

			if($receiveRequest)
			{
				$message = $cancelMessage;
				$this->jsonarray['pushNotificationData']['id']         = $obj->id;
				$this->jsonarray['pushNotificationData']['to']         = $tblCompany->userid;
				$this->jsonarray['pushNotificationData']['message']    = $message;
				$this->jsonarray['pushNotificationData']['type']       =  JText::_('PUSHNOTIFICATION_TYPE_SERVICEBOOKINGCANCEL'); //'servicebookingcancel';
				$this->jsonarray['pushNotificationData']['configtype'] = '';
			}
		}

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"deleteConfirmedBooking",
	 * 		"taskData":{
	 * 			"serviceBookingID":"number"
	 * 		}
	 * 	}
	 */
	function deleteConfirmedBooking()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$BookingID = IJReq::getTaskData('serviceBookingID',0,'int');

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');

		$tblServiceBooking->load($BookingID);

		if(!$tblServiceBooking->service_booking_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$statusID = array();
		$statusID[] = $this->helper->getStatusIDFromStatusName('Booked');
		$statusID[] = $this->helper->getStatusIDFromStatusName('Cancelled');
		$statusID[] = $this->helper->getStatusIDFromStatusName('Decline');

		//if($statusID != $tblServiceBooking->status)
		if(!in_array($tblServiceBooking->status, $statusID))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_BOOKING_STATUS_NOT_BOOKING'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblCompany->load($tblServiceBooking->company_id);

		if($tblCompany->userid == $this->IJUserID)
		{
			$tblServiceBooking->deleted_by_company = 1;
		}
		elseif($tblServiceBooking->user_id == $this->IJUserID)
		{
			$tblServiceBooking->deleted_by_user = 1;
		}



		/*if($tblCompany->userid != $this->IJUserID)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}*/

		//$tblServiceBooking->is_deleted = 1;

		if(!$tblServiceBooking->store())
		{
			$this->jsonarray['code'] = 500;

			return $this->jsonarray;
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"deleteBooking",
	 * 		"taskData":{
	 * 			"serviceBookingID":"number"
	 * 		}
	 * 	}
	 */
	function deleteBooking()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$BookingID = IJReq::getTaskData('serviceBookingID',0,'int');

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblServiceBooking = JTable::getInstance('ServiceBooking', 'BctedTable');

		$tblServiceBooking->load($BookingID);

		if(!$tblServiceBooking->service_booking_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$statusID = $this->helper->getStatusIDFromStatusName('Request');

		if($statusID != $tblServiceBooking->status)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_BOOKING_STATUS_NOT_BOOKING'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblCompany->load($tblServiceBooking->company_id);

		if($tblCompany->userid != $this->IJUserID)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblServiceBooking->is_deleted = 1;

		if(!$tblServiceBooking->store())
		{
			$this->jsonarray['code'] = 500;

			return $this->jsonarray;
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"company",
	 * 		"extTask":"getAccountHistory",
	 * 		"taskData":{
	 *   		"companyID":"number",
	 *   		"pageNO":"number"
	 * 		}
	 * 	}
	 */
	function getAccountHistory()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$companyID   = IJReq::getTaskData('companyID',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblCompany = JTable::getInstance('Company', 'BctedTable');

		$tblCompany->load($companyID);

		if(!$tblCompany->company_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVALID_TABLE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblCompany->userid != $this->IJUserID)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_PERMISSION_DENIED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('sb.*')
			->from($db->quoteName('#__bcted_service_booking','sb'))
			->where($db->quoteName('sb.company_id') . ' = ' . $db->quote($tblCompany->company_id))
			->where($db->quoteName('sb.status') . ' = ' . $db->quote(5))
			->order($db->quoteName('sb.service_booking_id') . ' DESC');

		$query->select('up.last_name,up.phoneno,up.avatar,up.about')
			->join('LEFT','#__bcted_user_profile AS up ON up.userid=sb.user_id');

		$query->select('u.name,u.username')
			->join('LEFT','#__users AS u ON u.id=sb.user_id');

		$query->select('cs.service_name,cs.service_price')
			->join('LEFT','#__bcted_company_services AS cs ON cs.service_id=sb.service_id');

		/*$query->select('')
			->join('LEFT','#__bcted_venue AS v ON v.venue_id=vb.venue_id');*/

			//venue_table_id

		// Set the query and load the result.
		$db->setQuery($query);

		$accountHistories = $db->loadObjectList();

		if(count($accountHistories) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TALBES_BOOKING_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);
			return false;
		}

		$pageNO    = IJReq::getTaskData('pageNO',0);
		$pageLimit = BCTED_GENERAL_LIST_LIMIT;

		if($pageNO==0 || $pageNO==1){
			$startFrom=0;
		}else{
			$startFrom = $pageLimit*($pageNO-1);
		}

		$accountHistories =array_slice($accountHistories,$startFrom,$pageLimit);

		$blockedUsers = $this->helper->getBlackList($tblCompany->company_id,'Company');

		$historyResult = array();

		foreach ($accountHistories as $key => $history)
		{

			$tmpData = array();
			$tmpData['date']             = date('d-m-Y',strtotime($history->service_booking_datetime));
			$tmpData['refernce']         = $history->service_booking_id;
			$tmpData['amount']           = number_format($history->total_price);
			$tmpData['amountPayable']    = number_format($history->amount_payable);
			$tmpData['userID']           = $history->user_id;
			$tmpData['companyID']        = $history->company_id;
			$tmpData['companyServiceID'] = $history->service_id;
			$tmpData['serviceName']      = $history->service_name;
			$tmpData['username']         = $history->name;
			$tmpData['userPhone']        = ($history->phoneno)?$history->phoneno:'';

			$tmpData['isBlocked'] = (in_array($history->user_id, $blockedUsers))?"1":"0";
			$tmpData['connectionID'] = $this->helper->getMessageConnectionOnly($history->user_id,$tblCompany->userid);

			$historyResult[] = $tmpData;
		}

		if(count($historyResult) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_COMPANY_SERVICE_BOOKING_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);
			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($historyResult);
		$this->jsonarray['accountHistories'] = $historyResult;

		return $this->jsonarray;

	}



	function uplaodFile($file,$elementType,$userID)
	{
		//$file = JRequest::getVar('image','','FILES','array');
		$uploadedImage = "";
		$uploadLimit	= 8;
		$uploadLimit	= ( $uploadLimit * 1024 * 1024 );

		if(is_array($file) && isset($file['size']) && $file['size']>0)
		{
			jimport('joomla.filesystem.file');
			jimport('joomla.utilities.utility');

			if( filesize( $file['tmp_name'] ) > $uploadLimit && $uploadLimit != 0 )
			{
				/*IJReq::setResponseCode(416);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_IMAGE_FILE_SIZE_EXCEEDED'));
				return false;*/
				return '';
			} // End of if( filesize( $file['tmp_name'] ) > $uploadLimit && $uploadLimit != 0 )

			//$imageMaxWidth	= 160;
			$filename = JApplication::getHash( $file['tmp_name'] . time() );
			$hashFileName	= JString::substr( $filename , 0 , 24 );
			$info['extension'] = pathinfo($file['name'],PATHINFO_EXTENSION);
			$info['extension'] = '.'.$info['extension'];

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/"))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/");
			}

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/".$elementType."/"))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/".$elementType."/");
			}

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/".$elementType."/". $userID . '/'))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/".$elementType."/". $userID . '/');
			}

			$storage      = JPATH_ROOT . '/images/bcted/'.$elementType.'/'. $userID . '/';
			$storageImage = $storage . '/' . $hashFileName .  $info['extension'] ;
			$uploadedImage   = 'images/bcted/'.$elementType.'/' .$userID .'/'. $hashFileName . $info['extension'] ;

			if(!JFile::upload($file['tmp_name'], $storageImage))
		    {
				//$venueImage = "";
				return '';
		    }

		    return $uploadedImage;

		} // End of if(is_array($file) && $file['size']>0)

		return '';
	}


}