<?php
/**
 * @package     iJoomerAdv.Site
 * @subpackage  com_ijoomeradv
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.helper' );
class users
{

	private $db;
	private $IJUserID;
	private $helper;
	private $defaultUserAvatar;
	private $defaultUserCover;
	private $jsonarray;

	function __construct()
	{
		$this->db                = JFactory::getDBO();
		$this->mainframe         =  JFactory::getApplication ();
		$this->IJUserID          = $this->mainframe->getUserState ( 'com_ijoomeradv.IJUserID', 0 ); //get login user id
		$this->my                = JFactory::getUser ( $this->IJUserID ); // set the login user object
		$this->helper            = new bctedAppHelper;
		//$this->defaultUserAvatar = JUri::root().'components/com_heartdart/assets/images/user-png.png';
		//$this->defaultUserCover = JUri::root().'components/com_heartdart/assets/images/hd_background.png';
		$this->jsonarray         = array();
	}

	 /* @example the json string will be like, :
	 * 	{
	 * 		"extName":"heartdart",
	 * 		"extView":"users",
	 * 		"extTask":"resizeProfileImages",
	 * 		"taskData":{
	 * 		}
	 * 	}
	 */
	function resizeProfileImages()
	{
		$allUsers = $this->helper->getLiveUsers();
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_heartdart/tables');


		foreach ($allUsers as $key => $userID)
		{
			$tblHDProfile       = JTable::getInstance('Profile', 'HeartdartTable');

			$profileID = $this->helper->getUserProfileID($userID);
			$tblHDProfile->load($profileID);

			if(!$tblHDProfile->profile_id)
			{
				continue;
			}

			$profileAvatar = $tblHDProfile->avatar;

			if(empty($profileAvatar))
			{
				continue;
			}

			$fullPath = JPATH_SITE."/".$profileAvatar;

			if(file_exists($fullPath))
			{
				$imageObj = new JImage($fullPath);
				// Resize the image using the SCALE_INSIDE method
				$imageObj->resize(200, 0, false, JImage::SCALE_INSIDE);

				unlink($fullPath);

				// Write it to disk
				$imageObj->toFile($fullPath);

				//echo $fullPath;
				//exit;
			}
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;

	}

	 /* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"users",
	 * 		"extTask":"getProfile",
	 * 		"taskData":{
	 * 			"userID":"number"
	 * 		}
	 * 	}
	 */
	function getProfile()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$userID = IJReq::getTaskData('userID',$this->IJUserID,'int');

		//$this->syncFriendCount();
		$hasSetting = 0;

		if($userID == $this->IJUserID)
		{
			$hasSetting = 1;
		}

		$userProfile = $this->helper->getUserProfile($userID);

		$userDetail = $this->helper->formatProfileData($userProfile,$hasSetting);

		$this->jsonarray['code'] = 200;
		$this->jsonarray['userData'] = $userDetail;

		$userType = $this->helper->getUserGroup($this->IJUserID);
		$elemntDetail = array();

		if($userType == "Club")
		{
			$elementID = $this->helper->getUserVenueID($this->IJUserID);

			$elemntDetail = $this->helper->getVenueDetail($elementID);
			$canChangeCurrency = $this->helper->is_booking_in_venue($elementID);

			if($canChangeCurrency == 0)
			{
				$elemntDetail['canChangeCurrency'] = 1;
			}
			else
			{
				$elemntDetail['canChangeCurrency'] = 0;
			}

		}
		else if($userType == "ServiceProvider")
		{
			$elementID = $this->helper->getUserCompanyID($this->IJUserID);

			$elemntDetail = $this->helper->getCompanyDetail($elementID);
			$canChangeCurrency = $this->helper->is_booking_in_company($elementID);

			if($canChangeCurrency == 0)
			{
				$elemntDetail['canChangeCurrency'] = 1;
			}
			else
			{
				$elemntDetail['canChangeCurrency'] = 0;
			}
		}
		else
		{
			$elementID = $this->helper->getUserProfileID($this->IJUserID);
		}

		$this->jsonarray['elementType'] = $userType;
		$this->jsonarray['elementID'] = $elementID;
		$this->jsonarray['elementDetail'] = $elemntDetail;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"users",
	 * 		"extTask":"getUsersList",
	 * 		"taskData":{
	 * 			"emails":"string",
	 * 			"fbIDs":"string"
	 * 		}
	 * 	}
	 */
	function getUsersList()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse( 704 );
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$userL = JFactory::getUser();

		$emails = IJReq::getTaskData('emails','','string');
		$fbIDs  = IJReq::getTaskData('fbIDs','','string');
		$myFbId  = IJReq::getTaskData('myFbId','','string');
		/*echo $fbIDs;
		exit;*/

		$myFbId = trim($myFbId);

		if(!empty($myFbId))
		{
			JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
			$tblProfile       = JTable::getInstance('Profile', 'BctedTable');
			$profileID = $this->helper->getUserProfileID($this->IJUserID);
			$tblProfile->load($profileID);

			$tblProfile->fbid = trim($myFbId);

			$tblProfile->store();
		}



		if(empty($emails) && empty($fbIDs))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_BCTED_USER_INVALID_USERLIST_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$emailFounds = array();
		$fbIDFounds  = array();

		if(!empty($emails))
		{
			$emails = explode(",", $emails);
			$emails = implode("','", $emails);
			$emails = "'".$emails."'";



			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('id')
				->from($db->quoteName('#__users'))
				->where($db->quoteName('email') . ' IN (' . $emails . ')')
				->where($db->quoteName('block') . ' = ' . $db->quote(0));

			// Set the query and load the result.
			$db->setQuery($query);

			$emailFounds = $db->loadColumn();
		}

		if(!empty($fbIDs))
		{
			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('userid')
				->from($db->quoteName('#__bcted_user_profile'))
				->where($db->quoteName('fbid') . ' IN (' . $fbIDs . ')')
				->where($db->quoteName('user_type_for_push') . ' = ' . $db->quote('guest'));

			/*echo $query->dump();
			exit;*/

			// Set the query and load the result.
			$db->setQuery($query);

			$fbIDFounds = $db->loadColumn();
		}

		if(count($emailFounds) == 0 && count($fbIDFounds)==0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_BCTED_USER_USERS_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$foundArray = array();
		if($emailFounds)
		{
			$foundArray = array_merge($foundArray,$emailFounds);
		}
		if($fbIDFounds)
		{
			$foundArray = array_merge($foundArray,$fbIDFounds);
		}

		$foundArray = array_unique($foundArray);
		$userDetail =array();

		foreach ($foundArray as $key => $userID)
		{
			$userProfile = $this->helper->getUserProfile($userID);

			$userDetail[] = $this->helper->formatProfileData($userProfile,0);
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($userDetail);
		$this->jsonarray['users'] = $userDetail;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"users",
	 * 		"extTask":"updateProfile",
	 * 		"taskData":{
	 * 			"firstName":"string",
	 * 			"lastName":"string",
	 * 			"password":"string",
	 * 			"companyName":"string",
	 * 			"location":"string",
	 * 			"about":"string",
	 * 			"timings":"string",
	 * 			"connectToFacebook":"0/1",
	 * 			"receiveMessage":"0/1",
	 * 			"updateMyBookingStatus":"0/1",
	 * 			"amenities":"string",
	 * 			"workingDays":"string (1,2,3,4,5,6,7)"
	 * 		}
	 * 	}
	 */
	function updateProfile()
	{
		/*echo "call iin update profile";
		exit;*/
		if(!$this->IJUserID)
		{
			IJReq::setResponse( 704 );
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$userL = JFactory::getUser();

		$userType = $this->helper->getUserGroup($this->IJUserID);

		$firstName   = IJReq::getTaskData('firstName','','string');
		$lastName    = IJReq::getTaskData('lastName','','string');
		$password    = IJReq::getTaskData('password','','string');
		$phoneno     = IJReq::getTaskData('phoneno','','string');

		$longitude   = IJReq::getTaskData('longitude','','string');
		$latitude    = IJReq::getTaskData('latitude','','string');

		$companyName = IJReq::getTaskData('companyName','','string');
		$location    = IJReq::getTaskData('location','','string');
		$about       = IJReq::getTaskData('about','','string');
		//$timings     = IJReq::getTaskData('timings','','string');
		$amenities   = IJReq::getTaskData('amenities','','string');

		$city     = IJReq::getTaskData('city','','string');
		$country  = IJReq::getTaskData('country','','string');
		$currency = IJReq::getTaskData('currency','','string');

		$hasVideo = IJReq::getTaskData('hasVideo',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblProfile       = JTable::getInstance('Profile', 'BctedTable');
		$profileID = $this->helper->getUserProfileID($this->IJUserID);
		$tblProfile->load($profileID);

		$params = json_decode($tblProfile->params);

		if(isset($params->settings->social->connectToFacebook))
		{
			//$connectToFacebook = IJReq::getTaskData('connectToFacebook',$params->settings->social->connectToFacebook,'string');
		}
		$connectToFacebook = IJReq::getTaskData('connectToFacebook',0,'int');
		$receiveRequest = IJReq::getTaskData('receiveRequest','','string');
		$receiveMessage = IJReq::getTaskData('receiveMessage','','string');
		$updateMyBookingStatus = IJReq::getTaskData('updateMyBookingStatus','','string');

		/*
		if(!empty($connectToFacebook))
		{
			echo "not Empty";
		}
		else
		{
			echo "empty";
		}
		exit;

		echo "connectToFacebook : " . $connectToFacebook;
		echo "<br />";
		echo "receiveRequest : " . $receiveRequest;
		echo "<br />";
		echo "receiveMessage : " . $receiveMessage;
		echo "<br />";
		echo "updateMyBookingStatus : " . $updateMyBookingStatus;
		echo "<br />";
		exit;*/


		$newParams = array();
		//if(!empty($connectToFacebook))
		//{
			if(isset($params->settings->social->connectToFacebook))
			{
				$newParams['settings']['social']['connectToFacebook']     = IJReq::getTaskData('connectToFacebook','0','string');
			}
		//}

		//if(!empty($receiveRequest))
		//{
			if(isset($params->settings->pushNotification->receiveRequest))
			{
				$newParams['settings']['pushNotification']['receiveRequest']        = IJReq::getTaskData('receiveRequest','0','string');
			}
		//}

		//if(!empty($receiveMessage))
		//{
			$newParams['settings']['pushNotification']['receiveMessage']        = IJReq::getTaskData('receiveMessage','0','string');
		//}

		//if(!empty($updateMyBookingStatus))
		//{
			$newParams['settings']['pushNotification']['updateMyBookingStatus'] = IJReq::getTaskData('updateMyBookingStatus','0','string');
		//}


		$updtUser = 0;

		if($userType == 'Club')
		{
			$venueID = $this->helper->getUserVenueID($this->IJUserID);
			$tblVenue = JTable::getInstance('Venue', 'BctedTable');

			$attire  = IJReq::getTaskData('attire','0','string');
			$food    = IJReq::getTaskData('food','0','string');
			$smoking = IJReq::getTaskData('smoking','0','string');
			$sign    = IJReq::getTaskData('sign','0','string');
			// $rating  = IJReq::getTaskData('rating','0','string');

			$workingDays = IJReq::getTaskData('workingDays','','string');
			$workingDays = trim($workingDays);

			$fromTime = IJReq::getTaskData('fromTime','','string');
			$toTime   = IJReq::getTaskData('toTime','','string');

			$fromTimeArray = explode(":", $fromTime);
			$toTimeArray = explode(":", $toTime);

			if(count($fromTimeArray)==1)
			{
				$fromTime = $fromTimeArray[0].":"."00:00";
			}
			else if(count($fromTimeArray)==2)
			{
				$fromTime = $fromTimeArray[0].":".$fromTimeArray[1].":00";
			}
			else if(count($fromTimeArray)>=3)
			{
				$fromTime = $fromTimeArray[0].":".$fromTimeArray[1].":".$fromTimeArray[2];
			}

			if(count($toTimeArray)==1)
			{
				$toTime = $toTimeArray[0].":"."00:00";
			}
			else if(count($toTimeArray)==2)
			{
				$toTime = $toTimeArray[0].":".$toTimeArray[1].":00";
			}
			else if(count($toTimeArray)>=3)
			{
				$toTime = $toTimeArray[0].":".$toTimeArray[1].":".$toTimeArray[2];
			}

			$tblVenue->load($venueID);

			$tblVenue->is_smart   = ($attire)?1:0;
			$tblVenue->is_casual  = ($attire)?0:1;
			$tblVenue->is_food    = ($food)?1:0;
			$tblVenue->is_drink   = ($food)?0:1;
			$tblVenue->is_smoking = $smoking;

			if(!empty($country))
			{
				$tblVenue->country = $country;
			}

			if(!empty($city))
			{
				$tblVenue->city = $city;
			}

			if(!empty($fromTime))
			{
				if($this->helper->isTime($fromTime,true,true))
				{
					$tblVenue->from_time = $fromTime;
				}
			}

			if(!empty($toTime))
			{
				if($this->helper->isTime($toTime,true,true))
				{
					$tblVenue->to_time = $toTime;
				}
			}

			if(!empty($currency))
			{
				$tblVenue->currency_code = strtoupper($currency);
				$tblVenue->currency_sign = '';

				if(strtoupper($currency) == 'GBP')
					$tblVenue->currency_sign = "£";
				elseif(strtoupper($currency) == 'EUR')
					$tblVenue->currency_sign = "€";
				elseif(strtoupper($currency) == 'AED')
					$tblVenue->currency_sign = "AED";
				elseif(strtoupper($currency) == 'USD')
					$tblVenue->currency_sign = "$";
				elseif(strtoupper($currency) == 'CAD')
					$tblVenue->currency_sign = "$";
				elseif(strtoupper($currency) == 'AUD')
					$tblVenue->currency_sign = "$";
				else
					$tblVenue->currency_code='';
			}

			// $tblVenue->rating      = $rating;
			$tblVenue->venue_signs = $sign;

			if(!empty($workingDays))
			{
				$workingDaysArray = explode(",", $workingDays);
				$workingDaysArray = array_unique($workingDaysArray);
				$workingDays = implode(",", $workingDaysArray);

				$tblVenue->working_days = $workingDays;
			}



			$updtVenue = 0;

			$file = JRequest::getVar('image','','FILES','array');

			if(is_array($file) && isset($file['size']) && $file['size']>0)
			{
				$venueImage = $this->uplaodFile($file,'venue',$this->IJUserID);

				//echo JPATH_SITE.' ||'.$venueImage; ///var/www/html/dev ||images/bcted/venue/442/674130a524c49da34adb27b6.jpg


				if(!empty($venueImage))
				{
					if(!empty($tblVenue->venue_image) && file_exists(JUri::root().$tblVenue->venue_image))
					{
						unlink(JUri::root().$tblVenue->venue_image);
					}

					if(!empty($tblVenue->venue_thumb_image) && file_exists(JUri::root().$tblVenue->venue_thumb_image))
					{
						unlink(JUri::root().$tblVenue->venue_thumb_image);
					}

					$tblVenue->venue_image = $venueImage;
					$updtVenue = 1;

					/*echo JPATH_ROOT . "images/bcted/venue/". $this->IJUserID . "/thumb";
					exit;*/

					if(!JFolder::exists(JPATH_ROOT . "/images/bcted/venue/". $this->IJUserID . "/thumb"))
					{
						JFolder::create(JPATH_ROOT . "/images/bcted/venue/". $this->IJUserID . "/thumb");
					}

					$pathInfo = pathinfo(JPATH_SITE.'/'.$venueImage);
					$thumbPath = $pathInfo['dirname'].'/thumb/thumb_'.$pathInfo['basename'];
					$storeThumbPath = "images/bcted/venue/". $this->IJUserID . "/thumb/thumb_".$pathInfo['basename'];

					/*echo JPATH_SITE.'/'.$venueImage."<pre>";
					print_r($pathInfo);
					echo "</pre>";
					exit;
					<pre>Array
					(
					    [dirname] => /var/www/html/dev/images/bcted/venue/442
					    [basename] => ac960b7bf781963600c1311e.jpg
					    [extension] => jpg
					    [filename] => ac960b7bf781963600c1311e
					)
					</pre>*/


					//JFolder::create(JPATH_ROOT . "images/bcted/".$elementType."/". $userID . '/');

					$this->helper->createThumb(JPATH_SITE.'/'.$venueImage,$thumbPath);

					//echo JUri::base().$storeThumbPath;
					/*exit;*/

					if(file_exists(JPATH_SITE.'/'.$storeThumbPath))
					{
						//echo JUri::base().$storeThumbPath;
						$tblVenue->venue_thumb_image = $storeThumbPath;
					}

				}
			}

			if($tblVenue->licence_type == 'basic')
			{
				$tblVenue->venue_video = "";
				$updtVenue = 1;
			}
			else
			{
				$video = JRequest::getVar('video','','FILES','array');

				/*echo "<pre>";
				print_r($video);
				echo "</pre>";
				exit;*/

				if(is_array($video) && isset($video['size']) && $video['size']>0)
				{
					$venueVideo= $this->uplaodFile($video,'venue',$this->IJUserID);

					/*echo $venueVideo;
					echo "<br />";
					echo getcwd();
					$venueVideo =images/bcted/venue/147/7c056ae4a57123dbe38e5e6a.MOV
					getcwd() = /var/www/html/dev
					exit;*/

					if(!empty($venueVideo))
					{
						$storageImage = getcwd().'/'.$venueVideo;
						/*$videoOut = getcwd().'/images/bcted/venue/'.$this->IJUserID.'/';
						$convertedvideo = $this->helper->convertVideo2($storageImage, $videoOut, '400x300', false);

						if(!empty($convertedvideo))
						{
							$venueVideo	= 'images/bcted/venue/'.$this->IJUserID.'/'.$convertedvideo;
						}*/

						$source        = $storageImage;
						$orignalFile   = pathinfo($source);
						$videoExtAllow = array('mov','mp4');
						$storeImg      = $venueVideo;
						$storeFlv      = "";
						$storeMp4      = "";
						$storeWebm     = "";

						$user = JFactory::getUser();

						if(!empty($venueVideo) && in_array(strtolower($orignalFile['extension']), $videoExtAllow))
						{
							$destFlv = $orignalFile['dirname'].'/'.$orignalFile['filename'].'_flv.flv';
							$storeFlv =  'images/bcted/venue/'.$user->id.'/'.$orignalFile['filename'].'_flv.flv';
							$command = "/usr/bin/ffmpeg -y -i $source -g 30 -vcodec copy -acodec copy $destFlv 2>".JPATH_SITE."/ffmpeg_test1.txt";
							$output = shell_exec($command);

							$destMp4 = $orignalFile['dirname'].'/'.$orignalFile['filename'].'_mp4.mp4';
							$storeMp4 =  'images/bcted/venue/'.$user->id.'/'.$orignalFile['filename'].'_mp4.mp4';
							$command = "/usr/bin/ffmpeg -i ".$destFlv." -ar 22050 -vf \"transpose=1\" ".$destMp4; //Wroking
							$output = shell_exec($command);

							$destPng = $orignalFile['dirname'].'/'.$orignalFile['filename'].'_png.png';
							$storeImg =  'images/bcted/venue/'.$user->id.'/'.$orignalFile['filename'].'_png.png';
							$command = "/usr/bin/ffmpeg -i $destMp4 -r 1 -s 700x600 -f image2 $destPng";
							$output = shell_exec($command);

							$destWebm = $orignalFile['dirname'].'/'.$orignalFile['filename'].'_webm.webm';
							$storeWebm =  'images/bcted/venue/'.$user->id.'/'.$orignalFile['filename'].'_webm.webm';
							$command = "/usr/bin/ffmpeg -i ".$source." -acodec libvorbis -ac 2 -ab 96k -ar 44100 -b 345k -s 640x360 -vf \"transpose=1\" ".$destWebm; //Wroking
							$output = shell_exec($command);

							/*echo $command;
							exit;*/

							if(!empty($tblVenue->venue_video) && file_exists(JUri::root().$tblVenue->venue_video))
							{
								unlink(JUri::root().$tblVenue->venue_image);
								unlink(JUri::root().$tblVenue->venue_video);
								unlink(JUri::root().$tblVenue->venue_video_flv);
								unlink(JUri::root().$tblVenue->venue_video_mp4);
								unlink(JUri::root().$tblVenue->venue_video_webm);
							}

							$tblVenue->venue_image      = $storeImg;
							$tblVenue->venue_video      = $venueVideo;
							$tblVenue->venue_video_flv  = $storeFlv;
							$tblVenue->venue_video_mp4  = $storeMp4;
							$tblVenue->venue_video_webm = $storeWebm;

							$updtVenue = 1;
						}
					}

					/*if(!empty($venueVideo))
					{
						if(!empty($tblVenue->venue_video) && file_exists(JUri::root().$tblVenue->venue_video))
						{
							unlink(JUri::root().$tblVenue->venue_video);
						}

						$tblVenue->venue_video = $venueVideo;
						$updtVenue = 1;
					}*/
				}
				else if($hasVideo == 0)
				{
					$tblVenue->venue_video = "";
					$updtVenue = 1;
				}
			}



			if(!empty($longitude))
			{
				$tblVenue->longitude = $longitude;
				$updtVenue = 1;
			}

			if(!empty($latitude))
			{
				$tblVenue->latitude = $latitude;
				$updtVenue = 1;
			}

			if(!empty($companyName))
			{
				$tblVenue->venue_name = $companyName;
				$updtVenue = 1;
			}

			if(!empty($location))
			{
				$tblVenue->venue_address = $location;
				$updtVenue = 1;
			}

			if(!empty($about))
			{
				$tblVenue->venue_about = $about;
				$updtVenue = 1;
			}

			if(!empty($amenities))
			{
				$tblVenue->venue_amenities = $amenities;
				$updtVenue = 1;
			}

			if(!empty($timings))
			{
				$tblVenue->venue_timings = $timings;
				$updtVenue = 1;
			}


			$tblVenue->venue_modified = date('Y-m-d h:i:s');
			$tblVenue->time_stamp = time();

			/*echo "<pre>";
			print_r($tblVenue);
			echo "</pre>";
			exit;*/

			$tblVenue->store();

		}
		else if($userType == 'ServiceProvider')
		{
			$companyID = $this->helper->getUserCompanyID($this->IJUserID);

			//$venueID = $this->helper->getUserVenueID($this->IJUserID);
			$tblCompany = JTable::getInstance('Company', 'BctedTable');

			$attire     = IJReq::getTaskData('attire','0','string');
			$restaurant = IJReq::getTaskData('restaurant','0','string');
			$smoking    = IJReq::getTaskData('smoking','0','string');
			$rating     = IJReq::getTaskData('rating','0','string');
			$price      = IJReq::getTaskData('price','0','string');
			$drink      = IJReq::getTaskData('drink','0','string');

			$tblCompany->load($companyID);

			$tblCompany->attire     = $attire;
			$tblCompany->restaurant = $restaurant;
			$tblCompany->smoking    = $smoking;
			$tblCompany->rating     = $rating;
			$tblCompany->price      = $price;
			$tblCompany->drink      = $drink;



			$file = JRequest::getVar('image','','FILES','array');

			if(is_array($file) && isset($file['size']) && $file['size']>0)
			{
				$venueImage = $this->uplaodFile($file,'ServiceProvider',$this->IJUserID);

				if(!empty($venueImage))
				{
					if(!empty($tblCompany->company_image) && file_exists(JUri::root().$tblCompany->company_image))
					{
						unlink(JUri::root().$tblVenue->company_image);
					}

					$tblCompany->company_image = $venueImage;
				}
			}

			if(!empty($currency))
			{
				$tblCompany->currency_code = strtoupper($currency);
				$tblCompany->currency_sign = '';

				if(strtoupper($currency) == 'GBP')
					$tblCompany->currency_sign = "£";
				elseif(strtoupper($currency) == 'EUR')
					$tblCompany->currency_sign = "€";
				elseif(strtoupper($currency) == 'AED')
					$tblCompany->currency_sign = "AED";
				elseif(strtoupper($currency) == 'USD')
					$tblCompany->currency_sign = "$";
				elseif(strtoupper($currency) == 'CAD')
					$tblCompany->currency_sign = "$";
				elseif(strtoupper($currency) == 'AUD')
					$tblCompany->currency_sign = "$";
				else
					$tblCompany->currency_code='';
			}

			if(!empty($city))
			{
				$tblCompany->city = $city;
			}

			if(!empty($country))
			{
				$tblCompany->country = $country;
			}

			if(!empty($longitude))
			{
				$tblCompany->longitude = $longitude;
			}

			if(!empty($latitude))
			{
				$tblCompany->latitude = $latitude;
			}

			if(!empty($companyName))
			{
				$tblCompany->company_name = $companyName;
			}

			if(!empty($location))
			{
				$tblCompany->company_address = $location;
			}

			if(!empty($about))
			{
				$tblCompany->company_about = $about;
			}

			$tblCompany->company_modified = date('Y-m-d h:i:s');
			$tblCompany->time_stamp = time();

			$tblCompany->store();
		}
		else
		{
			if(!empty($firstName))
			{
				$userData['name'] = $firstName;
				$updtUser = 1;
			}

			$city     = IJReq::getTaskData('city','','string');



			if(!empty($city))
			{
				$tblProfile->city = $city;
			}

			/*echo "<pre>";
			print_r($tblProfile);
			echo "</pre>";
			exit;*/

			if(!empty($lastName))
			{
				$tblProfile->last_name = $lastName;
			}

			if(!empty($phoneno))
			{
				$tblProfile->phoneno = $phoneno;
			}

			if(!empty($city))
			{
				$tblProfile->city = $city;
			}
		}


		if(count($newParams)!=0)
		{
			$tblProfile->params = json_encode($newParams);
		}

		try
		{
			$tblProfile->store();
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		$user = new JUser;
		$user->load($userL->id);

		if(!empty($city))
		{
			$userData['city'] = $city;
			//$updtUser = 1;
		}

		if(!empty($password))
		{
			$userData['password'] = $password;
			$userData['password2'] = $password;
			$updtUser = 1;
		}

		if($updtUser)
		{
			$user->bind($userData);

			if (!$user->save())
			{
				IJReq::setResponseCode(500);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_HEARTDART_USER_DETAIL_NOT_SAVE'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}
		}

		$this->getProfile();
		//$this->jsonarray['code'] = 200;

		return $this->jsonarray;

		//$userName  = IJReq::getTaskData('username',$userL->username,'string');



		/*$tblProfile = JTable::getInstance('Profile', 'BctedTable');
		$profileID = $this->helper->getUserProfileID($userL->id);
		$tblProfile->load($profileID);

		$email     = $userL->email;

		$user = new JUser;
		$user->load($userL->id);

		if(!empty($password))
		{
			$userData['password'] = $password;
			$userData['password2'] = $password;
		}

		$userData['username'] = $userName;
		$userData['name']     = $userName;
		$userData['email']    = $email;
		$user->bind($userData);

		if (!$user->save())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_HEARTDART_USER_DETAIL_NOT_SAVE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}*/


	}

	function uplaodFile($file,$elementType,$userID)
	{
		//$file = JRequest::getVar('image','','FILES','array');
		$uploadedImage = "";
		$uploadLimit	= 8;
		$uploadLimit	= ( $uploadLimit * 1024 * 1024 );

		if(is_array($file) && isset($file['size']) && $file['size']>0)
		{
			jimport('joomla.filesystem.file');
			jimport('joomla.utilities.utility');

			if( filesize( $file['tmp_name'] ) > $uploadLimit && $uploadLimit != 0 )
			{
				/*IJReq::setResponseCode(416);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_IMAGE_FILE_SIZE_EXCEEDED'));
				return false;*/
				return '';
			} // End of if( filesize( $file['tmp_name'] ) > $uploadLimit && $uploadLimit != 0 )

			//$imageMaxWidth	= 160;
			$filename = JApplication::getHash( $file['tmp_name'] . time() );
			$hashFileName	= JString::substr( $filename , 0 , 24 );
			$info['extension'] = pathinfo($file['name'],PATHINFO_EXTENSION);
			$info['extension'] = '.'.$info['extension'];

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/"))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/");
			}

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/".$elementType."/"))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/".$elementType."/");
			}

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/".$elementType."/". $userID . '/'))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/".$elementType."/". $userID . '/');
			}

			$storage      = JPATH_ROOT . '/images/bcted/'.$elementType.'/'. $userID . '/';
			$storageImage = $storage . '/' . $hashFileName .  $info['extension'] ;
			$uploadedImage   = 'images/bcted/'.$elementType.'/' .$userID .'/'. $hashFileName . $info['extension'] ;

			if(!JFile::upload($file['tmp_name'], $storageImage))
		    {
				//$venueImage = "";
				return '';
		    }

		    return $uploadedImage;

		} // End of if(is_array($file) && $file['size']>0)

		return '';
	}

	// Not Used
	function updateVenue($venueID)
	{
		$loginUser = JFactory::getUser();
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_heartdart/tables');

		$venueName = IJReq::getTaskData('venueName','','string');
		$location  = IJReq::getTaskData('location','','string');
		$about     = IJReq::getTaskData('about','','string');
		$amenities = IJReq::getTaskData('amenities','','string');
		$timings   = IJReq::getTaskData('timings','','string');

		$attire     = IJReq::getTaskData('attire','','string');
		$restaurant = IJReq::getTaskData('restaurant','','string');
		$smoking    = IJReq::getTaskData('smoking','','string');
		$rating     = IJReq::getTaskData('rating','','string');
		$price      = IJReq::getTaskData('price','','string');
		$drink      = IJReq::getTaskData('drink','','string');



		if(!empty($venueName))
		{
			$data['venue_name'] = $venueName;
		}

		if(!empty($location))
		{
			$data['venue_address'] = $location;
		}

		if(!empty($about))
		{
			$data['venue_about'] = $about;
		}

		if(!empty($amenities))
		{
			$data['venue_amenities'] = $amenities;
		}

		if(!empty($timings))
		{
			$data['venue_timings'] = $timings;
		}

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');

		$tblVenue->load($venueID);

		$data['attire']     = ($attire)?$attire:$tblVenue->attire;
		$data['restaurant'] = ($restaurant)?$restaurant:$tblVenue->restaurant;
		$data['smoking']    = ($smoking)?$smoking:$tblVenue->smoking;
		$data['rating']     = ($rating)?$rating:$tblVenue->rating;
		$data['price']      = ($price)?$price:$tblVenue->price;
		$data['drink']      = ($drink)?$drink:$tblVenue->drink;

		$file = JRequest::getVar('image','','FILES','array');
		$venueImage = "";
		$uploadLimit	= 8;
		$uploadLimit	= ( $uploadLimit * 1024 * 1024 );

		if(is_array($file) && isset($file['size']) && $file['size']>0)
		{
			jimport('joomla.filesystem.file');
			jimport('joomla.utilities.utility');

			if( filesize( $file['tmp_name'] ) > $uploadLimit && $uploadLimit != 0 )
			{
				IJReq::setResponseCode(416);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_IMAGE_FILE_SIZE_EXCEEDED'));
				return false;
			} // End of if( filesize( $file['tmp_name'] ) > $uploadLimit && $uploadLimit != 0 )

			$imageMaxWidth	= 160;
			$filename = JApplication::getHash( $file['tmp_name'] . time() );
			$hashFileName	= JString::substr( $filename , 0 , 24 );
			$info['extension'] = pathinfo($file['name'],PATHINFO_EXTENSION);
			$info['extension'] = '.'.$info['extension'];

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/"))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/");
			}

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/venues/"))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/venues/");
			}

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/venues/". $loginUser->id . '/'))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/venues/". $loginUser->id . '/');
			}

			$storage      = JPATH_ROOT . '/images/bcted/venues/'. $loginUser->id . '/';
			$storageImage = $storage . '/' . $hashFileName .  $info['extension'] ;
			$venueImage   = 'images/bcted/venues/' .$loginUser->id .'/'. $hashFileName . $info['extension'] ;

			if(!JFile::upload($file['tmp_name'], $storageImage))
		    {
				$venueImage = "";
		    }

		} // End of if(is_array($file) && $file['size']>0)

		if(!empty($venueImage))
		{
			$data['venue_image'] = $venueImage;
		}

		if(count($data) != 0)
		{

			$tblVenue->bind($data);
			if($tblVenue->store())
			{
				IJReq::setResponse( 200 );
				//IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_HEARETDART_USER_PROFILE_NOT_SAVE'));

				IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
				return false;
			}
		}

		IJReq::setResponse( 500 );
		IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_HEARETDART_USER_PROFILE_NOT_SAVE'));
		IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
		return false;


	}

	/* @example the json string will be like, :
	  	{
	  		"extName":"bcted",
	  		"extView":"users",
	  		"extTask":"testing",
	  		"taskData":{
	  		}
	  	}
	 */
	function testing()
	{
		/*$currentDate = date('Y-m-d H:i:s');

		echo "Current Date" . $currentDate . "<br />";

		echo "l :" . date("l") . "<br />";
		echo "N :" . date("N") . "<br />";
		echo "w :" . date("w") . "<br />";*/

		$output = shell_exec("whereis ffmpeg");
		$outputArray = explode(" ", $output);

		echo "call<pre>";
		print_r($outputArray);
		echo "</pre>";


		exit;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"users",
	 * 		"extTask":"getLoyaltyPoints",
	 * 		"taskData":{
	 * 		}
	 * 	}
	 */
	function getLoyaltyPoints()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse( 704 );
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$currency = IJReq::getTaskData('currency','','string');

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_loyalty_point'))
			->where($db->quoteName('is_valid') . ' = ' . $db->quote('1'))
			->where($db->quoteName('user_id') . ' = ' . $db->quote($this->IJUserID))
			->order($db->quoteName('lp_id') . ' DESC');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$result = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		$totalPointEarn = 0;
		$pointEntry = array();

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/

		foreach ($result as $key => $value)
		{
			$tmpData = array();
			$tmpData['pointID'] = $value->lp_id;
			$tmpData['earnPoint'] = $value->earn_point;

			if($value->point_app == 'purchase.package')
			{
				$tmpData['earnPointType'] = "Purchase";
			}
			else if($value->point_app == 'purchase.venue')
			{
				$tmpData['earnPointType'] = "Purchase";
			}
			else if($value->point_app == 'purchase.service')
			{
				$tmpData['earnPointType'] = "Purchase";
			}
			else if($value->point_app == 'Payout')
			{
				$tmpData['earnPointType'] = "Payout";
			}
			else if($value->point_app == 'Referral')
			{
				$tmpData['earnPointType'] = "Referral";
			}
			else if($value->point_app == 'admin.added')
			{
				$tmpData['earnPointType'] = "Admin";
			}
			else if($value->point_app == 'admin.removed')
			{
				$tmpData['earnPointType'] = "Admin";
			}
			else if($value->point_app == 'venue.cancelbooking' || $value->point_app == 'service.cancelbooking')
			{
				$tmpData['earnPointType'] = "Cancelled";
			}
			else if($value->point_app == 'venue.noshow')
			{
				$tmpData['earnPointType'] = "Deduction";
			}

			$tmpData['pointID'] = date('d-m-Y',strtotime($value->created));

			$totalPointEarn = $totalPointEarn + $value->earn_point;
			$pointEntry[] = $tmpData;

		}

		$this->jsonarray['code']         = 200;
		$this->jsonarray['totalPoint']   = number_format($totalPointEarn,2);
		$this->jsonarray['pointEntries'] = $pointEntry;
		if(!empty($currency))
		{
			$this->jsonarray['currencyRate'] = $this->helper->convertCurrencyGoogle(1,strtoupper($currency),'USD');
		}
		else
		{
			$this->jsonarray['currencyRate'] = "";
		}


		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"users",
	 * 		"extTask":"getCityList",
	 * 		"taskData":{
	 * 		}
	 * 	}
	 */
	function getCityList()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse( 704 );
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('distinct(city)')
			->from($db->quoteName('#__bcted_venue'))
			->where($db->quoteName('venue_active') . ' = ' . $db->quote('1'))
			->where($db->quoteName('city') . ' <> ' . $db->quote(''))
			->order($db->quoteName('city') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$cities = $db->loadColumn();
		}
		catch (RuntimeException $e)
		{
			throw new RuntimeException($e->getMessage(), $e->getCode());
		}

		$totalPointEarn = 0;
		$pointEntry = array();

		if(count($cities) == 0)
		{
			$this->jsonarray['code'] = 204;

			return $this->jsonarray;
		}

		$resultCity = array();
		foreach ($cities as $key => $city)
		{
			$tmpData = array();
			$tmpData['name'] = $city;
			//$tmpData['value'] = $key;

			$resultCity[] = $tmpData;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['city'] = $resultCity;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"users",
	 * 		"extTask":"bctRefer",
	 * 		"taskData":{
	 * 			"emails":"string"
	 * 		}
	 * 	}
	 */
	function bctRefer()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse( 704 );
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$emails = IJReq::getTaskData('emails','','string');
		$loginUser = JFactory::getUser();

		//$this->helper->filterOnlyGuestEmail($emails);

		if(empty($emails))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_BCTED_USER_INVALID_USERLIST_DATA'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$bctRegiEmails = $this->helper->filterEmails($emails);

		$emailsArray = explode(",", $emails);
		$filterEmails = array();

		foreach ($emailsArray as $key => $singleEmail)
		{
			if(in_array($singleEmail, $bctRegiEmails['allRegEmail']))
			{
				if(in_array($singleEmail, $bctRegiEmails['service'])){ continue; }
				if(in_array($singleEmail, $bctRegiEmails['venue'])){ continue; }
				if(in_array($singleEmail, $bctRegiEmails['guest']) && $this->my->email == $singleEmail){ continue; }
			}

			$filterEmails[] = $singleEmail;
		}

		if(count($filterEmails)==0)
		{
			IJReq::setResponseCode(300);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVITE_NOT_VALID_SELF_EMAIL_OR_CLUB_SERVICE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*echo "Filter email<pre>";
		print_r($filterEmails);
		echo "</pre>";
		exit;*/

		$emailsArray = $filterEmails;

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('email')
			->from($db->quoteName('#__users'));

		// Set the query and load the result.
		$db->setQuery($query);

		$registeredEmails = $db->loadColumn();

		$query2 = $db->getQuery(true);

		// Create the base select statement.
		$query2->select('refer_email')
			->from($db->quoteName('#__bcted_user_refer'))
			->where($db->quoteName('userid') . ' = ' . $db->quote($this->IJUserID));

		// Set the query and load the result.
		$db->setQuery($query2);

		$alreadyReferEmails = $db->loadColumn();

		$emailsAfterRegi = array();

		foreach ($emailsArray as $key => $singleEmail)
		{
			if(!in_array($singleEmail, $registeredEmails))
			{
				$emailsAfterRegi[] = $singleEmail;
			}
		}

		$referEmails = array();
		foreach ($emailsAfterRegi as $key => $singleEmail)
		{
			if(!in_array($singleEmail, $alreadyReferEmails))
			{
				$referEmails[] = $singleEmail;
			}
		}

		//$referEmails = array_diff($emailsArray, $registeredEmails);

		//$referEmails = array_diff($referEmails, $alreadyReferEmails);

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');


		foreach ($referEmails as $key => $value)
		{
			$tblRefer = JTable::getInstance('Refer', 'BctedTable');
			$tblRefer->load(0);
			$referData['userid']        = $this->IJUserID;
			$referData['refer_email']   = $value;
			$referData['is_registered'] = 0;
			$referData['ref_user_id']   = 0;
			$referData['is_fp_done']    = 0;
			//$referData['fp_date']       = $this->IJUserID;
			$referData['created']       = date('Y-m-d H:i:s');
			$referData['time_stamp']    = time();

			$tblRefer->bind($referData);
			$tblRefer->store();
		}

		$this->jsonarray['code'] = 200;

		// Initialise variables.
		$app     = JFactory::getApplication();
		$config  = JFactory::getConfig();

		$site    = $config->get('sitename');
		$from    = $config->get('mailfrom');
		$sender  = $config->get('fromname');
		//$email = implode(",", $referEmails);
		$email = $referEmails;
		//$email   = $app->input->get('mailto');
		$subject = JText::sprintf('COM_BCTED_INVITE_BCT_USER_REFERAL_EMAIL_SBUJECT', $loginUser->name);

		// Build the message to send.
		//$msg     = JText::_('COM__SEND_EMAIL_MSG');
		$link = JUri::base();

		$link = '<a href="'.$link.'index.php">Click here</a>';
		/*echo $link;
		exit;*/

		$body    = JText::sprintf('COM_BCTED_INVITE_BCT_USER_REFERAL_EMAIL_BODY', $loginUser->name,$link);

		/*echo $body;
		exit;*/

		// Clean the email data.
		$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body,true);

		// Check for an error.
		/*if ($return !== true)
		{
			return new JException(JText::_('COM__SEND_MAIL_FAILED'), 500);
		}*/

		return $this->jsonarray;

	}
}