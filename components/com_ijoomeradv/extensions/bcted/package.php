<?php
/**
 * @package     iJoomerAdv.Site
 * @subpackage  com_ijoomeradv
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.helper' );
class package
{

	private $db;
	private $IJUserID;
	private $helper;
	private $jsonarray;

	function __construct()
	{
		$this->db                = JFactory::getDBO();
		$this->mainframe         =  JFactory::getApplication ();
		$this->IJUserID          = $this->mainframe->getUserState ( 'com_ijoomeradv.IJUserID', 0 ); //get login user id
		$this->my                = JFactory::getUser ( $this->IJUserID ); // set the login user object
		$this->helper            = new bctedAppHelper;
		$this->jsonarray         = array();
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"package",
	 * 		"extTask":"getPackages",
	 * 		"taskData":{
	 *
	 * 		}
	 * 	}
	 */
	function getPackages()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query1 = $db->getQuery(true);

		// Create the base select statement.
		$query->select('package_id')
			->from($db->quoteName('#__bcted_package'))
			->where($db->quoteName('package_date') . ' >=  ' .  $db->quote(date('Y-m-d')))
			->where($db->quoteName('package_active') . ' = ' . $db->quote('1'))
			->where($db->quoteName('venue_id') . ' <> ' . $db->quote('0'))
			->order($db->quoteName('package_date') . ' ASC');

		// Set the query and load the result.
		$db->setQuery($query);

		/*echo "<pre>";
		print_r($query->dump());
		echo "</pre>";
		exit;*/

		$packagesVenue = $db->loadColumn();

		try
		{
			$total     = count($packagesVenue);
			$resultPackage = $this->helper->getPakcagesDetail($packagesVenue);
		}
		catch (RuntimeException $e)
		{
			//throw new RuntimeException($e->getMessage(), $e->getCode());
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_($e->getMessage() . ' - ' . $e->getCode()));
			return false;
		}

		if(count($resultPackage) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGES_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		}

		$this->jsonarray['code']      = 200;
		$this->jsonarray['total']     = $total;
		//$this->jsonarray['pageLimit'] = $pageLimit;
		$this->jsonarray['packages']  = $resultPackage;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"package",
	 * 		"extTask":"createPackage",
	 * 		"taskData":{
	 * 			"packageName":"string",
	 * 			"packageDetail":"string",
	 * 			"price":"number"
	 * 		}
	 * 	}
	 */
	function createPackage()
	{
		$data['package_name']     = IJReq::getTaskData('packageName','','string');
		$data['package_details']  = IJReq::getTaskData('packageDetail','','string');
		$data['package_price']    = IJReq::getTaskData('price','','string');
		$data['package_created']  = date('Y-m-d H:i:s');
		$data['package_modified'] = date('Y-m-d H:i:s');
		$data['time_stamp']       = time();
		$data['package_active']   = 1;
		$data['user_id']          = 63;

		$file = JRequest::getVar('image','','FILES','array');

		if(is_array($file) && isset($file['size']) && $file['size']>0)
		{
			$packageImage = $this->uplaodFile($file,'package',63);

			if(!empty($packageImage))
			{
				/*if(!empty($tblTable->venue_table_image) && file_exists(JUri::root().$tblTable->venue_table_image))
				{
					unlink(JUri::root().$tblTable->venue_table_image);
				}*/

				$data['package_image'] = $packageImage;
			}
		}



		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblPackage = JTable::getInstance('Package', 'BctedTable');
		$tblPackage->load(0);

		$tblPackage->bind($data);

		if($tblPackage->store())
		{
			$this->jsonarray['code'] = 200;

			return $this->jsonarray;
		}

		$this->jsonarray['code'] = 500;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
		{
			"extName":"bcted",
			"extView":"package",
			"extTask":"sendPackageRequest",
			"taskData":{
				"packageID":"number",
				"location":"string",
				"maleCount":"number",
				"femaleCount":"number",
				"additionalInfo":"string"
			}
		}
	 */
	function sendPackageRequest()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$packageID = IJReq::getTaskData('packageID',0,'int');

		if(!$packageID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');

		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackage          = JTable::getInstance('Package', 'BctedTable');
		$tblVenue            = JTable::getInstance('Venue', 'BctedTable');
		$tblCompany          = JTable::getInstance('Company', 'BctedTable');

		$tblPackage->load($packageID);

		if(!$tblPackage->package_id || $tblPackage->package_active == 0)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$location       = IJReq::getTaskData('location','','string');
		$guestsCount    = IJReq::getTaskData('guestsCount',0, 'int');
		$maleCount    = IJReq::getTaskData('maleCount',0, 'int');
		$femaleCount    = IJReq::getTaskData('femaleCount',0, 'int');

		$additionalInfo = IJReq::getTaskData('additionalInfo','','string');
		$packageTime    = IJReq::getTaskData('time','','string');


		if(!$guestsCount)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BOOKING_INVALID_BOOKING_DETAIL'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblPackagePurchased->load(0);
		$packageName = $tblPackage->package_name;

		//if($tblPackage->venue_id)
		//{
			$tblVenue->load($tblPackage->venue_id);
			$purchaseData['status']           = 1;
			$purchaseData['user_status']      = 2;
		//}
		/*else if($tblPackage->company_id)
		{
			$tblCompany->load($tblPackage->company_id);
			$purchaseData['status']           = 6;
			$purchaseData['user_status']      = 3;
		}*/



		$purchaseData['package_id']                 = $tblPackage->package_id;
		$purchaseData['user_id']                    = $this->IJUserID;
		$purchaseData['venue_id']                   = $tblPackage->venue_id;
		$purchaseData['company_id']                 = $tblPackage->company_id;
		$purchaseData['total_price']                = $tblPackage->package_price * $guestsCount;

		$purchaseData['male_count']                 = $maleCount;
		$purchaseData['female_count']               = $femaleCount;
		$purchaseData['package_location']           = $location;
		$purchaseData['time_stamp']                 = time();
		$purchaseData['package_datetime']           = $tblPackage->package_date;
		$purchaseData['package_time']               = $packageTime;
		$purchaseData['package_number_of_guest']    = $guestsCount;
		$purchaseData['package_additional_info']    = $additionalInfo;
		$purchaseData['package_purchased_datetime'] = date('Y-m-d H:i:s');

		$purchaseData['booked_currency_code']       = $tblPackage->currency_code;
		$purchaseData['booked_currency_sign']       = $tblPackage->currency_sign;
		$purchaseData['can_invite']                 = 1;

		/*echo "<pre>";
		print_r($purchaseData);
		echo "</pre>";
		exit;*/

		$tblPackagePurchased->bind($purchaseData);

		if(!$tblPackagePurchased->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_BOOKING_FAILED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		//$newBookingID = $tblPackagePurchased->tblPackagePurchased;

		$newBookingID = $tblPackagePurchased->package_purchase_id;

		/*echo $newBookingID;
		exit;*/

		$bookingData = $this->helper->getBookingDetailForPackage($newBookingID,'Club');

		$this->jsonarray['code'] = 200;

		$this->jsonarray['bookingDetail'] = $bookingData;

		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "Package";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');


		if($obj->id)
		{
			if($tblPackage->venue_id)
			{
				$userProfile    = $this->helper->getUserProfile($tblVenue->userid);
				$userIDForPush = $tblVenue->userid;
			}
			else if($tblPackage->company_id)
			{
				$userProfile    = $this->helper->getUserProfile($tblCompany->userid);
				$userIDForPush = $tblCompany->userid;
			}


			/*echo "<pre>";
			print_r($userProfile);
			echo "</pre>";
			exit;*/

			$receiveRequest = 0;

			if($userProfile)
			{
				$params = json_decode($userProfile->params);
				$receiveRequest = $params->settings->pushNotification->receiveRequest;
			}

			//$receiveRequest = 1;

			if($receiveRequest)
			{
				$message = 'You have new Request for '. $tblPackage->package_name;
				$message = JText::sprintf('PUSHNOTIFICATION_TYPE_NEWPACKAGEBOOKINGREQUESTRECEIVED_MESSAGE',$tblPackage->package_name);

				$this->jsonarray['pushNotificationData']['id']         = $obj->id;
				$this->jsonarray['pushNotificationData']['to']         = $userIDForPush;
				$this->jsonarray['pushNotificationData']['message']    = $message;
				$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_NEWPACKAGEBOOKINGREQUESTRECEIVED'); //'NewPackageBookingRequestReceived';
				$this->jsonarray['pushNotificationData']['configtype'] = '';
			}

		}

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
		{
			"extName":"bcted",
			"extView":"package",
			"extTask":"getPackageBookingDetail",
			"taskData":{
				"filter":"string(Request/Booked)"
			}
		}
	 */
	function getPackageBookingDetail()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$bookingID = IJReq::getTaskData('bookingID','','string');

		$userType = "User";

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackagePurchased->load($bookingID);

		if(!$tblPackagePurchased->package_purchase_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGES_BOOKING_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$data = $this->helper->getBookingDetailForPackage($bookingID,$userType,1);

		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;*/

		if(count($data) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGES_BOOKING_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['bookingDetail'] = $data;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
		{
			"extName":"bcted",
			"extView":"package",
			"extTask":"requestRefund",
			"taskData":{
				"bookingID":"number"
			}
		}
	 */

	function requestRefund()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$bookingID = IJReq::getTaskData('bookingID',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');

		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackagePurchased->load($bookingID);

		if(!$tblPackagePurchased->package_purchase_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_ACCEPT_INVALID_DATA5'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblPackagePurchased->status==10)
		{
			IJReq::setResponseCode(707);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_REFUND_ALREADY_DONE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$bookingDT = $tblPackagePurchased->package_datetime . ' ' . $tblPackagePurchased->package_time;
		$bookingTS = strtotime($bookingDT);
		$before24 = strtotime("-24 hours", strtotime($bookingDT));
		$currentTime = time();
		if($currentTime<=$before24)
		{

		}
		else
		{
			IJReq::setResponseCode(102);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_REFUND_NOT_ALLOW'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblPackage = JTable::getInstance('Package', 'BctedTable');
		$tblVenue->load($tblPackagePurchased->venue_id);
		$tblCompany->load($tblPackagePurchased->company_id);
		$tblPackage->load($tblPackagePurchased->package_id);
		$loginUser = JFactory::getUser();



		$tblPackagePurchased->status = 10;
		$tblPackagePurchased->user_status = 10;

		//$message = $loginUser->username . ' has requested refund for '. $tblPackage->package_name;
		$message = JText::sprintf('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREFUND_MESSAGE',$loginUser->username,$tblPackage->package_name);
		$messageType = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREFUND'); //'PackageRequestRefund';

		if(!$tblPackagePurchased->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_ACCEPT_INVALID_DATA5'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		$bookingData = $this->helper->getBookingDetailForPackage($tblPackagePurchased->package_purchase_id,'Club');

		//$bookingData = $this->helper->getBookingDetailForVenueTable($tblVenuebooking->venue_booking_id,'User');
		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "package";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');

		if($packagePurchased->venue_id)
		{
			$pushUserID=$tblVenue->userid;
		}
		else if($packagePurchased->company_id)
		{
			$pushUserID=$tblCompany->userid;
		}

		$this->jsonarray['pushNotificationData']['id']         = $obj->id;
		$this->jsonarray['pushNotificationData']['to']         = $pushUserID;
		$this->jsonarray['pushNotificationData']['message']    = $message;
		$this->jsonarray['pushNotificationData']['type']       = $messageType;
		$this->jsonarray['pushNotificationData']['configtype'] = '';

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
		{
			"extName":"bcted",
			"extView":"package",
			"extTask":"requestRefundForPackageInvitation",
			"taskData":{
				"packageInviteID":"number"
			}
		}
	 */
	function requestRefundForPackageInvitation()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$packageInviteID = IJReq::getTaskData('packageInviteID',0,'int');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');

		$tblPackageInvite = JTable::getInstance('PackageInvite', 'BctedTable');
		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackageInvite->load($packageInviteID);

		if(!$tblPackageInvite->package_invite_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_ACCEPT_INVALID_DATA5'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblPackagePurchased->load($tblPackageInvite->package_purchase_id);

		if(!$tblPackagePurchased->package_purchase_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_ACCEPT_INVALID_DATA5'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblPackageInvite->status==10)
		{
			IJReq::setResponseCode(707);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_REFUND_ALREADY_DONE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$bookingDT = $tblPackagePurchased->package_datetime . ' ' . $tblPackagePurchased->package_time;
		$bookingTS = strtotime($bookingDT);
		$before24 = strtotime("-24 hours", strtotime($bookingDT));
		$currentTime = time();
		if($currentTime<=$before24)
		{

		}
		else
		{
			IJReq::setResponseCode(102);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_REFUND_NOT_ALLOW'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblCompany = JTable::getInstance('Company', 'BctedTable');
		$tblPackage = JTable::getInstance('Package', 'BctedTable');
		$tblVenue->load($tblPackagePurchased->venue_id);
		$tblCompany->load($tblPackagePurchased->company_id);
		$tblPackage->load($tblPackagePurchased->package_id);
		$loginUser = JFactory::getUser();



		$tblPackageInvite->status = 10;


		$message = $loginUser->username . ' has requested refund for '. $tblPackage->package_name;
		//$messageType = 'PackageRequestRefund';
		$message = JText::sprintf('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREFUND_MESSAGE',$loginUser,$tblPackage->package_name);
		$messageType = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREFUND');

		if(!$tblPackageInvite->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_ACCEPT_INVALID_DATA5'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		$bookingData = $this->helper->getBookingDetailForPackage($tblPackagePurchased->package_purchase_id,'Club');

		//$bookingData = $this->helper->getBookingDetailForVenueTable($tblVenuebooking->venue_booking_id,'User');
		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "package";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');

		if($packagePurchased->venue_id)
		{
			$pushUserID=$tblVenue->userid;
		}
		else if($packagePurchased->company_id)
		{
			$pushUserID=$tblCompany->userid;
		}

		$this->jsonarray['pushNotificationData']['id']         = $obj->id;
		$this->jsonarray['pushNotificationData']['to']         = $pushUserID;
		$this->jsonarray['pushNotificationData']['message']    = $message;
		$this->jsonarray['pushNotificationData']['type']       = $messageType;
		$this->jsonarray['pushNotificationData']['configtype'] = '';

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
		{
			"extName":"bcted",
			"extView":"package",
			"extTask":"getPackagesRequests",
			"taskData":{
				"filter":"string(Request/Booked)"
			}
		}
	 */

	function getPackagesRequests()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$filter = IJReq::getTaskData('filter','','string');

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$userType = $this->helper->getUserGroup($this->IJUserID);
		if($userType == 'Club')
		{
			$venueID = $this->helper->getUserVenueID($this->IJUserID);
			if(!$venueID)
			{
				IJReq::setResponseCode(400);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

			// Create the base select statement.
			$query->select('pp.package_purchase_id')
				->from($db->quoteName('#__bcted_package_purchased','pp'))
				->where($db->quoteName('pp.is_deleted') . ' = ' . $db->quote(0))
				->where($db->quoteName('pp.deleted_by_venue') . ' = ' . $db->quote(0))
				->where($db->quoteName('pp.venue_id') . ' = ' . $db->quote($venueID));

			if($filter == 'Booked')
			{
				$statusID = $this->helper->getStatusIDFromStatusName('Booked');

				$query->where($db->quoteName('pp.status') . ' = ' . $db->quote($statusID));
			}
			else if($filter == 'Request')
			{
				$statusID = $this->helper->getStatusIDFromStatusName('Booked');

				$query->where($db->quoteName('pp.status') . ' <> ' . $db->quote($statusID));
			}
		}
		else if($userType == 'ServiceProvider')
		{
			$companyID = $this->helper->getUserCompanyID($this->IJUserID);
			if(!$companyID)
			{
				IJReq::setResponseCode(400);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

			// Create the base select statement.
			$query->select('pp.package_purchase_id')
				->from($db->quoteName('#__bcted_package_purchased','pp'))
				->where($db->quoteName('pp.is_deleted') . ' = ' . $db->quote(0))
				->where($db->quoteName('pp.company_id') . ' = ' . $db->quote($companyID));

			if($filter == 'Booked')
			{
				$statusID = $this->helper->getStatusIDFromStatusName('Booked');

				$query->where($db->quoteName('pp.status') . ' = ' . $db->quote($statusID));
			}
			else if($filter == 'Request')
			{
				$statusID = $this->helper->getStatusIDFromStatusName('Booked');

				$query->where($db->quoteName('pp.status') . ' <> ' . $db->quote($statusID));
			}
		}
		else if($userType == "Registered")
		{
			// Create the base select statement.
			$query->select('pp.package_purchase_id')
				->from($db->quoteName('#__bcted_package_purchased','pp'))
				->where($db->quoteName('pp.deleted_by_user') . ' = ' . $db->quote(0))
				->where($db->quoteName('pp.user_id') . ' = ' . $db->quote($this->IJUserID));

			if($filter == 'Booked')
			{
				$statusID = $this->helper->getStatusIDFromStatusName('Booked');

				$query->where($db->quoteName('pp.status') . ' = ' . $db->quote($statusID));

				$query1 = $db->getQuery(true);

				$query1->select('package_purchase_id')
					->from($db->quoteName('#__bcted_package_invite'))
					->where($db->quoteName('invited_user_id') . ' = ' . $db->quote($this->IJUserID));

				$statusID = $this->helper->getStatusIDFromStatusName('Booked');

				$query1->where($db->quoteName('status') . ' = ' . $db->quote($statusID));

				/*echo $query1->dump();
				exit;*/
				$db->setQuery($query1);
				$invited = $db->loadColumn();

			}
			else if($filter == 'Request')
			{
				$statusID = $this->helper->getStatusIDFromStatusName('Booked');

				$query->where($db->quoteName('pp.status') . ' <> ' . $db->quote($statusID));
			}

			$userType = "User";
		}

		//$packageID = IJReq::getTaskData('packageID',0,'int');
		//$venueID = 27;
		//$query->select('v.venue_name')
		$query->join('LEFT','#__bcted_venue AS v ON v.venue_id=pp.venue_id');
		//$query->join('LEFT','#__bcted_company AS c ON c.company_id=pp.company_id');

		$currentDate = date('Y-m-d');

		//$query->select('v.package_name')
		$query->join('LEFT','#__bcted_package AS pckg ON pckg.package_id=pp.package_id AND pckg.package_active=1');

		/*echo $query->dump();
		exit;*/

		// Set the query and load the result.
		$db->setQuery($query);

		$result = array();

		try
		{
			$result = $db->loadColumn();

			/*if($userType == "User")
			{
				$result = array_merge($result,$invited);
				$result = array_unique($result);


			}*/

			/*echo $userType;
			exit;*/
		}
		catch (RuntimeException $e)
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage($e->getCode() . ' ' . $e->getMessage());
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
			//throw new RuntimeException($e->getMessage(), $e->getCode());
		}





		if(count($result) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGES_BOOKING_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		exit;*/

		$bookingData = $this->helper->getBookingDetailForPackage($result,$userType,0,1);

		if(count($bookingData) == 0)
		{
			IJReq::setResponseCode(204);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGES_BOOKING_NOT_FOUND'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($bookingData);
		$this->jsonarray['packageRequests'] = $bookingData;

		/*if($userType == "Registered")
		{
			$this->jsonarray['packageInvitation'] = $this->getCheckForInvitedPackage($this->IJUserID);
		}*/

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	  	{
	  		"extName":"bcted",
	  		"extView":"package",
	  		"extTask":"packageInvitation",
	  		"taskData":{
	  		}
	  	}
	 */
	function packageInvitation()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$packageInvitations = array();

		$packageInvitations = $this->getCheckForInvitedPackage($this->IJUserID);

		if(count($packageInvitations) == 0)
		{
			$this->jsonarray['code'] = 204;

			return $this->jsonarray;
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($packageInvitations);
		$this->jsonarray['packageInvitation'] = $packageInvitations;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	  	{
	  		"extName":"bcted",
	  		"extView":"package",
	  		"extTask":"packageInvitationDetail",
	  		"taskData":{
	  			"inviteID":"number"
	  		}
	  	}
	 */
	function packageInvitationDetail()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$inviteID = IJReq::getTaskData('inviteID','','string');

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_package_invite'))
			->where($db->quoteName('package_invite_id') . ' = ' . $db->quote($inviteID));
			//->where($db->quoteName('invited_user_id') . ' = ' . $db->quote($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		/*echo $query->dump();
		exit;*/

		$result = $db->loadColumn();

		if(count($result) == 0)
		{
			return array();
		}

		$invitations = $this->helper->getIviteDetailForPackage($result,'User');

		//return $invitations;

		$this->jsonarray['code'] = 200;
		$this->jsonarray['total'] = count($invitations);
		$this->jsonarray['packageInvitation'] = $invitations;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	  	{
	  		"extName":"bcted",
	  		"extView":"package",
	  		"extTask":"cancelPackageRequest",
	  		"taskData":{
	  			"packageBookingID":"number"
	  		}
	  	}
	 */
	function cancelPackageRequest()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$packageBookingID = IJReq::getTaskData('packageBookingID',0,'int');
		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackagePurchased->load($packageBookingID);

		if(!$tblPackagePurchased->package_purchase_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE_BOOKING_ID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblPackagePurchased->user_id != $this->IJUserID)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_NOT_YOUR_BOOKING'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		/*if($tblPackagePurchased->status == 5)
		{
			$dateTimeStamp = strtotime('-1 day',strtotime($tblPackagePurchased->package_datetime.' '.$tblPackagePurchased->package_time));
			$timeStamp = time();

			if($dateTimeStamp < $timeStamp)
			{
				IJReq::setResponseCode(706);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_NOT_YOUR_BOOKING'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}
		}*/

		$tblPackagePurchased->status = 11;
		$tblPackagePurchased->user_status = 11;

		if(!$tblPackagePurchased->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_NOT_CANCELLED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;
	}

	/* @example the json string will be like, :
	  	{
	  		"extName":"bcted",
	  		"extView":"package",
	  		"extTask":"changePackageRequestStatus",
	  		"taskData":{
	  			"requestID":"number",
	  			"message":"string",
	  			"action":"accept/reject"
	  		}
	  	}
	 */
	function changePackageRequestStatus()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$requestID = IJReq::getTaskData('requestID',0,'int');
		$action = IJReq::getTaskData('action','','string');
		$ownerMessage = IJReq::getTaskData('message','','string');

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');

		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackagePurchased->load($requestID);

		/*echo "<pre>";
		print_r($tblPackagePurchased);
		echo "</pre>";
		exit;*/

		$elementID = $this->helper->getUserVenueID($this->IJUserID);
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblPackage = JTable::getInstance('Package', 'BctedTable');
		$tblVenue->load($elementID);
		$tblPackage->load($tblPackagePurchased->package_id);

		/*echo "<pre>";
		print_r($tblPackage);
		echo "</pre>";
		exit;*/

		if(!$tblVenue->venue_id || !$tblPackagePurchased->package_purchase_id || !$tblPackage->package_active || !$tblPackage->package_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_ACCEPT_INVALID_DATA1'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblVenue->venue_id != $tblPackagePurchased->venue_id)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_ACCEPT_INVALID_DATA2'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($tblVenue->venue_id != $tblPackage->venue_id)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_ACCEPT_INVALID_DATA3'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$message = '';
		$messageType = '';

		if($action == 'Accept')
		{
			$tblPackagePurchased->status = 6;
			$tblPackagePurchased->user_status = 3;

			$message = $tblVenue->venue_name . ' has accepted your request for '. $tblPackage->package_name;
			$messageType = 'PackageRequestAccepted';

			$message = JText::sprintf('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTACCEPTED_MESSAGE',$tblVenue->venue_name,$tblPackage->package_name);
			$messageType = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTACCEPTED');

		}
		else if($action == 'Reject')
		{
			$tblPackagePurchased->status = 9;
			$tblPackagePurchased->user_status = 9;

			$message = $tblVenue->venue_name . ' has rejected your request for '. $tblPackage->package_name;
			$messageType = 'PackageRequestRejected';
			$message = JText::sprintf('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREJECTED_MESSAGE',$tblVenue->venue_name,$tblPackage->package_name);
			$messageType = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEREQUESTREJECTED');
		}
		else
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_ACCEPT_INVALID_DATA4'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblPackagePurchased->owner_message = $ownerMessage;

		if(!$tblPackagePurchased->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_REQUEST_ACCEPT_INVALID_DATA5'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		$bookingData = $this->helper->getBookingDetailForPackage($tblPackagePurchased->package_purchase_id,'User');

		//$bookingData = $this->helper->getBookingDetailForVenueTable($tblVenuebooking->venue_booking_id,'User');
		$pushcontentdata=array();

		$pushcontentdata['data'] = $bookingData;
		$pushcontentdata['elementType'] = "package";

		$pushOptions['detail']['content_data'] = $pushcontentdata;
		$pushOptions = gzcompress(json_encode($pushOptions));

		$obj          = new stdClass();
		$obj->id      = null;
		$obj->detail  = $pushOptions;
		$obj->tocount = 1;
		$this->db->insertObject('#__ijoomeradv_push_notification_data',$obj,'id');



		$this->jsonarray['pushNotificationData']['id']         = $obj->id;
		$this->jsonarray['pushNotificationData']['to']         = $tblPackagePurchased->user_id;
		$this->jsonarray['pushNotificationData']['message']    = $message;
		$this->jsonarray['pushNotificationData']['type']       = $messageType;
		$this->jsonarray['pushNotificationData']['configtype'] = '';

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"package",
	 * 		"extTask":"confirmPackagePurchase",
	 * 		"taskData":{
	 * 			"requestID":"number",
	 * 			"emails":"string"
	 * 		}
	 * 	}
	 */
	function confirmPackagePurchase()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$requestID = IJReq::getTaskData('requestID',0,'int');

		if(!$requestID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');

		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackagePurchased->load($requestID);

		$tblPackage = JTable::getInstance('Package', 'BctedTable');
		$tblVenue = JTable::getInstance('Venue', 'BctedTable');
		$tblCompany = JTable::getInstance('Company', 'BctedTable');

		$tblPackage->load($tblPackagePurchased->package_id);

		if($tblPackagePurchased->user_id != $this->IJUserID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if(!$tblPackage->package_id || $tblPackage->package_active == 0)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$tblVenue->load($tblPackage->venue_id);
		$tblCompany->load($tblPackage->company_id);

		$invitedFriends = IJReq::getTaskData('invitedFriends',0, 'int');
		$emails         = IJReq::getTaskData('emails','','string');

		$packagePrice = $tblPackage->package_price;
		$invitedPersonCount = 1;

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('invited_email')
			->from($db->quoteName('#__bcted_package_invite'))
			->where($db->quoteName('package_purchase_id') . ' = ' . $db->quote($tblPackagePurchased->package_purchase_id));

		// Set the query and load the result.
		$db->setQuery($query);

		$alreadyInvitationSend = $db->loadColumn();
		$alreadyInvitedCount = count($alreadyInvitationSend);



		if($alreadyInvitedCount >= $tblPackagePurchased->package_number_of_guest)
		{
			$this->jsonarray['code'] = 500;
			$this->jsonarray['message'] = JText::_('COM_IJOOMERADV_BCTED_GUEST_LIST_LIMIT_OVER');
			return $this->jsonarray;
		}

		if(!empty($emails) && $alreadyInvitedCount!=0)
		{
			$bctRegiEmails = $this->helper->filterEmails($emails);

			$emailsArray = explode(",", $emails);
			$filterEmails = array();

			foreach ($emailsArray as $key => $singleEmail)
			{
				if(in_array($singleEmail, $bctRegiEmails['allRegEmail']))
				{
					if(in_array($singleEmail, $bctRegiEmails['service'])){ continue; }
					if(in_array($singleEmail, $bctRegiEmails['venue'])){ continue; }
					if(in_array($singleEmail, $bctRegiEmails['guest']) && $this->my->email == $singleEmail){ continue; }
				}

				$filterEmails[] = $singleEmail;
			}
			$emailArray = $filterEmails;

			if(count($emailArray) == 0)
			{
				IJReq::setResponseCode(300);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_TABLE_INVITE_NOT_VALID_SELF_EMAIL_OR_CLUB_SERVICE'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

			if(($alreadyInvitedCount+count($emailArray)) >= $tblPackagePurchased->package_number_of_guest)
			{
				$this->jsonarray['code'] = 500;
				$this->jsonarray['message'] = JText::_('COM_IJOOMERADV_BCTED_GUEST_LIST_LIMIT_OVER');
				return $this->jsonarray;
			}

			$newEmail =array();
			foreach ($emailArray as $key => $sEmail)
			{
				if(!in_array($sEmail, $alreadyInvitationSend))
				{
					$newEmail[] = $sEmail;
				}
			}

			if(count($newEmail) == 0)
			{
				$newBookingID = $tblPackagePurchased->package_purchase_id;
				$this->jsonarray['code'] = 200;
				$this->jsonarray['bookingID'] = $newBookingID;

				return $this->jsonarray;
			}

			$emails = implode(",", $newEmail);
		}

		$emailArray = explode(",", $emails);
		if( count($emailArray) >= $tblPackagePurchased->package_number_of_guest)
		{
			$this->jsonarray['code'] = 500;
			$this->jsonarray['message'] = JText::_('COM_IJOOMERADV_BCTED_GUEST_LIST_LIMIT_OVER');
			return $this->jsonarray;
		}


		if(!empty($emails))
		{
			$emailArray = explode(",", $emails);
			$invitedPersonCount += count($emailArray);
		}

		$invitedPersonCount = $invitedPersonCount + $alreadyInvitedCount;

		//echo $invitedPersonCount . ' || ' . $packagePrice . "<br />";
		//exit;

		$singleUserPay = (($packagePrice * $tblPackagePurchased->package_number_of_guest)/$invitedPersonCount);

		/*echo $singleUserPay;
		exit;*/

		//$singleUserPay = number_format($singleUserPay,2);

		$tblPackagePurchased->total_price = $singleUserPay;
		$tblPackagePurchased->invited_email_count = $invitedPersonCount;

		if(!$tblPackagePurchased->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE2'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$newBookingID = $tblPackagePurchased->package_purchase_id;

		//$bookingData = $this->helper->getBookingDetailForPackage($newBookingID);
		//$this->jsonarray['bookingDetail'] = $bookingData;

		$userEmail = array();
		$pushToUser = array();

		if(!empty($emails))
		{
			$emailArray = explode(",", $emails);
			//$emailArray = $emails;
			foreach ($emailArray as $key => $singleEmail)
			{
				// Initialiase variables.
				$db    = JFactory::getDbo();
				$query = $db->getQuery(true);

				// Create the base select statement.
				$query->select('*')
					->from($db->quoteName('#__users'))
					->where($db->quoteName('email') . ' = ' . $db->quote($singleEmail))
					->where($db->quoteName('block') . ' = ' . $db->quote(0));
				$db->setQuery($query);

				$emailUser = $db->loadObject();
				$invitedData = array();

				$tblPackageInvite = JTable::getInstance('PackageInvite', 'BctedTable');
				$tblPackageInvite->load(0);

				if($emailUser)
				{
					$invitedData['invited_email'] = $singleEmail;
					$invitedData['invited_user_id'] = $emailUser->id;
					$invitedData['package_purchase_id'] = $newBookingID;
					$invitedData['package_id'] = $tblPackagePurchased->package_id;
					$invitedData['status'] = 2;
					$invitedData['amount_payable'] = $singleUserPay; // $this->helper->currencyFormat('',$singleUserPay);
					$invitedData['created'] = date('Y-m-d H:i:s');

					$pushToUser[] = $emailUser->id;
				}
				else
				{
					$invitedData['invited_email'] = $singleEmail;
					$invitedData['invited_user_id'] = 0;
					$invitedData['package_purchase_id'] = $newBookingID;
					$invitedData['package_id'] = $tblPackagePurchased->package_id;
					$invitedData['status'] = 2;
					$invitedData['amount_payable'] = $singleUserPay; // $this->helper->currencyFormat('',$singleUserPay);
					$invitedData['created'] = date('Y-m-d H:i:s');
				}

				$tblPackageInvite->bind($invitedData);

				if($tblPackageInvite->store())
				{
					if($tblPackage->venue_id)
					{
						$this->sendPackagePurchageEmail2($singleEmail, JUri::base(),$tblPackage->package_name,$tblVenue->venue_name, date('d-m-Y',strtotime($tblPackagePurchased->package_datetime)),$tblPackagePurchased->package_time);
					}
					else if ($tblPackage->company_id)
					{
						$this->sendPackagePurchageEmail2($singleEmail, JUri::base(),$tblPackage->package_name,$tblCompany->company_name, date('d-m-Y',strtotime($tblPackagePurchased->package_datetime)),$tblPackagePurchased->package_time);
					}

				}
			}
		}

		$this->jsonarray['code'] = 200;
		$this->jsonarray['bookingID'] = $newBookingID;

		$this->jsonarray['paymentURL']= JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $newBookingID . '&booking_type=package';

		$link = JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $newBookingID . '&booking_type=package';

		if(count($pushToUser)!=0)
		{
			$loginUser = JFactory::getUser();

			$htmlLink = '<a href="'.$link.'"> Click Here</a>';

			$htmlLink = "";

			//$inviteID =  getIviteDetailForPackage($ids,$userType);

			$message = JText::sprintf('COM_IJOOMERADV_BCTED_PACKAGE_PURCHASE_PAYMENT_MAIL_BODY', $loginUser->name,$packageName,$htmlLink);

			$this->jsonarray['pushNotificationData']['id']         = $newBookingID;
			$this->jsonarray['pushNotificationData']['to']         = implode(",", $pushToUser);
			$this->jsonarray['pushNotificationData']['message']    = $message.';'.$link;
			$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEPURCHASEDGUESTINVITE'); //'PackagePurchasedGuestInvite';
			$this->jsonarray['pushNotificationData']['configtype'] = '';

			//mail('bcted.developer@gmail.com', 'Payment Push notification', json_encode($this->jsonarray));

			/*$this->jsonarray['pushNotificationData']['id']         = $newBookingID;
			$this->jsonarray['pushNotificationData']['to']         = implode(",", $pushToUser);
			$this->jsonarray['pushNotificationData']['message']    = $message.';'.$link;
			$this->jsonarray['pushNotificationData']['type']       = 'PackagePurchasedGuestInvite';
			$this->jsonarray['pushNotificationData']['configtype'] = '';*/
		}

		return $this->jsonarray;

	}

	function goForInvitedPackagePayment()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$packageInviteID = IJReq::getTaskData('packageInviteID',0,'int');

		if(!$packageInviteID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');

		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackagePurchasedNew = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackageInvite = JTable::getInstance('PackageInvite', 'BctedTable');
		$tblPackage = JTable::getInstance('Package', 'BctedTable');

		$tblPackageInvite->load($packageInviteID);
		//$tblPackage->load($tblPackageInvite->package_id);
		$tblPackagePurchased->load($tblPackageInvite->package_purchase_id);

		$purchaseData['package_id']       = $tblPackagePurchased->package_id;
		$purchaseData['user_id']          = $this->IJUserID;
		$purchaseData['parent_booking_id'] = $tblPackagePurchased->package_purchase_id;
		$purchaseData['venue_id']         = $tblPackagePurchased->venue_id;
		$purchaseData['company_id']       = $tblPackagePurchased->company_id;
		$purchaseData['status']           = 6;
		$purchaseData['user_status']      = 3;
		$purchaseData['male_count']       = $$tblPackagePurchased->male_count;
		$purchaseData['female_count']     = $$tblPackagePurchased->female_count;
		$purchaseData['package_location'] = $tblPackagePurchased->package_location;
		$purchaseData['time_stamp']       = time();
		$purchaseData['package_datetime'] = $tblPackagePurchased->package_datetime;
		$purchaseData['package_time']           = $tblPackagePurchased->package_time;
		$purchaseData['package_number_of_guest']    = $tblPackagePurchased->package_number_of_guest;
		$purchaseData['package_additional_info']    = $tblPackagePurchased->package_additional_info;
		$purchaseData['package_purchased_datetime'] = date('Y-m-d H:i:s');

		$tblPackagePurchasedNew->laod(0);

		/*$tblPackagePurchasedNew->bind($purchaseData);

		if(!$tblPackagePurchasedNew->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_BOOKING_FAILED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}*/


	}

	function getCheckForInvitedPackage($userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('*')
			->from($db->quoteName('#__bcted_package_invite'))
			->where($db->quoteName('invited_user_id') . ' = ' . $db->quote($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		/*echo $query->dump();
		exit;*/

		$result = $db->loadColumn();

		if(count($result) == 0)
		{
			return array();
		}

		$invitations = $this->helper->getIviteDetailForPackage($result,'User');

		return $invitations;
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"package",
	 * 		"extTask":"myPackageBooking",
	 * 		"taskData":{
	 * 		}
	 * 	}
	 */
	function myPackageBooking()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		// $packageID = IJReq::getTaskData('packageID',0,'int');

		$userType = $this->helper->getUserGroup($this->IJUserID); // Return Registered OR Club

		if($userType=="Registered")
		{
			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('*')
				->from($db->quoteName('#__bcted_package_invite'))
				->where($db->quoteName('invited_user_id') . ' = ' . $db->quote($this->IJUserID));

			// Set the query and load the result.
			$db->setQuery($query);

			/*echo $query->dump();
			exit;*/

			$result = $db->loadColumn();

			/*echo "<pre>";
			print_r($result);
			echo "</pre>";
			exit;*/

			if(count($result) == 0)
			{
				IJReq::setResponseCode(204);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_NO_INVITATION_FOR_PACKAGE'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

			$invitations = $this->helper->getIviteDetailForPackage($result,'User');

			$this->jsonarray['code'] = 200;

			$this->jsonarray['packageInvitation'] = $invitations;

			return $this->jsonarray;
		}
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"package",
	 * 		"extTask":"purchasePackage",
	 * 		"taskData":{
	 * 			"packageID":"number",
	 * 			"date":"date",
	 * 			"time":"time",
	 * 			"location":"string",
	 * 			"guestsCount":"number",
	 * 			"additionalInfo":"string",
	 * 			"emails":"string",
	 *			"fbIDs":"string"
	 * 		}
	 * 	}
	 */
	function purchasePackage()
	{

		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$packageID = IJReq::getTaskData('packageID',0,'int');

		if(!$packageID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');

		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackage = JTable::getInstance('Package', 'BctedTable');

		$tblPackage->load($packageID);

		if(!$tblPackage->package_id || $tblPackage->package_active == 0)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$date           = IJReq::getTaskData('date','','string');
		$time           = IJReq::getTaskData('time','','string');
		$location       = IJReq::getTaskData('location','','string');
		//$guestsCount  = IJReq::getTaskData('guestsCount',0, 'int');
		$maleCount      = IJReq::getTaskData('maleCount',0, 'int');
		$femaleCount    = IJReq::getTaskData('femaleCount',0, 'int');
		$guestsCount    = $maleCount + $femaleCount;
		$invitedFriends = IJReq::getTaskData('invitedFriends',1, 'int');
		$additionalInfo = IJReq::getTaskData('additionalInfo','','string');
		$emails         = IJReq::getTaskData('emails','','string');
		$fbIDs          = IJReq::getTaskData('fbIDs','','string');

		if(empty($date) || empty($time))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_VENUE_BOOKING_INVALID_BOOKING_DETAIL'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		//$tblVenuebooking = JTable::getInstance('Venuebooking', 'BctedTable');

		$tblPackagePurchased->load(0);
		$packageName = $tblPackage->package_name;

		$purchaseData['package_id']                 = $tblPackage->package_id;
		$purchaseData['user_id']                    = $this->IJUserID;
		$purchaseData['package_datetime']           = $date . " " . $time;
		$purchaseData['package_number_of_guest']    = $guestsCount;
		$purchaseData['male_count']                 = $maleCount;
		$purchaseData['female_count']               = $femaleCount;
		$purchaseData['package_location']           = $location;
		$purchaseData['package_additional_info']    = $additionalInfo;
		$purchaseData['package_purchased_datetime'] = date('Y-m-d H:i:s');
		$purchaseData['time_stamp']                 = time();

		$tblPackagePurchased->bind($purchaseData);

		if(!$tblPackagePurchased->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_BOOKING_FAILED'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$newBookingID = $tblPackagePurchased->package_purchase_id;

		//$bookingData = $this->helper->getBookingDetailForPackage($newBookingID);

		//$this->jsonarray['bookingDetail'] = $bookingData;

		$userEmail = array();
		$pushToUser = array();

		if(!empty($emails))
		{
			$emails = explode(",", $emails);
			$emailArray = $emails;
			$emails = implode("','", $emails);
			$emails = "'".$emails."'";

			// Initialiase variables.
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Create the base select statement.
			$query->select('a.*')
				->from($db->quoteName('#__users','a'))
				->where($db->quoteName('a.email') . ' IN (' . $emails . ')')
				->where($db->quoteName('a.block') . ' = ' . $db->quote(0));

			$query->select('b.device_token')
					->join('LEFT','#__ijoomeradv_users AS b ON b.userid=a.id AND b.device_token<>""');

			// Set the query and load the result.
			$db->setQuery($query);

			$emailFounds = $db->loadObjectList();

			/*echo "<pre>";
			print_r($emailFounds);
			echo "</pre>";
			exit;*/

			$userEmail = array();
			$pushToUser = array();

			foreach ($emailFounds as $key => $user)
			{
				$userEmail[] = $user->email;
				$pushToUser[] = $user->id;
			}

			$emailToMail = array_diff($emailArray, $userEmail);

			/*echo "<pre>";
			print_r($emailToMail);
			echo "</pre>";
			exit;*/


		}

		$myFbId  = IJReq::getTaskData('myFbId','','string');
		/*echo $fbIDs;
		exit;*/

		$myFbId = trim($myFbId);

		if(!empty($myFbId))
		{
			JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
			$tblProfile       = JTable::getInstance('Profile', 'BctedTable');
			$profileID = $this->helper->getUserProfileID($this->IJUserID);
			$tblProfile->load($profileID);

			$tblProfile->fbid = trim($myFbId);

			$tblProfile->store();
		}

		$this->jsonarray['code'] = 200;

		$this->jsonarray['paymentURL']= JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $newBookingID . '&booking_type=package';

		$link = JUri::base().'index.php?option=com_bcted&task=packageorder.packagePurchased&booking_id=' . $newBookingID . '&booking_type=package';

		if(count($emailToMail)!=0)
		{
			$this->sendPackagePurchageEmail($emailToMail,$link,$packageName);
		}

		if(count($pushToUser)!=0)
		{
			$loginUser = JFactory::getUser();

			$htmlLink = '';

			$message = JText::sprintf('COM_IJOOMERADV_BCTED_PACKAGE_PURCHASE_PAYMENT_MAIL_BODY', $loginUser->name,$packageName,$htmlLink);

			$this->jsonarray['pushNotificationData']['id']         = $newBookingID;
			$this->jsonarray['pushNotificationData']['to']         = implode(",", $pushToUser);
			$this->jsonarray['pushNotificationData']['message']    = $message.';'.$link;
			$this->jsonarray['pushNotificationData']['type']       = JText::_('PUSHNOTIFICATION_TYPE_PACKAGEPURCHASEDGUESTINVITE'); //'PackagePurchasedGuestInvite';
			$this->jsonarray['pushNotificationData']['configtype'] = '';
		}

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"package",
	 * 		"extTask":"deletePackageBooking",
	 * 		"taskData":{
	 * 			"packageBookingID":"number"
	 * 		}
	 * 	}
	 */

	function deletePackageBooking()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$packageBookingID = IJReq::getTaskData('packageBookingID',0,'int');

		if(!$packageBookingID)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblPackagePurchased = JTable::getInstance('PackagePurchased', 'BctedTable');
		$tblPackagePurchased->load($packageBookingID);

		if(!$tblPackagePurchased->package_purchase_id)
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$userGroup = $this->helper->getUserGroup($this->IJUserID);
		if($userGroup == 'Club')
		{
			$venueID = $this->helper->getUserVenueID($this->IJUserID);

			if($venueID != $tblPackagePurchased->venue_id)
			{
				IJReq::setResponseCode(706);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

			$tblPackagePurchased->deleted_by_venue = 1;
		}
		else if($userGroup == 'Registered')
		{
			if($this->IJUserID != $tblPackagePurchased->user_id)
			{
				IJReq::setResponseCode(706);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

			$tblPackagePurchased->deleted_by_user = 1;
		}
		else
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}





		/*$statusID = $this->helper->getStatusIDFromStatusName('Booked');

		if($statusID != $tblPackagePurchased->status)
		{
			IJReq::setResponseCode(706);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}*/

		// $tblPackagePurchased->is_deleted = 1;

		if(!$tblPackagePurchased->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_PACKAGE_INVALID_PACKAGE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;
	}

	function sendPackagePurchageEmail($emailArray, $link,$packageName)
	{
		// Initialise variables.
		$app     = JFactory::getApplication();
		$config  = JFactory::getConfig();

		$site    = $config->get('sitename');
		$from    = $config->get('mailfrom');
		$sender  = $config->get('fromname');
		$email   = $emailArray; //implode(",", $emailArray);//$app->input->get('mailto');

		/*echo $email;
		exit;*/

		$subject = JText::sprintf('COM_IJOOMERADV_BCTED_PACKAGE_PURCHASE_PAYMENT_MAIL_SUBJECT', $packageName);//$app->input->get('subject');

		$loginUser = JFactory::getUser();

		$htmlLink = '<a href="'.$link.'">Click Here</a>';

		// Build the message to send.
		$body     = JText::sprintf('COM_IJOOMERADV_BCTED_PACKAGE_PURCHASE_PAYMENT_MAIL_BODY', $loginUser->name,$packageName,$htmlLink);

		/*echo $body;
		exit;*/
		//$body    = sprintf($msg, $site, $sender, $from);

		// Clean the email data.
		$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);

		//$email = array("aljo1597@gmail.com","maan1539@gmail.com");
		//$email = explode($ema, string)

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body,true);

		// Check for an error.
		/*if ($return !== true)
		{
			return new JException(JText::_('COM__SEND_MAIL_FAILED'), 500);
		}*/
	}

	function sendPackagePurchageEmail2($emailArray,$link,$packageName,$venueName, $packageDate,$packageTime)
	{
		$packageTime = explode(":", $packageTime);
		$packageTime = $packageTime[0].':'.$packageTime[1];

		// Initialise variables.
		$app     = JFactory::getApplication();
		$config  = JFactory::getConfig();

		$site    = $config->get('sitename');
		$from    = $config->get('mailfrom');
		$sender  = $config->get('fromname');
		$email   = $emailArray; //implode(",", $emailArray);//$app->input->get('mailto');

		/*echo "<pre>";
		print_r($email);
		echo "</pre>";
		exit;*/

		$subject = JText::sprintf('COM_IJOOMERADV_BCTED_PACKAGE_PURCHASE_PAYMENT_INVITE_MAIL_SUBJECT', $packageName);//$app->input->get('subject');

		$loginUser = JFactory::getUser();

		$htmlLink = '<a href="'.JUri::base().'">Website</a>';
		$imgPath = JUri::base().'images/footer-logo.png';
		$imageLink = '<img src="'.$imgPath.'"/>';

		// Build the message to send.
		$body     = JText::sprintf('COM_IJOOMERADV_BCTED_PACKAGE_PURCHASE_PAYMENT_INVITE_MAIL_BODY',$loginUser->name,$venueName,$packageDate,$packageTime,$htmlLink,$imageLink);

		/*echo $body;
		exit;*/
		//$body    = sprintf($msg, $site, $sender, $from);

		// Clean the email data.
		$sender  = JMailHelper::cleanAddress($sender);
		$subject = JMailHelper::cleanSubject($subject);
		$body    = JMailHelper::cleanBody($body);

		//$email = array("aljo1597@gmail.com","maan1539@gmail.com");
		//$email = explode($ema, string)

		// Send the email.
		$return = JFactory::getMailer()->sendMail($from, $sender, $email, $subject, $body,true);

		// Check for an error.
		if ($return !== true)
		{
			return new JException(JText::_('COM__SEND_MAIL_FAILED'), 500);
		}
	}


	function uplaodFile($file,$elementType,$userID)
	{
		//$file = JRequest::getVar('image','','FILES','array');
		$uploadedImage = "";
		$uploadLimit	= 8;
		$uploadLimit	= ( $uploadLimit * 1024 * 1024 );

		if(is_array($file) && isset($file['size']) && $file['size']>0)
		{
			jimport('joomla.filesystem.file');
			jimport('joomla.utilities.utility');

			if( filesize( $file['tmp_name'] ) > $uploadLimit && $uploadLimit != 0 )
			{
				/*IJReq::setResponseCode(416);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_IMAGE_FILE_SIZE_EXCEEDED'));
				return false;*/
				return '';
			} // End of if( filesize( $file['tmp_name'] ) > $uploadLimit && $uploadLimit != 0 )

			//$imageMaxWidth	= 160;
			$filename = JApplication::getHash( $file['tmp_name'] . time() );
			$hashFileName	= JString::substr( $filename , 0 , 24 );
			$info['extension'] = pathinfo($file['name'],PATHINFO_EXTENSION);
			$info['extension'] = '.'.$info['extension'];

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/"))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/");
			}

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/".$elementType."/"))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/".$elementType."/");
			}

			if(!JFolder::exists(JPATH_ROOT . "images/bcted/".$elementType."/". $userID . '/'))
			{
				JFolder::create(JPATH_ROOT . "images/bcted/".$elementType."/". $userID . '/');
			}

			$storage      = JPATH_ROOT . '/images/bcted/'.$elementType.'/'. $userID . '/';
			$storageImage = $storage . '/' . $hashFileName .  $info['extension'] ;
			$uploadedImage   = 'images/bcted/'.$elementType.'/' .$userID .'/'. $hashFileName . $info['extension'] ;

			if(!JFile::upload($file['tmp_name'], $storageImage))
		    {
				//$venueImage = "";
				return '';
		    }

		    return $uploadedImage;

		} // End of if(is_array($file) && $file['size']>0)

		return '';
	}


}