<?php
/**
 * @package     iJoomerAdv.Site
 * @subpackage  com_ijoomeradv
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.helper' );
class favourite
{

	private $db;
	private $IJUserID;
	private $helper;
	private $jsonarray;

	function __construct()
	{
		$this->db                = JFactory::getDBO();
		$this->mainframe         =  JFactory::getApplication ();
		$this->IJUserID          = $this->mainframe->getUserState ( 'com_ijoomeradv.IJUserID', 0 ); //get login user id
		$this->my                = JFactory::getUser ( $this->IJUserID ); // set the login user object
		$this->helper            = new bctedAppHelper;
		$this->jsonarray         = array();
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"favourite",
	 * 		"extTask":"addFavourite",
	 * 		"taskData":{
	 *   		"elementID":"number",
	 *   		"elementType":"string" // Company or Venue
	 * 		}
	 * 	}
	 */
	function addFavourite()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$elementID = IJReq::getTaskData('elementID',0,'int');
		$elementType = IJReq::getTaskData('elementType','','string');

		$availableType = array('Service','Venue');

		if(!$elementID || empty($elementType))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_FAVOURITE_DATA_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if(!in_array($elementType, $availableType))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_FAVOURITE_DATA_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if($this->checkForAlreadyFavourite($elementID,$elementType,$this->IJUserID))
		{
			if($elementType == 'Service')
			{
				IJReq::setResponseCode(707);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_FAVOURITE_COMPANY_SERVICE_ALREADY_IN_FAVOURITE_LIST'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}
			else if($elementType == 'Venue')
			{
				IJReq::setResponseCode(707);
				IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_FAVOURITE_VENUE_ALREADY_IN_FAVOURITE_LIST'));
				IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

				return false;
			}

		}

		JTable::addIncludePath(JPATH_SITE . '/administrator/components/com_bcted/tables');
		$tblFavourite = JTable::getInstance('Favourite', 'BctedTable');
		$tblFavourite->load(0);

		$data = array();

		$data['favourite_type']     = $elementType;
		$data['favourited_id']      = $elementID;
		$data['user_id']            = $this->IJUserID;
		$data['favourite_datetime'] = date('Y-m-d h:i:s');
		$data['time_stamp']         = time();

		$tblFavourite->bind($data);

		if(!$tblFavourite->store())
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_FAVOURITE_DATA_NOT_SAVE'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		$this->jsonarray['code'] = 200;

		return $this->jsonarray;
	}

	function checkForAlreadyFavourite($elementID,$elementType,$userID)
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('favourite_id')
			->from($db->quoteName('#__bcted_favourites'))
			->where($db->quoteName('favourited_id') . ' = ' . $db->quote($elementID))
			->where($db->quoteName('favourite_type') . ' = ' . $db->quote($elementType))
			->where($db->quoteName('user_id') . ' = ' . $db->quote($userID));

		// Set the query and load the result.
		$db->setQuery($query);

		return $db->loadResult();
	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"favourite",
	 * 		"extTask":"deleteFavourite",
	 * 		"taskData":{
	 *	  		"elementID":"number",
	 *	    	"elementType":"string" // Company or Venue
	 * 		}
	 * 	}
	 */
	function deleteFavourite()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$elementID = IJReq::getTaskData('elementID',0,'int');
		$elementType = IJReq::getTaskData('elementType','','string');

		$availableType = array('Service','Venue');

		if(!$elementID || empty($elementType))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_FAVOURITE_DATA_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		if(!in_array($elementType, $availableType))
		{
			IJReq::setResponseCode(400);
			IJReq::setResponseMessage(JText::_('COM_IJOOMERADV_FAVOURITE_DATA_NOT_VALID'));
			IJException::setErrorInfo(__FILE__, __LINE__, __CLASS__, __METHOD__, __FUNCTION__);

			return false;
		}

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base delete statement.
		$query->delete()
			->from($db->quoteName('#__bcted_favourites'))
			->where($db->quoteName('user_id') . ' = ' . $db->quote((int) $this->IJUserID))
			->where($db->quoteName('favourite_type') . ' = ' . $db->quote($elementType))
			->where($db->quoteName('favourited_id') . ' = ' . $db->quote((int) $elementID));

		// Set the query and execute the delete.
		$db->setQuery($query);

		try
		{
			$db->execute();

			$this->jsonarray['code'] = 200;

			return $this->jsonarray;
		}
		catch (RuntimeException $e)
		{
			IJReq::setResponseCode(500);
			IJReq::setResponseMessage(JText::_($e->getMessage() . ' - ' . $e->getCode()));
			return false;
		}

		$this->jsonarray['code'] = 500;

		return $this->jsonarray;

	}

	/* @example the json string will be like, :
	 * 	{
	 * 		"extName":"bcted",
	 * 		"extView":"favourite",
	 * 		"extTask":"getFavourite",
	 * 		"taskData":{
	 * 		}
	 * 	}
	 */
	function getFavourite()
	{
		if(!$this->IJUserID)
		{
			IJReq::setResponse(704);
			IJException::setErrorInfo(__FILE__,__LINE__,__CLASS__,__METHOD__,__FUNCTION__);
			return false;
		} // End of login Condition

		$favouriteVenue = array();
		$favouriteCompanyService = array();

		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		// Create the base select statement.
		$query->select('favourited_id')
			->from($db->quoteName('#__bcted_favourites'))
			->where($db->quoteName('user_id') . ' = ' . $db->quote($this->IJUserID))
			->where($db->quoteName('favourite_type') . ' = ' . $db->quote('Service'));

		// Set the query and load the result.
		$db->setQuery($query);
		$favService = $db->loadColumn();

		if(count($favService) != 0)
		{
			$query = $db->getQuery(true);
			// Create the base select statement.
			$query->select('company_id')
				->from($db->quoteName('#__bcted_company'))
				->where($db->quoteName('company_id') . ' IN (' . implode(",", $favService) . ')')
				->where($db->quoteName('company_active') . ' = ' . $db->quote(1))
				->order($db->quoteName('company_name') . ' ASC ');

			// Set the query and load the result.
			$db->setQuery($query);
			$favServices = $db->loadColumn();
			foreach ($favServices as $key => $favourite)
			{
				$tmpData = $this->helper->getCompanyDetail($favourite);
				if(count($tmpData) != 0)
				{
					$favouriteCompanyService[] = $tmpData;
				}
			}

		}



		$query = $db->getQuery(true);
		// Create the base select statement.
		$query->select('favourited_id')
			->from($db->quoteName('#__bcted_favourites'))
			->where($db->quoteName('user_id') . ' = ' . $db->quote($this->IJUserID))
			->where($db->quoteName('favourite_type') . ' = ' . $db->quote('Venue'));

		// Set the query and load the result.
		$db->setQuery($query);
		$favVenue = $db->loadColumn();

		if(count($favVenue) != 0)
		{
			$query = $db->getQuery(true);
			// Create the base select statement.
			$query->select('venue_id')
				->from($db->quoteName('#__bcted_venue'))
				->where($db->quoteName('venue_id') . ' IN (' . implode(",", $favVenue) . ')')
				->where($db->quoteName('venue_active') . ' = ' . $db->quote(1))
				->order($db->quoteName('venue_name') . ' ASC ');

			// Set the query and load the result.
			$db->setQuery($query);
			$favVenues = $db->loadColumn();
			foreach ($favVenues as $key => $favourite)
			{
				$tmpData = $this->helper->getVenueDetail($favourite);
				if(count($tmpData) != 0)
				{
					$favouriteVenue[] = $tmpData;
				}
			}

		}



		/*$resultFavourites = array();



		foreach ($favourites as $key => $favourite)
		{
			if($favourite->favourite_type == "Service")
			{

				//$tmpData = $this->helper->getCompanyServiceDetail($favourite->favourited_id,1);
				$tmpData = $this->helper->getCompanyDetail($favourite->favourited_id);
				if(count($tmpData) != 0)
				{
					$favouriteCompanyService[] = $tmpData;
				}
			}
			else if ($favourite->favourite_type == "Company")
			{
				//$company[] = $this->helper->getCompanyDetail($favourite->favourited_id);
			}
			else if ($favourite->favourite_type == "Venue")
			{
				$favouriteVenue[] = $this->helper->getVenueDetail($favourite->favourited_id);
			}
		}*/

		if(count($favouriteVenue) == 0 && count($favouriteCompanyService) == 0)
		{
			$this->jsonarray['code'] = 204;

			return $this->jsonarray;
		}



		$this->jsonarray['code']    = 200;
		$this->jsonarray['total']   = (count($favouriteVenue) + count($favouriteCompanyService));
		$this->jsonarray['venues'] = $favouriteVenue;
		$this->jsonarray['companyServices'] = $favouriteCompanyService;

		return $this->jsonarray;
	}
}